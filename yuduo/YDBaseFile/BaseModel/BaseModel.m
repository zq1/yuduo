//
//  BaseModel.m
//  yuduo
//
//  Created by Mac on 2019/7/24.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "BaseModel.h"

@implementation BaseModel
-(instancetype)initWithDic:(NSMutableDictionary *)dic
{
    self = [super init];
    if (self) {
        [self setValuesForKeysWithDictionary:dic];
    }
    return self;
}
-(void)setValue:(id)value forUndefinedKey:(NSString *)key{
    
    if ([key isEqualToString:@"id"]) {
        self.sid = value;
    }
}
@end
