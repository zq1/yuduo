//
//  DisHotLiveTableViewCell.h
//  yuduo
//
//  Created by Mac on 2019/8/5.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DisHotLiveTableViewCell : UITableViewCell
PropertyStrong(UIView , imageBgView);
PropertyStrong(UIImageView, headImageView);

PropertyStrong(UIView, cellView);
@end

NS_ASSUME_NONNULL_END
