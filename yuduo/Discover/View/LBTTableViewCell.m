//
//  LBTTableViewCell.m
//  yuduo
//
//  Created by Mac on 2019/8/5.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "LBTTableViewCell.h"

@implementation LBTTableViewCell
{
    NSArray *_imagesURLStrings;
//    SDCycleScrollView *_customCellScrollViewDemo;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createView];
    }
    return self;
}

- (void)createView {
        self.headBGView = [[UIView alloc]initWithFrame:CGRectMake(15, 15, KScreenW-30, 175)];
//        self.headBGView.backgroundColor = GMRedColor;
        [self.contentView addSubview:self.headBGView];
    NSArray *imagesURLStrings = @[
                                  @"http://yuduo.oss-cn-beijing.aliyuncs.com/image/709550e6e063eddf73a05965f0ed163c.jpg",
                                  @"http://yuduo.oss-cn-beijing.aliyuncs.com/image/e3332348a2d45444c9e4699ec5579e3f.jpg"
                                  
                                  ];
    _imagesURLStrings = imagesURLStrings;
    
    // 网络加载 ---
    UIView *view1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.headBGView.frame.size.width, 175)];
    view1.layer.backgroundColor = GMWhiteColor.CGColor;
    //            NSString *str = [NSString stringWithFormat:@" %ld",pageIndex+1];
    //            NSString *sumLabel = @"/3";
    //            view.text = [str stringByAppendingString:sumLabel];
    view1.layer.cornerRadius = 9;
    [self.headBGView addSubview:view1];
    //            view1.textColor = GMWhiteColor;
    
    SDCycleScrollView *cycleScrollView3 = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, KScreenW-30, 175) delegate:self placeholderImage:[UIImage imageNamed:@"placeholder"]];
    cycleScrollView3.layer.cornerRadius = 10;
    cycleScrollView3.layer.masksToBounds = YES;
    cycleScrollView3.currentPageDotImage = [UIImage imageNamed:@"pageControlCurrentDot"];
    
    cycleScrollView3.pageDotImage = [UIImage imageNamed:@"pageControlDot"];
    cycleScrollView3.imageURLStringsGroup = imagesURLStrings;
    [view1 addSubview:cycleScrollView3];
    self.customCellScrollViewDemo = cycleScrollView3;    
//
//    self.scrollView = [[MCScrollView alloc] initWithFrame:CGRectMake(self.headBGView.frame.origin.x+15, 0, self.headBGView.frame.size.width-30, 175)];
//    [self.headBGView addSubview:self.scrollView];
//    //    self.scrollView.backgroundColor = [UIColor brownColor];
//    __weak typeof(self) weakSelf = self;
//    self.scrollView.indexBack = ^(NSInteger pageIndex) {
//        weakSelf.selectIndex = pageIndex;
//        //        NSLog(@"=========%zd", pageIndex);
//        /*
//         轮播数字
//         **/
//        UILabel *view = [[UILabel alloc] init];
//        //        view.frame = CGRectMake(302,self.scrollView.frame.origin.y-6,34.5,18);
//        view.layer.backgroundColor = GMBlackColor.CGColor;
//        NSString *str = [NSString stringWithFormat:@" %ld",pageIndex+1];
//        NSString *sumLabel = @"/3";
//        view.text = [str stringByAppendingString:sumLabel];
//        //        NSLog(@"xxxxxxxxxxx");
//        view.layer.cornerRadius = 9;
//        view.textColor = GMWhiteColor;
//        [weakSelf.scrollView addSubview:view];
//        view.sd_layout.bottomSpaceToView(weakSelf.scrollView, 6)
//        .rightSpaceToView(weakSelf.scrollView, 9)
//        .widthIs(35).heightIs(18);
//
//
//        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:weakSelf action:@selector(clickImage)];
//        [weakSelf.scrollView addGestureRecognizer:tapGesture];
//        weakSelf.scrollView.userInteractionEnabled = YES;
//    };
//    self.scrollView.imageArray = @[@"1.png", @"2.png", @"3.png"];
//    self.scrollView.duration = 2.0;
    
}

- (void)clickImage {
    //选择图片
    NSLog(@"xxxxxxx%ld",self.selectIndex);
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
