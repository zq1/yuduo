//
//  LBTTableViewCell.h
//  yuduo
//
//  Created by Mac on 2019/8/5.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCScrollView.h"

#import "SDCycleScrollView.h"

#import "UIImageView+WebCache.h"
NS_ASSUME_NONNULL_BEGIN

@interface LBTTableViewCell : UITableViewCell<SDCycleScrollViewDelegate>
PropertyStrong(UIView, headBGView);//轮播图背景
PropertyStrong(MCScrollView, scrollView);
PropertyStrong(SDCycleScrollView, customCellScrollViewDemo);
@property (assign , nonatomic) NSInteger selectIndex;

@end

NS_ASSUME_NONNULL_END
