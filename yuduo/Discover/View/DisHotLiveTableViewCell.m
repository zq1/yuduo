//
//  DisHotLiveTableViewCell.m
//  yuduo
//
//  Created by Mac on 2019/8/5.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "DisHotLiveTableViewCell.h"

@implementation DisHotLiveTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createHotOneCourse];
    };
    return self;
}

- (void)createHotOneCourse {
    
    self.cellView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenW, 255)];
    self.cellView.layer.cornerRadius = 10;
    //    self.cellView.backgroundColor = GMlightGrayColor;
    [self.contentView addSubview:self.cellView];
    
    self.imageBgView = [[UIView alloc]init];
    self.imageBgView.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0];
    [self.cellView addSubview:self.imageBgView];
    self.imageBgView.backgroundColor = GMWhiteColor;
    self.imageBgView.layer.cornerRadius = 10;
    [self.cellView addSubview:self.imageBgView];
    self.imageBgView.sd_layout.topSpaceToView(self.cellView, 0).leftSpaceToView(self.cellView, 0)
    .rightSpaceToView(self.cellView, 0).heightIs(255);
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
