//
//  DisActiveTableViewCell.h
//  yuduo
//
//  Created by Mac on 2019/8/5.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DisActiveTableViewCell : UITableViewCell
PropertyStrong(UIView, bgView);
PropertyStrong(UIImageView, imgView);
PropertyStrong(UILabel , titleLabel);
@end

NS_ASSUME_NONNULL_END
