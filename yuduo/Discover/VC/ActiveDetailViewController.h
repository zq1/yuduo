//
//  ActiveDetailViewController.h
//  yuduo
//
//  Created by Mac on 2019/9/11.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
NS_ASSUME_NONNULL_BEGIN

@interface ActiveDetailViewController : BaseViewController
PropertyStrong(NSString, category_id);
PropertyStrong(NSString, title);
@property(nonatomic,assign)BOOL online;
@end

NS_ASSUME_NONNULL_END
