//
//  DiscoverViewController.m
//  yuduo
//
//  Created by Mac on 2019/7/23.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "DiscoverViewController.h"
#import "LBTTableViewCell.h" //轮播图
#import "DisActiveTableViewCell.h" //热门活动cell
#import "DisHotLiveTableViewCell.h"//热门直播
#import "DisHotZXTableViewCell.h" //热门咨询
#import "MCScrollView.h"
#import "MGBannerModel.h"
#import "GMBannerCell.h"
#import "GMBannerView.h"
#import "HotActiveViewController.h"//热门活动
#import "ActiveViewController.h"
#import "HomeLiveViewController.h" //热门直播
#import "HomeZXViewController.h"
#import "CourseLiveListsModel.h"
//发现活动分类
#import "DisCategory_idModel.h"
//发现活动列表
#import "DisActiveListModel.h"
//活动详情
#import "ActiveDetailViewController.h"
//搜索
#import "SearchViewController.h"
#import "ZXDetailViewController.h"
#import "NewZhiboDetailViewController.h"
//轮播图模型
#import "HomeBannerModel.h"
//热门咨询model
#import "HotZXModel.h"
@interface DiscoverViewController ()<UITableViewDataSource,UITableViewDelegate,GMBannerViewDelegate,SDCycleScrollViewDelegate>
PropertyStrong(UITableView, discoverTab);//发现tableview
PropertyStrong(MCScrollView,scrollView);//轮播图
PropertyStrong(UIView, headBGView);//头部视图
PropertyStrong(NSArray, sectionTitle); //分区数组
PropertyStrong(NSMutableArray, dataArray);
PropertyStrong(UIButton , btn);//滚动按钮
PropertyStrong(NSMutableArray, disCoverListArray);//发现活动列表数组

//获取热门咨询
PropertyStrong(NSMutableArray, hotZXArray);

//@property (nonatomic, weak) SDCycleScrollView *bannerCycleScrollerView; //轮播图
@property (nonatomic, strong) NSMutableArray *homeBannerArray;//轮播图
@property(nonatomic,strong)NSMutableArray *courseLiveArray;//热门直播

@end

@implementation DiscoverViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = NO;
    self.title = @"发现";
    [self setRightBarItemWithImage:@"ico_search"];
    //tablviewview
    [self creatDiscoverTabView];
    //获取分类ID
    [self getCategory_id];
    
    //获取热门咨询
    [self getHotZX];
    
    [self getDataHomeBanner];
    [self getCourseLiveData];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}
-(void)rightBarItemAction:(UITapGestureRecognizer *)gesture{
    //
    NSLog(@"发现搜索");
    SearchViewController *searchVC = [[SearchViewController alloc]init];
    [searchVC setHidesBottomBarWhenPushed:YES];
    [self.navigationController pushViewController:searchVC animated:YES];
}
#pragma mark 创建TableView

- (void)creatDiscoverTabView {
    
    self.sectionTitle = @[@"轮播图",@"热门活动",@"热门直播",@"热门咨询"];
//    self.headBGView = [[UIView alloc]initWithFrame:CGRectMake(0, 11, KScreenW, 217)];
//    //        self.headBGView.backgroundColor = GMRedColor;
//    //    self.bgScrollView.backgroundColor = GMBlueColor;
//    //
    self.discoverTab = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, KScreenW, KScreenH-87)style:UITableViewStyleGrouped];
    self.discoverTab.delegate = self;
    self.discoverTab.dataSource = self;
    self.discoverTab.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.discoverTab.backgroundColor = GMWhiteColor;
    self.discoverTab.tableHeaderView = self.headBGView;
    //    self.HomeTableView.tableHeaderView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 0, CGFLOAT_MAX)];
    self.discoverTab.backgroundView = nil;
    self.discoverTab.backgroundColor = [UIColor clearColor];
    //    self.HomeTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.discoverTab addSubview:self.headBGView];
    [self.view addSubview:self.discoverTab];
}



#pragma mark 返回区高度

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    switch (section) {
        case 0:
            return 0;
            break;
        case 1:
            return 39;
            break;
        case 2:
            return 37;
            break;
        case 3:
            return 42;
            break;

        default:
            break;
    }
    
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    switch (section) {
        case 0:
            return 1;
            break;
        case 1:
            if (self.disCoverListArray.count >= 1) {
                return 1;
            }
            return 0;
            break;
        case 2:
            return 1;
            break;
        case 3:
            return self.hotZXArray.count;
            break;
        default:
            break;
    }
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (indexPath.section) {
        case 0:
            return 220;
            break;
        case 1:
            return 220;
            break;
        case 2:
            return 255;
            break;
        case 3:
            return 100;
            break;
            
            default:
            break;
    }
    return 0;
}

- (NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [NSMutableArray arrayWithObjects:@"1", @"1" , @"1" , @"1", nil];
    }
    return _dataArray;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return self.sectionTitle.count;
}

#pragma mark 点击事件 动画

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.section) {
        case 1:
        {
            ActiveDetailViewController *active = [[ActiveDetailViewController alloc]init];
            DisActiveListModel *mol = [self.disCoverListArray objectAtIndex:indexPath.row];
            active.category_id = mol.activity_id;
            [active setHidesBottomBarWhenPushed:YES];
            [self.navigationController pushViewController:active animated:YES];
        }
            break;
        case 3:
        {
             ZXDetailViewController *zxVC = [[ZXDetailViewController alloc]init];
            HotZXModel *mol = [self.hotZXArray objectAtIndex:indexPath.row];
            zxVC.zxIDString = mol.manag_id;
            zxVC.zxtimeString = mol.manag_time;
            zxVC.zxtitleString = mol.manag_title;
            zxVC.likeNumber = mol.thumbsup;
            [zxVC setHidesBottomBarWhenPushed:YES];
            [self.navigationController pushViewController:zxVC animated:YES];
        }
            break;
            
        default:
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier0 = @"cell0";
    static NSString *cellIdentifier1 = @"cell1";
    static NSString *cellIdentifier2 = @"cell2";
    static NSString *cellIdentifier3 = @"cell3";
    switch (indexPath.section){
            // 轮播图
        case 0:
        {
            LBTTableViewCell *cell0 = [[LBTTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier0];
            cell0.customCellScrollViewDemo.delegate = self;
            NSMutableArray *imageList = [NSMutableArray array];
            for (HomeBannerModel *model in self.homeBannerArray) {
                [imageList addObject:model.picture];
            }
            cell0.customCellScrollViewDemo.imageURLStringsGroup = imageList;
//            cell0.backgroundColor = GMlightGrayColor;
//          cell0.hidden = YES;
            //隐藏section 高度
            self.discoverTab.contentInset = UIEdgeInsetsMake(-35, 0, 0, 0);
            return cell0;
        }
            break;
            //热门活动
        case 1:
        {
            //热门活动列表
            DisActiveTableViewCell *cell1 = [[DisActiveTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier1];
            DisActiveListModel *mol = [self.disCoverListArray objectAtIndex:indexPath.row];
            cell1.titleLabel.text = mol.activity_name;
            [cell1.imgView sd_setImageWithURL:[NSURL URLWithString:mol.activity_cover]];
            cell1.imgView.sd_cornerRadius = [NSNumber numberWithInteger:10];
            //            cell1.backgroundColor = GMlightGrayColor;

            return cell1;
        }
            break;
            //热门直播
        case 2:
        {
            DisHotLiveTableViewCell *cell2 = [[DisHotLiveTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier2];
            NSMutableArray *models = @[].mutableCopy;
                        for (CourseLiveListsModel *dic in self.courseLiveArray) {
                            MGBannerModel *model = [MGBannerModel new];
                            model.title = dic.name;
                            model.picture = dic.publicity_cover;
                            model.date = dic.start_date;
            //                [model setValuesForKeysWithDictionary:dic];
                            [models addObject:model];
                        }
            
            GMBannerView *banner = ({
                
                //                    CGFloat width = self.view.bounds.size.width;
                GMBannerView *banner = [[GMBannerView alloc] initWithFrame:CGRectMake(0, 0,KScreenW, 255)];
                banner.models = models;
                banner.delegate = self;
                banner;
            });
            [cell2.imageBgView addSubview:banner];
            return cell2;
        }
            break;
            //热门咨询
        case 3:
        {
            DisHotZXTableViewCell *cell3 = [[DisHotZXTableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier3];
            HotZXModel *mol = [self.hotZXArray objectAtIndex:indexPath.row];
            cell3.timeLabel.text = mol.manag_time;
            cell3.titleLabel.text = mol.manag_title;
            cell3.peopleLabel.text = mol.thumbsup;
            cell3.dianZanLabel.text = mol.browse;
            [cell3.headImg sd_setImageWithURL:[NSURL URLWithString:mol.manag_picture]];
            cell3.selectionStyle = UITableViewCellSelectionStyleBlue;
            return cell3;
        }
            break;
            
    }
    return nil;
}
-(void)HZBannerViewTest:(GMBannerView *)bannerView didSelectedAt:(NSInteger)indexGM{
    NewZhiboDetailViewController * zhibo = [[NewZhiboDetailViewController alloc] init];
    [self.navigationController pushViewController:zhibo animated:YES];
}
#pragma mark -- 推荐直播
- (void)getCourseLiveData {
    self.courseLiveArray = [NSMutableArray array];
    [GMAfnTools PostHttpDataWithUrlStr:KCourseLiveList Dic:nil SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"----课程---推荐直播--%@",responseObject);
        for (NSMutableDictionary *liveDic in responseObject) {
            CourseLiveListsModel *mol = [[CourseLiveListsModel alloc]init];
            [mol mj_setKeyValues:liveDic];
            
            [self.courseLiveArray addObject:mol];
        }
        [self.discoverTab reloadData];
    } FailureBlock:^(id  _Nonnull error) {
        NSLog(@"-+++++%@",error);
    }];
}
#pragma mark--发现--分类-3活动
- (void)getCategory_id {
    
    NSMutableDictionary *categoryDic = [NSMutableDictionary dictionary];
    categoryDic[@"pid"] = @"3";
    [GMAfnTools PostHttpDataWithUrlStr:KCategoryID Dic:categoryDic SuccessBlock:^(id  _Nonnull responseObject) {
        for (NSMutableDictionary *categoryID in responseObject) {
            
            NSLog(@"x----%@",[categoryID objectForKey:@"category_id"]);
            [self getDiscoverActive:[categoryDic objectForKey:@"category_id"]];
        }
        
    } FailureBlock:^(id  _Nonnull error) {
        
    }];
}
#pragma mark--热门活动
- (void)getDiscoverActive:(NSString *)category_id {
    self.disCoverListArray = [NSMutableArray array];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"category_id"] = category_id;
    [GMAfnTools PostHttpDataWithUrlStr:KDiscoveryList Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"打印发现列表===%@",responseObject);
        for (NSMutableDictionary *activeDic in responseObject) {
            DisActiveListModel *activeModel = [[DisActiveListModel alloc]init];
            [activeModel mj_setKeyValues:activeDic];
            [self.disCoverListArray addObject:activeModel];
        }
        [self.discoverTab reloadData];
    } FailureBlock:^(id  _Nonnull error) {
        
    }];
    
}
#pragma mark 自定义section
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section

{
    [self.discoverTab.tableHeaderView removeFromSuperview];
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenW, 0)];
    //    view.backgroundColor = [UIColor cyanColor];
    //各个模块
    UILabel *sectionTitle = [[UILabel alloc] init];
    sectionTitle.frame = CGRectMake(16,0,90,17);
    //更多
//    UILabel *moreTitle = [[UILabel alloc] init];
//    moreTitle.numberOfLines = 0;
//    NSMutableAttributedString *stringMore = [[NSMutableAttributedString alloc] initWithString:@"更多" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 14],NSForegroundColorAttributeName: [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1.0]}];
//    moreTitle.attributedText = stringMore;
//    moreTitle.textAlignment = NSTextAlignmentRight;
//    [view addSubview:moreTitle];
//    moreTitle.sd_layout.topEqualToView(view).rightSpaceToView(view, 34).widthIs(60).heightIs(13.5);
    
    //更多
    UIButton *gdButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [gdButton setTitle:@"更多" forState:UIControlStateNormal];
    [gdButton addTarget:self action:@selector(moreClick:) forControlEvents:UIControlEventTouchUpInside];
    gdButton.tag = section;
    [gdButton setTitleColor:GMlightGrayColor forState:UIControlStateNormal];
    [gdButton.titleLabel setFont:[UIFont systemFontOfSize:14]];
    [view addSubview:gdButton];
    gdButton.sd_layout.topSpaceToView(view, 0)
    .rightSpaceToView(view, 0 )
    .widthIs(100).heightIs(16);
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:[self.sectionTitle objectAtIndex:section] attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Medium" size: 18],NSForegroundColorAttributeName: [UIColor colorWithRed:23/255.0 green:151/255.0 blue:164/255.0 alpha:1.0]}];
    sectionTitle.attributedText = string;
    [view addSubview:sectionTitle];
    //按钮
    UIButton *moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [moreButton setImage:[UIImage imageNamed:@"ico_next"] forState:UIControlStateNormal];
    [moreButton addTarget:self action:@selector(moreClick:) forControlEvents:UIControlEventTouchUpInside];
    moreButton.tag = section;
    [view addSubview:moreButton];
    moreButton.sd_layout.topSpaceToView(view, 0)
    .rightSpaceToView(view, 15).widthIs(8).heightIs(14);
    
    return view;
    
}

#pragma mark 更多

- (void)moreClick:(UIButton *)send {
    switch (send.tag) {
        case 1:
        {
            NSLog(@"热门活动");
            
            ActiveViewController *activeVC = [[ActiveViewController alloc]init];
            [activeVC setHidesBottomBarWhenPushed:YES];
//            activeVC.navigationController.navigationBar.hidden = YES;
            [self.navigationController pushViewController:activeVC animated:NO];
            
        }
            break;
        case 2:
        {
            
            NSLog(@"热门直播");
            HomeLiveViewController *homeVC = [[HomeLiveViewController alloc]init];
            homeVC.navigationController.navigationBar.hidden = YES;
            homeVC.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:homeVC animated:YES];
            
        }
            
            break;
        case 3:
        {
            NSLog(@"热门咨询");
            HomeZXViewController *homeZX = [[HomeZXViewController alloc]init];
            
            [homeZX setHidesBottomBarWhenPushed:YES];
//            homeZX.navigationController.navigationBar.hidden = YES;
            [self.navigationController pushViewController:homeZX animated:YES];
        }
            break;
        default:
            break;
    }
}


-(NSArray<NSString *> *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return nil;
}


//#pragma mark 修改t导航栏
//- (void)changeNavigation {
//    self.navigationController.navigationBar.barTintColor = GMWhiteColor;//导航栏背景颜色
//    [self.navigationController.navigationBar setTitleTextAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:18], NSForegroundColorAttributeName : RGB(153, 153, 153)}];
//
//    UIButton *searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    [searchBtn setBackgroundImage:[UIImage imageNamed:@"ico_search"] forState:UIControlStateNormal];
//    searchBtn.frame = CGRectMake(0, 0, 21, 20);
//    [searchBtn addTarget:self action:@selector(searchClick) forControlEvents:UIControlEventTouchUpInside];
//
//    UIBarButtonItem *rightBtn = [[UIBarButtonItem alloc]initWithCustomView:searchBtn];
//    self.navigationItem.rightBarButtonItem = rightBtn;
//
//
//}
#pragma mark 搜索按钮

- (void)searchClick {
    NSLog(@"点击发现页搜索按钮");
    SearchViewController *search = [[SearchViewController alloc]init];
    [self.navigationController pushViewController:search animated:YES];
//    //测试
//    HotActiveViewController *hotActive = [[HotActiveViewController alloc]init];
//    [self.navigationController pushViewController:hotActive animated:YES];
    
    
}
//#pragma mark 隐藏导航栏分割线
//- (void)viewWillAppear:(BOOL)animated{
//    [super viewWillAppear:animated];
//    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@""]forBarPosition:UIBarPositionAny barMetrics:UIBarMetricsDefault];
//    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
//    
//}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)initializeButtonWithFrame:(CGRect)frame title:(NSString*)title action:(SEL)aSEL{
    
    self.btn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.btn.frame = frame;
    [self.btn setBackgroundImage:[UIImage imageNamed:@"zhiding"] forState:UIControlStateNormal];
    [self.btn setBackgroundColor:RGB(21, 152, 164)];
    [self.btn setTitle:title forState:0];
    [self.btn addTarget:self action:aSEL forControlEvents:UIControlEventTouchUpInside];
    self.btn.layer.cornerRadius = 25;
    [self.view addSubview:self.btn];
    
}

- (void)scrollToTop:(UIButton*)sender{
    sender.hidden = YES;
    
    NSIndexPath *topView = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.discoverTab scrollToRowAtIndexPath:topView atScrollPosition:UITableViewScrollPositionTop animated:YES];
    //    [self.view setContentOffset:CGPointMake(0, 20) animated:Y÷ES];
    //    [self.HomeTableView setContentOffset:CGPointMake(0, -34) ];
    // (statusbar)
    //判断状态栏高度
//    CGRect rectOfStatusbar = [[UIApplication sharedApplication] statusBarFrame];
//    if (rectOfStatusbar.size.height == 20) {
//        [self.HomeTableView setContentOffset:CGPointMake(0, -20)];
//    }
//    else {
//        [self.HomeTableView setContentOffset:CGPointMake(0, -44)];
//    }
//    NSLog(@"statusbar height: %f", rectOfStatusbar.size.height);   // 高度
//    NSLog(@"滚到顶部");
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    self.btn.hidden  = YES;
    CGPoint point = scrollView.contentOffset;
    if (point.y  > KScreenH-300) {
        //按钮frame设置
        [self initializeButtonWithFrame:CGRectMake(KScreenW-50, KScreenH-300,50, 50) title:@"滚到顶部" action:@selector(scrollToTop:)];
    }
    //打印坐标
    //    NSLog(@"%f,%f",point.x,point.y);
}
#pragma mark--轮播图1
- (void)getDataHomeBanner {
    self.homeBannerArray = [NSMutableArray array];
    NSMutableDictionary *bannerDic = [NSMutableDictionary dictionary];
    bannerDic[@"type"] = @"3";
    bannerDic[@"url_type"] = @"1";
    
    [GMAfnTools PostHttpDataWithUrlStr:KHomeBanner Dic:bannerDic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"首页轮播图-----%@",responseObject);
        NSMutableArray *imageList = [NSMutableArray array];
        for (NSMutableDictionary *dic in responseObject) {
            HomeBannerModel *mol = [[HomeBannerModel alloc]init];
            [mol mj_setKeyValues:dic];
            if ([mol.status isEqualToString:@"1"]) {
                [self.homeBannerArray addObject:mol];
                [imageList addObject:mol.picture];
            }
        }
//        self.bannerCycleScrollerView.imageURLStringsGroup = imageList;
    } FailureBlock:^(id  _Nonnull error) {
        
    }];
}
#pragma mark--热门咨询
- (void)getHotZX {
    self.hotZXArray = [NSMutableArray array];
    [GMAfnTools PostHttpDataWithUrlStr:KhomeHotZX Dic:nil SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"发现 热门咨询-----%@",responseObject);
        for (NSMutableDictionary *dic in responseObject) {
            HotZXModel *mol = [[HotZXModel alloc]init];
            [mol mj_setKeyValues:dic];
            [self.hotZXArray addObject:mol];
        }
    } FailureBlock:^(id  _Nonnull error) {
        
    }];
}

@end
