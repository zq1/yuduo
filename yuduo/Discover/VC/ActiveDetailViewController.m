//
//  ActiveDetailViewController.m
//  yuduo
//
//  Created by Mac on 2019/9/11.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "ActiveDetailViewController.h"
#import "DisDetailModel.h"
#import "ListTableViewCell.h"
#import "ActiveCenterDetailWebView.h"
#import "HomeRightItemView.h"
@interface ActiveDetailViewController ()<UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource,ActiveCenterDetailWebViewDelegate>
{
    UIView *_bigScr;
    UIView *headView;
    UIImageView *_headImg;
    UILabel *_viperLabel;
    UILabel *_activeTitle;
    UILabel *_activeDetail;
    UILabel *activeTime;
    UILabel *lineLabel;
    UILabel *startTime;
    UILabel *endTime;
    UITableView *_tableView;
    UILabel *_activeIntroduce;
    
    UIView *bgView ;
}
@property(nonatomic,strong)HomeRightItemView * rightItemvView;
PropertyStrong(NSMutableArray , arr);
PropertyStrong(NSMutableArray, cancelCollectString);
@property(nonatomic,strong)ActiveCenterDetailWebView * webView;
@property(nonatomic,copy)NSString  * webCode;
@end
@implementation ActiveDetailViewController
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = YES;
}
-(void)setupRightItem{
    UIBarButtonItem *btn_right = [[UIBarButtonItem alloc] initWithCustomView:self.rightItemvView];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -10;
    
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:negativeSpacer, btn_right, nil];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.webCode = @"";
    self.view.backgroundColor = RGB(245, 245, 245);
    //self.navigationController.navigationBar.hidden = YES;
    self.title = @"活动详情";
    [self setLeftBarItemWithImage:@"back-icon"];
    NSLog(@"打印详情id==%@",self.category_id);
    [self setupRightItem];
  
    _bigScr = [[UIView alloc]init];
//    _bigScr.delegate = self;
//    _bigScr.contentSize = CGSizeMake(KScreenW, KScreenH*2);
    _bigScr.sd_layout.topSpaceToView(self.view, MStatusBarHeight+54)
    .leftSpaceToView(self.view, 0)
    .widthIs(KScreenW).heightIs(400);
    
    headView = [[UIView alloc]init];
//    headView.backgroundColor = GMlightGrayColor;
    [_bigScr addSubview:headView];
    headView.sd_layout.topSpaceToView(_bigScr, 0)
    .leftSpaceToView(_bigScr, 0)
    .widthIs(KScreenW).heightIs(388);
    
    _headImg = [[UIImageView alloc]init];
    [_headImg sd_setImageWithURL:[NSURL URLWithString:@""]];
    _headImg.layer.cornerRadius = 10;
    _headImg.layer.masksToBounds = YES;
    _headImg.backgroundColor = GMWhiteColor;
    [headView addSubview:_headImg];
    _headImg.sd_layout.topSpaceToView(headView, 18)
    .leftSpaceToView(headView, 16)
    .rightSpaceToView(headView, 15)
    .heightIs(194);
    
    _viperLabel = [[UILabel alloc]init];
    _viperLabel.backgroundColor = RGB(228, 102, 67);
    _viperLabel.textColor = [UIColor whiteColor];
     _viperLabel.font = kFont(11);
     _viperLabel.layer.cornerRadius = 5;
     _viperLabel.layer.masksToBounds = YES;
     _viperLabel.text =@"vip免费";
     _viperLabel.textAlignment = NSTextAlignmentCenter;
    [headView addSubview:_viperLabel];
    _viperLabel.sd_layout.topSpaceToView(_headImg, 20)
    .leftSpaceToView(headView, 10)
    .widthIs(55).heightIs(14);
    
    _activeTitle = [[UILabel alloc]init];
    _activeTitle.font = [UIFont fontWithName:KPFType size:15];
    _activeTitle.textColor = RGB(69, 69, 69);
    
//    _activeTitle.backgroundColor = GMBrownColor;
    [headView addSubview:_activeTitle];
    _activeTitle.sd_layout.topSpaceToView(_headImg, 20)
    .leftSpaceToView(headView, 70)
    .widthIs(260).heightIs(14);
    
    _activeDetail = [[UILabel alloc]init];
    _activeDetail.font = [UIFont fontWithName:KPFType size:14];
//    _activeDetail.textColor = RGB(102, 102, 102);
    _activeDetail.text = @"简介减价而间接决胜巅峰老师大家来说都交给你简介减价而间接决胜巅峰老师大家来说都交给你的双方各简介减价而间接决胜巅峰老师大家来说都交给你的双方各的双方各";
//    _activeDetail.backgroundColor = GMRedColor;
    
    
    [headView addSubview:_activeDetail];
    _activeDetail.sd_layout.topSpaceToView(_activeTitle, 20)
    .leftSpaceToView(headView, 16)
    .rightSpaceToView(headView, 15)
    .heightIs(60);
    
    activeTime = [[UILabel alloc]init];
    activeTime.font = kFont(14);
//    activeTime.backgroundColor = [UIColor purpleColor];
    [headView addSubview:activeTime];
    activeTime.sd_layout.topSpaceToView(_activeDetail, 15)
    .leftSpaceToView(headView, 16)
    .widthIs(KScreenW-45).heightIs(14);
    lineLabel = [[UILabel alloc]init];
    lineLabel.backgroundColor = RGB(230, 230, 230);
    //    activeTime.backgroundColor = [UIColor purpleColor];
    [headView addSubview:lineLabel];
    lineLabel.sd_layout.topSpaceToView(_activeDetail, 45)
        .leftSpaceToView(headView, 0)
        .widthIs(KScreenW).heightIs(14);

    _tableView  = [[UITableView alloc]init];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.tableFooterView = self.webView;
    _tableView.tableHeaderView = _bigScr;
    [self.view addSubview:_tableView];
    if (self.online) {
         _tableView.sd_layout.topSpaceToView(self.view, 10)
           .leftSpaceToView(self.view, 0)
           .widthIs(KScreenW).heightIs(KScreenH-kNavigationHeight);
    }else{
        _tableView.sd_layout.topSpaceToView(self.view, 10)
           .leftSpaceToView(self.view, 0)
           .widthIs(KScreenW).heightIs(KScreenH-kNavigationHeight);
    }
   
    self.arr = [NSMutableArray array];
    
    NSUserDefaults *detafaults = [NSUserDefaults standardUserDefaults];
    NSString  *user_id =  [detafaults objectForKey:@"user_id"];
    NSString *token =  [detafaults objectForKey:@"token"];
    NSMutableDictionary *detailParms = [NSMutableDictionary dictionary];
    detailParms[@"user_id"] = user_id;
    detailParms[@"token"] = token;
    detailParms[@"activity_id"] = self.category_id;
    
    [GMAfnTools PostHttpDataWithUrlStr:KDisActivityList Dic:detailParms SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"xxxxx-*---%@",responseObject);
        [self->_headImg sd_setImageWithURL:[NSURL URLWithString:[responseObject objectForKey:@"activity_image"]]];
        self->_activeTitle.text = [responseObject objectForKey:@"activity_name"];
        self->_activeDetail.text = [responseObject objectForKey:@"activity_brief"];
        self->_webCode = [responseObject objectForKey:@"activity_details"];
        
        NSString *timeStr = [self getTimeFromTimesTamp:[responseObject objectForKey:@"activity_end_date"]];
        NSString *timeStr1 = [self getTimeFromTimesTamp:[responseObject objectForKey:@"activity_start_date"]];
        self.rightItemvView.isCollection = responseObject[@"is_collection"];
        NSString *contentStr1 = [@"活动时间 : " stringByAppendingString:timeStr1];
        NSString * time = [NSString stringWithFormat:@"%@-%@",contentStr1,timeStr];
        NSMutableAttributedString *str = [[NSMutableAttributedString alloc]initWithString:time];
        //设置：在0-3个单位长度内的内容显示成红色
         [str addAttribute:NSForegroundColorAttributeName value:RGB(69, 69, 69) range:NSMakeRange(0, 5)];
        self->activeTime.attributedText = str;
        
        DisDetailModel *mol = [[DisDetailModel alloc]init];
        mol.activity_stop_date = [responseObject objectForKey:@"activity_stop_date"];
        mol.activity_type = [responseObject objectForKey:@"activity_type"];
        mol.activity_start_date = [responseObject objectForKey:@"activity_start_date"];
        mol.activity_end_date = [responseObject objectForKey:@"activity_end_date"];
        mol.activity_brief = [responseObject objectForKey:@"activity_class_name"];
        mol.activity_price = [responseObject objectForKey:@"activity_price"];
        mol.activity_address = [responseObject objectForKey:@"activity_address"];
            [self.arr addObject:mol];
        self.webView.webCode = self.webCode;
        [self->_tableView reloadData];
        
    } FailureBlock:^(id  _Nonnull error) {
        
    }];
    
    
//    _activeIntroduce = [[UILabel alloc]init];
//    _activeIntroduce.text = @"活动介绍";
//    _activeIntroduce.font = [UIFont fontWithName:KPFType size:14];
//    _activeIntroduce.textColor = RGB(69, 69, 69);
//    [threeView addSubview:_activeIntroduce];
//    _activeIntroduce.sd_layout.topSpaceToView(threeView, 15)
//    .leftSpaceToView(threeView, 15)
//    .widthIs(60).heightIs(14);
    //=====
    
    
    //成为为朋友购买
    UIButton *friendPay = [UIButton buttonWithType:UIButtonTypeCustom];
    [friendPay setTitleColor:GMWhiteColor forState:UIControlStateNormal];
    friendPay.backgroundColor = RGB(252,171,71);
    friendPay.layer.cornerRadius = 14.5;
    [friendPay setTitle:@"为朋友购买" forState:UIControlStateNormal];
    friendPay.titleLabel.font = kFont(13);
    [friendPay addTarget:self action:@selector(frienPay:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:friendPay];
    friendPay.sd_layout.bottomSpaceToView(self.view, MStatusBarHeight-10)
    .leftSpaceToView(self.view,15*kGMWidthScale)
    .widthIs(165*kGMWidthScale).heightIs(40);
    
    //立刻购买
    UIButton *buyButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [buyButton setTitleColor:GMWhiteColor forState:UIControlStateNormal];
    [buyButton setTitle:@"立刻购买" forState:UIControlStateNormal];
    buyButton.backgroundColor = RGB(228, 102, 67);
    buyButton.layer.cornerRadius = 14.5;
    buyButton.titleLabel.font = kFont(14);
    [buyButton addTarget:self action:@selector(buyClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:buyButton];
    buyButton.sd_layout.bottomSpaceToView(self.view, MStatusBarHeight-10)
    .leftSpaceToView(friendPay,10*kGMWidthScale)
    .widthIs(165*kGMWidthScale).heightIs(40);
}
#pragma mark Getter...
-(HomeRightItemView *)rightItemvView{
    if (!_rightItemvView) {
        _rightItemvView = [[HomeRightItemView alloc] initWithFrame:CGRectMake(0, 0, 60, 30)];
        [_rightItemvView reloadLeftImage:@"矢量智能对象(8)" rightImage:@"fx-dis"];
        WS(weakSelf);
        _rightItemvView.HomeRightItemViewMessageButtonBlock = ^(UIButton * _Nonnull sender) {
            [weakSelf oneClick:sender];
        };
        _rightItemvView.HomeRightItemViewBbsButtonBlock = ^(UIButton * _Nonnull sender) {
            [weakSelf twoClick];
        };
    }
    return _rightItemvView;
}
-(ActiveCenterDetailWebView *)webView{
    if (!_webView) {
        _webView = [[ActiveCenterDetailWebView alloc] initWithFrame:CGRectMake(15, CGRectGetMaxY(_tableView.frame) + 60, KScreenW-30, 500)];
       // _webView.delegate = self;
        [_bigScr addSubview:_webView];
    }
    return _webView;
}
//-(void)changeHeaderViewHeight:(CGFloat)height{
//    self.webView.height = height;
//    [_tableView reloadData];
//}
- (void)frienPay:(UIButton *)send {
    NSLog(@"为朋友购买");
}
- (void)buyBtn:(UIButton *)send {
    NSLog(@"立刻购买");
}
-(void)setOnline:(BOOL)online{
    _online = online;
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.online) {
        return 6;
    }else{
        return 7;
    }
    return 7;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"cellID";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
//
    for (DisDetailModel *dic in self.arr) {
        switch (indexPath.row) {
            case 0:
                {
                    NSLog(@"+++++++++%@",dic.activity_start_date);
                    NSString *startStr = [self getTimeFromTimesTamp:dic.activity_stop_date];
                    NSString *endStr = [@"-" stringByAppendingString:[self getTimeFromTimesTamp:dic.activity_start_date]];
                    NSString *bmsjString = [[@"报名时间 : " stringByAppendingString:startStr] stringByAppendingString:endStr];
                    NSMutableAttributedString *str = [[NSMutableAttributedString alloc]initWithString:bmsjString];
                    //设置：在0-3个单位长度内的内容显示成红色
                     [str addAttribute:NSForegroundColorAttributeName value:GMRedColor range:NSMakeRange(5, bmsjString.length - 5)];
                    cell.textLabel.attributedText = str;
                     cell.textLabel.font = kFont(14);
                    
                }
                break;
            case 1:
            {
                NSString *typeStr =[dic.activity_type integerValue] == 1 ? @"活动类型：线上活动": @"活动类型：线下活动";
                NSMutableAttributedString *str = [[NSMutableAttributedString alloc]initWithString:typeStr];
                                
                                    [str addAttribute:NSForegroundColorAttributeName value:RGB(69, 69, 69) range:NSMakeRange(5, typeStr.length - 5)];
                                   
                cell.textLabel.attributedText = str;
                cell.textLabel.font = kFont(14);
            }
                break;
            case 2:
            {
                 NSString *typeStr = [@"活动分类 : "stringByAppendingString: dic.activity_brief];
                NSMutableAttributedString *str = [[NSMutableAttributedString alloc]initWithString:typeStr];
                                
                                    [str addAttribute:NSForegroundColorAttributeName value:RGB(69, 69, 69) range:NSMakeRange(5, typeStr.length - 5)];
                                   
                cell.textLabel.attributedText = str;
                 cell.textLabel.font = kFont(14);
            }
                break;
            case 3:
            {
                NSString *typeStr = [@"活动费用 : "stringByAppendingString: dic.activity_price ];
                NSMutableAttributedString *str = [[NSMutableAttributedString alloc]initWithString:typeStr];
                                
                                    [str addAttribute:NSForegroundColorAttributeName value:GMBrownColor range:NSMakeRange(5, typeStr.length - 5)];
                                   
                cell.textLabel.attributedText = str;
                 cell.textLabel.font = kFont(14);
            }
                break;
            case 4:
            {
                NSString *typeStr = [@"活动地址 : "stringByAppendingString: dic.activity_address ];
                NSMutableAttributedString *str = [[NSMutableAttributedString alloc]initWithString:typeStr];
                                
                                    [str addAttribute:NSForegroundColorAttributeName value:RGB(69, 69, 69) range:NSMakeRange(5, typeStr.length - 5)];
                                   
                cell.textLabel.attributedText = str;
                 cell.textLabel.font = kFont(14);
            }
                break;
                case 5:
                {
                    
                                       
                    cell.backgroundColor = RGB(245, 245, 245);
                }
                    break;
                case 6:
                {
                    
                                       
                    cell.textLabel.text = @"活动介绍";
                     cell.textLabel.font = kFont(14);
                }
                    break;
            default:
                break;
        }
        
        
    }

   
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.online) {
        if (indexPath.row ==  5) {
            return 10;
        }
    }else{
        if (indexPath.row == 5) {
            return 10;
        }
    }
    return 40;
}
//时间戳：1520915618

- (NSString *)getTimeFromTimesTamp:(NSString *)timeStr

{
    
        double time = [timeStr doubleValue];
    
        NSDate *myDate = [NSDate dateWithTimeIntervalSince1970:time];
    
        NSDateFormatter *formatter = [NSDateFormatter new];
    
        [formatter setDateFormat:@"YYYY-MM-dd"];
    
        //将时间转换为字符串
    
        NSString *timeS = [formatter stringFromDate:myDate];
    
        return timeS;
    
}

//原文链接：https://blog.csdn.net/YFQXXSX/article/details/79540048
//#pragma mark ---- 将时间戳转换成时间
//
//- (NSString *)getTimeFromTimestamp:(NSString *)timex{
//
//    //将对象类型的时间转换为NSDate类型
//
////    double time =1504667976;
//    double time = [timex intValue];
//    NSDate * myDate=[NSDate dateWithTimeIntervalSince1970:time];
//
//    //设置时间格式
//
//    NSDateFormatter * formatter=[[NSDateFormatter alloc]init];
//
////    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
//    [formatter setDateFormat:@"YYYY-MM-dd"];
//    //将时间转换为字符串
//
//    NSString *timeStr=[formatter stringFromDate:myDate];
//
//    return timeStr;
//
//}


- (void)setNavigation {
    
    UIView *bgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenW, MStatusBarHeight+44)];
    bgView.backgroundColor = GMWhiteColor;
    [self.view addSubview:bgView];
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:[UIImage imageNamed:@"back-icon"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButton) forControlEvents:UIControlEventTouchUpInside];
    [bgView addSubview:backButton];
    backButton.sd_layout.topSpaceToView(bgView,MStatusBarHeight+12)
    .leftSpaceToView(bgView, 15)
    .widthIs(11).heightIs(19);
    UIButton *backButtonView = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 80, MStatusBarHeight+44)];
    backButtonView.backgroundColor = RGBA(15, 15, 15, 0);
    [backButtonView addTarget:self action:@selector(backButton) forControlEvents:UIControlEventTouchUpInside];
    [bgView addSubview:backButtonView];

    
    UILabel *titleLabel = [[UILabel alloc]init];
    [bgView addSubview:titleLabel];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"活动详情" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 18],NSForegroundColorAttributeName: [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1.0]}];
    titleLabel.attributedText = string;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.sd_layout.topSpaceToView(bgView, MStatusBarHeight+13)
    .centerXEqualToView(self.view)
    .heightIs(18).widthIs(150);
    
    UIButton *oneButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [oneButton addTarget:self action:@selector(oneClick:) forControlEvents:UIControlEventTouchUpInside];
    [oneButton setBackgroundImage:[UIImage imageNamed:@"矢量智能对象(8)"] forState:UIControlStateNormal];
//    [oneButton setBackgroundImage:[UIImage imageNamed:@"矢量智能对象(14)"] forState:UIControlStateNormal];
    [bgView addSubview:oneButton];
    oneButton.sd_layout.topSpaceToView(bgView, MStatusBarHeight+12)
    .rightSpaceToView(bgView, 48)
    .widthIs(19).heightIs(18);
    
    UIButton *twoButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [twoButton addTarget:self action:@selector(twoClick) forControlEvents:UIControlEventTouchUpInside];
    [twoButton setBackgroundImage:[UIImage imageNamed:@"fx-dis"] forState:UIControlStateNormal];
//    [twoButton setBackgroundImage:[UIImage imageNamed:@"矢量智能对象(14)"] forState:UIControlStateNormal];
    [bgView addSubview:twoButton];
    twoButton.sd_layout.topSpaceToView(bgView, MStatusBarHeight+12)
    .rightSpaceToView(bgView, 15)
    .widthIs(18).heightIs(18);
}
#pragma mark-收藏
- (void)oneClick:(UIButton *)send {
    //    NSUserDefaults *defaultsx =  [NSUserDefaults standardUserDefaults];
    //    if ([[defaultsx objectForKey:@"user_id"] isEqualToString:@""]) {
    //        LoginViewController *login = [[LoginViewController alloc]init];
    //        [self.navigationController presentViewController:login animated:YES completion:nil];
    //    }
    self.cancelCollectString = [NSMutableArray array];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:@"token"] ) {
        
        
        send.selected = !send.selected;
        if (send.selected == YES) {
            //[send setBackgroundImage:[UIImage imageNamed:@"矢量智能对象(14)"] forState:UIControlStateNormal];
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            dic[@"user_id"] = [defaults objectForKey:@"user_id"];
            dic[@"token"] = [defaults objectForKey:@"token"];
            dic[@"type"] = @"4";
            dic[@"collection_id"] = self.category_id;
            
            [GMAfnTools PostHttpDataWithUrlStr:KCollectAdd Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
                NSLog(@"收藏成功++%@",responseObject);
                if ([[responseObject objectForKey:@"code"] isEqualToString:@"1"]) {
                    self.cancelCollectString = [[responseObject objectForKey:@"data"] objectForKey:@"collection_id"];
                    
                    NSUserDefaults *collectStringID = [NSUserDefaults standardUserDefaults];
                    [collectStringID setObject:self.cancelCollectString forKey:@"collection_id"];
                    [self showError:[responseObject objectForKey:@"msg"]];
                } else {
                    NSLog(@"取消成功");
                    //                [self showError:[responseObject objectForKey:@"msg"]];
                }
            } FailureBlock:^(id  _Nonnull error) {
                
            }];
            
            NSLog(@"搜藏");
        }
        if (send.selected == NO) {
            NSLog(@"xxxxxx%@",self.cancelCollectString);
            //[send setBackgroundImage:[UIImage imageNamed:@"sc-xz"] forState:UIControlStateNormal];
            [self test];
            NSLog(@"取消搜藏");
        }
    }else {
        [self showError:@"请先登录"];
    }
}
- (void)test {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"user_id"] = [defaults objectForKey:@"user_id"];
    dic[@"token"] = [defaults objectForKey:@"token"];
    dic[@"type"] = @"1";
    dic[@"collection_id"] = self.category_id;
    NSLog(@"--%@--%@",dic,KCollectCancelxx);
    [GMAfnTools PostHttpDataWithUrlStr:KCollectCancelxx Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
        if ([[responseObject objectForKey:@"code"] isEqualToString:@"1"]) {
            [self showError:[responseObject objectForKey:@"msg"]];
        }
        NSLog(@"%@",responseObject);
        
        
        //            if ([[responseObject objectForKey:@"code"] isEqualToString:@"1"]) {
        //                self.cancelCollectString = [[responseObject objectForKey:@"data"] objectForKey:@"collection_id"];
        //
        //                NSUserDefaults *collectStringID = [NSUserDefaults standardUserDefaults];
        //                [collectStringID setObject:self.cancelCollectString forKey:@"collection_id"];
        //                [self showError:[responseObject objectForKey:@"msg"]];
        //            } else {
        //                [self showError:[responseObject objectForKey:@"msg"]];
        //            }
    } FailureBlock:^(id  _Nonnull error) {
        NSLog(@"取消收藏失败---%@",error);
    }];
}
- (void)twoClick {
    NSLog(@"分享");
}
- (void)backButton {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark --活动购买

- (void)buyClick:(UIButton *)send {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *user_id = [defaults objectForKey:@"user_id"];
    NSString *token = [defaults objectForKey:@"token"];
    NSMutableDictionary *orderDic = [NSMutableDictionary dictionary];
    orderDic[@"user_id"] = user_id;
    orderDic[@"token"] = token;
    orderDic[@"course_types"] = @"4";
    orderDic[@"course_id"] = self.category_id;
    NSLog(@"生成活动订单URL---%@",orderDic);
    __weak typeof(self) weakSelf = self;
    [GMAfnTools PostHttpDataWithUrlStr:KOrderIDURL Dic:orderDic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"生成商品订单---%@",responseObject);
        NSMutableDictionary *dataDic = [responseObject objectForKey:@"data"];
        NSString *order_id = [dataDic objectForKey:@"order_id"];
        [weakSelf user_id:user_id token:token order_id:order_id];
    } FailureBlock:^(id  _Nonnull error) {
        NSLog(@"-------服务器错误-%@",error);
    }];
    
}
#pragma mark--育点支付
- (void)user_id:(NSString *)user_id token:(NSString *)token order_id:(NSString *)order_id {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"user_id"] = user_id;
    dic[@"token"] = token;
    dic[@"order_id"] = order_id;
    NSLog(@"======+%@",dic);
    [GMAfnTools PostHttpDataWithUrlStr:KPointPayURL Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"支付成功------%@",responseObject);
        if ([[responseObject objectForKey:@"code"] isEqualToString:@"1"]) {
            [self showError:[responseObject objectForKey:@"msg"]];
        }else {
            [self showError:[responseObject objectForKey:@"msg"]];
        }
    } FailureBlock:^(id  _Nonnull error) {
        NSLog(@"支付失败======%@",error);
    }];
}
#pragma mark -- 提示框
- (void)showError:(NSString *)errorMsg {
    // 1.弹框提醒
    // 初始化对话框
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:errorMsg preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:nil]];
    // 弹出对话框
    [self presentViewController:alert animated:true completion:nil];
    
    [self performSelector:@selector(dimisssAlert:) withObject:alert afterDelay:1];
    
}

- (void) dimisssAlert:(UIAlertController *)alert
{
    if (alert) {
        [alert dismissViewControllerAnimated:YES completion:nil];
    }
    
}

@end
