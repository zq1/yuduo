//
//  ListTableViewCell.m
//  yuduo
//
//  Created by Mac on 2019/9/11.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "ListTableViewCell.h"

@implementation ListTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier]) {
        
        self.listLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 200, 50)];
        self.listLabel.backgroundColor = GMRedColor;
        self.listLabel.text = @"列表";
        [self.contentView addSubview:self.listLabel];
    }
    return self;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
