//
//  DisCategory_idModel.h
//  yuduo
//
//  Created by Mac on 2019/9/11.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface DisCategory_idModel : BaseModel
PropertyStrong(NSString, title);
PropertyStrong(NSString, category_id);
@end

NS_ASSUME_NONNULL_END
