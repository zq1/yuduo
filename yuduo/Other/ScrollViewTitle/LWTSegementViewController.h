//
//  ViewController.m
//
//  Created by mason on 19/8/9.
//  Copyright © 2019年 . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LWTSegementViewController : UIViewController


@property (nonatomic, strong) NSArray *titleArray;
@property (nonatomic, strong) NSMutableArray *controllerArray;
@property (nonatomic, strong) UIScrollView *scroll;
@property (nonatomic, strong) UIView *sliderV;
@property (nonatomic, strong) UIView *sliderView;

- (void)setTheTextColor:(UIColor *)textColor sliderColor:(UIColor *)sliderColor;
-(void)scrollViewDidScroll:(UIScrollView *)scrollView;
- (void)scrollViewSelectToIndex:(UIButton *)button;
- (void)selectButton:(NSInteger)index;


@end
