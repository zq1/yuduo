//
//  AliUpLoadImageTool.m
//  yuduo
//
//  Created by mason on 2019/9/7.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "AliUpLoadImageTool.h"
#import <Foundation/Foundation.h>
#import <AliyunOSSiOS/OSSService.h>

@interface AliUpLoadImageTool ()
@property (nonatomic,strong) NSMutableArray *dataSourece;
@end

@implementation AliUpLoadImageTool
/*
+ (AliUpLoadImageTool *)shareIncetance {
    static AliUpLoadImageTool *tool_;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        tool_ = [[AliUpLoadImageTool alloc] init];
    });
    return tool_;
}

- (NSMutableArray *)dataSourece {
    if (!_dataSourece) {
        _dataSourece = [[NSMutableArray alloc] init];
    }
    return _dataSourece;
}

+ (void)upLoadImageWithPamgamar:(NSDictionary *)parmar imageData:(NSData *)imageData success:(void (^)(NSString *objectKey))successBlock faile:(void (^)(NSError *error))faile {
    NSString *endpoint = kAliEndPoint;
    
//    id<OSSCredentialProvider> credential = [[OSSPlainTextAKSKPairCredentialProvider alloc] initWithPlainTextAccessKey: kAiAccessKey secretKey:kAliAccessSecret];
    id<OSSCredentialProvider> credential = [[OSSStsTokenCredentialProvider alloc] initWithAccessKeyId:kAiAccessKey secretKeyId:kAliAccessSecret securityToken:nil];
    OSSClientConfiguration * conf = [OSSClientConfiguration new];
    conf.maxRetryCount = 2;
    conf.timeoutIntervalForRequest = 30;
    conf.timeoutIntervalForResource = 24 * 60 * 60;
    OSSClient *client = [[OSSClient alloc] initWithEndpoint:[NSString stringWithFormat:@"http://%@",endpoint] credentialProvider:credential clientConfiguration:conf];
    OSSPutObjectRequest * put = [OSSPutObjectRequest new];
    put.bucketName = KaliBucketName;
    NSDate* dat = [NSDate dateWithTimeIntervalSinceNow:0];
    NSTimeInterval timeStamp =[dat timeIntervalSince1970]*1000;
    NSString *objectKey  = [NSString stringWithFormat:@"%@/%ld-%d.png",@"customer",(long)timeStamp + arc4random()%1000 +arc4random()%1000000,arc4random()%1000000];
    put.objectKey =objectKey;
    put.uploadingData = imageData; // 直接上传NSData
    OSSTask * putTask = [client putObject:put];
    // 上传阿里云
    [putTask continueWithBlock:^id(OSSTask *task) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (!task.error) {
                NSLog(@"url = %@%@",kAliImageURLPrefix,objectKey);
                if (successBlock) {
                    successBlock(objectKey);
                }
            } else {
                if (faile) {
                    faile(task.error);
                }
            }
        });
        return nil;
    }];
}


 这个方法用于上传多张图片到里云上
 @param imageDataArray 二进制图片数组
 @param parmar       这是参数是一个字典，直接将后台的字典传入
 @param successBlock 上传成功后的回调，你可以在这里处理UI
 @param faile        上传失败会走的回调
 
- (void)upLoadImageWithPamgamar:(NSDictionary *)parmar imageDataArray:(NSArray *)imageDataArray success:(void (^)(NSArray *objectKeys))successBlock faile:(void (^)(NSError *error))faile {
    [self.dataSourece removeAllObjects];
    NSString *endpoint = kAliEndPoint;
    id<OSSCredentialProvider> credential = [[OSSPlainTextAKSKPairCredentialProvider alloc] initWithPlainTextAccessKey:kAiAccessKey secretKey:kAliAccessSecret];
//
    
    OSSClientConfiguration * conf = [OSSClientConfiguration new];
    conf.maxRetryCount = 2;
    conf.timeoutIntervalForRequest = 30;
    conf.timeoutIntervalForResource = 24 * 60 * 60;
    conf.maxConcurrentRequestCount = 10;
    OSSClient *client = [[OSSClient alloc] initWithEndpoint:[NSString stringWithFormat:@"http://%@",endpoint] credentialProvider:credential clientConfiguration:conf];
    for (NSInteger i= 0; i<imageDataArray.count ; i ++) {
        [self.dataSourece addObject:@"notValue"];
        [self uploadOneImage:[imageDataArray objectAtIndex:i] oSSClient:client currentIndex:i  success:successBlock faile:faile];
    }
}

- (void)uploadOneImage:(NSData *)imageData oSSClient:(OSSClient *)client currentIndex:(NSInteger)index  success:(void (^)(NSArray *objectKeys))successBlock faile:(void (^)(NSError *))faile {
    OSSPutObjectRequest * put = [OSSPutObjectRequest new];
    put.bucketName = KaliBucketName;
    NSDate* dat = [NSDate dateWithTimeIntervalSinceNow:0];
    NSTimeInterval timeStamp =[dat timeIntervalSince1970]*1000;
    NSString *objectKey  = [NSString stringWithFormat:@"%@/%ld.png",@"customer",(long)timeStamp+10000*index];
    put.objectKey =objectKey;
    put.uploadingData = imageData; // 直接上传NSData
    OSSTask * putTask = [client putObject:put];
    // 上传阿里云
    [putTask continueWithBlock:^id(OSSTask *task) {
        task = [client presignPublicURLWithBucketName:KaliBucketName withObjectKey:objectKey];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (!task.error) {
                [self.dataSourece replaceObjectAtIndex:index withObject:[NSString stringWithFormat:@"%@%@",kAliImageURLPrefix,objectKey]];
                if (![self.dataSourece containsObject:@"notValue"]) {
                    if (successBlock) {
                        successBlock(self.dataSourece);
                    }
                }
            } else {
                [self.dataSourece removeAllObjects];
                if (faile) {
                    faile(task.error);
                }
            }
        });
        return nil;
    }];
}
*/

+(void)upLoadImage:(UIImage*)image success:(void(^)(NSString*_Nonnull url))success failure:(void(^)(NSString*_Nonnull errorString))failure{
    
    //    将image转成Data
    
    NSData *data = UIImagePNGRepresentation(image);
    
    NSString *endpoint = kAliEndPoint;
    
    //AccessKeyId --- secretKeyId 去阿里云控制台查看  securityToken填空字符串
    
    id<OSSCredentialProvider> credential = [[OSSStsTokenCredentialProvider alloc] initWithAccessKeyId:kAiAccessKey secretKeyId:kAliAccessSecret securityToken:@""];
    
    OSSClient*client = [[OSSClient alloc]initWithEndpoint:endpoint credentialProvider:credential];
    
    OSSPutObjectRequest * put = [OSSPutObjectRequest new];
    
    put.bucketName = KaliBucketName;
    
    //根据用户ID和时间戳来生成一段字符串 来保证图片的唯一性
    
    NSUserDefaults *dex = [NSUserDefaults standardUserDefaults];
    NSString *userId = [dex objectForKey:@"user_id"];
    NSString*timestr = [NSString stringWithFormat:@"%@%@",userId,[self getGMTDate]];
    
    // 其中test/是bucket下的子文件夹路径，可以根据路径上传到指定文件夹
    
    put.objectKey= [NSString stringWithFormat:@"consult/%@.png",timestr];
    
    put.uploadingData= data;// 直接上传NSData
    
//    put.uploadProgress= ^(int64_tbytesSent,int64_ttotalByteSent,int64_ttotalBytesExpectedToSend) {
//
//        NSLog(@"%lld, %lld, %lld", bytesSent, totalByteSent, totalBytesExpectedToSend);
//
//    };
//
    put.uploadProgress = ^(int64_t bytesSent,int64_t totalByteSent, int64_t totalBytesExpectedToSend) {
        // NSLog(@"%lld, %lld, %lld", bytesSent, totalByteSent, totalBytesExpectedToSend);
        NSInteger fir = totalByteSent;
        NSInteger all = totalBytesExpectedToSend;
        CGFloat rate = 1.0*fir/all;
        NSLog(@"比例 = %lf.2",rate);
        
    };
    OSSTask* putTask = [client putObject:put];
    
    [putTask continueWithBlock:^id(OSSTask*task) {
        
        //这一步很关键 缺少这一步运行会出错   里面的参数和上面一样
        
        task = [client presignPublicURLWithBucketName:KaliBucketName
                
                                        withObjectKey:[NSString stringWithFormat:@"consult/%@.png",timestr]];
        
        NSLog(@"objectkey ========%@",put.objectKey);
        
        if(!task.error) {
            
            //task.result就是返回的外网访问路径，即图片URL
            
            success(task.result);
            
        }else{
            
            NSLog(@"upload object failed, error: %@" , task.error);
            
            failure(task.error.localizedDescription);
            
        }
        
        return nil;
        
    }];
    
}

+ (NSString*) getGMTDate{
    
    NSDate* dat = [NSDate dateWithTimeIntervalSinceNow:0];
    
    NSTimeInterval a=[dat timeIntervalSince1970];
    
    NSString*timeString = [NSString stringWithFormat:@"%0.f", a];//转为字符型
    
    return timeString;
    
}


@end
