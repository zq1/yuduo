//
//  GMAfnTools.m
//  yuduo
//
//  Created by Mac on 2019/8/19.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "GMAfnTools.h"

@implementation GMAfnTools
+(void)PostHttpDataWithUrlStr:(NSString *)url Dic:(NSMutableDictionary *)dic SuccessBlock:(SuccessBlock)successBlock FailureBlock:(FailedBlock)failureBlock {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    //无条件的信任服务器上的证书
    
    AFSecurityPolicy *securityPolicy =  [AFSecurityPolicy defaultPolicy];
    
    // 客户端是否信任非法证书
    
    securityPolicy.allowInvalidCertificates = YES;
    
    // 是否在证书域字段中验证域名
    
    securityPolicy.validatesDomainName = NO;
    
    manager.securityPolicy = securityPolicy;

    manager.responseSerializer.acceptableContentTypes =[NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",nil];
    [manager POST:url parameters:dic progress:^(NSProgress * _Nonnull uploadProgress) { } success:^(NSURLSessionDataTask * _Nonnull task, id _Nullable responseObject) { if (successBlock) { successBlock(responseObject); } } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) { if (failureBlock) { failureBlock(error); } } ];
}

+(void)GetHttpDataWithUrlStr:(NSString *)url Dic:(NSMutableDictionary *)dic SuccessBlock:(SuccessBlock)successBlock FailureBlock:(FailedBlock)failureBlock{ AFHTTPSessionManager *manager = [AFHTTPSessionManager manager]; manager.responseSerializer.acceptableContentTypes =[NSSet setWithObjects:@"text/html",@"text/plain",nil]; [manager GET:url parameters:dic progress:^(NSProgress * _Nonnull uploadProgress) { } success:^(NSURLSessionDataTask * _Nonnull task, id _Nullable responseObject) { if (successBlock) { successBlock(responseObject); } } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) { if (failureBlock) { failureBlock(error); } } ];
} 
@end
