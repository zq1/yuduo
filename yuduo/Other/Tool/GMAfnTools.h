//
//  GMAfnTools.h
//  yuduo
//
//  Created by Mac on 2019/8/19.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GMAfnTools : NSObject
typedef void (^SuccessBlock) (id responseObject);
typedef void (^FailedBlock) (id error);
+(void)PostHttpDataWithUrlStr:(NSString *)url Dic:(NSMutableDictionary *)dic SuccessBlock:(SuccessBlock)successBlock FailureBlock:(FailedBlock)failureBlock;

+(void)GetHttpDataWithUrlStr:(NSString *)url Dic:(NSMutableDictionary *)dic SuccessBlock:(SuccessBlock)successBlock FailureBlock:(FailedBlock)failureBlock;
@end

NS_ASSUME_NONNULL_END
