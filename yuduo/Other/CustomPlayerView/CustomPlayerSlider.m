//
//  CustomPlayerSlider.m
//  yuduo
//
//  Created by Mac on 2019/10/25.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "CustomPlayerSlider.h"

@interface CustomPlayerSlider ()

@property (nonatomic, strong) UIButton *playButton;
@property (nonatomic, strong) UISlider *slider;
@property (nonatomic, strong) UILabel *nowTimeLabel;
@property (nonatomic, strong) UILabel *totalTimeLabel;

@end

@implementation CustomPlayerSlider

- (instancetype)init {
    if (self = [super init]) {
        self.backgroundColor = [UIColor whiteColor];
        
        [self addSubview:self.playButton];
        [self addSubview:self.nowTimeLabel];
        [self addSubview:self.slider];
        [self addSubview:self.totalTimeLabel];
        
        self.playButton.sd_layout.leftSpaceToView(self, 15)
        .centerYEqualToView(self)
        .widthIs(20).heightIs(20);
        
        self.nowTimeLabel.sd_layout.leftSpaceToView(self.playButton, 15)
        .widthIs(40).heightIs(10)
        .centerYEqualToView(self);
        
        self.totalTimeLabel.sd_layout.rightSpaceToView(self, 10)
        .widthIs(40).heightIs(10)
        .centerYEqualToView(self);
        
        self.slider.sd_layout.leftSpaceToView(self.nowTimeLabel, 10)
        .rightSpaceToView(self.totalTimeLabel, 10)
        .centerYEqualToView(self);
    }
    return self;
}

- (void)setTotalTime:(NSInteger)totalTime {
    _totalTime = totalTime;
    
    NSInteger min = totalTime / 60;
    NSInteger sec = totalTime % 60;
    self.totalTimeLabel.text = [NSString stringWithFormat:@"%02ld:%02ld", (long)min, (long)sec];
    self.slider.maximumValue = totalTime;
}

- (void)setNowTime:(NSInteger)nowTime {
    _nowTime = nowTime;
    
    NSInteger min = nowTime / 60;
    NSInteger sec = nowTime % 60;
    self.nowTimeLabel.text = [NSString stringWithFormat:@"%02ld:%02ld", (long)min, (long)sec];
    self.slider.value = nowTime;
}

- (void)setIsPlay:(BOOL)isPlay {
    _isPlay = isPlay;
    
    if (isPlay) {
        [self.playButton setImage:[UIImage imageNamed:@"暂停"] forState:UIControlStateNormal];
    } else {
        [self.playButton setImage:[UIImage imageNamed:@"播放"] forState:UIControlStateNormal];
    }
}

- (UIButton *)playButton {
    if (!_playButton) {
        _playButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_playButton setImage:[UIImage imageNamed:@"播放"] forState:UIControlStateNormal];
        [_playButton addTarget:self action:@selector(playButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _playButton;
}

- (UILabel *)nowTimeLabel {
    if (!_nowTimeLabel) {
        _nowTimeLabel = [[UILabel alloc] init];
        _nowTimeLabel.textColor = RGB(153, 153, 153);
        _nowTimeLabel.font = [UIFont systemFontOfSize:12];
        _nowTimeLabel.text = @"00:00";
    }
    return _nowTimeLabel;
}

- (UILabel *)totalTimeLabel {
    if (!_totalTimeLabel) {
        _totalTimeLabel = [[UILabel alloc] init];
        _totalTimeLabel.textColor = RGB(153, 153, 153);
        _totalTimeLabel.font = [UIFont systemFontOfSize:12];
        _totalTimeLabel.text = @"00:00";
    }
    return _totalTimeLabel;
}

- (UISlider *)slider {
    if (!_slider) {
        _slider = [[UISlider alloc] init];
        _slider.minimumTrackTintColor = RGB(38, 145, 155);
        _slider.maximumTrackTintColor = RGB(230, 230, 230);
        _slider.minimumValue = 0;
        _slider.maximumValue = 1.0;
        [_slider addTarget:self action:@selector(sliderValueChange:) forControlEvents:UIControlEventValueChanged];
        [_slider setThumbImage:[UIImage imageNamed:@"_s-进度条"] forState:UIControlStateNormal];
    }
    return _slider;
}

- (void)sliderValueChange:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(customPlayerSliderChange:value:)]) {
        [self.delegate customPlayerSliderChange:self value:self.slider.value];
    }
}

- (void)playButtonClick:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(customPlayerPlayButtonClick:)]) {
        [self.delegate customPlayerPlayButtonClick:self];
    }
}

@end
