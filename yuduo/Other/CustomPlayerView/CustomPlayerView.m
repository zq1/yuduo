//
//  CustomPlayerView.m
//  yuduo
//
//  Created by Mac on 2019/10/25.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "CustomPlayerView.h"
#import <AliyunPlayer/AliyunPlayer.h>
#import "CustomPlayerSlider.h"

@interface CustomPlayerView () <AVPDelegate, CustomPlayerSliderDelegate>

@property (nonatomic, strong) UIView *playerView;
@property (nonatomic, strong) CustomPlayerSlider *slider;
@property (nonatomic, strong) UIImageView *thumbnailImageView;

@property (nonatomic, assign) BOOL isPlay;

@end

@implementation CustomPlayerView

+ (instancetype)shareInstance {
    static CustomPlayerView *playerView = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        playerView = [[CustomPlayerView alloc] init];
    });
    return playerView;
}

- (instancetype)init {
    if (self = [super init]) {
        [self addSubview:self.playerView];
        [self addSubview:self.slider];
        [self addSubview:self.thumbnailImageView];
        
        self.playerView.sd_layout.topEqualToView(self)
        .leftEqualToView(self)
        .rightEqualToView(self)
        .bottomSpaceToView(self, 44);
        
        self.slider.sd_layout.bottomEqualToView(self)
        .leftEqualToView(self)
        .rightEqualToView(self)
        .heightIs(44);
        
        self.thumbnailImageView.sd_layout.topEqualToView(self)
        .leftEqualToView(self)
        .rightEqualToView(self)
        .bottomSpaceToView(self, 44);
    }
    return self;
}

- (void)setUrl:(NSString *)url {
    _url = url;
    
    [self.player stop];
    self.slider.nowTime = 0;
    self.isPlay = NO;
    
    AVPUrlSource *source = [[AVPUrlSource alloc] init];
    [source setPlayerUrl:[NSURL URLWithString:url]];
    [self.player setUrlSource:source];
    [self.player prepare];
    self.isPlay = YES;
}

- (void)setThumbnailUrl:(NSString *)thumbnailUrl {
    _thumbnailUrl = thumbnailUrl;
    
    [self.thumbnailImageView sd_setImageWithURL:[NSURL URLWithString:thumbnailUrl]];
}

- (void)customPlayerPlayButtonClick:(CustomPlayerSlider *)slider {
    self.isPlay = !self.isPlay;
}

- (void)stop {
    self.isPlay = NO;
}

- (void)setIsPlay:(BOOL)isPlay {
    _isPlay = isPlay;
    self.slider.isPlay = isPlay;
    if (isPlay) {
        [self.player start];
    } else {
        [self.player pause];
    }
}

- (void)setIsVideo:(BOOL)isVideo {
    _isVideo = isVideo;
    self.thumbnailImageView.hidden = isVideo;
}

- (void)customPlayerSliderChange:(CustomPlayerSlider *)slider value:(NSInteger)value {
    [self.player seekToTime:value * 1000 seekMode:AVP_SEEKMODE_ACCURATE];
    if (self.isPlay) {
        [self.player pause];
        [self.player start];
    }
}

- (void)onPlayerEvent:(AliPlayer*)player eventType:(AVPEventType)eventType {
    if (eventType == AVPEventPrepareDone) {
        AVPMediaInfo *info = [self.player getMediaInfo];
        self.slider.totalTime = [self.player getMediaInfo].duration / 1000;
    }
}

- (void)onCurrentPositionUpdate:(AliPlayer*)player position:(int64_t)position {
    self.slider.nowTime = position / 1000;
}

- (UIView *)playerView {
    if (!_playerView) {
        _playerView = [[UIView alloc] init];
    }
    return _playerView;
}

- (AliPlayer *)player {
    if (!_player) {
        _player = [[AliPlayer alloc] init];
        _player.playerView = self.playerView;
        _player.delegate = self;
    }
    return _player;
}

- (CustomPlayerSlider *)slider {
    if (!_slider) {
        _slider = [[CustomPlayerSlider alloc] init];
        _slider.delegate = self;
    }
    return _slider;
}

- (UIImageView *)thumbnailImageView {
    if (!_thumbnailImageView) {
        _thumbnailImageView = [[UIImageView alloc] init];
    }
    return _thumbnailImageView;
}

- (void)dealloc {
    _player = nil;
}

@end
