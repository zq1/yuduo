//
//  CustomPlayerAudioView.h
//  yuduo
//
//  Created by Mac on 2019/10/28.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AliPlayer;

NS_ASSUME_NONNULL_BEGIN

@interface CustomPlayerAudioView : UIView

@property (nonatomic, strong) AliPlayer *player;

@property (nonatomic, copy) NSString *imageUrl;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subTitle;

+ (instancetype)shareInstance;

- (void)close;

@end

NS_ASSUME_NONNULL_END
