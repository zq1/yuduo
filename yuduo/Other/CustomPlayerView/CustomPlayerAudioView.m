//
//  CustomPlayerAudioView.m
//  yuduo
//
//  Created by Mac on 2019/10/28.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "CustomPlayerAudioView.h"
#import <AliyunPlayer/AliyunPlayer.h>

@interface CustomPlayerAudioView ()

@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *subTitleLabel;
@property (nonatomic, strong) UIProgressView *progressView;
@property (nonatomic, strong) UIButton *playButton;
@property (nonatomic, strong) UIButton *closeButton;

@property (nonatomic, assign) BOOL isPlay;

@end

@implementation CustomPlayerAudioView

+ (instancetype)shareInstance {
    static CustomPlayerAudioView *playerView = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        playerView = [[CustomPlayerAudioView alloc] init];
    });
    return playerView;
}

- (instancetype)init {
    if (self = [super init]) {
        self.backgroundColor = RGB(237, 237, 237);
        self.layer.cornerRadius = 10;
        
        [self addSubview:self.imageView];
        [self addSubview:self.titleLabel];
        [self addSubview:self.subTitleLabel];
        [self addSubview:self.progressView];
        [self addSubview:self.playButton];
        [self addSubview:self.closeButton];
        
        self.imageView.sd_layout.topSpaceToView(self, 11)
        .leftSpaceToView(self, 11)
        .widthIs(40).heightIs(40);
        
        self.titleLabel.sd_layout.leftSpaceToView(self.imageView, 10)
        .topSpaceToView(self, 10)
        .rightSpaceToView(self, 90);
        
        self.subTitleLabel.sd_layout.leftSpaceToView(self.imageView, 10)
        .topSpaceToView(self.titleLabel, 5);
        
        self.progressView.sd_layout.leftSpaceToView(self, 15)
        .bottomSpaceToView(self, 10)
        .widthIs(210).heightIs(5);
        
        self.playButton.sd_layout.centerYEqualToView(self)
        .rightSpaceToView(self, 40)
        .widthIs(40).heightIs(40);
        
        self.closeButton.sd_layout.centerYEqualToView(self)
        .rightSpaceToView(self, 10)
        .widthIs(20).heightIs(20);
    }
    return self;
}

- (void)stop {
    [self.player stop];
}

- (void)onCurrentPositionUpdate:(AliPlayer*)player position:(int64_t)position {
    CGFloat progress = (CGFloat)position / (CGFloat)[player getMediaInfo].duration;
    self.progressView.progress = progress;
}

- (void)setImageUrl:(NSString *)imageUrl {
    _imageUrl = imageUrl;
    
    [self.imageView sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
}

- (void)setTitle:(NSString *)title {
    _title = title;
    
    self.titleLabel.text = title;
}

- (void)setIsPlay:(BOOL)isPlay {
    _isPlay = isPlay;
    if (isPlay) {
        [self.playButton setImage:[UIImage imageNamed:@"zantingtingzhi-2"] forState:UIControlStateNormal];
        [self.player start];
    } else {
        [self.playButton setImage:[UIImage imageNamed:@"play-round"] forState:UIControlStateNormal];
        [self.player pause];
    }
}

- (UIImageView *)imageView {
    if (!_imageView) {
        _imageView = [[UIImageView alloc] init];
        _imageView.sd_cornerRadius = @(20);
    }
    return _imageView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = [UIFont systemFontOfSize:14];
        _titleLabel.textColor = RGB(69, 69, 69);
    }
    return _titleLabel;
}

- (UILabel *)subTitleLabel {
    if (!_subTitleLabel) {
        _subTitleLabel = [[UILabel alloc] init];
        _subTitleLabel.font = [UIFont systemFontOfSize:12];
        _subTitleLabel.textColor = RGB(128, 128, 128);
    }
    return _subTitleLabel;
}

- (UIProgressView *)progressView {
    if (!_progressView) {
        _progressView = [[UIProgressView alloc] init];
        _progressView.progressTintColor = RGB(38, 145, 155);
        _progressView.trackTintColor = [UIColor whiteColor];
    }
    return _progressView;
}

- (void)setPlayer:(AliPlayer *)player {
    _player = player;
    _player.delegate = self;
    self.isPlay = YES;
}

- (UIButton *)playButton {
    if (!_playButton) {
        _playButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_playButton addTarget:self action:@selector(playButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _playButton;
}

- (void)playButtonClick:(id)sender {
    self.isPlay = !self.isPlay;
}

- (UIButton *)closeButton {
    if (!_closeButton) {
        _closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_closeButton setImage:[UIImage imageNamed:@"guanbi-3"] forState:UIControlStateNormal];
        [_closeButton addTarget:self action:@selector(closeButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _closeButton;
}

- (void)closeButtonClick:(id)sender {
    [self close];
}

- (void)close {
    [self.player stop];
    _player = nil;
    [self removeFromSuperview];
}

@end
