//
//  CustomPlayerView.h
//  yuduo
//
//  Created by Mac on 2019/10/25.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AliPlayer;

NS_ASSUME_NONNULL_BEGIN

@interface CustomPlayerView : UIView

@property (nonatomic, copy) NSString *url;
@property (nonatomic, copy) NSString *thumbnailUrl;
@property (nonatomic, strong) AliPlayer *player;
@property (nonatomic, assign) BOOL isVideo;

+ (instancetype)shareInstance;

- (void)playBackgroundMode;
- (void)destroy;
- (void)stop;

@end

NS_ASSUME_NONNULL_END
