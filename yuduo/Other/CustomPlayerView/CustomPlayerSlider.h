//
//  CustomPlayerSlider.h
//  yuduo
//
//  Created by Mac on 2019/10/25.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CustomPlayerSlider;

NS_ASSUME_NONNULL_BEGIN

@protocol CustomPlayerSliderDelegate <NSObject>

- (void)customPlayerPlayButtonClick:(CustomPlayerSlider *)slider;

- (void)customPlayerSliderChange:(CustomPlayerSlider *)slider value:(NSInteger)value;

@end

@interface CustomPlayerSlider : UIView

@property (nonatomic, weak) id<CustomPlayerSliderDelegate> delegate;

@property (nonatomic, assign) NSInteger totalTime;
@property (nonatomic, assign) NSInteger nowTime;

@property (nonatomic, assign) BOOL isPlay;

@end

NS_ASSUME_NONNULL_END
