//
//  ListCouViewController.h
//  yuduo
//
//  Created by Mac on 2019/8/12.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ListCouViewController : UIViewController
@property (nonatomic, assign) NSInteger index;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSString *gmxtest;

@property (nonatomic, assign) BOOL isAll;
@property (nonatomic, copy) NSString *categoryID;

@property (nonatomic, copy) void(^stringIDBlock)(NSString *stringID);
@end

NS_ASSUME_NONNULL_END
