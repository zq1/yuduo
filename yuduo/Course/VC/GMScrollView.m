//
//  GMScrollView.m
//  yuduo
//
//  Created by Mac on 2019/8/16.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "GMScrollView.h"

@implementation GMScrollView
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    if (gestureRecognizer.state != 0) {
        return YES;
    } else {
        return NO;
    }
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    UIView *view = [super hitTest:point withEvent:event];
    if ([view.superview isMemberOfClass:NSClassFromString(@"xxxxx")]){
        self.scrollEnabled = NO;
    }else{
        self.scrollEnabled = YES;
    }
    return view;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
