//
//  GMScrollView.h
//  yuduo
//
//  Created by Mac on 2019/8/16.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GMScrollView : UIScrollView<UIGestureRecognizerDelegate>

@end

NS_ASSUME_NONNULL_END
