//
//  CoureSoundViewController.m
//  yuduo
//
//  Created by Mac on 2019/8/19.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "CoureSoundViewController.h"
#import "SoundViewController.h"
#import "VideoViewController.h"
#import "PicViewController.h"

#import "TapSliderScrollView.h"
#import "SimpleTapSliderScrollView.h"

#import "CourseVideoModel.h"
#import "CourseVideoOtherModel.h"
#import "AppDelegate.h"
#import "CustomPlayerAudioView.h"
#import "CustomPlayerView.h"

@interface CoureSoundViewController ()<SliderLineViewDelegate,SimpleSliderLineViewDelegate,UITableViewDelegate,UITableViewDataSource, UIScrollViewDelegate,UIGestureRecognizerDelegate, VideoViewControllerDelegate>
PropertyStrong(UIButton, collectButton);
PropertyStrong(UIButton, shareButton);
PropertyStrong(SimpleTapSliderScrollView, simpleview);
PropertyStrong(VideoViewController, vc1);
PropertyStrong(VideoViewController, vc2);
PropertyStrong(PicViewController, vc3);
PropertyStrong(NSMutableArray, cancelCollectString);
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, assign) NSInteger nowType;
@end

@implementation CoureSoundViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[CustomPlayerAudioView shareInstance] close];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if (self.nowType == 2) {
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        appDelegate.audioPlayerView = [CustomPlayerAudioView shareInstance];
        [CustomPlayerAudioView shareInstance].player = self.vc1.player.player;
        [self.tabBarController.view addSubview:[CustomPlayerAudioView shareInstance]];
        [CustomPlayerAudioView shareInstance].frame = CGRectMake(5, KScreenH - 80 - 70, KScreenW - 10, 70);
        [CustomPlayerAudioView shareInstance].imageUrl = self.vc1.model.audio_picture;
        [CustomPlayerAudioView shareInstance].title = self.vc1.model.course_hour_name;
        if (self.navigationController.viewControllers.count > 1) {
            [CustomPlayerAudioView shareInstance].hidden = YES;
        }
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"打印video----------%@",self.videoArray);
    self.view.backgroundColor = RGB(244, 243, 244);
    [self setNavigation];
    [self scrollViewSound];
    [self getData:self.courseHourID type:self.type];
    // Do any additional setup after loading the view.
}

- (void)scrollViewSound {
    self.vc1 = [[VideoViewController alloc]init];
    self.vc1.delegate = self;
    self.vc1.priceString = self.priceString;
    self.vc2 = [[VideoViewController alloc]init];
    self.vc2.delegate = self;
    self.vc2.priceString = self.priceString;
    self.vc3 = [[PicViewController alloc]init];
    self.simpleview = [[SimpleTapSliderScrollView alloc]initWithFrame:CGRectMake(0, MStatusBarHeight+44, KScreenW, KScreenH-MStatusBarHeight)];
    self.simpleview.delegate = self;
    self.simpleview.sliderViewColor = RGB(21, 152, 164);
    self.simpleview.titileColror = RGB(153, 153, 153);
    self.simpleview.selectedColor = RGB(21, 152, 164);
    [self.simpleview createView:@[@"音频",@"视频",@"图文"] andViewArr:@[self.vc1,self.vc2,self.vc3] andRootVc:self];
    [self.view addSubview:self.simpleview];
    [self.simpleview sliderToViewIndex:0];
}

- (void)getData:(NSString *)courseHourID type:(NSInteger)type {
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD show];
    self.nowType = type;
    if (type == 1) {
        [self.simpleview sliderToViewIndex:1];
        [self getSoundList];
        [self getVideoInfo:courseHourID];
    } else if (type == 2) {
        [self.simpleview sliderToViewIndex:0];
        [self getVideoList];
        [self getSoundInfo:courseHourID];
    }
}

- (void)getSoundList {
    NSDictionary *param = @{
        @"course_id" : self.courseID
    };
    NSString *url = self.source == 1 ? KCourseAudioList : KGementAudioList;
    [GMAfnTools PostHttpDataWithUrlStr:url Dic:param.mutableCopy SuccessBlock:^(id  _Nonnull responseObject) {
        NSArray *list = [CourseVideoOtherModel mj_objectArrayWithKeyValuesArray:responseObject];
        self.vc1.list = list;
    } FailureBlock:^(id  _Nonnull error) {
        
    }];
}

- (void)getSoundInfo:(NSString *)courseHourID {
    NSDictionary *param = @{
        @"course_id" : self.courseID,
        @"course_hour_id" : courseHourID
    };
    NSString *url = self.source == 1 ? KCourseAudioDetail : KGementAudioDetail;
    [GMAfnTools PostHttpDataWithUrlStr:url Dic:param.mutableCopy SuccessBlock:^(id  _Nonnull responseObject) {
        CourseVideoModel *model = [CourseVideoModel mj_objectWithKeyValues:responseObject];
        self.titleLabel.text = model.course_hour_name;
        self.vc1.model = model;
        self.vc3.model = model.tuwen;
        [SVProgressHUD dismissWithDelay:1];
    } FailureBlock:^(id  _Nonnull error) {
        
    }];
}

- (void)getVideoList {
    NSDictionary *param = @{
        @"course_id" : self.courseID
    };
    NSString *url = self.source == 1 ? KCourseVideoList : KGementVideoList;
    [GMAfnTools PostHttpDataWithUrlStr:url Dic:param.mutableCopy SuccessBlock:^(id  _Nonnull responseObject) {
        NSArray *list = [CourseVideoOtherModel mj_objectArrayWithKeyValuesArray:responseObject];
        self.vc2.list = list;
    } FailureBlock:^(id  _Nonnull error) {
        
    }];
}

- (void)getVideoInfo:(NSString *)courseHourID {
    NSDictionary *param = @{
        @"course_id" : self.courseID,
        @"course_hour_id" : courseHourID
    };
    NSString *url = self.source == 1 ? KCourseVideoDetail : KGementVideoDetail;
    [GMAfnTools PostHttpDataWithUrlStr:url Dic:param.mutableCopy SuccessBlock:^(id  _Nonnull responseObject) {
        CourseVideoModel *model = [CourseVideoModel mj_objectWithKeyValues:responseObject];
        self.titleLabel.text = model.course_hour_name;
        self.vc2.model = model;
        self.vc3.model = model.tuwen;
        [SVProgressHUD dismissWithDelay:1];
    } FailureBlock:^(id  _Nonnull error) {
        
    }];
}

-(void)setNavigation {
    UIView *bgView = [[UIView alloc]init];
    bgView.backgroundColor = GMWhiteColor;
    [self.view addSubview:bgView];
    bgView.sd_layout.topSpaceToView(self.view, 0)
    .leftSpaceToView(self.view, 0)
    .widthIs(KScreenW).heightIs(MStatusBarHeight+44);
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:[UIImage imageNamed:@"back-icon"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButton) forControlEvents:UIControlEventTouchUpInside];
    [bgView addSubview:backButton];
    backButton.sd_layout.topSpaceToView(bgView,MStatusBarHeight+12)
    .leftSpaceToView(bgView, 15)
    .widthIs(11).heightIs(19);
    UIButton *backButtonView = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 80, MStatusBarHeight+44)];
    backButtonView.backgroundColor = RGBA(15, 15, 15, 0);
    [backButtonView addTarget:self action:@selector(backButton) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButtonView];
    self.titleLabel = [[UILabel alloc]init];
    self.titleLabel.text = @"你的家庭谁是营养守门员";
    [bgView addSubview:self.titleLabel];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:self.titleLabel.text attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 18],NSForegroundColorAttributeName: [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1.0]}];
    self.titleLabel.attributedText = string;
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.sd_layout.topSpaceToView(bgView, MStatusBarHeight+13)
    .leftSpaceToView(bgView, 94)
    .heightIs(18).widthIs(200);
    
    self.collectButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.collectButton setBackgroundImage:[UIImage imageNamed:@"sc-xz"] forState:UIControlStateNormal];
    [self.collectButton addTarget:self action:@selector(collect:) forControlEvents:UIControlEventTouchUpInside];
    [bgView addSubview:self.collectButton];
    self.collectButton.sd_layout.topEqualToView(backButton)
    .rightSpaceToView(bgView, 53)
    .widthIs(18).heightIs(18);
    
    self.shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.shareButton setBackgroundImage:[UIImage imageNamed:@"fx-icon(1)"] forState:UIControlStateNormal];
    [self.shareButton addTarget:self action:@selector(share) forControlEvents:UIControlEventTouchUpInside];
    [bgView addSubview:self.shareButton];
    self.shareButton.sd_layout.topEqualToView(backButton)
    .rightSpaceToView(bgView, 15)
    .widthIs(18).heightIs(18);
}
- (void)playOther:(CourseVideoOtherModel *)model {
    [self getData:model.course_hour_id type:model.course_hour_type.integerValue];
}
#pragma mark-收藏
- (void)collect:(UIButton *)send {
    NSLog(@"收藏");
        self.cancelCollectString = [NSMutableArray array];
         NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        if ([defaults objectForKey:@"token"] ) {
            
        
        send.selected = !send.selected;
        if (send.selected == YES) {
            [send setBackgroundImage:[UIImage imageNamed:@"矢量智能对象(14)"] forState:UIControlStateNormal];
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            dic[@"user_id"] = [defaults objectForKey:@"user_id"];
            dic[@"token"] = [defaults objectForKey:@"token"];
            dic[@"type"] = @"1";
            dic[@"collection_id"] = self.courseID;
            
            [GMAfnTools PostHttpDataWithUrlStr:KCollectAdd Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
                NSLog(@"收藏成功++%@",responseObject);
                if ([[responseObject objectForKey:@"code"] isEqualToString:@"1"]) {
                
                    self.cancelCollectString = [[responseObject objectForKey:@"data"] objectForKey:@"collection_id"];
                    
                    NSUserDefaults *collectStringID = [NSUserDefaults standardUserDefaults];
                    [collectStringID setObject:self.cancelCollectString forKey:@"collection_id"];
                     [self showError:[responseObject objectForKey:@"msg"]];
                } else {
                    NSLog(@"取消成功");
    //                [self showError:[responseObject objectForKey:@"msg"]];
                }
            } FailureBlock:^(id  _Nonnull error) {
                
            }];
           
            NSLog(@"搜藏");
        }
        if (send.selected == NO) {
            NSLog(@"xxxxxx%@",self.cancelCollectString);
            [send setBackgroundImage:[UIImage imageNamed:@"sc-xz"] forState:UIControlStateNormal];
            [self test];
            NSLog(@"取消搜藏");
        }
        }else {
            [self showError:@"请先登录"];
        }
}

- (void)test {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"user_id"] = [defaults objectForKey:@"user_id"];
    dic[@"token"] = [defaults objectForKey:@"token"];
    dic[@"type"] = @"1";
    dic[@"collection_id"] = self.courseID;
    NSLog(@"--%@--%@",dic,KCollectCancelxx);
    [GMAfnTools PostHttpDataWithUrlStr:KCollectCancelxx Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
        if ([[responseObject objectForKey:@"code"] isEqualToString:@"1"]) {
            [self showError:[responseObject objectForKey:@"msg"]];
        }
        NSLog(@"%@",responseObject);
        
        
        //            if ([[responseObject objectForKey:@"code"] isEqualToString:@"1"]) {
        //                self.cancelCollectString = [[responseObject objectForKey:@"data"] objectForKey:@"collection_id"];
        //
        //                NSUserDefaults *collectStringID = [NSUserDefaults standardUserDefaults];
        //                [collectStringID setObject:self.cancelCollectString forKey:@"collection_id"];
        //                [self showError:[responseObject objectForKey:@"msg"]];
        //            } else {
        //                [self showError:[responseObject objectForKey:@"msg"]];
        //            }
    } FailureBlock:^(id  _Nonnull error) {
        NSLog(@"取消收藏失败---%@",error);
    }];
}

- (void)showError:(NSString *)errorMsg {
    // 1.弹框提醒
    // 初始化对话框
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:errorMsg preferredStyle:UIAlertControllerStyleAlert];
//    [alert addAction:[UIAlertAction actionWithTitle:@"" style:UIAlertActionStyleDefault handler:nil]];
    // 弹出对话框
    [self presentViewController:alert animated:true completion:nil];
    
    [self performSelector:@selector(dimisssAlert:) withObject:alert afterDelay:1];
    
}

- (void) dimisssAlert:(UIAlertController *)alert
{
    if (alert) {
        [alert dismissViewControllerAnimated:YES completion:nil];
    }
    
}
#pragma mark-分享
- (void)share {
    NSLog(@"分享");
}
- (void)backButton {
    [self.navigationController popViewControllerAnimated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
