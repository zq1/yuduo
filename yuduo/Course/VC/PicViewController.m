//
//  PicViewController.m
//  yuduo
//
//  Created by Mac on 2019/8/19.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "PicViewController.h"
#import "KCNRTableViewCell.h"
#import "CourseVideoTuwenModel.h"
@interface PicViewController ()
<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *coureNR;

@end

@implementation PicViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = RGB(244, 243, 244);
    UIView *videoView = [[UIView alloc]init];
    videoView.backgroundColor = GMlightGrayColor;
    [self.view addSubview:videoView];
    videoView.sd_layout.topSpaceToView(self.view, 88+MStatusBarHeight)
    .widthIs(KScreenW)
    .heightIs(324*kGMHeightScale);
    self.coureNR = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.coureNR.delegate = self;
    self.coureNR.dataSource = self;
    self.coureNR.tableFooterView = [UIView new];
    self.coureNR.separatorStyle = UITableViewCellSeparatorStyleNone;
//    self.coureNR.tableHeaderView = videoView;
    [self.view addSubview:self.coureNR];
    self.coureNR.sd_layout.topSpaceToView(self.view, 10)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .widthIs(KScreenW).heightIs(KScreenH-MStatusBarHeight-44-44-10-48);
    [self.coureNR reloadData];
    
    UIView *payView = [[UIView alloc]init];
    payView.backgroundColor = GMWhiteColor;
    [self.view addSubview:payView];
    payView.sd_layout.bottomSpaceToView(self.view,MStatusBarHeight+28)
    .leftSpaceToView(self.view, 0)
    .widthIs(KScreenW).heightIs(48);
    
    UIButton *vipLogo = [UIButton buttonWithType:UIButtonTypeCustom];
    [vipLogo setBackgroundImage:[UIImage imageNamed:@"huiyuan"] forState:UIControlStateNormal];
    [payView addSubview:vipLogo];
    vipLogo.sd_layout.topSpaceToView(payView, 10)
    .leftSpaceToView(payView, 33)
    .widthIs(16).heightIs(14);
    
    //成为会员
    UIButton *viperButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [viperButton setTitleColor:RGB(21, 152, 164) forState:UIControlStateNormal];
    [viperButton setTitle:@"成为会员" forState:UIControlStateNormal];
    
    viperButton.titleLabel.font = kFont(14);
    [payView addSubview:viperButton];
    viperButton.sd_layout.bottomSpaceToView(payView, 9)
    .leftSpaceToView(payView, 10)
    .widthIs(58).heightIs(11);
    // Do any additional setup after loading the view.
    
    UILabel *priceLabel = [[UILabel alloc]init];
    priceLabel.text = @"¥196.00";
    [payView addSubview:priceLabel];
    priceLabel.sd_layout.topSpaceToView(payView, 19)
    .leftSpaceToView(viperButton, 20*kGMWidthScale)
    .widthIs(60).heightIs(13);
    NSMutableAttributedString *pricelString = [[NSMutableAttributedString alloc] initWithString:priceLabel.text attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFang SC" size: 16],NSForegroundColorAttributeName: [UIColor colorWithRed:228/255.0 green:102/255.0 blue:67/255.0 alpha:1.0]}];
    priceLabel.attributedText = pricelString;
    
    //成为为朋友购买
    UIButton *friendPay = [UIButton buttonWithType:UIButtonTypeCustom];
    [friendPay setTitleColor:GMWhiteColor forState:UIControlStateNormal];
    friendPay.backgroundColor = RGB(252,171,71);
    friendPay.layer.cornerRadius = 14.5;
    [friendPay setTitle:@"为朋友购买" forState:UIControlStateNormal];
    friendPay.titleLabel.font = kFont(13);
    [payView addSubview:friendPay];
    friendPay.sd_layout.topSpaceToView(payView, 10)
    .leftSpaceToView(priceLabel,17*kGMWidthScale)
    .widthIs(90*kGMWidthScale).heightIs(29);
    
    //立刻购买
    UIButton *buyButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [buyButton setTitleColor:GMWhiteColor forState:UIControlStateNormal];
    [buyButton setTitle:@"立刻购买" forState:UIControlStateNormal];
    buyButton.backgroundColor = RGB(228, 102, 67);
    buyButton.layer.cornerRadius = 14.5;
    buyButton.titleLabel.font = kFont(14);
    [payView addSubview:buyButton];
    buyButton.sd_layout.topSpaceToView(payView, 10)
    .leftSpaceToView(friendPay,10*kGMWidthScale)
    .widthIs(90*kGMWidthScale).heightIs(29);
    
}

- (void)setModel:(CourseVideoTuwenModel *)model {
    _model = model;
    [self.coureNR reloadData];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSLog(@"----%ld",indexPath.row);
    
}

#pragma mark--返回行高
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return 60;
    }
    UILabel *label = [[UILabel alloc] init];
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithData:[self.model.details dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes: nil error:nil];
    label.attributedText = text;
    label.numberOfLines = 0;
    [label sizeToFit];
    return label.mj_h + 10;
    return UITableViewAutomaticDimension;
}
#pragma mark--返回行数
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    static NSString *key0=@"cell0";
//    KCNRTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:key0];
//    if (!cell) {
//        cell = [[KCNRTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:key0];
//
//    }
//    //    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    UITableViewCell *cell;
    if (indexPath.row == 0) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell0"];
        cell.textLabel.text = self.model.title;
        cell.textLabel.textColor = RGB(69, 69, 69);
        cell.textLabel.font = kFont(18);
    } else {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell0"];
        UILabel *label = [[UILabel alloc] init];
        [cell.contentView addSubview:label];
        
        NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithData:[self.model.details dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes: nil error:nil];
        label.attributedText = text;
        label.numberOfLines = 0;
        [label sizeToFit];
        
        label.sd_layout.leftSpaceToView(cell.contentView, 15)
        .rightSpaceToView(cell.contentView, 15)
        .topEqualToView(cell.contentView)
        .bottomEqualToView(cell.contentView);
        
        [label sizeToFit];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
