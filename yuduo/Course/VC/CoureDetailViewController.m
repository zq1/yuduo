//
//  CoureDetailViewController.m
//  yuduo
//
//  Created by Mac on 2019/8/6.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "CoureDetailViewController.h"
//课程介绍
#import "JSViewController.h"
#import "NRViewController.h"
#import "PJViewController.h"

#import "TapSliderScrollView.h"
#import "SimpleTapSliderScrollView.h"
#import "GMScrollView.h"
#import "CourseDetailCollectModel.h"
#import "DKSTextView.h"
#import "DKSKeyboardView.h"
#import "LoginViewController.h"
#import "CourseModel.h"

typedef void (^GetGps) (NSString* string);

@interface CoureDetailViewController ()<SliderLineViewDelegate,SimpleSliderLineViewDelegate,UITableViewDelegate,UITableViewDataSource, UIScrollViewDelegate,UIGestureRecognizerDelegate,DKSKeyboardDelegate>
{
    JSViewController *vc1 ;
    NRViewController *vc2 ;
    PJViewController *vc3 ;
}

@property (nonatomic ,assign) BOOL selectSort;

@property (nonatomic, strong) NSArray *dataArr;

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) NSMutableArray *myChildViewControllers;
@property (nonatomic, strong) NSMutableArray *kcNrArray;
@property (nonatomic, assign) BOOL status;
PropertyStrong(UIScrollView , bgScrollView);//背景
PropertyStrong(UIImageView, bigHeadView);//顶部图片
PropertyStrong(UIButton , backButton);//返回按钮
PropertyStrong(UIButton , collectButton);//收藏按钮
PropertyStrong(UIButton, shareButton); //分享按钮
PropertyStrong(UIView, titleDetailView); //课程详情
PropertyStrong(UILabel , titleLable);//标题
PropertyStrong(UILabel, detailLable);
PropertyStrong(UILabel, coureLableOne);
PropertyStrong(UILabel, coureLaleTwo);//课程标签2
PropertyStrong(UILabel, mianfeiLabel);
PropertyStrong(UILabel, priceLable);//价格
PropertyStrong(UIImageView, lookImg);//观看图标
PropertyStrong(UILabel, peoplesLable);//观看人数

//顶部按钮View
PropertyStrong(UIView, buttonView);

PropertyStrong(NSMutableArray, cancelCollectString);
PropertyStrong(  CourseDetailCollectModel ,mol);

PropertyStrong(UITableView, pjTableView);
//输入框
PropertyStrong(DKSKeyboardView, keyView);

@property (nonatomic , strong)SimpleTapSliderScrollView *simpleview;

PropertyStrong(NSString, buyPriceString);
PropertyStrong(UILabel, priceLabel);

@property (nonatomic, copy) NSDictionary *coureDetailInfo;
@property (nonatomic, strong) JSViewController *vc1;

@end
@implementation CoureDetailViewController
-(void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = RGB(244,243 , 244);
//    NSLog(@"=======%@",self.courseDetailIDString);
//    NSMutableDictionary *detailDic = [NSMutableDictionary dictionaryWithObject:self.courseDetailIDString forKey:@"course_id"];
//    [GMAfnTools PostHttpDataWithUrlStr:KCourseDetail Dic:detailDic SuccessBlock:^(id  _Nonnull responseObject) {
//        NSLog(@"---------%@",responseObject);
//    } FailureBlock:^(id  _Nonnull error) {
//        
//    }];
//    self.navigationController.navigationBar.hidden = YES;
//    self.navigationController.navigationBarHidden = YES;
    
    [self layoutSubViews];
    //课程介绍课程内容课程评价
    [self scrollZX];
    //获取数据
    [self getDetailData];
    
    
    
    
    // Do any additional setup after loading the view.
}
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
 
        return YES;
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGPoint point = scrollView.contentOffset;
//    self.buttonView.backgroundColor = GMBlueColor;
//    NSLog(@"xxxxxxxxxx%f,",point.y);
    if (MStatusBarHeight > 25) {
        self.buttonView.frame = CGRectMake(0, 150, 60, 80);
    }
    if (MStatusBarHeight == 20) {
        
        self.buttonView.frame = CGRectMake(0, point.y+44, KScreenW, 24);
    }
 
    
//    self.simpleview.frame = CGRectMake(0, point.y+100, KScreenW, 100);
}
//-(void)scrollViewDidEndDecelerating:(UIScrollView* )scrollView
//{
//    CGPoint  point = scrollView.contentOffset;
//    self.buttonView.frame = CGRectMake(0, point.y+44, KScreenW, 24);
//
//}
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
}
- (void)layoutSubViews {
    //滑动背景
    self.bgScrollView = [[UIScrollView alloc]initWithFrame:[UIScreen mainScreen].bounds];
    self.bgScrollView.mj_y -= MStatusBarHeight;
    self.bgScrollView.mj_h -= MStatusBarHeight+10;
    self.bgScrollView.contentSize = CGSizeMake(KScreenW, KScreenH+200);
    self.bgScrollView.contentOffset = CGPointMake(0, 0);
    self.bgScrollView.delegate = self;
//    self.bgScrollView.delaysContentTouches = NO;
//    self.scrollView.canCancelContentTouches = NO;
    self.bgScrollView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self getDetailData];
    }];
    [self.view addSubview:self.bgScrollView];
    
   
    //img背景
    self.bigHeadView = [[UIImageView alloc]init];
    self.bigHeadView.backgroundColor = GMWhiteColor;
    self.bigHeadView.userInteractionEnabled = YES;
    self.bigHeadView.contentMode = UIViewContentModeScaleAspectFit;
    [self.bgScrollView addSubview:self.bigHeadView];
    self.bigHeadView.sd_layout.topSpaceToView(self.bgScrollView, 0)
    .leftSpaceToView(self.bgScrollView,0)
    .rightEqualToView(self.bgScrollView)
    .heightIs(280*kGMHeightScale);
    
    
    self.buttonView = [[UIView alloc]init];
//    self.buttonView.backgroundColor = GMWhiteColor;
    [self.bigHeadView addSubview:self.buttonView];
    self.buttonView.sd_layout.topSpaceToView(self.bigHeadView, MStatusBarHeight+16)
    .leftSpaceToView(self.bigHeadView, 0)
    .rightSpaceToView(self.bigHeadView, 0)
    .heightIs(24);
    
    self.backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.backButton setBackgroundImage:[UIImage imageNamed:@"fankui-icon"] forState:UIControlStateNormal];
    [self.backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonView addSubview:self.backButton];
    self.backButton.sd_layout.topSpaceToView(self.buttonView,0)
    .leftSpaceToView(self.buttonView, 16)
    .widthIs(24).heightIs(24);
    
    self.collectButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.collectButton setBackgroundImage:[UIImage imageNamed:@"sc-xz"] forState:UIControlStateNormal];
    [self.collectButton addTarget:self action:@selector(collect:) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonView addSubview:self.collectButton];
    self.collectButton.sd_layout.topEqualToView(self.backButton)
    .rightSpaceToView(self.buttonView, 53)
    .widthIs(24).heightIs(24);
    
    self.shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.shareButton setBackgroundImage:[UIImage imageNamed:@"fx-icon(1)"] forState:UIControlStateNormal];
    [self.shareButton addTarget:self action:@selector(share) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonView addSubview:self.shareButton];
    self.shareButton.sd_layout.topEqualToView(self.backButton)
    .rightSpaceToView(self.buttonView, 15)
    .widthIs(24).heightIs(24);
    
    //标题详情背景
    self.titleDetailView = [[UIView alloc]init];
    self.titleDetailView.backgroundColor = GMWhiteColor;
    [self.bgScrollView addSubview:self.titleDetailView];
    self.titleDetailView.sd_layout.topSpaceToView(self.bigHeadView, 1)
    .leftSpaceToView(self.bgScrollView, 0)
    .rightSpaceToView(self.bgScrollView, 0)
    .widthIs(KScreenW).heightIs(125*kGMHeightScale);
    
//    //标题
//    self.titleLable = [[UILabel alloc]init];
//    [self.titleDetailView addSubview:self.titleLable];
//    self.titleLable.textColor = RGB(69, 69, 69);
//    self.titleLable.font = [UIFont fontWithName:@"PingFangSC-Regular" size:15];
//    self.titleLable.sd_layout.topSpaceToView(self.titleDetailView, 15)
//    .leftSpaceToView(self.titleDetailView, 66)
//    .widthIs(180).heightIs(15);
//
////    NSMutableAttributedString *titleString = [[NSMutableAttributedString alloc] initWithString:@"送个孩子们的《草虫日记》" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 15],NSForegroundColorAttributeName: [UIColor colorWithRed:69/255.0 green:69/255.0 blue:69/255.0 alpha:1.0]}];
////    self.titleLable.attributedText = titleString;
//
//    self.mianfeiLabel = [[UILabel alloc]init];
//    self.mianfeiLabel.textColor = GMWhiteColor;
//    self.mianfeiLabel.font = [UIFont fontWithName:KPFType size:12];
//    self.mianfeiLabel.backgroundColor = RGB(228, 102, 67);
//    self.mianfeiLabel.sd_cornerRadius = [NSNumber numberWithInteger:8.5];
//    [self.titleDetailView addSubview:self.mianfeiLabel];
//    self.mianfeiLabel.sd_layout.topSpaceToView(self.titleDetailView, 14)
//    .leftSpaceToView(self.titleDetailView, 20)
//    .widthIs(45).heightIs(17);
    
    //观看图标
    self.lookImg = [[UIImageView alloc]init];
    self.lookImg.image = [UIImage imageNamed:@"ico_look"];
    [self.titleDetailView addSubview: self.lookImg];
    self.lookImg.sd_layout.topSpaceToView(self.titleDetailView, 18)
    .rightSpaceToView(self.titleDetailView, 58)
    .widthIs(10).heightIs(15);
    
    
    //观看人数
    self.peoplesLable = [[UILabel alloc]init];
//    self.peoplesLable.text = @"1962";
    self.peoplesLable.textColor = RGB(153, 153, 153);
    self.peoplesLable.font = [UIFont fontWithName:KPFType size:12];
    [self.titleDetailView addSubview:self.peoplesLable];
    self.peoplesLable.sd_layout.topSpaceToView(self.titleDetailView, 20)
    .rightSpaceToView(self.titleDetailView, 15)
    .widthIs(35).heightIs(9);
    
    //标题介绍
    self.detailLable = [[UILabel alloc]init];
    self.detailLable.numberOfLines = 2;
    self.detailLable.textColor = RGB(153, 153, 153);
    self.detailLable.font = [UIFont fontWithName:KPFType size:13];
    [self.titleDetailView addSubview:self.detailLable];
    self.detailLable.sd_layout.topSpaceToView(self.titleLable, 50*kGMHeightScale)
    .leftSpaceToView(self.titleDetailView, 16)
    .rightSpaceToView(self.titleDetailView, 16)
    .widthIs(KScreenW-32).heightIs(40);
    //价格
    self.priceLable = [[UILabel alloc]init];
    self.priceLable.textColor =RGB(228, 102, 67);
//    self.priceLable.backgroundColor = GMBlueColor;
    
    self.priceLable.font = [UIFont fontWithName:KPFType size:15];
    [self.titleDetailView addSubview:self.priceLable];
    self.priceLable.sd_layout.bottomSpaceToView(self.titleDetailView, 23*kGMHeightScale)
    .rightSpaceToView(self.titleDetailView, 16)
    .widthIs(58).heightIs(12);
    
    //课程标签
    self.coureLableOne = [[UILabel alloc]init];
//    self.coureLableOne.backgroundColor = GMlightGrayColor;
    self.coureLableOne.layer.borderColor = RGB(21, 153, 164).CGColor;
    self.coureLableOne.layer.borderWidth = 1;
    self.coureLableOne.textAlignment = NSTextAlignmentCenter;
    self.coureLableOne.textColor = RGB(21, 153, 164);
    [self.titleDetailView addSubview:self.coureLableOne];
    self.coureLableOne.sd_layout.bottomSpaceToView(self.titleDetailView, 9*kGMHeightScale)
    .leftSpaceToView(self.titleDetailView, 15)
    .heightIs(18);
    
    //    课程标签2
    self.coureLaleTwo = [[UILabel alloc]init];
//    self.coureLaleTwo.backgroundColor = GMlightGrayColor;
    self.coureLaleTwo.layer.borderColor = RGB(21, 153, 164).CGColor;
    self.coureLaleTwo.layer.borderWidth = 1;
    self.coureLaleTwo.textAlignment = NSTextAlignmentCenter;
    self.coureLaleTwo.textColor = RGB(21, 153, 164);
    [self.titleDetailView addSubview:self.coureLaleTwo];
    self.coureLaleTwo.sd_layout.bottomSpaceToView(self.titleDetailView, 9*kGMHeightScale)
    .leftSpaceToView(self.coureLableOne, 15)
    .heightIs(18);
    
}

- (void)scrollZX {
    NSLog(@"xxx999999%@",self.courseTitle);
         self.vc1 = [[JSViewController alloc]init];
    self.vc1.text = [self.coureDetailInfo objectForKey:@"course_details"];
         NRViewController *vc2 = [[NRViewController alloc]init];
            vc2.dataArray = self.kcNrArray;
    vc2.source = 1;
    vc2.priceString = self.buyPriceString;
            PJViewController *vc3 = [[PJViewController alloc]init];
 
        vc3.coursePJTitleString = self.courseTitle;
        vc3.courseIDString = self.courseDetailIDString;
    self.simpleview = [[SimpleTapSliderScrollView alloc]initWithFrame:CGRectMake(0, (415)*kGMHeightScale, KScreenW, KScreenH-((415-MStatusBarHeight)*kGMHeightScale)+200)];
        self.simpleview.delegate = self;
        self.simpleview.sliderViewColor = RGB(21, 152, 164);
        self.simpleview.titileColror = RGB(153, 153, 153);
        self.simpleview.selectedColor = RGB(21, 152, 164);
        [self.simpleview createView:@[@"课程介绍",@"课程内容",@"课程评价"] andViewArr:@[self.vc1,vc2,vc3] andRootVc:self];
        [self.bgScrollView addSubview:self.simpleview];
//        simpleview.sd_layout.topSpaceToView(self.bigHeadView, 0)
//        .leftSpaceToView(self.bgScrollView, 0)
//        .rightSpaceToView(self.bgScrollView, 0)
//    .widthIs(KScreenW).heightIs(KScreenH);
        [self.simpleview sliderToViewIndex:0];
}
#pragma mark--课程详情评价成功
- (void)post:(NSMutableDictionary *)dic {
    [GMAfnTools PostHttpDataWithUrlStr:KHomeCoursePJ Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"评价成功====%@",responseObject);
        NSString *str = [NSString stringWithFormat:@"%@",[responseObject objectForKey:@"code"]];
        if ([str isEqualToString:@"1"]) {
            [self showError:[responseObject objectForKey:@"msg"]];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"commentList" object:nil];
        }else {
            [self showError:@"请先登录在评论"];
        }
        
    } FailureBlock:^(id  _Nonnull error) {
       
        NSLog(@"课程评价 失败 ====%@",error);
    }];
}

#pragma mark sliderDelegate -- OC
-(void)sliderViewAndReloadData:(NSInteger)index
    {
        NSLog(@"刷新数据啦%ld",index);
        //gouma
        if (index == 0) {
            [self buyCourseView];
        }
        if (index == 2) {
            self.keyView = [[DKSKeyboardView alloc] initWithFrame:CGRectMake(0, KScreenH - 51, KScreenW, 51)];
            //设置代理方法
            __weak typeof(self) weakSelf = self;
            self.keyView.delegate = self;
            self.keyView.textBlock = ^(NSString *textString) {
                NSUserDefaults *dex = [NSUserDefaults standardUserDefaults];
                NSString *user_id = [dex objectForKey:@"user_id"];
                NSString *token = [dex objectForKey:@"token"];
                NSString *type = @"1";
//                NSLog(@"xxxxxxxxxxX++9+9++9+999+9%@",textString);
//                [weakSelf userReply:textString];
                NSMutableDictionary *dic = [NSMutableDictionary dictionary];
                dic[@"user_id"] = user_id;
                dic[@"token"] = token;
                dic[@"type"] = type;
                dic[@"title"] = self.courseTitle;
                dic[@"content_id"] = self.courseDetailIDString;
//                NSLog(@"209382093840298--%@--%@",self.courseTitle,self.courseDetailIDString);
                dic[@"reply"] = textString;
                [weakSelf post:dic];
            };
            [self.view addSubview:_keyView];
            
        }
        if (index == 0) {
            self.keyView.hidden = YES;
        }
        if (index == 1) {
            self.keyView.hidden = YES;
        }
    }

#pragma mark--获取详情数据
- (void)getDetailData {
//    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
//    [SVProgressHUD show];
    NSUserDefaults *detafaults = [NSUserDefaults standardUserDefaults];
    NSString  *user_id =  [detafaults objectForKey:@"user_id"];
    NSString *token =  [detafaults objectForKey:@"token"];
    NSMutableDictionary *parmsDic = [NSMutableDictionary dictionary];
    parmsDic[@"user_id"] = user_id;
    parmsDic[@"token"] = token;
    parmsDic[@"course_id"] = self.courseDetailIDString;
    NSLog(@"xxxxxxxxxxxxx详情ID == %@",self.courseDetailIDString);
    [GMAfnTools PostHttpDataWithUrlStr:KCourseDetail Dic:parmsDic SuccessBlock:^(id  _Nonnull responseObject) {
        [self.bgScrollView.mj_header endRefreshing];
        self.coureDetailInfo = responseObject;
//        NSLog(@"打印详情数据---%@",responseObject);
        if (responseObject) {
            if ([responseObject[@"is_collection"] boolValue]) {
                self.collectButton.selected = YES;
                [self.collectButton setBackgroundImage:[UIImage imageNamed:@"矢量智能对象(14)"] forState:UIControlStateNormal];
            } else {
                self.collectButton.selected = NO;
                [self.collectButton setBackgroundImage:[UIImage imageNamed:@"sc-xz"] forState:UIControlStateNormal];
            }
            
            self.vc1.text = [responseObject objectForKey:@"course_details"];
            
            self.mianfeiLabel = [[UILabel alloc]init];
            self.mianfeiLabel.textColor = GMWhiteColor;
            self.mianfeiLabel.font = [UIFont fontWithName:KPFType size:12];
            self.mianfeiLabel.backgroundColor = RGB(228, 102, 67);
            self.mianfeiLabel.sd_cornerRadius = [NSNumber numberWithInteger:8.5];
            [self.titleDetailView addSubview:self.mianfeiLabel];
            self.mianfeiLabel.sd_layout.topSpaceToView(self.titleDetailView, 14)
            .leftSpaceToView(self.titleDetailView, 20)
            .widthIs(45).heightIs(17);
            
            //标题
            self.titleLable = [[UILabel alloc]init];
            [self.titleDetailView addSubview:self.titleLable];
            self.titleLable.textColor = RGB(69, 69, 69);
            self.titleLable.font = [UIFont fontWithName:@"PingFangSC-Regular" size:15];
            self.titleLable.sd_layout.topSpaceToView(self.titleDetailView, 15)
            .leftSpaceToView(self.mianfeiLabel, 12)
            .widthIs(180).heightIs(15);
                NSLog(@"------详情页数据---%@",responseObject);
            //最新课程详情封面
            [self.bigHeadView sd_setImageWithURL:[NSURL URLWithString:[responseObject objectForKey:@"course_details_cover"]]];
                self.titleLable.text = [responseObject objectForKey:@"course_name"];
            self.priceLable.text = [@"¥" stringByAppendingString:[responseObject objectForKey:@"course_price"]];
            //购买价格
                self.detailLable.text = [responseObject objectForKey:@"brief"];
            
            if ([responseObject[@"is_free"] integerValue] == 1) {
                self.priceLable.text = @"免费";
            }
            self.buyPriceString = self.priceLable.text;

//            self.coureLableOne.text = @"测试";
//            self.coureLaleTwo.text = @"测试1";
            NSMutableArray *couse_labelArray = [responseObject objectForKey:@"course_label"];
            self.coureLableOne.hidden = YES;
            self.coureLaleTwo.hidden = YES;
            if (couse_labelArray.count == 0) {
                self.coureLableOne.text = @"";
            } else if (couse_labelArray.count == 1) {
                self.coureLableOne.hidden = NO;
                self.coureLableOne.text = [couse_labelArray objectAtIndex:0];
                self.coureLableOne.sd_resetLayout.bottomSpaceToView(self.titleDetailView, 9*kGMHeightScale)
                .leftSpaceToView(self.titleDetailView, 15)
                .heightIs(18).widthIs([self calculateRowWidth:[couse_labelArray objectAtIndex:0] fontSize:18]);
            } else if (couse_labelArray.count == 2) {
                self.coureLableOne.hidden = NO;
                self.coureLaleTwo.hidden = NO;
                self.coureLableOne.text = [couse_labelArray objectAtIndex:0];
                self.coureLaleTwo.text = [couse_labelArray objectAtIndex:1];
                
                self.coureLableOne.sd_resetLayout.bottomSpaceToView(self.titleDetailView, 9*kGMHeightScale)
                .leftSpaceToView(self.titleDetailView, 15)
                .heightIs(18).widthIs([self calculateRowWidth:[couse_labelArray objectAtIndex:0] fontSize:18]);
                self.coureLaleTwo.sd_resetLayout.bottomSpaceToView(self.titleDetailView, 9*kGMHeightScale)
                .leftSpaceToView(self.coureLableOne, 15)
                .heightIs(18).widthIs([self calculateRowWidth:[couse_labelArray objectAtIndex:1] fontSize:18]);
            }
                self.peoplesLable.text = [responseObject objectForKey:@"course_vrows"];
                NSString *str = [NSString stringWithFormat:@"%@",[responseObject objectForKey:@"member_is_free"]];
                self.mianfeiLabel.text = str;
                if ([str isEqualToString:@"1"]) {
                    self.mianfeiLabel.text = @"Vip免费";
                }else {
                    self.mianfeiLabel.hidden = YES;
                    self.titleLable.sd_layout.topSpaceToView(self.titleDetailView, 15)
                    .leftSpaceToView(self.titleDetailView, 17)
                    .widthIs(180).heightIs(15);
//                    self.coureLableOne.text = [responseObject objectForKey:@"course_label"];
//                    self.coureLaleTwo.text = [responseObject objectForKey:@"course_label"];
                   // [self.bigHeadView sd_setImageWithURL:[NSURL URLWithString:[responseObject objectForKey:@"course_list_cover"]]];
                }
            
            
            self.kcNrArray = [NSMutableArray array];

            for(NSMutableDictionary *gementChildren in [responseObject objectForKey:@"course_hour_children"]) {

                CourseModel *courseModel = [[CourseModel alloc]init];
                [courseModel mj_setKeyValues:gementChildren];
                [self.kcNrArray addObject:courseModel];

                //课程介绍课程内容课程评价
                [self scrollZX];
  
            }
          
        }
         // [SVProgressHUD dismissWithDelay:2];
    } FailureBlock:^(id  _Nonnull error) {
        
//        [SVProgressHUD setStatus:@"加载失败..."];
//       [SVProgressHUD dismissWithDelay:2];
        
    }];
    
 
}

- (CGFloat)calculateRowWidth:(NSString *)string fontSize:(CGFloat)size {
    NSDictionary *dic = @{NSFontAttributeName:[UIFont systemFontOfSize:size]};
    CGRect rect = [string boundingRectWithSize:CGSizeMake(0, 30)/*计算宽度时要确定高度*/ options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:dic context:nil];
    return rect.size.width;
}
#pragma mark-返回
- (void)back {
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark-收藏
- (void)collect:(UIButton *)send {
//    NSUserDefaults *defaultsx =  [NSUserDefaults standardUserDefaults];
//    if ([[defaultsx objectForKey:@"user_id"] isEqualToString:@""]) {
//        LoginViewController *login = [[LoginViewController alloc]init];
//        [self.navigationController presentViewController:login animated:YES completion:nil];
//    }
    self.cancelCollectString = [NSMutableArray array];
     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:@"token"] ) {
        
    
    send.selected = !send.selected;
    if (send.selected == YES) {
        [send setBackgroundImage:[UIImage imageNamed:@"矢量智能对象(14)"] forState:UIControlStateNormal];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        dic[@"user_id"] = [defaults objectForKey:@"user_id"];
        dic[@"token"] = [defaults objectForKey:@"token"];
        dic[@"type"] = @"1";
        dic[@"collection_id"] = self.courseDetailIDString;
        
        [GMAfnTools PostHttpDataWithUrlStr:KCollectAdd Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
            NSLog(@"收藏成功++%@",responseObject);
            if ([[responseObject objectForKey:@"code"] isEqualToString:@"1"]) {
            
                self.cancelCollectString = [[responseObject objectForKey:@"data"] objectForKey:@"collection_id"];
                
                NSUserDefaults *collectStringID = [NSUserDefaults standardUserDefaults];
                [collectStringID setObject:self.cancelCollectString forKey:@"collection_id"];
                 [self showError:[responseObject objectForKey:@"msg"]];
            } else {
                NSLog(@"取消成功");
//                [self showError:[responseObject objectForKey:@"msg"]];
            }
        } FailureBlock:^(id  _Nonnull error) {
            
        }];
       
        NSLog(@"搜藏");
    }
    if (send.selected == NO) {
        NSLog(@"xxxxxx%@",self.cancelCollectString);
        [send setBackgroundImage:[UIImage imageNamed:@"sc-xz"] forState:UIControlStateNormal];
        [self test];
        NSLog(@"取消搜藏");
    }
    }else {
        [self showError:@"请先登录"];
    }
}
- (void)test {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"user_id"] = [defaults objectForKey:@"user_id"];
    dic[@"token"] = [defaults objectForKey:@"token"];
    dic[@"type"] = @"1";
    dic[@"collection_id"] = self.courseDetailIDString;
    NSLog(@"--%@--%@",dic,KCollectCancelxx);
    [GMAfnTools PostHttpDataWithUrlStr:KCollectCancelxx Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
        if ([[responseObject objectForKey:@"code"] isEqualToString:@"1"]) {
            [self showError:[responseObject objectForKey:@"msg"]];
        }
        NSLog(@"%@",responseObject);
        
        
        //            if ([[responseObject objectForKey:@"code"] isEqualToString:@"1"]) {
        //                self.cancelCollectString = [[responseObject objectForKey:@"data"] objectForKey:@"collection_id"];
        //
        //                NSUserDefaults *collectStringID = [NSUserDefaults standardUserDefaults];
        //                [collectStringID setObject:self.cancelCollectString forKey:@"collection_id"];
        //                [self showError:[responseObject objectForKey:@"msg"]];
        //            } else {
        //                [self showError:[responseObject objectForKey:@"msg"]];
        //            }
    } FailureBlock:^(id  _Nonnull error) {
        NSLog(@"取消收藏失败---%@",error);
    }];
}
#pragma mark -- 提示框
- (void)showError:(NSString *)errorMsg {
    // 1.弹框提醒
    // 初始化对话框
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:errorMsg preferredStyle:UIAlertControllerStyleAlert];
//    [alert addAction:[UIAlertAction actionWithTitle:@"" style:UIAlertActionStyleDefault handler:nil]];
    // 弹出对话框
    [self presentViewController:alert animated:true completion:nil];
    
    [self performSelector:@selector(dimisssAlert:) withObject:alert afterDelay:1];
    
}

- (void) dimisssAlert:(UIAlertController *)alert
{
    if (alert) {
        [alert dismissViewControllerAnimated:YES completion:nil];
    }
    
}

#pragma mark-分享
- (void)share {
    NSLog(@"分享");
}


- (void)buyCourseView {
    
    UIView *payView = [[UIView alloc]init];
    payView.backgroundColor = GMWhiteColor;
    [self.view addSubview:payView];
    payView.sd_layout.bottomSpaceToView(self.view,0)
    .leftSpaceToView(self.view, 0)
    .widthIs(KScreenW).heightIs(48);
    
    UIButton *vipLogo = [UIButton buttonWithType:UIButtonTypeCustom];
    [vipLogo setBackgroundImage:[UIImage imageNamed:@"huiyuan"] forState:UIControlStateNormal];
    [payView addSubview:vipLogo];
    vipLogo.sd_layout.topSpaceToView(payView, 10)
    .leftSpaceToView(payView, 33)
    .widthIs(16).heightIs(14);
    
    //成为会员
    UIButton *viperButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [viperButton setTitleColor:RGB(21, 152, 164) forState:UIControlStateNormal];
    [viperButton setTitle:@"成为会员" forState:UIControlStateNormal];
    
    viperButton.titleLabel.font = kFont(14);
    [payView addSubview:viperButton];
    viperButton.sd_layout.bottomSpaceToView(payView, 9)
    .leftSpaceToView(payView, 10)
    .widthIs(58).heightIs(11);
    // Do any additional setup after loading the view.
    
    UILabel *priceLabel = [[UILabel alloc]init];
//    priceLabel.text = @"¥196.00";
    priceLabel.text = self.buyPriceString;
    priceLabel.textColor = RGB(228, 102, 67);
    priceLabel.font = [UIFont fontWithName:KPFType size:16];
    [payView addSubview:priceLabel];
    priceLabel.sd_layout.topSpaceToView(payView, 19)
    .leftSpaceToView(viperButton, 20*kGMWidthScale)
    .widthIs(60).heightIs(13);
    //成为为朋友购买
    UIButton *friendPay = [UIButton buttonWithType:UIButtonTypeCustom];
    [friendPay setTitleColor:GMWhiteColor forState:UIControlStateNormal];
    friendPay.backgroundColor = RGB(252,171,71);
    friendPay.layer.cornerRadius = 14.5;
    [friendPay setTitle:@"为朋友购买" forState:UIControlStateNormal];
    friendPay.titleLabel.font = kFont(13);
    [payView addSubview:friendPay];
    friendPay.sd_layout.topSpaceToView(payView, 10)
    .leftSpaceToView(priceLabel,17*kGMWidthScale)
    .widthIs(90*kGMWidthScale).heightIs(29);
    
    //立刻购买
    UIButton *buyButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [buyButton setTitleColor:GMWhiteColor forState:UIControlStateNormal];
    [buyButton setTitle:@"立刻购买" forState:UIControlStateNormal];
    buyButton.backgroundColor = RGB(228, 102, 67);
    buyButton.layer.cornerRadius = 14.5;
    buyButton.titleLabel.font = kFont(14);
    [buyButton addTarget:self action:@selector(buyClick:) forControlEvents:UIControlEventTouchUpInside];
    [payView addSubview:buyButton];
    buyButton.sd_layout.topSpaceToView(payView, 10)
    .leftSpaceToView(friendPay,10*kGMWidthScale)
    .widthIs(90*kGMWidthScale).heightIs(29);
}

- (void)buyClick:(UIButton *)send {
    NSLog(@"点击购买课程");
    NSLog(@"打印课程ID---%@",self.courseDetailIDString);
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *user_id = [defaults objectForKey:@"user_id"];
    NSString *token = [defaults objectForKey:@"token"];
    NSMutableDictionary *orderDic = [NSMutableDictionary dictionary];
    orderDic[@"user_id"] = user_id;
    orderDic[@"token"] = token;
    orderDic[@"course_types"] = @"1";
    orderDic[@"course_id"] = self.courseDetailIDString;
    NSLog(@"生成课程订单URL---%@",orderDic);
    __weak typeof(self) weakSelf = self;
    [GMAfnTools PostHttpDataWithUrlStr:KOrderIDURL Dic:orderDic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"生成商品订单---%@",responseObject);
        NSMutableDictionary *dataDic = [responseObject objectForKey:@"data"];
        NSString *order_id = [dataDic objectForKey:@"order_id"];
//        [weakSelf user_id:user_id token:token order_id:order_id];
    } FailureBlock:^(id  _Nonnull error) {
        NSLog(@"-------服务器错误-%@",error);
    }];

}
#pragma mark--育点支付
- (void)user_id:(NSString *)user_id token:(NSString *)token order_id:(NSString *)order_id {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"user_id"] = user_id;
    dic[@"token"] = token;
    dic[@"order_id"] = order_id;
    NSLog(@"======+%@",dic);
    [GMAfnTools PostHttpDataWithUrlStr:KPointPayURL Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"支付成功------%@",responseObject);
        if ([[responseObject objectForKey:@"code"] isEqualToString:@"1"]) {
            [self showError:[responseObject objectForKey:@"msg"]];
        }else {
            [self showError:[responseObject objectForKey:@"msg"]];
        }
    } FailureBlock:^(id  _Nonnull error) {
        NSLog(@"支付失败======%@",error);
    }];
}


@end
