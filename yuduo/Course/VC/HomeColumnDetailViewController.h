//
//  HomeColumnDetailViewController.h
//  yuduo
//
//  Created by mason on 2019/9/1.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeColumnDetailViewController : UIViewController
PropertyStrong(NSString, courseDetailIDString);
PropertyStrong(NSString, courseTitle);//课程标题
@end

NS_ASSUME_NONNULL_END
