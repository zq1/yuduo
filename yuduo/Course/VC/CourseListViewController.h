//
//  CourseListViewController.h
//  yuduo
//
//  Created by Mac on 2019/8/12.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CourseListViewController : BaseViewController
PropertyStrong(NSMutableArray, respoceArray);
@property (nonatomic,copy) void(^resBlockValue)( NSMutableArray *resArray);
@end

NS_ASSUME_NONNULL_END
