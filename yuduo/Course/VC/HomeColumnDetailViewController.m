//
//  HomeColumnDetailViewController.m
//  yuduo
//
//  Created by mason on 2019/9/1.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "HomeColumnDetailViewController.h"
//专栏介绍 专栏内容 专栏评价
#import "JSViewController.h"
#import "NRViewController.h"
#import "ZLPJViewController.h"

#import "TapSliderScrollView.h"
#import "SimpleTapSliderScrollView.h"
#import "GMScrollView.h"
#import "ColumnZLneiRong.h"
#import "CourseModel.h"

#import "DKSTextView.h"
#import "DKSKeyboardView.h"

@interface HomeColumnDetailViewController ()<SliderLineViewDelegate,SimpleSliderLineViewDelegate,UITableViewDelegate,UITableViewDataSource, UIScrollViewDelegate,UIGestureRecognizerDelegate,DKSKeyboardDelegate>
{
//    JSViewController *vc1 ;
//    NRViewController *vc2 ;
//    ZLPJViewController *vc3 ;
}

@property (nonatomic, strong) JSViewController *vc1;
@property (nonatomic, strong) NRViewController *vc2;
@property (nonatomic, strong) ZLPJViewController *vc3;

@property (nonatomic, copy) NSDictionary *responseObject;

@property (nonatomic ,assign) BOOL selectSort;

@property (nonatomic, strong) NSArray *dataArr;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) NSMutableArray *myChildViewControllers;
PropertyStrong(NSMutableArray, cancelCollectString);
PropertyStrong(UIScrollView , bgScrollView);//背景
PropertyStrong(UIImageView, bigHeadView);//顶部图片
PropertyStrong(UIButton , backButton);//返回按钮
PropertyStrong(UIButton , collectButton);//收藏按钮
PropertyStrong(UIButton, shareButton); //分享按钮
PropertyStrong(UIView, titleDetailView); //课程详情
PropertyStrong(UILabel , titleLable);//标题
PropertyStrong(UILabel, detailLable);
PropertyStrong(UILabel, coureLableOne);
PropertyStrong(UILabel, coureLaleTwo);//课程标签2
PropertyStrong(UILabel, priceLable);//价格
PropertyStrong(UIImageView, lookImg);//观看图标
PropertyStrong(UILabel, peoplesLable);//观看人数
PropertyStrong(UILabel , mianfeiLabel);


PropertyStrong(NSMutableArray, zlnrArray);
//顶部按钮View
PropertyStrong(UIView, buttonView);

//PropertyStrong(CourseDetailCollectModel ,mol);
PropertyStrong(UITableView, pjTableView);
//输入框
PropertyStrong(DKSKeyboardView, keyView);
@end

@implementation HomeColumnDetailViewController

-(void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = RGB(244,243 , 244);
    //    NSLog(@"=======%@",self.courseDetailIDString);
    //    NSMutableDictionary *detailDic = [NSMutableDictionary dictionaryWithObject:self.courseDetailIDString forKey:@"course_id"];
    //    [GMAfnTools PostHttpDataWithUrlStr:KCourseDetail Dic:detailDic SuccessBlock:^(id  _Nonnull responseObject) {
    //        NSLog(@"---------%@",responseObject);
    //    } FailureBlock:^(id  _Nonnull error) {
    //
    //    }];
    //    self.navigationController.navigationBar.hidden = YES;
    //    self.navigationController.navigationBarHidden = YES;
    
    [self layoutSubViews];
    //获取数据
    [self getDetailData];
//    //课程介绍课程内容课程评价
    [self scrollZX];
    
//    [self buyCourseView];
    // Do any additional setup after loading the view.
}
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    
    return YES;
}
- (void)layoutSubViews {
    //滑动背景
    self.bgScrollView = [[UIScrollView alloc]initWithFrame:[UIScreen mainScreen].bounds];
    self.bgScrollView.contentSize = CGSizeMake(KScreenW, KScreenH+200);
    self.bgScrollView.contentOffset = CGPointMake(0, 0);
    self.bgScrollView.delegate = self;
    //    self.bgScrollView.delaysContentTouches = NO;
    //    self.scrollView.canCancelContentTouches = NO;
    [self.view addSubview:self.bgScrollView];
    
    //img背景
    self.bigHeadView = [[UIImageView alloc]init];
    self.bigHeadView.backgroundColor = GMlightGrayColor;
    self.bigHeadView.userInteractionEnabled = YES;
    [self.bgScrollView addSubview:self.bigHeadView];
    self.bigHeadView.sd_layout.topSpaceToView(self.bgScrollView, - MStatusBarHeight)
    .leftSpaceToView(self.bgScrollView,0)
    .rightEqualToView(self.bgScrollView)
    .heightIs(280*kGMHeightScale);
    
    self.backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.backButton setBackgroundImage:[UIImage imageNamed:@"fankui-icon"] forState:UIControlStateNormal];
    [self.backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [self.bigHeadView addSubview:self.backButton];
    self.backButton.sd_layout.topSpaceToView(self.bigHeadView,MStatusBarHeight+16)
    .leftSpaceToView(self.bigHeadView, 16)
    .widthIs(24).heightIs(24);
    
    self.collectButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [self.collectButton setBackgroundImage:[UIImage imageNamed:@"sc-xz"] forState:UIControlStateNormal];
    [self.collectButton addTarget:self action:@selector(collect:) forControlEvents:UIControlEventTouchUpInside];
    [self.bigHeadView addSubview:self.collectButton];
    self.collectButton.sd_layout.topEqualToView(self.backButton)
    .rightSpaceToView(self.bigHeadView, 53)
    .widthIs(24).heightIs(24);
    
    self.shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.shareButton setBackgroundImage:[UIImage imageNamed:@"fx-icon(1)"] forState:UIControlStateNormal];
    [self.shareButton addTarget:self action:@selector(share) forControlEvents:UIControlEventTouchUpInside];
    [self.bigHeadView addSubview:self.shareButton];
    self.shareButton.sd_layout.topEqualToView(self.backButton)
    .rightSpaceToView(self.bigHeadView, 15)
    .widthIs(24).heightIs(24);
    
    //标题详情背景
    self.titleDetailView = [[UIView alloc]init];
    self.titleDetailView.backgroundColor = GMWhiteColor;
    [self.bgScrollView addSubview:self.titleDetailView];
    self.titleDetailView.sd_layout.topSpaceToView(self.bigHeadView, 1)
    .leftSpaceToView(self.bgScrollView, 0)
    .rightSpaceToView(self.bgScrollView, 0)
    .widthIs(KScreenW).heightIs(125*kGMHeightScale);
//
//    //标题
//    self.titleLable = [[UILabel alloc]init];
//    [self.titleDetailView addSubview:self.titleLable];
//    self.titleLable.textColor = RGB(69, 69, 69);
//    self.titleLable.font = [UIFont fontWithName:@"PingFangSC-Regular" size:15];
//    self.titleLable.sd_layout.topSpaceToView(self.titleDetailView, 15)
//    .leftSpaceToView(self.titleDetailView, 66)
//    .widthIs(180).heightIs(15);
    
    //免费logo
    self.mianfeiLabel = [[UILabel alloc] init];
    self.mianfeiLabel.numberOfLines = 0;
    self.mianfeiLabel.textColor = RGB(255, 255, 255);
    self.mianfeiLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:11];
    self.mianfeiLabel.textAlignment = NSTextAlignmentCenter;
    self.mianfeiLabel.backgroundColor = RGB(228, 102, 67);
    [self.titleDetailView addSubview:self.mianfeiLabel];
    self.mianfeiLabel.sd_cornerRadius = [NSNumber numberWithInteger:8.5];
    self.mianfeiLabel.sd_layout.topSpaceToView(self.titleDetailView, 15).leftSpaceToView(self.titleDetailView, 15).widthIs(45).heightIs(17);
    
    
    //    NSMutableAttributedString *titleString = [[NSMutableAttributedString alloc] initWithString:@"送个孩子们的《草虫日记》" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 15],NSForegroundColorAttributeName: [UIColor colorWithRed:69/255.0 green:69/255.0 blue:69/255.0 alpha:1.0]}];
    //    self.titleLable.attributedText = titleString;
    
    //观看图标
    self.lookImg = [[UIImageView alloc]init];
    self.lookImg.image = [UIImage imageNamed:@"ico_look"];
    [self.titleDetailView addSubview: self.lookImg];
    self.lookImg.sd_layout.topSpaceToView(self.titleDetailView, 18)
    .rightSpaceToView(self.titleDetailView, 48)
    .widthIs(10).heightIs(15);
    
    //观看人数
    self.peoplesLable = [[UILabel alloc]init];
    //    self.peoplesLable.text = @"1962";
    self.peoplesLable.textColor = RGB(153, 153, 153);
    self.peoplesLable.font = [UIFont fontWithName:KPFType size:12];
    [self.titleDetailView addSubview:self.peoplesLable];
    self.peoplesLable.sd_layout.topSpaceToView(self.titleDetailView, 20)
    .rightSpaceToView(self.titleDetailView, 15)
    .widthIs(28).heightIs(9);
    
    //标题介绍
    self.detailLable = [[UILabel alloc]init];
    self.detailLable.numberOfLines = 2;
    self.detailLable.textColor = RGB(153, 153, 153);
    self.detailLable.font = [UIFont fontWithName:KPFType size:13];
    [self.titleDetailView addSubview:self.detailLable];
    self.detailLable.sd_layout.topSpaceToView(self.titleLable, 52)
    .leftSpaceToView(self.titleDetailView, 16)
    .rightSpaceToView(self.titleDetailView, 16)
    .widthIs(KScreenW-32).heightIs(40);
    //价格
    self.priceLable = [[UILabel alloc]init];
    self.priceLable.textColor =RGB(228, 102, 67);
    self.priceLable.font = [UIFont fontWithName:KPFType size:15];
    [self.titleDetailView addSubview:self.priceLable];
    self.priceLable.sd_layout.bottomSpaceToView(self.titleDetailView, 23*kGMHeightScale)
    .rightSpaceToView(self.titleDetailView, 16)
    .widthIs(58).heightIs(12);
    
    //课程标签
    self.coureLableOne = [[UILabel alloc]init];
//    self.coureLableOne.backgroundColor = GMlightGrayColor;
    self.coureLableOne.layer.borderColor = RGB(21, 153, 164).CGColor;
    self.coureLableOne.layer.borderWidth = 1;
    self.coureLableOne.textAlignment = NSTextAlignmentCenter;
    self.coureLableOne.textColor = RGB(21, 153, 164);
    [self.titleDetailView addSubview:self.coureLableOne];
    self.coureLableOne.sd_layout.bottomSpaceToView(self.titleDetailView, 9*kGMHeightScale)
    .leftSpaceToView(self.titleDetailView, 15)
    .widthIs(60).heightIs(18);
    
    //    课程标签2
    self.coureLaleTwo = [[UILabel alloc]init];
//    self.coureLaleTwo.backgroundColor = GMlightGrayColor;
    self.coureLaleTwo.layer.borderColor = RGB(21, 153, 164).CGColor;
    self.coureLaleTwo.layer.borderWidth = 1;
    self.coureLaleTwo.textAlignment = NSTextAlignmentCenter;
    self.coureLaleTwo.textColor = RGB(21, 153, 164);
    [self.titleDetailView addSubview:self.coureLaleTwo];
    self.coureLaleTwo.sd_layout.bottomSpaceToView(self.titleDetailView, 9*kGMHeightScale)
    .leftSpaceToView(self.coureLableOne, 15)
    .widthIs(60).heightIs(18);
    
}

- (void)scrollZX {
    self.vc1 = [[JSViewController alloc]init];
    self.vc2 = [[NRViewController alloc]init];
    self.vc2.source = 2;
    self.vc2.priceString = self.priceLable.text;
    self.vc3 = [[ZLPJViewController alloc]init];
    self.vc3.coursePJTitleString = self.courseTitle;
    NSLog(@"xxxxxxxx%@",self.courseTitle);
    self.vc3.courseIDString = self.courseDetailIDString;
    if (self.responseObject) {
        self.vc1.text = [self.responseObject objectForKey:@"course_details"];
    }
    if (self.zlnrArray) {
        self.vc2.dataArray = self.zlnrArray;
    }
    SimpleTapSliderScrollView *simpleview = [[SimpleTapSliderScrollView alloc]initWithFrame:CGRectMake(0, (415-MStatusBarHeight)*kGMHeightScale, KScreenW, KScreenH-((415-MStatusBarHeight)*kGMHeightScale)+200)];
    simpleview.delegate = self;
    simpleview.sliderViewColor = RGB(21, 152, 164);
    simpleview.titileColror = RGB(153, 153, 153);
    simpleview.selectedColor = RGB(21, 152, 164);
    [simpleview createView:@[@"专栏介绍",@"专栏内容",@"专栏评价"] andViewArr:@[self.vc1,self.vc2,self.vc3] andRootVc:self];
    [self.bgScrollView addSubview:simpleview];
    //        simpleview.sd_layout.topSpaceToView(self.bigHeadView, 0)
    //        .leftSpaceToView(self.bgScrollView, 0)
    //        .rightSpaceToView(self.bgScrollView, 0)
    //    .widthIs(KScreenW).heightIs(KScreenH);
    
    [simpleview sliderToViewIndex:0];
    
}
#pragma mark--课程详情评价成功
- (void)post:(NSMutableDictionary *)dic {
    [GMAfnTools PostHttpDataWithUrlStr:KHomeCoursePJ Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"专栏成功====%@",responseObject);
        NSString *str = [NSString stringWithFormat:@"%@",[responseObject objectForKey:@"code"]];
        if ([str isEqualToString:@"1"]) {
            [self showError:[responseObject objectForKey:@"msg"]];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"commentListZL" object:nil];
        }else {
            [self showError:@"请先登录在评论"];
        }
        
    } FailureBlock:^(id  _Nonnull error) {
        
        NSLog(@"专栏评价 失败 ====%@",error);
    }];
}

#pragma mark sliderDelegate -- OC
-(void)sliderViewAndReloadData:(NSInteger)index
{
    if (index == 0 || index == 1) {
        [self buyCourseView];
    }
    if (index == 2) {
        self.keyView = [[DKSKeyboardView alloc] initWithFrame:CGRectMake(0, KScreenH - 51, KScreenW, 51)];
        //设置代理方法
        __weak typeof(self) weakSelf = self;
        self.keyView.delegate = self;
        self.keyView.textBlock = ^(NSString *textString) {
            NSUserDefaults *dex = [NSUserDefaults standardUserDefaults];
            NSString *user_id = [dex objectForKey:@"user_id"];
            NSString *token = [dex objectForKey:@"token"];
            NSString *type = @"2";
            //                NSLog(@"xxxxxxxxxxX++9+9++9+999+9%@",textString);
            //                [weakSelf userReply:textString];
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            dic[@"user_id"] = user_id;
            dic[@"token"] = token;
            dic[@"type"] = type;
            dic[@"title"] = weakSelf.courseTitle;
            dic[@"content_id"] = weakSelf.courseDetailIDString;
            //                NSLog(@"209382093840298--%@--%@",self.courseTitle,self.courseDetailIDString);
            dic[@"reply"] = textString;
            [weakSelf post:dic];
            
            
        };
        [self.view addSubview:_keyView];
        
    }
    if (index == 0) {
        self.keyView.hidden = YES;
    }
    if (index == 1) {
        self.keyView.hidden = YES;
    }
}

#pragma mark--获取详情数据
- (void)getDetailData {
    NSUserDefaults *detafaults = [NSUserDefaults standardUserDefaults];
    NSString  *user_id =  [detafaults objectForKey:@"user_id"];
    NSString *token =  [detafaults objectForKey:@"token"];
    NSMutableDictionary *parmsDic = [NSMutableDictionary dictionary];
    parmsDic[@"user_id"] = user_id;
    parmsDic[@"token"] = token;
    //专栏详情ID
    parmsDic[@"gement_id"] = self.courseDetailIDString;
//    NSLog(@"获取详情h数据 ID === %@",self.courseDetailIDString);
//    parmsDic[@"screen"] = @"";
    [GMAfnTools PostHttpDataWithUrlStr:KCoulumnDetail Dic:parmsDic SuccessBlock:^(id  _Nonnull responseObject) {
        self.responseObject = responseObject;
        NSLog(@"最新专栏详情数据======%@",responseObject);
        
        if ([responseObject[@"is_collection"] boolValue]) {
            self.collectButton.selected = YES;
            [self.collectButton setBackgroundImage:[UIImage imageNamed:@"矢量智能对象(14)"] forState:UIControlStateNormal];
        } else {
            self.collectButton.selected = NO;
            [self.collectButton setBackgroundImage:[UIImage imageNamed:@"sc-xz"] forState:UIControlStateNormal];
        }
        
        NSString *vipFree = [NSString stringWithFormat:@"%@",[responseObject objectForKey:@"member_is_free"]];
        if ([vipFree isEqualToString:@"1"]) {
            self.mianfeiLabel.text = @"Vip免费";
            //标题
            self.titleLable = [[UILabel alloc]init];
            [self.titleDetailView addSubview:self.titleLable];
            self.titleLable.textColor = RGB(69, 69, 69);
            self.titleLable.font = [UIFont fontWithName:@"PingFangSC-Regular" size:15];
            self.titleLable.sd_layout.topSpaceToView(self.titleDetailView, 15)
            .leftSpaceToView(self.titleDetailView, 66)
            .widthIs(180).heightIs(15);
        }else {
            self.mianfeiLabel.hidden = YES;
            //标题
            self.titleLable = [[UILabel alloc]init];
            [self.titleDetailView addSubview:self.titleLable];
            self.titleLable.textColor = RGB(69, 69, 69);
            self.titleLable.font = [UIFont fontWithName:@"PingFangSC-Regular" size:15];
            self.titleLable.sd_layout.topSpaceToView(self.titleDetailView, 15)
            .leftSpaceToView(self.titleDetailView, 22)
            .widthIs(180).heightIs(15);
        }
        self.titleLable.text = [responseObject objectForKey:@"course_name"];
        self.detailLable.text = [responseObject objectForKey:@"brief"];
        self.peoplesLable.text = [[responseObject objectForKey:@"course_vrows"] stringByAppendingString:@""];
//        self.coureLableOne.text = [responseObject objectForKey:@"course_label"];
//        self.coureLaleTwo.text = [responseObject objectForKey:@"course_label"];
        NSMutableArray *couse_labelArray = [responseObject objectForKey:@"course_label"];
        self.coureLableOne.hidden = YES;
        self.coureLaleTwo.hidden = YES;
        if ([couse_labelArray isKindOfClass:[NSArray class]]) {
            if (couse_labelArray.count == 0) {
                self.coureLableOne.text = @"";
            } else if (couse_labelArray.count == 1) {
                self.coureLableOne.hidden = NO;
                self.coureLableOne.text = [couse_labelArray objectAtIndex:0];
                self.coureLableOne.sd_resetLayout.bottomSpaceToView(self.titleDetailView, 9*kGMHeightScale)
                .leftSpaceToView(self.titleDetailView, 15)
                .heightIs(18).widthIs([self calculateRowWidth:[couse_labelArray objectAtIndex:0] fontSize:18]);
            } else if (couse_labelArray.count == 2) {
                self.coureLableOne.hidden = NO;
                self.coureLaleTwo.hidden = NO;
                self.coureLableOne.text = [couse_labelArray objectAtIndex:0];
                self.coureLaleTwo.text = [couse_labelArray objectAtIndex:1];
                
                self.coureLableOne.sd_resetLayout.bottomSpaceToView(self.titleDetailView, 9*kGMHeightScale)
                .leftSpaceToView(self.titleDetailView, 15)
                .heightIs(18).widthIs([self calculateRowWidth:[couse_labelArray objectAtIndex:0] fontSize:18]);
                self.coureLaleTwo.sd_resetLayout.bottomSpaceToView(self.titleDetailView, 9*kGMHeightScale)
                .leftSpaceToView(self.coureLableOne, 15)
                .heightIs(18).widthIs([self calculateRowWidth:[couse_labelArray objectAtIndex:1] fontSize:18]);
            }
        }
        self.priceLable.text = [@"¥" stringByAppendingString:[responseObject objectForKey:@"course_under_price"]];
        [self.bigHeadView sd_setImageWithURL:[NSURL URLWithString:[responseObject objectForKey:@"course_details_cover"]]];
        
        self.zlnrArray = [NSMutableArray array];
        
        self.vc1.text = [responseObject objectForKey:@"course_details"];
        
        for(NSMutableDictionary *gementChildren in [responseObject objectForKey:@"gement_hour_children"]) {
            
//            ColumnZLneiRong *zlModel = [[ColumnZLneiRong alloc]init];
            CourseModel *zlModel = [[CourseModel alloc] init];
            [zlModel mj_setKeyValues:gementChildren];
            [self.zlnrArray addObject:zlModel];
            //课程介绍课程内容课程评价
//            [self scrollZX];
            
        }
        self.vc2.dataArray = self.zlnrArray;
        [self sliderViewAndReloadData:0];
    } FailureBlock:^(id  _Nonnull error) {
        NSLog(@"最新专栏详情数据错误---%@",error);
    }];
}

- (CGFloat)calculateRowWidth:(NSString *)string fontSize:(CGFloat)size {
    NSDictionary *dic = @{NSFontAttributeName:[UIFont systemFontOfSize:size]};
    CGRect rect = [string boundingRectWithSize:CGSizeMake(0, 30)/*计算宽度时要确定高度*/ options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:dic context:nil];
    return rect.size.width;
}
#pragma mark-返回
- (void)back {
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark-收藏
- (void)collect:(UIButton *)send {
    //    NSUserDefaults *defaultsx =  [NSUserDefaults standardUserDefaults];
    //    if ([[defaultsx objectForKey:@"user_id"] isEqualToString:@""]) {
    //        LoginViewController *login = [[LoginViewController alloc]init];
    //        [self.navigationController presentViewController:login animated:YES completion:nil];
    //    }
    self.cancelCollectString = [NSMutableArray array];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:@"token"] ) {
        send.selected = !send.selected;
        if (send.selected == YES) {
            [send setBackgroundImage:[UIImage imageNamed:@"矢量智能对象(14)"] forState:UIControlStateNormal];
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            dic[@"user_id"] = [defaults objectForKey:@"user_id"];
            dic[@"token"] = [defaults objectForKey:@"token"];
            dic[@"type"] = @"2";
            dic[@"collection_id"] = self.courseDetailIDString;
            
            [GMAfnTools PostHttpDataWithUrlStr:KCollectAdd Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
                NSLog(@"收藏成功++%@",responseObject);
                if ([[responseObject objectForKey:@"code"] isEqualToString:@"1"]) {
                    self.cancelCollectString = [[responseObject objectForKey:@"data"] objectForKey:@"collection_id"];
                    
                    NSUserDefaults *collectStringID = [NSUserDefaults standardUserDefaults];
                    [collectStringID setObject:self.cancelCollectString forKey:@"collection_id"];
                    [self showError:[responseObject objectForKey:@"msg"]];
                } else {
                    NSLog(@"取消成功");
                    //                [self showError:[responseObject objectForKey:@"msg"]];
                }
            } FailureBlock:^(id  _Nonnull error) {
                
            }];
            
            NSLog(@"搜藏");
        }
        if (send.selected == NO) {
            NSLog(@"xxxxxx%@",self.cancelCollectString);
            [send setBackgroundImage:[UIImage imageNamed:@"sc-xz"] forState:UIControlStateNormal];
            [self test];
            NSLog(@"取消搜藏");
        }
    }else {
        [self showError:@"请先登录"];
    }
}
- (void)test {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"user_id"] = [defaults objectForKey:@"user_id"];
    dic[@"token"] = [defaults objectForKey:@"token"];
    dic[@"type"] = @"2";
    dic[@"collection_id"] = self.courseDetailIDString;
    NSLog(@"--%@--%@",dic,KCollectCancelxx);
    [GMAfnTools PostHttpDataWithUrlStr:KCollectCancelxx Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
        if ([[responseObject objectForKey:@"code"] isEqualToString:@"1"]) {
            [self showError:[responseObject objectForKey:@"msg"]];
        }
        //            if ([[responseObject objectForKey:@"code"] isEqualToString:@"1"]) {
        //                self.cancelCollectString = [[responseObject objectForKey:@"data"] objectForKey:@"collection_id"];
        //
        //                NSUserDefaults *collectStringID = [NSUserDefaults standardUserDefaults];
        //                [collectStringID setObject:self.cancelCollectString forKey:@"collection_id"];
        //                [self showError:[responseObject objectForKey:@"msg"]];
        //            } else {
        //                [self showError:[responseObject objectForKey:@"msg"]];
        //            }
    } FailureBlock:^(id  _Nonnull error) {
        NSLog(@"xxxxxxx%@",error);
    }];
}
#pragma mark -- 提示框
- (void)showError:(NSString *)errorMsg {
    // 1.弹框提醒
    // 初始化对话框
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:errorMsg preferredStyle:UIAlertControllerStyleAlert];
    //    [alert addAction:[UIAlertAction actionWithTitle:@"" style:UIAlertActionStyleDefault handler:nil]];
    // 弹出对话框
    [self presentViewController:alert animated:true completion:nil];
    
    [self performSelector:@selector(dimisssAlert:) withObject:alert afterDelay:1];
    
}

- (void) dimisssAlert:(UIAlertController *)alert
{
    if (alert) {
        [alert dismissViewControllerAnimated:YES completion:nil];
    }
    
}
#pragma mark-分享
- (void)share {
    NSLog(@"分享");
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)buyCourseView {
    
    UIView *payView = [[UIView alloc]init];
    payView.backgroundColor = GMWhiteColor;
    [self.view addSubview:payView];
    payView.sd_layout.bottomSpaceToView(self.view,0)
    .leftSpaceToView(self.view, 0)
    .widthIs(KScreenW).heightIs(48);
    
    UIButton *vipLogo = [UIButton buttonWithType:UIButtonTypeCustom];
    [vipLogo setBackgroundImage:[UIImage imageNamed:@"huiyuan"] forState:UIControlStateNormal];
    [payView addSubview:vipLogo];
    vipLogo.sd_layout.topSpaceToView(payView, 10)
    .leftSpaceToView(payView, 33)
    .widthIs(16).heightIs(14);
    
    //成为会员
    UIButton *viperButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [viperButton setTitleColor:RGB(21, 152, 164) forState:UIControlStateNormal];
    [viperButton setTitle:@"成为会员" forState:UIControlStateNormal];
//    viperButton addTarget:self action:@selector(openVIP) forControlEvents:<#(UIControlEvents)#>
    viperButton.titleLabel.font = kFont(14);
    [payView addSubview:viperButton];
    viperButton.sd_layout.bottomSpaceToView(payView, 9)
    .leftSpaceToView(payView, 10)
    .widthIs(58).heightIs(11);
    // Do any additional setup after loading the view.
    
    UILabel *priceLabel = [[UILabel alloc]init];
    //    priceLabel.text = @"¥196.00";
//    priceLabel.text = self.buyPriceString;
    priceLabel.textColor = RGB(228, 102, 67);
    priceLabel.font = [UIFont fontWithName:KPFType size:16];
    [payView addSubview:priceLabel];
    priceLabel.sd_layout.topSpaceToView(payView, 19)
    .leftSpaceToView(viperButton, 20*kGMWidthScale)
    .widthIs(60).heightIs(13);
    //成为为朋友购买
    UIButton *friendPay = [UIButton buttonWithType:UIButtonTypeCustom];
    [friendPay setTitleColor:GMWhiteColor forState:UIControlStateNormal];
    friendPay.backgroundColor = RGB(252,171,71);
    friendPay.layer.cornerRadius = 14.5;
    [friendPay setTitle:@"为朋友购买" forState:UIControlStateNormal];
    friendPay.titleLabel.font = kFont(13);
    [payView addSubview:friendPay];
    friendPay.sd_layout.topSpaceToView(payView, 10)
    .leftSpaceToView(priceLabel,17*kGMWidthScale)
    .widthIs(90*kGMWidthScale).heightIs(29);
    
    //立刻购买
    UIButton *buyButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [buyButton setTitleColor:GMWhiteColor forState:UIControlStateNormal];
    [buyButton setTitle:@"立刻购买" forState:UIControlStateNormal];
    buyButton.backgroundColor = RGB(228, 102, 67);
    buyButton.layer.cornerRadius = 14.5;
    buyButton.titleLabel.font = kFont(14);
    [buyButton addTarget:self action:@selector(buyClick:) forControlEvents:UIControlEventTouchUpInside];
    [payView addSubview:buyButton];
    buyButton.sd_layout.topSpaceToView(payView, 10)
    .leftSpaceToView(friendPay,10*kGMWidthScale)
    .widthIs(90*kGMWidthScale).heightIs(29);
}

- (void)buyClick:(UIButton *)send {
    NSLog(@"点击购买课程");
    NSLog(@"打印课程ID---%@",self.courseDetailIDString);
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *user_id = [defaults objectForKey:@"user_id"];
    NSString *token = [defaults objectForKey:@"token"];
    NSMutableDictionary *orderDic = [NSMutableDictionary dictionary];
    orderDic[@"user_id"] = user_id;
    orderDic[@"token"] = token;
    orderDic[@"course_types"] = @"2";
    orderDic[@"course_id"] = self.courseDetailIDString;
    NSLog(@"生成专栏订单URL---%@",orderDic);
    __weak typeof(self) weakSelf = self;
    [GMAfnTools PostHttpDataWithUrlStr:KOrderIDURL Dic:orderDic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"生成商品订单---%@",responseObject);
        NSMutableDictionary *dataDic = [responseObject objectForKey:@"data"];
        NSString *order_id = [dataDic objectForKey:@"order_id"];
        [weakSelf user_id:user_id token:token order_id:order_id];
    } FailureBlock:^(id  _Nonnull error) {
        NSLog(@"-------服务器错误-%@",error);
    }];
    
}
#pragma mark--育点支付
- (void)user_id:(NSString *)user_id token:(NSString *)token order_id:(NSString *)order_id {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"user_id"] = user_id;
    dic[@"token"] = token;
    dic[@"order_id"] = order_id;
    NSLog(@"======+%@",dic);
    [GMAfnTools PostHttpDataWithUrlStr:KPointPayURL Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"支付成功------%@",responseObject);
        if ([[responseObject objectForKey:@"code"] isEqualToString:@"1"]) {
            [self showError:[responseObject objectForKey:@"msg"]];
        }else {
            [self showError:[responseObject objectForKey:@"msg"]];
        }
    } FailureBlock:^(id  _Nonnull error) {
        NSLog(@"支付失败======%@",error);
    }];
}

@end
