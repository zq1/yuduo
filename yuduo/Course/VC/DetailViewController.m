//
//  DetailViewController.m
//  yuduo
//
//  Created by Mac on 2019/8/5.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "DetailViewController.h"
#import "DetailTableViewCell.h"
@interface DetailViewController ()<UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource>
{
    UIView *lightBgView;
    UIButton *sortButton;
    UIWindow *windowBg;
    
    UIButton*tembtn;
    NSMutableArray*labArr;
    NSMutableArray*btnArr;
    UIPageControl* page;
    NSMutableArray*imgarr;
    
    NSMutableArray *tableViewArray;
    CGRect temRect;
}
@property (nonatomic ,assign) BOOL selectSort;
@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"课程";
    self.view.backgroundColor = GMlightGrayColor;
    
    
    //设置navigation
    [self navigationSet];
    //创建排序列表
//    [self sortTab];
    //分类列表
    [self cateTableViewCreate];
  
                   
    
    // Do any additional setup after loading the view.
}
#pragma mark - 分类列表

- (void)cateTableViewCreate {
    imgarr=[NSMutableArray new];
    //    labArr=[NSMutableArray new];
    btnArr=[NSMutableArray new];
    //tableview 数组
    tableViewArray = [NSMutableArray new];
    temRect = CGRectZero;
    [self creatScrollview];
    [self creatTableview];
    lightBgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenW, KScreenH)];
    windowBg = [[UIApplication sharedApplication].windows lastObject];
    lightBgView.backgroundColor = RGBA(153, 153, 153, 0.5);
    lightBgView.hidden = YES;
    lightBgView.userInteractionEnabled  = YES;
    [windowBg addSubview:lightBgView];
    
    UIButton *tapButton = [UIButton buttonWithType:UIButtonTypeCustom];
    tapButton.frame = lightBgView.frame;
    [tapButton addTarget:self action:@selector(touchClick) forControlEvents:UIControlEventTouchUpInside];
    [lightBgView addSubview:tapButton];
    //创建排序
    [self sortTab];
}

-(void)creatTableview
{
    for (int i = 0; i < btnArr.count; i ++) {
        _HotTableView = [[UITableView alloc] initWithFrame:CGRectMake(KScreenW*i, 10, KScreenW,CGRectGetHeight(_Scrview2.frame)) style:UITableViewStylePlain];
        _HotTableView.delegate = self;
        _HotTableView.dataSource = self;
        _HotTableView.tag = 10000+i;
        [_Scrview2 addSubview:_HotTableView];
    }
}
#pragma mark--创建ScrollView
-(void)creatScrollview
{
    NSArray*arr=@[@"全部",@"亲子阅读",@"思维导图",@"幼婴睡眠",@"英语",@"中文"];
    for (int i=0; i<arr.count; i++) {
        UIButton*btn=[[UIButton alloc] initWithFrame:CGRectMake(i*KScreenW/4, 0, KScreenW/4, 44)];
        btn.backgroundColor = [UIColor brownColor];
        btn.titleLabel.textAlignment = NSTextAlignmentCenter;
        btn.tag = i;
        [btn setTitle:arr[i] forState:UIControlStateNormal];
        btn.titleLabel.font=[UIFont systemFontOfSize:14];
        
        [btnArr addObject:btn];
        if (i==0) {
            [btn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
            btn.titleLabel.font = [UIFont systemFontOfSize:17];
            tembtn=btn;
            temRect=btn.frame;
            
        }
        else
            [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        //        btn.titleLabel.font = [UIFont systemFontOfSize:14];
        [btn addTarget:self action:@selector(btn_clicked:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn];
        
    }
    _Scrview1=[[UIScrollView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(temRect), KScreenW/5, 0)];
    [self.view addSubview:_Scrview1];
    _Scrview1.backgroundColor=[UIColor clearColor];
    _Scrview1.delegate=self;
    _Scrview1.contentSize=CGSizeMake(KScreenW*5, 2);
    _Scrview1.showsHorizontalScrollIndicator=NO;
    _Scrview1.showsVerticalScrollIndicator=NO;
    
    _Scrview2=[[UIScrollView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_Scrview1.frame),KScreenW , KScreenH-CGRectGetMaxY(_Scrview1.frame))];
    [self.view addSubview:_Scrview2];
    _Scrview2.delegate=self;
    page=[[UIPageControl alloc]initWithFrame:CGRectZero];
    page.numberOfPages=0;
    page.currentPage=0;
    _Scrview2.pagingEnabled=YES;
    [_Scrview2 addSubview:page];
    _Scrview2.showsHorizontalScrollIndicator=NO;
    _Scrview2.showsVerticalScrollIndicator=NO;
    _Scrview2.contentSize=CGSizeMake(KScreenW*5,KScreenH/2);
    
    
}

#pragma mark--实时监听滚动
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView==_Scrview2) {
        
        _Scrview1.contentOffset=CGPointMake(_Scrview2.contentOffset.x/5, 0);
        _Scrview1.frame=CGRectMake(_Scrview2.contentOffset.x/5, 50, KScreenW/5, 2);
        
    }
    
}


#pragma mark--停止滚动
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    
    if (scrollView==_Scrview2) {
        
        int i=_Scrview2.contentOffset.x/KScreenW;
        UIButton*btn=btnArr[i];
        [tembtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        tembtn.titleLabel.font = [UIFont systemFontOfSize:13];
        
        [btn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont systemFontOfSize:16];
        tembtn=btn;
        
    }
    
}


#pragma mark--文字点击事件
-(void)btn_clicked:(UIButton*)btn
{
    //tembtn  未选中字体和颜色
    [tembtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    tembtn.titleLabel.font = [UIFont systemFontOfSize:13];
    //选中的颜色和字体
    [btn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:16];
    
    tembtn=btn;
    
    if (btn.tag==0) {//1
        
        
        [_Scrview2 setContentOffset:CGPointMake(0, 0) animated:YES];
    }
    
    if (btn.tag==1) {//2
        
        [_Scrview2 setContentOffset:CGPointMake(KScreenW, 0) animated:YES];
        
    }
    
    if (btn.tag==2) {//3
        
        [_Scrview2 setContentOffset:CGPointMake(KScreenW*2, 0) animated:YES];
        
    }
    
    if (btn.tag==3) {
        [_Scrview2 setContentOffset:CGPointMake(KScreenW*3, 0) animated:YES];
        
    }
    
    if (btn.tag == 4) {
        [_Scrview2 setContentOffset:CGPointMake(KScreenW*4, 0 )animated:YES];
    }
    if (btn.tag == 5) {
        [_Scrview2 setContentOffset:CGPointMake(KScreenW*5, 0 )animated:YES];
        [_Scrview1 setContentSize:CGSizeMake(KScreenW*5, 0)];
    }
    
}


#pragma mark navigationBarSet
- (void)navigationSet {
    //隐藏返回文字
    self.navigationController.navigationBar.topItem.title = @"";
    UIButton *sortButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [sortButton setBackgroundImage:[UIImage imageNamed:@"排序"] forState:UIControlStateNormal];
    [sortButton addTarget:self action:@selector(sortButton:) forControlEvents:UIControlEventTouchUpInside];
//    sortButton.selected = NO;
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc]initWithCustomView:sortButton];
    self.navigationItem.rightBarButtonItem = rightButton;
    
    self.navigationController.navigationBar.shadowImage = [UIImage new];
}

#pragma mark - 排序method
- (void)sortButton:(UIButton *)send {
    NSLog(@"点击了排序按钮");
    send.selected=!sortButton.selected;//每次点击都改变按钮的状态
    [UIView transitionWithView:lightBgView duration:1 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        self->lightBgView.backgroundColor = RGBA(153, 153, 153, 0.4);
    } completion:nil];
    
    lightBgView.hidden = NO;
    
    if(send.selected == YES){
        
        lightBgView.hidden = NO;
    }
    if (send.selected == NO) {
        lightBgView.hidden = YES;
    }
}
- (void)touchClick {
    NSLog(@" touch");
    sortButton.selected = NO;
    lightBgView.hidden = YES;
    //    self.view.backgroundColor = GMlightGrayColor;
    //        sortButton.selected = NO;
}

- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBarHidden = NO;
    self.navigationController.navigationBar.topItem.title = @"课程";
}

#pragma mark 排序tableView
- (void)sortTab {
    self.sortTableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.sortTableView.delegate = self;
    self.sortTableView.dataSource = self;
    self.sortTableView.tag = 10086;
    self.sortTableView.layer.cornerRadius = 10;
//    self.sortTableView.backgroundColor = GMBlueColor;
//    self.sortTableView.hidden = YES;
    [lightBgView addSubview:self.sortTableView];
    self.sortTableView.sd_layout.topSpaceToView(lightBgView, 70*kGMHeightScale)
    .rightSpaceToView(lightBgView, 15)
    .widthIs(105*kGMWidthScale).heightIs(169);
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (tableView.tag == 10002) {
        //            [NSString stringWithFormat:@"s: %ld, r: %ld", indexPath.section, indexPath.row];
        
        NSLog(@"s: %ld, r: %ld",indexPath.section, indexPath.row);
        
    }
}
#pragma mark--返回行高
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (tableView.tag) {
        case 10000:
            return 150;
            break;
            
        default:
            break;
    }
    return 40;
}
#pragma mark--返回行数
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.sortTableView) {
        return 4;
    }
    
    switch (tableView.tag) {
        case 10000:
            return 4;
            break;
        case 10001:
            return 3;
            break;
        case 10002:
            return 4;
            break;
        case 10003:
            return 2;
            break;
        case 10004:
            return 26;
        default:
            break;
    }
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSArray *arr = @[@"最新课程",@"最热课程",@"免费课程",@"VIP限免"];
    static NSString*key0=@"cell0";
    
    static NSString*key1=@"cell1";
    static NSString*key2=@"cell2";
    static NSString*key3=@"cell3";
    static NSString*key4=@"cell4";
    static NSString*key5=@"cell5";

    UITableViewCell*cell;
    
    if (tableView.tag == 10086) {
        cell=[self.sortTableView dequeueReusableCellWithIdentifier:key0];
        if (cell==nil) {
            cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:key0];
        }
        cell.textLabel.text = [arr objectAtIndex:indexPath.row];
        cell.textLabel.font = [UIFont systemFontOfSize:14];
    }
    
    if (tableView.tag == 10000) {
            DetailTableViewCell *cell0 = [[DetailTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:key1];
            return cell0;
    }
    
    if (tableView.tag == 10001) {
        cell=[_HotTableView dequeueReusableCellWithIdentifier:key2];
        if (cell==nil) {
            cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:key2];
        }
        
        cell.textLabel.text=[NSString stringWithFormat:@"第二个table:数据%li",(long)indexPath.row];
    }
    
    if (tableView.tag == 10002) {
        
        cell=[_HotTableView dequeueReusableCellWithIdentifier:key3];
        if (cell==nil) {
            cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:key3];
        }
        
        cell.textLabel.text=[NSString stringWithFormat:@"第三个table:数据%li",(long)indexPath.row];
    }
    
    if (tableView.tag == 10003) {
        
        cell=[_HotTableView dequeueReusableCellWithIdentifier:key4];
        if (cell==nil) {
            cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:key4];
        }
        
        cell.textLabel.text=[NSString stringWithFormat:@"第si个table:数据%li",(long)indexPath.row];
    }
    
    
    if (tableView.tag == 10004) {
        
        cell=[_HotTableView dequeueReusableCellWithIdentifier:key5];
        if (cell==nil) {
            cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:key5];
        }
        
        cell.textLabel.text=[NSString stringWithFormat:@"第si个table:数据%li",(long)indexPath.row];
    }
    
    //    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    return cell;
}

//-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    self.sortTableView.hidden = YES;
//    self.view.backgroundColor = GMlightGrayColor;
//}

@end

