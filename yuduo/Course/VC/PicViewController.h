//
//  PicViewController.h
//  yuduo
//
//  Created by Mac on 2019/8/19.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CourseVideoTuwenModel;

NS_ASSUME_NONNULL_BEGIN

@interface PicViewController : UIViewController
PropertyStrong(NSMutableArray, picArray);

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *content;

@property (nonatomic, strong) CourseVideoTuwenModel *model;

@end

NS_ASSUME_NONNULL_END
