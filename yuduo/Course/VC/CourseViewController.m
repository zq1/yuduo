//
//  CourseViewController.m
//  yuduo
//
//  Created by Mac on 2019/7/23.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "CourseViewController.h"
#import "MCScrollView.h"
#import "RecColumnTableViewCell.h"
#import "RecCourseTableViewCell.h"
#import "RecLiveTableViewCell.h"
#import "YDChooseTableViewCell.h"
#import "DetailViewController.h" //课程详情页
//课程介绍内容评价
#import "CoureDetailViewController.h"
#import "RecColumnCollectionViewCell.h"
#import "CourseListViewController.h"
#import "NewZLViewController.h"
//直播
#import "CourseZhiBoViewController.h"
//精选详情
#import "JXdetailViewController.h"
//推荐专栏
#import "CourseTJZLViewController.h"

//推荐专栏数据
#import "CourseColumnListModel.h"
//推荐直播
#import "CourseLiveListsModel.h"
#import "HomeHotCourseModel.h"
#import "HomeHotColumnModel.h"
#import "HomeColumnDetailViewController.h"
#import "SearchViewController.h"
#import "SDCycleScrollView.h"
#import "NewZhiboDetailViewController.h"
#import "NewYuDuoChoiceDetailViewController.h"
#import "HomeLiveViewController.h"
#import "UIImageView+WebCache.h"
//轮播图模型
#import "HomeBannerModel.h"
//育朵精选
#import "YDSelectModel.h"
@interface CourseViewController ()<UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UINavigationControllerDelegate,SDCycleScrollViewDelegate>
{
    NSArray *_imagesURLStrings;
    SDCycleScrollView *_customCellScrollViewDemo;
}
PropertyStrong(UITableView, courseTab);//课程tableview
PropertyStrong(MCScrollView,scrollView);
PropertyStrong(UIView, headBGView);
PropertyStrong(NSArray, sectionTitle); //分区数组
PropertyStrong(NSMutableArray, dataArray);
@property (assign , nonatomic) NSInteger selectIndex;

@property (nonatomic, strong) SDCycleScrollView *bannerCycleScrollerView; //轮播图
@property (nonatomic, strong) NSMutableArray *homeBannerArray;//轮播图

PropertyStrong(NSMutableArray, TJColumnArray);
PropertyStrong(NSMutableArray, TJCourseArray);
PropertyStrong(NSMutableArray, TJCourseLiveArray);
PropertyStrong(NSMutableArray, homeHotColumnArray);
PropertyStrong(NSMutableArray, homeHotArray);
PropertyStrong(NSMutableArray, ydSelectArray);

@end

@implementation CourseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.delegate = self;
    self.navigationController.navigationBar.topItem.title = @"课程";
    
    //热门课程
    [self getDataHomeHotCourse];
    //推荐专栏
    [self getdataHomeHotColumn];
    //推荐专栏数据
    [self getTJColumnData];
    //推荐c课程直播
    [self getTJCourseLiveData];
    //育朵精选
    [self getYdSelectData];
    //设置导航栏
//    [self changeNavigation];
    //创建tableview
    [self creatHomeTabView];
    // 获取首页轮播图
    [self getDataHomeBanner];
    // Do any additional setup after loading the view.
}

#pragma mark 创建TableView

- (void)creatHomeTabView {
    
    self.sectionTitle = @[@"推荐课程",@"推荐专栏",@"推荐直播",@"育朵精选"];
    self.headBGView = [[UIView alloc]initWithFrame:CGRectMake(0, 11, KScreenW, 217)];
//        self.headBGView.backgroundColor = GMRedColor;
    //    self.bgScrollView.backgroundColor = GMBlueColor;
    //
    self.courseTab = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, KScreenW, KScreenH-87)style:UITableViewStyleGrouped];
    self.courseTab.delegate = self;
    self.courseTab.dataSource = self;
    self.courseTab.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.courseTab.backgroundColor = GMWhiteColor;
    self.courseTab.tableHeaderView = self.headBGView;
    //    self.HomeTableView.tableHeaderView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 0, CGFLOAT_MAX)];
    self.courseTab.backgroundView = nil;
    self.courseTab.backgroundColor = [UIColor clearColor];
    //    self.HomeTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.courseTab addSubview:self.headBGView];
    [self.view addSubview:self.courseTab];
    
    
    self.scrollView = [[MCScrollView alloc] initWithFrame:CGRectMake(self.view.frame.origin.x+15, 10, self.view.frame.size.width-30, 175)];
    [self.headBGView addSubview:self.scrollView];
    
    NSArray *imagesURLStrings = @[
                                  @"http://yuduo.oss-cn-beijing.aliyuncs.com/image/709550e6e063eddf73a05965f0ed163c.jpg",
                                  @"http://yuduo.oss-cn-beijing.aliyuncs.com/image/e3332348a2d45444c9e4699ec5579e3f.jpg"
                                  
                                  ];
    _imagesURLStrings = imagesURLStrings;
    
    // 网络加载 ---
    UIView *view1 = [[UIView alloc]initWithFrame:CGRectMake(15, 10, KScreenW-30, 175)];
    view1.layer.backgroundColor = GMBlackColor.CGColor;
    //            NSString *str = [NSString stringWithFormat:@" %ld",pageIndex+1];
    //            NSString *sumLabel = @"/3";
    //            view.text = [str stringByAppendingString:sumLabel];
    view1.layer.cornerRadius = 9;
    [self.headBGView addSubview:view1];
    //            view1.textColor = GMWhiteColor;
    
    self.bannerCycleScrollerView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, KScreenW-30, 175) delegate:self placeholderImage:[UIImage imageNamed:@"placeholder"]];
    self.bannerCycleScrollerView.layer.cornerRadius = 10;
    self.bannerCycleScrollerView.layer.masksToBounds = YES;
    self.bannerCycleScrollerView.currentPageDotImage = [UIImage imageNamed:@"pageControlCurrentDot"];
    
    self.bannerCycleScrollerView.pageDotImage = [UIImage imageNamed:@"pageControlDot"];
    self.bannerCycleScrollerView.imageURLStringsGroup = imagesURLStrings;
    [view1 addSubview:self.bannerCycleScrollerView];
//    //    self.scrollView.backgroundColor = [UIColor brownColor];
//    __weak typeof(self) weakSelf = self;
//    self.scrollView.indexBack = ^(NSInteger pageIndex) {
//        weakSelf.selectIndex = pageIndex;
//        //        NSLog(@"=========%zd", pageIndex);
//        /*
//         轮播数字
//         **/
//        UILabel *view = [[UILabel alloc] init];
//        //        view.frame = CGRectMake(302,self.scrollView.frame.origin.y-6,34.5,18);
//        view.layer.backgroundColor = GMBlackColor.CGColor;
//        NSString *str = [NSString stringWithFormat:@" %ld",pageIndex+1];
//        NSString *sumLabel = @"/3";
//        view.text = [str stringByAppendingString:sumLabel];
////        NSLog(@"xxxxxxxxxxx");
//        view.layer.cornerRadius = 9;
//        view.textColor = GMWhiteColor;
//        [weakSelf.scrollView addSubview:view];
//        view.sd_layout.bottomSpaceToView(weakSelf.scrollView, 6)
//        .rightSpaceToView(weakSelf.scrollView, 9)
//        .widthIs(35).heightIs(18);
//        
//        
//        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:weakSelf action:@selector(clickImage)];
//        [weakSelf.scrollView addGestureRecognizer:tapGesture];
//        weakSelf.scrollView.userInteractionEnabled = YES;
//    };
//    self.scrollView.imageArray = @[@"1.png", @"2.png", @"3.png"];
//    self.scrollView.duration = 2.0;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    switch (section) {
        case 0:
            return 43;
            break;
        case 1:
            return 39;
        case 2:
            return 37;
        case 3:
            return 42;
        default:
            break;
    }
    
    return 50;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    switch (section) {
        case 0:
            if (self.homeHotArray.count > 4) {
                return 4;
            }
            return self.homeHotArray.count;
            break;
        case 1:
            return 1;
            break;
        case 2:
        {
            if (self.TJCourseLiveArray.count > 4) {
                return 4;
            }
            return self.TJCourseLiveArray.count;
        }
            break;
        case 3:
        {
           return self.ydSelectArray.count;
        }
            break;
        case 5:
            return 4;
            break;
        case 6:
            return 3;
        default:
            break;
    }
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (indexPath.section) {
        case 0:
            return 159;
            break;
        case 1:
            return 486;
        case 2:
            return 229;
        case 3:
            return 110;
            break;
    }
    return 0;
}

- (NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [NSMutableArray arrayWithObjects:@"1", @"1" , @"1" , @"1", nil];
    }
    return _dataArray;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    
    return self.sectionTitle.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier0 = @"cell0";
    static NSString *cellIdentifier1 = @"cell1";
    static NSString *cellIdentifier2 = @"cell2";
    static NSString *cellIdentifier3 = @"cell3";
    switch (indexPath.section){
            // 推荐课程1
        case 0:
        {
            RecCourseTableViewCell *cell0 = [[RecCourseTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier0];
            HomeHotCourseModel *hotC = [self.homeHotArray objectAtIndex:indexPath.row];
            cell0.titleLabel.text = hotC.course_name;
            cell0.detailLabel.text  = hotC.brief;
            [cell0.headImgView sd_setImageWithURL:[NSURL URLWithString:hotC.course_list_cover]];
            cell0.headImgView.sd_cornerRadius = [NSNumber numberWithInt:10];
            cell0.keshiLabel.text = [hotC.class_hour stringByAppendingFormat:@"课时"];
            NSDictionary *attribtDic = @{NSStrikethroughStyleAttributeName: [NSNumber numberWithInteger:NSUnderlineStyleSingle]};
            NSMutableAttributedString *attribtStr = [[NSMutableAttributedString alloc]initWithString:[@"¥" stringByAppendingString:hotC.course_under_price] attributes:attribtDic];
            cell0.selectionStyle = UITableViewCellSelectionStyleNone;
            cell0.xPriceLabel.attributedText = attribtStr;
//            cell0.xPriceLabel.text = hotC.course_price;
            cell0.peoplesLabel.text = [hotC.course_vrows stringByAppendingFormat:@"人"];
            cell0.priceLabel.text = [@"¥" stringByAppendingString:hotC.course_price];
            if ([hotC.is_free isEqualToString:@"1"]) {
                cell0.priceLabel.text = @"免费";
            }
            return cell0;
        }
            break;
            //推荐专栏2
        case 1:
        {
            UITableViewCell *cell1 = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier1];
                cell1.backgroundColor = [UIColor whiteColor];
        
            self.flowLayout = [[UICollectionViewFlowLayout alloc]init];
            self.flowLayout.itemSize = CGSizeMake((self.view.frame.size.width-30)/2-5, 225);
            self.flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
            //    self.flowLayout.minimumLineSpacing = 10;
            //    self.flowLayout.minimumInteritemSpacing = 10;
            self.collectionV = [[UICollectionView alloc]initWithFrame:CGRectMake(15, 25, self.view.frame.size.width-30, 460) collectionViewLayout:self.flowLayout];
            self.collectionV.backgroundColor = GMWhiteColor;
            [cell1.contentView addSubview:self.collectionV];
            self.collectionV.delegate = self;
            self.collectionV.dataSource = self;
            cell1.selectionStyle = UITableViewCellSelectionStyleNone;
            [self.collectionV registerClass:[RecColumnCollectionViewCell class] forCellWithReuseIdentifier:@"myCell"];
            

        return cell1;
        }
            break;
            //推荐直播
        case 2:
        { 
            RecLiveTableViewCell *cell2 = [[RecLiveTableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier2];
            //                cell2.backgroundColor = [UIColor blueColor];
            cell2.selectionStyle = UITableViewCellSelectionStyleNone;
            CourseLiveListsModel *liveModel = [self.TJCourseLiveArray objectAtIndex:indexPath.row];
            cell2.titleLabel.text = liveModel.name;
            [cell2.imgView sd_setImageWithURL:[NSURL URLWithString:liveModel.publicity_cover]];
            
            
            return cell2;
        }
            break;
            //育朵精选
        case 3:
        {
            YDChooseTableViewCell *cell3=[[YDChooseTableViewCell alloc]initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:cellIdentifier3];
            YDSelectModel *ydSelectM = [self.ydSelectArray objectAtIndex:indexPath.row];
            
            cell3.titleLabel.text = ydSelectM.article_title;
            cell3.detailLabel.text = ydSelectM.brief;
            cell3.zanLabel.text = [ydSelectM.thumbs_up stringByAppendingString:@"人"];
            cell3.mianfeiLabel.text = @"免费";
            cell3.zanLabel.text = ydSelectM.article_vrows;
            [cell3.headImgView sd_setImageWithURL:[NSURL URLWithString:ydSelectM.article_cover]];
            if ([ydSelectM.article_member_is_free isEqualToString:@"1"]) {
                cell3.mianfeiLabel.text = @"vip免费";
            }else {
                cell3.mianfeiLabel.hidden = YES;
                cell3.titleLabel.sd_layout.leftSpaceToView(cell3.headImgView, 17);
            }
            cell3.priceLabel.text = ydSelectM.article_price;
            cell3.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell3;
        }
            break;
            //热门直播
    }
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0) {
        HomeHotCourseModel *model = [self.homeHotArray objectAtIndex:indexPath.row];
        CoureDetailViewController *coureDetail = [[CoureDetailViewController alloc]init];
        coureDetail.hidesBottomBarWhenPushed = YES;
        coureDetail.courseDetailIDString = model.course_id;
        [coureDetail.navigationController setNavigationBarHidden:YES];
        [self.navigationController pushViewController:coureDetail animated:YES];
    }
    if (indexPath.section == 2) {
        NewZhiboDetailViewController * zhibo = [[NewZhiboDetailViewController alloc] init];
        [self.navigationController pushViewController:zhibo animated:YES];
    }
    if (indexPath.section == 3) {
        YDSelectModel *ydSelectM = [self.ydSelectArray objectAtIndex:indexPath.row];
        NewYuDuoChoiceDetailViewController *jxVC = [[NewYuDuoChoiceDetailViewController alloc]init];
        [jxVC setHidesBottomBarWhenPushed:YES];
        jxVC.articleStringID = ydSelectM.article_id;
        jxVC.articleTitle = ydSelectM.article_title;
        [self.navigationController pushViewController:jxVC animated:YES];
    }
    
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}
#pragma mark 自定义section
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section

{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenW, 0)];
//    view.backgroundColor = [UIColor cyanColor];
    //各个模块
    UILabel *sectionTitle = [[UILabel alloc] init];
    sectionTitle.frame = CGRectMake(16,0,90,17);

    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:[self.sectionTitle objectAtIndex:section] attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Medium" size: 18],NSForegroundColorAttributeName: [UIColor colorWithRed:23/255.0 green:151/255.0 blue:164/255.0 alpha:1.0]}];
    sectionTitle.attributedText = string;
    [view addSubview:sectionTitle];
    
    //更多
    UIButton *gdButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [gdButton setTitle:@"更多" forState:UIControlStateNormal];
    [gdButton addTarget:self action:@selector(moreClick:)
       forControlEvents:UIControlEventTouchUpInside];
    gdButton.tag = section;
    [gdButton setTitleColor:GMlightGrayColor forState:UIControlStateNormal];
    [gdButton.titleLabel setFont:[UIFont systemFontOfSize:14]];
    [view addSubview:gdButton];
    gdButton.sd_layout.topSpaceToView(view, 0)
    .rightSpaceToView(view, 0 )
    .widthIs(100).heightIs(16);
    //按钮
    UIButton *moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [moreButton setImage:[UIImage imageNamed:@"ico_next"] forState:UIControlStateNormal];
    [moreButton addTarget:self action:@selector(moreClick:) forControlEvents:UIControlEventTouchUpInside];
    moreButton.tag = section;
    [view addSubview:moreButton];
    moreButton.sd_layout.topSpaceToView(view, 0)
    .rightSpaceToView(view, 15).widthIs(8).heightIs(14);
    if (section == 3) {
        
        UILabel *bgLabel = [[UILabel alloc]init];
        bgLabel.backgroundColor = GMWhiteColor;
        [view addSubview:bgLabel];
        bgLabel.sd_layout.topSpaceToView(view, 0)
        .rightSpaceToView(view, 0)
        .widthIs(80)
        .heightIs(31);
        
    }
    return view;
    
}
#pragma mark 更多

- (void)moreClick:(UIButton *)send {
    NSLog(@"点击更多 %ld",(long)send.tag);
    switch (send.tag) {
        case 0:
        {
            CourseListViewController *detailVC = [[CourseListViewController alloc]init];
                //隐藏tabbar
            [detailVC setHidesBottomBarWhenPushed:YES];
            [self.navigationController pushViewController: detailVC animated:YES];
        }
            break;
        case 1:
            
        {
            NewZLViewController *detailVC = [[NewZLViewController alloc]init];
            //隐藏tabbar
            [detailVC setHidesBottomBarWhenPushed:YES];
            [self.navigationController pushViewController: detailVC animated:YES];
        }
            
            break;
        case 2:
        {
            HomeLiveViewController *zhibo = [[HomeLiveViewController alloc]init];
            //隐藏tabbar
            [zhibo setHidesBottomBarWhenPushed:YES];
            [self.navigationController pushViewController: zhibo animated:YES];
        }
            break;
            
        default:
            break;
    }
    
}


-(NSArray<NSString *> *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    
    
    return nil;
}

- (void)clickImage {
    //选择图片
    NSLog(@"xxxxxxx%ld",self.selectIndex);
}

#pragma mark 搜索按钮
- (void)searchClick {
    
    NSLog(@"点击课程页搜索按钮");
    SearchViewController *searchVC = [[SearchViewController alloc]init];
    
    [self.navigationController pushViewController:searchVC animated:YES];
}

#pragma mark -- collectionView delegate , datasource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (self.TJColumnArray.count > 4) {
        return 4;
    }
    return self.TJColumnArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    RecColumnCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"myCell" forIndexPath:indexPath];
      cell.contentView.layer.cornerRadius =5.0f;
      cell.contentView.layer.borderWidth =0.08f;
      cell.contentView.layer.borderColor = [UIColor blackColor].CGColor;    cell.contentView.layer.masksToBounds =YES;
    CourseColumnListModel *columnModel = [self.TJColumnArray objectAtIndex:indexPath.row];
    cell.titleLabel.text = columnModel.course_name;
    cell.keshiLabel.text = [columnModel.class_hour stringByAppendingString:@"课时"];
    //多少人观看
    cell.peoplesLabel.text = [columnModel.course_vrows stringByAppendingString:@"人"];
    cell.priceLabel.text = [@"¥" stringByAppendingString:columnModel.course_under_price];
    [cell.imgView sd_setImageWithURL:[NSURL URLWithString:columnModel.course_list_cover]];
    cell.imgView .sd_cornerRadius = [NSNumber numberWithInteger:10];
    
    cell.backgroundColor = GMWhiteColor;
    
    return cell;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"%ld %ld",indexPath.section,indexPath.row);
    
    
    HomeColumnDetailViewController *coureDetailVC = [[HomeColumnDetailViewController alloc]init];
    HomeHotColumnModel *mol = [self.homeHotColumnArray objectAtIndex:indexPath.row];
    coureDetailVC.courseDetailIDString = mol.gement_id;
    
    [coureDetailVC setHidesBottomBarWhenPushed:YES];
    [self.navigationController pushViewController:coureDetailVC animated:YES];
    
}
- (void)column:(NSString *)columnString {
    self.homeHotColumnArray = [NSMutableArray array];
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"category_id"] = columnString;
    dic[@"screen"] = @"1";
    [GMAfnTools PostHttpDataWithUrlStr:KCoulumnGementList Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"打印最新专栏列表====%@",responseObject);
        for (NSMutableDictionary *dic in responseObject) {
            HomeHotColumnModel *mol = [[HomeHotColumnModel alloc]init];
            [mol mj_setKeyValues:dic];
            [self.homeHotColumnArray addObject:mol];
        }
        //        NSLog(@"最新专栏---%@",responseObject);
        [self.courseTab reloadData];
    } FailureBlock:^(id  _Nonnull error) {
        
    }];
}
#pragma mark--轮播图1
- (void)getDataHomeBanner {
    self.homeBannerArray = [NSMutableArray array];
    NSMutableDictionary *bannerDic = [NSMutableDictionary dictionary];
    bannerDic[@"type"] = @"3";
    bannerDic[@"url_type"] = @"1";
    
    [GMAfnTools PostHttpDataWithUrlStr:KHomeBanner Dic:bannerDic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"首页轮播图-----%@",responseObject);
        NSMutableArray *imageList = [NSMutableArray array];
        for (NSMutableDictionary *dic in responseObject) {
            HomeBannerModel *mol = [[HomeBannerModel alloc]init];
            [mol mj_setKeyValues:dic];
            if ([mol.status isEqualToString:@"1"]) {
                [self.homeBannerArray addObject:mol];
                [imageList addObject:mol.picture];
            }
        }
        self.bannerCycleScrollerView.imageURLStringsGroup = imageList;
        [self.courseTab reloadData];
    } FailureBlock:^(id  _Nonnull error) {
        
    }];
}
#pragma mark--最新专栏4
- (void)getdataHomeHotColumn {
    
    
    __weak typeof(self) weakSelf = self;
    NSMutableDictionary *columnDic = [NSMutableDictionary dictionary];
    columnDic[@"pid"] = @"2";
    [GMAfnTools PostHttpDataWithUrlStr:KCategoryID Dic:columnDic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"xxxxx8888888%@",responseObject);
        for (NSMutableDictionary *dic in responseObject) {
            HomeHotColumnModel *mol = [[HomeHotColumnModel alloc]init];
            [mol mj_setKeyValues:dic];
            
            [weakSelf column:mol.gement_id];
            //            NSLog(@"打印最新专栏4--%@",mol.category_id);
        }
    } FailureBlock:^(id  _Nonnull error) {
        
    }];
}
#pragma mark--推荐专栏
- (void)getTJColumnData {
    self.TJColumnArray  = [NSMutableArray array];
    [GMAfnTools PostHttpDataWithUrlStr:KRecommendZLList Dic:nil SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"-------推荐专栏%@",responseObject);
        for (NSMutableDictionary *columnDic in responseObject) {
            CourseColumnListModel *mol = [[CourseColumnListModel alloc]init];
            [mol mj_setKeyValues:columnDic];
            [self.TJColumnArray addObject:mol];
        }
        [self.courseTab reloadData];
    } FailureBlock:^(id  _Nonnull error) {
        
        NSLog(@"-----推荐专栏错误-%@",error);
        
    }];
}
#pragma mark -- 推荐直播
- (void)getTJCourseLiveData {
    self.TJCourseLiveArray  = [NSMutableArray array];
    [GMAfnTools PostHttpDataWithUrlStr:KCourseLiveList Dic:nil SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"----课程---推荐直播--%@",responseObject);
        for (NSMutableDictionary *liveDic in responseObject) {
            CourseLiveListsModel *mol = [[CourseLiveListsModel alloc]init];
            [mol mj_setKeyValues:liveDic];
            
            [self.TJCourseLiveArray addObject:mol];
        }
        [self.courseTab reloadData];
    } FailureBlock:^(id  _Nonnull error) {
        NSLog(@"-+++++%@",error);
    }];
}
#pragma mark--推荐课程
- (void)getDataHomeHotCourse {
    
    self.homeHotArray = [NSMutableArray array];
    [GMAfnTools PostHttpDataWithUrlStr:KRecommendCourseList Dic:nil SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"推荐--最热课程%@",responseObject);
        for (NSMutableDictionary *dic in responseObject) {
            HomeHotCourseModel *mol = [[HomeHotCourseModel alloc]init];
            [mol mj_setKeyValues:dic];
            [self.homeHotArray addObject:mol];
        }
        [self.courseTab reloadData];
    } FailureBlock:^(id  _Nonnull error) {
        
    }];
}
#pragma mark--育朵精选
- (void)getYdSelectData {
    self.ydSelectArray = [NSMutableArray array];
    [GMAfnTools PostHttpDataWithUrlStr:KhomeSelected Dic:nil SuccessBlock:^(id  _Nonnull responseObject) {
        for (NSMutableDictionary *dic in responseObject) {
            YDSelectModel *mol = [[YDSelectModel alloc]init];
            [mol mj_setKeyValues:dic];
            [self.ydSelectArray addObject:mol];
            
            NSLog(@"========%@",self.ydSelectArray);
        }
        [self.courseTab reloadData];
    } FailureBlock:^(id  _Nonnull error) {
        
    }];
}
#pragma mark - SDCycleScrollViewDelegate

- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index
{
    NSLog(@"---点击了第%ld张图片", (long)index);
    
    
}
@end
