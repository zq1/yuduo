//
//  CoureSoundViewController.h
//  yuduo
//
//  Created by Mac on 2019/8/19.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CoureSoundViewController : UIViewController

@property (nonatomic, copy) NSString *courseID;
@property (nonatomic, copy) NSString *courseHourID;
/// 显示课程类型(1视频 2音频)
@property (nonatomic, assign) NSInteger type;
/// 跳转z来源(1课程 2专栏)
@property (nonatomic, assign) NSInteger source;
@property (nonatomic, copy) NSString *priceString;

PropertyStrong(NSMutableArray, videoArray);
PropertyStrong(NSString, videoAddressString);//视频播放地址
PropertyStrong(NSString, mp3AddressString);//音频播放地址

@end

NS_ASSUME_NONNULL_END
