//
//  ZLPJViewController.m
//  yuduo
//
//  Created by Mac on 2019/9/19.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "ZLPJViewController.h"
#import "PJTableViewCell.h"
#import "DKSKeyboardView.h"
#import "DKSTextView.h"

#import "KCPJModel.h"
@interface ZLPJViewController ()
<UITableViewDataSource,UITableViewDelegate,DKSKeyboardDelegate>
PropertyStrong(UITableView, pjTableView);
PropertyStrong(DKSKeyboardView, keyView);
PropertyStrong(UITextView, pjTextView);
@property (nonatomic, assign) BOOL vcCanScroll;
@property (nonatomic, assign) BOOL canScroll;
PropertyStrong(NSMutableArray, plArray);

@end

@implementation ZLPJViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getData];
    
    //回复成功  通知评论列表 再次调用
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getCommentListZL) name:@"commentListZL" object:nil];
    NSLog(@"----%@----课程评价ID===%@=-课程评价标题",self.courseIDString,self.coursePJTitleString);
    self.pjTableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.pjTableView.delegate = self;
    self.pjTableView.dataSource = self;
    self.pjTableView.tableFooterView =[UIView new];
    [self.view addSubview:self.pjTableView];
    self.pjTableView.sd_layout.topSpaceToView(self.view, 10)
    .leftSpaceToView(self.view, 0)
    .widthIs(KScreenW)
    .heightIs(KScreenH-100);
    //
    //    self.pjTextView = [[UITextView alloc]init];
    //    self.pjTextView.backgroundColor = GMlightGrayColor;
    //    self.pjTextView.text = @"测试";
    //    [self.view addSubview:self.pjTextView];
    //    self.pjTextView.sd_layout.topSpaceToView(self.pjTableView, 10)
    //    .leftEqualToView(self.view)
    //    .widthIs(KScreenW-100).heightIs(100);
    
    //    UIButton *releaseButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //    [releaseButton addTarget:self action:@selector(releaseClick:) forControlEvents:UIControlEventTouchUpInside];
    //    [releaseButton setTitle:@"发布" forState:UIControlStateNormal];
    //    releaseButton.backgroundColor = RGB(23, 153, 167);
    //    releaseButton.layer.cornerRadius = 22.5;
    //    [self.view addSubview:releaseButton];
    //    releaseButton.sd_layout.topSpaceToView(self.pjTextView, 0)
    //    .rightSpaceToView(self.view, 50)
    //    .widthIs(100).heightIs(50);
    
    //
    //    self.keyView = [[DKSKeyboardView alloc] initWithFrame:CGRectMake(0, KScreenH - 51, KScreenW, 51)];
    //    //设置代理方法
    //    __weak typeof(self) weakSelf = self;
    //    self.keyView.delegate = self;
    //    self.keyView.textBlock = ^(NSString *textString) {
    //        NSLog(@"xxxxxxxxxxX%@",textString);
    //        [weakSelf userReply:textString];
    //    };
    //    [self.view addSubview:self.keyView];
    // Do any additional setup after loading the view.
    
}
- (void)getCommentListZL {
    [self getData];
}
- (void)getData {
    NSUserDefaults *defauls = [NSUserDefaults standardUserDefaults];
    NSString *user_id = [defauls objectForKey:@"user_id"];
    NSString *token = [defauls objectForKey:@"token"];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"user_id"] = user_id;
    dic[@"token"] = token;
    dic[@"type"] = @"2";
    dic[@"content_id"] = @"6";
    self.plArray = [NSMutableArray array];
    
    if (!user_id) {
        NSLog(@"用户未登录");
    } else {
    
    [GMAfnTools PostHttpDataWithUrlStr:KCoursePJList Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
        //        NSLog(@"打印课程详情评价----%@",responseObject);
        for (NSMutableDictionary *dic in responseObject) {
//            NSLog(@"打印专栏----评论--%@",dic);
            for (NSMutableDictionary *comment_list in [dic objectForKey:@"user_comment_lists"]) {
//                                NSLog(@"打印评论内容---%@",comment_list);
                KCPJModel *moldel = [[KCPJModel alloc]init];
                [moldel mj_setKeyValues:comment_list];
                [self.plArray addObject:moldel];
            }
        }
        
        
        //        for (NSMutableDictionary *dic in responseObject) {
        //
        //            KCPJModel *moldel = [[KCPJModel alloc]init];
        //            [moldel mj_setKeyValues:dic];
        //            [self.plArray addObject:moldel];
        //
        //        }
        [self.pjTableView reloadData];
    } FailureBlock:^(id  _Nonnull error) {
        
    }];
}
}
- (void)releaseClick: (UIButton *)send {
    
    NSLog(@"----%@",self.pjTextView.text);
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.0001f;
}

#pragma mark--用户回复
- (void)userReply:(NSString *)testString {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *userReplyDic = [NSMutableDictionary dictionary];
    userReplyDic[@"user_id"] = [defaults objectForKey:@"user_id"];
    userReplyDic[@"token"] = [defaults objectForKey:@"token"];
    userReplyDic[@"type"] = @"1";
    //    userReplyDic[@"manag_id"] = self.zxIDString;
    userReplyDic[@"manag_reply"] = testString;
    userReplyDic[@"manag_picture"] = @"";
    
    [GMAfnTools PostHttpDataWithUrlStr:KUserReply Dic:userReplyDic SuccessBlock:^(id  _Nonnull responseObject) {
        //        NSLog(@"用户回复成功----%@",responseObject);
        if ([[responseObject objectForKey:@"code"] isEqualToString:@"1"]) {
            
            
            [self.pjTableView reloadData];
            
            [self showError:[responseObject objectForKey:@"msg"]];
            
        }
    } FailureBlock:^(id  _Nonnull error) {
        NSLog(@"用户回复失败----%@",error);
    }];
}

#pragma mark -- 提示框
- (void)showError:(NSString *)errorMsg {
    // 1.弹框提醒
    // 初始化对话框
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:errorMsg preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:nil]];
    // 弹出对话框
    [self presentViewController:alert animated:true completion:nil];
    
    [self performSelector:@selector(dimisssAlert:) withObject:alert afterDelay:1];
    
}

- (void) dimisssAlert:(UIAlertController *)alert
{
    if (alert) {
        [alert dismissViewControllerAnimated:YES completion:nil];
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 150;
}

//-(CGFloat)cellHeightForIndexPath:(NSIndexPath *)indexPath cellContentViewWidth:(CGFloat)width tableView:(UITableView *)tableView {
//    return 300;
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.plArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"cellID";
    PJTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[PJTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    KCPJModel *mol = [self.plArray objectAtIndex:indexPath.row];
    cell.timeLabel.text = mol.comments_time;
    cell.nameLabel.text = mol.nikname;
    cell.answerLabel.text = mol.comments;
    [cell.headImg sd_setImageWithURL:[NSURL URLWithString:mol.portrait]];
    return cell;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
