//
//  NRViewController.h
//  yuduo
//
//  Created by Mac on 2019/8/14.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NRViewController : UIViewController
@property (nonatomic,strong)NSMutableArray *dataArray;
@property (nonatomic, assign) NSInteger source;
@property (nonatomic, copy) NSString *priceString;
@end

NS_ASSUME_NONNULL_END
