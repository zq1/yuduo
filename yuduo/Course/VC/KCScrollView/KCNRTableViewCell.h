//
//  KCNRTableViewCell.h
//  yuduo
//
//  Created by Mac on 2019/8/14.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface KCNRTableViewCell : UITableViewCell
PropertyStrong(UIView, bgView);
PropertyStrong(UILabel,headImg);
PropertyStrong(UILabel, sortlabel);

PropertyStrong(UILabel, titleLabel);
PropertyStrong(UILabel, timeLabel);
PropertyStrong(UIImageView, timeImg);
PropertyStrong(UIImageView, seeImg);
PropertyStrong(UILabel, videoLabel);
PropertyStrong(UIImageView, musicImg);
PropertyStrong(UILabel, musLabel);
PropertyStrong(UIImageView, picView);
PropertyStrong(UILabel,picLabel);

PropertyStrong(UILabel, peopleLabel);
PropertyStrong(UIImageView, zanImg);
PropertyStrong(UILabel, dianZanLabel);
@end

NS_ASSUME_NONNULL_END
