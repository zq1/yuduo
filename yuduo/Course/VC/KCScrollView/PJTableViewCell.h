//
//  PJTableViewCell.h
//  yuduo
//
//  Created by Mac on 2019/8/19.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZXHFDetailModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface PJTableViewCell : UITableViewCell

PropertyStrong(UIImageView, headImg);//头像
PropertyStrong(UILabel, nameLabel);//用户昵称
PropertyStrong(UILabel, timeLabel);//时间
PropertyStrong(UILabel, questionLabel);//问题
PropertyStrong(UILabel, answerLabel);//客服回答
PropertyStrong(ZXHFDetailModel, model);

@end

NS_ASSUME_NONNULL_END
