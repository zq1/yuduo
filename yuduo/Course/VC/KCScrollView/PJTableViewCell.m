//
//  PJTableViewCell.m
//  yuduo
//
//  Created by Mac on 2019/8/19.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "PJTableViewCell.h"

@implementation PJTableViewCell

- (void)setModel:(ZXHFDetailModel *)model {
    _model = model;
    self.timeLabel.text = model.manag_reply_time;
    self.nameLabel.text = model.manipulate;
    self.answerLabel.text = model.manag_reply;
    [self.headImg sd_setImageWithURL:[NSURL URLWithString:model.portrait]];
    [self setupAutoHeightWithBottomView:self.answerLabel bottomMargin:15];
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    if (self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier]) {
        [self setLayout];
    }
    return self;
}

- (void)setLayout {
    
    self.headImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@""]];
    self.headImg.backgroundColor = GMlightGrayColor;
    self.headImg.layer.cornerRadius = 20;
    self.headImg.clipsToBounds = YES;
    [self.contentView addSubview:self.headImg];
    self.headImg.sd_layout.topSpaceToView(self.contentView, 20)
    .leftSpaceToView(self.contentView, 15)
    .widthIs(40).heightIs(40);
    self.nameLabel = [[UILabel alloc]init];
    self.nameLabel.text = @"用户昵称";
    self.nameLabel.textColor = RGB(69, 69, 69);
    self.nameLabel.font = [UIFont fontWithName:KPFType size:15];
    [self.contentView addSubview:self.nameLabel];
    self.nameLabel.sd_layout.topSpaceToView(self.contentView, 23)
    .leftSpaceToView(self.headImg, 15)
    .widthIs(80).heightIs(15);
    
    self.timeLabel = [[UILabel alloc]init];
    self.timeLabel.text = @"2019.05.10  10:00:00";
    self.timeLabel.font = [UIFont fontWithName:KPFType size:12];
    self.timeLabel.textColor = RGB(153, 153, 153);
    [self.contentView addSubview:self.timeLabel];
    self.timeLabel.sd_layout.topSpaceToView(self.nameLabel, 10)
    .leftSpaceToView(self.headImg, 15)
    .widthIs(160).heightIs(10);
    
    //问题
    self.answerLabel = [[UILabel alloc]init];
    [self.contentView addSubview:self.answerLabel];
    self.answerLabel.numberOfLines = 0;
    self.answerLabel.textColor = RGB(69, 69, 69);
    self.answerLabel.font = [UIFont fontWithName:KPFType size:14];
    self.answerLabel.textColor = [UIColor blackColor];
//    self.answerLabel.backgroundColor = GMRedColor;
    self.answerLabel.text = @"课程很好，在对过敏性鼻炎，哮喘提前停药吗？";
    CGSize size = [self.answerLabel sizeThatFits:CGSizeMake(self.answerLabel.frame.size.width, MAXFLOAT)];
    self.answerLabel.sd_layout.topSpaceToView(self.timeLabel, 15).leftEqualToView(self.timeLabel).rightSpaceToView(self.contentView, 15).autoHeightRatio(0);
    
//    //客服回答
//    self.questionLabel = [[UILabel alloc]initWithFrame:CGRectMake(71,self.answerLabel.frame.size.height+50, KScreenW-96, 60)];
//    self.questionLabel.numberOfLines = 0;
//    self.questionLabel.textColor = [UIColor blackColor];
//    self.questionLabel.backgroundColor = RGB(245, 245, 245);
//    self.questionLabel.text = @"第三方噶抗裂砂浆阿拉山口大姐夫阿斯蒂芬阿斯山口大姐夫阿斯蒂芬阿斯蒂芬阿斯蒂芬阿斯蒂芬阿斯蒂芬按时阿斯蒂芬按时 是的发送到发斯蒂芬阿斯";
//    CGSize sizeQuestion = [self.questionLabel sizeThatFits:CGSizeMake(self.questionLabel.frame.size.width, MAXFLOAT)];
//    self.questionLabel.frame = CGRectMake(self.questionLabel.frame.origin.x, self.questionLabel.frame.origin.y, self.questionLabel.frame.size.width, sizeQuestion.height);
//    [self.contentView addSubview:self.questionLabel];
//    NSMutableAttributedString *questionString = [[NSMutableAttributedString alloc] initWithString:self.questionLabel.text attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFang SC" size: 14],NSForegroundColorAttributeName: [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1.0]}];
//    self.questionLabel.attributedText = questionString;
    //客服回答
    //    self.questionLabel = [[UILabel alloc]init];
    //    [self.contentView addSubview:self.questionLabel];
    
    
}

- (CGFloat)getStringHeightWithText:(NSString *)text font:(UIFont *)font viewWidth:(CGFloat)width {
    // 设置文字属性 要和label的一致
    NSDictionary *attrs = @{NSFontAttributeName :font};
    CGSize maxSize = CGSizeMake(width, MAXFLOAT);

    NSStringDrawingOptions options = NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading;

    // 计算文字占据的宽高
    CGSize size = [text boundingRectWithSize:maxSize options:options attributes:attrs context:nil].size;

   // 当你是把获得的高度来布局控件的View的高度的时候.size转化为ceilf(size.height)。
    return  ceilf(size.height);
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
