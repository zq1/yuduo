//
//  PJViewController.h
//  yuduo
//
//  Created by Mac on 2019/8/14.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PJViewController : UIViewController
@property (nonatomic, copy)void(^textBlock)(NSString  *textString);
PropertyStrong(NSString, coursePJTitleString);
PropertyStrong(NSString, courseIDString);
@end

NS_ASSUME_NONNULL_END
