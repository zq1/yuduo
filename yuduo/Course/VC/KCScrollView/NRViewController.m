//
//  NRViewController.m
//  yuduo
//
//  Created by Mac on 2019/8/14.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "NRViewController.h"
#import "KCNRTableViewCell.h"
#import "CoureSoundViewController.h"
#import "CourseModel.h"
@interface NRViewController ()<UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate>

@property (nonatomic, strong) UITableView *coureNR;

@end

@implementation NRViewController

- (void)viewDidLoad {
    [super viewDidLoad];
  
 
    
    self.coureNR = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.coureNR.tableFooterView = [UIView new];
    self.coureNR.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.coureNR.delegate = self;
    self.coureNR.dataSource = self;
    [self.view addSubview:self.coureNR];
    self.coureNR.sd_layout.topSpaceToView(self.view, 0)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .widthIs(KScreenW).heightIs(400);
    
    // Do any additional setup after loading the view.
}
//专栏视频播放地址
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CoureSoundViewController *soundVC = [[CoureSoundViewController alloc]init];
    CourseModel *courseModel = [self.dataArray objectAtIndex:indexPath.row];
    soundVC.type = courseModel.course_hour_type.integerValue;
    soundVC.courseID = courseModel.course_id;
    soundVC.courseHourID = courseModel.course_hour_id;
    soundVC.source = self.source;
    soundVC.priceString = self.priceString;
    soundVC.videoAddressString = courseModel.course_hour_video;
    [self.navigationController pushViewController:soundVC animated:YES];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSLog(@"----%ld",indexPath.row);
    
}


-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    if (gestureRecognizer.state != 0) {
        return YES;
    } else {
        return NO;
    }
}

- (void)setDataArray:(NSMutableArray *)dataArray {
    _dataArray = dataArray;
    [self.coureNR reloadData];
}
#pragma mark--返回行高
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 90;
}
#pragma mark--返回行数
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *key0 = @"cell0";
    KCNRTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:key0];
    if (!cell) {
        cell = [[KCNRTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:key0];
    }
   
    cell.headImg.text = [@"0"stringByAppendingString:[NSString stringWithFormat:@"%ld",indexPath.row]];
    CourseModel *courseModel = [self.dataArray objectAtIndex:indexPath.row];
    cell.titleLabel.text = courseModel.course_hour_name;
    cell.timeLabel.text = courseModel.course_hour_time;
    
    cell.seeImg.hidden = YES;
    cell.videoLabel.hidden = YES;
    
    if ([courseModel.course_hour_type isEqualToString:@"1"]) {
        // 视频
        [cell.musicImg setImage:[UIImage imageNamed:@"视频"]];
        cell.musLabel.text = @"视频";
    } else {
        // 音频
        [cell.musicImg setImage:[UIImage imageNamed:@"矢量智能对象"]];
        cell.musLabel.text = @"音频";
    }
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
