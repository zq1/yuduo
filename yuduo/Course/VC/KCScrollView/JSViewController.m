//
//  JSViewController.m
//  yuduo
//
//  Created by Mac on 2019/8/14.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "JSViewController.h"

@interface JSViewController () <UIScrollViewDelegate, UIWebViewDelegate, UITextViewDelegate>

@property (nonatomic, strong) UITextView *textView;

@end

@implementation JSViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    UIScrollView *scrollerView = [[UIScrollView alloc] init];
//    [self.view addSubview:scrollerView];
//
//
//
//    UILabel *textLabel = [[UILabel alloc] init];
//    textLabel.sd_layout.topEqualToView(self.view).leftEqualToView(self.view).rightEqualToView(self.view);
    
    self.textView = [[UITextView alloc] init];
    self.textView.editable = NO;
    self.textView.delegate = self;
    self.textView.dataDetectorTypes = UIDataDetectorTypeNone;
    [self.view addSubview:self.textView];
    
    self.textView.sd_layout.topEqualToView(self.view)
    .leftSpaceToView(self.view, 20)
    .rightEqualToView(self.view)
    .bottomSpaceToView(self.view, 20);
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    return NO;
}

- (BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange interaction:(UITextItemInteraction)interaction {
    return NO;
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGPoint point = scrollView.contentOffset;
    if (point.x > 0) {
        scrollView.contentOffset = CGPointMake(0, point.y);//这里不要设置为CGPointMake(0, 0)，这样我们在文章下面左右滑动的时候，就跳到文章的起始位置，不科学
    }
}

- (void)setText:(NSString *)text {
    _text = text;
    
    NSMutableAttributedString *attText = [[NSMutableAttributedString alloc] initWithData:[text dataUsingEncoding:NSUnicodeStringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes: nil error:nil];
    dispatch_async(dispatch_get_main_queue(), ^{
        self.textView.attributedText = attText;
    });
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
