//
//  ZLPJViewController.h
//  yuduo
//
//  Created by Mac on 2019/9/19.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZLPJViewController : UIViewController
@property (nonatomic, copy)void(^textBlock)(NSString  *textString);
PropertyStrong(NSString, coursePJTitleString);
PropertyStrong(NSString, courseIDString);
@end

NS_ASSUME_NONNULL_END
