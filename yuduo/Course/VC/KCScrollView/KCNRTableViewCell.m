//
//  KCNRTableViewCell.m
//  yuduo
//
//  Created by Mac on 2019/8/14.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "KCNRTableViewCell.h"

@implementation KCNRTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier]) {
        
        [self createLayout];
    }
    return self;
}

- (void)createLayout {
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = GMWhiteColor;
    self.bgView.layer.cornerRadius = 10;
    self.bgView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.bgView.layer.shadowOffset = CGSizeMake(0, 0);
    self.bgView.layer.shadowOpacity = 0.1;
    self.bgView.layer.shadowRadius = 3.0;
    self.bgView.userInteractionEnabled = YES;
    [self.contentView addSubview:self.bgView];
    self.bgView.sd_layout.topSpaceToView(self.contentView, 0)
    .leftSpaceToView(self.contentView, 15*kGMWidthScale)
    .rightSpaceToView(self.contentView, 15*kGMWidthScale)
    .widthIs(KScreenW-30*kGMWidthScale).heightIs(80);
    
    self.headImg = [[UILabel alloc]init];
    self.headImg.backgroundColor = RGB(21, 152, 164);
    self.headImg.text = @"asdfasdf";
    self.headImg.textColor = GMWhiteColor;
    self.headImg.textAlignment = NSTextAlignmentCenter;
    self.headImg.font = [UIFont fontWithName:KPFType size:15];
    
    self.headImg.sd_cornerRadius = [NSNumber numberWithInteger:16];
    [self.bgView addSubview:self.headImg];
    self.headImg.sd_layout.topSpaceToView(self.bgView, 24)
    .leftSpaceToView(self.bgView, 13*kGMWidthScale)
    .widthIs(32).heightEqualToWidth();
    
    self.titleLabel = [[UILabel alloc]init];
    self.titleLabel.text = @"一切言动都要安详十差九错只为慌张";
    self.titleLabel.font = [UIFont fontWithName:KPFType size:15];
    self.titleLabel.textColor = RGB(69, 69, 69);
    [self.bgView addSubview:self.titleLabel];
    self.titleLabel.sd_layout.topSpaceToView(self.bgView, 21)
    .leftSpaceToView(self.headImg, 14)
    .widthIs(260).heightIs(15);

        self.timeImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ico_time"]];
        [self.bgView addSubview:self.timeImg];
        self.timeImg.sd_layout.bottomSpaceToView(self.bgView, 22)
        .leftSpaceToView(self.headImg, 14*kGMWidthScale)
        .widthIs(13).heightIs(13);
    
    self.timeLabel = [[UILabel alloc]init];
    self.timeLabel.text = @"05:05:05";
    self.timeLabel.textColor = RGB(153, 153, 153);
    self.timeLabel.font = [UIFont fontWithName:KPFType size:12];
    [self.bgView addSubview:self.timeLabel];
    self.timeLabel.sd_layout.bottomSpaceToView(self.bgView, 23)
    .leftSpaceToView(self.timeImg, 6*kGMWidthScale)
    .widthIs(50).heightIs(9);
    
    self.seeImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"视频"]];
    [self.bgView addSubview:self.seeImg];
    self.seeImg.sd_layout.bottomSpaceToView(self.bgView, 21)
    .leftSpaceToView(self.timeLabel, 11*kGMWidthScale)
    .widthIs(15).heightIs(12);
    
    self.videoLabel = [[UILabel alloc]init];
    self.videoLabel.text = @"视频";
    self.videoLabel.textColor = RGB(153, 153, 153);
    self.videoLabel.font = [UIFont fontWithName:KPFType size:12];
    [self.bgView addSubview:self.videoLabel];
    self.videoLabel.sd_layout.bottomSpaceToView(self.bgView, 21)
    .leftSpaceToView(self.seeImg, 9*kGMWidthScale)
    .widthIs(24).heightIs(12);
    
    self.musicImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"矢量智能对象"]];
    [self.bgView addSubview:self.musicImg];
    self.musicImg.sd_layout.bottomSpaceToView(self.bgView, 20)
    .leftSpaceToView(self.videoLabel, 19*kGMWidthScale)
    .widthIs(12).heightIs(15);
    
    self.musLabel = [[UILabel alloc]init];
    self.musLabel.text = @"音频";
    self.musLabel.textColor = RGB(153, 153, 153);
    self.musLabel.font = [UIFont fontWithName:KPFType size:12];
    [self.bgView addSubview:self.musLabel];
    self.musLabel.sd_layout.bottomSpaceToView(self.bgView, 21)
    .leftSpaceToView(self.musicImg, 7*kGMWidthScale)
    .widthIs(26).heightIs(12);

    self.picView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"tuwen"]];
    [self.bgView addSubview:self.picView];
    self.picView.sd_layout.bottomSpaceToView(self.bgView, 21)
    .leftSpaceToView(self.musLabel, 17*kGMWidthScale)
    .widthIs(14).heightIs(15);
    
    self.picLabel = [[UILabel alloc]init];
    self.picLabel.text = @"图文";
    self.picLabel.textColor = RGB(153, 153, 153);
    self.picLabel.font = [UIFont fontWithName:KPFType size:12];
    [self.bgView addSubview:self.picLabel];
    self.picLabel.sd_layout.bottomSpaceToView(self.bgView, 22)
    .leftSpaceToView(self.picView, 5*kGMWidthScale)
    .widthIs(25).heightIs(12);

}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
