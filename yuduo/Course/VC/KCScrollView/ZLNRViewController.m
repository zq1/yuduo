//
//  ZLNRViewController.m
//  yuduo
//
//  Created by Mac on 2019/9/19.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "ZLNRViewController.h"
#import "KCNRTableViewCell.h"
#import "CoureSoundViewController.h"
#import "ColumnZLneiRong.h"
@interface ZLNRViewController ()<UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate>

@end

@implementation ZLNRViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"xxx打印专栏内容 播放列表==%@",self.dataArray);
    
    UITableView *coureNR = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    coureNR.delegate = self;
    coureNR.dataSource = self;
    coureNR.tableFooterView = [UIView new];
    [self.view addSubview:coureNR];
    coureNR.sd_layout.topSpaceToView(self.view, 0)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .widthIs(KScreenW).heightIs(KScreenH);
    
    // Do any additional setup after loading the view.
}
//专栏视频播放地址
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CoureSoundViewController *soundVC = [[CoureSoundViewController alloc]init];
     ColumnZLneiRong *zlModel = [self.dataArray objectAtIndex:indexPath.row];
    soundVC.type = zlModel.course_hour_type.integerValue;
    soundVC.courseID = zlModel.course_id;
    soundVC.courseHourID = zlModel.course_hour_id;
    soundVC.source = 2;
    soundVC.videoAddressString = zlModel.course_hour_video;
    [self.navigationController pushViewController:soundVC animated:YES];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSLog(@"----%ld",indexPath.row);
    
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    if (gestureRecognizer.state != 0) {
        return YES;
    } else {
        return NO;
    }
}

#pragma mark--返回行高
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 90;
}
#pragma mark--返回行数
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *key0 = @"cell0";
    KCNRTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:key0];
    if (!cell) {
        cell = [[KCNRTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:key0];
    }
    
    cell.headImg.text = [@"0"stringByAppendingString:[NSString stringWithFormat:@"%ld",indexPath.row]];
    
    //    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    return cell;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
