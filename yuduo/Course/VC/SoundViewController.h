//
//  SoundViewController.h
//  yuduo
//
//  Created by Mac on 2019/8/19.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CourseVideoModel;
@class CourseVideoOtherModel;
NS_ASSUME_NONNULL_BEGIN

@interface SoundViewController : UIViewController

@property (nonatomic, strong) CourseVideoModel *model;
@property (nonatomic, copy) NSArray<CourseVideoOtherModel *> *list;

PropertyStrong(NSMutableArray, mp3Array);
PropertyStrong(NSString, mp3String);
@end

NS_ASSUME_NONNULL_END
