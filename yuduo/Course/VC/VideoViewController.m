//
//  VideoViewController.m
//  yuduo
//
//  Created by Mac on 2019/8/19.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "VideoViewController.h"
#import "KCNRTableViewCell.h"
#import "CustomPlayerView.h"

#import "CourseVideoModel.h"
#import "CourseVideoOtherModel.h"
#import "CourseVideoTuwenModel.h"

@interface VideoViewController ()
<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UIView *videoView;
@property (nonatomic, strong) UITableView *coureNR;
@property (nonatomic, copy) NSArray<CourseVideoOtherModel *> *otherList;

@end

@implementation VideoViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = RGB(244, 243, 244);
    self.videoView = [[UIView alloc]init];
    self.videoView.backgroundColor = GMlightGrayColor;
    [self.view addSubview:self.videoView];

    self.videoView.sd_layout.topSpaceToView(self.view, 88+MStatusBarHeight)
    .widthIs(KScreenW)
    .heightIs((MStatusBarHeight+199)*kGMHeightScale);
    
    
//    //加载MP4
//    UIWebView *web = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, KScreenW, videoView.frame.size.height)];
//    [web loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.videoString]]];
//    [videoView addSubview:web];

    self.player = [[CustomPlayerView alloc] init];
    [self.videoView addSubview:self.player];
    self.player.sd_layout.leftEqualToView(self.videoView)
    .rightEqualToView(self.videoView)
    .topEqualToView(self.videoView)
    .bottomEqualToView(self.videoView);
    
//    self.player = [[AliPlayer alloc] init];
//    self.player.playerView = videoView;
//    self.player.delegate = self;
//    self.player.scalingMode = AVP_SCALINGMODE_SCALETOFILL;
//    self.player.rate = 1.0;
//    [self.player setAutoPlay:YES];

//    AVPUrlSource *source = [[AVPUrlSource alloc] init];
////    NSString *filePath = [[NSBundle mainBundle]pathForResource:@"abc.mp4" ofType:nil];
////    NSURL *url = [NSURL URLWithString:filePath];
////    [source fileURLWithPath:filePath];
//    [source urlWithString:self.videoString];
//    [self.player setUrlSource:source];
//    AVPConfig *config = [self.player getConfig];
////    if (url.absoluteString.length > 4 && [[url.absoluteString substringToIndex:4] isEqualToString:@"artp"]) {
////        config.maxDelayTime = 100;
////    }else {
////        config.maxDelayTime = 5000;
////    }
//    [self.player setConfig:config];
//    [self.player prepare];
////    player.playerView = videoView;
//    [self.player start];
    
    self.coureNR = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.coureNR.delegate = self;
    self.coureNR.dataSource = self;
    self.coureNR.tableHeaderView = self.videoView;
    self.coureNR.tableFooterView = [UIView new];
    [self.view addSubview:self.coureNR];
    self.coureNR.sd_layout.topSpaceToView(self.view, 10)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .widthIs(KScreenW).heightIs(KScreenH-MStatusBarHeight-44-44-10-48);
    [self.coureNR reloadData];
    
    UIView *payView = [[UIView alloc]init];
    payView.backgroundColor = GMWhiteColor;
    [self.view addSubview:payView];
    payView.sd_layout.bottomSpaceToView(self.view,MStatusBarHeight+28)
    .leftSpaceToView(self.view, 0)
    .widthIs(KScreenW).heightIs(48);
    
    UIButton *vipLogo = [UIButton buttonWithType:UIButtonTypeCustom];
    [vipLogo setBackgroundImage:[UIImage imageNamed:@"huiyuan"] forState:UIControlStateNormal];
    [payView addSubview:vipLogo];
    vipLogo.sd_layout.topSpaceToView(payView, 10)
    .leftSpaceToView(payView, 33)
    .widthIs(16).heightIs(14);
    
    //成为会员
    UIButton *viperButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [viperButton setTitleColor:RGB(21, 152, 164) forState:UIControlStateNormal];
    [viperButton setTitle:@"成为会员" forState:UIControlStateNormal];
    
    viperButton.titleLabel.font = kFont(14);
    [payView addSubview:viperButton];
    viperButton.sd_layout.bottomSpaceToView(payView, 9)
    .leftSpaceToView(payView, 10)
    .widthIs(58).heightIs(11);
    // Do any additional setup after loading the view.
    
    UILabel *priceLabel = [[UILabel alloc]init];
//    priceLabel.text = @"¥196.00";
    priceLabel.textColor = [UIColor colorWithRed:228/255.0 green:102/255.0 blue:67/255.0 alpha:1.0];
    priceLabel.font = [UIFont fontWithName:@"PingFang SC" size: 16];
    priceLabel.text = self.priceString;
    [payView addSubview:priceLabel];
    priceLabel.sd_layout.topSpaceToView(payView, 19)
    .leftSpaceToView(viperButton, 20*kGMWidthScale)
    .widthIs(60).heightIs(13);
//    NSMutableAttributedString *pricelString = [[NSMutableAttributedString alloc] initWithString:priceLabel.text attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFang SC" size: 16],NSForegroundColorAttributeName: [UIColor colorWithRed:228/255.0 green:102/255.0 blue:67/255.0 alpha:1.0]}];
//    priceLabel.attributedText = pricelString;
    
    //成为为朋友购买
    UIButton *friendPay = [UIButton buttonWithType:UIButtonTypeCustom];
    [friendPay setTitleColor:GMWhiteColor forState:UIControlStateNormal];
    friendPay.backgroundColor = RGB(252,171,71);
    friendPay.layer.cornerRadius = 14.5;
    [friendPay setTitle:@"为朋友购买" forState:UIControlStateNormal];
    friendPay.titleLabel.font = kFont(13);
    [payView addSubview:friendPay];
    friendPay.sd_layout.topSpaceToView(payView, 10)
    .leftSpaceToView(priceLabel,17*kGMWidthScale)
    .widthIs(90*kGMWidthScale).heightIs(29);
    
    //立刻购买
    UIButton *buyButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [buyButton setTitleColor:GMWhiteColor forState:UIControlStateNormal];
    [buyButton setTitle:@"立刻购买" forState:UIControlStateNormal];
    buyButton.backgroundColor = RGB(228, 102, 67);
    buyButton.layer.cornerRadius = 14.5;
    buyButton.titleLabel.font = kFont(14);
    [payView addSubview:buyButton];
    buyButton.sd_layout.topSpaceToView(payView, 10)
    .leftSpaceToView(friendPay,10*kGMWidthScale)
    .widthIs(90*kGMWidthScale).heightIs(29);
    
}

- (void)setList:(NSArray<CourseVideoOtherModel *> *)list {
    _list = list;
    self.coureNR.tableHeaderView = nil;
    self.otherList = list;
    [self.coureNR reloadData];
    [self.player stop];
}

- (void)setModel:(CourseVideoModel *)model {
    _model = model;
    self.coureNR.tableHeaderView = self.videoView;
    self.otherList = model.other;
    self.player.url = model.course_hour_video;
    self.player.isVideo = [model.course_hour_type isEqualToString:@"1"];
    if ([model.course_hour_type isEqualToString:@"2"]) {
        [self.player setThumbnailUrl:model.audio_picture];
    }
    [self.coureNR reloadData];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSLog(@"----%ld",indexPath.row);
    if (self.delegate && [self.delegate respondsToSelector:@selector(playOther:)]) {
        [self.delegate playOther:self.otherList[indexPath.row]];
    }
}
#pragma mark--返回行高
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 90;
}
#pragma mark--返回行数
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.otherList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *key0=@"cell0";
    KCNRTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:key0];
    if (!cell) {
        cell = [[KCNRTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:key0];
        
    }
    cell.seeImg.hidden = YES;
    cell.videoLabel.hidden = YES;
    if ([self.otherList[indexPath.row].course_hour_type isEqualToString:@"1"]) {
        // 视频
        [cell.musicImg setImage:[UIImage imageNamed:@"视频"]];
        cell.musLabel.text = @"视频";
    } else {
        // 音频
        [cell.musicImg setImage:[UIImage imageNamed:@"矢量智能对象"]];
        cell.musLabel.text = @"音频";
    }
    cell.headImg.text = [NSString stringWithFormat:@"%02ld", (long)indexPath.row];
    cell.titleLabel.text = self.otherList[indexPath.row].course_hour_name;
    //    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (void)dealloc {
    _player = nil;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
