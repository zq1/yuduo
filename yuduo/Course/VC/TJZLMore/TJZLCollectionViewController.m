//
//  TJZLCollectionViewController.m
//  yuduo
//
//  Created by mason on 2019/8/19.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "TJZLCollectionViewController.h"
#import "NewColumnsCollectionViewCell.h"
@interface TJZLCollectionViewController ()
<UICollectionViewDelegate,UICollectionViewDataSource>
@end

@implementation TJZLCollectionViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    [self createCollectionVC];
    self.view.backgroundColor = RGB(244, 243, 244);
    // Do any additional setup after loading the view.
}
- (void)createCollectionVC {
    self.flowLayout = [[UICollectionViewFlowLayout alloc]init];
    self.flowLayout.itemSize = CGSizeMake((self.view.frame.size.width-30)/2-5, 225);
    self.flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    //    self.flowLayout.minimumLineSpacing = 10;
    //    self.flowLayout.minimumInteritemSpacing = 10;
    self.tableView = [[UICollectionView alloc]initWithFrame:CGRectMake(15, 0, self.view.frame.size.width-30, KScreenH-MStatusBarHeight-25-64) collectionViewLayout:self.flowLayout];
    self.tableView.backgroundColor = GMWhiteColor;
    [self.view addSubview:self.tableView];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView registerClass:[NewColumnsCollectionViewCell class] forCellWithReuseIdentifier:@"myCell"];
}

#pragma mark -- collectionView delegate , datasource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 20;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NewColumnsCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"myCell" forIndexPath:indexPath];
    
    cell.contentView.layer.cornerRadius = 5.0f;
    cell.contentView.layer.borderWidth = 0.08f;
    cell.contentView.layer.borderColor = [UIColor blackColor].CGColor; cell.contentView.layer.masksToBounds =YES;
    
    //    self.index = indexPath.row;
    if (self.index == 0) {
        cell.titleLabel.text = @"测试11111";
    }
    if (self.index == 1) {
        cell.titleLabel.text = @"测试22222";
    }
    if (self.index == 2) {
        cell.titleLabel.text = @"测试33333";
    }
    return cell;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"%ld %ld",indexPath.section,indexPath.row);
    
    //    CoureDetailViewController *coureDetailVC = [[CoureDetailViewController alloc]init];
    //    [coureDetailVC setHidesBottomBarWhenPushed:YES];
    //    [self.navigationController pushViewController:coureDetailVC animated:YES];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
