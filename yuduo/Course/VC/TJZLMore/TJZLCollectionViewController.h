//
//  TJZLCollectionViewController.h
//  yuduo
//
//  Created by mason on 2019/8/19.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TJZLCollectionViewController : UIViewController
@property (nonatomic, assign) NSInteger index;
@property (nonatomic, strong) UICollectionView *tableView;
//@property (strong,nonatomic)UICollectionView *collectionV;
@property (strong,nonatomic)UICollectionViewFlowLayout *flowLayout;
@end

NS_ASSUME_NONNULL_END
