//
//  CourseViewController.h
//  yuduo
//
//  Created by Mac on 2019/7/23.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

/// TabBar课程
@interface CourseViewController : BaseViewController

@property (strong,nonatomic)UICollectionView *collectionV;
@property (strong,nonatomic)UICollectionViewFlowLayout *flowLayout;
@end

NS_ASSUME_NONNULL_END
