//
//  ListCouViewController.m
//  yuduo
//
//  Created by Mac on 2019/8/12.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//
//一、====提示样式====
//@property (assign, nonatomic) SVProgressHUDStyle defaultStyle
//SVProgressHUDStyleLight  //默认  背景为白色
//SVProgressHUDStyleDark    //背景为 黑色
//SVProgressHUDStyleCustom   //自定义
//设置显示样式：
//[SVProgressHUD setDefaultStyle:SVProgressHUDStyleLight];
//二、===提示view背景颜色(自定义模式下才生效)====
//提示view的背景颜色必须要在现实样式为“自定义”的情况下才能生效，
//也就是：
//[SVProgressHUD setDefaultStyle:SVProgressHUDStyleCustom];
//1.view的背景颜色
//[SVProgressHUD setBackgroundColor:[UIColor orangeColor]];
//2.view上面的旋转小图标的 颜色
//[SVProgressHUD setForegroundColor:[UIColor blueColor]];
//三、====是否添加遮罩====
//遮罩：在请求数据，却还没完成时，用一个另外view来把显示界面盖住
//@property (assign, nonatomic) SVProgressHUDMaskType defaultMaskType
//SVProgressHUDMaskTypeNone = 1,  // 默认 没有遮罩
//SVProgressHUDMaskTypeClear,   //透明
//SVProgressHUDMaskTypeBlack,     //黑色
//SVProgressHUDMaskTypeGradient,  //光斑效果/聚光
//SVProgressHUDMaskTypeCustom   //自定义类型
//代码:
//[SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
//其实遮罩一样可以定制
//注意:遮罩的颜色  设置遮罩类型SVProgressHUDMaskTypeCustom   才生效
//[SVProgressHUD setBackgroundLayerColor:[UIColor yellowColor]];
//四、动画样式 (旋转的小图标的样式)
//@property (assign, nonatomic) SVProgressHUDAnimationType defaultAnimationType
//SVProgressHUDAnimationTypeFlat,   //默认  圆圈
//SVProgressHUDAnimationTypeNative  //菊花
//显示时间
//@property (assign, nonatomic) NSTimeInterval minimumDismissTimeInterval;
//默认为5秒
//[SVProgressHUD setMinimumDismissTimeInterval:1.0];   //设为1妙
#import "ListCouViewController.h"
#import "NewCourseTwoTableViewCell.h"
#import "CourseListViewController.h"
#import "testModel.h"
#import "CourseCategory_idModel.h"
#import <SVProgressHUD.h>
#import "CoureDetailViewController.h"

@interface ListCouViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) NSMutableArray *respoceArray;
PropertyStrong(NSMutableArray, courseArray);
PropertyStrong(NSMutableArray, hotCourseArray);
PropertyStrong(NSMutableArray, mianfeiArray);
PropertyStrong(NSMutableArray, vipMianfeiArray);
@end

@implementation ListCouViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
//    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(100, 0, 200, 80)];
//    view.backgroundColor = GMBlueColor;
//    [self.view addSubview:view];
//    [self getCategory];
    //最新课程
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(HomeNewcourse2) name:@"HomeNewcourse" object:nil];
    //最热课程
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(HomeHotCourse2) name:@"HomeHotCourse" object:nil];
    //免费课程
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(MianfeiCourse2) name:@"mianfeiCourse" object:nil];
    //vip限免
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(vipMianfei2) name:@"vipMianfei" object:nil];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 10, KScreenW, KScreenH-48-MStatusBarHeight-44) style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [UIView new];
    [self.view addSubview:self.tableView];
    [self getData:@"1"];
}

- (void)HomeNewcourse2 {
    [self getData:@"1"];
}

- (void)HomeHotCourse2 {
    [self getData:@"2"];
}

- (void)MianfeiCourse2 {
    [self getData:@"3"];
}

- (void)vipMianfei2 {
    [self getData:@"4"];
}

- (void)getData:(NSString *)screen {
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    if (!self.isAll) {
        param[@"category_id"] = self.categoryID;
    }
    param[@"screen"] = screen;
    
    [GMAfnTools PostHttpDataWithUrlStr:KCourseList Dic:param SuccessBlock:^(id  _Nonnull responseObject) {
        self.respoceArray = [NSMutableArray array];
        for (NSMutableDictionary *dic in responseObject) {
            testModel *mol = [[testModel alloc]init];
            [mol mj_setKeyValues:dic];
            [self.respoceArray addObject:mol];
        }
        [self.tableView reloadData];
    } FailureBlock:^(id  _Nonnull error) {
        
    }];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 156;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    if (self.index == 0) {
//        if (self.respoceArray) {
//            return self.respoceArray.count;
//        }
//
//    }
//
//
//    return 0;
    return self.respoceArray.count;
}
//- (void)getCategory {
//    self.courseArray = [NSMutableArray array];
//    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
//    dic[@"pid"] = @"1";
//    [GMAfnTools PostHttpDataWithUrlStr:KCategoryID Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
//        NSLog(@"打印课程-----%@",responseObject);
//        for (NSMutableDictionary *idDic in responseObject) {
//            CourseCategory_idModel *model = [[CourseCategory_idModel alloc]init];
//            [model mj_setKeyValues:idDic];
//            [self.courseArray addObject:model];
//        }
//
//    } FailureBlock:^(id  _Nonnull error) {
//
//    }];
//}
/*-----------++++++++++++++VIP课程----------------------*/
#pragma mark--VIP免费
- (void)vipMianfei {
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD show];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // 延时事件
        //        [self NewCourseOnde];
        self.vipMianfeiArray = [NSMutableArray array];
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        dic[@"pid"] = @"1";
        __weak typeof(self ) weakSelf = self;
        [GMAfnTools PostHttpDataWithUrlStr:KCategoryID Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
            if (responseObject) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    for (NSMutableDictionary *idDic in responseObject) {
                        CourseCategory_idModel *model = [[CourseCategory_idModel alloc]init];
                        [model mj_setKeyValues:idDic];
                        [self.vipMianfeiArray addObject:model];
                        [weakSelf vipCourse:model.category_id];
                    }
                    [SVProgressHUD dismiss];
                });
                NSLog(@"打印课程-----%@",responseObject);
            }else {
                //加载失败隐藏
            }
        }
                              FailureBlock:^(id  _Nonnull error) {
                                  [SVProgressHUD setStatus:@"加载失败..."];
                                  [SVProgressHUD dismissWithDelay:10];
                              }];
    });
}
#pragma mark--VIP免费
- (void)vipCourse:(NSString *)string {
    self.respoceArray = [NSMutableArray array];
    NSMutableDictionary *newCourseDic = [NSMutableDictionary dictionary];
    
    newCourseDic[@"category_id"] = string;
    newCourseDic[@"screen"] = @"4";
    [GMAfnTools PostHttpDataWithUrlStr:KCourseList Dic:newCourseDic SuccessBlock:^(id  _Nonnull responseObject) {
        for (NSMutableDictionary *dic in responseObject) {
            testModel *mol = [[testModel alloc]init];
            [mol mj_setKeyValues:dic];
            [self.respoceArray addObject:mol];
        }
        [self.tableView reloadData];
        
    }
                          FailureBlock:^(id  _Nonnull error) {
                          }];
    //    [self.tableView reloadData];
}
/*-----------++++++++++++++免费课程----------------------*/
#pragma mark--免费课程
- (void)MianfeiCourse {
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD show];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // 延时事件
        //        [self NewCourseOnde];
        self.mianfeiArray = [NSMutableArray array];
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        dic[@"pid"] = @"1";
        __weak typeof(self ) weakSelf = self;
        [GMAfnTools PostHttpDataWithUrlStr:KCategoryID Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
            if (responseObject) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    for (NSMutableDictionary *idDic in responseObject) {
                        CourseCategory_idModel *model = [[CourseCategory_idModel alloc]init];
                        [model mj_setKeyValues:idDic];
                        [self.mianfeiArray addObject:model];
                        [weakSelf mianfeiCourse:model.category_id];
                    }
                    [SVProgressHUD dismiss];
                });
                NSLog(@"打印课程-----%@",responseObject);
            }else {
                //加载失败隐藏
            }
        }
                              FailureBlock:^(id  _Nonnull error) {
                                  [SVProgressHUD setStatus:@"加载失败..."];
                                  [SVProgressHUD dismissWithDelay:10];
                              }];
    });
}
#pragma mark--免费课程
- (void)mianfeiCourse:(NSString *)string {
    self.respoceArray = [NSMutableArray array];
    NSMutableDictionary *newCourseDic = [NSMutableDictionary dictionary];
    
    newCourseDic[@"category_id"] = string;
    newCourseDic[@"screen"] = @"3";
    [GMAfnTools PostHttpDataWithUrlStr:KCourseList Dic:newCourseDic SuccessBlock:^(id  _Nonnull responseObject) {
        for (NSMutableDictionary *dic in responseObject) {
            testModel *mol = [[testModel alloc]init];
            [mol mj_setKeyValues:dic];
            [self.respoceArray addObject:mol];
        }
        [self.tableView reloadData];
        
    }
                          FailureBlock:^(id  _Nonnull error) {
                          }];
    //    [self.tableView reloadData];
}
/*----------------最热课程------------------------*/
#pragma mark--最热课程
- (void)HomeHotCourse {
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD show];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // 延时事件
//        [self NewCourseOnde];
        self.courseArray = [NSMutableArray array];
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        dic[@"pid"] = @"1";
        __weak typeof(self ) weakSelf = self;
        [GMAfnTools PostHttpDataWithUrlStr:KCategoryID Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
            if (responseObject) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    for (NSMutableDictionary *idDic in responseObject) {
                        CourseCategory_idModel *model = [[CourseCategory_idModel alloc]init];
                        [model mj_setKeyValues:idDic];
                        [self.courseArray addObject:model];
                        [weakSelf hotCourse:model.category_id];
                    }
                    [SVProgressHUD dismiss];
                });
                NSLog(@"打印课程-----%@",responseObject);
            }else {
                //加载失败隐藏
            }
        }
            FailureBlock:^(id  _Nonnull error) {
                [SVProgressHUD setStatus:@"加载失败..."];
                [SVProgressHUD dismissWithDelay:10];
        }];
    });
}
#pragma mark--最热课程
- (void)hotCourse:(NSString *)string {
    self.respoceArray = [NSMutableArray array];
    NSMutableDictionary *newCourseDic = [NSMutableDictionary dictionary];
    
    newCourseDic[@"category_id"] = string;
    newCourseDic[@"screen"] = @"2";
    [GMAfnTools PostHttpDataWithUrlStr:KCourseList Dic:newCourseDic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"54545445454545454==%@",responseObject);
        for (NSMutableDictionary *dic in responseObject) {
            testModel *mol = [[testModel alloc]init];
            [mol mj_setKeyValues:dic];
            [self.respoceArray addObject:mol];
        }
        [self.tableView reloadData];
        
    }
                          FailureBlock:^(id  _Nonnull error) {
                          }];
    //    [self.tableView reloadData];
}

/*++++++++++++++++最新课程++++++++++++++++++++++++++++*/

#pragma mark--最新课程
- (void)HomeNewcourse {
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD show];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // 延时事件
        //        [self NewCourseOnde];
        self.hotCourseArray = [NSMutableArray array];
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        dic[@"pid"] = @"1";
        __weak typeof(self ) weakSelf = self;
        [GMAfnTools PostHttpDataWithUrlStr:KCategoryID Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
            if (responseObject) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    for (NSMutableDictionary *idDic in responseObject) {
                        CourseCategory_idModel *model = [[CourseCategory_idModel alloc]init];
                        [model mj_setKeyValues:idDic];
                        [self.hotCourseArray addObject:model];
                        
                        [weakSelf newCourse:model.category_id];
                    }
                    [SVProgressHUD dismiss];
                });
                NSLog(@"打印课程-----%@",responseObject);
                
            }else {
                //加载失败隐藏
                
            }
        }
                              FailureBlock:^(id  _Nonnull error) {
                                  [SVProgressHUD setStatus:@"加载失败..."];
                                  [SVProgressHUD dismissWithDelay:10];
                                  
                              }];
        
        
    });
}
#pragma mark--最新课程
- (void)newCourse:(NSString *)string {
    self.respoceArray = [NSMutableArray array];
    NSMutableDictionary *newCourseDic = [NSMutableDictionary dictionary];
    
    newCourseDic[@"category_id"] = string;
    newCourseDic[@"screen"] = @"1";
    [GMAfnTools PostHttpDataWithUrlStr:KCourseList Dic:newCourseDic SuccessBlock:^(id  _Nonnull responseObject) {
        for (NSMutableDictionary *dic in responseObject) {
            testModel *mol = [[testModel alloc]init];
            [mol mj_setKeyValues:dic];
            [self.respoceArray addObject:mol];
        }
        [self.tableView reloadData];
        
    }
     
                          FailureBlock:^(id  _Nonnull error) {
                              
                          }];
    //    [self.tableView reloadData];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    testModel *mol = [self.respoceArray objectAtIndex:indexPath.row];
    CoureDetailViewController *courseVC = [[CoureDetailViewController alloc]init];
    courseVC.courseDetailIDString = mol.course_id;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"pushName" object:mol.course_id];

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"cellID";
    NewCourseTwoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    //    cell.textLabel.text = [NSString stringWithFormat:@"%ld",self.index ];
    if (!cell) {
        cell = [[NewCourseTwoTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    testModel *mox = [self.respoceArray objectAtIndex:indexPath.row];
    //                cell.textLabel.text = mox.course_price;
    [cell.headImgView sd_setImageWithURL:[NSURL URLWithString:mox.course_list_cover]];
    cell.headImgView.sd_cornerRadius = [NSNumber numberWithInteger:10];
    cell.titleLabel.text = mox.course_name;
    cell.detailLabel.text = mox.brief;
    cell.keshiLabel.text = [mox.class_hour stringByAppendingString:@"课时"];
    if ([mox.member_is_free isEqualToString:@"1"]) {
        cell.mianfeiLabel.hidden = NO;
        cell.mianfeiLabel.text = @"Vip免费";
        cell.titleLabel.sd_resetLayout.topEqualToView(cell.mianfeiLabel)
        .leftSpaceToView(cell.mianfeiLabel, 5)
        .widthIs(161*KScreenW/375).heightIs(15);
    } else {
        cell.mianfeiLabel.hidden = YES;
        cell.titleLabel.sd_resetLayout.topEqualToView(cell.mianfeiLabel)
        .leftSpaceToView(cell.headImgView, 22)
        .widthIs(161*KScreenW/375).heightIs(15);
    }
    
    cell.xPriceLabel.text = [NSString stringWithFormat:@"¥%@", mox.course_under_price];
    if (mox.course_price.floatValue == 0) {
        cell.priceLabel.text = @"免费";
    } else {
        cell.priceLabel.text = [NSString stringWithFormat:@"¥%@", mox.course_price];
    }
    if ([mox.is_free isEqualToString:@"1"]) {
        cell.priceLabel.text = @"免费";
    }
    
    cell.peoplesLabel.text = mox.course_vrows;
    NSLog(@"testestsetet%@",mox.course_price);
    return cell;
    
    switch (self.index) {
        case 0:{
       
                testModel *mox = [self.respoceArray objectAtIndex:indexPath.row];
//                cell.textLabel.text = mox.course_price;
                [cell.headImgView sd_setImageWithURL:[NSURL URLWithString:mox.course_list_cover]];
            cell.headImgView.sd_cornerRadius = [NSNumber numberWithInteger:10];
                cell.titleLabel.text = mox.course_name;
                cell.detailLabel.text = mox.course_name;
                cell.keshiLabel.text = [mox.class_hour stringByAppendingString:@"课时"];
            if ([mox.member_is_free isEqualToString:@"1"]) {
                cell.mianfeiLabel.text = @"Vip免费";
            }else {
                cell.mianfeiLabel.hidden = YES;
                cell.titleLabel.sd_layout.leftSpaceToView(cell.headImgView, 22);
            }
            
                cell.xPriceLabel.text = mox.course_under_price;
                cell.priceLabel.text = mox.course_price;
//                cell.peoplesLabel.text = mox.
                NSLog(@"testestsetet%@",mox.course_price);
            
        }
            break;
        case 1:
        {
            
//            cell.textLabel.text = @"1111";
        }
            
            break;
            
        case 2:{
            
//            cell.textLabel.text = @"222";
        }
            
            break;
        case 3:{
            
//            cell.textLabel.text = @"333";
        }
            break;
        case 4:{
            
//            cell.textLabel.text = @"9999999999";
        }
            break;
            
        default:
            break;
    }
    
    return cell;
    
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
