//
//  VideoViewController.h
//  yuduo
//
//  Created by Mac on 2019/8/19.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CourseVideoModel;
@class CourseVideoOtherModel;
@class CustomPlayerView;
NS_ASSUME_NONNULL_BEGIN

@protocol VideoViewControllerDelegate <NSObject>

- (void)playOther:(CourseVideoOtherModel *)model;

@end

@interface VideoViewController : UIViewController

@property (nonatomic, strong) CourseVideoModel *model;
@property (nonatomic, copy) NSArray<CourseVideoOtherModel *> *list;
@property (nonatomic, weak) id<VideoViewControllerDelegate> delegate;
@property (nonatomic, strong) CustomPlayerView *player;
@property (nonatomic, copy) NSString *priceString;
PropertyStrong(NSMutableArray, mp4Array);
PropertyStrong(NSString, videoString);//视频地址
PropertyStrong(NSString, mp3String);//mp3地址;
@end

NS_ASSUME_NONNULL_END
