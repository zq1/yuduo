//
//  DetailViewController.h
//  yuduo
//
//  Created by Mac on 2019/8/5.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface DetailViewController : BaseViewController
PropertyStrong(UITableView, sortTableView);

@property(nonatomic,strong)UIScrollView*Scrview1; //滚动条
@property(nonatomic,strong)UIScrollView*Scrview2;
@property(nonatomic,strong)UITableView*HotTableView;
@end

NS_ASSUME_NONNULL_END
