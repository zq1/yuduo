//
//  CoureDetailViewController.h
//  yuduo
//
//  Created by Mac on 2019/8/6.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CoureDetailViewController : UIViewController
PropertyStrong(NSString, courseDetailIDString);
PropertyStrong(NSString, courseTitle);//课程标题

@end

NS_ASSUME_NONNULL_END
