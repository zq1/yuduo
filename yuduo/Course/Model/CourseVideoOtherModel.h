//
//  CourseVideoOtherModel.h
//  yuduo
//
//  Created by Mac on 2019/10/25.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CourseVideoOtherModel : NSObject

@property (nonatomic, copy) NSString *course_hour_id;
@property (nonatomic, copy) NSString *course_id;
@property (nonatomic, copy) NSString *course_hour_name;
@property (nonatomic, copy) NSString *course_hour_type;
@property (nonatomic, copy) NSString *course_hour_free;
@property (nonatomic, copy) NSString *course_hour_trysee;
@property (nonatomic, copy) NSString *course_hour_trytime;
@property (nonatomic, copy) NSString *course_hour_video;
@property (nonatomic, copy) NSString *course_hour_video_audio_name;
@property (nonatomic, copy) NSString *course_hour_time;
@property (nonatomic, copy) NSString *course_hour_upload_time;
@property (nonatomic, copy) NSString *tuwen_id;
@property (nonatomic, copy) NSString *videoid;

@end

NS_ASSUME_NONNULL_END
