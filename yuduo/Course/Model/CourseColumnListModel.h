//
//  CourseColumnListModel.h
//  yuduo
//
//  Created by Mac on 2019/8/30.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CourseColumnListModel : BaseModel

PropertyStrong(NSString, gement_id);
PropertyStrong(NSString, course_name);
PropertyStrong(NSString, course_price);
PropertyStrong(NSString, course_under_price);
PropertyStrong(NSString, is_free);
PropertyStrong(NSString, member_is_free);
PropertyStrong(NSString, start_date);
PropertyStrong(NSString, end_date);
PropertyStrong(NSString, course_vrows);
PropertyStrong(NSString, course_list_cover);
PropertyStrong(NSString, class_hour);

@end

NS_ASSUME_NONNULL_END
