//
//  KCPJModel.h
//  yuduo
//
//  Created by mason on 2019/9/17.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface KCPJModel : BaseModel
PropertyStrong(NSString, comments);
PropertyStrong(NSString, comment_id);
PropertyStrong(NSString, comments_time);
PropertyStrong(NSString, sid);
PropertyStrong(NSString, service_reply);
PropertyStrong(NSString, reply_time);
PropertyStrong(NSString, status);
PropertyStrong(NSString, nikname);
PropertyStrong(NSString, thumbs_up);
PropertyStrong(NSString, user_id);
PropertyStrong(NSString, comment_title);
PropertyStrong(NSString, portrait);
@end

NS_ASSUME_NONNULL_END
