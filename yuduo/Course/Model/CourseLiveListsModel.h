//
//  CourseLiveListsModel.h
//  yuduo
//
//  Created by Mac on 2019/8/30.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CourseLiveListsModel : BaseModel
PropertyStrong(NSString, live_id);
PropertyStrong(NSString, name);
PropertyStrong(NSString, start_date);
PropertyStrong(NSString, publicity_cover);
PropertyStrong(NSString, upper);
PropertyStrong(NSString, live_status);
@end

NS_ASSUME_NONNULL_END
