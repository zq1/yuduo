//
//  CourseCategory_idModel.h
//  yuduo
//
//  Created by Mac on 2019/9/12.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CourseCategory_idModel : BaseModel
PropertyStrong(NSString, title);
PropertyStrong(NSString, category_id);
@end

NS_ASSUME_NONNULL_END
