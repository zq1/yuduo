//
//  testModel.h
//  yuduo
//
//  Created by mason on 2019/8/31.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface testModel : BaseModel
PropertyStrong(NSString, course_price);
PropertyStrong(NSString, course_category_id);
PropertyStrong(NSString, course_list_cover);
PropertyStrong(NSString, is_free);
PropertyStrong(NSString, start_date);
PropertyStrong(NSString, member_is_free);
PropertyStrong(NSString, class_hour);
PropertyStrong(NSString, course_under_price);
PropertyStrong(NSString, end_date);
PropertyStrong(NSString, course_name);
PropertyStrong(NSString, course_id);
PropertyStrong(NSString, brief);
PropertyStrong(NSString, course_vrows);




@end

NS_ASSUME_NONNULL_END
