//
//  HomeColumnModel.h
//  yuduo
//
//  Created by mason on 2019/9/15.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface HomeColumnModel : BaseModel
PropertyStrong(NSString, category_id);
//最新专栏列表标题
PropertyStrong(NSString, titlex);
@end

NS_ASSUME_NONNULL_END
