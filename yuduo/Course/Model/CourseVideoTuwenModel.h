//
//  CourseVideoTuwenModel.h
//  yuduo
//
//  Created by Mac on 2019/10/25.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CourseVideoTuwenModel : NSObject

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *try_details;
@property (nonatomic, copy) NSString *creattime;
@property (nonatomic, copy) NSString *details;

@end

NS_ASSUME_NONNULL_END
