//
//  CourseVideoModel.h
//  yuduo
//
//  Created by Mac on 2019/10/25.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CourseVideoOtherModel;
@class CourseVideoTuwenModel;

NS_ASSUME_NONNULL_BEGIN

@interface CourseVideoModel : NSObject

@property (nonatomic, copy) NSString *course_hour_id;
@property (nonatomic, copy) NSString *course_id;
@property (nonatomic, copy) NSString *course_hour_name;
@property (nonatomic, copy) NSString *course_hour_type;
@property (nonatomic, copy) NSString *course_hour_free;
@property (nonatomic, copy) NSString *course_hour_trysee;
@property (nonatomic, copy) NSString *course_hour_trytime;
@property (nonatomic, copy) NSString *course_hour_video;
@property (nonatomic, copy) NSString *course_hour_video_audio_name;
@property (nonatomic, copy) NSString *course_hour_time;
@property (nonatomic, copy) NSString *course_hour_upload_time;
@property (nonatomic, copy) NSString *status;
@property (nonatomic, copy) NSString *create_time;
@property (nonatomic, copy) NSString *manipulate;
@property (nonatomic, copy) NSString *manipulate_time;
@property (nonatomic, copy) NSString *tuwen_id;
@property (nonatomic, copy) NSString *sort;
@property (nonatomic, strong) CourseVideoTuwenModel *tuwen;
@property (nonatomic, copy) NSArray<CourseVideoOtherModel *> *other;
@property (nonatomic, copy) NSString *audio_picture;//音频封面

@end

NS_ASSUME_NONNULL_END
