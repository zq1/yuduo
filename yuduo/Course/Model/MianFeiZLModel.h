//
//  MianFeiZLModel.h
//  yuduo
//
//  Created by mason on 2019/9/15.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MianFeiZLModel : BaseModel
PropertyStrong(NSString, course_category_id);
PropertyStrong(NSString, course_list_cover);
PropertyStrong(NSString, course_price);
PropertyStrong(NSString, is_free);
PropertyStrong(NSString, start_date);
PropertyStrong(NSString, class_hour);
PropertyStrong(NSString, course_under_price);
PropertyStrong(NSString, end_date);
PropertyStrong(NSString, course_name);
PropertyStrong(NSString, course_id);
@end

NS_ASSUME_NONNULL_END
