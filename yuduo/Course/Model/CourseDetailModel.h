//
//  CourseDetailModel.h
//  yuduo
//
//  Created by Mac on 2019/8/27.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CourseDetailModel : BaseModel
PropertyStrong(NSString, id);
PropertyStrong(NSString, course_name);
PropertyStrong(NSString, course_price);
PropertyStrong(NSString, course_under_price);
PropertyStrong(NSString, course_label);
PropertyStrong(NSString, is_free);
PropertyStrong(NSString, member_is_free);
PropertyStrong(NSString, start_date);
PropertyStrong(NSString, end_date);
PropertyStrong(NSString, course_vrows);
PropertyStrong(NSString, is_recommend);
PropertyStrong(NSString, is_friends_buys);
PropertyStrong(NSString, course_details);
PropertyStrong(NSString, course_list_cover);
PropertyStrong(NSString, course_recommend_cover);
PropertyStrong(NSString, course_free_cover);
PropertyStrong(NSString, course_details_cover);
PropertyStrong(NSString, create_time);
PropertyStrong(NSString, update_time);
PropertyStrong(NSString, manipulate);
PropertyStrong(NSString, manipulate_time);
PropertyStrong(NSString, status);
PropertyStrong(NSString, content);
PropertyStrong(NSString, course_hour);

@end

NS_ASSUME_NONNULL_END
