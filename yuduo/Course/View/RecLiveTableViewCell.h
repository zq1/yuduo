//
//  RecLiveTableViewCell.h
//  yuduo
//
//  Created by Mac on 2019/7/30.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RecLiveTableViewCell : UITableViewCell
PropertyStrong(UIView, bgView);
PropertyStrong(UIImageView, imgView);
PropertyStrong(UILabel , titleLabel);

@end

NS_ASSUME_NONNULL_END
