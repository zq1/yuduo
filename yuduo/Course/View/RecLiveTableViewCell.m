//  RecLiveTableViewCell.m
//  yuduo
//  Created by Mac on 2019/7/30.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "RecLiveTableViewCell.h"

@implementation RecLiveTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        [self creatView];
    }
    return self;
}

- (void)creatView {
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = GMWhiteColor;
    self.bgView.layer.cornerRadius = 10;
    _bgView.layer.shadowColor = [UIColor blackColor].CGColor;
    _bgView.layer.shadowOffset = CGSizeMake(0, 0);
    _bgView.layer.shadowOpacity = 0.3;
    _bgView.layer.shadowRadius = 9.0;
    _bgView.layer.cornerRadius = 9.0;
//    UILabel *label = [[UILabel alloc] initWithFrame:shadowView.bounds];
//    label.backgroundColor =[UIColor redColor];
//    label.layer.cornerRadius = 10;
//    label.layer.masksToBounds = YES;
//    label.text = @"撒旦法撒旦法撒旦法";
//    [_bgView addSubview:label];
    [self addSubview:self.bgView];
    self.bgView.sd_layout.topSpaceToView(self, 0)
    .leftSpaceToView(self, 17)
    .rightSpaceToView(self, 13).heightIs(219);
    
    self.imgView = [[UIImageView alloc]init];
    self.imgView.backgroundColor = GMBlueColor;
    self.imgView.layer.cornerRadius = 10;
    self.imgView.layer.masksToBounds =YES;
    [self.bgView addSubview:self.imgView];
    self.imgView.sd_layout.topSpaceToView(self.bgView, 0)
    .leftEqualToView(self.bgView)
    .rightEqualToView(self.bgView).heightIs(173);
    
    self.titleLabel = [[UILabel alloc]init];
//    self.titleLabel.backgroundColor = GMWhiteColor;
    self.titleLabel.layer.cornerRadius = 20  ;
    self.titleLabel.text = @"课程标题课程标题";
    [self.bgView addSubview:self.titleLabel];
    self.titleLabel.sd_layout.topSpaceToView(self.imgView, 0)
    .leftEqualToView(self.imgView)
    .rightEqualToView(self.imgView)
    .heightIs(45);
    }

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
