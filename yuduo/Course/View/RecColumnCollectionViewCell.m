//
//  RecColumnCollectionViewCell.m
//  yuduo
//
//  Created by Mac on 2019/8/7.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "RecColumnCollectionViewCell.h"

@implementation RecColumnCollectionViewCell
-(instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        [self createNewCourse];
    }
    return self;
}

- (void)createNewCourse {
    
    self.imgView = [[UIImageView alloc]init];
    self.imgView.backgroundColor = GMlightGrayColor;
    
//    self.imgView.layer.masksToBounds = YES;
    [self.contentView addSubview:self.imgView];
    
    self.imgView.sd_layout.topEqualToView(self.contentView)
    .leftEqualToView(self.contentView)
    .rightEqualToView(self.contentView)
    .heightIs(112);
    
    self.titleLabel = [[UILabel alloc]init];
    self.titleLabel.numberOfLines = 2;
    self.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:14];
    self.titleLabel.text = @"标题";
    self.titleLabel.textColor = RGB(69, 69, 69);
    [self.contentView addSubview:self.titleLabel];
    self.titleLabel.sd_layout.topSpaceToView(self.imgView, 16)
    .leftSpaceToView(self.contentView, 11)
    .widthIs(142).heightIs(40);
    
    self.keshiImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"更多"]];
    [self.contentView addSubview:self.keshiImg];
    self.keshiImg.sd_layout.topSpaceToView(self.titleLabel, 15)
    .leftSpaceToView(self.contentView, 11)
    .widthIs(10).heightIs(15);
    
    self.keshiLabel = [[UILabel alloc]init];
    self.keshiLabel.textColor = RGB(153, 153, 153);
    self.keshiLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:11];
    self.keshiLabel.text = @"2课时";
    [self.contentView addSubview:self.keshiLabel];
    self.keshiLabel.sd_layout.topSpaceToView(self.titleLabel, 16)
    .leftSpaceToView(self.keshiImg, 11)
    .widthIs(37).heightIs(11);
    
    self.seeImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ico_look"]];
    [self.contentView addSubview:self.seeImg];
    self.seeImg.sd_layout.topEqualToView(self.keshiImg)
    .rightSpaceToView(self.contentView, 64)
    .widthIs(10).heightIs(15);
    
    self.peoplesLabel = [[UILabel alloc]init];
    self.peoplesLabel.textColor = RGB(153, 153, 153);
    self.peoplesLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:11];
    self.peoplesLabel.text = @"247人";
    [self.contentView addSubview:self.peoplesLabel];
    self.peoplesLabel.sd_layout.topEqualToView(self.keshiLabel)
    .rightSpaceToView(self.contentView, 17)
    .widthIs(38).heightIs(11);
    
    self.priceLabel = [[UILabel alloc]init];
    self.priceLabel.textColor = RGB(228, 102, 67);
    self.priceLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:15];
    self.priceLabel.text = @"¥199.00";
    [self.contentView addSubview:self.priceLabel];
    self.priceLabel.sd_layout.bottomSpaceToView(self.contentView, 11)
    .leftSpaceToView(self.contentView, 11)
    .widthIs(56).heightIs(12);
    
}
@end
