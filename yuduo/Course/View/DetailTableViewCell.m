//
//  DetailTableViewCell.m
//  yuduo
//
//  Created by Mac on 2019/8/6.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "DetailTableViewCell.h"

@implementation DetailTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        
        [self createNewCourse];
    }
    return self;
}

- (void)createNewCourse {
    //背景view（所有控件都加到背景）
    self.bgView = [[UIView alloc]init];
    //    self.bgView.backgroundColor = GMBlueColor;
    self.bgView.frame = CGRectMake(0, 0, KScreenW, 134+18);
    [self.contentView addSubview:self.bgView];
    //头像
    self.headImgView = [[UIImageView alloc]init];
    self.headImgView.backgroundColor = GMlightGrayColor;
    self.headImgView.layer.cornerRadius = 10;
    [self.bgView addSubview:self.headImgView];
    self.headImgView.sd_layout.topSpaceToView(self.bgView, 18)
    .leftSpaceToView(self.bgView, 15).widthIs(100*kGMWidthScale).heightIs(134);
    //免费logo
    self.mianfeiLabel = [[UILabel alloc] init];
    self.mianfeiLabel.numberOfLines = 0;
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"vip免费" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 11],NSForegroundColorAttributeName: [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0]}];
    
    
    self.mianfeiLabel.backgroundColor = [UIColor colorWithRed:228/255.0 green:102/255.0 blue:67/255.0 alpha:1.0];
    
    
    self.mianfeiLabel.attributedText = string;
    [self.bgView addSubview:self.mianfeiLabel];
    self.mianfeiLabel.sd_cornerRadius = [NSNumber numberWithInteger:8.5];
    self.mianfeiLabel.sd_layout.topSpaceToView(self.bgView, 40).leftSpaceToView(self.headImgView, 15).widthIs(45).heightIs(17);
    //标题
    self.titleLabel = [[UILabel alloc] init];
    
    self.titleLabel.numberOfLines = 0;
    NSMutableAttributedString *stringTitle = [[NSMutableAttributedString alloc] initWithString:@"课程名称课程名称课程asdfasdfasdfasdfasdf" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 15],NSForegroundColorAttributeName: [UIColor colorWithRed:69/255.0 green:69/255.0 blue:69/255.0 alpha:1.0]}];
    self.titleLabel.attributedText = stringTitle;
    [self.bgView addSubview:self.titleLabel];
    self.titleLabel.sd_layout.topEqualToView(_mianfeiLabel)
    .leftSpaceToView(self.mianfeiLabel, 5)
    .widthIs(161*KScreenW/375).heightIs(15);
    //课程简介
    self.detailLabel = [[UILabel alloc] init];
    //    self.detailLabel.backgroundColor = [UIColor redColor];
    NSMutableAttributedString *stringDetail = [[NSMutableAttributedString alloc] initWithString:@"课程简介课程简介课程简介课程爱死了的开发建设的覅世纪东方阿斯利康地方贾老师客观地讲拉电饭煲把电饭锅简介课程简asdfaafas阿斯顿发斯蒂芬阿斯蒂芬阿萨德发送到发生地方 dfasdfasdfasdfasdf介课程简介课程简介课程..." attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 13],NSForegroundColorAttributeName: [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1.0]}];
    self.detailLabel.attributedText = stringDetail;
    self.detailLabel.numberOfLines = 2;
    self.detailLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [self.bgView addSubview:self.detailLabel];
    
    //     self.detailLabel.frame = CGRectMake(132,755.5,228.5,32.5);
    self.detailLabel.sd_layout.topSpaceToView(self.mianfeiLabel, 12)
    .leftSpaceToView(self.headImgView, 17)
    .rightSpaceToView(self.bgView, 15)
    .heightIs(38);
    //课时按钮
    self.keshiButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [self.keshiButton setBackgroundImage:[UIImage imageNamed:@"更多"] forState:UIControlStateNormal];
    [self.bgView addSubview:self.keshiButton];
    self.keshiButton.sd_layout.leftSpaceToView(self.headImgView, 17*kGMWidthScale)
    .topSpaceToView(self.detailLabel, 26).widthIs(15).heightIs(15);
    //课时标签
    self.keshiLabel = [[UILabel alloc] init];
    //    label.frame = CGRectMake(157,816,34.5,10.5);
    self.keshiLabel.numberOfLines = 0;
    [self.bgView addSubview:self.keshiLabel];
    
    NSMutableAttributedString *stringKS = [[NSMutableAttributedString alloc] initWithString:@"55课时" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 11],NSForegroundColorAttributeName: [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1.0]}];
    self.keshiLabel.attributedText = stringKS;
    
    self.keshiLabel.sd_layout.topSpaceToView(self.detailLabel, 28)
    .leftSpaceToView(self.keshiButton, 11*kGMWidthScale).widthIs(38).heightIs(11);
    //多少人看过图标
    self.seeImg = [[UIImageView alloc] init];
    self.seeImg.image = [UIImage imageNamed:@"ico_look"];
    [self.bgView addSubview:self.seeImg];
    self.seeImg.sd_layout.topEqualToView(self.keshiButton).leftSpaceToView(self.keshiLabel, 20*kGMWidthScale).widthIs(15).heightIs(15);
    
    
    //多少人看过具体人数标签
    
    self.peoplesLabel = [[UILabel alloc] init];
    //    self.peoplesLabel.frame = CGRectMake(236.5,970,36,10.5);
    self.peoplesLabel.numberOfLines = 0;
    [self.bgView addSubview:self.peoplesLabel];
    
    NSMutableAttributedString *stringPeople = [[NSMutableAttributedString alloc] initWithString:@"2478人" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 11],NSForegroundColorAttributeName: [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1.0]}];
    
    self.peoplesLabel.attributedText = stringPeople;
    
    self.peoplesLabel.sd_layout.topEqualToView(self.keshiLabel).leftSpaceToView(self.seeImg, 11*kGMWidthScale).widthIs(40).heightIs(11);
    //划掉的价格
    
    self.xPriceLabel = [[UILabel alloc] init];
    //    self.xPriceLabel.frame = CGRectMake(314,954.5,45,9.5);
    self.xPriceLabel.numberOfLines = 0;
    [self.bgView addSubview:self.xPriceLabel];
    
    NSMutableAttributedString *stringXprice = [[NSMutableAttributedString alloc] initWithString:@"¥299.00" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 12],NSForegroundColorAttributeName: [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1.0]}];
    
    self.xPriceLabel.attributedText = stringXprice;
    self.xPriceLabel.sd_layout.topSpaceToView(self.detailLabel, 10)
    .rightSpaceToView(self.bgView, 15).widthIs(50).heightIs(10);
    //现在的价格
    
    self.priceLabel = [[UILabel alloc] init];
    //    sel.frame = CGRectMake(311.5,815.5,48.5,11.5);
    self.priceLabel.numberOfLines = 0;
    [self.bgView addSubview:self.priceLabel];
    NSMutableAttributedString *stringPrice = [[NSMutableAttributedString alloc] initWithString:@"¥69.00" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 15],NSForegroundColorAttributeName: [UIColor colorWithRed:228/255.0 green:102/255.0 blue:67/255.0 alpha:1.0]}];
    self.priceLabel.attributedText = stringPrice;
    self.priceLabel.sd_layout.topSpaceToView(self.xPriceLabel, 6)
    .rightSpaceToView(self.bgView, 16).widthIs(49).heightIs(12);
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
