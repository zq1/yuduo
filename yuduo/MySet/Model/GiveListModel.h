//
//  GiveListModel.h
//  yuduo
//
//  Created by zhouqi on 2019/10/27.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GiveListModel : NSObject
//  //1,课程.2专栏,3文章,4活动
@property(nonatomic, strong) NSString *give_time;
@property(nonatomic, assign) NSInteger type;
@property(nonatomic, strong) NSString *name;
@property(nonatomic, assign) NSInteger course_id;
@end

NS_ASSUME_NONNULL_END
