//
//  PersonInfoModel.h
//  yuduo
//
//  Created by Mac on 2019/9/2.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface PersonInfoModel : BaseModel
PropertyStrong(NSString, mobile);
PropertyStrong(NSString, nikname);
PropertyStrong(NSString, sex);
PropertyStrong(NSString, portrait);
PropertyStrong(NSString, age);
PropertyStrong(NSString, province);
PropertyStrong(NSString, city);
PropertyStrong(NSString, county);
PropertyStrong(NSString, ltation_number);
PropertyStrong(NSString, is_vip);
PropertyStrong(NSString, vip_expire_time);


@end

NS_ASSUME_NONNULL_END
