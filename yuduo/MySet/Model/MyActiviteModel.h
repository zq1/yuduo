//
//  MyActiviteModel.h
//  yuduo
//
//  Created by zhouqi on 2019/10/26.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyActiviteModel : NSObject
@property(nonatomic, strong) NSString *name;
@property(nonatomic, strong) NSString *under_price;
@property(nonatomic, strong) NSString *pay_price;
@property(nonatomic, strong) NSString *cover;
@property(nonatomic, assign) NSInteger course_id;
@property(nonatomic, strong) NSString *activity_type;
@property(nonatomic, strong) NSString *activity_address;
@property(nonatomic, strong) NSString *activity_detailed_address;
@property(nonatomic, strong) NSString *activity_start_date;
@property(nonatomic, strong) NSString *activity_end_date;
@property(nonatomic, strong) NSString *activity_price;
@property(nonatomic, strong) NSString *activity_cover;

@end

NS_ASSUME_NONNULL_END
