//
//  RedeemCodeLogs.h
//  yuduo
//
//  Created by zhouqi on 2019/10/27.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface RedeemCodeLogs : NSObject
@property(nonatomic, strong) NSString *picture;
@property(nonatomic, strong) NSString *title;
@property(nonatomic, strong) NSString *class_hour;
@property(nonatomic, strong) NSString *hange_time;
@end

NS_ASSUME_NONNULL_END
