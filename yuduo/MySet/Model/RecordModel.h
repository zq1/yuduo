//
//  RecordModel.h
//  yuduo
//
//  Created by mason on 2019/9/18.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface RecordModel : BaseModel
PropertyStrong(NSString, id);

PropertyStrong(NSString, money);
PropertyStrong(NSString, user_id);
PropertyStrong(NSString, name);
PropertyStrong(NSString, purchase_time);
@end

NS_ASSUME_NONNULL_END
