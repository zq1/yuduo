//
//  BabyRecordModel.h
//  yuduo
//
//  Created by zhouqi on 2019/10/27.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BabyRecordModel : NSObject
@property(nonatomic, strong) NSString *birthday;
@property(nonatomic, strong) NSString *user_children_id;
@property(nonatomic, strong) NSString *baby_sex;
@end

NS_ASSUME_NONNULL_END
