//
//  OpenVipList.h
//  yuduo
//
//  Created by zhouqi on 2019/10/27.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface OpenVipList : NSObject
@property(nonatomic, strong) NSString *title;
@property(nonatomic, assign) NSInteger vip_id;
@property(nonatomic, strong) NSString *vip_price;
@end

NS_ASSUME_NONNULL_END
