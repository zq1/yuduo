//
//  PayModel.h
//  yuduo
//
//  Created by Mac on 2019/9/18.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface PayModel : BaseModel
PropertyStrong(NSString, product_id);
PropertyStrong(NSString, pay_price);
@end

NS_ASSUME_NONNULL_END
