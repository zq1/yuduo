//
//  OpenViperViewController.h
//  yuduo
//
//  Created by Mac on 2019/9/18.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface OpenViperViewController : UIViewController
@property (strong,nonatomic)UICollectionView *collectionV;
@property (strong,nonatomic)UICollectionViewFlowLayout *flowLayout;
@end

NS_ASSUME_NONNULL_END
