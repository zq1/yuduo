//
//  BabyRecordViewController.m
//  yuduo
//
//  Created by mason on 2019/8/18.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "BabyRecordViewController.h"
#import "BRTextField.h"
#import "BRPickerView.h"
#import "SexBabyViewController.h"
#import "AddBabyViewController.h"
#import "BabyRecordModel.h"

@interface BabyRecordViewController ()
<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
PropertyStrong(UIView, sectionView);
PropertyStrong(NSArray, sectionTitle);
PropertyStrong(UITableView, searchTableView);

/** 地区 */
@property (nonatomic, strong) BRTextField *nameTF;
/** 出生年月 */
@property (nonatomic, strong) BRTextField *birthdayTF;
/** 性别 */
@property (nonatomic, strong) BRTextField *genderTF;
/** 地区 */
@property (nonatomic, strong) BRTextField *addressTF;
@property (nonatomic, strong) NSMutableArray *dataArray;
@end

@implementation BabyRecordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = RGB(244, 243, 244);
    //    self.sectionTitle = @[@"大宝",@"二宝"];
    [self setNavigation];
    [self createTableView];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.dataArray = [NSMutableArray new];
    NSUserDefaults *dafaults = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"user_id"] = [dafaults objectForKey:@"user_id"];
    dic[@"token"] = [dafaults objectForKey:@"token"];
    [GMAfnTools PostHttpDataWithUrlStr:KbabyInfoURL Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
        for (NSDictionary *dic in responseObject) {
            BabyRecordModel *model = [BabyRecordModel new];
            [model mj_setKeyValues:dic];
            [self.dataArray addObject:model];
        }
        [self.searchTableView reloadData];
    } FailureBlock:^(id  _Nonnull error) {
        
    }];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return self.dataArray.count;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 40;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *titleArray = @[@"性别",@"出生日期"];
    static NSString *cellID = @"cellID";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    //    UILabel *subLabel = [UILabel new];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    for (UIView *subV in cell.contentView.subviews) {
        if ([subV isKindOfClass:[UIButton class]]) {
            [subV removeFromSuperview];
        }
    }
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.imageView.image = [UIImage imageNamed: [titleArray objectAtIndex:indexPath.row]];
    cell.textLabel.text = [titleArray objectAtIndex:indexPath.row];
    
    BabyRecordModel *model = self.dataArray[indexPath.section];
    if (indexPath.row == 0) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setTitleColor:RGB_HEX(666666, 1) forState:UIControlStateNormal];
        button.frame = CGRectMake(100, 10, 100, 30);
        [button setContentHorizontalAlignment:(UIControlContentHorizontalAlignmentRight)];
        button.titleLabel.font = [UIFont systemFontOfSize:14];
        button.tag = indexPath.section;
        [button setTitle:model.baby_sex.intValue == 1 ? @"男" : @"女" forState:UIControlStateNormal];
        [button addTarget:self action:@selector(gender:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:button];
        button.sd_layout.bottomEqualToView(cell.contentView)
        .topEqualToView(cell.contentView)
        .widthIs(100)
        .rightSpaceToView(cell.contentView, 10);
    }
    if (indexPath.row == 1) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(100, 10, 100, 30);
        [button setContentHorizontalAlignment:(UIControlContentHorizontalAlignmentRight)];
        button.titleLabel.font = [UIFont systemFontOfSize:14];
        [button setTitle:model.birthday forState:UIControlStateNormal];
        button.tag = indexPath.section;
        [button addTarget:self action:@selector(click:) forControlEvents:UIControlEventTouchUpInside];
        [button setTitleColor:RGB_HEX(666666, 1) forState:UIControlStateNormal];
        [cell.contentView addSubview:button];
        button.sd_layout.bottomEqualToView(cell.contentView)
        .topEqualToView(cell.contentView)
        .widthIs(100)
        .rightSpaceToView(cell.contentView, 10);
    }
    return cell;
}
#pragma mark -- 提示框
- (void)showError:(NSString *)errorMsg {
    // 1.弹框提醒
    // 初始化对话框
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:errorMsg preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:nil]];
    // 弹出对话框
    [self presentViewController:alert animated:true completion:nil];
    
    [self performSelector:@selector(dimisssAlert:) withObject:alert afterDelay:1];
    
}

- (void) dimisssAlert:(UIAlertController *)alert
{
    if (alert) {
        [alert dismissViewControllerAnimated:YES completion:nil];
    }
    
}
- (void)gender:(UIButton *)send {
    NSInteger section = send.tag;
    __weak typeof(self) weakSelf = self;
    [BRStringPickerView showStringPickerWithTitle:@"宝宝性别" dataSource:@[@"男", @"女"] defaultSelValue:@"男" isAutoSelect:NO resultBlock:^(id selectValue) {
        [send setTitle:selectValue forState:UIControlStateNormal];
        BabyRecordModel *model = weakSelf.dataArray[section];
        model.baby_sex = selectValue == @"男" ? @"1" : @"2";
        weakSelf.dataArray[section] = model;
    }];
}

- (void)click:(UIButton *)send {
    NSInteger section = send.tag;
    __weak typeof(self) weakSelf = self;
    [BRDatePickerView showDatePickerWithTitle:@"选择出生日期" dateType:UIDatePickerModeDate defaultSelValue:nil minDateStr:nil maxDateStr:nil isAutoSelect:NO resultBlock:^(NSString *selectValue) {
        [send setTitle:selectValue forState:UIControlStateNormal];
        BabyRecordModel *model = weakSelf.dataArray[section];
        model.birthday = selectValue;
        weakSelf.dataArray[section] = model;
    }];
}
- (void)textFieldDidChange:(UITextField *)text
{
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSLog(@"滑到了第 %ld 组 %ld个",indexPath.section, indexPath.row);
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
}
#pragma mark- 禁止section停留
-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    //    CGFloat sectionHeaderHeight = 50;
    //
    //    if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
    //
    //        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    //
    //    } else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
    //
    //        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
    //
    //    }
    
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    self.sectionView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenW, 40)];
    //各个模块
    UILabel *sectionTitle = [[UILabel alloc] init];
    sectionTitle.frame = CGRectMake(16,0,90,40);
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"宝宝档案" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Medium" size: 18],NSForegroundColorAttributeName: RGB_HEX(666666, 1)}];
    sectionTitle.attributedText = string;
    sectionTitle.font = [UIFont systemFontOfSize:14];
    [self.sectionView addSubview:sectionTitle];
    
    return self.sectionView;
}

- (void)setNavigation {
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenW,44+MStatusBarHeight)];
    view.backgroundColor = GMWhiteColor;
    [self.view addSubview:view];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:[UIImage imageNamed:@"back-icon"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButton) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:backButton];
    backButton.sd_layout.topSpaceToView(view,MStatusBarHeight+12)
    .leftSpaceToView(view, 15)
    .widthIs(11).heightIs(19);
    UIButton *backButtonView = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 80, MStatusBarHeight+44)];
    backButtonView.backgroundColor = RGBA(15, 15, 15, 0);
    [backButtonView addTarget:self action:@selector(backButton) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButtonView];
    
    UILabel *titleLabel = [[UILabel alloc]init];
    [view addSubview:titleLabel];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"孩子信息" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 18],NSForegroundColorAttributeName: [UIColor blackColor]}];
    titleLabel.attributedText = string;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.sd_layout.topSpaceToView(view, MStatusBarHeight+13)
    .centerXEqualToView(view)
    .heightIs(18).widthIs(100);
    
    UIButton *addBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [addBtn setImage:[UIImage imageNamed:@"add"] forState:(UIControlStateNormal)];
    [addBtn addTarget:self action:@selector(addBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:addBtn];
    addBtn.sd_layout.topSpaceToView(self.view, MStatusBarHeight+10)
    .rightSpaceToView(self.view, 30)
    .widthIs(24).heightIs(24);
    
    UIButton *saveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    saveBtn.backgroundColor = RGB(23, 153, 167);
    saveBtn.layer.cornerRadius = 22.5;
    [saveBtn addTarget:self action:@selector(saveBtn) forControlEvents:UIControlEventTouchUpInside];
    [saveBtn setTitleColor:GMWhiteColor forState:UIControlStateNormal];
    [saveBtn setTitle:@"保存" forState:UIControlStateNormal];
    [self.view addSubview:saveBtn];
    saveBtn.sd_layout.bottomSpaceToView(self.view, 30)
    .leftSpaceToView(self.view, 15)
    .rightSpaceToView(self.view, 15)
    .heightIs(45);
}
//保存
- (void)saveBtn {
    [self showError:@"保存成功"];
    
    NSMutableDictionary *infoParms = [NSMutableDictionary dictionary];
    NSUserDefaults *detafaults = [NSUserDefaults standardUserDefaults];
    NSString  *user_id =  [detafaults objectForKey:@"user_id"];
    NSString *token =  [detafaults objectForKey:@"token"];
    infoParms[@"user_id"] = user_id;
    infoParms[@"token"] = token;
    NSMutableArray *tmp = [NSMutableArray new];
    for (BabyRecordModel *model in self.dataArray) {
        NSDictionary *dic = [model mj_JSONObject];
        [tmp addObject:dic];
    }
    NSString *babyInfo = [self toJsonString: tmp];
    infoParms[@"babyinfo"] = babyInfo;
    __weak typeof(self) weakSelf = self;
    NSLog(@"%@", infoParms);
    [GMAfnTools PostHttpDataWithUrlStr:KSaveBabyInfo Dic:infoParms SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"xxxxxx++++x%@",responseObject);
    } FailureBlock:^(id  _Nonnull error) {
         NSLog(@"获取个人信息%@",error);
    }];
    
}
//添加
- (void)addBtn {
    [self.navigationController pushViewController:[AddBabyViewController new] animated:true];
    //    NSMutableDictionary *infoParms = [NSMutableDictionary dictionary];
    //    NSUserDefaults *detafaults = [NSUserDefaults standardUserDefaults];
    //    NSString  *user_id =  [detafaults objectForKey:@"user_id"];
    //    NSString *token =  [detafaults objectForKey:@"token"];
    //    infoParms[@"user_id"] = user_id;
    //    infoParms[@"token"] = token;
    //    NSString *babyInfo = [self toJsonString:@[@{@"baby_sex": @"1", @"birthday": @"2019-08-07"}, @{@"baby_sex": @"1", @"birthday": @"2019-08-07"}]];
    //    infoParms[@"babyinfo"] = babyInfo;
    //    __weak typeof(self) weakSelf = self;
    //    NSLog(@"%@", infoParms);
    //    [GMAfnTools PostHttpDataWithUrlStr:KSaveBabyInfo Dic:infoParms SuccessBlock:^(id  _Nonnull responseObject) {
    //        NSLog(@"xxxxxx++++x%@",responseObject);
    //    } FailureBlock:^(id  _Nonnull error) {
    //         NSLog(@"获取个人信息%@",error);
    //    }];
}

- (NSString *)toJsonString:(NSArray *)arrJson {
    
    if (![arrJson isKindOfClass:[NSArray class]] || ![NSJSONSerialization isValidJSONObject:arrJson]) {
        return nil;
    }
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:arrJson options:0 error:nil];
    NSString *strJson = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return strJson;
}

- (void)createTableView {
    
    self.sectionTitle = @[@"大宝档案",@"二宝档案"];
    self.searchTableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    self.searchTableView.delegate = self;
    self.searchTableView.estimatedSectionHeaderHeight = 0;
    self.searchTableView.estimatedSectionFooterHeight = 0;
    [self.searchTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.searchTableView.dataSource = self;
    self.searchTableView.backgroundColor = RGB(244, 243, 244);
    [self.view addSubview:self.searchTableView];
    self.searchTableView.sd_layout.topSpaceToView(self.view, MStatusBarHeight+44)
    .leftSpaceToView(self.view, 0)
    .rightSpaceToView(self.view, 0)
    .bottomSpaceToView(self.view, 100);
}

- (BRTextField *)getTextField:(UITableViewCell *)cell {
    BRTextField *textField = [[BRTextField alloc]initWithFrame:CGRectMake(SCREEN_WIDTH - 230, 0, 200, 50)];
    textField.backgroundColor = [UIColor clearColor];
    textField.font = [UIFont systemFontOfSize:16.0f];
    textField.textAlignment = NSTextAlignmentRight;
    textField.textColor = RGB_HEX(0x666666, 1.0);
    textField.delegate = self;
    [cell.contentView addSubview:textField];
    return textField;
}

#pragma mark - 年龄 textField
- ( void)setupAgeTF:(UITableViewCell *)cell {
    if (!_birthdayTF) {
        _birthdayTF = [self getTextField:cell];
        _birthdayTF.placeholder = @"请选择";
        __weak typeof(self) weakSelf = self;
        _birthdayTF.tapAcitonBlock = ^{
            [BRStringPickerView showStringPickerWithTitle:@"选择年龄" dataSource:@[@"80后", @"90后", @"00后"] defaultSelValue:@"90后" isAutoSelect:YES resultBlock:^(id selectValue) {
                weakSelf.birthdayTF.text = selectValue;
                
                
            }];
            
            
        };
    }
}
#pragma mark - 性别 textField
- (void)setupGenderTF:(UITableViewCell *)cell {
    if (!_genderTF) {
        _genderTF = [self getTextField:cell];
        
        _genderTF.placeholder = @"请选择";
        __weak typeof(self) weakSelf = self;
        _genderTF.tapAcitonBlock = ^{
            [BRStringPickerView showStringPickerWithTitle:@"宝宝性别" dataSource:@[@"男", @"女"] defaultSelValue:@"男" isAutoSelect:YES resultBlock:^(id selectValue) {
                weakSelf.genderTF.text = selectValue;
                
                NSLog(@"获取性别===%@",weakSelf.genderTF.text);
                NSMutableDictionary *sexDic = [NSMutableDictionary dictionary];
                
                NSUserDefaults *detafaults = [NSUserDefaults standardUserDefaults];
                NSString  *user_id =  [detafaults objectForKey:@"user_id"];
                NSString *token =  [detafaults objectForKey:@"token"];
                sexDic[@"user_id"] = user_id;
                sexDic[@"token"] = token;
                if ([selectValue isEqualToString:@"男"]) {
                    sexDic[@"sex"] = @"1";
                }else {
                    sexDic[@"sex"] = @"2";
                }
                [weakSelf changeSex:sexDic];
            }];
            
            
        };
    }
}
#pragma mark--更改性别
-(void)changeSex:(NSMutableDictionary *)parmasDic {
    [GMAfnTools PostHttpDataWithUrlStr:KChangeSex Dic:parmasDic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"======上传成功==%@",responseObject);
    } FailureBlock:^(id  _Nonnull error) {
        
    }];
}
- (void)backButton {
    [self.navigationController popViewControllerAnimated:YES];
}


@end
