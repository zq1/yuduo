//
//  MySetViewController.m
//  yuduo
//
//  Created by Mac on 2019/7/23.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "MySetViewController.h"
#import "MysetBgView.h"
#import "AliUpLoadImageTool.h"
#import "MysetTableViewCell.h"
#import "MyDetailViewController.h"
#import "PersonInfoModel.h"
#import "MyCommentViewController.h"
#import "MySetListViewController.h"
//登录页
#import "LoginViewController.h"
//注册页
#import "RegisterViewController.h"
//设置按钮
#import "SetLoginOutViewController.h"
//我的余额1
#import "YueViewController.h"
//我的订单2
#import "DingdanViewController.h"
//我的购买3
#import "GoumaiViewController.h"
//我的浏览4
#import "LiulanViewController.h"
//我的下载5
#import "DownViewController.h"
//我的直播6
#import "MyZhiBoViewController.h"
//我的收藏7
#import "ShouCangViewController.h"
//我的评论8
#import "PingLunViewController.h"
//我的咨询9
#import "ZiXunViewController.h"
//我的兑换10
#import "DuiHuanMaViewController.h"
//赠送记录11
#import "ZengSongViewController.h"
#import "HomeLiveViewController.h"
#import "SexBabyViewController.h"
#import "OpenViperViewController.h"
#import <Foundation/Foundation.h>
#import <AliyunOSSiOS/OSSService.h>
@interface MySetViewController ()<UITableViewDelegate,UITableViewDataSource,UIPickerViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    UIView *shadowView;
    NSArray *_iconArray;
    NSArray *_titleArray;
}
PropertyStrong(UIButton, setButton);
PropertyStrong(MysetBgView,mySetView);
PropertyStrong(UITableView, myTableView);
PropertyStrong(UIImagePickerController, picker);
PropertyStrong(UIImageView , twoImg);
@end

@implementation MySetViewController

-(void)viewWillAppear:(BOOL)animated {
    
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getIfoNotification) name:@"tongzhi" object:nil];
    
    [self getInfo];
    //    UIApplication.sharedApplication().setStatusBarStyle(UIStatusBarStyle.LightContent, animated: true)
    //    [self getInfo];
}
//- (void)getIfoNotification {
//    [self getInfo];
//}
- (void)getInfo {
    NSMutableDictionary *infoDic = [NSMutableDictionary dictionary];
    //    infoDic[@"user_id"] = KUser_id;
    //    infoDic[@"token"] = KToken;
    
    NSUserDefaults *detafaults = [NSUserDefaults standardUserDefaults];
    NSString  *user_id =  [detafaults objectForKey:@"user_id"];
    NSString *token =  [detafaults objectForKey:@"token"];
    infoDic[@"user_id"] = user_id;
    infoDic[@"token"] = token;
    NSLog(@"user_id ====%@",user_id);
    NSLog(@"token======%@",token);
    
    [GMAfnTools PostHttpDataWithUrlStr:KInfoShow Dic:infoDic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"获取个人信息成功 == %@",responseObject);
        PersonInfoModel *personModel = [[PersonInfoModel alloc]init];
        [personModel mj_setKeyValues:responseObject];
        NSLog(@"person==%@", personModel.nikname);
        self.mySetView.myNameLabel.text = personModel.nikname;
        [self.mySetView.headImg sd_setImageWithURL:[NSURL URLWithString:personModel.portrait]];
        [self.mySetView.headTapImage sd_setImageWithURL:[NSURL URLWithString:personModel.portrait] placeholderImage:[UIImage imageNamed:@"viper"]];
        self.mySetView.headTapImage.sd_cornerRadius = [NSNumber numberWithInteger:36];
        self.mySetView.validityLabel.text = ([personModel.vip_expire_time isEqualToString:@""] || personModel.vip_expire_time == nil) ? @"" : [NSString stringWithFormat:@"有效期至：%@", personModel.vip_expire_time];
        self.mySetView.openViper.enabled = [personModel.is_vip isEqualToString:@"2"];
        //        self.mySetView.headButton.layer.cornerRadius = 36;
        //        self.mySetView.headButton.layer.masksToBounds = YES;
    } FailureBlock:^(id  _Nonnull error) {
        NSLog(@"获取个人信息失败 == %@",error);
    }];
    
}
- (void)updateImg {
    [self getInfo];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateImg) name:@"updateImg" object:nil];
    
    self.view.backgroundColor = GMWhiteColor;
    self.picker.delegate = self;
    self.picker.allowsEditing = YES;
    self.navigationController.navigationBarHidden = YES;
    self.mySetView = [[MysetBgView alloc]initWithFrame:CGRectMake(0, 0, KScreenW, 278)];
    self.mySetView.myNameLabel.text = @"我的昵称";
    [self.view addSubview:self.mySetView];
    
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(noticeLogOut:) name:KLogOutNotifacation object:nil];
    
    //    [[NSNotificationCenter defaultCenter] postNotificationName:KLoginNotification object:nil];
    
    [self backgroundView];
    [self createTableView];
    
    [self getInfo];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showLogOut) name:KLogOutNotifacation object:nil];;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(creatTZ) name:KLoginNotification object:nil];
    [self creatTZ];
    //设置按钮
    __weak typeof(self) weakSelf = self;
    self.mySetView.setButtonBlock = ^(NSInteger indexSet) {
        [weakSelf setButtonClick];
    };
    //登录按钮
    self.mySetView.loginButtonBlock = ^(NSInteger indexLogin) {
        [weakSelf loginClick];
    };
    //注册按钮
    self.mySetView.registerbButtonBlock = ^(NSInteger indexRegisButton) {
        [weakSelf registerClick];
    };
    //我的资料
    self.mySetView.headButtonBlock = ^(NSInteger indexButton) {
        [weakSelf myDetailVC];
    };
    //开通会员
    self.mySetView.openButtonBlock = ^(NSInteger indexVip) {
        [weakSelf openVip];
    };
    //个人资料
    self.mySetView.titleBlock = ^(UIButton * _Nonnull btnTitle) {
        [weakSelf pushDetail];
    };
    
    //    [self getInfo];
    // Do any additional setup after loading the view.
}

- (void)pushDetail {
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"token"]) {
        LoginViewController *loginVC = [[LoginViewController alloc]init];
        loginVC.modalPresentationStyle = UIModalPresentationFullScreen;
        [self presentViewController:loginVC animated:YES completion:nil];
        return;
    }
    MyDetailViewController *detailVC = [[MyDetailViewController alloc]init];
    [detailVC setHidesBottomBarWhenPushed: YES];
    [self.navigationController pushViewController:detailVC animated:YES];
}

#pragma mark -- 退出登录
- (void)noticeLogOut:(id)sender {
    NSUserDefaults *account = [NSUserDefaults standardUserDefaults];
    NSString *token = [account objectForKey:@"token"];
    NSLog(@"+++++++%@",token);
    if (token) {
        
        self.mySetView.loginRegisterBgView.hidden = YES;
        self.mySetView.viperLoginView.hidden = NO;
        
    }else {
        
        self.mySetView.loginRegisterBgView.hidden = NO;
        self.mySetView.viperLoginView.hidden = YES;
    }
}

- (void) creatTZ {
    //    //通知登录成功 显示会员界面
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notice:) name:KLoginNotification object:nil];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *token = [defaults objectForKey:@"token"];
    NSLog(@"xxxxxxxxxx------%@",token);
    if (token) {
        self.mySetView.loginRegisterBgView.hidden = YES;
        self.mySetView.viperLoginView.hidden = NO;
        
    }else {
        self.mySetView.loginRegisterBgView.hidden = NO;
        self.mySetView.viperLoginView.hidden = YES;
    }
}

-(void)notice:(id)sender {
    NSLog(@"%@",sender);
    self.mySetView.loginRegisterBgView.hidden = YES;
    self.mySetView.viperLoginView.hidden = NO;
}
- (void)showLogOut {
    self.mySetView.loginRegisterBgView.hidden = NO;
    self.mySetView.viperLoginView.hidden = YES;
}
- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
}
#pragma mark -创建tableView

- (void)createTableView {
    
    _titleArray = @[@"我的余额",@"我的订单",@"我的购买",@"我的浏览",@"我的下载",@"我的直播",@"我的收藏",@"我的评论",@"我的咨询",@"使用兑换",@"赠送记录"];
    _iconArray = @[@"wd-ye-icon",@"ico_my_1_2",@"矢量智能对象",@"kecheng",@"download",@"矢量智能对象(1)",@"ico_my_1_4",@"ico_my_1_3",@"矢量智能对象(2)",@"矢量智能对象(3)",@"zengsongzhong"];
    self.myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, shadowView.frame.size.width, shadowView.frame.size.height) style:UITableViewStylePlain];
    self.myTableView.delegate = self;
    self.myTableView.dataSource = self;
    //隐藏滚动条
    self.myTableView.showsVerticalScrollIndicator = NO;
    self.myTableView.layer.cornerRadius = 10;
    [shadowView addSubview:self.myTableView];
}
#pragma mark - tableview delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _iconArray .count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 45;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellId = @"cellId";
    MysetTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[MysetTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        
    }
    cell.titleLabel.text = [_titleArray objectAtIndex:indexPath.row];
    cell.iconImg.image = [UIImage imageNamed:[_iconArray objectAtIndex:indexPath.row]];
    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch (indexPath.row) {
        case 0:
        {
            YueViewController *yueVC = [[YueViewController alloc]init];
            [yueVC setHidesBottomBarWhenPushed:YES];
            [self myDetailShowLogin:yueVC];
        }
            break;
        case 1:
        {
            DingdanViewController *dingdanVC = [[DingdanViewController alloc]init];
            [dingdanVC setHidesBottomBarWhenPushed:YES];
            [self myDetailShowLogin:dingdanVC];
            //            [self.navigationController pushViewController:dingdanVC animated:YES];
        }
            break;
        case 2:
        {
            MySetListViewController *listVC = [MySetListViewController new];
            listVC.type = MySetListTypeBuy;
            [listVC setHidesBottomBarWhenPushed:YES];
            listVC.title = @"我的购买";
            [self myDetailShowLogin:listVC];
            //            [self.navigationController pushViewController:goumaiVC animated:YES];
            
        }
            break;
        case 3:
        {
            MySetListViewController *listVC = [MySetListViewController new];
            listVC.type = MySetListTypeBrowse;
            listVC.title = @"我的浏览";
            [listVC setHidesBottomBarWhenPushed:YES];
            [self myDetailShowLogin:listVC];
            //            [self.navigationController pushViewController:liuLanVC animated:YES];
        }
            break;
        case 4:
        {
//            DownViewController *downVC = [[DownViewController alloc]init];
//            [downVC setHidesBottomBarWhenPushed:YES];
//            [self myDetailShowLogin:downVC];
            //            [self myDetailShowLogin:downVC];
            //            [self.navigationController pushViewController:downVC animated:YES];
        }
            break;
        case 5:
        {
            HomeLiveViewController *zhiboVC = [[HomeLiveViewController alloc]init];
            [zhiboVC setHidesBottomBarWhenPushed:YES];
            [self myDetailShowLogin:zhiboVC];
            //            [self.navigationController pushViewController:zhiboVC animated:YES];
        }
            break;
        case 6:
        {
            MySetListViewController *listVC = [MySetListViewController new];
            listVC.type = MySetListTypeCollect;
            listVC.title = @"我的收藏";
            [listVC setHidesBottomBarWhenPushed:YES];
            [self myDetailShowLogin:listVC];
        }
            break;
        case 7:
        {
            MyCommentViewController *commentVC = [MyCommentViewController new];
            [commentVC setHidesBottomBarWhenPushed:YES];
            [self myDetailShowLogin:commentVC];
            //            [self.navigationController pushViewController:pingLunVC animated:YES];
            
        }
            break;
        case 8:
        {
            ZiXunViewController *zxVC = [[ZiXunViewController alloc]init];
            [zxVC setHidesBottomBarWhenPushed:YES];
            [self myDetailShowLogin:zxVC];
        }
            break;
        case 9:
        {
            DuiHuanMaViewController *duiHuan = [[DuiHuanMaViewController alloc]init];
            [duiHuan setHidesBottomBarWhenPushed:YES];
            [self myDetailShowLogin:duiHuan];
        }
            break;
        case 10:
        {
            ZengSongViewController *zsVC = [[ZengSongViewController alloc]init];
            [zsVC setHidesBottomBarWhenPushed:YES];
            [self myDetailShowLogin:zsVC];
            //            [self.navigationController pushViewController:zsVC animated:YES];
        }
            break;
        default:
            break;
    }
}
- (void)backgroundView {
    shadowView = [[UIView alloc]init];
    shadowView.backgroundColor = GMWhiteColor;
    shadowView.layer.shadowColor = [UIColor blackColor].CGColor;
    shadowView.layer.shadowOffset = CGSizeMake(0, 3);
    shadowView.layer.shadowOpacity = 0.3;
    shadowView.layer.shadowRadius = 5.0;
    shadowView.layer.cornerRadius = 20.0;
    shadowView.layer.masksToBounds = NO;    //剪裁
    [self.view addSubview:shadowView];
    //卡顿的原因
    //    CGRect rectOfStatusbar = [[UIApplication sharedApplication] statusBarFrame];
    //    if (rectOfStatusbar.size.height == 20) {
    //
    shadowView.sd_layout.topSpaceToView(self.view, MStatusBarHeight+148)
    .bottomSpaceToView(self.view, 59)
    .leftSpaceToView(self.view, 10)
    .rightSpaceToView(self.view, 10)
    .widthIs(KScreenW-20).heightIs(KScreenH-168-59-MStatusBarHeight);
    //    }else {
    //
    //        shadowView.sd_layout.topSpaceToView(self.view, 188)
    //        .leftSpaceToView(self.view, 10)
    //        .rightSpaceToView(self.view, 10)
    //        .widthIs(KScreenW-20).heightIs(KScreenH-188-59-34);
    //
    //    }
}
//#pragma makr - 我的页面强制登录
//- (void)myDetailShowLogin:(UIViewController *)VC{
////    BOOL is_login = [[[NSUserDefaults standardUserDefaults] objectForKey:kIsLogin] boolValue];
////    NSLog(@"%@",/* DISABLES CODE */ (YES)?@"YES":@"NO");
//   NSUserDefaults *defaults =  [NSUserDefaults standardUserDefaults];
//    LoginViewController *loginVC = [[LoginViewController alloc]init];
//
//    if (![defaults objectForKey:@"token"]) {
//        [self presentViewController:loginVC animated:YES completion:nil];
//
//    }else {
//        [self.navigationController pushViewController:VC animated:YES];
//    }
//}
#pragma mark--我的资料
//- (void)myDetailVC {
//    NSLog(@"我的资料");
//
//}
- (void)sendUpZX:(NSString *)url {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *zxDic = [NSMutableDictionary dictionary];
    zxDic[@"user_id"] = [defaults objectForKey:@"user_id"];
    zxDic[@"token"] = [defaults objectForKey:@"token"];
    zxDic[@"portrait"] = url;
    NSLog(@"xxxxx---%@",url);
    //    zxDic[@"nikname"] = @"小七";
    
    [GMAfnTools PostHttpDataWithUrlStr:KChangeHeadImg Dic:zxDic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"上传成功 success==%@",responseObject);
        if ([defaults objectForKey:@"token"]) {
            if ([[responseObject objectForKey:@"code"] isEqualToString:@"1"]) {
                
                [self showError:[responseObject objectForKey:@"msg"]];
            }else {
                
                [self showError:[responseObject objectForKey:@"msg"]];
            }
        }else {
            [self showError:@"请先登录"];
        }
    } FailureBlock:^(id  _Nonnull error) {
        NSLog(@"上传失败 error == %@",error);
    }];
}
#pragma mark -- 提示框
- (void)showError:(NSString *)errorMsg {
    // 1.弹框提醒
    // 初始化对话框
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:errorMsg preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:nil]];
    // 弹出对话框
    [self presentViewController:alert animated:true completion:nil];
    
    [self performSelector:@selector(dimisssAlert:) withObject:alert afterDelay:1];
    
}

- (void) dimisssAlert:(UIAlertController *)alert
{
    if (alert) {
        [alert dismissViewControllerAnimated:YES completion:nil];
    }
    
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    //    获取图片
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(300, 300), NO, image.scale);
    [image drawInRect:(CGRect){0,0,CGSizeMake(300, 300)}];
    UIImage * normalizedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    image = normalizedImage;
    
    //    self.twoImg.image = image;
    self.mySetView.headImg.image = image;
    self.mySetView.headTapImage.image = image;
    [AliUpLoadImageTool upLoadImage:image success:^(NSString * _Nonnull url) {
        NSLog(@"xxxxxxxx%@",url);
        [self sendUpZX:url];
        [[NSNotificationCenter defaultCenter]postNotificationName:@"updateImg" object:nil];
    } failure:^(NSString * _Nonnull errorString) {
        NSLog(@"222222%@",errorString);
    }];
    
    //    获取图片后返回
    [picker dismissViewControllerAnimated:YES completion:nil];
}

//按取消按钮时候的功能
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    //    返回
    [picker dismissViewControllerAnimated:YES completion:nil];
}
- (UIImagePickerController *)picker
{
    if (!_picker) {
        _picker = [[UIImagePickerController alloc]init];
    }
    return _picker;
}
#pragma mark - 调用系统相册
- (void)myDetailVC {
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"token"]) {
        LoginViewController *loginVC = [[LoginViewController alloc]init];
        loginVC.modalPresentationStyle = UIModalPresentationFullScreen;
        [self presentViewController:loginVC animated:YES completion:nil];
        return;
    }
    NSLog(@"调用系统相册");
    
    UIAlertController *alertVc = [UIAlertController alertControllerWithTitle:@"请选择" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    //默认只有标题 没有操作的按钮:添加操作的按钮 UIAlertAction
    
    UIAlertAction *cancelBtn = [UIAlertAction actionWithTitle:@"相机" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self selectImg:10000];
    }];
    //添加确定
    UIAlertAction *sureBtn = [UIAlertAction actionWithTitle:@"相册" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull   action) {
        NSLog(@"确定");
        [self selectImg:10001];
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    //设置`确定`按钮的颜色
    //    [sureBtn setValue:[UIColor redColor] forKey:@"titleTextColor"];
    //将action添加到控制器
    [alertVc addAction:cancelBtn];
    [alertVc addAction :sureBtn];
    [alertVc addAction:cancel];
    //展示
    [self presentViewController:alertVc animated:YES completion:nil];
    
}

- (void)selectImg:(NSInteger)sender {
    
    BOOL isPicker = NO;
    
    switch (sender) {
        case 10000:
            //            打开相机
            isPicker = true;
            //            判断相机是否可用
            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                self.picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                isPicker = true;
            }
            break;
            
        case 10001:
            //            打开相册
            self.picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            isPicker = true;
            break;
            
        default:
            self.picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            isPicker = true;
            break;
    }
    
    if (isPicker) {
        self.picker.modalPresentationStyle = UIModalPresentationFullScreen;
        [self presentViewController:self.picker animated:YES completion:nil];
    }else {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"错误" message:@"相机不可用" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:nil];
        [alert addAction:action];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}
#pragma mark - 设置按钮
- (void)setButtonClick {
    NSLog(@"点击设置按钮");
    SetLoginOutViewController *loginOutVC = [[SetLoginOutViewController alloc]init];
    [loginOutVC setHidesBottomBarWhenPushed:YES];
    [self.navigationController pushViewController:loginOutVC animated:YES];
    
}

#pragma mark - 登录按钮
- (void)loginClick {
    NSLog(@"登录按钮");
    
    LoginViewController *loginVC = [[LoginViewController alloc]init];
    UINavigationController *loginNav = [[UINavigationController alloc]initWithRootViewController:loginVC];
    loginNav.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:loginNav animated:YES completion:nil];
}

#pragma mark - 注册按钮
- (void)registerClick {
    NSLog(@"注册按钮");
    RegisterViewController *regiterVC = [[RegisterViewController alloc]init];
    UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:regiterVC];
    nav.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:nav animated:YES completion:nil];
}
#pragma mark - 会员设置页面

- (void)vipShowLogin {
    //    NSLog(@"开通会员");
    
}
#pragma mark - 开通会员
- (void)openVip {
    NSLog(@"开通会员");
    OpenViperViewController *openVC = [[OpenViperViewController alloc]init];
    openVC.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:openVC animated:YES completion:nil];
    [openVC setHidesBottomBarWhenPushed:YES];
    //    [self.navigationController pushViewController:openVC animated:YES];
    //    [self.navigationController presentViewController:openVC animated:YES completion:nil];
    //    SexBabyViewController *sexBaby = [[SexBabyViewController alloc]init];
    //    [self presentViewController:sexBaby animated:YES completion:nil];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
