//
//  MyDetailViewController.m
//  yuduo
//
//  Created by mason on 2019/8/18.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "MyDetailViewController.h"
#import "BRTextField.h"
#import "BRPickerView.h"
#import "BabyRecordViewController.h"
#import "PersonInfoModel.h"
@interface MyDetailViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
PropertyStrong(UIImageView, bgImgView);
PropertyStrong(UIImageView, headImg);
PropertyStrong(UIImageView, logoImg);
PropertyStrong(UILabel, myLabel);
PropertyStrong(UIButton , backButton);
PropertyStrong(UITableView, dataTableView);
/** 地区 */
@property (nonatomic, strong) BRTextField *nameTF;
/** 出生年月 */
@property (nonatomic, strong) BRTextField *birthdayTF;
/** 性别 */
@property (nonatomic, strong) BRTextField *genderTF;
/** 地区 */
@property (nonatomic, strong) BRTextField *addressTF;
@property (nonatomic, strong) PersonInfoModel *infoModel;
PropertyStrong(UITextField,  textName);
@end

@implementation MyDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = RGB(244, 243, 244);
    [self getDataInfo];
    [self setNavigation];
    [self creteTableView];
    // Do any additional setup after loading the view.
}

#pragma mark -- 获取个人信息

- (void)getDataInfo {
    NSMutableDictionary *infoParms = [NSMutableDictionary dictionary];
    NSUserDefaults *detafaults = [NSUserDefaults standardUserDefaults];
    NSString  *user_id =  [detafaults objectForKey:@"user_id"];
    NSString *token =  [detafaults objectForKey:@"token"];
    infoParms[@"user_id"] = user_id;
    infoParms[@"token"] = token;
    __weak typeof(self) weakSelf = self;
    [GMAfnTools PostHttpDataWithUrlStr:KInfoShow Dic:infoParms SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"xxxxxx++++x%@",responseObject);
        PersonInfoModel *mol = [[PersonInfoModel alloc]init];
        [mol mj_setKeyValues:responseObject];
        weakSelf.infoModel = mol;
        [weakSelf.dataTableView reloadData];
        [weakSelf setupInfoView];
    } FailureBlock:^(id  _Nonnull error) {
         NSLog(@"获取个人信息%@",error);
    }];
}

- (void) creteTableView {
    self.dataTableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    self.dataTableView.delegate = self;
    self.dataTableView.dataSource = self;
    self.dataTableView.estimatedSectionFooterHeight = 10;
    self.dataTableView.estimatedSectionHeaderHeight = 10;
    [self.view addSubview:self.dataTableView];
    self.dataTableView.sd_layout.topSpaceToView(self.bgImgView, 10)
    .leftSpaceToView(self.view, 0)
    .widthIs(KScreenW).bottomEqualToView(self.view);
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 4) {
        BabyRecordViewController *recordVC = [[BabyRecordViewController alloc]init];
        [self.navigationController pushViewController:recordVC animated:YES];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *titleArray = @[@"昵称",@"年龄",@"性别",@"所在地",@"我的宝宝档案"];
    static NSString *cellID = @"cellID";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    for (UIView *subView in cell.contentView.subviews) {
        [subView removeFromSuperview];
    }
    UIImageView *imgV = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ico_next"]];
    [cell.contentView addSubview:imgV];
    imgV.sd_layout.topSpaceToView(cell.contentView, 18)
    .rightSpaceToView(cell.contentView, 15)
    .widthIs(8).heightIs(14);
    cell.textLabel.text = [titleArray objectAtIndex:indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    switch (indexPath.row) {
        case 0:
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
//            [self setupNameTF:cell];
            
            self.textName = [[UITextField alloc]init];
//            self.ttexextName.backgroundColor = GMlightGrayColor;
            self.textName.placeholder = @"请输入姓名";
            self.textName.textAlignment = NSTextAlignmentRight;
            self.textName.delegate = self;
            [self.textName addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
            [cell.contentView addSubview:self.textName];
            self.textName.sd_layout.topSpaceToView(cell.contentView, 10)
            .rightSpaceToView(cell.contentView, 30)
            .widthIs(230).heightIs(35);
        }
            break;
        case 1:
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
            [self setupAgeTF:cell];
        }
            break;
        case 2:
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
            [self setupGenderTF:cell];
        }
            break;
        case 3:
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
            [self setupAddressTF:cell];
        }
            break;
            
        default:
            break;
    }
    self->_nameTF.placeholder = _infoModel.nikname;
    if ([_infoModel.sex isEqualToString:@"1"]) {
        
        self->_genderTF.placeholder = @"男";
    } else {
        self->_genderTF.placeholder = @"女";
    }
    self->_addressTF.placeholder = [[_infoModel.province stringByAppendingString:_infoModel.city] stringByAppendingString:_infoModel.county];
    self.textName.placeholder = _infoModel.nikname;
    self.birthdayTF.text = _infoModel.age;
    return cell;
}

- (void)textFieldDidChange:(UITextField*)value {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
//    dic[@"user_id"] = KUser_id;
//    dic[@"token"] = KToken;
    NSUserDefaults *detafaults = [NSUserDefaults standardUserDefaults];
    NSString  *user_id =  [detafaults objectForKey:@"user_id"];
    NSString *token =  [detafaults objectForKey:@"token"];
    dic[@"user_id"] = user_id;
    dic[@"token"] = token;
    dic[@"nikname"] = value.text;
    
    [GMAfnTools PostHttpDataWithUrlStr:KChangeName Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"xxxxxxxxxxxx更改名字---%@",responseObject);
        if (responseObject) {
            [[NSNotificationCenter defaultCenter]postNotificationName:@"tongzhi" object:nil];
        }
    } FailureBlock:^(id  _Nonnull error) {
        
    }];
}
- (BRTextField *)getTextField:(UITableViewCell *)cell {
    BRTextField *textField = [[BRTextField alloc]initWithFrame:CGRectMake(SCREEN_WIDTH - 230, 0, 200, 50)];
    textField.backgroundColor = [UIColor clearColor];
    textField.font = [UIFont systemFontOfSize:16.0f];
    textField.textAlignment = NSTextAlignmentRight;
    textField.textColor = RGB_HEX(0x666666, 1.0);
    textField.delegate = self;
    [cell.contentView addSubview:textField];
    return textField;
}
#pragma mark - 年龄 textField
- (void)setupAgeTF:(UITableViewCell *)cell {
//    if (!_birthdayTF) {
        _birthdayTF = [self getTextField:cell];
        _birthdayTF.placeholder = @"请选择";
        __weak typeof(self) weakSelf = self;
        _birthdayTF.tapAcitonBlock = ^{
            [BRStringPickerView showStringPickerWithTitle:@"选择年龄" dataSource:@[@"70后", @"80后", @"90后", @"00后", @"10后"] defaultSelValue:@"90后" isAutoSelect:YES resultBlock:^(id selectValue) {
                weakSelf.birthdayTF.text = selectValue;
                
                NSMutableDictionary *ageDic = [NSMutableDictionary dictionary];
                NSUserDefaults *detafaults = [NSUserDefaults standardUserDefaults];
                NSString  *user_id =  [detafaults objectForKey:@"user_id"];
                NSString *token =  [detafaults objectForKey:@"token"];
                ageDic[@"user_id"] = user_id;
                ageDic[@"token"] = token;
                                ageDic[@"age"] = selectValue;
                
                                [weakSelf changeAge:ageDic];
                
            }];
            
            
        };
//    }
}
#pragma mark - 名字 textField
- (void)setupNameTF:(UITableViewCell *)cell {
//    if (!_nameTF) {
        _nameTF = [self getTextField:cell];
        _nameTF.placeholder = @"请输入";
        
//    }
}
#pragma mark - 性别 textField
- (void)setupGenderTF:(UITableViewCell *)cell {
//    if (!_genderTF) {
        _genderTF = [self getTextField:cell];
        
//        _genderTF.placeholder = @"请选择";
        __weak typeof(self) weakSelf = self;
        _genderTF.tapAcitonBlock = ^{
            [BRStringPickerView showStringPickerWithTitle:@"选择性别" dataSource:@[@"男", @"女"] defaultSelValue:@"男" isAutoSelect:YES resultBlock:^(id selectValue) {
                weakSelf.genderTF.text = selectValue;
               
                NSMutableDictionary *sexDic = [NSMutableDictionary dictionary];
                NSUserDefaults *detafaults = [NSUserDefaults standardUserDefaults];
                NSString  *user_id =  [detafaults objectForKey:@"user_id"];
                NSString *token =  [detafaults objectForKey:@"token"];
                sexDic[@"user_id"] = user_id;
                sexDic[@"token"] = token;
                if ([selectValue isEqualToString:@"男"]) {
                    sexDic[@"sex"] = @"1";
                }else {
                    sexDic[@"sex"] = @"2";
                }
                [weakSelf changeSex:sexDic];
            }];
        };
}

#pragma mark--更改性别
-(void)changeSex:(NSMutableDictionary *)parmasDic {
    [GMAfnTools PostHttpDataWithUrlStr:KChangeSex Dic:parmasDic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"======上传成功==%@",responseObject);
    } FailureBlock:^(id  _Nonnull error) {
        
    }];
}
#pragma mark--更改地址
-(void)changeAddress:(NSMutableDictionary *)parmasDic {
    [GMAfnTools PostHttpDataWithUrlStr:KChangeAddress Dic:parmasDic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"======上传成功==%@",responseObject);
    } FailureBlock:^(id  _Nonnull error) {
        
    }];
}
#pragma mark--更改年龄
- (void)changeAge:(NSMutableDictionary *)changeDic {
  
    [GMAfnTools PostHttpDataWithUrlStr:KChangeAge Dic:changeDic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"xxxxxxxxxxxx更改成功%@",responseObject);
    } FailureBlock:^(id  _Nonnull error) {
        NSLog(@"xxxxxxxx%@",error);
    }];
}
//
//#pragma mark - 出生日期 textField
//- (void)setupBirthdayTF:(UITableViewCell *)cell {
//    if (!_birthdayTF) {
//        _birthdayTF = [self getTextField:cell];
//        _birthdayTF.placeholder = @"请选择";
//        __weak typeof(self) weakSelf = self;
//        _birthdayTF.tapAcitonBlock = ^{
//            [BRDatePickerView showDatePickerWithTitle:@"出生年月" dateType:UIDatePickerModeDate defaultSelValue:weakSelf.birthdayTF.text minDateStr:@"" maxDateStr:[NSDate currentDateString] isAutoSelect:YES resultBlock:^(NSString *selectValue) {
//                weakSelf.birthdayTF.text = selectValue;
//            }];
//        };
//    }
//}

#pragma mark - 地址 textField
- (void)setupAddressTF:(UITableViewCell *)cell {
//    if (!_addressTF) {
        _addressTF = [self getTextField:cell];
        _addressTF.placeholder = @"请选择";
        __weak typeof(self) weakSelf = self;
        _addressTF.tapAcitonBlock = ^{
            [BRAddressPickerView showAddressPickerWithDefaultSelected:@[@0, @0, @0] isAutoSelect:YES resultBlock:^(NSArray *selectAddressArr) {
                weakSelf.addressTF.text = [NSString stringWithFormat:@"%@%@%@", selectAddressArr[0], selectAddressArr[1], selectAddressArr[2]];
//            NSLog(@"xxxxx省%@=市=%@=区=%@",selectAddressArr[0],selectAddressArr[1],selectAddressArr[2]);
                NSMutableDictionary *addressDic = [NSMutableDictionary dictionary];
                NSUserDefaults *detafaults = [NSUserDefaults standardUserDefaults];
                NSString  *user_id =  [detafaults objectForKey:@"user_id"];
                NSString *token =  [detafaults objectForKey:@"token"];
                addressDic[@"user_id"] = user_id;
                addressDic[@"token"] = token;
//                addressDic[@"user_id"] = KUser_id;
//                addressDic[@"token"] = KToken;
                addressDic[@"province"] = selectAddressArr[0];
                addressDic[@"city"] = selectAddressArr[1];
                addressDic[@"county"] = selectAddressArr[2];
                [weakSelf changeAddress:addressDic];
                
            }];
        };
//    }
}


- (void) setNavigation {
    
    self.bgImgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"矩形 5"]];
    self.bgImgView.userInteractionEnabled = YES;
    [self.view addSubview:self.bgImgView];
    self.bgImgView.sd_layout.topSpaceToView(self.view, 0)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .widthIs(KScreenW).heightIs(175);
    
    self.myLabel = [[UILabel alloc]init];
    self.myLabel.textAlignment = NSTextAlignmentCenter;
    [self.bgImgView addSubview:self.myLabel];
    NSMutableAttributedString *myString = [[NSMutableAttributedString alloc] initWithString:@"个人资料" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 18],NSForegroundColorAttributeName: [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0]}];
    self.myLabel.attributedText = myString;
    self.myLabel.sd_layout.topSpaceToView(self.bgImgView, MStatusBarHeight+13)
    .centerXEqualToView(self.bgImgView)
    .widthIs(140).heightIs(18);
    
    self.backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.backButton setBackgroundImage:[UIImage imageNamed:@"wback"] forState:UIControlStateNormal];
    [self.backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [self.bgImgView addSubview:self.backButton];
    self.backButton.sd_layout.topSpaceToView(self.bgImgView, MStatusBarHeight+12)
    .leftSpaceToView(self.bgImgView, 15)
    .widthIs(11).heightIs(19);
  
    UIButton *backButtonView = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 80, MStatusBarHeight+44)];
    backButtonView.backgroundColor = RGBA(15, 15, 15, 0);
    [backButtonView addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [self.bgImgView addSubview:backButtonView];
    
}

- (void)setupInfoView {
    UIButton *headButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.view addSubview:headButton];
    [headButton setBackgroundImage:[UIImage imageNamed:@"viper"] forState:UIControlStateNormal];
    headButton.sd_cornerRadius = [NSNumber numberWithInteger:36];
    headButton.clipsToBounds = YES;
    headButton.sd_layout.topSpaceToView(self.view, 56+MStatusBarHeight)
    .leftSpaceToView(self.view, 15)
    .widthIs(72).heightIs(72);

    UILabel *myNameLabel = [[UILabel alloc]init];
    myNameLabel.text = @"我的昵称";
    myNameLabel.textColor = GMWhiteColor;
    [self.view addSubview:myNameLabel];
    myNameLabel.sd_layout.topSpaceToView(self.view, MStatusBarHeight+72)
    .leftSpaceToView(headButton, 16)
    .heightIs(17).rightSpaceToView(self.view, 20);
    
    UILabel *validityLabel = [[UILabel alloc]init];
    validityLabel.text = @"有效期至 : 2020-05-05";
    validityLabel.textColor = GMWhiteColor;
    validityLabel.font = [UIFont fontWithName:@"PingFang SC" size: 12];
    [self.view addSubview:validityLabel];
    validityLabel.sd_layout.topSpaceToView(myNameLabel, 10)
    .leftSpaceToView(headButton, 16)
    .widthIs(250).heightIs(12);
    
    [headButton sd_setImageWithURL:[NSURL URLWithString:_infoModel.portrait] forState:UIControlStateNormal];
    myNameLabel.text = _infoModel.nikname;
    validityLabel.text = ([_infoModel.vip_expire_time isEqualToString:@""] || _infoModel.vip_expire_time == nil) ? @"" : [NSString stringWithFormat:@"有效期至：%@", _infoModel.vip_expire_time];
}

- (void)back {
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
