//
//  OpenViperViewController.m
//  yuduo
//
//  Created by Mac on 2019/9/18.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "OpenViperViewController.h"
#import "OpenVipList.h"
#import "OpenViperCollectionViewCell.h"
#import "PersonInfoModel.h"
@interface OpenViperViewController ()<UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource>
PropertyStrong(UIImageView, headView);
PropertyStrong(UIImageView, titleImgView);
PropertyStrong(UIImageView, headImg);
PropertyStrong(UILabel, nameLabel);
PropertyStrong(UILabel, vipShowLabel);
PropertyStrong(UILabel, openVipMsg);
PropertyStrong(NSString, priceString);
PropertyStrong(NSMutableArray, vipItemArray);
@property(nonatomic, assign) NSInteger vipId;
@end

@implementation OpenViperViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.vipItemArray = [[NSMutableArray alloc] init];
    [self setNavigation];
    [self getvipListData];
    [self getInfo];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(popMethod) name:@"popName" object:nil];
    // Do any additional setup after loading the view.
}

- (void)getInfo {
    NSMutableDictionary *infoDic = [NSMutableDictionary dictionary];
    NSUserDefaults *detafaults = [NSUserDefaults standardUserDefaults];
    NSString  *user_id =  [detafaults objectForKey:@"user_id"];
    NSString *token =  [detafaults objectForKey:@"token"];
    infoDic[@"user_id"] = user_id;
    infoDic[@"token"] = token;
    __weak typeof(self) weakSelf = self;
    [GMAfnTools PostHttpDataWithUrlStr:KInfoShow Dic:infoDic SuccessBlock:^(id  _Nonnull responseObject) {
        PersonInfoModel *personModel = [[PersonInfoModel alloc]init];
        [personModel mj_setKeyValues:responseObject];
        weakSelf.nameLabel.text = personModel.nikname;
        [weakSelf.titleImgView sd_setImageWithURL:[NSURL URLWithString:personModel.portrait]];
        weakSelf.vipShowLabel.text = personModel.is_vip == 2 ? @"未开通VIP" : @"";
    } FailureBlock:^(id  _Nonnull error) {
        NSLog(@"获取个人信息失败 == %@",error);
    }];

}

- (void)popMethod {
    [self dismissViewControllerAnimated:YES completion:nil];
    //    [self backBtn];
}
- (void)payButton:(UIButton *)send {
    NSLog(@"点击开通会员--%@",self.priceString);
    if (self.vipId == 0) {
        [self showError:@"请选择开通类型"];
        return;
    }
    NSUserDefaults *defaulst = [NSUserDefaults standardUserDefaults];
    NSString *user_id = [defaulst objectForKey:@"user_id"];
    NSString *token = [defaulst objectForKey:@"token"];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"user_id"] = user_id;
    dic[@"token"] = token;
    dic[@"vip_id"] = [NSString stringWithFormat:@"%ld", self.vipId];
    NSLog(@"------888=%@",self.priceString);
    [GMAfnTools PostHttpDataWithUrlStr:KOpenViper Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"开通会员成功---------%@",responseObject);
        [self showError:[responseObject objectForKey:@"msg"]];
        if([[responseObject objectForKey:@"code"] isEqualToString:@"1"])
        {
            [self dismissViewControllerAnimated:true completion:nil];
            //开通成功修改会员名字
            [[NSNotificationCenter defaultCenter]postNotificationName:@"changeVIP" object:nil];
            [[NSNotificationCenter defaultCenter]postNotificationName:@"changeVipTitle" object:nil];

        }
    } FailureBlock:^(id  _Nonnull error) {
        NSLog(@"----开通错误---%@",error);
    }];
}
#pragma mark -- 提示框
- (void)showError:(NSString *)errorMsg {
    // 1.弹框提醒
    // 初始化对话框
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:errorMsg preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    }]];
    // 弹出对话框
    [self presentViewController:alert animated:true completion:nil];
    
    //    [self performSelector:@selector(dimisssAlert:) withObject:alert afterDelay:1];
    
}

//- (void) dimisssAlert:(UIAlertController *)alert
//{
//    if (alert) {
//        [alert dismissViewControllerAnimated:YES completion:nil];
//    }
//
//}

- (void)setNavigation {
    self.view.backgroundColor = RGB(244, 243, 244);
    
    UIView *titleView = [[UIView alloc]init];
    titleView.backgroundColor = GMWhiteColor;
    [self.view addSubview:titleView];
    titleView.sd_layout.topSpaceToView(self.view, 0)
    .leftEqualToView(self.view)
    .widthIs(KScreenW).heightIs(44+MStatusBarHeight);
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //    backButton.backgroundColor = GMBlueColor;
    [backButton setBackgroundImage:[UIImage imageNamed:@"back-icon"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
    [titleView addSubview:backButton];
    backButton.sd_layout.topSpaceToView(titleView,MStatusBarHeight+15)
    .leftSpaceToView(titleView, 15)
    .widthIs(11).heightIs(19);
    UIButton *backButtonView = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 80, MStatusBarHeight+44)];
    backButtonView.backgroundColor = RGBA(15, 15, 15, 0);
    [backButtonView addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButtonView];
    
    UILabel *forgetPsw = [[UILabel alloc]init];
    forgetPsw.text = @"VIP会员";
    forgetPsw.font = kFont(18);
    forgetPsw.textAlignment = NSTextAlignmentCenter;
    [titleView addSubview:forgetPsw];
    forgetPsw.sd_layout.topSpaceToView(titleView, MStatusBarHeight+15)
    .centerXEqualToView(titleView).widthIs(150).heightIs(17);
    
    self.headView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"vipBG"]];
    [self.view addSubview:self.headView];
    self.headView.sd_layout.topSpaceToView(self.view, MStatusBarHeight+44)
    .leftSpaceToView(self.view, 0)
    .rightSpaceToView(self.view, 0)
    .heightIs(94);
    
    self.headImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"vipHead"]];
    [self.headView addSubview:self.headImg];
    self.headImg.sd_layout.topSpaceToView(self.headView, 31)
    .leftSpaceToView(self.headView, 11)
    .rightSpaceToView(self.headView, 11)
    .heightIs(108);
    
    self.titleImgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"viper"]];
    //    self.titleImgView.backgroundColor = GMlightGrayColor;
    self.titleImgView.layer.cornerRadius = 30;
    self.titleImgView.clipsToBounds = true;
    [self.headImg addSubview:self.titleImgView];
    self.titleImgView.sd_layout.topSpaceToView(self.headImg, 25)
    .leftSpaceToView(self.headImg, 20)
    .widthIs(60).heightIs(60);
    
    //姓名
    self.nameLabel = [[UILabel alloc]init];
    self.nameLabel.textColor = RGB(69, 69, 69);
    self.nameLabel.text = @"庞亚楠-父母必读";
    self.nameLabel.font = [UIFont fontWithName:KPFType size:15];
    [self.headImg addSubview:self.nameLabel];
    self.nameLabel.sd_layout.topSpaceToView(self.headImg, 34)
    .leftSpaceToView(self.titleImgView, 17)
    .widthIs(120).heightIs(14);
    
    self.vipShowLabel = [[UILabel alloc]init];
    self.vipShowLabel.text = @"VIP已过期";
    self.vipShowLabel.textColor = RGB(228, 102, 67);
    self.vipShowLabel.font = [UIFont fontWithName:KPFType size:12];
    [self.headImg addSubview:self.vipShowLabel];
    self.vipShowLabel.sd_layout.topSpaceToView(self.nameLabel, 10)
    .leftSpaceToView(self.titleImgView, 17)
    .widthIs(60).heightIs(13);
    
    self.openVipMsg = [[UILabel alloc]init];
    self.openVipMsg.text = @"开通育朵VIP会员卡";
    self.openVipMsg.textColor = RGB(69, 69, 69);
    self.openVipMsg.font = [UIFont fontWithName:KPFType size:15];
    self.openVipMsg.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:self.openVipMsg];
    self.openVipMsg.sd_layout.topSpaceToView(self.view, MStatusBarHeight+209)
    .centerXEqualToView(self.view)
    .widthIs(130).heightIs(15);
    
    self.flowLayout = [[UICollectionViewFlowLayout alloc]init];
    self.flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    
    self.collectionV = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:self.flowLayout];
    self.collectionV.backgroundColor = RGB(244, 243, 244);
    self.collectionV.delegate = self;
    self.collectionV.dataSource = self;
    [self.collectionV registerClass:[OpenViperCollectionViewCell class] forCellWithReuseIdentifier:@"collection"];
    [self.view addSubview:self.collectionV];
    self.collectionV.sd_layout.topSpaceToView(self.openVipMsg, 34)
    .leftSpaceToView(self.view, 15)
    .rightSpaceToView(self.view, 15)
    .heightIs(140);
    
    UILabel *continueLablel = [[UILabel alloc]init];
    continueLablel.textColor = RGB(69, 69, 69);
    continueLablel.text = @"自动续费服务说明";
//    [self.view addSubview:continueLablel];
    continueLablel.sd_layout.topSpaceToView(self.collectionV, 30)
    .leftSpaceToView(self.view, 18)
    .widthIs(200).heightIs(15);
    
    //支付
    UIButton *payButton = [UIButton buttonWithType:UIButtonTypeCustom];
    payButton.backgroundColor = RGB(23, 153, 167);
    payButton.layer.cornerRadius = 22.5;
    [payButton addTarget:self action:@selector(payButton:) forControlEvents:UIControlEventTouchUpInside];
    [payButton setTitleColor:GMWhiteColor forState:UIControlStateNormal];
    [payButton setTitle:@"确认支付" forState:UIControlStateNormal];
    [self.view addSubview:payButton];
    payButton.sd_layout.bottomSpaceToView(self.view, 40)
    .leftSpaceToView(self.view, 15)
    .rightSpaceToView(self.view, 15)
    .heightIs(45);
    
    UILabel *oneLabel = [[UILabel alloc]init];
    oneLabel.text = @"付款: 用户确认购买付款后计入iTunes账户";
//    oneLabel.text = @"1、付款: 用户确认购买付款后计入iTunes账户 \r\n2、取消续订：如果取消续订，请在当前订阅周期到期前手动取消自动续费功能";
    oneLabel.textColor = RGB(153, 153, 153);
    oneLabel.numberOfLines = 0;
    oneLabel.font = [UIFont fontWithName:KPFType size:14];
//    [self.view addSubview:oneLabel];
    oneLabel.sd_layout.topSpaceToView(continueLablel, 18)
    .leftSpaceToView(self.view, 16)
    .widthIs(KScreenW-32).autoHeightRatio(0);
    
}

- (void)getvipListData {
    NSUserDefaults *defaulst = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"user_id"] = [defaulst objectForKey:@"user_id"];
    dic[@"token"] = [defaulst objectForKey:@"token"];
    __weak typeof(self) weakSelf = self;
    [GMAfnTools PostHttpDataWithUrlStr:KVipLists Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"开通会员成功---------%@",responseObject);
        for (NSMutableDictionary *dic in responseObject) {
            OpenVipList *model = [OpenVipList new];
            [model mj_setKeyValues:dic];
            [weakSelf.vipItemArray addObject:model];
        }
        [weakSelf.collectionV reloadData];
    } FailureBlock:^(id  _Nonnull error) {
        NSLog(@"----开通错误---%@",error);
    }];
}
#pragma mark -- collectionView delegate , datasource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.vipItemArray.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((collectionView.frame.size.width-30)/3 ,100);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    OpenViperCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"collection" forIndexPath:indexPath];
    
    cell.layer.cornerRadius = 5.0f;
    cell.layer.borderWidth = 0.5f;
    cell.layer.borderColor = RGB(252, 171, 71).CGColor;
    cell.layer.masksToBounds = NO;
    OpenVipList *model = self.vipItemArray[indexPath.row];
    cell.priceLabel.text = model.vip_price;
    cell.vipLabel.text = model.title;
//    cell.numPriceLabel.text = [numPrice objectAtIndex:indexPath.row];
    
    return cell;
}



-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    OpenViperCollectionViewCell *cell = (OpenViperCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    cell.backgroundColor = RGB(21, 153, 164);
    cell.priceLabel.textColor = GMWhiteColor;
    cell.vipLabel.textColor = GMWhiteColor;
    self.priceString = [NSString stringWithFormat:@"%ld",(long)indexPath.row];
    OpenVipList *model = self.vipItemArray[indexPath.row];

    self.vipId = model.vip_id;
}

-(void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    OpenViperCollectionViewCell *cell = (OpenViperCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    cell.backgroundColor = GMWhiteColor;
    cell.priceLabel.textColor = RGB(228, 102, 67);
    cell.vipLabel.textColor = RGB(69, 69, 69);
}



- (void)backBtn {
    NSLog(@"返回返回");
    //    [self.navigationController popViewControllerAnimated:YES];
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
