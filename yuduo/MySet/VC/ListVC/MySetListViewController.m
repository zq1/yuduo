//
//  MySetListViewController.m
//  yuduo
//
//  Created by zhouqi on 2019/10/26.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "MySetListViewController.h"
#import "ZJScrollPageView.h"
#import "MySetListDetailViewController.h"

@interface MySetListViewController ()<ZJScrollPageViewDelegate>
@property(strong, nonatomic)NSArray<NSString *> *titles;
@property(strong, nonatomic)NSArray<UIViewController *> *childVcs;
@property (nonatomic, strong) ZJScrollPageView *scrollPageView;
@end

@implementation MySetListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = GMWhiteColor;
    [self setNavigation];
    [self setScrollView];
}

- (void)setNavigation {
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:[UIImage imageNamed:@"back-icon"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButton) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButton];
    backButton.sd_layout.topSpaceToView(self.view,MStatusBarHeight+12)
    .leftSpaceToView(self.view, 15)
    .widthIs(11).heightIs(19);
    UIButton *backButtonView = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 80, MStatusBarHeight+44)];
    backButtonView.backgroundColor = RGBA(15, 15, 15, 0);
    [backButtonView addTarget:self action:@selector(backButton) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButtonView];
    
    UILabel *titleLabel = [[UILabel alloc]init];
    [self.view addSubview:titleLabel];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:self.title attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 18],NSForegroundColorAttributeName: [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1.0]}];
    titleLabel.attributedText = string;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.sd_layout.topSpaceToView(self.view, MStatusBarHeight+13)
    .centerXEqualToView(self.view)
    .heightIs(18).widthIs(100);
}

- (void)backButton {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setScrollView {
    
    ZJSegmentStyle *style = [[ZJSegmentStyle alloc] init];
    style.showLine = YES;
    style.scrollTitle = NO;
    style.autoAdjustTitlesWidth = true;
    style.adjustCoverOrLineWidth = YES;
    style.gradualChangeTitleColor = YES;
    style.normalTitleColor = RGB(153, 153, 153);
    style.scrollLineColor = RGB(21, 152, 164);
    style.selectedTitleColor = RGB(21, 152, 164);
    
    self.titles = @[@"课程",@"专栏",@"活动",@"文章"];
    _scrollPageView = [[ZJScrollPageView alloc] initWithFrame:CGRectMake(0, 44+MStatusBarHeight, self.view.bounds.size.width, self.view.bounds.size.height - 44-MStatusBarHeight) segmentStyle:style titles:self.titles parentViewController:self delegate:self];

    [self.view addSubview:_scrollPageView];
}

- (NSInteger)numberOfChildViewControllers {
    return self.titles.count;
}

- (UIViewController<ZJScrollPageViewChildVcDelegate> *)childViewController:(UIViewController<ZJScrollPageViewChildVcDelegate> *)reuseViewController forIndex:(NSInteger)index {
    UIViewController<ZJScrollPageViewChildVcDelegate> *childVc = reuseViewController;
    
    if (!childVc) {
        NSInteger detailType = index+1;
        if (index == 2) {
            detailType = 4;
        } else if (index == 3) {
            detailType = 3;
        }
        childVc = [MySetListDetailViewController assignWithListType:self.type detailType:detailType];
    }
    
    return childVc;
}


- (BOOL)shouldAutomaticallyForwardAppearanceMethods {
    return NO;
}

@end
