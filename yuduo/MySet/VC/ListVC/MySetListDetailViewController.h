//
//  MySetListDetailViewController.h
//  yuduo
//
//  Created by zhouqi on 2019/10/26.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZJScrollPageViewDelegate.h"

NS_ASSUME_NONNULL_BEGIN

@interface MySetListDetailViewController : UITableViewController<ZJScrollPageViewChildVcDelegate>
+ (MySetListDetailViewController *)assignWithListType: (NSInteger)type detailType: (NSInteger)detailType;
@end

NS_ASSUME_NONNULL_END
