//
//  MySetListDetailViewController.m
//  yuduo
//
//  Created by zhouqi on 2019/10/26.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "MySetListDetailViewController.h"
#import "SouCangCourseModel.h"
#import "ZhuanLanTableViewCell.h"
#import "MyActiviteModel.h"
#import "ActiveCenterOnlineTableViewCell.h"
#import "ActiveCenterDefultTableViewCell.h"
#import "GouMaiTableViewCell.h"
#import "JingxuanTableViewCell.h"
#import "YDSelectModel.h"

@interface MySetListDetailViewController ()
@property(assign, nonatomic) NSInteger type;
@property(assign, nonatomic) NSInteger detailType;
PropertyStrong(NSMutableArray, courseCollectArray);
@end

@implementation MySetListDetailViewController

+ (MySetListDetailViewController *)assignWithListType: (NSInteger)type detailType: (NSInteger)detailType {
    MySetListDetailViewController *detailVC = [[MySetListDetailViewController alloc] initWithStyle:(UITableViewStylePlain)];
    detailVC.type = type;
    detailVC.detailType = detailType;
    return detailVC;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = GMWhiteColor;
    self.tableView.estimatedRowHeight = 150;
    self.tableView.estimatedSectionHeaderHeight = 0;
    self.tableView.estimatedSectionFooterHeight = 0;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:(CGRectZero)];
    __weak typeof(self)  weakSelf = self;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf requestData];
    }];
    [self.tableView.mj_header beginRefreshing];
}

- (void)requestData {
    NSString *url = KBrowseList;
    if (self.type == 1) {
        url = KGouMaiListURL;
    } else if (self.type == 2) {
        url = KCollectList;
    }
    self.courseCollectArray = [NSMutableArray array];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *user_id = [defaults objectForKey:@"user_id"];
    NSString *token = [defaults objectForKey:@"token"];
    NSMutableDictionary *courseDic = [NSMutableDictionary dictionary];
    courseDic[@"user_id"] = user_id;
    courseDic[@"token"] = token;
    courseDic[@"type"] = [NSString stringWithFormat:@"%ld", self.detailType];
    __weak typeof(self)  weakSelf = self;
    [GMAfnTools PostHttpDataWithUrlStr:url Dic:courseDic SuccessBlock:^(id  _Nonnull responseObject) {
        [weakSelf.tableView.mj_header endRefreshing];
        NSLog(@"MySetListDetailViewController===%@",responseObject);
        for (NSMutableDictionary *dic in responseObject) {
            if (weakSelf.detailType < 3) {
                SouCangCourseModel *soucangCM = [[SouCangCourseModel alloc]init];
                [soucangCM mj_setKeyValues:dic];
                [self.courseCollectArray addObject:soucangCM];
            } else if (weakSelf.detailType == 4) {
                MyActiviteModel *model = [MyActiviteModel new];
                [model mj_setKeyValues:dic];
                [self.courseCollectArray addObject:model];
            } else if (weakSelf.detailType == 3) {
                YDSelectModel *model = [YDSelectModel new];
                [model mj_setKeyValues:dic];
                [self.courseCollectArray addObject:model];
            }
        }
        [self.tableView reloadData];
    } FailureBlock:^(id  _Nonnull error) {
        NSLog(@"MySetListDetailViewController列表错误===%@",error);
        [weakSelf.tableView.mj_header endRefreshing];
    }];
    
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.courseCollectArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (_detailType == 2) {
        NSString * cellID = @"ZhuanLanTableViewCell";
        ZhuanLanTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        if (!cell) {
            cell = [[ZhuanLanTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        }
        cell.backgroundColor = RGB(244, 243, 244);
        SouCangCourseModel *model = [self.courseCollectArray objectAtIndex:indexPath.row];
        cell.titleLable.text = self.type == 2 ? model.course_name : model.name;
        NSString *cover = ([model.cover isEqualToString:@""] || model.cover == nil) ? model.course_list_cover : model.cover;
        [cell.headImg sd_setImageWithURL:[NSURL URLWithString:cover]];
        cell.headImg.sd_cornerRadius = [NSNumber numberWithInt:10];
        cell.headImg.sd_cornerRadius = [NSNumber numberWithInteger:10];
        cell.peopleLable.text = model.course_vrows;
        cell.priceLabel.text = self.type == 2 ? model.course_price : model.pay_price;
        cell.lineLabel.text = self.type == 2 ? model.course_under_price : model.under_price;
        cell.xPriceLabel.text = self.type == 2 ? model.course_under_price : model.under_price;
        cell.keshiLable.text = model.class_hour;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    } else if (_detailType == 1) {
        static NSString *cellID = @"GouMaiTableViewCell";
        GouMaiTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        if (!cell) {
            cell = [[GouMaiTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        }
        SouCangCourseModel *model = [self.courseCollectArray objectAtIndex:indexPath.row];
        if (self.type == 2) {
            cell.titleLabel.text = model.course_name;
            [cell.headImgView sd_setImageWithURL:[NSURL URLWithString:model.course_list_cover]];
            cell.headImgView.sd_cornerRadius = [NSNumber numberWithInteger:10];
            cell.xPriceLabel.text = model.course_under_price;
            cell.priceLabel.text = model.course_price;
            cell.lineLabel.text = model.course_under_price;
        } else {
            cell.titleLabel.text = model.name;
            [cell.headImgView sd_setImageWithURL:[NSURL URLWithString:model.cover]];
            cell.headImgView.sd_cornerRadius = [NSNumber numberWithInteger:10];
            cell.xPriceLabel.text = model.under_price;
            cell.priceLabel.text = model.pay_price;
            cell.lineLabel.text = model.under_price;
        }
        cell.detailLabel.text = model.brief;
        cell.keshiLabel.text = model.class_hour;
        cell.peoplesLabel.text = model.course_vrows;
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return cell;
    } else if (_detailType == 3) {
        JingxuanTableViewCell *cell6 = [[JingxuanTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellIdentifier6"];
        YDSelectModel *ydSelectM = [self.courseCollectArray objectAtIndex:indexPath.row];
        cell6.titleLabel.text = ydSelectM.article_title;
        cell6.zanLabel.text = [ydSelectM.thumbs_up stringByAppendingString:@"人"];
        cell6.mianfeiLabel.hidden = YES;
        cell6.titleLabel.sd_layout.leftSpaceToView(cell6.headImgView, 17).widthIs(226*KScreenW/375);
        cell6.zanLabel.text = ydSelectM.article_vrows;
        [cell6.headImgView sd_setImageWithURL:[NSURL URLWithString:ydSelectM.article_cover]];
        CGFloat price = ydSelectM.article_price.floatValue;
        if (price > 0) {
            cell6.priceLabel.text = [NSString stringWithFormat:@"¥%.2f", price];
        } else {
            cell6.priceLabel.text = @"免费";
        }
        
        cell6.messageLabel.text = ydSelectM.comment_count;
        cell6.detailLabel.text = ydSelectM.brief;
        cell6.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell6;
    }
    else if (_detailType == 4) {
        MyActiviteModel *activeModel = [self.courseCollectArray objectAtIndex:indexPath.row];
        if ([activeModel.activity_type integerValue] == 1) {
            static NSString *cellID = @"cellOnlineID";
            ActiveCenterOnlineTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
            //    ActiveAddressTableViewCell *cell1 = [tableView dequeueReusableCellWithIdentifier:cellID1];
            if (!cell) {
                cell = [[ActiveCenterOnlineTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
            }
            cell.myAcModel = activeModel;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        } else {
            static NSString *cellID = @"cellDefultID";
            ActiveCenterDefultTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
            if (!cell) {
                cell = [[ActiveCenterDefultTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
            }
            cell.myAcModel = activeModel;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
    }
    
    return [UITableViewCell new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.detailType == 1) {
        return 170;
    } else if (self.detailType == 2) {
        return 130;
    } else if (self.detailType == 4) {
        MyActiviteModel *activeModel = [self.courseCollectArray objectAtIndex:indexPath.row];
        if ([activeModel.activity_type integerValue] == 1) {
            return 270;
        } else {
            return  290;
        }
    }
    return 110;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
