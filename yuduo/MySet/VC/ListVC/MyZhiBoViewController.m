//
//  MyZhiBoViewController.m
//  yuduo
//
//  Created by Mac on 2019/8/13.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "MyZhiBoViewController.h"
#import "HomeLiveTableViewCell.h"
@interface MyZhiBoViewController ()
<UITableViewDelegate,UITableViewDataSource>
PropertyStrong(UITableView, liveTableView);
@end

@implementation MyZhiBoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = RGB(244, 243, 244);
    [self setNavigation];
    [self createTableView];
    // Do any additional setup after loading the view.
}

- (void)createTableView {
    self.liveTableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.liveTableView.delegate = self;
    self.liveTableView.dataSource = self;
    
    [self.view addSubview:self.liveTableView];
    self.liveTableView.sd_layout.topSpaceToView(self.view, 54+MStatusBarHeight)
    .leftSpaceToView(self.view, 15)
    .rightSpaceToView(self.view, 15)
    .widthIs(KScreenW-30).heightIs(KScreenH-54-MStatusBarHeight);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 239*kGMHeightScale;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellId = @"cellID";
    HomeLiveTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    
    if (!cell) {
        
        cell = [[HomeLiveTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];;
        
    }
    
    cell.backgroundColor = RGB(244, 243, 244);
    return cell;
}

- (void)getLiveData {

    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)setNavigation {
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:[UIImage imageNamed:@"back-icon"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButton) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButton];
    backButton.sd_layout.topSpaceToView(self.view,MStatusBarHeight+12)
    .leftSpaceToView(self.view, 15)
    .widthIs(11).heightIs(19);
    UIButton *backButtonView = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 80, MStatusBarHeight+44)];
    backButtonView.backgroundColor = RGBA(15, 15, 15, 0);
    [backButtonView addTarget:self action:@selector(backButton) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButtonView];
    UILabel *titleLabel = [[UILabel alloc]init];
    
    [self.view addSubview:titleLabel];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"我的直播" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 18],NSForegroundColorAttributeName: [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1.0]}];
    
    titleLabel.attributedText = string;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.sd_layout.topSpaceToView(self.view, MStatusBarHeight+13)
    .centerXEqualToView(self.view)
    .heightIs(18).widthIs(100);
}
- (void)backButton {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
