//
//  ZCTableViewCell.m
//  yuduo
//
//  Created by Mac on 2019/8/14.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "ZCTableViewCell.h"

@implementation ZCTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier]) {
        
        [self createLayout];
    }
    return self;
}

- (void)setModel:(GiveListModel *)model {
    _model = model;
    if (model.type == 1) {
        self.typeLabel.backgroundColor = RGB(252, 171, 71);
        self.typeLabel.text = @"课程";
    } else if (model.type == 2) {
        self.typeLabel.text = @"专栏";
      self.typeLabel.backgroundColor = RGB(0, 152, 165);
    } else if (model.type == 3) {
        self.typeLabel.text = @"文章";
         self.typeLabel.backgroundColor = RGB(230, 102, 60);
    } else {
        self.typeLabel.text = @"活动";
        self.typeLabel.backgroundColor = RGB(98, 205, 152);
    }
    self.titleLabel.text = model.name;
    self.timeLabel.text = model.give_time;
}

- (void)createLayout {
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = GMWhiteColor;
    self.bgView.layer.cornerRadius = 10;
    [self.contentView addSubview:self.bgView];
    self.bgView.sd_layout.topSpaceToView(self.contentView, 0)
    .leftSpaceToView(self.contentView, 0)
    .rightSpaceToView(self.contentView, 0)
    .widthIs(KScreenW).bottomEqualToView(self.contentView);
    
    self.typeLabel = [UILabel new];
    [self.bgView addSubview:self.typeLabel];
    self.typeLabel.sd_layout.topSpaceToView(self.bgView, 15).leftSpaceToView(self.bgView, 15).heightIs(50).widthIs(50);
    self.typeLabel.textColor = [UIColor whiteColor];
    self.typeLabel.font = [UIFont systemFontOfSize:18];
    self.typeLabel.textAlignment = NSTextAlignmentCenter;
    self.typeLabel.layer.cornerRadius = 25;
    self.typeLabel.clipsToBounds = YES;
    
    self.headImg = [[UIImageView alloc]init];
    self.headImg.backgroundColor = GMlightGrayColor;
    self.headImg.layer.cornerRadius = 23;
//    [self.bgView addSubview:self.headImg];
    self.headImg.sd_layout.topSpaceToView(self.bgView, 17)
    .leftSpaceToView(self.bgView, 15)
    .widthIs(46).heightEqualToWidth();
    
    
    self.ZSButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.ZSButton.backgroundColor = RGB(224, 91, 59);
    [self.ZSButton setTitle:@"赠送" forState:UIControlStateNormal];
    self.ZSButton.titleLabel.font = [UIFont systemFontOfSize:13];
    self.ZSButton.layer.cornerRadius = 15;
    [self.ZSButton setTitleColor:GMWhiteColor forState:UIControlStateNormal];
    [self .bgView addSubview:self.ZSButton];
    self.ZSButton.sd_layout.topSpaceToView(self.bgView, 30)
    .rightSpaceToView(self.bgView, 10)
    .centerYIs(self.bgView.frame.size.height)
    .widthIs(60).heightIs(30);
    
    self.titleLabel = [[UILabel alloc]init];
    self.titleLabel.text = @"";
    [self.bgView addSubview:self.titleLabel];
    self.titleLabel.sd_layout.topSpaceToView(self.bgView, 22)
    .leftSpaceToView(self.typeLabel, 10)
    .rightSpaceToView(self.ZSButton, 10).heightIs(14);
    
    NSMutableAttributedString *titleString = [[NSMutableAttributedString alloc] initWithString:self.titleLabel.text attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 14],NSForegroundColorAttributeName: [UIColor colorWithRed:69/255.0 green:69/255.0 blue:69/255.0 alpha:1.0]}];
    self.titleLabel.attributedText = titleString;
    
    self.timeLabel = [[UILabel alloc]init];
    self.timeLabel.text = @"2019-05-30 16: 00";
    [self.bgView addSubview:self.timeLabel];
    self.timeLabel.sd_layout.topSpaceToView(self.titleLabel, 16)
    .leftSpaceToView(self.typeLabel, 10)
    .widthIs(134).heightIs(9);
    NSMutableAttributedString *timeString = [[NSMutableAttributedString alloc] initWithString:self.timeLabel.text attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 12],NSForegroundColorAttributeName: [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1.0]}];
    self.timeLabel.attributedText = timeString;
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

@end
