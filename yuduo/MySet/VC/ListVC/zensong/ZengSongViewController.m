//
//  ZengSongViewController.m
//  yuduo
//
//  Created by Mac on 2019/8/14.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "ZengSongViewController.h"
#import "WeiZSViewController.h"
#import "YiSCViewController.h"

#import "TapSliderScrollView.h"
#import "SimpleTapSliderScrollView.h"

@interface ZengSongViewController ()
<SliderLineViewDelegate,SimpleSliderLineViewDelegate,UITableViewDelegate,UITableViewDataSource, UIScrollViewDelegate>

@property (nonatomic ,assign) BOOL selectSort;

@property (nonatomic, strong) NSArray *dataArr;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) NSMutableArray *myChildViewControllers;
PropertyStrong(UILabel, titleLabel);
@end

@implementation ZengSongViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationController.navigationBar.hidden = YES;
    [self setNavigation];
    [self scrollZX];
}

- (void)scrollZX {
    WeiZSViewController *vc1 = [[WeiZSViewController alloc]init];
    YiSCViewController *vc2 = [[YiSCViewController alloc]init];
    SimpleTapSliderScrollView *simpleview = [[SimpleTapSliderScrollView alloc]initWithFrame:CGRectMake(0, 45+MStatusBarHeight, KScreenW, KScreenH)];
    simpleview.delegate = self;
    simpleview.sliderViewColor = RGB(21, 152, 164);
    simpleview.titileColror = RGB(153, 153, 153);
    simpleview.selectedColor = RGB(21, 152, 164);
    [simpleview createView:@[@"未赠送",@"已赠出"] andViewArr:@[vc1,vc2] andRootVc:self];
    [self.view addSubview:simpleview];
    [simpleview sliderToViewIndex:0];
}

#pragma mark sliderDelegate -- OC
-(void)sliderViewAndReloadData:(NSInteger)index
{
    NSLog(@"刷新数据啦%ld",index);
    if (index == 3) {
        NSLog(@"创建tableview");
    }
}

- (void)setNavigation {
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:[UIImage imageNamed:@"back-icon"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButton) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButton];
   backButton.sd_layout.topSpaceToView(self.view,MStatusBarHeight+12)
    .leftSpaceToView(self.view, 15)
    .widthIs(11).heightIs(19);
    UIButton *backButtonView = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 80, MStatusBarHeight+44)];
    backButtonView.backgroundColor = RGBA(15, 15, 15, 0);
    [backButtonView addTarget:self action:@selector(backButton) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButtonView];
    
    UILabel *titleLabel = [[UILabel alloc]init];
    [self.view addSubview:titleLabel];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"我的赠送" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 18],NSForegroundColorAttributeName: [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1.0]}];
    titleLabel.attributedText = string;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.sd_layout.topSpaceToView(self.view, MStatusBarHeight+13)
    .centerXEqualToView(self.view)
    .heightIs(18).widthIs(100);
}
- (void)backButton {
    [self.navigationController popViewControllerAnimated:YES];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
