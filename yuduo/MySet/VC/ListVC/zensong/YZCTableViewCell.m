//
//  YZCTableViewCell.m
//  yuduo
//
//  Created by Mac on 2019/8/14.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "YZCTableViewCell.h"

@implementation YZCTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier]) {
        
        [self createLayout];
    }
    return self;
}

- (void)createLayout {
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = GMWhiteColor;
    self.bgView.layer.cornerRadius = 10;
    [self.contentView addSubview:self.bgView];
    self.bgView.sd_layout.topSpaceToView(self.contentView, 0)
    .leftSpaceToView(self.contentView, 0)
    .rightSpaceToView(self.contentView, 0)
    .widthIs(KScreenW).heightIs(90);
    
    self.headImg = [[UIImageView alloc]init];
    self.headImg.backgroundColor = GMlightGrayColor;
    self.headImg.layer.cornerRadius = 23;
    [self.bgView addSubview:self.headImg];
    self.headImg.sd_layout.topSpaceToView(self.bgView, 17)
    .leftSpaceToView(self.bgView, 15)
    .widthIs(46).heightEqualToWidth();
    
    self.titleLabel = [[UILabel alloc]init];
    self.titleLabel.text = @"前央视主持人李雷:50...";
    [self.bgView addSubview:self.titleLabel];
    self.titleLabel.sd_layout.topSpaceToView(self.bgView, 22)
    .leftSpaceToView(self.headImg, 10)
    .widthIs(180).heightIs(14);
    
    NSMutableAttributedString *titleString = [[NSMutableAttributedString alloc] initWithString:self.titleLabel.text attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 14],NSForegroundColorAttributeName: [UIColor colorWithRed:69/255.0 green:69/255.0 blue:69/255.0 alpha:1.0]}];
    self.titleLabel.attributedText = titleString;
//    self.timeImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ico_time"]];
//    [self.bgView addSubview:self.timeImg];
//    self.timeImg.sd_layout.bottomSpaceToView(self.bgView, 25)
//    .leftSpaceToView(self.headImg, 17)
//    .widthIs(13).heightIs(13);
    
    self.timeLabel = [[UILabel alloc]init];
    self.timeLabel.text = @"2019-05-30 16: 00";
    [self.bgView addSubview:self.timeLabel];
    self.timeLabel.sd_layout.topSpaceToView(self.titleLabel, 16)
    .leftSpaceToView(self.headImg, 10)
    .widthIs(124).heightIs(9);
    NSMutableAttributedString *timeString = [[NSMutableAttributedString alloc] initWithString:self.timeLabel.text attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 12],NSForegroundColorAttributeName: [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1.0]}];
    self.timeLabel.attributedText = timeString;
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
