//
//  WeiZSViewController.m
//  yuduo
//
//  Created by Mac on 2019/8/14.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "WeiZSViewController.h"
#import "ZCTableViewCell.h"
#import "GiveListModel.h"
@interface WeiZSViewController ()
<UITableViewDelegate,UITableViewDataSource>
PropertyStrong(NSMutableArray, dataArray);
PropertyStrong(UITableView, myZXTableView);
@end

@implementation WeiZSViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.myZXTableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    self.myZXTableView.estimatedSectionHeaderHeight = 0;
    self.myZXTableView.estimatedSectionFooterHeight = 0;
    self.myZXTableView.delegate = self;
    self.myZXTableView.dataSource = self;
    self.myZXTableView.backgroundColor = RGB(244, 243, 244);
    [self.view addSubview:self.myZXTableView];
    self.myZXTableView.sd_layout.topSpaceToView(self.view, 0)
    .leftSpaceToView(self.view, 0)
    .widthIs(KScreenW).bottomEqualToView(self.view);
    [self requestData];
}

- (void)requestData {
  
    self.dataArray = [NSMutableArray array];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *user_id = [defaults objectForKey:@"user_id"];
    NSString *token = [defaults objectForKey:@"token"];
    NSMutableDictionary *courseDic = [NSMutableDictionary dictionary];
    courseDic[@"user_id"] = user_id;
    courseDic[@"token"] = token;
    courseDic[@"status"] = @"1";
    
    [GMAfnTools PostHttpDataWithUrlStr:KGiveLists Dic:courseDic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"打印课程购买列表===%@",responseObject);
        for (NSMutableDictionary *dic in responseObject) {
            GiveListModel *model = [GiveListModel new];
            [model mj_setKeyValues:dic];
            [self.dataArray addObject:model];
        }
        [self.myZXTableView reloadData];
    } FailureBlock:^(id  _Nonnull error) {
        NSLog(@"打印购买列表错误===%@",error);
    }];
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"cellID";
    ZCTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[ZCTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    GiveListModel *model = self.dataArray[indexPath.row];
    cell.model = model;
    cell.ZSButton.tag = indexPath.row;
    [cell.ZSButton addTarget:self action:@selector(share:) forControlEvents:(UIControlEventTouchUpInside)];
    return cell;
}

- (void)share:(UIButton *)sender {
    GiveListModel *model = self.dataArray[sender.tag];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}
@end
