//
//  ZCTableViewCell.h
//  yuduo
//
//  Created by Mac on 2019/8/14.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GiveListModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface ZCTableViewCell : UITableViewCell
PropertyStrong(UIView, bgView);
PropertyStrong(UIImageView,headImg);
PropertyStrong(UILabel, titleLabel);
PropertyStrong(UILabel, timeLabel);
PropertyStrong(UIImageView, timeImg);
PropertyStrong(UIImageView, seeImg);
PropertyStrong(UILabel, peopleLabel);
PropertyStrong(UIImageView, zanImg);
PropertyStrong(UILabel, dianZanLabel);
PropertyStrong(UILabel, typeLabel);

PropertyStrong( UIButton , ZSButton);
@property(nonatomic, strong) GiveListModel *model;
@end

NS_ASSUME_NONNULL_END
