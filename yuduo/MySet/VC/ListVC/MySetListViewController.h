//
//  MySetListViewController.h
//  yuduo
//
//  Created by zhouqi on 2019/10/26.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, MySetListType) {
    MySetListTypeBrowse = 0,
    MySetListTypeBuy,
    MySetListTypeCollect
};

NS_ASSUME_NONNULL_BEGIN

@interface MySetListViewController : UIViewController
@property(assign, nonatomic) MySetListType type;
@end

NS_ASSUME_NONNULL_END
