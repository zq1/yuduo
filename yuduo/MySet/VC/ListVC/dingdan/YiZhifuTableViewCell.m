//
//  YiZhifuTableViewCell.m
//  yuduo
//
//  Created by Mac on 2019/8/14.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "YiZhifuTableViewCell.h"

@implementation YiZhifuTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        
        [self createNewCourse];
    }
    return self;
}

- (void)createNewCourse {
    //背景view（所有控件都加到背景）
    self.bgView = [[UIView alloc]init];
        self.bgView.backgroundColor = GMWhiteColor;
    self.bgView.frame = CGRectMake(0, 10, KScreenW, 130);
    [self.contentView addSubview:self.bgView];
    //头像
    self.headImgView = [[UIImageView alloc]init];
    self.headImgView.backgroundColor = GMlightGrayColor;
    self.headImgView.layer.cornerRadius = 10;
    [self.bgView addSubview:self.headImgView];
    self.headImgView.sd_layout.topSpaceToView(self.bgView, 19)
    .leftSpaceToView(self.bgView, 15).widthIs(80*kGMWidthScale).heightIs(80);
    //免费logo
    self.mianfeiLabel = [[UILabel alloc] init];
    self.mianfeiLabel.textAlignment = NSTextAlignmentCenter;
    self.mianfeiLabel.font = [UIFont fontWithName:KPFType size:11];
    self.mianfeiLabel.backgroundColor = RGB(228,102,67);
    self.mianfeiLabel.numberOfLines = 0;
    self.mianfeiLabel.text = @"vip免费";
    self.mianfeiLabel.textColor = GMWhiteColor;
//    [self.bgView addSubview:self.mianfeiLabel];
    self.mianfeiLabel.sd_cornerRadius = [NSNumber numberWithInteger:8.5];
    self.mianfeiLabel.sd_layout.topSpaceToView(self.bgView, 21).leftSpaceToView(self.headImgView, 16).widthIs(45).heightIs(17);
    //标题
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.font = [UIFont fontWithName:KPFType size:15];
    self.titleLabel.textColor = RGB(69, 69, 69);
    self.titleLabel.text = @"课程名称";
    self.titleLabel.numberOfLines = 1;
    [self.bgView addSubview:self.titleLabel];
    self.titleLabel.sd_layout.topSpaceToView(self.bgView, 21)
    .leftSpaceToView(self.headImgView, 16)
    .widthIs(161*kGMWidthScale).heightIs(17);
    //课程简介
    self.detailLabel = [[UILabel alloc] init];
    self.detailLabel.numberOfLines = 1;
    self.detailLabel.text = @"课程简介课程简介";
    self.detailLabel.font = [UIFont fontWithName:KPFType size:13];
    self.detailLabel.textColor = RGB(153, 153, 153);
    self.detailLabel.numberOfLines = 1;
    self.detailLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [self.bgView addSubview:self.detailLabel];
    //     self.detailLabel.frame = CGRectMake(132,755.5,228.5,32.5);
    self.detailLabel.sd_layout.topSpaceToView(self.titleLabel, 11)
    .leftSpaceToView(self.headImgView, 18)
    .rightSpaceToView(self.bgView, 15)
    .heightIs(17);
    //课时按钮
    self.keshiButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.keshiButton setBackgroundImage:[UIImage imageNamed:@"更多"] forState:UIControlStateNormal];
    [self.bgView addSubview:self.keshiButton];
    self.keshiButton.sd_layout.bottomSpaceToView(self.bgView, 26)
    .leftSpaceToView(self.headImgView, 17*kGMWidthScale)
    .widthIs(15).heightIs(15);
    //课时标签
    self.keshiLabel = [[UILabel alloc] init];
    self.keshiLabel.font = [UIFont fontWithName:KPFType size:11];
    self.keshiLabel.textColor = RGB(153, 153, 153);
    self.keshiLabel.text = @"55课时";
    self.keshiLabel.numberOfLines = 0;
    [self.bgView addSubview:self.keshiLabel];
    self.keshiLabel.sd_layout.bottomEqualToView(self.keshiButton)
    .leftSpaceToView(self.keshiButton, 11*kGMWidthScale).widthIs(38).heightIs(11);
    //多少人看过图标
    self.seeImg = [[UIImageView alloc] init];
    self.seeImg.image = [UIImage imageNamed:@"ico_look"];
    [self.bgView addSubview:self.seeImg];
    self.seeImg.sd_layout.bottomEqualToView(self.keshiButton).leftSpaceToView(self.keshiLabel, 20*kGMWidthScale).widthIs(15).heightIs(15);
    
    
    //多少人看过具体人数标签
    
    self.peoplesLabel = [[UILabel alloc] init];
    self.peoplesLabel.numberOfLines = 0;
    self.peoplesLabel.text = @"247";
    self.peoplesLabel.textColor = RGB(153, 153, 153);
    self.peoplesLabel.font = [UIFont fontWithName:KPFType size:11];
    [self.bgView addSubview:self.peoplesLabel];
    self.peoplesLabel.sd_layout.bottomEqualToView(self.keshiLabel).leftSpaceToView(self.seeImg, 11*kGMWidthScale).widthIs(40).heightIs(15);
    
    //现在的价格
    self.priceLabel = [[UILabel alloc] init];
    self.priceLabel.numberOfLines = 0;
    self.priceLabel.font = [UIFont fontWithName:KPFType size:15];
    self.priceLabel.textColor = RGB(228, 102, 67);
    self.priceLabel.text = @"¥69.00";
    [self.bgView addSubview:self.priceLabel];
    self.priceLabel.sd_layout.bottomSpaceToView(self.bgView, 26)
    .rightSpaceToView(self.bgView, 16).heightIs(12);
    [self.priceLabel setSingleLineAutoResizeWithMaxWidth:100];
    //划掉的价格
    
    self.xPriceLabel = [[UILabel alloc] init];
    self.xPriceLabel.text = @"¥299.00";
    self.xPriceLabel.font = [UIFont fontWithName:KPFType size:12];
    self.xPriceLabel.textColor = RGB(153, 153, 153);
    self.xPriceLabel.numberOfLines = 0;
    [self.bgView addSubview:self.xPriceLabel];
    
    self.labelx = [[UILabel alloc]init];
    self.labelx.backgroundColor = RGB(153, 153, 153);
    [self.xPriceLabel addSubview:self.labelx];
    self.labelx.sd_layout.topSpaceToView(self.xPriceLabel, 5)
    .leftSpaceToView(self.xPriceLabel, 0)
    .widthIs(50)
    .heightIs(1);

    self.xPriceLabel.sd_layout.bottomSpaceToView(self.priceLabel,6)
    .rightSpaceToView(self.bgView, 15).heightIs(10);
    [self.xPriceLabel setSingleLineAutoResizeWithMaxWidth:100];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
