//
//  YiZhifuViewController.m
//  yuduo
//
//  Created by Mac on 2019/8/14.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "YiZhifuViewController.h"
#import "YiZhifuTableViewCell.h"
#import "payStatusModel.h"
@interface YiZhifuViewController ()
<UITableViewDelegate,UITableViewDataSource>
PropertyStrong(UITableView, myZXTableView);
PropertyStrong(NSMutableArray, payArray);

@end

@implementation YiZhifuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.myZXTableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    self.myZXTableView.delegate = self;
    self.myZXTableView.dataSource = self;
    self.myZXTableView.estimatedSectionFooterHeight = 0;
    self.myZXTableView.estimatedSectionHeaderHeight = 0;
    self.myZXTableView.backgroundColor = RGB(244, 243, 244);
    [self.view addSubview:self.myZXTableView];
    self.myZXTableView.sd_layout.topSpaceToView(self.view, 0)
    .leftSpaceToView(self.view, 0)
    .widthIs(KScreenW).bottomEqualToView(self.view);
    
    [self getPayData];
    // Do any additional setup after loading the view.
}

- (void)getPayData {
    self.payArray = [NSMutableArray array];
    NSUserDefaults *defa = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"user_id"] = [defa objectForKey:@"user_id"];
    dic[@"token"] = [defa objectForKey:@"token"];
    dic[@"type"] = @"1";
    [GMAfnTools PostHttpDataWithUrlStr:KOrderList Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"打印已支付----%@",responseObject);
        for (NSMutableDictionary *dic in responseObject) {
            payStatusModel *payModel = [[payStatusModel alloc]init];
            [payModel mj_setKeyValues:dic];
            [self.payArray addObject:payModel];
        }
        [self.myZXTableView reloadData];
    } FailureBlock:^(id  _Nonnull error) {
        
    }];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.payArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 140;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"cellID";
    YiZhifuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[YiZhifuTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = RGB(244, 243, 244);
    payStatusModel *model = [self.payArray objectAtIndex:indexPath.row];
    cell.titleLabel.text = model.name;
    cell.xPriceLabel.text = model.under_price;
    CGSize size=[model.under_price boundingRectWithSize:CGSizeMake(1000, 20) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:12]} context:nil].size;
    cell.labelx.sd_layout.widthIs(size.width);
    cell.priceLabel.text = model.pay_price;
    [cell.headImgView sd_setImageWithURL:[NSURL URLWithString:model.cover]];
    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
