//
//  WeizhifuTableViewCell.m
//  yuduo
//
//  Created by Mac on 2019/8/14.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "WeizhifuTableViewCell.h"

@implementation WeizhifuTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createNewCourse];
    }
    return self;
}

- (void)createNewCourse {
    //背景view（所有控件都加到背景）
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = GMWhiteColor;
    self.bgView.userInteractionEnabled = YES;
    self.bgView.frame = CGRectMake(0, 10, KScreenW, 170);
    [self.contentView addSubview:self.bgView];
    //头像
    self.headImgView = [[UIImageView alloc]init];
    self.headImgView.backgroundColor = GMlightGrayColor;
    [self.bgView addSubview:self.headImgView];
    self.headImgView.sd_layout.topSpaceToView(self.bgView, 19)
    .leftSpaceToView(self.bgView, 15).widthIs(80*kGMWidthScale).heightIs(80);

    self.mianfeiLabel = [[UILabel alloc] init];
    self.mianfeiLabel.textAlignment = NSTextAlignmentCenter;
    
    self.mianfeiLabel.numberOfLines = 0;
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"vip免费" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 11],NSForegroundColorAttributeName: [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0]}];
    self.mianfeiLabel.backgroundColor = [UIColor colorWithRed:228/255.0 green:102/255.0 blue:67/255.0 alpha:1.0];
    
    self.mianfeiLabel.attributedText = string;
//    [self.bgView addSubview:self.mianfeiLabel];
    self.mianfeiLabel.sd_cornerRadius = [NSNumber numberWithInteger:8.5];
    self.mianfeiLabel.sd_layout.topSpaceToView(self.bgView, 21).leftSpaceToView(self.headImgView, 16).widthIs(50).heightIs(17);
    //标题
    self.titleLabel = [[UILabel alloc] init];
    
    NSMutableAttributedString *stringTitle = [[NSMutableAttributedString alloc] initWithString:@"课程名称课程名称课程asdfasdfasdfasdfasdf" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 15],NSForegroundColorAttributeName: [UIColor colorWithRed:69/255.0 green:69/255.0 blue:69/255.0 alpha:1.0]}];
    self.titleLabel.attributedText = stringTitle;
    [self.bgView addSubview:self.titleLabel];
    self.titleLabel.sd_layout.topSpaceToView(self.bgView, 21)
    .leftSpaceToView(self.headImgView, 16)
    .rightEqualToView(self.bgView).heightIs(17);
    //课程简介
    self.detailLabel = [[UILabel alloc] init];
    self.detailLabel.numberOfLines = 1;
    self.detailLabel.text = @"课程简介课程简介";
    //    self.detailLabel.backgroundColor = [UIColor redColor];
    NSMutableAttributedString *stringDetail = [[NSMutableAttributedString alloc] initWithString:self.detailLabel.text attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 13],NSForegroundColorAttributeName: [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1.0]}];
    self.detailLabel.attributedText = stringDetail;
    self.detailLabel.numberOfLines = 1;
    self.detailLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [self.bgView addSubview:self.detailLabel];
    //     self.detailLabel.frame = CGRectMake(132,755.5,228.5,32.5);
    self.detailLabel.sd_layout.topSpaceToView(self.titleLabel, 9)
    .leftSpaceToView(self.headImgView, 18)
    .rightSpaceToView(self.bgView, 15)
    .heightIs(16);
    //课时按钮
    self.keshiButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.keshiButton setBackgroundImage:[UIImage imageNamed:@"更多"] forState:UIControlStateNormal];
    [self.bgView addSubview:self.keshiButton];
    self.keshiButton.sd_layout.bottomSpaceToView(self.bgView, 70)
    .leftSpaceToView(self.headImgView, 17*kGMWidthScale)
    .widthIs(15).heightIs(15);
    //课时标签
    self.keshiLabel = [[UILabel alloc] init];
    //    label.frame = CGRectMake(157,816,34.5,10.5);
    self.keshiLabel.numberOfLines = 0;
    [self.bgView addSubview:self.keshiLabel];
    
    NSMutableAttributedString *stringKS = [[NSMutableAttributedString alloc] initWithString:@"55课时" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 11],NSForegroundColorAttributeName: [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1.0]}];
    self.keshiLabel.attributedText = stringKS;
    
    self.keshiLabel.sd_layout.bottomEqualToView(self.keshiButton)
    .leftSpaceToView(self.keshiButton, 11*kGMWidthScale).widthIs(38).heightIs(15);
    //多少人看过图标
    self.seeImg = [[UIImageView alloc] init];
    self.seeImg.image = [UIImage imageNamed:@"ico_look"];
    [self.bgView addSubview:self.seeImg];
    self.seeImg.sd_layout.bottomEqualToView(self.keshiButton).leftSpaceToView(self.keshiLabel, 20*kGMWidthScale).widthIs(15).heightIs(15);
    
    
    //多少人看过具体人数标签
    
    self.peoplesLabel = [[UILabel alloc] init];
    //    self.peoplesLabel.frame = CGRectMake(236.5,970,36,10.5);
    self.peoplesLabel.numberOfLines = 0;
    [self.bgView addSubview:self.peoplesLabel];
    
    NSMutableAttributedString *stringPeople = [[NSMutableAttributedString alloc] initWithString:@"2478人" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 11],NSForegroundColorAttributeName: [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1.0]}];
    
    self.peoplesLabel.attributedText = stringPeople;
    self.peoplesLabel.sd_layout.bottomEqualToView(self.keshiLabel).leftSpaceToView(self.seeImg, 11*kGMWidthScale).widthIs(40).heightIs(15);
    
    //现在的价格
    self.priceLabel = [[UILabel alloc] init];
    //    sel.frame = CGRectMake(311.5,815.5,48.5,11.5);
    self.priceLabel.font = [UIFont fontWithName:KPFType size:15];
    self.priceLabel.textColor = RGB(228, 102, 67);
    self.priceLabel.numberOfLines = 0;
    [self.bgView addSubview:self.priceLabel];
    self.priceLabel.sd_layout.bottomSpaceToView(self.bgView, 72)
    .rightSpaceToView(self.bgView, 16).heightIs(12);
    
    [self.priceLabel setSingleLineAutoResizeWithMaxWidth:100];
    //划掉的价格
    
    self.xPriceLabel = [[UILabel alloc] init];
    //    self.xPriceLabel.frame = CGRectMake(314,954.5,45,9.5);
    self.xPriceLabel.numberOfLines = 0;
    self.xPriceLabel.textColor = RGB(153, 153, 153);
    self.xPriceLabel.font = [UIFont fontWithName:KPFType size:12];
    [self.bgView addSubview:self.xPriceLabel];
    self.xPriceLabel.sd_layout.bottomSpaceToView(self.priceLabel,6)
    .rightSpaceToView(self.bgView, 15).heightIs(10);
    
    [self.xPriceLabel setSingleLineAutoResizeWithMaxWidth:100];
    
    self.labelx = [[UILabel alloc]init];
    self.labelx.backgroundColor = RGB(153, 153, 153);
    [self.bgView addSubview:self.labelx];
    
    self.labelx.sd_layout.bottomSpaceToView(self.priceLabel,11)
    .rightSpaceToView(self.bgView, 15)
    .heightIs(1)
    .widthIs(50);
    
    self.payButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.payButton setTitle:@"立即支付" forState:UIControlStateNormal];
    self.payButton.titleLabel.font = kFont(12);
    [self.payButton addTarget:self action:@selector(payClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.payButton setTitleColor:RGB(21, 152, 164) forState:UIControlStateNormal];
    self.payButton.backgroundColor = GMWhiteColor;
    self.payButton.layer.borderWidth = 0.7;
    self.payButton.layer.cornerRadius = 5;
    self.payButton.layer.borderColor = RGB(21, 152, 164).CGColor;
    [self.bgView addSubview:self.payButton];
    self.payButton.sd_layout.bottomSpaceToView(self.bgView, 11)
    .rightSpaceToView(self.bgView, 20)
    .widthIs(60).heightIs(28);
    
    UILabel *lineLabel = [[UILabel alloc]init];
    lineLabel.backgroundColor = RGB(230, 230, 230);
    [self.bgView addSubview:lineLabel];
    lineLabel.sd_layout.bottomSpaceToView(self.payButton, 11)
    .rightSpaceToView(self.bgView, 20)
    .widthIs(KScreenW).heightIs(1);

}

- (void)payClick:(UIButton *)send {
    if (self.payButtonClick) {
        self.payButtonClick(send.tag);
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
