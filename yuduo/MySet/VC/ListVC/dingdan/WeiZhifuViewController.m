//
//  WeiZhifuViewController.m
//  yuduo
//
//  Created by Mac on 2019/8/14.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "WeiZhifuViewController.h"
#import "WeizhifuTableViewCell.h"
#import "payStatusModel.h"
@interface WeiZhifuViewController ()
<UITableViewDelegate,UITableViewDataSource>
PropertyStrong(UITableView, myZXTableView);
PropertyStrong(NSMutableArray, noPayArray);
@end

@implementation WeiZhifuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.myZXTableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    self.myZXTableView.delegate = self;
    self.myZXTableView.dataSource = self;
    self.myZXTableView.estimatedSectionFooterHeight = 0;
    self.myZXTableView.estimatedSectionHeaderHeight = 0;
    self.myZXTableView.backgroundColor = RGB(244, 243, 244);
    [self.view addSubview:self.myZXTableView];
    self.myZXTableView.sd_layout.topSpaceToView(self.view, 0)
    .leftSpaceToView(self.view, 0)
    .widthIs(KScreenW).bottomEqualToView(self.view);
    
    [self getNoPayData];
}

- (void)getNoPayData {
    self.noPayArray = [NSMutableArray array];
    NSUserDefaults *defa = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"user_id"] = [defa objectForKey:@"user_id"];
    dic[@"token"] = [defa objectForKey:@"token"];
    dic[@"type"] = @"2";
    [GMAfnTools PostHttpDataWithUrlStr:KOrderList Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"打印未支付----%@",responseObject);
        for (NSMutableDictionary *dic in responseObject) {
            payStatusModel *payModel = [[payStatusModel alloc]init];
            NSString *idSting = [dic objectForKey:@"id"];
            payModel.sid = idSting;
            [payModel mj_setKeyValues:dic];
            [self.noPayArray addObject:payModel];
            NSLog(@"打印未支付--id--%@",payModel.sid);
        }
        [self.myZXTableView reloadData];
    } FailureBlock:^(id  _Nonnull error) {
        NSLog(@"-未支付 错误----%@",error);
    }];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.noPayArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 180;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"cellID";
    WeizhifuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[WeizhifuTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
     payStatusModel *model = [self.noPayArray objectAtIndex:indexPath.row];
    __weak typeof(self) weakSelf = self;
    cell.payButtonClick = ^(NSInteger payIndex) {
        NSLog(@"%ld",indexPath.row);
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        dic[@"user_id"] = [defaults objectForKey:@"user_id"];
        dic[@"token"] = [defaults objectForKey:@"token"];
        dic[@"order_id"] = model.sid;
        [weakSelf order_id:model.order_number dic:dic index:indexPath.row];
    };
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = RGB(244, 243, 244);
    cell.titleLabel.text = model.name;
    cell.xPriceLabel.text = model.under_price;
    CGSize size=[model.under_price boundingRectWithSize:CGSizeMake(1000, 20) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:12]} context:nil].size;
    cell.labelx.sd_layout.widthIs(size.width);
    cell.priceLabel.text = model.pay_price;
    [cell.headImgView sd_setImageWithURL:[NSURL URLWithString:model.cover]];
    cell.headImgView.sd_cornerRadius = [NSNumber numberWithInteger:10];
    return cell;
}


#pragma mark--点击未支付 支付
- (void)order_id:(NSString *)order_id dic:(NSMutableDictionary *)parms index:(NSInteger)index{
    [GMAfnTools PostHttpDataWithUrlStr:KPointPayURL Dic:parms SuccessBlock:^(id  _Nonnull responseObject) {
        if (responseObject) {
            [self.noPayArray removeObjectAtIndex:index];
            [self.myZXTableView reloadData];
            [self showError:[responseObject objectForKey:KSucessMSG]];
        }
    } FailureBlock:^(id  _Nonnull error) {
        NSLog(@"--支付服务器错误---%@",error);
    }];
}
#pragma mark -- 提示框
- (void)showError:(NSString *)errorMsg {
    // 1.弹框提醒
    // 初始化对话框
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:errorMsg preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:nil]];
    // 弹出对话框
    [self presentViewController:alert animated:true completion:nil];
    
    [self performSelector:@selector(dimisssAlert:) withObject:alert afterDelay:1];
    
}

- (void) dimisssAlert:(UIAlertController *)alert
{
    if (alert) {
        [alert dismissViewControllerAnimated:YES completion:nil];
    }
    
}
@end
