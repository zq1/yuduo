//
//  payStatusModel.h
//  yuduo
//
//  Created by Mac on 2019/9/26.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface payStatusModel : BaseModel
PropertyStrong(NSString, order_number);
PropertyStrong(NSString, pay_status);
PropertyStrong(NSString, status);
PropertyStrong(NSString, under_price);
PropertyStrong(NSString, create_order_time);
PropertyStrong(NSString, user_id);
PropertyStrong(NSString, pay_type);
PropertyStrong(NSString, pay_order_time);
PropertyStrong(NSString, cover);
PropertyStrong(NSString, pay_price);
PropertyStrong(NSString, course_types);
PropertyStrong(NSString, name);
PropertyStrong(NSString, id);

@end

NS_ASSUME_NONNULL_END
