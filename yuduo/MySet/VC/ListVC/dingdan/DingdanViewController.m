//
//  DingdanViewController.m
//  yuduo
//
//  Created by Mac on 2019/8/13.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "DingdanViewController.h"
#import "TapSliderScrollView.h"
#import "SimpleTapSliderScrollView.h"
#import "WeiZhifuViewController.h"
#import "YiZhifuViewController.h"
@interface DingdanViewController ()
<SliderLineViewDelegate,SimpleSliderLineViewDelegate,UITableViewDelegate,UITableViewDataSource, UIScrollViewDelegate>

@property (nonatomic ,assign) BOOL selectSort;

@property (nonatomic, strong) NSArray *dataArr;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) NSMutableArray *myChildViewControllers;
@property (nonatomic, strong) WeiZhifuViewController *vc1;
@property (nonatomic, strong) YiZhifuViewController *vc2;

@end

@implementation DingdanViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = GMWhiteColor;
    [self setNavigation];
    [self scrollPay];
    // Do any additional setup after loading the view.
}

- (void)dealloc
{
    NSLog(@"DingdanViewController");
}

- (void)setNavigation {
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:[UIImage imageNamed:@"back-icon"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButton) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButton];
   backButton.sd_layout.topSpaceToView(self.view,MStatusBarHeight+12)
    .leftSpaceToView(self.view, 15)
    .widthIs(11).heightIs(19);
    UIButton *backButtonView = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 80, MStatusBarHeight+44)];
    backButtonView.backgroundColor = RGBA(15, 15, 15, 0);
    [backButtonView addTarget:self action:@selector(backButton) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButtonView];
    UILabel *titleLabel = [[UILabel alloc]init];
    [self.view addSubview:titleLabel];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"我的订单" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 18],NSForegroundColorAttributeName: [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1.0]}];
    titleLabel.attributedText = string;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.sd_layout.topSpaceToView(self.view, MStatusBarHeight+13)
    .centerXEqualToView(self.view)
    .heightIs(18).widthIs(100);
}
- (void)scrollPay {
    
    self.vc1 = [[WeiZhifuViewController alloc]init];
    self.vc2 = [[YiZhifuViewController alloc]init];
    SimpleTapSliderScrollView *simpleview = [[SimpleTapSliderScrollView alloc]initWithFrame:CGRectMake(0, 45+MStatusBarHeight, KScreenW, KScreenH-45-MStatusBarHeight)];
    simpleview.delegate = self;
    simpleview.sliderViewColor = [UIColor clearColor];
    simpleview.titileColror = RGB(153, 153, 153);
    simpleview.selectedColor = RGB(21, 152, 164);
    [simpleview createView:@[@"未支付",@"已支付"] andViewArr:@[self.vc1,self.vc2] andRootVc:self];
    [self.view addSubview:simpleview];
    [simpleview sliderToViewIndex:0];
}
#pragma mark sliderDelegate -- OC
-(void)sliderViewAndReloadData:(NSInteger)index
{
    if (index == 0) {
        [self.vc1 getNoPayData];
    } else if (index == 1) {
        [self.vc2 getPayData];
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}
#pragma mark--返回行高
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40;
}
#pragma mark--返回行数
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *key0=@"cell0";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:key0];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:key0];
    }
    //    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (void)backButton {
    [self.navigationController popViewControllerAnimated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
