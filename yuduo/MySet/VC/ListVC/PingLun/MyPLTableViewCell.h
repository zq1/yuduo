//
//  MyPLTableViewCell.h
//  yuduo
//
//  Created by Mac on 2019/9/4.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "KCPJModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface MyPLTableViewCell : UITableViewCell
PropertyStrong(UIImageView, headImg);//头像
PropertyStrong(UILabel, nameLabel);//用户昵称
PropertyStrong(UILabel, timeLabel);//时间
PropertyStrong(UILabel, questionLabel);//问题
PropertyStrong(UILabel, answerLabel);//客服回答

PropertyStrong(UILabel, coursePJ);
PropertyStrong(UILabel, courseDetail);

PropertyStrong(KCPJModel, model);
PropertyStrong(UIButton, yesButton);
PropertyStrong(UILabel, zanNumLabel);

@property (nonatomic ,copy)void (^zanButtonBlock)(UIButton * indexSelect);
@end

NS_ASSUME_NONNULL_END
