//
//  PingLunViewController.m
//  yuduo
//
//  Created by Mac on 2019/8/13.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "PingLunViewController.h"
//课程介绍
#import "CoursePingLunViewController.h"
#import "ColumnPingLunViewController.h"
#import "WenZhangPLViewController.h"
#import "PJViewController.h"

#import "TapSliderScrollView.h"
#import "SimpleTapSliderScrollView.h"
#import "GMScrollView.h"
@interface PingLunViewController ()
<SliderLineViewDelegate,SimpleSliderLineViewDelegate,UITableViewDelegate,UITableViewDataSource, UIScrollViewDelegate,UIGestureRecognizerDelegate>
{
    CoursePingLunViewController *vc1 ;
    CoursePingLunViewController *vc2 ;
    WenZhangPLViewController *vc3 ;
}
@property (nonatomic ,assign) BOOL selectSort;

@property (nonatomic, strong) NSArray *dataArr;

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) NSMutableArray *myChildViewControllers;
@end

@implementation PingLunViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = RGB(244,243 , 244);

    [self setNavigation];
    [self scrollZX];
    
    // Do any additional setup after loading the view.
}
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    
    return YES;
}
- (void)setNavigation {
    self.view.backgroundColor = RGB(244, 243, 244);
    
    UIView *titleView = [[UIView alloc]init];
    titleView.backgroundColor = GMWhiteColor;
    [self.view addSubview:titleView];
    titleView.sd_layout.topSpaceToView(self.view, 0)
    .leftEqualToView(self.view)
    .widthIs(KScreenW).heightIs(44+MStatusBarHeight);
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //    backButton.backgroundColor = GMBlueColor;
    [backButton setBackgroundImage:[UIImage imageNamed:@"back-icon"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
    [titleView addSubview:backButton];
    backButton.sd_layout.topSpaceToView(titleView,MStatusBarHeight+15)
    .leftSpaceToView(titleView, 15)
    .widthIs(11).heightIs(19);
    UIButton *backButtonView = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 80, MStatusBarHeight+44)];
    backButtonView.backgroundColor = RGBA(15, 15, 15, 0);
    [backButtonView addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButtonView];
    
    UILabel *forgetPsw = [[UILabel alloc]init];
    forgetPsw.text = @"我的评论";
    forgetPsw.font = kFont(18);
    [titleView addSubview:forgetPsw];
    forgetPsw.sd_layout.topSpaceToView(0, MStatusBarHeight+15)
    .centerXEqualToView(titleView).widthIs(100).heightIs(17);
}
- (void)backBtn {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)scrollZX {
    
    CoursePingLunViewController *vc1 = [[CoursePingLunViewController alloc]init];
    ColumnPingLunViewController *vc2 = [[ColumnPingLunViewController alloc]init];
    WenZhangPLViewController *vc3 = [[WenZhangPLViewController alloc]init];
    SimpleTapSliderScrollView *simpleview = [[SimpleTapSliderScrollView alloc]initWithFrame:CGRectMake(0, MStatusBarHeight+44, KScreenW, KScreenH)];
    simpleview.delegate = self;
    simpleview.sliderViewColor = RGB(21, 152, 164);
    simpleview.titileColror = RGB(153, 153, 153);
    simpleview.selectedColor = RGB(21, 152, 164);
    [simpleview createView:@[@"课程评论",@"专栏评论",@"文章评论"] andViewArr:@[vc1,vc2,vc3] andRootVc:self];
    [self.view addSubview:simpleview];
//            simpleview.sd_layout.topSpaceToView(self.view, 0)
//            .leftSpaceToView(self.view, 0)
//            .rightSpaceToView(self.view, 0)
//        .widthIs(KScreenW).heightIs(KScreenH);
    
    [simpleview sliderToViewIndex:0];
    
}
#pragma mark sliderDelegate -- OC
-(void)sliderViewAndReloadData:(NSInteger)index
{
    NSLog(@"刷新数据啦%ld",index);
    if (index == 3) {
        NSLog(@"创建tableview");
    }
}


@end
