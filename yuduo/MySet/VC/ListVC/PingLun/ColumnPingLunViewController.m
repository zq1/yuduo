//
//  ColumnPingLunViewController.m
//  yuduo
//
//  Created by Mac on 2019/9/4.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "ColumnPingLunViewController.h"
#import "MyPLTableViewCell.h"
#import "StatusModel.h"
#import "KCPJModel.h"
@interface ColumnPingLunViewController ()
<UITableViewDataSource,UITableViewDelegate>
PropertyStrong(UITableView, pjTableView);
PropertyStrong(NSMutableArray, myCoursePLArray);
PropertyStrong(NSMutableArray, statusArray);
@end

@implementation ColumnPingLunViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    self.pjTableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.pjTableView.delegate = self;
    self.pjTableView.dataSource = self;
    [self.view addSubview:self.pjTableView];
    self.pjTableView.sd_layout.topSpaceToView(self.view, 10)
    .leftSpaceToView(self.view, 0)
    .widthIs(KScreenW)
    .heightIs(KScreenH-MStatusBarHeight-44-44);
    [self getColumnPL];
    // Do any additional setup after loading the view.
}
#pragma mark--评论专栏数据
- (void)getColumnPL {
    self.myCoursePLArray = [NSMutableArray array];
    self.statusArray = [NSMutableArray array];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"user_id"] = [defaults objectForKey:@"user_id"];
    dic[@"token"] = [defaults objectForKey:@"token"];
    dic[@"type"] = @"2";
    [GMAfnTools PostHttpDataWithUrlStr:KCoursePJList Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
        
        NSLog(@"----我的评论-专栏-%@---",responseObject);
        
        for (NSMutableDictionary *dic in responseObject) {
            StatusModel *statuM = [[StatusModel alloc]init];
            [statuM mj_setKeyValues:dic];
            [self.statusArray addObject:statuM];
            for (NSMutableDictionary *comment_list in [dic objectForKey:@"user_comment_lists"]) {
                KCPJModel *moldel = [[KCPJModel alloc]init];
                [moldel mj_setKeyValues:comment_list];
                [self.myCoursePLArray addObject:moldel];
            }
        }
        
        [self.pjTableView reloadData];
    } FailureBlock:^(id  _Nonnull error) {
        
        NSLog(@"----我的评论-课程--服务器错误--%@",error);
    }];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 250;
}

-(CGFloat)cellHeightForIndexPath:(NSIndexPath *)indexPath cellContentViewWidth:(CGFloat)width tableView:(UITableView *)tableView {
    return 500;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.statusArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"cellID";
    MyPLTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[MyPLTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    StatusModel *statuM = [self.statusArray objectAtIndex:indexPath.row];
    cell.courseDetail.text =  statuM.comment_title;
    KCPJModel *kcModel = [self.myCoursePLArray objectAtIndex:indexPath.row];
    cell.nameLabel.text = kcModel.nikname;
    cell.timeLabel.text = kcModel.comments_time;
    cell.answerLabel.text = kcModel.comments;
    cell.zanNumLabel.text = kcModel.thumbs_up;
    cell.zanButtonBlock = ^(UIButton * _Nonnull indexSelect) {
        indexSelect.selected = !indexSelect.selected;
        if (indexSelect.selected) {
            [indexSelect setBackgroundImage:[UIImage imageNamed:@"ico_zan_1 拷贝"] forState:UIControlStateNormal];
            NSLog(@"点赞了");
            
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            dic[@"comment_lists_id"] = kcModel.comment_id;
            [GMAfnTools PostHttpDataWithUrlStr:KGiveThumbsUpURL Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
                if (responseObject) {
                    [self showError:[responseObject objectForKey:KSucessMSG]];
                    [self.pjTableView reloadData];
                }
            } FailureBlock:^(id  _Nonnull error) {
                
            }];
            
        }else {
            
            [indexSelect setBackgroundImage:[UIImage imageNamed:@"ico_zan_1"] forState:UIControlStateNormal];
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            dic[@"comment_lists_id"] = kcModel.comment_id;
            [GMAfnTools PostHttpDataWithUrlStr:KCancelGiveThumb Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
                if (responseObject) {
                    
                    [self showError:[responseObject objectForKey:KSucessMSG]];
                    [self.pjTableView reloadData];
                }
            } FailureBlock:^(id  _Nonnull error) {
                
            }];
        }
    };
    return cell;
}
#pragma mark -- 提示框
- (void)showError:(NSString *)errorMsg {
    // 1.弹框提醒
    // 初始化对话框
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:errorMsg preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:nil]];
    // 弹出对话框
    [self presentViewController:alert animated:true completion:nil];
    
    [self performSelector:@selector(dimisssAlert:) withObject:alert afterDelay:1];
    
}

- (void) dimisssAlert:(UIAlertController *)alert
{
    if (alert) {
        [alert dismissViewControllerAnimated:YES completion:nil];
    }
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
