//
//  WenZhangPLViewController.h
//  yuduo
//
//  Created by Mac on 2019/9/4.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZJScrollPageViewDelegate.h"
NS_ASSUME_NONNULL_BEGIN

@interface WenZhangPLViewController : UIViewController<ZJScrollPageViewChildVcDelegate>
@property(nonatomic, assign) NSInteger type;
@end

NS_ASSUME_NONNULL_END
