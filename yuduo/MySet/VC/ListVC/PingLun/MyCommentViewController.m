//
//  MyCommentViewController.m
//  yuduo
//
//  Created by zhouqi on 2019/10/26.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "MyCommentViewController.h"
#import "ZJScrollPageView.h"
#import "WenZhangPLViewController.h"

@interface MyCommentViewController ()<ZJScrollPageViewDelegate>
@property(strong, nonatomic)NSArray<NSString *> *titles;
@property(strong, nonatomic)NSArray<UIViewController *> *childVcs;
@property (nonatomic, strong) ZJScrollPageView *scrollPageView;
@end

@implementation MyCommentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavigation];
    [self setScrollView];
}

- (void)setNavigation {
    self.view.backgroundColor = RGB(244, 243, 244);
    
    UIView *titleView = [[UIView alloc]init];
    titleView.backgroundColor = GMWhiteColor;
    [self.view addSubview:titleView];
    titleView.sd_layout.topSpaceToView(self.view, 0)
    .leftEqualToView(self.view)
    .widthIs(KScreenW).heightIs(44+MStatusBarHeight);
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //    backButton.backgroundColor = GMBlueColor;
    [backButton setBackgroundImage:[UIImage imageNamed:@"back-icon"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
    [titleView addSubview:backButton];
    backButton.sd_layout.topSpaceToView(titleView,MStatusBarHeight+15)
    .leftSpaceToView(titleView, 15)
    .widthIs(11).heightIs(19);
    UIButton *backButtonView = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 80, MStatusBarHeight+44)];
    backButtonView.backgroundColor = RGBA(15, 15, 15, 0);
    [backButtonView addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButtonView];
    
    UILabel *forgetPsw = [[UILabel alloc]init];
    forgetPsw.text = @"我的评论";
    forgetPsw.font = kFont(18);
    [titleView addSubview:forgetPsw];
    forgetPsw.sd_layout.topSpaceToView(0, MStatusBarHeight+15)
    .centerXEqualToView(titleView).widthIs(100).heightIs(17);
}

- (void)backBtn {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setScrollView {
    
    ZJSegmentStyle *style = [[ZJSegmentStyle alloc] init];
    style.showLine = YES;
    style.scrollTitle = NO;
    style.autoAdjustTitlesWidth = true;
    style.adjustCoverOrLineWidth = YES;
    style.gradualChangeTitleColor = YES;
    style.normalTitleColor = RGB(153, 153, 153);
    style.scrollLineColor = RGB(21, 152, 164);
    style.selectedTitleColor = RGB(21, 152, 164);
    
    self.titles = @[@"课程评论",@"专栏评论",@"文章评论"];
    _scrollPageView = [[ZJScrollPageView alloc] initWithFrame:CGRectMake(0, 44+MStatusBarHeight, self.view.bounds.size.width, self.view.bounds.size.height - 44-MStatusBarHeight) segmentStyle:style titles:self.titles parentViewController:self delegate:self];

    [self.view addSubview:_scrollPageView];
}

- (NSInteger)numberOfChildViewControllers {
    return self.titles.count;
}

- (UIViewController<ZJScrollPageViewChildVcDelegate> *)childViewController:(UIViewController<ZJScrollPageViewChildVcDelegate> *)reuseViewController forIndex:(NSInteger)index {
    UIViewController<ZJScrollPageViewChildVcDelegate> *childVc = reuseViewController;
    
    if (!childVc) {
        WenZhangPLViewController *vc = [WenZhangPLViewController new];
        vc.type = index+1;
        childVc = vc;
    }
    
    return childVc;
}


- (BOOL)shouldAutomaticallyForwardAppearanceMethods {
    return NO;
}
@end
