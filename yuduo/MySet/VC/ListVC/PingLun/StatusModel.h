//
//  StatusModel.h
//  yuduo
//
//  Created by Mac on 2019/9/29.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface StatusModel : BaseModel
PropertyStrong(NSString, status);
PropertyStrong(NSString, comment_title);
@end

NS_ASSUME_NONNULL_END
