//
//  MyPLTableViewCell.m
//  yuduo
//
//  Created by Mac on 2019/9/4.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "MyPLTableViewCell.h"

@implementation MyPLTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    if (self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier]) {
        [self setLayout];
    }
    return self;
}

- (void)setModel:(KCPJModel *)model {
    _model = model;
    self.nameLabel.text = model.nikname;
    self.timeLabel.text = model.comments_time;
    self.answerLabel.text = model.comments;
    self.coursePJ.text = model.comment_title;
    [self.yesButton setTitle:model.thumbs_up forState:(UIControlStateNormal)];
    [self.headImg sd_setImageWithURL:[NSURL URLWithString:model.portrait]];
    [self setupAutoHeightWithBottomView:self.answerLabel bottomMargin:15];
}

- (void)setLayout {
    self.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.coursePJ = [[UILabel alloc]init];
    self.coursePJ.font = [UIFont fontWithName:KPFType size:14];
    self.coursePJ.textColor = RGB(69, 69, 69);
    self.coursePJ.text = @"评价课程: ";
    self.coursePJ.numberOfLines = 2;
    [self.contentView addSubview:self.coursePJ];
    self.coursePJ.sd_layout.topSpaceToView(self.contentView, 30)
    .leftSpaceToView(self.contentView, 30)
    .rightSpaceToView(self.contentView, 30).autoHeightRatio(0);
        
    UIView *separate = [UIView new];
    separate.backgroundColor = RGBA(210, 210, 210, 0.5);
    [self.contentView addSubview:separate];
    separate.sd_layout.leftSpaceToView(self.contentView, 15).rightSpaceToView(self.contentView, 15).topSpaceToView(self.coursePJ, 15).heightIs(0.5);
    
    self.headImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@""]];
    self.headImg.backgroundColor = GMlightGrayColor;
    self.headImg.layer.cornerRadius = 20;
    self.headImg.clipsToBounds = YES;
    [self.contentView addSubview:self.headImg];
    self.headImg.sd_layout.topSpaceToView(separate, 15)
    .leftSpaceToView(self.contentView, 30)
    .widthIs(40).heightIs(40);
    self.nameLabel = [[UILabel alloc]init];
    self.nameLabel.font = [UIFont fontWithName:KPFType size:15];
    self.nameLabel.textColor = RGB(69, 69, 69);
    self.nameLabel.text = @"用户昵称";
    [self.contentView addSubview:self.nameLabel];
    self.nameLabel.sd_layout.topSpaceToView(separate, 20)
    .leftSpaceToView(self.headImg, 15)
    .widthIs(80).heightIs(15);
    
    
    self.timeLabel = [[UILabel alloc]init];
    self.timeLabel.text = @"2019.05.10  10:00:00";
    self.timeLabel.textColor = RGB(153, 153, 153);
    self.timeLabel.font = [UIFont fontWithName:KPFType size:12];
    [self.contentView addSubview:self.timeLabel];
    self.timeLabel.sd_layout.topSpaceToView(self.nameLabel, 10)
    .leftSpaceToView(self.headImg, 15)
    .widthIs(130).heightIs(10);
    
    //问题
    self.answerLabel = [[UILabel alloc]initWithFrame:CGRectMake(71,144, KScreenW-96, 10)];
    self.answerLabel.numberOfLines = 0;
    self.answerLabel.textColor = [UIColor blackColor];
    self.answerLabel.font = [UIFont fontWithName:KPFType size:14];
    self.answerLabel.text = @"课程很好，在对过敏性鼻炎，哮喘和咳嗽的长期控制中，是否正常的打育苗？需要提前停药吗，在对过敏性鼻炎，哮喘和咳嗽的长期控制中，是否正常的打育苗？需要提前停药吗？";
    CGSize size = [self.answerLabel sizeThatFits:CGSizeMake(self.answerLabel.frame.size.width, MAXFLOAT)];
//    self.answerLabel.frame = CGRectMake(self.answerLabel.frame.origin.x, self.answerLabel.frame.origin.y, self.answerLabel.frame.size.width, size.height);
    [self.contentView addSubview:self.answerLabel];
    self.answerLabel.sd_layout.topSpaceToView(self.timeLabel, 15).leftEqualToView(self.timeLabel).rightSpaceToView(self.contentView, 30).autoHeightRatio(0);
//    //客服回答
//    self.questionLabel = [[UILabel alloc]initWithFrame:CGRectMake(71,self.answerLabel.frame.size.height+50+100, KScreenW-96, 60)];
//    self.questionLabel.numberOfLines = 0;
//    self.questionLabel.textColor = [UIColor blackColor];
//    self.questionLabel.backgroundColor = RGB(245, 245, 245);
//    self.questionLabel.text = @"第三方噶抗裂砂浆阿拉山口大姐夫阿斯蒂芬阿斯山口大姐夫阿斯蒂芬阿斯蒂芬阿斯蒂芬阿斯蒂芬阿斯蒂芬按时阿斯蒂芬按时 是的发送到发斯蒂芬阿斯";
//    CGSize sizeQuestion = [self.questionLabel sizeThatFits:CGSizeMake(self.questionLabel.frame.size.width, MAXFLOAT)];
//    self.questionLabel.frame = CGRectMake(self.questionLabel.frame.origin.x, self.questionLabel.frame.origin.y, self.questionLabel.frame.size.width, sizeQuestion.height);
//    [self.contentView addSubview:self.questionLabel];
//    NSMutableAttributedString *questionString = [[NSMutableAttributedString alloc] initWithString:self.questionLabel.text attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFang SC" size: 14],NSForegroundColorAttributeName: [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1.0]}];
//    self.questionLabel.attributedText = questionString;
    //客服回答
    //    self.questionLabel = [[UILabel alloc]init];
    //    [self.contentView addSubview:self.questionLabel];
    
    self.yesButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.yesButton addTarget:self action:@selector(zanClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.yesButton setImage:[UIImage imageNamed:@"ico_zan_1"] forState:(UIControlStateNormal)];
    [self.yesButton setImage:[UIImage imageNamed:@"ico_zan_1 拷贝"] forState:(UIControlStateSelected)];
    self.yesButton.tag = 10000;
    [self.contentView addSubview:self.yesButton];
    self.yesButton.sd_layout.topSpaceToView(self.coursePJ, 22)
    .rightSpaceToView(self.contentView, 64)
    .heightIs(40).autoWidthRatio(0);
    
    self.zanNumLabel = [[UILabel alloc]init];
    
    UIView *line = [[UIView alloc]init];
    line.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:line];
    line.sd_layout.topSpaceToView(self.answerLabel, 15).leftSpaceToView(self.contentView, 15).rightSpaceToView(self.contentView, 15).heightIs(1);
    
    
    UIView *bgView = [[UIView alloc]init];
    bgView.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:bgView];
    bgView.layer.cornerRadius = 10;
    bgView.clipsToBounds = YES;
    bgView.sd_layout.topSpaceToView(self.contentView, 15).leftSpaceToView(self.contentView, 15).rightSpaceToView(self.contentView, 15).bottomSpaceToView(line, 0);
    [self.contentView sendSubviewToBack:bgView];

//    self.zanNumLabel.text = @"200";
//    self.zanNumLabel.font = [UIFont fontWithName:KPFType size:12];
//    self.zanNumLabel.textColor = RGB(153, 153, 153);
//    [self.contentView addSubview:self.zanNumLabel];
//    self.zanNumLabel.sd_layout.topSpaceToView(self.courseDetail, 49)
//    .leftSpaceToView(self.yesButton, 9)
//    .widthIs(31).heightIs(10);
}
- (void)zanClick:(UIButton *)send {
    
    
    if (self.zanButtonBlock) {
        self.zanButtonBlock(send);
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


@end
