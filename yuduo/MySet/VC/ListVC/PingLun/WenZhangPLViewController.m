//
//  WenZhangPLViewController.m
//  yuduo
//
//  Created by Mac on 2019/9/4.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "WenZhangPLViewController.h"
#import "MyPLTableViewCell.h"
#import "StatusModel.h"
#import "KCPJModel.h"
@interface WenZhangPLViewController ()
<UITableViewDataSource,UITableViewDelegate>
PropertyStrong(UITableView, pjTableView);
PropertyStrong(NSMutableArray, myCoursePLArray);
PropertyStrong(NSMutableArray, statusArray);
@end

@implementation WenZhangPLViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.pjTableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.pjTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.pjTableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.pjTableView.estimatedSectionFooterHeight = 0;
    self.pjTableView.delegate = self;
    self.pjTableView.dataSource = self;
    [self.view addSubview:self.pjTableView];
    self.pjTableView.sd_layout.topSpaceToView(self.view, 0)
    .leftSpaceToView(self.view, 0)
    .widthIs(KScreenW)
    .bottomEqualToView(self.view);
    __weak typeof(self)  weakSelf = self;
    self.pjTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf getArticleData];
    }];
    [self.pjTableView.mj_header beginRefreshing];
}

#pragma mark--评论专栏数据
- (void)getArticleData {
    self.myCoursePLArray = [NSMutableArray array];
    self.statusArray = [NSMutableArray array];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"user_id"] = [defaults objectForKey:@"user_id"];
    dic[@"token"] = [defaults objectForKey:@"token"];
    dic[@"type"] = [NSString stringWithFormat:@"%ld", self.type];
    __weak typeof(self)  weakSelf = self;
    [GMAfnTools PostHttpDataWithUrlStr:KCoursePJList Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
        [weakSelf.pjTableView.mj_header endRefreshing];
        for (NSMutableDictionary *dic in responseObject) {
            
            StatusModel *statuM = [[StatusModel alloc]init];
            [statuM mj_setKeyValues:dic];
            [weakSelf.statusArray addObject:statuM];
            
            for (NSMutableDictionary *comment_list in [dic objectForKey:@"user_comment_lists"]) {
                KCPJModel *moldel = [[KCPJModel alloc]init];
                [moldel mj_setKeyValues:comment_list];
                if (weakSelf.type == 1) {
                    moldel.comment_title = [NSString stringWithFormat:@"%@%@",@"评价课程: ",statuM.comment_title];
                } else if (weakSelf.type == 2) {
                    moldel.comment_title = [NSString stringWithFormat:@"%@%@",@"评价专栏: ",statuM.comment_title];
                } else if (weakSelf.type == 3) {
                    moldel.comment_title = [NSString stringWithFormat:@"%@%@",@"评价文章: ",statuM.comment_title];
                }
                [weakSelf.myCoursePLArray addObject:moldel];
            }
        }
        
        [weakSelf.pjTableView reloadData];
    } FailureBlock:^(id  _Nonnull error) {
        [weakSelf.pjTableView.mj_header endRefreshing];
    }];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self.pjTableView cellHeightForIndexPath:indexPath model:[self.myCoursePLArray objectAtIndex:indexPath.row] keyPath:@"model" cellClass:[MyPLTableViewCell class] contentViewWidth:KScreenW];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.statusArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"cellID";
    MyPLTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[MyPLTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    KCPJModel *kcModel = [self.myCoursePLArray objectAtIndex:indexPath.row];
    cell.model = kcModel;
    [cell useCellFrameCacheWithIndexPath:indexPath tableView:tableView];
    cell.zanButtonBlock = ^(UIButton * _Nonnull indexSelect) {
        indexSelect.selected = !indexSelect.selected;
        if (indexSelect.selected) {
            NSLog(@"点赞了");
            
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            dic[@"comment_lists_id"] = kcModel.comment_id;
            __weak typeof(self)  weakSelf = self;
            [GMAfnTools PostHttpDataWithUrlStr:KGiveThumbsUpURL Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
                if (responseObject) {
                    [weakSelf showError:[responseObject objectForKey:KSucessMSG]];
                    [weakSelf.pjTableView reloadData];
                }
            } FailureBlock:^(id  _Nonnull error) {
                
            }];
            
        }else {
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            dic[@"comment_lists_id"] = kcModel.comment_id;
            __weak typeof(self)  weakSelf = self;
            [GMAfnTools PostHttpDataWithUrlStr:KCancelGiveThumb Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
                if (responseObject) {
                    
                    [weakSelf showError:[responseObject objectForKey:KSucessMSG]];
                    [weakSelf.pjTableView reloadData];
                }
            } FailureBlock:^(id  _Nonnull error) {
                
            }];
        }
    };
    return cell;
}
#pragma mark -- 提示框
- (void)showError:(NSString *)errorMsg {
    // 1.弹框提醒
    // 初始化对话框
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:errorMsg preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:nil]];
    // 弹出对话框
    [self presentViewController:alert animated:true completion:nil];
    
    [self performSelector:@selector(dimisssAlert:) withObject:alert afterDelay:1];
    
}

- (void) dimisssAlert:(UIAlertController *)alert
{
    if (alert) {
        [alert dismissViewControllerAnimated:YES completion:nil];
    }
    
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


@end
