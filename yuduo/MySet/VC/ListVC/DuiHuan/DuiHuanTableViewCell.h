//
//  DuiHuanTableViewCell.h
//  yuduo
//
//  Created by Mac on 2019/9/26.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RedeemCodeLogs.h"
NS_ASSUME_NONNULL_BEGIN

@interface DuiHuanTableViewCell : UITableViewCell
PropertyStrong(UIView, bgView);
PropertyStrong(UIImageView, headImg);
PropertyStrong(UILabel, titleLable);
PropertyStrong(UIImageView, keshiImg);
PropertyStrong(UILabel, keshiLable);
PropertyStrong(UIImageView, seeImg);
PropertyStrong(UILabel, peopleLable);
PropertyStrong(UILabel, mianfeiLable);
PropertyStrong(UILabel, xPriceLabel);
PropertyStrong(UILabel, priceLabel);
PropertyStrong(UILabel, timeLabel);
PropertyStrong(RedeemCodeLogs, model);
@end

NS_ASSUME_NONNULL_END
