//
//  DuiHuanLogViewController.m
//  yuduo
//
//  Created by Mac on 2019/9/26.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "DuiHuanLogViewController.h"
#import "DuiHuanTableViewCell.h"
#import "RedeemCodeLogs.h"

@interface DuiHuanLogViewController ()<UITableViewDataSource,UITableViewDelegate>
PropertyStrong(UITableView, myZXTableView);
PropertyStrong(NSMutableArray, dataArray);
@end

@implementation DuiHuanLogViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.dataArray = [NSMutableArray new];
    [self changeNavigation];
    
    self.myZXTableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.myZXTableView.delegate = self;
    self.myZXTableView.dataSource = self;
    self.myZXTableView.backgroundColor = RGB(244, 243, 244);
    self.myZXTableView.tableFooterView = [[UIView alloc] initWithFrame:(CGRectZero)];
    [self.view addSubview:self.myZXTableView];
    self.myZXTableView.sd_layout.topSpaceToView(self.view, MStatusBarHeight+54)
    .leftSpaceToView(self.view, 0)
    .widthIs(KScreenW).bottomSpaceToView(self.view, 0);
    [self requestData];
}


- (void)changeNavigation {
    
    self.navigationController.navigationBar.topItem.title = @"兑换记录";
    UIView *titleView = [[UIView alloc]init];
    titleView.backgroundColor = GMWhiteColor;
    [self.view addSubview:titleView];
    titleView.sd_layout.topSpaceToView(self.view,0)
    .leftSpaceToView(self.view, 0)
    .rightSpaceToView(self.view, 0)
    .widthIs(KScreenW).heightIs(MStatusBarHeight+44);
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setBackgroundImage:[UIImage imageNamed:@"back-icon"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
    [titleView addSubview:backButton];
    backButton.sd_layout.bottomSpaceToView(titleView,13)
    .leftSpaceToView(titleView, 15)
    .widthIs(11).heightIs(19);
    UIButton *backButtonView = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 80, MStatusBarHeight+44)];
    backButtonView.backgroundColor = RGBA(15, 15, 15, 0);
    [backButtonView addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButtonView];
    UILabel *titleLabel = [[UILabel alloc]init];
    [titleView addSubview:titleLabel];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"兑换记录" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 18],NSForegroundColorAttributeName: [UIColor colorWithRed:69/255.0 green:69/255.0 blue:69/255.0 alpha:1.0]}];
    titleLabel.attributedText = string;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.sd_layout.bottomSpaceToView(titleView, 15)
    .centerXEqualToView(titleView)
    .heightIs(18).widthIs(100);
    
}

- (void)requestData {
    NSMutableDictionary *infoDic = [NSMutableDictionary dictionary];
    NSUserDefaults *detafaults = [NSUserDefaults standardUserDefaults];
    NSString  *user_id =  [detafaults objectForKey:@"user_id"];
    NSString *token =  [detafaults objectForKey:@"token"];
    infoDic[@"user_id"] = user_id;
    infoDic[@"token"] = token;
    __weak typeof(self) weakSelf = self;
    [GMAfnTools PostHttpDataWithUrlStr:KRedeemLogs Dic:infoDic SuccessBlock:^(id  _Nonnull responseObject) {
        for (NSDictionary *dic in responseObject) {
            RedeemCodeLogs *model = [RedeemCodeLogs new];
            [model mj_setKeyValues:dic];
            [weakSelf.dataArray addObject:model];
        }
        [weakSelf.myZXTableView reloadData];
    } FailureBlock:^(id  _Nonnull error) {
        NSLog(@"获取兑换记录失败 == %@",error);
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 150;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"cellID";
    DuiHuanTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[DuiHuanTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        
    }
    cell.model = self.dataArray[indexPath.row];
    return cell;
}

- (void)backBtn {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
