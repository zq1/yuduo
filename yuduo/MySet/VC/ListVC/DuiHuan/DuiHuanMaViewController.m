//
//  DuiHuanMaViewController.m
//  yuduo
//
//  Created by Mac on 2019/8/13.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "DuiHuanMaViewController.h"
#import "DuiHuanLogViewController.h"
@interface DuiHuanMaViewController ()
PropertyStrong(UITextField, duiHuanText);
@end

@implementation DuiHuanMaViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = GMWhiteColor;
    [self changeNavigation];
    // Do any additional setup after loading the view.
}


- (void)changeNavigation {
    
    self.navigationController.navigationBar.topItem.title = @"使用兑换码";
    UIView *titleView = [[UIView alloc]init];
    titleView.backgroundColor = GMWhiteColor;
    [self.view addSubview:titleView];
    titleView.sd_layout.topSpaceToView(self.view,0)
    .leftSpaceToView(self.view, 0)
    .rightSpaceToView(self.view, 0)
    .widthIs(KScreenW).heightIs(MStatusBarHeight+44);
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setBackgroundImage:[UIImage imageNamed:@"back-icon"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
    [titleView addSubview:backButton];
    backButton.sd_layout.bottomSpaceToView(titleView,13)
    .leftSpaceToView(titleView, 15)
    .widthIs(11).heightIs(19);
    UIButton *backButtonView = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 80, MStatusBarHeight+44)];
    backButtonView.backgroundColor = RGBA(15, 15, 15, 0);
    [backButtonView addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButtonView];
    UILabel *titleLabel = [[UILabel alloc]init];
    [titleView addSubview:titleLabel];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"使用兑换码" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 18],NSForegroundColorAttributeName: [UIColor colorWithRed:69/255.0 green:69/255.0 blue:69/255.0 alpha:1.0]}];
    titleLabel.attributedText = string; 
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.sd_layout.bottomSpaceToView(titleView, 15)
    .centerXEqualToView(titleView)
    .heightIs(18).widthIs(100);
    
    UIButton *zxLable = [UIButton buttonWithType:UIButtonTypeCustom];
    [zxLable setTitleColor:RGB(102, 102, 102) forState:UIControlStateNormal];
    [zxLable.titleLabel setFont:[UIFont fontWithName:@"PingFangSC-Regular" size: 16]];
    [zxLable addTarget:self action:@selector(duiHuanLog:) forControlEvents:UIControlEventTouchUpInside];
    [zxLable setTitle:@"兑换记录" forState:UIControlStateNormal];
    [titleView addSubview:zxLable];
    zxLable.sd_layout.bottomSpaceToView(titleView, 14)
    .rightSpaceToView(titleView, 17)
    .widthIs(70).heightIs(16);
    
    UIImageView *logoImg = [[UIImageView alloc]init];
    logoImg.image = [UIImage imageNamed:@"logo"];
    [self.view addSubview:logoImg];
    logoImg.sd_layout.topSpaceToView(self.view, MStatusBarHeight+135)
    .centerXEqualToView(self.view)
    .widthIs(60).heightIs(60);
    
    //请输入兑换码
    self.duiHuanText = [[UITextField alloc]init];
    self.duiHuanText.placeholder = @"请输入兑换码";
    self.duiHuanText.backgroundColor = RGB(245, 245, 245);
    [self.view addSubview:self.duiHuanText];
    self.duiHuanText.sd_layout.topSpaceToView(logoImg, 50)
    .leftSpaceToView(self.view, 15)
    .rightSpaceToView(self.view, 15)
    .heightIs(44);
    
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 44)];
    self.duiHuanText.leftViewMode = UITextFieldViewModeAlways;
    self.duiHuanText.leftView = leftView;
    
    //立即兑换
    UIButton *duiHuanBtn = [UIButton buttonWithType:UIButtonTypeCustom];
   duiHuanBtn.backgroundColor = RGB(23, 153, 167);
   duiHuanBtn.layer.cornerRadius = 22.5;
   duiHuanBtn.tag = 10002;
    [duiHuanBtn addTarget:self action:@selector(duiHuanButton:) forControlEvents:UIControlEventTouchUpInside];
    [duiHuanBtn setTitleColor:GMWhiteColor forState:UIControlStateNormal];
    [duiHuanBtn setTitle:@"立即兑换" forState:UIControlStateNormal];
    [self.view addSubview:duiHuanBtn];
    duiHuanBtn.sd_layout.topSpaceToView(logoImg, 154)
    .leftSpaceToView(self.view, 15)
    .rightSpaceToView(self.view, 15)
    .heightIs(45);
    //    [self cateTableViewCreate];
}
- (void)duiHuanButton:(UIButton *)send {
    if (self.duiHuanText.text == nil || [self.duiHuanText.text isEqualToString:@""]) {
        [self showError:@"请输入兑换码"];
        return;
    }
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *user_id = [defaults objectForKey:@"user_id"];
    NSString *token = [defaults objectForKey:@"token"];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"user_id"] = user_id;
    dic[@"token"] = token;
    dic[@"android_ios_id"] = DIV_UUID;
    dic[@"redeemcode"] = self.duiHuanText.text;
    [GMAfnTools PostHttpDataWithUrlStr:KRedeemcode Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"打印token ---=--%@",responseObject);
        if ( [responseObject objectForKey:@"code"] && [[responseObject objectForKey:@"code"] isEqualToString:@"1"]){
            [self showError:@"兑换成功"];
        } else {
            [self showError:[responseObject objectForKey:@"msg"]];
        }
    } FailureBlock:^(id  _Nonnull error) {
        NSLog(@"上传错误===%@",error);
    }];
    
}


#pragma mark -- 提示框
- (void)showError:(NSString *)errorMsg {
    // 1.弹框提醒
    // 初始化对话框
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:errorMsg preferredStyle:UIAlertControllerStyleAlert];
//    [alert addAction:[UIAlertAction actionWithTitle:@"请输入兑换码" style:UIAlertActionStyleDefault handler:nil]];
    // 弹出对话框
    [self presentViewController:alert animated:true completion:nil];
    
    [self performSelector:@selector(dimisssAlert:) withObject:alert afterDelay:1.3];
    
}

- (void) dimisssAlert:(UIAlertController *)alert
{
    if (alert) {
        [alert dismissViewControllerAnimated:YES completion:nil];
    }
    
}

- (void)duiHuanLog:(UIButton *)send {
    NSLog(@"兑换记录");
    DuiHuanLogViewController *tab = [[DuiHuanLogViewController alloc]init];
    [self.navigationController pushViewController:tab animated:YES];
}
- (void)backBtn {
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
