//
//  DuiHuanTableViewCell.m
//  yuduo
//
//  Created by Mac on 2019/9/26.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "DuiHuanTableViewCell.h"

@implementation DuiHuanTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier]) {
        
        [self zhuanlanLayout];
    }
    return self;
}

- (void)setModel:(RedeemCodeLogs *)model {
    _model = model;
    self.timeLabel.text = [NSString stringWithFormat:@"兑换时间: %@", model.hange_time];
    self.titleLable.text = model.title;
    self.titleLable.text = model.class_hour;
    [self.headImg sd_setImageWithURL:[NSURL URLWithString:model.picture]];
}

- (void)zhuanlanLayout {
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = GMWhiteColor;
    [self.contentView addSubview:self.bgView];
    self.bgView.sd_layout.topSpaceToView(self.contentView, 0)
    .leftSpaceToView(self.contentView,0)
    .widthIs(KScreenW).bottomEqualToView(self.contentView);

    self.timeLabel = [[UILabel alloc]init];
    self.timeLabel.font = [UIFont fontWithName:KPFType size:14];
    self.timeLabel.textColor = RGB(69, 69, 69);
    self.timeLabel.text = @"兑换时间 : ";
    [self.bgView addSubview:self.timeLabel];
    self.timeLabel.sd_layout.topSpaceToView(self.bgView, 0)
    .leftSpaceToView(self.bgView, 15)
    .rightSpaceToView(self.bgView, 15).heightIs(39);
    self.headImg = [[UIImageView alloc]init];
    self.headImg.backgroundColor = GMlightGrayColor;
    
    UIView *line = [UIView new];
    line.backgroundColor = RGBA(200, 200, 200, 0.5);
    [self.bgView addSubview:line];
    
    line.sd_layout.topSpaceToView(self.timeLabel, 0).leftEqualToView(self.bgView).rightEqualToView(self.bgView).heightIs(0.5);
    
    [self.bgView addSubview:self.headImg];
    
    self.headImg.sd_layout.topSpaceToView(line, 15)
    .leftSpaceToView(self.bgView, 14)
    .widthIs(120).heightIs(80);
    //
    self.titleLable = [[UILabel alloc]init];
    self.titleLable.numberOfLines = 2;
    self.titleLable.font = [UIFont fontWithName:KPFType size:14];
    self.titleLable.textColor = RGB(69, 69, 69);
    self.titleLable.text = @"前央视主持人李雷：50堂魅力声音必修课";
    [self.bgView addSubview:self.titleLable];
    self.titleLable.sd_layout.topSpaceToView(self.bgView, 60)
    .leftSpaceToView(self.headImg, 15)
    .widthIs(210).autoHeightRatio(0);
    
    self.keshiImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"更多"]];
    [self.bgView addSubview:self.keshiImg];
    self.keshiImg.sd_layout.bottomSpaceToView(self.bgView, 20)
    .leftSpaceToView(self.headImg, 15)
    .widthIs(10).heightIs(15);
    //
    self.keshiLable = [[UILabel alloc]init];
    self.keshiLable.font = [UIFont fontWithName:KPFType size:11];
    self.keshiLable.textColor = RGB(153, 153, 153);
    self.keshiLable.text = @"55课时";
    [self.bgView addSubview:self.keshiLable];
    self.keshiLable.sd_layout.bottomSpaceToView(self.bgView, 20)
    .leftSpaceToView(self.keshiImg, 11)
    .widthIs(37).heightIs(15);
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
