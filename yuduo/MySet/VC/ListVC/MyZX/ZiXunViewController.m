//
//  ZiXunViewController.m
//  yuduo
//
//  Created by Mac on 2019/8/13.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "ZiXunViewController.h"
#import "TapSliderScrollView.h"
#import "SimpleTapSliderScrollView.h"
#import "HotZXViewController.h"
#import "myZXViewController.h"
#import "ZXDetailViewController.h"
#import "ZXShuoMingViewController.h"
@interface ZiXunViewController ()
<SliderLineViewDelegate,SimpleSliderLineViewDelegate,UITableViewDelegate,UITableViewDataSource, UIScrollViewDelegate>
    
@property (nonatomic ,assign) BOOL selectSort;
    
@property (nonatomic, strong) NSArray *dataArr;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) NSMutableArray *myChildViewControllers;
    
@end
    
@implementation ZiXunViewController
    
- (void)viewDidLoad {
        [super viewDidLoad];
        self.navigationController.navigationBar.hidden = YES;
        [self changeNavigation];
        [self scrollZX];
    self.view.backgroundColor = GMWhiteColor;
    
        //    [self tab];
        // Do any additional setup after loading the view.
    }
#pragma mark 导航栏文字返回
    - (void)viewWillAppear:(BOOL)animated
    {
        [super viewWillAppear:animated];
        self.navigationController.navigationBar.hidden = YES;
        self.navigationController.navigationBar.topItem.title = @"发现";
    }
    
    - (void)scrollZX {
        
        HotZXViewController *vc1 = [[HotZXViewController alloc]init];
        myZXViewController *vc2 = [[myZXViewController alloc]init];
        SimpleTapSliderScrollView *simpleview = [[SimpleTapSliderScrollView alloc]initWithFrame:CGRectMake(0, 45+MStatusBarHeight, KScreenW, KScreenH)];
        simpleview.delegate = self;
        simpleview.sliderViewColor = RGB(21, 152, 164);
        simpleview.titileColror = RGB(153, 153, 153);
        simpleview.selectedColor = RGB(21, 152, 164);
        [simpleview createView:@[@"热门咨询",@"我的咨询"] andViewArr:@[vc1,vc2] andRootVc:self];
        [self.view addSubview:simpleview];
        [simpleview sliderToViewIndex:0];
    }
#pragma mark sliderDelegate -- OC
    -(void)sliderViewAndReloadData:(NSInteger)index
    {
        NSLog(@"刷新数据啦%ld",index);
        if (index == 3) {
            NSLog(@"创建tableview");
        }
    }
    
    
    - (void)tab {
        UITableView *table = [[UITableView alloc]initWithFrame:CGRectMake(0, 100, 300, 400) style:UITableViewStylePlain];
        table.backgroundColor = GMBlueColor;
        table.delegate =self;
        table.dataSource= self;
        [self.view addSubview:table];
    }
    
    
    - (void)changeNavigation {
        
        self.navigationController.navigationBar.topItem.title = @"咨询";
        UIView *titleView = [[UIView alloc]init];
        titleView.backgroundColor = GMWhiteColor;
        [self.view addSubview:titleView];
        titleView.sd_layout.topSpaceToView(self.view,0)
        .leftSpaceToView(self.view, 0)
        .rightSpaceToView(self.view, 0)
        .widthIs(KScreenW).heightIs(MStatusBarHeight+44);
        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [backButton setBackgroundImage:[UIImage imageNamed:@"back-icon"] forState:UIControlStateNormal];
        [backButton addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
        [titleView addSubview:backButton];
        backButton.sd_layout.bottomSpaceToView(titleView,13)
        .leftSpaceToView(titleView, 15)
        .widthIs(11).heightIs(19);
        UIButton *backButtonView = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 80, MStatusBarHeight+44)];
        backButtonView.backgroundColor = RGBA(15, 15, 15, 0);
        [backButtonView addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:backButtonView];
        UILabel *titleLabel = [[UILabel alloc]init];
        [titleView addSubview:titleLabel];
        NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"咨询" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 18],NSForegroundColorAttributeName: [UIColor colorWithRed:69/255.0 green:69/255.0 blue:69/255.0 alpha:1.0]}];
        titleLabel.attributedText = string;
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.sd_layout.bottomSpaceToView(titleView, 15)
        .centerXEqualToView(titleView)
        .heightIs(18).widthIs(36);
        
        UIButton *zxLable = [UIButton buttonWithType:UIButtonTypeCustom];
        [zxLable setTitleColor:RGB(21, 151, 164) forState:UIControlStateNormal];
        [zxLable.titleLabel setFont:[UIFont fontWithName:@"PingFangSC-Regular" size: 16]];
        [zxLable setTitle:@"咨询说明" forState:UIControlStateNormal];
        [zxLable addTarget:self action:@selector(zxClick:) forControlEvents:UIControlEventTouchUpInside];
        [titleView addSubview:zxLable];
        zxLable.sd_layout.bottomSpaceToView(titleView, 14)
        .rightSpaceToView(titleView, 17)
        .widthIs(70).heightIs(16);
        
        //    [self cateTableViewCreate];
    }
- (void)zxClick:(UIButton *)send {
    NSLog(@"xxxx咨询说明");
    ZXShuoMingViewController *zx = [[ZXShuoMingViewController alloc]init];
    [self.navigationController pushViewController:zx animated:YES];
}
    
    
    
    - (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
    {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
      
        
    }
#pragma mark--返回行高
    - (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
        return 40;
    }
#pragma mark--返回行数
    - (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
    {
        return 10;
    }
    
    
    - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
        
        static NSString *key0=@"cell0";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:key0];
        if (!cell) {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:key0];
        }
        //    cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
        return cell;
    }
    
    - (void)backBtn {
        NSLog(@"xxxxxxxx");
        [self.navigationController popViewControllerAnimated:YES];
    }


@end
