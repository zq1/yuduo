//
//  ZXDetailViewController.m
//  yuduo
//
//  Created by mason on 2019/9/8.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "ZXDetailViewController.h"
#import "PJTableViewCell.h"
#import "DKSTextView.h"
#import "DKSKeyboardView.h"
#import "ZXHFDetailModel.h"
@interface ZXDetailViewController ()
<UITableViewDataSource,UITableViewDelegate,DKSKeyboardDelegate>

{
    UIImageView *_zxHeadImg;
    UIImageView *_headImg;
    UILabel *_myZXTitle;
    UIImageView *_timeImg;
    UILabel *_timeLabel;
}
PropertyStrong(UIImageView, zanImg);
PropertyStrong(UILabel, dianZanLabel);
PropertyStrong(UITableView, pjTableView);
//输入框
PropertyStrong(DKSKeyboardView, keyView);
//用户回复数组
PropertyStrong(NSMutableArray, hfarray);
@end
@implementation ZXDetailViewController
#pragma mark--解决返回导航消失问题
- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBar.topItem.title = @"发现";
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavigation];
    [self getZXDetailData];
    [self pjDetail];
    self.keyView = [[DKSKeyboardView alloc] initWithFrame:CGRectMake(0, KScreenH - 51, KScreenW, 51)];
    //设置代理方法
    __weak typeof(self) weakSelf = self;
    self.keyView.delegate = self;
    self.keyView.textBlock = ^(NSString *textString) {
        
        NSLog(@"xxxxxxxxxxX%@",textString);
        [weakSelf userReply:textString];
        
        
    };
    [self.view addSubview:_keyView];
    
    // Do any additional setup after loading the view.
}

#pragma mark--用户回复
- (void)userReply:(NSString *)testString {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *userReplyDic = [NSMutableDictionary dictionary];
    userReplyDic[@"user_id"] = [defaults objectForKey:@"user_id"];
    userReplyDic[@"token"] = [defaults objectForKey:@"token"];
    userReplyDic[@"manag_id"] = self.zxIDString;
    userReplyDic[@"manag_reply"] = testString;
    userReplyDic[@"manag_picture"] = @"";
    
    [GMAfnTools PostHttpDataWithUrlStr:KUserReply Dic:userReplyDic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"用户回复成功----%@",responseObject);
        if ([[responseObject objectForKey:@"code"] isEqualToString:@"1"]) {
            [self getZXDetailData];
            [self showError:[responseObject objectForKey:@"msg"]];
            
        }
        [self.pjTableView reloadData];
    } FailureBlock:^(id  _Nonnull error) {
        NSLog(@"用户回复失败----%@",error);
    }];
}
- (void)getZXDetailData {
    self.hfarray = [NSMutableArray array];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"manag_id"] = self.zxIDString;
    NSLog(@"%@",self.zxIDString);
    [GMAfnTools PostHttpDataWithUrlStr:KZXDetailUrl Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"用户咨询详情---%@",responseObject);
        NSMutableArray *manag_reply = [responseObject objectForKey:@"manag_reply"];
        for (NSMutableDictionary *detailDic in manag_reply) {
            
            ZXHFDetailModel *model = [[ZXHFDetailModel alloc]init];
            [model mj_setKeyValues:detailDic];
            [self.hfarray addObject:model];
            
            NSLog(@"用户回复详情 ---%@",self.hfarray);
        }
        //        [self pjTableView];
        [self.pjTableView reloadData];
    } FailureBlock:^(id  _Nonnull error) {
        NSLog(@"%@",error);
    }];
}

- (void)setNavigation {
    //        self.view.backgroundColor = RGB(244, 243, 244);
    self.view.backgroundColor = GMWhiteColor;
    
    UIView *titleView = [[UIView alloc]init];
    titleView.backgroundColor = GMWhiteColor;
    [self.view addSubview:titleView];
    titleView.sd_layout.topSpaceToView(self.view, 0)
    .leftEqualToView(self.view)
    .widthIs(KScreenW).heightIs(44+MStatusBarHeight);
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //    backButton.backgroundColor = GMBlueColor;
    [backButton setBackgroundImage:[UIImage imageNamed:@"back-icon"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
    [titleView addSubview:backButton];
    backButton.sd_layout.topSpaceToView(titleView,MStatusBarHeight+15)
    .leftSpaceToView(titleView, 15)
    .widthIs(11).heightIs(19);
    UIButton *backButtonView = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 80, MStatusBarHeight+44)];
    backButtonView.backgroundColor = RGBA(15, 15, 15, 0);
    [backButtonView addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButtonView];
    
    UILabel *forgetPsw = [[UILabel alloc]init];
    forgetPsw.text = @"咨询详情";
    forgetPsw.font = kFont(18);
    forgetPsw.textColor = RGB(69, 69, 69);
    forgetPsw.textAlignment = NSTextAlignmentCenter;
    [titleView addSubview:forgetPsw];
    forgetPsw.sd_layout.topSpaceToView(titleView, MStatusBarHeight+15)
    .centerXEqualToView(titleView).widthIs(150).heightIs(17);
    
    
    _zxHeadImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"矩形 8"]];
    //    _zxHeadImg.backgroundColor = GMlightGrayColor;
    _zxHeadImg.backgroundColor = GMWhiteColor;
    _zxHeadImg.layer.cornerRadius = 10;
    _zxHeadImg.layer.shadowColor = [GMBlackColor CGColor];
    _zxHeadImg.layer.shadowOffset = CGSizeMake(0, 0);
    _zxHeadImg.layer.shadowOpacity = 0.3;
    _zxHeadImg.layer.shadowRadius = 1.0;
    _zxHeadImg.userInteractionEnabled = YES;
    //_zxHeadImg.layer.masksToBounds = YES;
    [self.view addSubview:_zxHeadImg];
    _zxHeadImg
    .sd_layout.topSpaceToView(self.view, MStatusBarHeight+59)
    .leftSpaceToView(self.view, 15)
    .widthIs(KScreenW-30).heightIs(90);
    
    _headImg = [[UIImageView alloc]init];
    _headImg.backgroundColor = GMBrownColor;
    _headImg.layer.cornerRadius = 30;
    _headImg.layer.masksToBounds = YES;
    [_headImg sd_setImageWithURL:[NSURL URLWithString:self.zxpicString]];
    [_zxHeadImg addSubview:_headImg];
    _headImg.sd_layout.topSpaceToView(_zxHeadImg, 15)
    .leftSpaceToView(_zxHeadImg, 15)
    .widthIs(60).heightIs(60);
    
    _myZXTitle = [[UILabel alloc]init];
    _myZXTitle.textColor = RGB(69, 69, 69);
    _myZXTitle.text = @"咨询标题";
    _myZXTitle.text = self.zxtitleString;
    _myZXTitle.font = [UIFont fontWithName:KPFType size:14];
    [_zxHeadImg addSubview:_myZXTitle];
    _myZXTitle.sd_layout.topSpaceToView(_zxHeadImg, 24)
    .leftSpaceToView(_headImg, 18)
    .heightIs(14).rightSpaceToView(_zxHeadImg, 15);
    
    _timeImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ico_time"]];
    [_zxHeadImg addSubview:_timeImg];
    _timeImg.sd_layout.topSpaceToView(_myZXTitle, 14)
    .leftSpaceToView(_headImg, 18)
    .widthIs(15).heightIs(15);
    
    _timeLabel = [[UILabel alloc]init];
    _timeLabel.text = @"2010-20-2";
    _timeLabel.text = self.zxtimeString;
    _timeLabel.font = [UIFont fontWithName:KPFType size:12];
    _timeLabel.textColor = RGB(153, 153, 153);
    [_zxHeadImg addSubview:_timeLabel];
    _timeLabel.sd_layout.topSpaceToView(_myZXTitle, 18)
    .leftSpaceToView(_timeImg, 10)
    .widthIs(125).heightIs(12);
    self.zanImg = [[UIImageView alloc]init];
    //    self.zanImg.backgroundColor = GMGreenColor;
        self.zanImg.image = [UIImage imageNamed:@"ico_zan_1"];
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(zanImgTouchUpInside:)];
        self.zanImg.userInteractionEnabled = YES;
        [self.zanImg addGestureRecognizer:tap];
    [_zxHeadImg addSubview:self.zanImg];
    self.zanImg.sd_layout.topSpaceToView(_myZXTitle, 16)
        .leftSpaceToView(_timeLabel, 50*kGMWidthScale)
        .widthIs(15)
        .heightIs(15);
    self.dianZanLabel = [[UILabel alloc] init];
    //    label.frame = CGRectMake(324.5,2785.5,20.5,9);
    self.dianZanLabel.numberOfLines = 0;
    self.dianZanLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:12];
    self.dianZanLabel.text = self.likeNumber;
    self.dianZanLabel.textColor = RGB(153, 153, 153);
    [_zxHeadImg addSubview:self.dianZanLabel];
    self.dianZanLabel.sd_layout.topSpaceToView(_myZXTitle, 20)
    .leftSpaceToView(self.zanImg, 5*kGMWidthScale).widthIs(25).heightIs(9);
}
-(void)zanImgTouchUpInside:(UIGestureRecognizer *)tap{
    NSMutableDictionary* dic = [[NSMutableDictionary alloc] init];
       [dic setValue:self.zxIDString forKey:@"manag_id"];
    [GMAfnTools PostHttpDataWithUrlStr:KhomeTalkLike Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
        //点赞成功
        if ([responseObject [@"code"] isEqualToString:@"1"]) {
            NSLog(@"点赞状态：%@",responseObject);
            self.dianZanLabel.text = [NSString stringWithFormat:@"%ld",[self.likeNumber integerValue] + 1];
        }
    } FailureBlock:^(id  _Nonnull error) {
        //点赞失败
    }];
}

- (void)pjDetail {
    
    self.pjTableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.pjTableView.delegate = self;
    self.pjTableView.dataSource = self;
    self.pjTableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    self.pjTableView.separatorColor = [UIColor lightGrayColor];
    self.pjTableView.tableFooterView = [UIView new];
    [self.view addSubview:self.pjTableView];
    self.pjTableView.sd_layout.topSpaceToView(_zxHeadImg, 10)
    .leftSpaceToView(self.view, 0)
    .widthIs(KScreenW)
    .heightIs(KScreenH-MStatusBarHeight-44-90-50-25);
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self.pjTableView cellHeightForIndexPath:indexPath model:[self.hfarray objectAtIndex:indexPath.row] keyPath:@"model" cellClass:[PJTableViewCell class] contentViewWidth:KScreenW];
}

-(CGFloat)cellHeightForIndexPath:(NSIndexPath *)indexPath cellContentViewWidth:(CGFloat)width tableView:(UITableView *)tableView {
    return 200;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.hfarray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"cellID";
    PJTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[PJTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.model = [self.hfarray objectAtIndex:indexPath.row];
    [cell useCellFrameCacheWithIndexPath:indexPath tableView:tableView];
    return cell;
}

#pragma mark -- 提示框
- (void)showError:(NSString *)errorMsg {
    // 1.弹框提醒
    // 初始化对话框
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:errorMsg preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:nil]];
    // 弹出对话框
    [self presentViewController:alert animated:true completion:nil];
    
    [self performSelector:@selector(dimisssAlert:) withObject:alert afterDelay:1];
    
}

- (void) dimisssAlert:(UIAlertController *)alert
{
    if (alert) {
        [alert dismissViewControllerAnimated:YES completion:nil];
    }
    
}

- (void)backBtn {
    [self.navigationController popViewControllerAnimated:YES];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
