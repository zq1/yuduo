//
//  ZXDetailViewController.h
//  yuduo
//
//  Created by mason on 2019/9/8.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZXDetailViewController : UIViewController
PropertyStrong(NSString, zxIDString);
PropertyStrong(NSString, zxtitleString);
PropertyStrong(NSString, zxtimeString);
PropertyStrong(NSString, zxpicString);
PropertyStrong(NSString, likeNumber);
@end

NS_ASSUME_NONNULL_END
