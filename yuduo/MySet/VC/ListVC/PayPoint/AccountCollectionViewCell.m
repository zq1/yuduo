//
//  AccountCollectionViewCell.m
//  yuduo
//
//  Created by Mac on 2019/9/10.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "AccountCollectionViewCell.h"

@implementation AccountCollectionViewCell
-(instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.priceLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 96, 58)];
        self.priceLabel.textAlignment = NSTextAlignmentCenter;
        self.priceLabel.textColor = RGB(21, 153, 164);
        
        [self addSubview:self.priceLabel];
        
        self.numPriceLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
        [self.priceLabel addSubview:self.numPriceLabel];
    }
    
    return self;
}
@end
