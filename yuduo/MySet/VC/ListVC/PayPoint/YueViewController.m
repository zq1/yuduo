//
//  YueViewController.m
//  yuduo
//
//  Created by Mac on 2019/8/13.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "YueViewController.h"
#import "YueTableViewCell.h"
#import "AccountViewController.h"
//消费记录
#import "RecordModel.h"
@interface YueViewController ()<UITableViewDelegate,UITableViewDataSource>
PropertyStrong(UITableView, myZXTableView);
PropertyStrong(UIImageView, headImg);
PropertyStrong(UILabel, moneyLabel);
PropertyStrong(UILabel, yueLabel);
PropertyStrong(NSMutableArray, recordArray);

@end

@implementation YueViewController

-(void)viewWillAppear:(BOOL)animated {
    [self getBalance];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = RGB(244, 243, 244);
    [self changeNavigation];
    [self payRecord];
    // Do any additional setup after loading the view.
}
#pragma mark -- 消费记录

- (void)payRecord {
    self.recordArray = [NSMutableArray array];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *user_id = [defaults objectForKey:@"user_id"];
    NSString *token = [defaults objectForKey:@"token"];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"user_id"] = user_id;
    dic[@"token"] = token;
    [GMAfnTools PostHttpDataWithUrlStr:KPayRecord Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"打印 消费记录====%@",responseObject);
        for (NSMutableDictionary *dic in responseObject) {
            RecordModel *mol = [[RecordModel alloc]init];
            [mol mj_setKeyValues:dic];
            [self.recordArray addObject:mol];
        }
        [self.myZXTableView reloadData];
    } FailureBlock:^(id  _Nonnull error) {
        
    }];
}

- (void)getBalance {
    
    //获取育点
    NSUserDefaults *defauls = [NSUserDefaults standardUserDefaults];
    NSString *user_id = [defauls objectForKey:@"user_id"];
    NSString *token = [defauls objectForKey:@"token"];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"user_id"] = user_id;
    dic[@"token"] = token;
    [GMAfnTools PostHttpDataWithUrlStr:KYDBlanceURL Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
        for (NSMutableDictionary *dic in responseObject) {
            self.yueLabel.text = [@"¥"stringByAppendingString:[dic objectForKey:@"balance"]];
        }
    } FailureBlock:^(id  _Nonnull error) {
        NSLog(@"xxxxx获取x%@",error);
    }];
}

- (void)changeNavigation {
    self.headImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg"]];
    self.headImg.userInteractionEnabled = YES;
    [self.view addSubview:self.headImg];
    self.headImg.sd_layout.topSpaceToView(self.view, 0)
    .leftSpaceToView(self.view, 0)
    .rightSpaceToView(self.view, 0)
    .widthIs(KScreenW).heightIs(150*kGMHeightScale);
    
//    self.navigationController.navigationBar.topItem.title = @"活动详情";
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:[UIImage imageNamed:@"wback"] forState:UIControlStateNormal];
    
    [backButton addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.headImg addSubview:backButton];
    UIButton *backButtonView = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 80, MStatusBarHeight+44)];
    backButtonView.backgroundColor = RGBA(15, 15, 15, 0);
    [backButtonView addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.headImg addSubview:backButtonView];
    backButton.sd_layout.topSpaceToView(self.headImg,MStatusBarHeight+12)
    .leftSpaceToView(self.headImg, 15)
    .widthIs(11).heightIs(19);
    
    UILabel *titleLabel = [[UILabel alloc]init];
    [self.headImg addSubview:titleLabel];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"我的余额" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 18],NSForegroundColorAttributeName: [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0]}];
    titleLabel.attributedText = string;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.sd_layout.topSpaceToView(self.headImg, MStatusBarHeight+13)
    .centerXEqualToView(self.headImg)
    .heightIs(18).widthIs(100);
    
    
    UIButton *czButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [czButton setTitle:@"充值" forState:UIControlStateNormal];
    [czButton setTitleColor:GMWhiteColor forState:UIControlStateNormal];
    czButton.titleLabel.font = kFont(14);
    [czButton addTarget:self action:@selector(czClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.headImg addSubview:czButton];
    czButton.sd_layout.topSpaceToView(self.headImg,MStatusBarHeight+14-19)
    .rightSpaceToView(self.headImg, 15)
    .widthIs(50).heightIs(50);
    
    //    //显示余额标签
       self.yueLabel = [[UILabel alloc]init];
       self.yueLabel.backgroundColor = GMWhiteColor;
        [self.yueLabel setTextAlignment:NSTextAlignmentCenter];
        self.yueLabel.text = @"¥10086";
        self.yueLabel.font = [UIFont fontWithName:KPFType size:18];
    self.yueLabel.textColor = RGB(21, 151, 164);
        self.yueLabel.layer.cornerRadius = 20;
        self.yueLabel.layer.masksToBounds = YES;
        [self.headImg addSubview:self.yueLabel];
        self.yueLabel.sd_layout.topSpaceToView(self.headImg, MStatusBarHeight+59)
        .centerXEqualToView(self.headImg)
        .widthIs(120).heightIs(36);
    
    UILabel *jiluLabel = [[UILabel alloc]init];
//    jiluLabel.backgroundColor = GMBlueColor;
    [self.view addSubview:jiluLabel];
    jiluLabel.sd_layout.topSpaceToView(self.headImg, 15)
    .leftSpaceToView(self.view, 15)
    .widthIs(55).heightIs(12);
    NSMutableAttributedString *jiluLabelString = [[NSMutableAttributedString alloc] initWithString:@"消费记录" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 12],NSForegroundColorAttributeName: [UIColor colorWithRed:69/255.0 green:69/255.0 blue:69/255.0 alpha:1.0]}];
    jiluLabel.attributedText = jiluLabelString;
    
    self.myZXTableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.myZXTableView.delegate = self;
    self.myZXTableView.dataSource = self;
    
    [self.myZXTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.myZXTableView.backgroundColor = RGB(244, 243, 244);
    [self.view addSubview:self.myZXTableView];
    self.myZXTableView.sd_layout.topSpaceToView(jiluLabel, 20)
    .leftSpaceToView(self.view, 0)
    .widthIs(KScreenW).heightIs(KScreenH-90-MStatusBarHeight-100);
    
    
}
#pragma mark--充值事件
- (void)czClick:(UIButton *)send {
    NSLog(@"点击了充值按钮");
    AccountViewController *actVC = [[AccountViewController alloc]init];
    [self.navigationController pushViewController:actVC animated:YES];
}
- (void)backBtn {
    NSLog(@"xxxxxxxx");
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.recordArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"cellID";
    YueTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[YueTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    RecordModel *mol = [self.recordArray objectAtIndex:indexPath.row];
    cell.titleLabel.text = mol.name;
    cell.timeLabel.text = mol.purchase_time;
    cell.priceLabel.text = [@"¥" stringByAppendingString:mol.money];
    cell.backgroundColor = RGB(244, 243, 244);
    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
