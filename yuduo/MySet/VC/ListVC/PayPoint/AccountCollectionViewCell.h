//
//  AccountCollectionViewCell.h
//  yuduo
//
//  Created by Mac on 2019/9/10.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AccountCollectionViewCell : UICollectionViewCell
PropertyStrong(UILabel, priceLabel);
PropertyStrong(UILabel, numPriceLabel);
@end

NS_ASSUME_NONNULL_END
