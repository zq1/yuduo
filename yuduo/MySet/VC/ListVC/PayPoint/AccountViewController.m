//
//  AccountViewController.m
//  yuduo
//
//  Created by Mac on 2019/8/14.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "AccountViewController.h"
#import "AccountCollectionViewCell.h"
#import "PayModel.h"
#import <StoreKit/StoreKit.h>
@interface AccountViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,SKPaymentTransactionObserver,SKProductsRequestDelegate>
{
    UILabel *payLayout;
}
PropertyStrong(UILabel,yueLabel);
PropertyStrong(UIImageView, headImg);
PropertyStrong(UILabel, moneyLabel);

//存储选中价格
PropertyStrong(NSString , priceString);
//存储选中商品ID
PropertyStrong(NSString,  productString);
//产品ID 价格
PropertyStrong(NSMutableArray, productIDPrice);
@end

@implementation AccountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = RGB(244, 243, 244);
    
    [self layoutAccount];
    [self declaration];
//    [self getPayProduct];
    
    [self getBalance];
    //    4.设置支付服务
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    // Do any additional setup after loading the view.
}
- (void)balance {
    [self getBalance];
}
#pragma mark--获取育点
- (void) getBalance {
    
    //获取育点
    NSUserDefaults *defauls = [NSUserDefaults standardUserDefaults];
    NSString *user_id = [defauls objectForKey:@"user_id"];
    NSString *token = [defauls objectForKey:@"token"];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"user_id"] = user_id;
    dic[@"token"] = token;
    [GMAfnTools PostHttpDataWithUrlStr:KYDBlanceURL Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"获取育点====%@",responseObject);
        for (NSMutableDictionary *dic in responseObject) {
            self.yueLabel.text = [[dic objectForKey:@"balance"] stringByAppendingString:@"育点"];
        }
    } FailureBlock:^(id  _Nonnull error) {
        NSLog(@"xxxxx获取x%@",error);
    }];
}
//#pragma mark--获取商品ID
//- (void)getPayProduct {
//
//    self.productIDPrice = [NSMutableArray array];
//    [GMAfnTools PostHttpDataWithUrlStr:KPayProductURL Dic:nil SuccessBlock:^(id  _Nonnull responseObject) {
//        NSLog(@"打印支付产品啊 ====%@",responseObject);
//        for (NSMutableDictionary *dic in responseObject) {
//            PayModel *model = [[PayModel alloc]init];
//            [model mj_setKeyValues:dic];
//            [self.productIDPrice addObject:model];
//        }
//    } FailureBlock:^(id  _Nonnull error) {
//
//    }];
//}
#pragma mark--确认支付
- (void)payButton:(UIButton *)send {
    
    NSLog(@"----打印产品价格==%@----打印产品ID==%@",self.priceString,self.productString);

    NSUserDefaults *defaus = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"user_id"] = [defaus objectForKey:@"user_id"];
    dic[@"token"] = [defaus objectForKey:@"token"];
    dic[@"product_id"] = self.productString;
    dic[@"pay_price"] =  self.priceString;

    [GMAfnTools PostHttpDataWithUrlStr:KPayOrderURL Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
        if ([[responseObject  objectForKey:@"code"] isEqualToString:@"1"]) {
            NSString *product = self.productString;
            if([SKPaymentQueue canMakePayments]){
                [self requestProductData:product];
            }else{
                NSLog(@"不允许程序内付费");
            }
        }else {
            [self showError:@"预订单创建失败"];
        }
    } FailureBlock:^(id  _Nonnull error) {
        NSLog(@"xxxxxxxxx%@",error);
    }];
    
    NSLog(@"-------------------------------------");
   
}
//请求商品
- (void)requestProductData:(NSString *)type{
    NSLog(@"-------------请求对应的产品信息----------------");
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD show]; 
    NSArray *product = [[NSArray alloc] initWithObjects:type,nil];
    NSSet *nsset = [NSSet setWithArray:product];
    SKProductsRequest *request = [[SKProductsRequest alloc] initWithProductIdentifiers:nsset];
    request.delegate = self;
    [request start];
    
}

//收到产品返回信息
- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response{
//    NSLog(@"--------------收到产品反馈消息---------------------");
    NSArray *product = response.products;
    if([product count] == 0){
        NSLog(@"--------------没有商品------------------");
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
        });
        return;
    }
    NSLog(@"productID:%@", response.invalidProductIdentifiers);
    NSLog(@"产品付费数量:%ld",[product count]);
    SKProduct *p = nil;
    for (SKProduct *pro in product) {
        NSLog(@"%@", [pro description]); //产品描述
        NSLog(@"%@", [pro localizedTitle]); //产品标题
        NSLog(@"%@", [pro localizedDescription]); //产品本地描述
        NSLog(@"%@", [pro price]); //产品价格
        NSLog(@"%@", [pro productIdentifier]); //产品ID
        // 如果后台消费条目的ID与这里需要请求一样 (用于确保订单的正确性)
//        if([pro.productIdentifier isEqualToString:@"yuduo_charge_6"]){
           if([pro.productIdentifier isEqualToString:self.priceString]){
            p = pro;
        }else{
            p = pro;
        }
    }
    if (p == nil) {
        return;
    }
    
    //发送购买请求
    SKPayment *payment = [SKPayment paymentWithProduct:p];
    NSLog(@"发送购买请求");
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}

//请求失败
- (void)request:(SKRequest *)request didFailWithError:(NSError *)error{
    NSLog(@"------------------错误-----------------:%@", error);
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss];
    });
}

- (void)requestDidFinish:(SKRequest *)request{
    NSLog(@"------------反馈信息结束-----------------");
}
//沙盒测试环境验证
#define SANDBOX @"https://sandbox.itunes.apple.com/verifyReceipt"
//正式环境验证
#define AppStore @"https://buy.itunes.apple.com/verifyReceipt"
/**
 *  验证购买，避免越狱软件模拟苹果请求达到非法购买问题
 *
 */
-(void)verifyPurchaseWithPaymentTransaction{
    
    //从沙盒中获取交易凭证并且拼接成请求体数据
    NSURL *receiptUrl = [[NSBundle mainBundle] appStoreReceiptURL];
    NSData *receiptData = [NSData dataWithContentsOfURL:receiptUrl];
//    NSLog(@"打印data----%@",receiptData);
    if(!receiptUrl){
        // 交易凭证为空验证失败
        NSLog(@"程序不允许付费");
        [self showError:@"程序不允许付费"];
        return;
    }
    //上传收据data 至服务器
//    NSLog(@"+++++++++++++打印收据--%@",receiptData);
    [SVProgressHUD setStatus:@"加载"];
    NSString *receiptString=[receiptData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];//转化为base64字符串
//    NSLog(@"------打印base64 String--%@",receiptString);
    
    NSUserDefaults *defauls = [NSUserDefaults standardUserDefaults];
    NSString *user_id = [defauls objectForKey:@"user_id"];
    NSString *token = [defauls objectForKey:@"token"];
    NSMutableDictionary *payDic = [NSMutableDictionary dictionary];
    payDic[@"user_id"] = user_id;
    payDic[@"token"] = token;
    payDic[@"receipt_data"] = receiptString;
//    NSLog(@"xxxxxxxxxx打印----参数user_id :%@ token :%@ receipt_data%@ :",user_id,token,receiptString);

    [GMAfnTools PostHttpDataWithUrlStr:@"https://cs1.91hzty.com/index/verifyReceipt" Dic:payDic SuccessBlock:^(id  _Nonnull responseObject) {
        [SVProgressHUD dismiss];
        NSLog(@"上传 recipString----%@",[responseObject objectForKey:@"status"]);
        if ([responseObject objectForKey:@"status"]) {
            [self getBalance];
        }
    } FailureBlock:^(id  _Nonnull error) {;
        
    }];
//
    
//    NSString *bodyString = [NSString stringWithFormat:@"{\"receipt-data\" : \"%@\"}", receiptString];//拼接请求数据
//    NSData *bodyData = [bodyString dataUsingEncoding:NSUTF8StringEncoding];
////    NSLog(@"xxxxxxx打印bodystring---%@",bodyString);
//    //创建请求到苹果官方进行购买验证
//    NSURL *url= [NSURL URLWithString:SANDBOX];
//    NSMutableURLRequest *requestM = [NSMutableURLRequest requestWithURL:url];
//    requestM.HTTPBody = bodyData;
//    requestM.HTTPMethod =   @"POST";
//    //创建连接并发送同步请求
//    NSError *error = nil;
//    NSData *responseData =[ NSURLConnection sendSynchronousRequest:requestM returningResponse:nil error:&error];
//    if (error) {
//        NSLog(@"验证购买过程中发生错误，错误信息：%@",error.localizedDescription);
//        return;
//    }
//    NSDictionary *dic=[NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:nil];
//    NSLog(@"----打印收据--%@",dic);
//    if([dic[@"status"] intValue]== 0){
//        NSLog(@"购买成功！");
//        NSDictionary *dicReceipt= dic[@"receipt"];
//        NSDictionary *dicInApp=[dicReceipt[@"in_app"] firstObject];
//        NSString *productIdentifier= dicInApp[@"product_id"];//读取产品标识
//        //如果是消耗品则记录购买数量，非消耗品则记录是否购买过
//        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//        if ([productIdentifier isEqualToString:@"yuduo_charge_6"]) {
//            long purchasedCount = [defaults integerForKey:productIdentifier];//已购买数量
//            [[NSUserDefaults standardUserDefaults] setInteger:(purchasedCount+1) forKey:productIdentifier];
////            NSLog(@"----------购买次数%ld",purchasedCount);
//        }else{
//            [defaults setBool:YES forKey:productIdentifier];
//        }
//        //在此处对购买记录进行存储，存储到服务器端
//    }else{
//        NSLog(@"购买失败，未通过验证！");
//    }
}

////监听购买结果
- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transaction{
    for(SKPaymentTransaction *tran in transaction){

        switch (tran.transactionState) {
            case SKPaymentTransactionStatePurchased:
                NSLog(@"交易完成");
                [self verifyPurchaseWithPaymentTransaction];
                [[SKPaymentQueue defaultQueue] finishTransaction:tran];
      
                break;
            case SKPaymentTransactionStatePurchasing:
                NSLog(@"商品添加进列表");
                
                break;
            case SKPaymentTransactionStateRestored:
                NSLog(@"已经购买过商品");
                [[SKPaymentQueue defaultQueue] finishTransaction:tran];
                break;
            case SKPaymentTransactionStateFailed:
                NSLog(@"交易失败");
                [SVProgressHUD dismiss];
                [[SKPaymentQueue defaultQueue] finishTransaction:tran];

                break;
            default:
                break;
        }
    }
}

//交易结束
- (void)completeTransaction:(SKPaymentTransaction *)transaction{
    NSLog(@"交易结束");
    
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
}


- (void)dealloc{
    [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(NSString * )environmentForReceipt:(NSString * )str
{
    str= [str stringByReplacingOccurrencesOfString:@"\r\n" withString:@""];
    
    str = [str stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    
    str = [str stringByReplacingOccurrencesOfString:@"\t" withString:@""];
    
    str=[str stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    str=[str stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    
    NSArray * arr=[str componentsSeparatedByString:@";"];
    
    //存储收据环境的变量
    NSString * environment=arr[2];
    return environment;
}

- (void)layoutAccount {
    
    self.headImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg"]];
    self.headImg.userInteractionEnabled = YES;
    [self.view addSubview:self.headImg];
    self.headImg.sd_layout.topSpaceToView(self.view, 0)
    .leftSpaceToView(self.view, 0)
    .rightSpaceToView(self.view, 0)
    .widthIs(KScreenW).heightIs(150*kGMHeightScale);
    
    //    self.navigationController.navigationBar.topItem.title = @"活动详情";
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:[UIImage imageNamed:@"wback"] forState:UIControlStateNormal];
    
    [backButton addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.headImg addSubview:backButton];
    backButton.sd_layout.topSpaceToView(self.headImg,MStatusBarHeight+12)
    .leftSpaceToView(self.headImg, 15)
    .widthIs(11).heightIs(19);
    
    UIButton *backButtonView = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 80, MStatusBarHeight+44)];
    backButtonView.backgroundColor = RGBA(15, 15, 15, 0);
    [backButtonView addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.headImg addSubview:backButtonView];
    UILabel *titleLabel = [[UILabel alloc]init];
    [self.headImg addSubview:titleLabel];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"我的账户" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 18],NSForegroundColorAttributeName: [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0]}];
    titleLabel.attributedText = string;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.sd_layout.topSpaceToView(self.headImg, MStatusBarHeight+13)
    .centerXEqualToView(self.headImg)
    .heightIs(18).widthIs(100);
    
 
    //    //显示余额标签
    self.yueLabel = [[UILabel alloc]init];
//    yueLabel.text = @"1999育点";
    self.yueLabel. textAlignment = NSTextAlignmentCenter;
    self.yueLabel.font = [UIFont fontWithName:KPFType size:18];
    self.yueLabel.textColor = GMWhiteColor;
    [self.headImg addSubview:self.yueLabel];
    self.yueLabel.sd_layout.topSpaceToView(titleLabel, 38)
    .centerXEqualToView(self.headImg)
    .widthIs(150).heightIs(18);
    
    UIView *shadowView = [[UIView alloc]init];
    shadowView.backgroundColor = GMWhiteColor;
    shadowView.layer.shadowColor = [UIColor blackColor].CGColor;
    shadowView.layer.shadowOffset = CGSizeMake(0, 1);
    shadowView.layer.shadowOpacity = 0.3;
    shadowView.layer.shadowRadius = 5.0;
    shadowView.layer.cornerRadius = 20.0;
    //    shadowView.layer.masksToBounds = NO;    //剪裁
    [self.view addSubview:shadowView];
    shadowView.sd_layout.topSpaceToView(self.headImg, 20)
    .leftSpaceToView(self.view, 15)
    .rightSpaceToView(self.view, 15)
    .widthIs(KScreenW-30).heightIs(200);
    
    UILabel *xufeiLabel = [[UILabel alloc]init];
    [self.view addSubview:xufeiLabel];
    xufeiLabel.sd_layout.topSpaceToView(shadowView, 25)
    .leftSpaceToView(self.view, 18)
    .widthIs(125).heightIs(15);
    NSMutableAttributedString *xufeString = [[NSMutableAttributedString alloc] initWithString:@"" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 15],NSForegroundColorAttributeName: [UIColor colorWithRed:69/255.0 green:69/255.0 blue:69/255.0 alpha:1.0]}];
    xufeiLabel.attributedText = xufeString;
    //支付
    UIButton *payButton = [UIButton buttonWithType:UIButtonTypeCustom];
    payButton.backgroundColor = RGB(23, 153, 167);
    payButton.layer.cornerRadius = 22.5;
    [payButton addTarget:self action:@selector(payButton:) forControlEvents:UIControlEventTouchUpInside];
    [payButton setTitleColor:GMWhiteColor forState:UIControlStateNormal];
    [payButton setTitle:@"确认支付" forState:UIControlStateNormal];
    [self.view addSubview:payButton];
    payButton.sd_layout.bottomSpaceToView(self.view, 39)
    .leftSpaceToView(self.view, 15)
    .rightSpaceToView(self.view, 15)
    .heightIs(45);
    
    self.flowLayout = [[UICollectionViewFlowLayout alloc]init];
    self.flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    
    self.collectionV = [[UICollectionView alloc]initWithFrame:CGRectMake(15, 37, shadowView.frame.size.width-30 , 200-37) collectionViewLayout:self.flowLayout];
    self.collectionV.backgroundColor = GMWhiteColor;
    self.collectionV.delegate = self;
    self.collectionV.dataSource = self;
    [self.collectionV registerClass:[AccountCollectionViewCell class] forCellWithReuseIdentifier:@"collection"];
    [shadowView addSubview:self.collectionV];
    
    
}
#pragma mark -- collectionView delegate , datasource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 6;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((collectionView.frame.size.width-30)/3 ,58);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    AccountCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"collection" forIndexPath:indexPath];
    
    cell.layer.cornerRadius = 5.0f;
    cell.layer.borderWidth = 0.5f;
    cell.layer.borderColor = RGB(21, 152, 164).CGColor;
    cell.layer.masksToBounds = NO;
    
    NSArray *arr = @[@"6育点",@"40育点",@"108育点",@"208育点",@"308育点",@"418育点"];
    cell.priceLabel.text = [arr objectAtIndex:indexPath.row];
    
    NSArray *arrNum = @[@"6",@"40",@"108",@"208",@"308",@"418"];
    cell.numPriceLabel.text = [arrNum objectAtIndex:indexPath.row];
    return cell;
}



-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    AccountCollectionViewCell *cell = (AccountCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    cell.backgroundColor = RGB(21, 153, 164);
    cell.priceLabel.textColor = GMWhiteColor;
    
//    self.priceString = cell.priceLabel.text;
    
//    NSLog(@"xxxxxxx%@",cell.priceLabel.text);
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"price"] = cell.numPriceLabel.text;
    self.productIDPrice = [NSMutableArray array];
        [GMAfnTools PostHttpDataWithUrlStr:KPayProductURL Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
            
            //价格
            self.priceString = [responseObject objectForKey:@"pay_price"];
            //产品ID
            self.productString = [responseObject objectForKey:@"product_id"];
            
            
        } FailureBlock:^(id  _Nonnull error) {
            
        }];

}

-(void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    AccountCollectionViewCell *cell = (AccountCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    cell.backgroundColor = GMWhiteColor;
    cell.priceLabel.textColor = RGB(21, 153, 164);
}


- (void)declaration {
    
    UILabel *xufeiLabel = [[UILabel alloc]init];
    [self.view addSubview:xufeiLabel];
    
}

- (void)backBtn {
    [self.navigationController popViewControllerAnimated:YES];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
#pragma mark -- 提示框
- (void)showError:(NSString *)errorMsg {
    // 1.弹框提醒
    // 初始化对话框
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:errorMsg preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:nil]];
    // 弹出对话框
    [self presentViewController:alert animated:true completion:nil];
    
    [self performSelector:@selector(dimisssAlert:) withObject:alert afterDelay:1];
    
}

- (void) dimisssAlert:(UIAlertController *)alert
{
    if (alert) {
        [alert dismissViewControllerAnimated:YES completion:nil];
    }
    
}
@end
