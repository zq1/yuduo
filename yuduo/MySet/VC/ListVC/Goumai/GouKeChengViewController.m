//
//  GouKeChengViewController.m
//  yuduo
//
//  Created by Mac on 2019/8/14.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "GouKeChengViewController.h"
#import "TapSliderScrollView.h"
#import "SimpleTapSliderScrollView.h"
#import "GouMaiTableViewCell.h"
#import "SouCangCourseModel.h"
#import "CoureDetailViewController.h"
@interface GouKeChengViewController ()
<UITableViewDelegate,UITableViewDataSource>
PropertyStrong(UITableView, myZXTableView);
PropertyStrong(NSMutableArray, courseCollectArray);
@end

@implementation GouKeChengViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getCollectData];
    self.myZXTableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.myZXTableView.delegate = self;
    self.myZXTableView.dataSource = self;
    self.myZXTableView.backgroundColor = RGB(244, 243, 244);
    [self.view addSubview:self.myZXTableView];
    self.myZXTableView.sd_layout.topSpaceToView(self.view, 0)
    .leftSpaceToView(self.view, 0)
    .widthIs(KScreenW).heightIs(KScreenH-90-MStatusBarHeight);
    
    // Do any additional setup after loading the view.
}

- (void)getCollectData {
    self.courseCollectArray = [NSMutableArray array];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *user_id = [defaults objectForKey:@"user_id"];
    NSString *token = [defaults objectForKey:@"token"];
    NSMutableDictionary *courseDic = [NSMutableDictionary dictionary];
    courseDic[@"user_id"] = user_id;
    courseDic[@"token"] = token;
    courseDic[@"type"] = @"1";
    [GMAfnTools PostHttpDataWithUrlStr:KGouMaiListURL Dic:courseDic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"打印课程购买列表===%@",responseObject);
        for (NSMutableDictionary *dic in responseObject) {
            SouCangCourseModel *soucangCM = [[SouCangCourseModel alloc]init];
            [soucangCM mj_setKeyValues:dic];
            [self.courseCollectArray addObject:soucangCM];
        }
        [self.myZXTableView reloadData];
    } FailureBlock:^(id  _Nonnull error) {
        NSLog(@"打印购买列表错误===%@",error);
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.courseCollectArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 170;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"cellID";
    GouMaiTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[GouMaiTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.backgroundColor = RGB(244, 243, 244);
    SouCangCourseModel *model = [self.courseCollectArray objectAtIndex:indexPath.row];
    cell.titleLabel.text = model.name;
    cell.headImgView.sd_cornerRadius = [NSNumber numberWithInteger:10];
//    cell.peoplesLabel.text = model.course_vrows;
    cell.xPriceLabel.text = model.pay_price;
    cell.priceLabel.text = model.under_price;
    [cell.headImgView sd_setImageWithURL:[NSURL URLWithString:model.cover]];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    CoureDetailViewController *detailVC = [[CoureDetailViewController alloc]init];
    SouCangCourseModel *model = [self.courseCollectArray objectAtIndex:indexPath.row];
    
    //需要上传courseID
    detailVC.courseDetailIDString = @"8";
    [self.navigationController pushViewController:detailVC animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
