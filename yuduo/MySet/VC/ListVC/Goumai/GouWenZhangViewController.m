//
//  GouWenZhangViewController.m
//  yuduo
//
//  Created by Mac on 2019/8/14.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "GouWenZhangViewController.h"
#import "TapSliderScrollView.h"
#import "SimpleTapSliderScrollView.h"
#import "GouMaiTableViewCell.h"
#import "WenZhangTableViewCell.h"
#import "BuyModel.h"
@interface GouWenZhangViewController ()
<UITableViewDelegate,UITableViewDataSource>
PropertyStrong(UITableView, myZXTableView);
PropertyStrong(NSMutableArray, articleCollectArray);
@end

@implementation GouWenZhangViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.myZXTableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.myZXTableView.delegate = self;
    self.myZXTableView.dataSource = self;
    self.myZXTableView.backgroundColor = RGB(244, 243, 244);
    [self.view addSubview:self.myZXTableView];
    self.myZXTableView.sd_layout.topSpaceToView(self.view, 0)
    .leftSpaceToView(self.view, 0)
    .widthIs(KScreenW).heightIs(KScreenH-90-MStatusBarHeight);
    
    [self getZLBuyList];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.articleCollectArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 130;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"cellID";
    WenZhangTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[WenZhangTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.backgroundColor = RGB(244, 243, 244);
    
    BuyModel *mol = [self.articleCollectArray objectAtIndex:indexPath.row];
    cell.titleLabel.text = mol.name;
    cell.priceLabel.text = mol.pay_price;
    [cell.headImgView sd_setImageWithURL:[NSURL URLWithString:mol.cover]];
    return cell;
}

- (void)getZLBuyList {
    self.articleCollectArray = [NSMutableArray array];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *user_id = [defaults objectForKey:@"user_id"];
    NSString *token = [defaults objectForKey:@"token"];
    
    NSMutableDictionary *courseDic = [NSMutableDictionary dictionary];
    
    courseDic[@"user_id"] = user_id;
    courseDic[@"token"] = token;
    courseDic[@"type"] = @"3";
    [GMAfnTools PostHttpDataWithUrlStr:KGouMaiListURL Dic:courseDic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"打印文章购买列表===%@",responseObject);
        for (NSMutableDictionary *dic in responseObject) {
            BuyModel *articleModel = [[BuyModel alloc]init];
            [articleModel mj_setKeyValues:dic];
            [self.articleCollectArray addObject:articleModel];
        }
        [self.myZXTableView reloadData];
    } FailureBlock:^(id  _Nonnull error) {
        NSLog(@"打印购买列表错误===%@",error);
    }];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
