//
//  BuyModel.h
//  yuduo
//
//  Created by Mac on 2019/9/26.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BuyModel : BaseModel
PropertyStrong(NSString, name);
PropertyStrong(NSString, pay_price);
PropertyStrong(NSString, under_price);
PropertyStrong(NSString, cover);
@end

NS_ASSUME_NONNULL_END
