//
//  GouZhuanLanViewController.m
//  yuduo
//
//  Created by Mac on 2019/8/14.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "GouZhuanLanViewController.h"
#import "TapSliderScrollView.h"
#import "SimpleTapSliderScrollView.h"
#import "GouMaiTableViewCell.h"
#import "ZhuanLanTableViewCell.h"
#import "SouCangCourseModel.h"
@interface GouZhuanLanViewController ()
<UITableViewDelegate,UITableViewDataSource>
PropertyStrong(UITableView, myZXTableView);
PropertyStrong(NSMutableArray, courseCollectArray);
@end

@implementation GouZhuanLanViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getZLBuyList];
    self.myZXTableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.myZXTableView.delegate = self;
    self.myZXTableView.dataSource = self;
    self.myZXTableView.backgroundColor = RGB(244, 243, 244);
    [self.view addSubview:self.myZXTableView];
    self.myZXTableView.sd_layout.topSpaceToView(self.view, 0)
    .leftSpaceToView(self.view, 0)
    .widthIs(KScreenW).heightIs(KScreenH-90-MStatusBarHeight);
    
    // Do any additional setup after loading the view.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.courseCollectArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 130;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"cellID";
    ZhuanLanTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[ZhuanLanTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.backgroundColor = RGB(244, 243, 244);
    SouCangCourseModel *mol = [self.courseCollectArray objectAtIndex:indexPath.row];
    [cell.headImg sd_setImageWithURL:[NSURL URLWithString:mol.cover]];
    cell.headImg.sd_cornerRadius = [NSNumber numberWithInteger:10];
    cell.titleLable.text = mol.name;
    cell.priceLabel.text = mol.pay_price;
    cell.xPriceLabel.text = mol.under_price;
    return cell;
}


- (void)getZLBuyList {
    self.courseCollectArray = [NSMutableArray array];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *user_id = [defaults objectForKey:@"user_id"];
    NSString *token = [defaults objectForKey:@"token"];
    NSMutableDictionary *courseDic = [NSMutableDictionary dictionary];
    courseDic[@"user_id"] = user_id;
    courseDic[@"token"] = token;
    courseDic[@"type"] = @"2";
    [GMAfnTools PostHttpDataWithUrlStr:KGouMaiListURL Dic:courseDic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"打印专栏购买列表===%@",responseObject);
        for (NSMutableDictionary *dic in responseObject) {
            SouCangCourseModel *soucangCM = [[SouCangCourseModel alloc]init];
            [soucangCM mj_setKeyValues:dic];
            [self.courseCollectArray addObject:soucangCM];
        }
        [self.myZXTableView reloadData];
    } FailureBlock:^(id  _Nonnull error) {
        NSLog(@"打印购买列表错误===%@",error);
    }];
}

@end
