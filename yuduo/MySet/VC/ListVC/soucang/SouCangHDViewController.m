//
//  SouCangHDViewController.m
//  yuduo
//
//  Created by Mac on 2019/8/14.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "SouCangHDViewController.h"
#import "TapSliderScrollView.h"
#import "SimpleTapSliderScrollView.h"
#import "GouMaiTableViewCell.h"
#import "HomeLiveTableViewCell.h"
#import "ActiveCenterTableViewCell.h"
@interface SouCangHDViewController ()
<UITableViewDelegate,UITableViewDataSource>
PropertyStrong(UITableView, myZXTableView);
PropertyStrong(NSMutableArray, activeArray);

@end

@implementation SouCangHDViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.myZXTableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.myZXTableView.delegate = self;
    self.myZXTableView.dataSource = self;
    self.myZXTableView.backgroundColor = RGB(244, 243, 244);
    [self.view addSubview:self.myZXTableView];
    self.myZXTableView.sd_layout.topSpaceToView(self.view, 10)
    .leftSpaceToView(self.view, 0)
    .rightSpaceToView(self.view, 0)
    .widthIs(KScreenW).heightIs(KScreenH-90-MStatusBarHeight);
    [self getZLBuyList];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.activeArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 286;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"cellID";
    ActiveCenterTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[ActiveCenterTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
//    cell.backgroundColor = RGB(244, 243, 244);
//    BuyModel *mol = [self.buyActivetArray objectAtIndex:indexPath.row];
//    cell.titleLabel.text = mol.name;
//    cell.priceLabel.text = mol.pay_price;
//    cell.vipLabel.hidden = YES;
//    [cell.headImg sd_setImageWithURL:[NSURL URLWithString:mol.cover]];
    
    
    return cell;
}

- (void)getZLBuyList {
    self.activeArray = [NSMutableArray array];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *user_id = [defaults objectForKey:@"user_id"];
    NSString *token = [defaults objectForKey:@"token"];
    NSMutableDictionary *courseDic = [NSMutableDictionary dictionary];
    courseDic[@"user_id"] = user_id;
    courseDic[@"token"] = token;
    courseDic[@"type"] = @"4";
    [GMAfnTools PostHttpDataWithUrlStr:KCollectList Dic:courseDic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"------收藏活动列表--%@",responseObject);
//        for (NSMutableDictionary *dic in responseObject) {
//
//            BuyModel *activewModel = [[BuyModel alloc]init];
//            [activewModel mj_setKeyValues:dic];
//            [self.buyActivetArray addObject:activewModel];
//
//        }
        [self.myZXTableView reloadData];
    } FailureBlock:^(id  _Nonnull error) {

    }];
}

@end
