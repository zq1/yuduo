//
//  SouCangCourseModel.h
//  yuduo
//
//  Created by Mac on 2019/9/19.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface SouCangCourseModel : BaseModel
PropertyStrong(NSString, course_name);
PropertyStrong(NSString, brief);
PropertyStrong(NSString, course_list_cover);
PropertyStrong(NSString, class_hour);
PropertyStrong(NSString, course_vrows);
PropertyStrong(NSString, user_id);
PropertyStrong(NSString, type);
PropertyStrong(NSString, course_id);
PropertyStrong(NSString, collection_id);

PropertyStrong(NSString, course_under_price);
PropertyStrong(NSString, course_price);

PropertyStrong(NSString, cancel_time);
PropertyStrong(NSString, create_time);
PropertyStrong(NSString, status);
PropertyStrong(NSString, cover);

//我的购买 - 课程
PropertyStrong(NSString, name);
PropertyStrong(NSString, pay_price);
PropertyStrong(NSString, under_price);


@end

NS_ASSUME_NONNULL_END
