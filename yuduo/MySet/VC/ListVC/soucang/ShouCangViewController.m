//
//  ShouCangViewController.m
//  yuduo
//
//  Created by Mac on 2019/8/13.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "ShouCangViewController.h"
#import "SouCangKCViewController.h"
#import "SouCangZLViewController.h"
#import "SouCangHDViewController.h"
#import "SouCangWZViewController.h"
#import "TapSliderScrollView.h"
#import "SimpleTapSliderScrollView.h"

#import "ShouCangViewController.h"
@interface ShouCangViewController ()
<SliderLineViewDelegate,SimpleSliderLineViewDelegate,UITableViewDelegate,UITableViewDataSource, UIScrollViewDelegate>
@property (nonatomic ,assign) BOOL selectSort;

@property (nonatomic, strong) NSArray *dataArr;

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) NSMutableArray *myChildViewControllers;
@end

@implementation ShouCangViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = GMWhiteColor;
    [self setNavigation];
    [self scrollZX];
    
    //    [self tab];
    // Do any additional setup after loading the view.
}
- (void)scrollZX {
    
    SouCangKCViewController *vc1 = [[SouCangKCViewController alloc]init];
    SouCangZLViewController *vc2 = [[SouCangZLViewController alloc]init];
    SouCangHDViewController *vc3 = [[SouCangHDViewController alloc]init];
    SouCangWZViewController *vc4 = [[SouCangWZViewController alloc]init];
    SimpleTapSliderScrollView *simpleview = [[SimpleTapSliderScrollView alloc]initWithFrame:CGRectMake(0, 45+MStatusBarHeight, KScreenW, KScreenH)];
    simpleview.delegate = self;
    simpleview.sliderViewColor = RGB(21, 152, 164);
    simpleview.titileColror = RGB(153, 153, 153);
    simpleview.selectedColor = RGB(21, 152, 164);
    [simpleview createView:@[@"课程",@"专栏",@"直播",@"文章"] andViewArr:@[vc1,vc2,vc3,vc4] andRootVc:self];
    [self.view addSubview:simpleview];
    [simpleview sliderToViewIndex:0];
}
#pragma mark sliderDelegate -- OC
-(void)sliderViewAndReloadData:(NSInteger)index
{
    NSLog(@"刷新数据啦%ld",index);
    if (index == 3) {
        NSLog(@"创建tableview");
    }
}


- (void)tab {
    UITableView *table = [[UITableView alloc]initWithFrame:CGRectMake(0, 100, 300, 400) style:UITableViewStylePlain];
    table.backgroundColor = GMBlueColor;
    table.delegate =self;
    table.dataSource= self;
    [self.view addSubview:table];
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}
#pragma mark--返回行高
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40;
}
#pragma mark--返回行数
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSArray *arr = @[@"最新课程",@"最热课程",@"免费课程",@"VIP限免"];
    static NSString *key0=@"cell0";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:key0];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:key0];
        
    }
    //    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    return cell;
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (void)setNavigation {
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:[UIImage imageNamed:@"back-icon"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButton) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButton];
    backButton.sd_layout.topSpaceToView(self.view,MStatusBarHeight+12)
    .leftSpaceToView(self.view, 15)
    .widthIs(11).heightIs(19);
    UIButton *backButtonView = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 80, MStatusBarHeight+44)];
    backButtonView.backgroundColor = RGBA(15, 15, 15, 0);
    [backButtonView addTarget:self action:@selector(backButton) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButtonView];
    
    UILabel *titleLabel = [[UILabel alloc]init];
    [self.view addSubview:titleLabel];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"我的收藏" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 18],NSForegroundColorAttributeName: [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1.0]}];
    titleLabel.attributedText = string;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.sd_layout.topSpaceToView(self.view, MStatusBarHeight+13)
    .centerXEqualToView(self.view)
    .heightIs(18).widthIs(100);
}
- (void)backButton {
    [self.navigationController popViewControllerAnimated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
