//
//  SouCangActiveModel.h
//  yuduo
//
//  Created by Mac on 2019/9/28.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface SouCangActiveModel : BaseModel
PropertyStrong(NSString, activity_name);
PropertyStrong(NSString, activity_start_date);
PropertyStrong(NSString, activity_price);
PropertyStrong(NSString, user_id);
PropertyStrong(NSString, activity_cover);
PropertyStrong(NSString, collection_id);
PropertyStrong(NSString, activity_type);
PropertyStrong(NSString, activity_end_date);
PropertyStrong(NSString, activity_stop_date);
PropertyStrong(NSString, create_time);
PropertyStrong(NSString, cancel_time);
PropertyStrong(NSString, status);

@end

NS_ASSUME_NONNULL_END
