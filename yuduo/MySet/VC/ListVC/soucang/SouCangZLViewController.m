//
//  SouCangZLViewController.m
//  yuduo
//
//  Created by Mac on 2019/8/14.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "SouCangZLViewController.h"
#import "TapSliderScrollView.h"
#import "SimpleTapSliderScrollView.h"
#import "GouMaiTableViewCell.h"
#import "ZhuanLanTableViewCell.h"
#import "SouCangCourseModel.h"
#import "HomeColumnDetailViewController.h"
@interface SouCangZLViewController ()
<UITableViewDelegate,UITableViewDataSource>
PropertyStrong(UITableView, myZXTableView);
PropertyStrong(NSMutableArray, courseCollectArray);
PropertyStrong(UILabel, xPriceLabel);
PropertyStrong(UILabel, priceLabel);
@end

@implementation SouCangZLViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getCollectData];
    self.myZXTableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.myZXTableView.delegate = self;
    self.myZXTableView.dataSource = self;
    self.myZXTableView.backgroundColor = RGB(244, 243, 244);
    [self.view addSubview:self.myZXTableView];
    self.myZXTableView.sd_layout.topSpaceToView(self.view, 0)
    .leftSpaceToView(self.view, 0)
    .widthIs(KScreenW).heightIs(KScreenH-90-MStatusBarHeight);
    
    // Do any additional setup after loading the view.
}

- (void)getCollectData {
    self.courseCollectArray = [NSMutableArray array];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *user_id = [defaults objectForKey:@"user_id"];
    NSString *token = [defaults objectForKey:@"token"];
    NSMutableDictionary *courseDic = [NSMutableDictionary dictionary];
    courseDic[@"user_id"] = user_id;
    courseDic[@"token"] = token;
    courseDic[@"type"] = @"2";
    [GMAfnTools PostHttpDataWithUrlStr:KCollectList Dic:courseDic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"打印课程收藏列表===%@",responseObject);
        for (NSMutableDictionary *dic in responseObject) {
            SouCangCourseModel *soucangCM = [[SouCangCourseModel alloc]init];
            [soucangCM mj_setKeyValues:dic];
            [self.courseCollectArray addObject:soucangCM];
        }
        [self.myZXTableView reloadData];
    } FailureBlock:^(id  _Nonnull error) {
        NSLog(@"打印课程收藏列表错误===%@",error);
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.courseCollectArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 130;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"cellID";
    ZhuanLanTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[ZhuanLanTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.backgroundColor = RGB(244, 243, 244);
    SouCangCourseModel *model = [self.courseCollectArray objectAtIndex:indexPath.row];
    cell.titleLable.text = model.course_name;
    [cell.headImg sd_setImageWithURL:[NSURL URLWithString:model.course_list_cover]];
    cell.headImg.sd_cornerRadius = [NSNumber numberWithInt:10];
    cell.headImg.sd_cornerRadius = [NSNumber numberWithInteger:10];
    cell.peopleLable.text = model.course_vrows;
    cell.priceLabel.text = model.course_price;
    cell.xPriceLabel.text = model.course_under_price;
    
   
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    HomeColumnDetailViewController *detailVC = [[HomeColumnDetailViewController alloc]init];
    SouCangCourseModel *model = [self.courseCollectArray objectAtIndex:indexPath.row];
    
    //需要上传courseID
    detailVC.courseDetailIDString = model.collection_id;
    [self.navigationController pushViewController:detailVC animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
