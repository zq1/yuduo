//
//  YueTableViewCell.m
//  yuduo
//
//  Created by Mac on 2019/8/14.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "YueTableViewCell.h"

@implementation YueTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier]) {
        
        [self createLayout];
    }
    return self;
}

- (void)createLayout {
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = GMWhiteColor;
    self.bgView.layer.cornerRadius = 10;
    [self.contentView addSubview:self.bgView];
    self.bgView.sd_layout.topSpaceToView(self.contentView, 10)
    .leftSpaceToView(self.contentView, 15)
    .rightSpaceToView(self.contentView, 15)
    .widthIs(KScreenW-30).heightIs(90);
    
//    self.headImg = [[UIImageView alloc]init];
//    self.headImg.backgroundColor = GMlightGrayColor;
//    self.headImg.layer.cornerRadius = 30;
//    [self.bgView addSubview:self.headImg];
//    self.headImg.sd_layout.topSpaceToView(self.bgView, 15)
//    .leftSpaceToView(self.bgView, 15)
//    .widthIs(60).heightEqualToWidth();
    
    self.titleLabel = [[UILabel alloc]init];
    self.titleLabel.text = @"成功购买<热门课程>";
    self.titleLabel.font = [UIFont fontWithName:KPFType size:14];
    self.titleLabel.textColor = RGB(69, 69, 69);
    [self.bgView addSubview:self.titleLabel];
    self.titleLabel.sd_layout.topSpaceToView(self.bgView, 20)
    .leftSpaceToView(self.bgView, 15)
    .widthIs(140).heightIs(14);
    
    
//
//    self.timeImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ico_time"]];
//    [self.bgView addSubview:self.timeImg];
//    self.timeImg.sd_layout.bottomSpaceToView(self.bgView, 25)
//    .leftSpaceToView(self.headImg, 17)
//    .widthIs(13).heightIs(13);
    
    self.timeLabel = [[UILabel alloc]init];
    self.timeLabel.text = @"2019-05-05 15:00:36";
    self.timeLabel.textColor = RGB(153, 153, 153);
    self.timeLabel.font = [UIFont fontWithName:KPFType size:12];
    [self.bgView addSubview:self.timeLabel];
    self.timeLabel.sd_layout.bottomSpaceToView(self.bgView, 22)
    .leftSpaceToView(self.bgView, 17)
    .widthIs(150).heightIs(9);
    
    self.priceLabel = [[UILabel alloc]init];
    self.priceLabel.text = @"¥49.99";
    self.priceLabel.textColor = RGB(228, 102, 67);
    self.priceLabel.font = [UIFont fontWithName:KPFType size:15];
    [self.bgView addSubview:self.priceLabel];
    self.priceLabel.sd_layout.topSpaceToView(self.bgView, 35)
    .rightSpaceToView(self.bgView, 20)
    .widthIs(55).heightIs(12);
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
