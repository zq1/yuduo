//
//  OpenViperCollectionViewCell.m
//  yuduo
//
//  Created by Mac on 2019/9/19.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "OpenViperCollectionViewCell.h"

@implementation OpenViperCollectionViewCell
-(instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        self.vipLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 20, 105, 14)];
        self.vipLabel.textAlignment = NSTextAlignmentCenter;
        self.vipLabel.textColor = RGB(69, 69, 69);
        self.vipLabel.font = [UIFont fontWithName:KPFType size:14];
        [self addSubview:self.vipLabel];
        
        
        self.priceLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 53, 105, 19)];
        self.priceLabel.textAlignment = NSTextAlignmentCenter;
        self.priceLabel.font = [UIFont fontWithName:KPFType size:20];
        self.priceLabel.textColor = RGB(228, 102, 67);
        [self addSubview:self.priceLabel];

        self.numPriceLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
        [self.priceLabel addSubview:self.numPriceLabel];
    }
    
    return self;
}
@end
