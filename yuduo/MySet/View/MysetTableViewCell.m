//
//  MysetTableViewCell.m
//  yuduo
//
//  Created by Mac on 2019/8/2.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "MysetTableViewCell.h"

@implementation MysetTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        [self layoutSubviewsMyset];
    }
    return self;
}

- (void)layoutSubviewsMyset {
    self.iconImg = [[UIImageView alloc]init];
    [self.contentView addSubview:self.iconImg];
    self.iconImg.sd_layout.topSpaceToView(self.contentView, 14)
    .leftSpaceToView(self.contentView, 14)
    .widthIs(18).heightIs(18);
    
    self.titleLabel = [[UILabel alloc]init];
    [self.contentView addSubview:self.titleLabel];
    self.titleLabel.sd_layout.topSpaceToView(self.contentView, 17)
    .leftSpaceToView(self.iconImg, 12)
    .widthIs(150).heightIs(15);
    
    self.moreImgView = [[UIImageView alloc]init];
    self.moreImgView.image = [UIImage imageNamed:@"ico_next"];
    [self.contentView addSubview:self.moreImgView];
    self.moreImgView.sd_layout.topSpaceToView(self.contentView, 15)
    .rightSpaceToView(self.contentView, 15)
    .widthIs(8).heightIs(14);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
