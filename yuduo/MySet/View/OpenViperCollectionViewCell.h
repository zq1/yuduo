//
//  OpenViperCollectionViewCell.h
//  yuduo
//
//  Created by Mac on 2019/9/19.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface OpenViperCollectionViewCell : UICollectionViewCell
//标题
PropertyStrong(UILabel, vipLabel);

PropertyStrong(UILabel, priceLabel);
//价格
PropertyStrong(UILabel, numPriceLabel);
//续费
PropertyStrong(UILabel, continueLabel);
@end

NS_ASSUME_NONNULL_END
