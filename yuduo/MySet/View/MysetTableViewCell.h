//
//  MysetTableViewCell.h
//  yuduo
//
//  Created by Mac on 2019/8/2.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MysetTableViewCell : UITableViewCell
PropertyStrong(UIImageView, iconImg);
PropertyStrong(UILabel, titleLabel;);
PropertyStrong(UIImageView, moreImgView);
@end

NS_ASSUME_NONNULL_END
