//
//  MysetBgView.h
//  yuduo
//
//  Created by Mac on 2019/8/2.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MysetBgView : UIView
PropertyStrong(UIView, viperLoginView);//登录后显示的界面
PropertyStrong(UILabel, myNameLabel); //我的昵称
PropertyStrong(UIImageView, nameLogo);//名字后面的logo
PropertyStrong(UILabel, validityLabel); // 会员有效期
PropertyStrong(UIButton, openViper); //开通会员
@property (nonatomic ,copy) void(^openButtonBlock)(NSInteger indexVip);

PropertyStrong(UIImageView, headBgImgView);
PropertyStrong(UILabel, myLabel);

PropertyStrong(UIButton, setButton);
@property (nonatomic ,copy) void(^setButtonBlock)(NSInteger indexSet);

PropertyStrong(UIImageView, headImg);
PropertyStrong(UIImageView, headTapImage);
PropertyStrong(UIView, loginRegisterBgView);
PropertyStrong(UIButton, loginButton);

//PropertyStrong(UIButton, headButton);//头像点击按钮
@property (nonatomic ,copy) void(^headButtonBlock) (NSInteger indexButton);

@property (nonatomic ,copy) void(^loginButtonBlock)(NSInteger indexLogin);
PropertyStrong(UIButton, registerButton);
@property (nonatomic ,copy) void(^registerbButtonBlock)(NSInteger indexRegisButton);



@property (nonatomic , copy) void(^titleBlock)(UIButton *btnTitle);
@end

NS_ASSUME_NONNULL_END
