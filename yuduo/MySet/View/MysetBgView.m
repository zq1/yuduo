//
//  MysetBgView.m
//  yuduo
//
//  Created by Mac on 2019/8/2.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "MysetBgView.h"

@implementation MysetBgView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self createView];
        self.backgroundColor = RGB(21, 151, 164);
    }
    return self;
}

- (void)createView {
  
    self.headBgImgView = [[UIImageView alloc]init];
    self.headBgImgView.image = [UIImage imageNamed:@"bg"];
    self.headBgImgView.userInteractionEnabled = YES;
//    self.headBgImgView.backgroundColor = RGB(21, 151, 164);
    [self addSubview:self.headBgImgView];
    //判断状态栏高度
//    CGRect rectOfStatusbar = [[UIApplication sharedApplication] statusBarFrame];
//    if (rectOfStatusbar.size.height == 20) {
        self.headBgImgView.sd_layout.topSpaceToView(self, 0)
        .leftSpaceToView(self, 0)
        .rightSpaceToView(self, 0)
        .heightIs(258+MStatusBarHeight).widthIs(KScreenW);
//    }
//    else {
//        self.headBgImgView.sd_layout.topSpaceToView(self, 24)
//        .leftSpaceToView(self, 0)
//        .rightSpaceToView(self, 0)
//        .heightIs(278+24).widthIs(KScreenW);

//    }
//    NSLog(@"statusbar height: %f", rectOfStatusbar.size.height);   // 高度
//    NSLog(@"滚到顶部");

    self.myLabel = [[UILabel alloc]init];
    self.myLabel.textAlignment = NSTextAlignmentCenter;
    [self.headBgImgView addSubview:self.myLabel];
    NSMutableAttributedString *myString = [[NSMutableAttributedString alloc] initWithString:@"我的" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 18],NSForegroundColorAttributeName: [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0]}];
    self.myLabel.attributedText = myString;
    self.myLabel.sd_layout.topSpaceToView(self.headBgImgView, 14+MStatusBarHeight)
        .centerXEqualToView(self.headBgImgView)
    .widthIs(40).heightIs(17);
    
    self.setButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.setButton setImage:[UIImage imageNamed:@"set"] forState:UIControlStateNormal];
    [self.setButton addTarget:self action:@selector(setButtonOne:) forControlEvents:UIControlEventTouchUpInside];
    [self.headBgImgView addSubview:self.setButton];
    self.setButton.sd_layout.topSpaceToView(self.headBgImgView, 13+MStatusBarHeight)
    .rightSpaceToView(self.headBgImgView, 15)
    .widthIs(20).heightIs(20);
    
//    self.headImg = [[UIImageView alloc]init];
//    self.headImg.userInteractionEnabled = YES;
//    self.headImg.backgroundColor = GMWhiteColor;
//
//    self.headImg.layer.cornerRadius = 36;
//    [self.headBgImgView addSubview:self.headImg];
//    self.headImg.sd_layout.topSpaceToView(self.headBgImgView, 76)
//    .leftSpaceToView(self.headBgImgView, 15)
//    .widthIs(72).heightIs(72);
//
    self.headTapImage = [[UIImageView alloc] init];
    self.headTapImage.contentMode = UIViewContentModeScaleAspectFill;
    self.headTapImage.image = [UIImage imageNamed:@"viper"];
    self.headTapImage.sd_cornerRadius = [NSNumber numberWithInteger:36];
    self.headTapImage.clipsToBounds = YES;
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(myDetailClick:)];
    self.headTapImage.userInteractionEnabled = YES;
    [self.headTapImage addGestureRecognizer:tap];
    [self.headBgImgView addSubview:self.headTapImage];
    self.headTapImage.sd_layout.topSpaceToView(self.headBgImgView, MStatusBarHeight+56)
    .leftSpaceToView(self.headBgImgView, 15)
    .widthIs(72).heightIs(72);
    
    
    self.loginRegisterBgView = [[UIView alloc]init];
    
    [self.headBgImgView addSubview:self.loginRegisterBgView];
  self.loginRegisterBgView.sd_layout.topSpaceToView(self.headBgImgView, 106)
    .rightSpaceToView(self.headBgImgView, 16)
    .widthIs(100).heightIs(17);
    
    self.loginButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.loginButton setTitle:@"登录" forState:UIControlStateNormal];
    [self.loginButton addTarget:self action:@selector(setLoginBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self.loginButton setTintColor:GMWhiteColor];
    [self.loginRegisterBgView addSubview:self.loginButton];
    self.loginButton.sd_layout.topSpaceToView(self.loginRegisterBgView, 0)
    .leftSpaceToView(self.loginRegisterBgView, 0)
  .widthIs(40).heightIs(self.loginRegisterBgView.frame.size.height);
    
    self.registerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.registerButton setTitle:@"/ 注册" forState:UIControlStateNormal];
    [self.registerButton addTarget:self action:@selector(setRegisBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self.registerButton setTintColor:GMWhiteColor];
    [self.loginRegisterBgView addSubview:self.registerButton];
    self.registerButton.sd_layout.topSpaceToView(self.loginRegisterBgView, 0)
    .rightSpaceToView(self.loginRegisterBgView, 10)
    .widthIs(50).heightIs(17);
    
    self.viperLoginView = [[UIView alloc]init];
//    self.viperLoginView.backgroundColor = GMlightGrayColor;
    [self.headBgImgView addSubview:self.viperLoginView];
    self.viperLoginView.sd_layout.topSpaceToView(self.headBgImgView, MStatusBarHeight+72)
    .leftSpaceToView(self.headTapImage, 16)
    .widthIs(KScreenW-87).heightIs(40);
    
    self.myNameLabel = [[UILabel alloc]init];
    self.myNameLabel.text = @"我的昵称";
    self.myNameLabel.textColor = GMWhiteColor;
    [self.viperLoginView addSubview:self.myNameLabel];
    self.myNameLabel.sd_layout.topSpaceToView(self.viperLoginView, 0)
    .leftSpaceToView(self.viperLoginView, 0)
    .heightIs(17).rightSpaceToView(self.viperLoginView, 0);
    
    self.validityLabel = [[UILabel alloc]init];
    self.validityLabel.text = @"有效期至 : 2020-05-05";
    self.validityLabel.textColor = GMWhiteColor;
    self.validityLabel.font = [UIFont fontWithName:@"PingFang SC" size: 12];
    [self.viperLoginView addSubview:self.validityLabel];
    self.validityLabel.sd_layout.bottomSpaceToView(self.viperLoginView, 0)
    .leftSpaceToView(self.viperLoginView, 0)
    .widthIs(250).heightIs(12);
    
    self.openViper = [UIButton buttonWithType:UIButtonTypeCustom];
//    self.openViper.backgroundColor = GMRedColor;
    [self.openViper setTitle:@"开通会员" forState:UIControlStateNormal];
    [self.openViper addTarget:self action:@selector(openClick:) forControlEvents:UIControlEventTouchUpInside];

    [self.openViper setTitle:@"已是会员" forState:(UIControlStateDisabled)];
//    self.openViper
    self.openViper.titleLabel.font = kFont(12);
    [self.openViper setTitleColor:GMWhiteColor forState:UIControlStateNormal];
    self.openViper.backgroundColor = RGB(234,56, 42);
    self.openViper.layer.borderWidth = 0.7;
    self.openViper.layer.cornerRadius = 5;
    self.openViper.layer.borderColor = RGB(21, 152, 164).CGColor;
    [self.viperLoginView addSubview:self.openViper];
    self.openViper.sd_layout.topSpaceToView(self.viperLoginView, 5)
    .rightSpaceToView(self.viperLoginView, 35)
    .widthIs(65).heightIs(24);
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(changeVIP) name:@"changeVIP" object:nil];
    
    UIButton *titleButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [titleButton addTarget:self action:@selector(personDetail:) forControlEvents:UIControlEventTouchUpInside];
//    titleButton.backgroundColor = GMRedColor;
    [self.headBgImgView addSubview:titleButton];
    titleButton.sd_layout.topEqualToView(self.headTapImage)
    .leftSpaceToView(self.headTapImage, 20)
    .widthIs(110*kGMWidthScale).heightIs(70);
}
- (void)changeVIP {
    [self.openViper setTitle:@"已是会员" forState:UIControlStateNormal];
    self.openViper.selected = NO;
    
}
- (void)personDetail:(UIButton *)send {
    if (self.titleBlock) {
        self.titleBlock(send);
    }
}


- (void)openClick:(UIButton *)send {
    if (self.openViper) {
        self.openButtonBlock(send.tag);
    }
}

- (void)myDetailClick:(UIGestureRecognizer *)send {
    if (self.headTapImage) {
        self.headButtonBlock(0);
    }
}
//设置按钮
- (void)setButtonOne:(UIButton *)send {
    if (self.setButton) {
        self.setButtonBlock(send.tag);
    }
}
//登录按钮
- (void)setLoginBtn:(UIButton *)loginSend {
    if (self.loginButtonBlock) {
        self.loginButtonBlock(loginSend.tag);
    }
    
}
//注册按钮
- (void)setRegisBtn:(UIButton *)regisSend {
    if (self.registerbButtonBlock) {
        self.registerbButtonBlock(regisSend.tag);
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
