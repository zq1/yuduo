//
//  VIPHotZXModel.h
//  yuduo
//
//  Created by mason on 2019/9/5.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface VIPHotZXModel : BaseModel
PropertyStrong(NSString, manag_id);
PropertyStrong(NSString, manag_title);
PropertyStrong(NSString, manag_time);
PropertyStrong(NSString, manag_picture);
PropertyStrong(NSString, thumbsup);
@end

NS_ASSUME_NONNULL_END
