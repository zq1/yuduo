//
//  VIPNewCourseModel.h
//  yuduo
//
//  Created by mason on 2019/9/1.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface VIPNewCourseModel : BaseModel
PropertyStrong(NSString, end_date);
PropertyStrong(NSString, start_date);
PropertyStrong(NSString, course_id);
PropertyStrong(NSString, course_list_cover);
PropertyStrong(NSString, is_free);
PropertyStrong(NSString, member_is_free);
PropertyStrong(NSString, course_under_price);
PropertyStrong(NSString, course_price);
PropertyStrong(NSString, course_name);
PropertyStrong(NSString, brief);
@end

NS_ASSUME_NONNULL_END
