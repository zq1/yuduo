//
//  VIPYDSeletModel.h
//  yuduo
//
//  Created by mason on 2019/9/1.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface VIPYDSeletModel : BaseModel
PropertyStrong(NSString, article_id);
PropertyStrong(NSString, article_title);
PropertyStrong(NSString, article_is_free);
PropertyStrong(NSString, article_member_is_free);
PropertyStrong(NSString, article_is_friends_buys);
PropertyStrong(NSString, article_cover);
PropertyStrong(NSString, thumbs_up);
PropertyStrong(NSString, article_vrows);
PropertyStrong(NSString, brief);//简介
PropertyStrong(NSString, comment_count);//评论数
PropertyStrong(NSString, article_price);//价格

@end

NS_ASSUME_NONNULL_END
