//
//  HotCourseTableViewCell.m
//  yuduo
//
//  Created by Mac on 2019/7/31.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "HotCourseTableViewCell.h"

@implementation HotCourseTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        
        [self createHotOneCourse];
        
        
    };
    return self;
}

- (void)createHotOneCourse {
    
    self.cellView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenW, 259)];
    //    self.cellView.backgroundColor = GMlightGrayColor;
    [self.contentView addSubview:self.cellView];
    
    self.imageBgView = [[UIView alloc]init];
    self.imageBgView.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0];
    self.imageBgView.layer.cornerRadius = 10;
    //    self.imageBgView.backgroundColor = [UIColor redColor];
    self.imageBgView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.imageBgView.layer.shadowOffset = CGSizeMake(0, 0);
    self.imageBgView.layer.shadowOpacity = 0.1;
    self.imageBgView.layer.shadowRadius = 2.0;
    self.imageBgView.layer.cornerRadius = 9.0;
    [self.cellView addSubview:self.imageBgView];
    self.imageBgView.sd_layout.topSpaceToView(self.cellView, 31).leftSpaceToView(self.cellView, 15)
    .rightSpaceToView(self.cellView, 15).heightIs(219);
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
