//
//  VipHotLiveTableViewCell.m
//  yuduo
//
//  Created by Mac on 2019/7/31.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "VipHotLiveTableViewCell.h"

@implementation VipHotLiveTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        
        [self createHotOneCourse];
        
        
    };
    return self;
}

- (void)createHotOneCourse {
    
    self.cellView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenW, 259)];
    self.cellView.layer.cornerRadius = 10;
//    self.cellView.backgroundColor = GMlightGrayColor;
    [self.contentView addSubview:self.cellView];
    
    self.imageBgViews = [[UIView alloc]init];
    self.imageBgViews.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0];
    [self.cellView addSubview:self.imageBgViews];
//    self.imageBgViews.backgroundColor = GMlightGrayColor;
    self.imageBgViews.layer.cornerRadius = 10;
    [self.cellView addSubview:self.imageBgViews];
    self.imageBgViews.sd_layout.topSpaceToView(self.cellView, 25).leftSpaceToView(self.cellView, 0)
    .rightSpaceToView(self.cellView, 0).heightIs(190);
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
