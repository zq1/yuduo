//
//  VipNewCourseTableViewCell.h
//  yuduo
//
//  Created by Mac on 2019/7/31.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface VipNewCourseTableViewCell : UITableViewCell
PropertyStrong(UIView, bgView); //cell底部view 所有控件加到上面
PropertyStrong(UIImageView, headImgView); //头像
PropertyStrong(UILabel , mianfeiLabel); //免费logo
PropertyStrong(UILabel , titleLabel); // 课程名称标题
PropertyStrong(UILabel, detailLabel); //课程简介
PropertyStrong(UIButton, keshiButton); //可是列表按钮
PropertyStrong(UILabel, keshiLabel); //可是标签
PropertyStrong(UIImageView, seeImg); //多少人看过图标
PropertyStrong(UILabel, peoplesLabel); //多少人看过
PropertyStrong(UILabel, xPriceLabel); // 原来划掉价格
PropertyStrong(UILabel, priceLabel); // 现在价格
@end

NS_ASSUME_NONNULL_END
