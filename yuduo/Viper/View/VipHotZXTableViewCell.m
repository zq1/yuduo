//
//  VipHotZXTableViewCell.m
//  yuduo
//
//  Created by Mac on 2019/7/31.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "VipHotZXTableViewCell.h"

@implementation VipHotZXTableViewCell

{
    UIView *_backView;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        [self creatView];
    }
    return self;
}
- (void)creatView {
    //背景view（所有控件都加到背景）
    self.bgView = [[UIView alloc]init];
    //        self.bgView.backgroundColor = GMBlueColor;
    self.bgView.frame = CGRectMake(0, 8, KScreenW, 85+25);
    [self.contentView addSubview:self.bgView];
    
    self.headImg = [[UIImageView alloc] init];
    //    imageView.frame = CGRectMake(30,2742.5,60,60);
    //    [UIImage imageNamed:@"图层 2.png"];
//    self.headImg.layer.cornerRadius = 30;
    self.headImg.backgroundColor = GMBrownColor;
    [self.bgView addSubview:self.headImg];
    self.headImg.sd_layout.topSpaceToView(self.bgView, 10)
    .leftSpaceToView(self.bgView, 15).widthIs(60).heightIs(60);
    
    self.titleLabel = [[UILabel alloc] init];
    //    label.frame = CGRectMake(105.5,2754.5,132.5,13.5);
    self.titleLabel.numberOfLines = 0;
    [self.bgView addSubview:self.titleLabel];
    
    NSMutableAttributedString *stringTitle = [[NSMutableAttributedString alloc] initWithString:@"如何让孩子自己睡觉？" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 14],NSForegroundColorAttributeName: [UIColor colorWithRed:69/255.0 green:69/255.0 blue:69/255.0 alpha:1.0]}];
    self.titleLabel.attributedText = stringTitle;
    self.titleLabel.sd_layout.topSpaceToView(self.bgView, 12)
    .leftSpaceToView(self.headImg, 16)
    .widthIs(200).heightIs(14);
    
    self.timeImg = [[UIImageView alloc]init];
    //    self.timeImg.backgroundColor = GMRedColor;
    self.timeImg.image = [UIImage imageNamed:@"ico_time"];
    [self.bgView addSubview:self.timeImg];
    self.timeImg.sd_layout.topSpaceToView(self.bgView, 49)
    .leftSpaceToView(self.headImg, 15).widthIs(15).heightIs(15);
    
    self.timeLabel = [[UILabel alloc] init];
    //    label.frame = CGRectMake(129.5,2785.5,68.5,9);
    self.timeLabel.numberOfLines = 0;
    self.timeLabel.text = [self currentdateInterval];
    [self.bgView addSubview:self.timeLabel];
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"2019-05-30" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 12],NSForegroundColorAttributeName: [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1.0]}];
    self.timeLabel.attributedText  = string;
    self.timeLabel.sd_layout.topSpaceToView(self.bgView, 53)
    .leftSpaceToView(self.timeImg, 10).widthIs(82).heightIs(9);
    
    //多少人看过图标
    self.seeImg = [[UIImageView alloc] init];
    self.seeImg.image = [UIImage imageNamed:@"ico_look"];
    [self.bgView addSubview:self.seeImg];
    self.seeImg.sd_layout.topEqualToView(self.timeImg).leftSpaceToView(self.timeLabel, 22).widthIs(15).heightIs(15);
    
    //多少人看过具体人数标签
    self.peopleLabel = [[UILabel alloc] init];
    //    self.peopleLabel.frame = CGRectMake(236.5,970,36,10.5);
    self.peopleLabel.numberOfLines = 0;
    [self.bgView addSubview:self.peopleLabel];
    NSMutableAttributedString *stringPeople = [[NSMutableAttributedString alloc] initWithString:@"2478" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 11],NSForegroundColorAttributeName: [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1.0]}];
    
    self.peopleLabel.attributedText = stringPeople;
    self.peopleLabel.sd_layout.topEqualToView(self.timeLabel)
    .leftSpaceToView(self.seeImg, 6).widthIs(30).heightIs(9);
    
    self.zanImg = [[UIImageView alloc]init];
    //    self.zanImg.backgroundColor = GMGreenColor;
    self.zanImg.image = [UIImage imageNamed:@"ico_zan_1"];
    [self.bgView addSubview:self.zanImg];
    self.zanImg.sd_layout.topSpaceToView(self.bgView, 50)
    .leftSpaceToView(self.peopleLabel, 5*kGMWidthScale)
    .widthIs(15)
    .heightIs(15);
    self.dianZanLabel = [[UILabel alloc] init];
    //    label.frame = CGRectMake(324.5,2785.5,20.5,9);
    self.dianZanLabel.numberOfLines = 0;
    [self.bgView addSubview:self.dianZanLabel];
    
    NSMutableAttributedString *stringDZ = [[NSMutableAttributedString alloc] initWithString:@"200" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 12],NSForegroundColorAttributeName: [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1.0]}];
    self.dianZanLabel.attributedText = stringDZ;
    self.dianZanLabel.sd_layout.topEqualToView(self.peopleLabel)
    .leftSpaceToView(self.zanImg, 5).widthIs(25).heightIs(9);
    
}

-(NSString *)currentdateInterval {
    NSDate *datenow = [NSDate date];
    NSString *timeSp = [NSString stringWithFormat:@"%ld", (long)([datenow timeIntervalSince1970]*1000)];
    return timeSp;
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
