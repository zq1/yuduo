//
//  HotCourseTableViewCell.h
//  yuduo
//
//  Created by Mac on 2019/7/31.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HotCourseTableViewCell : UITableViewCell
PropertyStrong(UIView , imageBgView);
PropertyStrong(UIView, cellView);
@end

NS_ASSUME_NONNULL_END
