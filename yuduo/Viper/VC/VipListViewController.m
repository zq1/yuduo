//
//  VipListViewController.m
//  yuduo
//
//  Created by mason on 2019/9/15.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "VipListViewController.h"
#import "NewCourseTwoTableViewCell.h"
#import "CourseListViewController.h"
#import "testModel.h"
#import "CourseCategory_idModel.h"
#import <SVProgressHUD.h>
#import "CoureDetailViewController.h"
@interface VipListViewController ()
<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) NSMutableArray *respoceArray;
PropertyStrong(NSMutableArray, courseArray);
PropertyStrong(NSMutableArray, hotCourseArray);
PropertyStrong(NSMutableArray, mianfeiArray);
PropertyStrong(NSMutableArray, vipMianfeiArray);
@end

@implementation VipListViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    //    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(100, 0, 200, 80)];
    //    view.backgroundColor = GMBlueColor;
    //    [self.view addSubview:view];
    //    [self getCategory];
    //最新课程
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(HomeNewcourseVIP) name:@"HomeNewcourseVIP" object:nil];
    //最热课程
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(HomeHotCourseVIP) name:@"HomeHotCourseVIP" object:nil];
    //免费课程
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(MianfeiCourseVIP) name:@"mianfeiCourseVIP" object:nil];
    //vip限免
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(vipMianfeiVIP) name:@"vipMianfeiVIP" object:nil];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 10, KScreenW, KScreenH-48-MStatusBarHeight-44) style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 156;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (self.index == 0) {
        if (self.respoceArray) {
            return self.respoceArray.count;
        }
        
    }
    return 0;
}

//- (void)getCategory {
//    self.courseArray = [NSMutableArray array];
//    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
//    dic[@"pid"] = @"1";
//    [GMAfnTools PostHttpDataWithUrlStr:KCategoryID Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
//        NSLog(@"打印课程-----%@",responseObject);
//        for (NSMutableDictionary *idDic in responseObject) {
//            CourseCategory_idModel *model = [[CourseCategory_idModel alloc]init];
//            [model mj_setKeyValues:idDic];
//            [self.courseArray addObject:model];
//        }
//
//    } FailureBlock:^(id  _Nonnull error) {
//
//    }];
//}
/*-----------++++++++++++++VIP课程----------------------*/
#pragma mark--VIP免费
- (void)vipMianfeiVIP {
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD show];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // 延时事件
        //        [self NewCourseOnde];
        self.vipMianfeiArray = [NSMutableArray array];
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        dic[@"pid"] = @"1";
        __weak typeof(self ) weakSelf = self;
        [GMAfnTools PostHttpDataWithUrlStr:KCategoryID Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
            if (responseObject) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    for (NSMutableDictionary *idDic in responseObject) {
                        CourseCategory_idModel *model = [[CourseCategory_idModel alloc]init];
                        [model mj_setKeyValues:idDic];
                        [self.vipMianfeiArray addObject:model];
                        [weakSelf vipCourse:model.category_id];
                    }
                    [SVProgressHUD dismiss];
                });
                NSLog(@"打印课程-----%@",responseObject);
            }else {
                //加载失败隐藏
            }
        }
                              FailureBlock:^(id  _Nonnull error) {
                                  [SVProgressHUD setStatus:@"加载失败..."];
                                  [SVProgressHUD dismissWithDelay:10];
                              }];
    });
}
#pragma mark--VIP免费
- (void)vipCourse:(NSString *)string {
    self.respoceArray = [NSMutableArray array];
    NSMutableDictionary *newCourseDic = [NSMutableDictionary dictionary];
    
    newCourseDic[@"category_id"] = string;
    newCourseDic[@"screen"] = @"4";
    [GMAfnTools PostHttpDataWithUrlStr:KCourseList Dic:newCourseDic SuccessBlock:^(id  _Nonnull responseObject) {
        for (NSMutableDictionary *dic in responseObject) {
            testModel *mol = [[testModel alloc]init];
            [mol mj_setKeyValues:dic];
            [self.respoceArray addObject:mol];
        }
        [self.tableView reloadData];
        
    }
                          FailureBlock:^(id  _Nonnull error) {
                          }];
    //    [self.tableView reloadData];
}
/*-----------++++++++++++++免费课程----------------------*/
#pragma mark--免费课程
- (void)MianfeiCourseVIP {
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD show];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // 延时事件
        //        [self NewCourseOnde];
        self.mianfeiArray = [NSMutableArray array];
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        dic[@"pid"] = @"1";
        __weak typeof(self ) weakSelf = self;
        [GMAfnTools PostHttpDataWithUrlStr:KCategoryID Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
            if (responseObject) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    for (NSMutableDictionary *idDic in responseObject) {
                        CourseCategory_idModel *model = [[CourseCategory_idModel alloc]init];
                        [model mj_setKeyValues:idDic];
                        [self.mianfeiArray addObject:model];
                        [weakSelf mianfeiCourse:model.category_id];
                    }
                    [SVProgressHUD dismiss];
                });
                NSLog(@"打印课程-----%@",responseObject);
            }else {
                //加载失败隐藏
            }
        }
                              FailureBlock:^(id  _Nonnull error) {
                                  [SVProgressHUD setStatus:@"加载失败..."];
                                  [SVProgressHUD dismissWithDelay:10];
                              }];
    });
}
#pragma mark--免费课程
- (void)mianfeiCourse:(NSString *)string {
    self.respoceArray = [NSMutableArray array];
    NSMutableDictionary *newCourseDic = [NSMutableDictionary dictionary];
    
    newCourseDic[@"category_id"] = string;
    newCourseDic[@"screen"] = @"3";
    [GMAfnTools PostHttpDataWithUrlStr:KCourseList Dic:newCourseDic SuccessBlock:^(id  _Nonnull responseObject) {
        for (NSMutableDictionary *dic in responseObject) {
            testModel *mol = [[testModel alloc]init];
            [mol mj_setKeyValues:dic];
            [self.respoceArray addObject:mol];
        }
        [self.tableView reloadData];
        
    }
                          FailureBlock:^(id  _Nonnull error) {
                          }];
    //    [self.tableView reloadData];
}
/*----------------最热课程------------------------*/
#pragma mark--最热课程
- (void)HomeHotCourseVIP {
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD show];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // 延时事件
        //        [self NewCourseOnde];
        self.courseArray = [NSMutableArray array];
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        dic[@"pid"] = @"1";
        __weak typeof(self ) weakSelf = self;
        [GMAfnTools PostHttpDataWithUrlStr:KCategoryID Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
            if (responseObject) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    for (NSMutableDictionary *idDic in responseObject) {
                        CourseCategory_idModel *model = [[CourseCategory_idModel alloc]init];
                        [model mj_setKeyValues:idDic];
                        [self.courseArray addObject:model];
                        [weakSelf hotCourse:model.category_id];
                    }
                    [SVProgressHUD dismiss];
                });
                NSLog(@"打印课程-----%@",responseObject);
            }else {
                //加载失败隐藏
            }
        }
                              FailureBlock:^(id  _Nonnull error) {
                                  [SVProgressHUD setStatus:@"加载失败..."];
                                  [SVProgressHUD dismissWithDelay:10];
                              }];
    });
}
#pragma mark--最热课程
- (void)hotCourse:(NSString *)string {
    self.respoceArray = [NSMutableArray array];
    NSMutableDictionary *newCourseDic = [NSMutableDictionary dictionary];
    
    newCourseDic[@"category_id"] = string;
    newCourseDic[@"screen"] = @"2";
    [GMAfnTools PostHttpDataWithUrlStr:KCourseList Dic:newCourseDic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"54545445454545454==%@",responseObject);
        for (NSMutableDictionary *dic in responseObject) {
            testModel *mol = [[testModel alloc]init];
            [mol mj_setKeyValues:dic];
            [self.respoceArray addObject:mol];
        }
        [self.tableView reloadData];
        
    }
                          FailureBlock:^(id  _Nonnull error) {
                          }];
    //    [self.tableView reloadData];
}

/*++++++++++++++++最新课程++++++++++++++++++++++++++++*/

#pragma mark--最新课程
- (void)HomeNewcourseVIP {
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD show];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // 延时事件
        //        [self NewCourseOnde];
        self.hotCourseArray = [NSMutableArray array];
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        dic[@"pid"] = @"1";
        __weak typeof(self ) weakSelf = self;
        [GMAfnTools PostHttpDataWithUrlStr:KCategoryID Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
            if (responseObject) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    for (NSMutableDictionary *idDic in responseObject) {
                        CourseCategory_idModel *model = [[CourseCategory_idModel alloc]init];
                        [model mj_setKeyValues:idDic];
                        [self.hotCourseArray addObject:model];
                        
                        [weakSelf newCourse:model.category_id];
                    }
                    [SVProgressHUD dismiss];
                });
                NSLog(@"打印课程-----%@",responseObject);
                
            }else {
                //加载失败隐藏
                
            }
        }
                              FailureBlock:^(id  _Nonnull error) {
                                  [SVProgressHUD setStatus:@"加载失败..."];
                                  [SVProgressHUD dismissWithDelay:10];
                                  
                              }];
        
        
    });
}
#pragma mark--最新课程
- (void)newCourse:(NSString *)string {
    self.respoceArray = [NSMutableArray array];
    NSMutableDictionary *newCourseDic = [NSMutableDictionary dictionary];
    
    newCourseDic[@"category_id"] = string;
    newCourseDic[@"screen"] = @"1";
    [GMAfnTools PostHttpDataWithUrlStr:KCourseList Dic:newCourseDic SuccessBlock:^(id  _Nonnull responseObject) {
        for (NSMutableDictionary *dic in responseObject) {
            testModel *mol = [[testModel alloc]init];
            [mol mj_setKeyValues:dic];
            [self.respoceArray addObject:mol];
        }
        [self.tableView reloadData];
        
    }
     
                          FailureBlock:^(id  _Nonnull error) {
                              
                          }];
    //    [self.tableView reloadData];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    testModel *mol = [self.respoceArray objectAtIndex:indexPath.row];
    CoureDetailViewController *courseVC = [[CoureDetailViewController alloc]init];
    courseVC.courseDetailIDString = mol.course_id;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"pushNameVIP" object:mol.course_id];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"cellID";
    NewCourseTwoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    //    cell.textLabel.text = [NSString stringWithFormat:@"%ld",self.index ];
    if (!cell) {
        cell = [[NewCourseTwoTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    switch (self.index) {
        case 0:{
            
            testModel *mox = [self.respoceArray objectAtIndex:indexPath.row];
            //                cell.textLabel.text = mox.course_price;
            [cell.headImgView sd_setImageWithURL:[NSURL URLWithString:mox.course_list_cover]];
            cell.headImgView.sd_cornerRadius = [NSNumber numberWithInteger:10];
            cell.titleLabel.text = mox.course_name;
            cell.detailLabel.text = mox.course_name;
            cell.keshiLabel.text = [mox.class_hour stringByAppendingString:@"课时"];
            if ([mox.member_is_free isEqualToString:@"1"]) {
                cell.mianfeiLabel.text = @"Vip免费";
            }else {
                cell.mianfeiLabel.hidden = YES;
                cell.titleLabel.sd_layout.leftSpaceToView(cell.headImgView, 22);
            }
            
            cell.xPriceLabel.text = mox.course_under_price;
            cell.priceLabel.text = mox.course_price;
            //                cell.peoplesLabel.text = mox.
            NSLog(@"testestsetet%@",mox.course_price);
            
        }
            break;
        case 1:
        {
            
            //            cell.textLabel.text = @"1111";
        }
            
            break;
            
        case 2:{
            
            //            cell.textLabel.text = @"222";
        }
            
            break;
        case 3:{
            
            //            cell.textLabel.text = @"333";
        }
            break;
        case 4:{
            
            //            cell.textLabel.text = @"9999999999";
        }
            break;
            
        default:
            break;
    }
    
    return cell;
    
    
}



@end
