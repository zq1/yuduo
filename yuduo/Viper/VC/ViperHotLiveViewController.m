//
//  ViperHotLiveViewController.m
//  yuduo
//
//  Created by Mac on 2019/8/20.
//  Copyright © 2019 yaocongkeji. All rights reserved.

#import "ViperHotLiveViewController.h"
#import "HomeLiveTableViewCell.h"
@interface ViperHotLiveViewController ()
<UITableViewDelegate,UITableViewDataSource>

PropertyStrong(UITableView, liveTableView);

@end

@implementation ViperHotLiveViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //创建tableview
    [self createTableView];
    [self changeNavigation];
    
    // Do any additional setup after loading the view.
}

- (void)createTableView {
    self.liveTableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    
    self.liveTableView.delegate = self;
    self.liveTableView.showsVerticalScrollIndicator = NO;
    self.liveTableView.dataSource = self;
    [self.view addSubview:self.liveTableView];
    self.liveTableView.sd_layout.topSpaceToView(self.view, 54+MStatusBarHeight)
    .leftSpaceToView(self.view, 15)
    .rightSpaceToView(self.view, 15)
    .widthIs(KScreenW-30).heightIs(KScreenH-54-MStatusBarHeight);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 239;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellId = @"cellID";
    HomeLiveTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    
    if (!cell) {
        
        cell = [[HomeLiveTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];;
        
    }
    
    cell.backgroundColor = GMlightGrayColor;
    return cell;
}

- (void)changeNavigation {
    
    self.navigationController.navigationBar.hidden = YES;
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = GMWhiteColor;
    [self.view addSubview:view];
    view.sd_layout.topSpaceToView(self.view, 0)
    .leftSpaceToView(self.view, 0)
    .widthIs(KScreenW).heightIs(44+MStatusBarHeight);
    
    self.navigationController.navigationBar.topItem.title = @"直播";
    self.view.backgroundColor = GMWhiteColor;
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setBackgroundImage:[UIImage imageNamed:@"back-icon"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:backButton];
    backButton.sd_layout.bottomSpaceToView(view, 13)
    .leftSpaceToView(view, 15)
    .widthIs(11).heightIs(19);
    UIButton *backButtonView = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 80, MStatusBarHeight+44)];
    backButtonView.backgroundColor = RGBA(15, 15, 15, 0);
    [backButtonView addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButtonView];
    
    
    UILabel *titleLabel = [[UILabel alloc]init];
    [view addSubview:titleLabel];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"直播" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 18],NSForegroundColorAttributeName: [UIColor colorWithRed:69/255.0 green:69/255.0 blue:69/255.0 alpha:1.0]}];
    titleLabel.attributedText = string;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.sd_layout.bottomSpaceToView(view, 14)
    .centerXEqualToView(view)
    .heightIs(17).widthIs(100);
}
- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBar.topItem.title = @"会员";
}
- (void)backBtn {
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
