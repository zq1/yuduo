//
//  VipListViewController.h
//  yuduo
//
//  Created by mason on 2019/9/15.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface VipListViewController : UIViewController
@property (nonatomic, assign) NSInteger index;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSString *gmxtest;

@property (nonatomic, copy) void(^stringIDBlock)(NSString *stringID);
@end

NS_ASSUME_NONNULL_END
