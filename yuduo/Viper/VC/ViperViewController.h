//
//  ViperViewController.h
//  yuduo
//
//  Created by Mac on 2019/7/23.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
NS_ASSUME_NONNULL_BEGIN

@interface ViperViewController : BaseViewController
PropertyStrong(UIView, headBgView); //头部view背景
PropertyStrong(UIImageView, vipCard); //会员卡
PropertyStrong(UILabel, addVipLabel); //加入会员
PropertyStrong(UIButton, atOnceBtn); //立即加入
PropertyStrong(UIImageView, logoImg); // childImg

PropertyStrong(UITableView, vipTableView);//Viptableview
//PropertyStrong(MCScrollView,scrollView);
PropertyStrong(UIView, headBGView);
PropertyStrong(NSArray, sectionTitle); //分区数组
PropertyStrong(NSMutableArray, dataArray);
@property (assign , nonatomic) NSInteger selectIndex;
@end

NS_ASSUME_NONNULL_END
