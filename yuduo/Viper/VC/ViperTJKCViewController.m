//
//  ViperTJKCViewController.m
//  yuduo
//
//  Created by Mac on 2019/8/20.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "ViperTJKCViewController.h"
#import "JZLSliderMenuView.h"
#import "ListCouViewController.h"
#import "VipListViewController.h"
#import "CoureDetailViewController.h"
@interface ViperTJKCViewController ()
<JZLSliderMenuDelegate,UITableViewDelegate,UITableViewDataSource>
{
    UIView *lightBgView;
    UIButton *sortButton;
    UIWindow *windowBg;
}
@property (nonatomic, strong) JZLSliderMenuView *sliderMenuView;
PropertyStrong(UITableView, sortTableView);
@end

@implementation ViperTJKCViewController

-(void)dealloc {
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = YES;
   
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(pushNameVIP:) name:@"pushNameVIP" object:nil];
    
    [self changeNavigation];
    [self test];
}

- (void)changeNavigation {
    
    self.navigationController.navigationBar.topItem.title = @"课程";
    self.view.backgroundColor = GMWhiteColor;
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setBackgroundImage:[UIImage imageNamed:@"back-icon"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButton];
    backButton.sd_layout.topSpaceToView(self.view,MStatusBarHeight+12)
    .leftSpaceToView(self.view, 15)
    .widthIs(11).heightIs(19);
    
    UIButton *backButtonView = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 80, MStatusBarHeight+44)];
    backButtonView.backgroundColor = RGBA(15, 15, 15, 0);
    [backButtonView addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButtonView];
    
    UILabel *titleLabel = [[UILabel alloc]init];
    [self.view addSubview:titleLabel];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"课程" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 18],NSForegroundColorAttributeName: [UIColor colorWithRed:69/255.0 green:69/255.0 blue:69/255.0 alpha:1.0]}];
    titleLabel.attributedText = string;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.sd_layout.topSpaceToView(self.view, MStatusBarHeight+13)
    .centerXEqualToView(self.view)
    .heightIs(18).widthIs(100);
    
    UIButton *tapButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [tapButton addTarget:self action:@selector(sortButton:) forControlEvents:UIControlEventTouchUpInside];
    [tapButton setBackgroundImage:[UIImage imageNamed:@"排序"] forState:UIControlStateNormal];
    [self.view addSubview:tapButton];
    tapButton.sd_layout.topSpaceToView(self.view,MStatusBarHeight+12)
    .rightSpaceToView(self.view, 15)
    .widthIs(24).heightIs(24);
    
    lightBgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenW, KScreenH)];
    windowBg = [[UIApplication sharedApplication].windows lastObject];
    lightBgView.backgroundColor = RGBA(153, 153, 153, 0.5);
    lightBgView.hidden = YES;
    lightBgView.userInteractionEnabled  = YES;
    [windowBg addSubview:lightBgView];
    
    UIButton *windowButton = [UIButton buttonWithType:UIButtonTypeCustom];
    windowButton.frame = lightBgView.frame;
    [windowButton addTarget:self action:@selector(touchClick) forControlEvents:UIControlEventTouchUpInside];
    [lightBgView addSubview:windowButton];
    //创建排序
    [self sortTab];
    
}
#pragma mark 排序tableView
- (void)sortTab {
    self.sortTableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.sortTableView.delegate = self;
    self.sortTableView.dataSource = self;
    self.sortTableView.tag = 10086;
    self.sortTableView.layer.cornerRadius = 10;
    //    self.sortTableView.backgroundColor = GMBlueColor;
    //    self.sortTableView.hidden = YES;
    [lightBgView addSubview:self.sortTableView];
    self.sortTableView.sd_layout.topSpaceToView(lightBgView, 70*kGMHeightScale)
    .rightSpaceToView(lightBgView, 15)
    .widthIs(105*kGMWidthScale).heightIs(169);
    
}
- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBar.topItem.title = @"会员";
}
#pragma mark - 排序method
- (void)sortButton:(UIButton *)send {
    NSLog(@"点击了排序按钮");
    send.selected=!sortButton.selected;//每次点击都改变按钮的状态
    [UIView transitionWithView:lightBgView duration:1 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        self->lightBgView.backgroundColor = RGBA(153, 153, 153, 0.4);
    } completion:nil];
    
    lightBgView.hidden = NO;
    
    if(send.selected == YES){
        
        lightBgView.hidden = NO;
    }
    if (send.selected == NO) {
        lightBgView.hidden = YES;
    }
}
- (void)touchClick {
    NSLog(@" touch");
    sortButton.selected = NO;
    lightBgView.hidden = YES;
    //    self.view.backgroundColor = GMlightGrayColor;
    //        sortButton.selected = NO;
}

- (void)backBtn {
    NSLog(@"xxxxxxxx");
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)test {
    
    NSArray *titleArr = @[@"全部",@"读书会",@"幼婴儿",@"学习",@"儿童",@"小学",@"感冒"];
    
    NSMutableArray *vcArr = [NSMutableArray array];
    for (int i =0; i < titleArr.count ; i ++) {
        VipListViewController *vc = [[VipListViewController alloc] init];
        [vcArr addObject:vc];
    }
    
    self.sliderMenuView = [JZLSliderMenuView initWithFrame:CGRectMake(0, MStatusBarHeight+44, [UIScreen mainScreen].bounds.size.width, KScreenH-MStatusBarHeight-44) childViewControllers:vcArr titleArray:titleArr selectedIndex:0];
    self.sliderMenuView.delegate = self;
    [self.view addSubview:self.sliderMenuView];
    
    //模拟控制器数据处理
    VipListViewController *vc = vcArr[2];
    vc.index = 2;
    [vc.tableView reloadData];
}
- (void)sliderMenu:(JZLSliderMenuView *)sliderMenuView selectItemAtIndex:(NSInteger)index {
    VipListViewController *vc = self.sliderMenuView.childViewControllers[index];
    vc.index = index;
    [vc.tableView reloadData];
    
}
- (void)pushNameVIP:(NSNotification *)courseID {
    CoureDetailViewController *home = [[CoureDetailViewController alloc]init];
    home.courseDetailIDString = [courseID object];
    [self.navigationController pushViewController:home animated:YES];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSLog(@"=======+%ld",indexPath.row);
    switch (indexPath.row) {
        case 0:
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"HomeNewcourseVIP" object:nil];
            [self touchClick];
        }
            
            break;
        case 1:
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"HomeHotCourseVIP" object:nil];
            [self touchClick];
        }
            break;
        case 2:
        {
            //免费课程
            [[NSNotificationCenter defaultCenter] postNotificationName:@"mianfeiCourseVIP" object:nil];
            [self touchClick];
            
        }
            break;
        case 3:
        {
            //vip限免
            [[NSNotificationCenter defaultCenter]postNotificationName:@"vipMianfeiVIP" object:nil];
            [self touchClick];
        }
            break;
        default:
            break;
    }
}

#pragma mark--返回行高
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40;
}
#pragma mark--返回行数
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.sortTableView) {
        return 4;
    }
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSArray *arr = @[@"最新课程",@"最热课程",@"免费课程",@"VIP限免"];
    static NSString*key0=@"cell0";
    
    UITableViewCell*cell;
    
    if (tableView.tag == 10086) {
        cell=[self.sortTableView dequeueReusableCellWithIdentifier:key0];
        if (cell==nil) {
            cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:key0];
        }
        cell.textLabel.text = [arr objectAtIndex:indexPath.row];
        cell.textLabel.font = [UIFont systemFontOfSize:14];
    }
    
    
    return cell;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
