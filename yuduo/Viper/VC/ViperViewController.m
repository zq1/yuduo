//
//  ViperViewController.m
//  yuduo
//
//  Created by Mac on 2019/7/23.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "ViperViewController.h"
#import "HotCourseTableViewCell.h"
#import "VipHotLiveTableViewCell.h"
#import "VipNewCourseTableViewCell.h"
#import "VipHotZXTableViewCell.h"
#import "YDJXTableViewCell.h"
//轮播图
#import "HZBannerView.h"
#import "HZBannerCell.h"
#import "HZBannerModel.h"
#import "GMBannerCell.h"
#import "GMBannerView.h"
#import "MGBannerModel.h"

#import "JoinVipViewController.h"
//最热课程
#import "CourseListViewController.h"
#import "HomeLiveViewController.h"
//热门直播
#import "ViperHotLiveViewController.h"
#import "ViperTJKCViewController.h"
#import "LoginViewController.h"
#import "CoureDetailViewController.h"
#import "NewZhiboDetailViewController.h"
//数据模型
#import "VIPNewCourseModel.h"//最新课程
#import "VIPYDSeletModel.h"//育朵精选
#import "VIPHotZXModel.h" //热门咨询
#import "CourseLiveListsModel.h" //直播列表
#import "OpenViperViewController.h"
#import "JXdetailViewController.h"
#import "HomeZXViewController.h"
#import "ZXDetailViewController.h"
#import "HomeBannerModel.h"
#import "NewYuDuoChoiceDetailViewController.h"
#import "SDCycleScrollView.h"
#import "PersonInfoModel.h"
@interface ViperViewController ()<UITableViewDelegate,UITableViewDataSource,HZBannerViewDelegate,GMBannerViewDelegate, SDCycleScrollViewDelegate>
@property (nonatomic, strong) SDCycleScrollView *bannerCycleScrollerView;
//最热课程
PropertyStrong(NSMutableArray, VIPHotCourseArray);
//最新课程数据
PropertyStrong(NSMutableArray, VIPNewCourseArray);
//育朵精选
PropertyStrong(NSMutableArray, VIPYDSseletArray);
//热门咨询
PropertyStrong(NSMutableArray, VIPHotZXArray);
@property(nonatomic,strong)NSMutableArray *homeBannerArray;
@property(nonatomic,strong)NSMutableArray *courseLiveArray;//热门直播

@end

@implementation ViperViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getNewCourse];
    [self getYDSelectData];
    [self getVIPHotZX];
    [self getHotCourse];
    [self getCourseLiveData];
    //修改会员
    
    //修改导航栏
    self.title = @"会员";
//    self.view.backgroundColor = GMBrownColor;
    [self changeNavigationssss];
    
    //会员卡
    [self vipCardBackground];
    [self creatVipTabView];
    [self getDataHomeBanner];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = NO;
    self.navigationController.navigationBar.topItem.title = @"会员";
    
    NSMutableDictionary *infoDic = [NSMutableDictionary dictionary];
    
    NSUserDefaults *detafaults = [NSUserDefaults standardUserDefaults];
    infoDic[@"user_id"] = [detafaults objectForKey:@"user_id"];
    infoDic[@"token"] = [detafaults objectForKey:@"token"];
    
    [GMAfnTools PostHttpDataWithUrlStr:KInfoShow Dic:infoDic SuccessBlock:^(id  _Nonnull responseObject) {
        PersonInfoModel *personModel = [[PersonInfoModel alloc]init];
        [personModel mj_setKeyValues:responseObject];
        self.bannerCycleScrollerView.hidden = [personModel.is_vip isEqualToString:@"2"];
    } FailureBlock:^(id  _Nonnull error) {
        
    }];
    
}

- (void)getDataHomeBanner {
    self.homeBannerArray = [NSMutableArray array];
    NSMutableDictionary *bannerDic = [NSMutableDictionary dictionary];
    bannerDic[@"type"] = @"3";
    bannerDic[@"url_type"] = @"1";
    
    [GMAfnTools PostHttpDataWithUrlStr:KHomeBanner Dic:bannerDic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"首页轮播图-----%@",responseObject);
        NSMutableArray *imageList = [NSMutableArray array];
        for (NSMutableDictionary *dic in responseObject) {
            HomeBannerModel *mol = [[HomeBannerModel alloc]init];
            [mol mj_setKeyValues:dic];
            if ([mol.status isEqualToString:@"1"]) {
                [self.homeBannerArray addObject:mol];
                [imageList addObject:mol.picture];
            }
        }
        self.bannerCycleScrollerView.imageURLStringsGroup = imageList;
    } FailureBlock:^(id  _Nonnull error) {
        
    }];
}



- (void)vipCardBackground{
    
    self.headBgView = [[UIView alloc]init];
//    self.headBgView.backgroundColor = GMRedColor;
    self.headBGView.userInteractionEnabled = YES;
//    [self.view addSubview:self.headBgView];
    self.headBgView.sd_layout.topSpaceToView(self.view, 17)
    .rightEqualToView(self.view)
    .leftEqualToView(self.view)
    .widthIs(KScreenW).heightIs(217);
    
    self.vipCard = [[UIImageView alloc]init];
//    self.vipCard.backgroundColor = GMBlueColor;
    self.vipCard.userInteractionEnabled = YES;
    self.vipCard.layer.cornerRadius = 20;
    self.vipCard.image = [UIImage imageNamed:@"加入会员"];
    [self.headBgView addSubview:self.vipCard];
    self.vipCard.sd_layout.topSpaceToView(self.headBGView, 21)
    .leftSpaceToView(self.headBgView, 15)
    .rightSpaceToView(self.headBgView, 15)
    .heightIs(175);
    
    
    self.bannerCycleScrollerView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(15, 21, KScreenW-30, 175) delegate:self placeholderImage:[UIImage imageNamed:@"placeholder"]];
    self.bannerCycleScrollerView.layer.cornerRadius = 10;
    self.bannerCycleScrollerView.layer.masksToBounds = YES;
    self.bannerCycleScrollerView.currentPageDotImage = [UIImage imageNamed:@"pageControlCurrentDot"];
    self.bannerCycleScrollerView.hidden = YES;
    self.bannerCycleScrollerView.pageDotImage = [UIImage imageNamed:@"pageControlDot"];
    [self.headBgView addSubview:self.bannerCycleScrollerView];
    
    
    //加入会员
    self.addVipLabel = [[UILabel alloc]init];
//     NSMutableAttributedString *stringVip = [[NSMutableAttributedString alloc] initWithString:@"加入会员" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 18],NSForegroundColorAttributeName: [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0]}];
//    self.addVipLabel.attributedText = stringVip;
    [self.vipCard addSubview:self.addVipLabel];
    self.addVipLabel.sd_layout.topSpaceToView(self.vipCard, 71)
    .leftSpaceToView(self.vipCard, 70)
    .bottomSpaceToView(self.vipCard, 87)
    .widthIs(87).heightIs(18);
    
    //修改会员状态
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(changeButtonTitle) name:@"changeVipTitle" object:nil];
    //立即加入
    self.atOnceBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    self.atOnceBtn.backgroundColor = GMlightGrayColor;
    self.atOnceBtn.layer.cornerRadius = 13;
//    self.atOnceBtn.tintColor = GMWhiteColor;
//    [self.atOnceBtn setTitle:@"立即加入" forState:UIControlStateNormal];
    self.atOnceBtn.titleLabel.font = [UIFont systemFontOfSize:12];
    [self.atOnceBtn addTarget:self action:@selector(vipButton) forControlEvents:UIControlEventTouchUpInside];
    [self.vipCard addSubview:self.atOnceBtn];
    self.atOnceBtn.sd_layout.bottomSpaceToView(self.vipCard, 48)
    .leftSpaceToView(self.vipCard, 67)
    .widthIs(74).heightIs(26);
//    //logoImg
//    self.logoImg = [[UIImageView alloc]init];
//    self.logoImg.backgroundColor = GMlightGrayColor;
//    [self.vipCard addSubview:self.logoImg];
//    self.logoImg.sd_layout.topSpaceToView(self.vipCard, 26)
//    .rightSpaceToView(self.vipCard, 6)
//    .widthIs(94).heightIs(119);
}

#pragma mark---修改会员状态
- (void)changeButtonTitle {
//    [self.atOnceBtn setTitle:@"已是会员" forState:UIControlStateNormal];
//    self.addVipLabel.text = @"会员";
}

#pragma mark 创建TableView

- (void)creatVipTabView {
    
    self.sectionTitle = @[@"最热课程",@"热门直播",@"热门咨询",@"最新课程",@"育朵精选"];
    //    self.bgScrollView.backgroundColor = GMBlueColor;
    self.vipTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, KScreenW, KScreenH-87)style:UITableViewStyleGrouped];
    self.vipTableView.delegate = self;
    self.vipTableView.dataSource = self;
    
    [self.vipTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    //取消分割线
//    self.vipTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.vipTableView.backgroundColor = GMWhiteColor;
    self.vipTableView.tableHeaderView = self.headBgView;
    self.automaticallyAdjustsScrollViewInsets = NO;

    //    self.HomeTableView.tableHeaderView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 0, CGFLOAT_MAX)];
    self.vipTableView.backgroundView = nil;
//    self.vipTableView.backgroundColor = [UIColor clearColor];
    //    self.HomeTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.vipTableView];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.sectionTitle.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier0 = @"cell0";
    static NSString *cellIdentifier1 = @"cell1";
    static NSString *cellIdentifier2 = @"cell2";
    static NSString *cellIdentifier3 = @"cell3";
    static NSString *cellIdentifier4 = @"cell4";
    switch (indexPath.section){
            // 课程最热课程
        case 0:
        {
            HotCourseTableViewCell *cell0 = [[HotCourseTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier0];
            NSMutableArray *arr = [NSMutableArray array];
            for (VIPNewCourseModel *mol in self.VIPNewCourseArray) {
                NSMutableDictionary *dic = [NSMutableDictionary dictionary];
                [dic setObject:mol.course_list_cover forKey:@"picture"];
                [dic setObject:mol.course_name forKey:@"title"];
                [dic setObject:mol.course_id forKey:@"courseID"];
                [dic setObject:mol.is_free forKey:@"isFree"];
                //                    //限时免费
                ////                    dic setObject:mol.cou forKey:<#(nonnull id<NSCopying>)#>
                [arr addObject:dic];
            }
            //                NSDictionary *xxxx = @{@"picture":@"http://yuduo.oss-cn-beijing.aliyuncs.com/70bdce5697c082cf6623ad5adf43e777.jpg"};
            //                NSArray *arr = @[xxxx];
//            _models = @[].mutableCopy;
            NSMutableArray *newArray = [NSMutableArray array];
            for (NSDictionary *dic in arr) {
                HZBannerModel *model = [HZBannerModel new];
                [model setValuesForKeysWithDictionary:dic];
                NSLog(@"-------hahaha%@",model);
                [newArray addObject:model];
            }
            
            HZBannerView *banner = ({
                //                    CGFloat width = self.view.bounds.size.width;
                HZBannerView *banner = [[HZBannerView alloc] initWithFrame:CGRectMake(0, 0,KScreenW, 219)];
                banner.models = newArray;
                banner.delegate = self;
                banner.tag = 1;
                banner;
            });
            [cell0.imageBgView addSubview:banner];
            
            return cell0;
        }
            break;
            //热门直播
        case 1:
        {
            VipHotLiveTableViewCell *cell1 = [[VipHotLiveTableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier1];
//            cell1.backgroundColor = [UIColor redColor];
           
//            NSString *filePath = [[NSBundle mainBundle] pathForResource:@"DataPropertyList" ofType:@"plist"];
//            NSArray *arr = [NSArray arrayWithContentsOfFile:filePath];
//            NSMutableArray *models = @[].mutableCopy;
//            for (NSDictionary *dic in arr) {
//                MGBannerModel *model = [MGBannerModel new];
//                [model setValuesForKeysWithDictionary:dic];
//                [models addObject:model];
//            }
            //                cell4.textLabel.text = @"asdfasdfasfd";
            
            NSMutableArray *models = @[].mutableCopy;
            for (CourseLiveListsModel *dic in self.courseLiveArray) {
                MGBannerModel *model = [MGBannerModel new];
                model.title = dic.name;
                model.picture = dic.publicity_cover;
                model.date = dic.start_date;
                model.status = dic.live_status.integerValue;
                //                [model setValuesForKeysWithDictionary:dic];
                [models addObject:model];
            }
            
            GMBannerView *banner = ({
                
                //                    CGFloat width = self.view.bounds.size.width;
                GMBannerView *banner = [[GMBannerView alloc] initWithFrame:CGRectMake(0, 0,KScreenW, 255)];
                banner.models = models;
                banner.delegate = self;
                banner.tag = 2;
                banner;
            });
            [cell1.imageBgViews addSubview:banner];
            
            return cell1;
        }
            break;
            //课程热门咨询
        case 2:
        {
            VipHotZXTableViewCell *cell2 = [[VipHotZXTableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier2];
//                            cell2.backgroundColor = [UIColor blueColor];
            cell2.selectionStyle = UITableViewCellSelectionStyleBlue;
            VIPHotZXModel *zxModel = [self.VIPHotZXArray objectAtIndex:indexPath.row];
            [cell2.headImg sd_setImageWithURL:[NSURL URLWithString:zxModel.manag_picture]];
            cell2.headImg.sd_cornerRadius = [NSNumber numberWithInteger:10];
            cell2.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell2;
        }
            break;
            //课程最新课程
        case 3:
        {
            VipNewCourseTableViewCell *cell3 = [[VipNewCourseTableViewCell alloc]initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:cellIdentifier3];
            //                cell3.backgroundColor = [UIColor purpleColor];
            cell3.selectionStyle = UITableViewCellSelectionStyleNone;
          
            VIPNewCourseModel *vipNewCM = [self.VIPNewCourseArray objectAtIndex:indexPath.row];
            cell3.titleLabel.text = vipNewCM.course_name;
            cell3.xPriceLabel.text = vipNewCM.course_under_price;
            cell3.priceLabel.text = vipNewCM.course_price;
//            cell3.peoplesLabel.text = vipNewCM.
            cell3.headImgView.sd_cornerRadius = [NSNumber numberWithInt:10];
            [cell3.headImgView sd_setImageWithURL:[NSURL URLWithString:vipNewCM.course_list_cover] placeholderImage:nil];
            if ([vipNewCM.member_is_free isEqualToString:@"1"]) {
                cell3.mianfeiLabel.text = @"vip免费";
            }else {
                cell3.mianfeiLabel.hidden = YES;
                cell3.titleLabel.sd_layout
                .leftSpaceToView(cell3.headImgView, 17)
                .widthIs(200).heightIs(15);
            }
            cell3.detailLabel.text = vipNewCM.brief;
            return cell3;
        }
            break;
            //课程育朵精选
        case 4:
        {
            YDJXTableViewCell *cell4 = [[YDJXTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier4];
            VIPYDSeletModel *selectModel = [self.VIPYDSseletArray objectAtIndex:indexPath.row];
            
            cell4.titleLabel.text = selectModel.article_title;
            cell4.detailLabel.text = selectModel.brief;
            cell4.zanLabel.text = [selectModel.thumbs_up stringByAppendingString:@"人"];
            cell4.mianfeiLabel.text = @"免费";
            cell4.selectionStyle = UITableViewCellSelectionStyleNone;
            cell4.zanLabel.text = selectModel.article_vrows;
            [cell4.headImgView sd_setImageWithURL:[NSURL URLWithString:selectModel.article_cover]];
            if ([selectModel.article_member_is_free isEqualToString:@"1"]) {
                cell4.mianfeiLabel.text = @"vip免费";
            }else {
                cell4.mianfeiLabel.hidden = YES;
                cell4.titleLabel.sd_layout.leftSpaceToView(cell4.headImgView, 17);
            }
            cell4.priceLabel.text = selectModel.article_price;
            cell4.selectionStyle = UITableViewCellSelectionStyleNone;
            
            return cell4;
        }
            break;
            //热门直播
    }
    return nil;
}

- (void)HZBannerView:(HZBannerView *)bannerView didSelectedAt:(NSInteger)index {
    if (index-2 >= self.VIPNewCourseArray.count) {
        return;
    }
    VIPNewCourseModel *mol = [self.VIPNewCourseArray objectAtIndex:index-2];
    CoureDetailViewController *coureVC = [[CoureDetailViewController alloc]init];
    coureVC.courseDetailIDString = mol.course_id;
    coureVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:coureVC animated:YES];
}

- (void)HZBannerViewTest:(GMBannerView *)bannerView didSelectedAt:(NSInteger)indexGM {
    NewZhiboDetailViewController * zhibo = [[NewZhiboDetailViewController alloc] init];
    [self.navigationController pushViewController:zhibo animated:NO];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    switch (indexPath.section) {
        case 2:
        {
            ZXDetailViewController *detail = [[ZXDetailViewController alloc]init];
            VIPHotZXModel *mol = [self.VIPHotZXArray objectAtIndex:indexPath.row];
            detail.zxtimeString = mol.manag_time;
            detail.zxtitleString = mol.manag_title;
            detail.zxIDString = mol.manag_id;
            detail.likeNumber = mol.thumbsup;
            [self.navigationController pushViewController:detail animated:YES];
        }
            break;
        case 3:
           {
               VIPNewCourseModel *vip = [self.VIPNewCourseArray objectAtIndex:indexPath.row];
               
               CoureDetailViewController *detailVC = [[CoureDetailViewController alloc]init];
               detailVC.courseDetailIDString = vip.course_id;
               
               NSLog(@"qwerqwerqwerwerwqerqwer--%@",vip.course_id);
               [self.navigationController pushViewController:detailVC animated:NO];
           }
             break;
            case 4:
        {
            VIPYDSeletModel *mol = [self.VIPYDSseletArray objectAtIndex:indexPath.row];
            NewYuDuoChoiceDetailViewController *jxVC = [[NewYuDuoChoiceDetailViewController alloc]init];
            jxVC.articleStringID = mol.article_id;
            jxVC.articleTitle = mol.article_title;
            [self.navigationController pushViewController:jxVC animated:YES];
        }
            
            break;
            
        default:
            break;
    }
}
- (void)vipButton {
    NSLog(@"加入会员");
    OpenViperViewController *openVC = [[OpenViperViewController alloc]init];
    
//    [self.navigationController pushViewController:joinVC animated:YES];
    openVC.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:openVC animated:YES completion:nil];
}
-(void)dealloc {
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}
#pragma mark 返回行数
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
        
        switch (section) {
            case 0:
                return 1;
                break;
            case 1:
                return 1;
                break;
            case 2:
                return self.VIPHotZXArray.count;
                break;
            case 3:
              
                return self.VIPNewCourseArray.count;
                break;
            case 4:
                return self.VIPYDSseletArray.count;
            default:
                break;
        }
        return 1;
    }
#pragma mark 返回行高
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
        
        switch (indexPath.section) {
            case 0:
                return 259;
                break;
            case 1:
                return 234;
            case 2:
                return 90;
            case 3:
                return 152;
            case 4:
                return 110;
                break;
        }
        return 0;
    }
#pragma mark 自定义section
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section

{

    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenW, 0)];
//        view.backgroundColor = [UIColor cyanColor];
    //    [view setBackgroundColor:[UIColor clearColor]];
    
    //各个模块
    UILabel *sectionTitle = [[UILabel alloc] init];
    sectionTitle.frame = CGRectMake(16,0,90,17);
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:[self.sectionTitle objectAtIndex:section] attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Medium" size: 18],NSForegroundColorAttributeName: [UIColor colorWithRed:23/255.0 green:151/255.0 blue:164/255.0 alpha:1.0]}];
    sectionTitle.attributedText = string;
    [view addSubview:sectionTitle];
    
    //更多
    UIButton *gdButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [gdButton setTitle:@"更多" forState:UIControlStateNormal];
    [gdButton addTarget:self action:@selector(moreClick:) forControlEvents:UIControlEventTouchUpInside];
    [gdButton setTitleColor:GMlightGrayColor forState:UIControlStateNormal];
    [gdButton.titleLabel setFont:[UIFont systemFontOfSize:14]];
    gdButton.tag = section;
    [view addSubview:gdButton];
    gdButton.sd_layout.topSpaceToView(view, 0)
    .rightSpaceToView(view, 0 )
    .widthIs(100).heightIs(16);
    
    UIButton *moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [moreButton setBackgroundImage:[UIImage imageNamed:@"ico_next"] forState:UIControlStateNormal];
    [view addSubview:moreButton];
    moreButton.sd_layout.topSpaceToView(view, 0)
    .rightSpaceToView(view, 15)
    .widthIs(8).heightIs(14);
    if (section == 4) {
        gdButton.hidden = YES;
        moreButton.hidden = YES;
    }
    return view;
}

#pragma mark 更多

- (void)moreClick:(UIButton *)send {
    NSLog(@"点击更多 %ld",(long)send.tag);
    switch (send.tag) {
        case 0:
            {
                NSLog(@"----vip最热课程-");
                CourseListViewController *detailVC = [[CourseListViewController alloc]init];
                [detailVC setHidesBottomBarWhenPushed:YES];
                [self.navigationController pushViewController:detailVC animated:YES];
                
            }
            break;
        case 1:
        {
            NSLog(@"----vip热门直播-");
            HomeLiveViewController *homeLiveVC = [[HomeLiveViewController alloc]init];
            [homeLiveVC setHidesBottomBarWhenPushed:YES];
            [self.navigationController pushViewController:homeLiveVC animated:YES];
        }
            break;
        case 2:
        {
            NSLog(@"----vip热门咨询-");
            HomeZXViewController *zxVC = [[HomeZXViewController alloc]init];
            [zxVC setHidesBottomBarWhenPushed:YES];
            [self.navigationController pushViewController:zxVC animated:YES];
        }
            break;
        case 3:
        {
            NSLog(@"----vip最新课程-");
            CourseListViewController *newDetail = [[CourseListViewController alloc]init];
            [newDetail setHidesBottomBarWhenPushed:YES];
            [self.navigationController pushViewController:newDetail animated:YES];
        }
            break;
            
        default:
            break;
    }
}


-(NSArray<NSString *> *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    
    return nil;
}

- (void)clickImage {
    //选择图片
    NSLog(@"xxxxxxx%ld",self.selectIndex);
}

- (void)changeNavigationssss {
    self.navigationController.navigationBar.barTintColor = GMWhiteColor;//导航栏背景颜色
    self.navigationController.title = @"";
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:18], NSForegroundColorAttributeName : RGB(153, 153, 153)}];
}
#pragma mark--VIP最热课程
- (void)getHotCourse {
    
}
#pragma mark-VIP最新课程
- (void)getNewCourse {
    self.VIPNewCourseArray  = [NSMutableArray array];
//    KHomeHotCourse, KVIPNewCourse
    [GMAfnTools PostHttpDataWithUrlStr:KHomeHotCourse Dic:nil SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"-------vip最新课程%@",responseObject);
        for (NSMutableDictionary *VipNewDic in responseObject) {
            VIPNewCourseModel *mol = [[VIPNewCourseModel alloc]init];
            [mol mj_setKeyValues:VipNewDic];
            
            [self.VIPNewCourseArray addObject:mol];
        }
        [self.vipTableView reloadData];
    } FailureBlock:^(id  _Nonnull error) {
        
    }];
    
}
#pragma mark--育朵精选
- (void)getYDSelectData {
    self.VIPYDSseletArray = [NSMutableArray array];
    [GMAfnTools PostHttpDataWithUrlStr:KVIPYDSelect Dic:nil SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"xxxxxxxxxxx---育朵精选%@",responseObject);
        for (NSMutableDictionary *VipSelect in responseObject) {
            VIPYDSeletModel *mol = [[VIPYDSeletModel alloc]init];
            [mol mj_setKeyValues:VipSelect];
            [self.VIPYDSseletArray addObject:mol];
        }
        [self.vipTableView reloadData];
    } FailureBlock:^(id  _Nonnull error) {
        
    }];
}
#pragma mark-VIP热门咨询
- (void)getVIPHotZX {
    self.VIPHotZXArray  = [NSMutableArray array];
    [GMAfnTools PostHttpDataWithUrlStr:KVIPHotZX Dic:nil SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"-------vip热门资讯%@",responseObject);
        for (NSMutableDictionary *VipNewDic in responseObject) {
            VIPHotZXModel *mol = [[VIPHotZXModel alloc]init];
            [mol mj_setKeyValues:VipNewDic];
            
            [self.VIPHotZXArray addObject:mol];
            
        }
        [self.vipTableView reloadData];
    } FailureBlock:^(id  _Nonnull error) {
        
    }];
    
}
#pragma mark -- 推荐直播
- (void)getCourseLiveData {
    self.courseLiveArray = [NSMutableArray array];
    [GMAfnTools PostHttpDataWithUrlStr:KCourseLiveList Dic:nil SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"----课程---推荐直播--%@",responseObject);
        for (NSMutableDictionary *liveDic in responseObject) {
            CourseLiveListsModel *mol = [[CourseLiveListsModel alloc]init];
            [mol mj_setKeyValues:liveDic];
            
            [self.courseLiveArray addObject:mol];
        }
        [self.vipTableView reloadData];
    } FailureBlock:^(id  _Nonnull error) {
        NSLog(@"-+++++%@",error);
    }];
}
@end
