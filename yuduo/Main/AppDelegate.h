//
//  AppDelegate.h
//  yuduo
// BUG
/*
 
 */
//  Created by Mac on 2019/7/23.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CustomPlayerAudioView;
@protocol SXWWeChatDelegate <NSObject>

-(void)loginSuccessByCode:(NSString *)code;
-(void)shareSuccessByCode:(int) code;

@end

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, weak) id<SXWWeChatDelegate> sxwWeChatDelegate;
@property (nonatomic, strong) CustomPlayerAudioView *audioPlayerView;

@end

