//
//  BaseViewController.m
//  yuduo
//
//  Created by Mac on 2019/7/23.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()<UIGestureRecognizerDelegate>

@end

@implementation BaseViewController
#pragma mark 隐藏导航栏分割线



- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    UIImageView *tmp=[self findNavBarBottomLine:self.navigationController.navigationBar];
    tmp.hidden=YES;
//    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@""]forBarPosition:UIBarPositionAny barMetrics:UIBarMetricsDefault];
//    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
//
}
#pragma mark 修改t导航栏
- (void)changeNavigation {
    self.navigationController.navigationBar.barTintColor = GMWhiteColor;//导航栏背景颜色
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:18], NSForegroundColorAttributeName : RGB(153, 153, 153)}];
    /**隐藏导航栏 返回文字*/
    UIBarButtonItem *item = [UIBarButtonItem appearance];
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:0.00001],
        NSForegroundColorAttributeName:[UIColor clearColor]};
    [item setTitleTextAttributes:attributes forState:UIControlStateNormal];
    
    /* ----------------------  ------------------ - - -- - - -- */
    
    
    UIButton *searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [searchBtn setBackgroundImage:[UIImage imageNamed:@"ico_search"] forState:UIControlStateNormal];
    searchBtn.frame = CGRectMake(0, 0, 21, 20);
    [searchBtn addTarget:self action:@selector(searchClick) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *rightBtn = [[UIBarButtonItem alloc]initWithCustomView:searchBtn];
    self.navigationItem.rightBarButtonItem = rightBtn;
    
    
}
- (void)handleNavigationTransition:(UIPanGestureRecognizer *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
/**
 *  左滑返回手势
 */
- (void)axcBaseOpenTheLeftBackSkip{
    // 获取系统自带滑动手势的target对象.
    id target = self.navigationController.interactivePopGestureRecognizer.delegate;
    // 创建全屏滑动手势，调用系统自带滑动手势的target的action方法.
    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:target action:@selector(handleNavigationTransition:)];
    // 设置手势代理，拦截手势触发.
    pan.delegate = self;
    // 给导航控制器的view添加全屏滑动手势.
    [self.view addGestureRecognizer:pan];
    // 禁止使用系统自带的滑动手势.
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    // 注意：只有非根控制器才有滑动返回功能，根控制器没有。
    // 判断导航控制器是否只有一个子控制器，如果只有一个子控制器，肯定是根控制器
    if (self.childViewControllers.count == 1) {
        // 表示用户在根控制器界面，就不需要触发滑动手势
        return NO;
    }
    return YES;
}
/**
 *  返回方法
 *
 */

- (void)viewDidLoad {
    [super viewDidLoad];
//    [self changeNavigation];
//    [self axcBaseOpenTheLeftBackSkip];
    // Do any additional setup after loading the view.
}
- (UIImageView *)findNavBarBottomLine:(UIView *)view{
    if ([view isKindOfClass:[UIImageView class]]&&view.bounds.size.height<1) {
        return (UIImageView *)view;
    }
    for (UIView *subView in view.subviews) {
        UIImageView *imageView=[self findNavBarBottomLine:subView];
        if (imageView) {
            return imageView;
        }
    }
    return nil;
}
/**
 override
 返回方法
 */
-(void)popViewController{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)setLeftBarItemWithString:(NSString*)string
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    /**
     *  设置frame只能控制按钮的大小
     */
    btn.frame= CGRectMake(0, 0, 44, 44);
    [btn setTitle:string  forState:UIControlStateNormal];
    [btn setTitleColor:GMBlackColor forState:UIControlStateNormal];
    btn.titleLabel.font = kFont(17);
    [btn addTarget:self action:@selector(leftBarItemAction:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *btn_right = [[UIBarButtonItem alloc] initWithCustomView:btn];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    /**
     *  width为负数时，相当于btn向右移动width数值个像素，由于按钮本身和边界间距为5pix，所以width设为-5时，间距正好调整
     *  为0；width为正数时，正好相反，相当于往左移动width数值个像素
     */
    negativeSpacer.width = -20;
    self.navigationItem.leftBarButtonItems = [NSArray arrayWithObjects:negativeSpacer, btn_right, nil];
}
- (void)setLeftBarItemWithImage:(NSString *)imageName
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    /**
     *  设置frame只能控制按钮的大小
     */
    btn.frame= CGRectMake(0, 0, 25, 25);
    [btn setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    //
    //btn.imageView.contentMode = UIViewContentModeScaleAspectFill;
    [btn addTarget:self action:@selector(leftBarItemAction:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *btn_right = [[UIBarButtonItem alloc] initWithCustomView:btn];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    /**
     *  width为负数时，相当于btn向右移动width数值个像素，由于按钮本身和边界间距为5pix，所以width设为-5时，间距正好调整
     *  为0；width为正数时，正好相反，相当于往左移动width数值个像素
     */
    //negativeSpacer.width = -25;
    self.navigationItem.leftBarButtonItems = [NSArray arrayWithObjects:negativeSpacer, btn_right, nil];
}
- (void)setRightBarItemWithString:(NSString*)string
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    /**
     *  设置frame只能控制按钮的大小
     */
    
    if (string.length <= 2) {
        btn.frame= CGRectMake(0, 0, 40, 44);
    }else{
        btn.frame= CGRectMake(0, 0, 60, 44);
    }
    //btn.frame= CGRectMake(0, 0, 40, 44);
    [btn setTitle:string  forState:UIControlStateNormal];
    btn.titleLabel.font = kFont(15);
    
    [btn addTarget:self action:@selector(rightBarItemAction:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *btn_right = [[UIBarButtonItem alloc] initWithCustomView:btn];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    /**
     *  width为负数时，相当于btn向右移动width数值个像素，由于按钮本身和边界间距为5pix，所以width设为-5时，间距正好调整
     *  为0；width为正数时，正好相反，相当于往左移动width数值个像素
     */
    if (string.length <= 2) {
        negativeSpacer.width = -5;
    }else{
        negativeSpacer.width = -5;
    }
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:negativeSpacer, btn_right, nil];
}
- (void)setRightBarItemWithImage:(NSString *)imageName{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    /**
     *  设置frame只能控制按钮的大小
     */
    btn.frame= CGRectMake(0, 0, 25, 25);
    //[btn setBackgroundImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [btn setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(rightBarItemAction:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *btn_right = [[UIBarButtonItem alloc] initWithCustomView:btn];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    /**
     *  width为负数时，相当于btn向右移动width数值个像素，由于按钮本身和边界间距为5pix，所以width设为-5时，间距正好调整
     *  为0；width为正数时，正好相反，相当于往左移动width数值个像素
     */
    //negativeSpacer.width = -20;
    
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:negativeSpacer, btn_right, nil];
}
- (void)setRightBarItemImage:(UIImage *)imgage title:(NSString *)str{
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton setFrame:CGRectMake(0, 0, 60, 25)];
    [leftButton setImage:imgage forState:UIControlStateNormal];
    [leftButton setImage:imgage forState:UIControlStateHighlighted];
    [leftButton setTitle:str forState:UIControlStateNormal];
    [leftButton setTitleColor:GMWhiteColor forState:UIControlStateNormal];
    [leftButton addTarget:self action:@selector(rightBarItemAction:)forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    
    negativeSpacer.width = -15;
    self.navigationItem.rightBarButtonItems = @[negativeSpacer,barButton];
}

#pragma mark 左右两侧NavBarItem事件相应

- (void)leftBarItemAction:(UIBarButtonItem *)gesture
{
    if(self.navigationController.viewControllers.count>1)
    {
        [self.view endEditing:YES];
        [self.navigationController popViewControllerAnimated:YES];
    }else
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}
- (void)rightBarItemAction:(UIBarButtonItem *)gesture{
}

#pragma makr - 我的页面强制登录
- (void)myDetailShowLogin:(UIViewController *)VC {
    NSUserDefaults *defaults =  [NSUserDefaults standardUserDefaults];
    LoginViewController *loginVC = [[LoginViewController alloc]init];
    
    if (![defaults objectForKey:@"token"]) {
        loginVC.modalPresentationStyle = UIModalPresentationFullScreen;
        [self presentViewController:loginVC animated:YES completion:nil];
    } else {
        [self.navigationController pushViewController:VC animated:YES];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
