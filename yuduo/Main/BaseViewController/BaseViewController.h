//
//  BaseViewController.h
//  yuduo
//
//  Created by Mac on 2019/7/23.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>
//登录页
#import "LoginViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface BaseViewController : UIViewController
/**
 *  设置左侧文字形式的BarItem
 */
- (void)setLeftBarItemWithString:(NSString*)string;
/**
 *  设置左侧图片形式的BarItem
 */
- (void)setLeftBarItemWithImage:(NSString *)imageName;
/**
 *  设置右侧文字形式的BarItem
 */
- (void)setRightBarItemWithString:(NSString*)string;
/**
 *  设置右侧图片形式的BarItem
 */
- (void)setRightBarItemWithImage:(NSString *)imageName;
/**
 *  设置右侧文字形式的BarItem
 */
- (void)setRightBarItemImage:(UIImage *)imgage title:(NSString *)str;

#pragma mark - 导航栏左右两侧点击事件
/**
 *  已内部实现常规左侧点击返回,如有必要请重写此方法
 */
- (void)leftBarItemAction:(UITapGestureRecognizer *)gesture;
/**
 *  需继承实现右侧点击事件
 */
- (void)rightBarItemAction:(UITapGestureRecognizer *)gesture;

/// 调整控制器(未登录时需要登录)
/// @param VC 要跳转的控制器
- (void)myDetailShowLogin:(UIViewController *)VC;

@end

NS_ASSUME_NONNULL_END
