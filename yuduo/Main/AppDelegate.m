//
//  AppDelegate.m
//  yuduo
//
//  Created by Mac on 2019/7/23.
//  Copyright © 2019 yaocongkeji. All rights reserved.


#import "AppDelegate.h"
#import "YDTabBarViewController.h"
#import "MySetViewController.h"
#import "WXApi.h"
#import "WXAuth.h"
#import "SystemDetailViewController.h"
//友盟推送

#import  <UMCommon/UMCommon.h>  // 公共组件是所有友盟产品的基础组件，必选

#import  <UMPush/UMessage.h>  // Push组件

#import  <UserNotifications/UserNotifications.h>// Push组件必须的系统库


@interface AppDelegate ()<WXApiDelegate,UNUserNotificationCenterDelegate>
@end
@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    //微信登录
    [WXApi registerApp:WX_App_ID];
    //友盟分享
    [UMConfigure setLogEnabled:YES];
    [UMConfigure initWithAppkey:UMAppKey channel:@"App Store"];
    self.window = [[UIWindow alloc]initWithFrame:[UIScreen mainScreen].bounds];
    YDTabBarViewController *YDtabBar = [[YDTabBarViewController alloc]init];
    self.window.rootViewController = YDtabBar;
    
    NSUserDefaults *defaults =  [NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    
    BOOL is_login = [[[NSUserDefaults standardUserDefaults] objectForKey:kIsLogin] boolValue];
    NSLog(@"+++++++++%@",[defaults objectForKey:kIsLogin]);
    MySetViewController *setVC = [[MySetViewController alloc]init];
    if (is_login) {
        [setVC creatTZ];
//        [[NSNotificationCenter defaultCenter]postNotificationName:KLogOutNotifacation object:nil];
    }else {
        //创建通知对象
        NSNotification *notification = [NSNotification notificationWithName:KLoginNotification object:nil];
        //发送通知
        [[NSNotificationCenter defaultCenter] postNotification:notification];
    }
    NSLog(@"=======token=%@",token);
    [self.window makeKeyAndVisible];
    
   /*---------友盟推送---------*/
    UMessageRegisterEntity * entity = [[UMessageRegisterEntity alloc] init];
    entity.types = UMessageAuthorizationOptionBadge|UMessageAuthorizationOptionAlert|UMessageAuthorizationOptionSound;
    //如果你期望使用交互式(只有iOS 8.0及以上有)的通知，请参考下面注释部分的初始化代码
    if (([[[UIDevice currentDevice] systemVersion]intValue]>=8)&&([[[UIDevice currentDevice] systemVersion]intValue]<10)) {
        UIMutableUserNotificationAction *action1 = [[UIMutableUserNotificationAction alloc] init];
        action1.identifier = @"action1_identifier";
        action1.title = @"打开应用";
        action1.activationMode = UIUserNotificationActivationModeForeground;//当点击的时候启动程序
        UIMutableUserNotificationAction *action2 = [[UIMutableUserNotificationAction alloc] init];  //第二按钮
        action2.identifier = @"action2_identifier";
        action2.title = @"忽略";
        action2.activationMode = UIUserNotificationActivationModeBackground;//当点击的时候不启动程序，在后台处理
        action2.authenticationRequired = YES;//需要解锁才能处理，如果action.activationMode = UIUserNotificationActivationModeForeground;则这个属性被忽略；
        action2.destructive = YES;
        UIMutableUserNotificationCategory *actionCategory1 = [[UIMutableUserNotificationCategory alloc] init];
        actionCategory1.identifier = @"category1";//这组动作的唯一标示
        [actionCategory1 setActions:@[action1,action2] forContext:(UIUserNotificationActionContextDefault)];
        NSSet *categories = [NSSet setWithObjects:actionCategory1, nil];
        entity.categories=categories;
    }
    //如果要在iOS10显示交互式的通知，必须注意实现以下代码
    if ([[[UIDevice currentDevice] systemVersion]intValue]>=10) {
        UNNotificationAction *action1_ios10 = [UNNotificationAction actionWithIdentifier:@"action1_identifier" title:@"打开应用" options:UNNotificationActionOptionForeground];
        UNNotificationAction *action2_ios10 = [UNNotificationAction actionWithIdentifier:@"action2_identifier" title:@"忽略" options:UNNotificationActionOptionForeground];
        //UNNotificationCategoryOptionNone
        //UNNotificationCategoryOptionCustomDismissAction  清除通知被触发会走通知的代理方法
        //UNNotificationCategoryOptionAllowInCarPlay       适用于行车模式
        UNNotificationCategory *category1_ios10 = [UNNotificationCategory categoryWithIdentifier:@"category1" actions:@[action1_ios10,action2_ios10]   intentIdentifiers:@[] options:UNNotificationCategoryOptionCustomDismissAction];
        NSSet *categories = [NSSet setWithObjects:category1_ios10, nil];
        entity.categories=categories;
    }
    [UNUserNotificationCenter currentNotificationCenter].delegate=self;
    [UMessage registerForRemoteNotificationsWithLaunchOptions:launchOptions Entity:entity completionHandler:^(BOOL granted, NSError * _Nullable error) {
        if (granted) {
            
        }else{
        }
    }];
    
//    [self UMPush:launchOptions];
    return YES;
}
#pragma mark--获取deviceTopken
- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    if (![deviceToken isKindOfClass:[NSData class]]) return;
    const unsigned *tokenBytes = (const unsigned *)[deviceToken bytes];
    NSString *hexToken = [NSString stringWithFormat:@"%08x%08x%08x%08x%08x%08x%08x%08x",
                          ntohl(tokenBytes[0]), ntohl(tokenBytes[1]), ntohl(tokenBytes[2]),
                          ntohl(tokenBytes[3]), ntohl(tokenBytes[4]), ntohl(tokenBytes[5]),
                          ntohl(tokenBytes[6]), ntohl(tokenBytes[7])];
    NSLog(@"-----友盟deviceToken:%@",hexToken);
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *user_id = [defaults objectForKey:@"user_id"];
    NSString *token = [defaults objectForKey:@"token"];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"user_id"] = user_id;
    dic[@"token"] = token;
    dic[@"device_token"] = hexToken;
    dic[@"android_ios_id"] = DIV_UUID;
    dic[@"type"] = @"2";
    NSLog(@"-------推送---%@",dic);
    [GMAfnTools PostHttpDataWithUrlStr:KDevice_UUIDURL Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"打印token ---=--%@",responseObject);
    } FailureBlock:^(id  _Nonnull error) {
        NSLog(@"上传错误===%@",error);
    }];
}

//iOS10新增：处理前台收到通知的代理方法
-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler{
    NSDictionary * userInfo = notification.request.content.userInfo;
    if([notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        //应用处于前台时的远程推送接受
        //关闭U-Push自带的弹出框
        [UMessage setAutoAlert:NO];
        //必须加这句代码
        [UMessage didReceiveRemoteNotification:userInfo];
     
        
    }else{
        //应用处于前台时的本地推送接受
    }
    //当应用处于前台时提示设置，需要哪个可以设置哪一个
    completionHandler(UNNotificationPresentationOptionSound|UNNotificationPresentationOptionBadge|UNNotificationPresentationOptionAlert);
}

-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)())completionHandler {NSDictionary * userInfo = response.notification.request.content.userInfo;if([response.notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
//    应用处于后台时的远程推送接受
    [UMessage didReceiveRemoteNotification:userInfo];
    NSLog(@"远程推送的消息来了%@",userInfo);
    if ([userInfo[@"type"] isEqualToString:@"workflow"])
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"notifyPresnetNotice" object:userInfo];}
    }
    else{
        //应用处于后台时的本地推送接受
        NSLog(@"本地后台推送的消息来了%@",userInfo);
        completionHandler();
    }
}
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    
    return [WXAUTH handleOpenURL:url];
    
}

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options{
    
    return [WXAUTH handleOpenURL:url];
    
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
    
    return [WXAUTH handleOpenURL:url];
    
}


//- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
//
//{
//    
//    [UMessage registerDeviceToken:deviceToken];
//    
//    NSLog(@"didRegisterForRemoteNotificationsWithDeviceToken success");
//    
//    NSLog(@"deviceToken————>>>%@",[[[[deviceToken description] stringByReplacingOccurrencesOfString: @"<"withString: @""]
//                                    
//                                    stringByReplacingOccurrencesOfString: @">"withString: @""]
//                                   
//                                   stringByReplacingOccurrencesOfString: @" "withString: @""]);
//    
//}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
