//
//  YDNavigationController.m
//  yuduo
//
//  Created by Mac on 2019/7/23.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "YDNavigationController.h"
#import "UIView+FrameTool.h"
#import "CustomPlayerAudioView.h"
@interface YDNavigationController ()<UINavigationControllerDelegate>

@end

@implementation YDNavigationController

- (void)viewDidLoad {
    //设置了NO之后View自动下沉navigationBar的高度
       self.navigationBar.translucent = NO;
    [super viewDidLoad];
    self.navigationBar.barStyle = UIBarStyleDefault;
    self.navigationBar.tintColor = GMBlackColor;
    NSDictionary * homedict = [NSDictionary dictionaryWithObject:kFont(17) forKey:UITextAttributeFont];
    self.navigationBar.titleTextAttributes = homedict;
    self.delegate = self;
}
-(void)dealloc{
    NSLog(@"界面内存已释放!");
}
-(void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated{
    if (viewController != nil) {
        if (self.viewControllers.count == 1) {
            viewController.hidesBottomBarWhenPushed = YES;
        }
        [super pushViewController:viewController animated:animated];
        if (self.viewControllers.count > 1) {
            [CustomPlayerAudioView shareInstance].hidden = YES;
        }
    }
}
-(UIViewController *)popViewControllerAnimated:(BOOL)animated{
    [CustomPlayerAudioView shareInstance].hidden = NO;
    return [super popViewControllerAnimated:animated];
}
-(NSArray<UIViewController *> *)popToRootViewControllerAnimated:(BOOL)animated{
    [CustomPlayerAudioView shareInstance].hidden = NO;
    return [super popToRootViewControllerAnimated:animated];
}
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    // 删除系统自带的tabBarButton
    int index = 0;
    CGFloat wigth = KScreenW / 5;
    for (UIView *tabBar in self.tabBarController.tabBar.subviews) {
        if ([tabBar isKindOfClass:NSClassFromString(@"UITabBarButton")]) {
            tabBar.x = index * wigth;
            tabBar.width = wigth;
            index++;
            if (index == 2) {
                index++;
            }
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
