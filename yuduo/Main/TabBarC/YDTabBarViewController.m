//
//  YDTabBarViewController.m
//  yuduo
//
//  Created by Mac on 2019/7/24.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "YDTabBarViewController.h"
#import "HomeViewcontroller.h"
#import "CourseViewController.h"
#import "ViperViewController.h"
#import "DiscoverViewController.h"
#import "MySetViewcontroller.h"
//导航栏
#import "YDNavigationController.h"
#import "LoginViewController.h"
@interface YDTabBarViewController ()

@property (nonatomic, weak) UIButton *selectedBtn;
@property (nonatomic, assign) NSUInteger selectItem;//选中的item
/** 红色view 用于置顶 */
@property (nonatomic, strong) UIView * redView;

/** 橘色view 用于置底 */
@property (nonatomic, strong) UIView * orangeView;

@property (nonatomic,strong)UIButton *btn;
@end

@implementation YDTabBarViewController
#define SCREEN_WIDTH  [UIScreen mainScreen].bounds.size.width
#define SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height

#define kIs_iphone (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define kIs_iPhoneX kScreenWidth >=375.0f && kScreenHeight >=812.0f&& kIs_iphone
/*状态栏高度*/
#define kStatusBarHeight (CGFloat)(kIs_iPhoneX?(44.0):(20.0))
/*导航栏高度*/
#define kNavBarHeight (44)
/*状态栏和导航栏总高度*/
#define kNavBarAndStatusBarHeight (CGFloat)(kIs_iPhoneX?(88.0):(64.0))
/*TabBar高度*/
#define kTabBarHeight (CGFloat)(kIs_iPhoneX?(49.0 + 34.0):(49.0))
/*顶部安全区域远离高度*/
#define kTopBarSafeHeight (CGFloat)(kIs_iPhoneX?(44.0):(0))
/*底部安全区域远离高度*/
#define kBottomSafeHeight (CGFloat)(kIs_iPhoneX?(34.0):(0))
/*iPhoneX的状态栏高度差值*/
#define kTopBarDifHeight (CGFloat)(kIs_iPhoneX?(24.0):(0))
/*导航条和Tabbar总高度*/
#define kNavAndTabHeight (kNavBarAndStatusBarHeight + kTabBarHeight)


#define HEIGHT_TAB_BAR         49
#define HEIGHT_BOTTOM_MARGIN   (SCREEN_HEIGHT==812?34:0) // 因为只有iPhoneX的高度为812pt

/** 设备屏幕宽度 */
#define LCLScreenWidth [[UIScreen mainScreen] bounds].size.width

/** 设备屏幕高度 */
#define LCLScreenHeight [[UIScreen mainScreen] bounds].size.height

/** iPhoneX判断 */
#define LCLIsIphoneX (CGSizeEqualToSize(CGSizeMake(375.f, 812.f), [UIScreen mainScreen].bounds.size) || CGSizeEqualToSize(CGSizeMake(812.f, 375.f), [UIScreen mainScreen].bounds.size))

/** 状态栏高度 */
#define LCL_StatusBar_Height ((LCLIsIphoneX) ? 44 : 20)

/** 导航栏高度 */
#define LCL_NavBar_Height ((LCLIsIphoneX) ? 88 : 64)

/** 标签栏高度 */
#define LCL_TabBar_Height ((LCLIsIphoneX) ? 83 : 49)

/** 底部横条高度 */
#define LCL_HomeIndicator_Height ((LCLIsIphoneX) ? 34 : 0)
-(BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event
{
    if (CGRectContainsPoint(_btn.frame, point)) {
        return YES;
    }
    return NO;
}
//重写这个方法，在跳转后自动隐藏tabbar

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = GMWhiteColor;
    
    // 取消tabbar分割线
    CGRect rect = CGRectMake(0, 0, KScreenW, KScreenH);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [[UIColor clearColor] CGColor]);
    CGContextFillRect(context, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [self.tabBar setBackgroundImage:img];
    [self.tabBar setShadowImage:img];
    self.selectedIndex = 0;
    
    //    [self setupTabar];
    //安全区域
    UIView *bgView = [[UIView alloc] init];
    self.tabBar.frame = CGRectMake(0, 0, self.view.frame.size.width, 100);
    
    UIImageView *imx = [[UIImageView alloc]init];
    imx.image = [UIImage imageNamed:@"tabbarIcon1"];
    imx.frame = CGRectMake(0, -30, self.view.frame.size.width, 100);
    [bgView addSubview:imx];
    
    bgView.frame = self.tabBar.bounds;
    bgView.backgroundColor = [UIColor whiteColor];
    [[UITabBar appearance]insertSubview:bgView atIndex:0];
    
    _btn   = [UIButton buttonWithType:UIButtonTypeCustom];
    _btn.frame = CGRectMake(self.view.frame.size.width/2 -25, 10, 50, 50);
    _btn.layer.cornerRadius = 25;
    [_btn setImage:[UIImage imageNamed:@"viper"] forState:UIControlStateNormal];
    [_btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    [imx addSubview:_btn];
    UILabel *labelViper = [[UILabel alloc]initWithFrame:CGRectMake(_btn.frame.origin.x+25/2, _btn.frame.size.height+10, 25, 25)];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"VIP" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Medium" size: 14.1],NSForegroundColorAttributeName: [UIColor colorWithRed:21/255.0 green:151/255.0 blue:164/255.0 alpha:1.0]}];
    labelViper.attributedText = string;
    [imx addSubview:labelViper];
    //    [self.tabBarItem setTitlePositionAdjustment:UIOffsetMake(0,y)];
    //image上移
    //    vc.tabBarItem.imageInsets = UIEdgeInsetsMake(x, 0, w, 0);
    // 此处是自定义的View的设置 如果使用了约束 可以不需要设置下面,_bottomView的frame
    [self addChildViewController];

}

#pragma mark-- mark vip点击事件
- (void)btnClick:(UIButton *)sender {
   
}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {
    self.selectedIndex = tabBarController.selectedIndex;
    NSLog(@"%ld",tabBarController.tabBarItem.tag);
}

-(void)addChildViewController
{
    [self addChildVC:[HomeViewController new] andTitle:@"首页" andImageName:@"home" selectImageName:@"index"];
    [self addChildVC:[CourseViewController new] andTitle:@"课程" andImageName:@"kx-icon" selectImageName:@"kc-xz-icon"];
    [self addChildVC:[ViperViewController new] andTitle:@"" andImageName:@"" selectImageName:@""];
    [self addChildVC:[DiscoverViewController new] andTitle:@"发现" andImageName:@"-home-fx-icon" selectImageName:@"home-fx"];
    [self addChildVC:[MySetViewController new] andTitle:@"我的" andImageName:@"home-wd-icon" selectImageName:@"wd-xz"];
    
}

-(void)addChildVC:(UIViewController *)childVC andTitle:(NSString *)title andImageName:(NSString *)name selectImageName:(NSString *)selectImageName
{
    childVC.tabBarItem.image = [UIImage imageNamed:name];
    childVC.tabBarItem.selectedImage = [[UIImage imageNamed:selectImageName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    childVC.title = title;
    
    NSDictionary *dictHome = [NSDictionary dictionaryWithObject:[UIColor colorWithRed:21/255.0 green:151/255.0 blue:164/255.0 alpha:1.0] forKey:NSForegroundColorAttributeName];
    [childVC.tabBarItem setTitleTextAttributes:dictHome forState:UIControlStateSelected];
    YDNavigationController *navigation = [[YDNavigationController alloc]initWithRootViewController:childVC];
    [self addChildViewController:navigation];
}
- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item{
    NSLog(@"item name = %@", item.title);
//    if ([item.title isEqualToString:@""]) {
//        NSLog(@"hahahaha");
//        
//        NSUserDefaults *defaulsts = [NSUserDefaults standardUserDefaults];
//        if (![defaulsts objectForKey:@"token"]) {
//            LoginViewController *loginVC = [[LoginViewController alloc]init];
//            [self presentViewController:loginVC animated:YES completion:nil];
//        }else {
//            [self addChildVC:[ViperViewController new] andTitle:@"" andImageName:@"" selectImageName:@""];
//            
//        }
//    }
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


@end
