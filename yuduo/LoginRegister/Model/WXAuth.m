//
//  WXAuth.m
//  hongyantub2b
//
//  Created by Apple on 2018/8/23.
//  Copyright © 2018年 Apple. All rights reserved.
//

#import "WXAuth.h"
#import "WXApi.h"
#import "WXLoginModel.h"
//绑定手机号
#import "Bind_MobileViewController.h"
#define DEFINE_SHARED_INSTANCE_USING_BLOCK(block) \
static dispatch_once_t pred = 0; \
static id _sharedObject = nil; \
dispatch_once(&pred, ^{ \
_sharedObject = block(); \
}); \
return _sharedObject; \

@interface WXAuth ()<WXApiDelegate>
PropertyStrong(NSMutableArray, infoArray);
PropertyStrong(WXLoginModel, loginModel);
@end

@implementation WXAuth

+ (WXAuth *)sharedInstance{
    
    DEFINE_SHARED_INSTANCE_USING_BLOCK(^{
        
        return [[self alloc] init];
    });
}

- (id)init{
    
    self = [super init];
    if (self) {
        
    }
    return self;
}


- (void)sendWXAuthReq{
    
    if([WXApi isWXAppInstalled]){//判断用户是否已安装微信App
        
        SendAuthReq *req = [[SendAuthReq alloc] init];
        req.state = @"wx_oauth_authorization_state";//用于保持请求和回调的状态，授权请求或原样带回
        req.scope = @"snsapi_userinfo";//授权作用域：获取用户个人信息
        [WXApi sendReq:req];
        
    }else{
        
        NSLog(@"未安装微信应用或版本过低");
    }
}

- (BOOL)handleOpenURL:(NSURL *)url{
    
    //处理回调
    if([url.host isEqualToString:@"platformId=wechat"] || [url.host isEqualToString:@"oauth"]){//微信WeChat分享回调
        
        return [WXApi handleOpenURL:url delegate:self];
    }else{
        
        return NO;
    }
}

/**
 Delegate微信回调方法
 */
- (void)onResp:(id)resp{
    self.infoArray = [NSMutableArray array];
    if([resp isKindOfClass:[SendAuthResp class]]){//判断是否为授权登录类
        
        SendAuthResp *req = (SendAuthResp *)resp;
        if([req.state isEqualToString:@"wx_oauth_authorization_state"]){//微信授权成功
            NSLog(@"微信授权成功");
            if(req.errCode == 0){
                self.codeBlock(req.code);
                NSLog(@"获取code：%@", req.code);
//
//                NSMutableDictionary *codeParms = [NSMutableDictionary dictionary];
//                codeParms[@"code"] = req.code;
//
//                NSLog(@"获取code成功-----%@",req.code);
//                [GMAfnTools PostHttpDataWithUrlStr:KWXAcessTOKEN Dic:codeParms SuccessBlock:^(id  _Nonnull responseObject) {
//                    NSLog(@"========获取AccessToken= %@",responseObject);
//                    self.loginModel = [[WXLoginModel alloc]init];
//                    self.loginModel.is_body_info = [responseObject objectForKey:@"is_body_info"];
//                    self.loginModel.user_id = [responseObject objectForKey:@"user_id"];
//                    self.loginModel.is_bind_mobile = [responseObject objectForKey:@"is_bind_mobile"];
//                    self.loginModel.token = [responseObject objectForKey:@"token"];
//
//                } FailureBlock:^(id  _Nonnull error) {
//                    NSLog(@"xxxxxxx获取失败%@",error);
//                }];
////
//                NSMutableDictionary *dic = [NSMutableDictionary dictionary];
//                dic[@"appid"] = WX_App_ID;
//                dic[@"secret"] = WX_App_Secret;
//                dic[@"code"] = req.code;
//
//                 NSString *url =[NSString stringWithFormat:@"https://api.weixin.qq.com/sns/oauth2/access_token?appid=%@&secret=%@&code=%@&grant_type=authorization_code",WX_App_ID,WX_App_Secret,req.code];
//                [GMAfnTools GetHttpDataWithUrlStr:url Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
//                    NSLog(@"========获取AccessToken 成功= %@",responseObject);
//                    if (responseObject) {
//
//                        [self sendWXAccess:[responseObject objectForKey:WX_ACCESS_TOKEN] openid:[responseObject objectForKey:WX_OPEN_ID]];
//                    }
//
//                } FailureBlock:^(id  _Nonnull error) {
//
//                    NSLog(@"++++++++获取失败%@",error);
//
//                }];
//
//                req.code;
            }
        }
    }
}

- (void)sendWXAccess:(NSString *)access_token openid:(NSString *)openid {
    
    
//    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
//    dic[@"access_token"] = access_token;
//    dic[@"openid"] = openid;
//    NSString *url = [NSString stringWithFormat:@"https://api.weixin.qq.com/sns/userinfo?access_token=%@&openid=%@",access_token,openid];
//    [GMAfnTools GetHttpDataWithUrlStr:url Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
//        NSLog(@"========获取info 成功= %@",responseObject);
//    } FailureBlock:^(id  _Nonnull error) {
//
//        NSLog(@"++++++++获取失败%@",error);
//
//    }];
    
    
}

@end
