//
//  YDUserModel.h
//  yuduo
//
//  Created by Mac on 2019/8/28.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface YDUserModel : NSObject
@property(nonatomic, copy) NSString  *user_id;
@property(nonatomic, copy) NSString  *is_bind_mobile;
@property(nonatomic, copy) NSString  *token;
//@property(nonatomic, copy) NSString  *phone;
//@property(nonatomic, copy) NSString  *realname;
//@property(nonatomic, copy) NSString  *company;
//@property(nonatomic, copy) NSString  *job;
@end

NS_ASSUME_NONNULL_END
