//
//  SendCodeModel.h
//  yuduo
//
//  Created by Mac on 2019/8/20.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SendCodeModel : NSObject

PropertyStrong(NSString, code);
PropertyStrong(NSString, msg);

@end

NS_ASSUME_NONNULL_END
