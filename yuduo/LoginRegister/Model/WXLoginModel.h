//
//  WXLoginModel.h
//  yuduo
//
//  Created by Mac on 2019/9/2.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface WXLoginModel : BaseModel
PropertyStrong(NSString, is_body_info);
PropertyStrong(NSString, user_id);
PropertyStrong(NSString, is_bind_mobile);
PropertyStrong(NSString, token);
@end

NS_ASSUME_NONNULL_END
