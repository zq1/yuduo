//
//  RegisterView.h
//  yuduo
//
//  Created by Mac on 2019/8/8.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RegisterView : UIView
//登录
PropertyStrong(UIImageView, logoImg);//logo
PropertyStrong(UIView, phoneBgView);
PropertyStrong(UIImageView, phoneImg);
PropertyStrong(UITextField, phoneText);
//验证码

PropertyStrong(UIView, codeBgView);
PropertyStrong(UIImageView, codeImg);
PropertyStrong(UITextField, codeText);
PropertyStrong(UIButton, codeSend);

//密码
PropertyStrong(UIView, pswBgView);
PropertyStrong(UIImageView, pswImg);
PropertyStrong(UITextField, pswText);

//立即注册
PropertyStrong(UIButton, registerButton);
//注册协议
PropertyStrong(UIButton, registerProtocol);//注册协议
PropertyStrong(UIButton, registerPtlImg);//是否同意注册协议
//注册送Vip
PropertyStrong(UIButton, registerVipButton);
//按钮
@property (nonatomic ,strong) void(^registerButtonBlock)(NSInteger indexButton);
@end

NS_ASSUME_NONNULL_END
