//
//  RegisterView.m
//  yuduo
//
//  Created by Mac on 2019/8/8.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "RegisterView.h"

@implementation RegisterView

-(instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self createLayout];
    }
    return self;
}

- (void)createLayout {
    self.logoImg = [[UIImageView alloc]initWithFrame:CGRectMake(KScreenW/2-49, 112*kGMHeightScale, 98, 58)];
//    self.logoImg.backgroundColor = GMRedColor;
    self.logoImg.image = [UIImage imageNamed:@"logo"];
    [self addSubview:self.logoImg];
    
    self.phoneBgView = [[UIView alloc]init];
    self.phoneBgView.backgroundColor = GMWhiteColor;
    self.phoneBgView.layer.borderWidth = 0.5;
    self.phoneBgView.layer.borderColor = RGB(23, 153, 167).CGColor;
    self.phoneBgView.layer.cornerRadius = 22.5;
    [self addSubview:self.phoneBgView];
    self.phoneBgView.sd_layout.topSpaceToView(self.logoImg, 45)
    .leftSpaceToView(self, 27)
    .rightSpaceToView(self, 27)
    .heightIs(45);
    
    self.phoneImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"shouji"]];
    [self.phoneBgView addSubview:self.phoneImg];
    self.phoneImg.sd_layout.topSpaceToView(self.phoneBgView, 15)
    .leftSpaceToView(self.phoneBgView, 14)
    .widthIs(17).heightIs(17);
    
    self.phoneText = [[UITextField alloc]init];
    self.phoneText.placeholder = @"请输入手机号";
    self.phoneText.backgroundColor = GMWhiteColor;
    self.phoneText.layer.cornerRadius = 15;
    [self.phoneBgView addSubview:self.phoneText];
    self.phoneText.sd_layout.topSpaceToView(self.phoneBgView, 5)
    .leftSpaceToView(self.phoneImg, 8)
    .widthIs(250)
    .heightIs(35);
    
    self.codeBgView = [[UIView alloc]init];
    self.codeBgView.backgroundColor = GMWhiteColor;
    self.codeBgView.layer.borderWidth = 0.5;
    self.codeBgView.layer.borderColor = RGB(23, 153, 167).CGColor;
    self.codeBgView.layer.cornerRadius = 22.5;
    [self addSubview:self.codeBgView];
    self.codeBgView.sd_layout.topSpaceToView(self.phoneBgView, 20)
    .leftEqualToView(self.phoneBgView)
    .rightEqualToView(self.phoneBgView)
    .heightIs(45);
    
    self.codeImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"yanzhengma"]];
    [self.codeBgView addSubview:self.codeImg];
    self.codeImg.sd_layout.topSpaceToView(self.codeBgView, 14)
    .leftSpaceToView(self.codeBgView, 15)
    .widthIs(17).heightEqualToWidth();
    
    
    self.codeSend = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.codeSend addTarget:self action:@selector(registerPageButton:) forControlEvents:UIControlEventTouchUpInside];
    self.codeSend.tag = 10000;
    [self.codeSend setTitle:@"发送验证码" forState:UIControlStateNormal];
    [self.codeSend setTitleColor:RGB(23, 153, 67) forState:UIControlStateNormal];
    self.codeSend.titleLabel.font = [UIFont systemFontOfSize:14];
    [self.codeBgView addSubview:self.codeSend];
    self.codeSend.sd_layout.topSpaceToView(self.codeBgView, 16)
    .rightSpaceToView(self.codeBgView, 0)
    .widthIs(85).heightIs(16);
    
    self.codeText = [[UITextField alloc]init];
    self.codeText.placeholder = @"请输入验证码";
//    self.codeText.backgroundColor = GMlightGrayColor;
    self.codeText.layer.cornerRadius = 15;
    [self.codeBgView addSubview:self.codeText];
    self.codeText.sd_layout.topSpaceToView(self.codeBgView, 5)
    .leftSpaceToView(self.codeImg, 8)
    .rightSpaceToView(self.codeSend, 10)
    .heightIs(35);
    
    //密码
    self.pswBgView = [[UIView alloc]init];
    self.pswBgView.backgroundColor = GMWhiteColor;
    self.pswBgView.layer.borderWidth = 0.5;
    self.pswBgView.layer.borderColor = RGB(23, 153, 167).CGColor;
    self.pswBgView.layer.cornerRadius = 22.5;
    [self addSubview:self.pswBgView];
    
    self.pswBgView.sd_layout.topSpaceToView(self.codeBgView, 20)
    .leftEqualToView(self.codeBgView)
    .rightEqualToView(self.codeBgView)
    .heightIs(45);
    
    self.pswImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"psw"]];
    [self.pswBgView addSubview:self.pswImg];
    self.pswImg.sd_layout.topSpaceToView(self.pswBgView, 14)
    .leftSpaceToView(self.pswBgView, 15)
    .widthIs(17).heightIs(17);
    
    self.pswText = [[UITextField alloc]init];
    self.pswText.placeholder = @"请输入密码";
    self.pswText.backgroundColor = GMWhiteColor;
    self.pswText.layer.cornerRadius = 15;
    [self.pswBgView addSubview:self.pswText];
    self.pswText.sd_layout.topSpaceToView(self.pswBgView, 5)
    .leftSpaceToView(self.pswImg, 8)
    .widthIs(250)
    .heightIs(35);
    
    //注册按钮
    self.registerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.registerButton.backgroundColor = RGB(23, 153, 167);
    self.registerButton.layer.cornerRadius = 22.5;
    self.registerButton.tag = 10001;
    [self.registerButton addTarget:self action:@selector(registerPageButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.registerButton setTitleColor:GMWhiteColor forState:UIControlStateNormal];
    [self.registerButton setTitle:@"立即注册" forState:UIControlStateNormal];
    [self addSubview:self.registerButton];
    self.registerButton.sd_layout.topSpaceToView(self.pswBgView, 60*kGMHeightScale)
    .leftSpaceToView(self, 27)
    .rightSpaceToView(self, 27)
    .heightIs(45);
    
    //注册协议图标
    self.registerPtlImg = [UIButton buttonWithType:UIButtonTypeCustom];
//    self.registerPtlImg.backgroundColor = GMBlueColor;
    [self.registerPtlImg setBackgroundImage:[UIImage imageNamed:@"approved"] forState:UIControlStateNormal];
    [self.registerPtlImg setBackgroundImage:[UIImage imageNamed:@"xz-icon"] forState:UIControlStateSelected];
    self.registerPtlImg.selected = NO;
    [self.registerPtlImg addTarget:self action:@selector(registerPageButton:) forControlEvents:UIControlEventTouchUpInside];
    self.registerPtlImg.tag = 10002;
    [self addSubview:self.registerPtlImg];
    self.registerPtlImg.sd_layout.topSpaceToView(self.registerButton, 15)
    .leftSpaceToView(self, 98*KScreenW/375)
    .heightIs(18).widthIs(18);
    
    self.registerProtocol = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.registerProtocol setTitle:@"我已阅读并同意《注册协议》" forState:UIControlStateNormal];
    [self.registerProtocol addTarget:self action:@selector(registerPageButton:) forControlEvents:UIControlEventTouchUpInside];
    self.registerProtocol.tag = 10003;
    [self.registerProtocol setTitleColor:RGB(69, 69, 69) forState:UIControlStateNormal];
    self.registerProtocol.titleLabel.font = [UIFont systemFontOfSize:12];
    [self addSubview:self.registerProtocol];
    self.registerProtocol.sd_layout.topSpaceToView(self.registerButton, 18)
    .leftSpaceToView(self.registerPtlImg, 11)
    .widthIs(160).heightIs(12);
    
    //注册送vip按钮
    self.registerVipButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.registerVipButton setTitle:@"注册就送7天免费VIP" forState:UIControlStateNormal];
    [self.registerVipButton setTitleColor:RGB(21, 152, 164) forState:UIControlStateNormal];
    [self.registerVipButton addTarget:self action:@selector(registerPageButton:) forControlEvents:UIControlEventTouchUpInside];
    self.registerVipButton.tag = 10004;
    self.registerVipButton.titleLabel.font = [UIFont systemFontOfSize:15];
    
    [self addSubview:self.registerVipButton];
    self.registerVipButton.sd_layout.bottomSpaceToView(self, 34*kGMHeightScale + 20)
    .centerXEqualToView(self)
    .widthIs(150).heightIs(15);
    
    
    
    
}

- (void)registerPageButton:(UIButton *)send {
    if (self.registerButtonBlock) {
        self.registerButtonBlock(send.tag);
    }
}

@end
