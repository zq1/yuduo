//
//  LoginView.m
//  yuduo
//
//  Created by Mac on 2019/8/7.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "LoginView.h"

@implementation LoginView

-(instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self createLayout];
    }
    return self;
}

- (void)createLayout {
    self.logoImg = [[UIImageView alloc]initWithFrame:CGRectMake(KScreenW/2-49, 96*kGMHeightScale, 98, 58)];
    self.logoImg.image = [UIImage imageNamed:@"logo"];
    [self addSubview:self.logoImg];
    
    self.phoneBgView = [[UIView alloc]init];
    self.phoneBgView.backgroundColor = GMWhiteColor;
    self.phoneBgView.layer.borderWidth = 0.5;
    self.phoneBgView.layer.borderColor = RGB(23, 153, 167).CGColor;
    self.phoneBgView.layer.cornerRadius = 22.5;
    [self addSubview:self.phoneBgView];
    self.phoneBgView.sd_layout.topSpaceToView(self.logoImg, 46)
    .leftSpaceToView(self, 27)
    .rightSpaceToView(self, 27)
    .heightIs(45);
    
    self.phoneImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"shouji"]];
    [self.phoneBgView addSubview:self.phoneImg];
    self.phoneImg.sd_layout.topSpaceToView(self.phoneBgView, 15)
    .leftSpaceToView(self.phoneBgView, 14)
    .widthIs(17).heightIs(17);
    
    self.phoneText = [[UITextField alloc]init];
    self.phoneText.placeholder = @"请输入手机号";
    self.phoneText.backgroundColor = GMWhiteColor;
    self.phoneText.layer.cornerRadius = 15;
    [self.phoneBgView addSubview:self.phoneText];
    self.phoneText.sd_layout.topSpaceToView(self.phoneBgView, 5)
    .leftSpaceToView(self.phoneImg, 8)
    .widthIs(250)
    .heightIs(35);
    
    self.pswBgView = [[UIView alloc]init];
    self.pswBgView.backgroundColor = GMWhiteColor;
    self.pswBgView.layer.borderWidth = 0.5;
    self.pswBgView.layer.borderColor = RGB(23, 153, 167).CGColor;
    self.pswBgView.layer.cornerRadius = 22.5;
    [self addSubview:self.pswBgView];
    
    self.pswBgView.sd_layout.topSpaceToView(self.phoneBgView, 20)
    .leftEqualToView(self.phoneBgView)
    .rightEqualToView(self.phoneBgView)
    .heightIs(45);
    
    self.pswImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"psw"]];
    [self.pswBgView addSubview:self.pswImg];
    self.pswImg.sd_layout.topSpaceToView(self.pswBgView, 14)
    .leftSpaceToView(self.pswBgView, 15)
    .widthIs(17).heightIs(17);
    
    self.pswText = [[UITextField alloc]init];
    self.pswText.placeholder = @"请输入密码";
    self.pswText.backgroundColor = GMWhiteColor;
    self.pswText.layer.cornerRadius = 15;
    self.pswText.secureTextEntry = YES;
    [self.pswBgView addSubview:self.pswText];
    self.pswText.sd_layout.topSpaceToView(self.pswBgView, 5)
    .leftSpaceToView(self.pswImg, 8)
    .widthIs(250)
    .heightIs(35);
    //忘记密码
    self.forgetButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.forgetButton setTitle:@"忘记密码" forState:UIControlStateNormal];
    [self.forgetButton setTitleColor:RGB(23, 153, 167) forState:UIControlStateNormal];
    [self.forgetButton addTarget:self action:@selector(loginButton:) forControlEvents:UIControlEventTouchUpInside];
    self.forgetButton.tag = 10000;
    [self addSubview:self.forgetButton];
    self.forgetButton.sd_layout.topSpaceToView(self.pswBgView, 15)
    .leftSpaceToView(self, 35)
    .widthIs(80).heightIs(14);
    
    //立即注册
    self.registerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.registerButton addTarget:self action:@selector(loginButton:) forControlEvents:UIControlEventTouchUpInside];
    self.registerButton.tag = 10001;
    [self.registerButton setTitle:@"立即注册" forState:UIControlStateNormal];
    [self.registerButton setTitleColor:RGB(23, 153, 167) forState:UIControlStateNormal];
    [self addSubview:self.registerButton];
    
    self.registerButton.sd_layout.topEqualToView(self.forgetButton)
    .rightSpaceToView(self, 36)
    .widthIs(80).heightIs(14);
    
    //登录按钮
    self.atOnceLogin = [UIButton buttonWithType:UIButtonTypeCustom];
    self.atOnceLogin.backgroundColor = RGB(23, 153, 167);
    self.atOnceLogin.layer.cornerRadius = 22.5;
    self.atOnceLogin.tag = 10002;
    [self.atOnceLogin addTarget:self action:@selector(loginButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.atOnceLogin setTitleColor:GMWhiteColor forState:UIControlStateNormal];
    [self.atOnceLogin setTitle:@"立即登录" forState:UIControlStateNormal];
    [self addSubview:self.atOnceLogin];
    self.atOnceLogin.sd_layout.topSpaceToView(self.pswBgView, 88)
    .leftSpaceToView(self, 27)
    .rightSpaceToView(self, 27)
    .heightIs(45);
    
    //微信按钮
    self.WXButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.WXButton setBackgroundImage:[UIImage imageNamed:@"weixin-2"] forState:UIControlStateNormal];
    [self.WXButton addTarget:self action:@selector(loginButton:) forControlEvents:UIControlEventTouchUpInside];
    self.WXButton.tag = 10003;
    [self addSubview:self.WXButton];
    self.WXButton.sd_layout.topSpaceToView(self.atOnceLogin, 90*kGMHeightScale)
    .leftSpaceToView(self, 73)
    .widthIs(50).heightIs(50);
    
    self.QQButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.QQButton setBackgroundImage:[UIImage imageNamed:@"qq-icon"] forState:UIControlStateNormal];
    [self.QQButton addTarget:self action:@selector(loginButton:) forControlEvents:UIControlEventTouchUpInside];
    self.QQButton.tag = 10004;
    [self addSubview:self.QQButton];
    self.QQButton.sd_layout.topEqualToView(self.WXButton)
    .rightSpaceToView(self, 73)
    .widthIs(50).heightIs(50);
}

- (void)loginButton:(UIButton *)send {
    if (self.loginButtonBlock) {
        self.loginButtonBlock(send.tag);
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
