//
//  AboutInfoSubView.m
//  zhuanzhuan
//
//  Created by ZZUIHelper on 2019/10/24.
//  Copyright © 2017年 ZZUIHelper. All rights reserved.
//

#import "AboutInfoSubView.h"

@interface AboutInfoSubView ()

@property (nonatomic, strong) UIImageView *iconImageView;

@property (nonatomic, strong) UILabel *textLabel;

@end

@implementation AboutInfoSubView

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self addSubview:self.iconImageView];
        [self addSubview:self.textLabel];
        [self addMasonry];
    }
    return self;
}

#pragma mark - # Private Methods
- (void)addMasonry {
    self.iconImageView.frame = CGRectMake(5, 5, 18, 18);
    self.textLabel.frame = CGRectMake(28, 5, 170, 18);
}

#pragma mark - # Getter
- (UIImageView *)iconImageView {
    if (!_iconImageView) {
        _iconImageView = [[UIImageView alloc] init];
        _iconImageView.contentMode = UIViewContentModeScaleAspectFill;
        
    }
    return _iconImageView;
}

- (UILabel *)textLabel {
    if (!_textLabel) {
        _textLabel = [[UILabel alloc] init];
        _textLabel.font = kFont(12);
        _textLabel.textColor = [UIColor blackColor];
    }
    return _textLabel;
}
-(void)reloadImage:(NSString *)img title:(NSString *)title{
    self.iconImageView.image = [UIImage imageNamed:img];
    self.textLabel.text = title;
}
@end
