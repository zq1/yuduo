//
//  ForgetTableViewCell.m
//  yuduo
//
//  Created by Mac on 2019/8/8.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "ForgetTableViewCell.h"

@implementation ForgetTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier]) {
        
        [self createLayoutView];
    }
    return self;
}

- (void)createLayoutView {
    self.forgetImg = [[UIImageView alloc]init];
    self.forgetImg.image = [UIImage imageNamed:@"shouji"];
    [self.contentView addSubview:self.forgetImg];
    self.forgetImg.sd_layout.topSpaceToView(self.contentView, 16)
    .leftSpaceToView(self.contentView, 14)
    .centerYEqualToView(self.contentView);
    
    self.forgetTextFeild = [[UITextField alloc]init];
//    self.forgetTextFeild.backgroundColor = GMBrownColor;
    self.forgetTextFeild.font = kFont(15);
    [self.contentView addSubview:self.forgetTextFeild];
    
    self.forgetTextFeild.sd_layout.topSpaceToView(self.contentView, 16)
    .leftSpaceToView(self.forgetImg, 8)
    .widthIs(200).heightIs(18);
    
       
    
    
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
