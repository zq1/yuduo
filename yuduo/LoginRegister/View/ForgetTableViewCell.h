//
//  ForgetTableViewCell.h
//  yuduo
//
//  Created by Mac on 2019/8/8.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ForgetTableViewCell : UITableViewCell
PropertyStrong(UIImageView, forgetImg);
PropertyStrong(UITextField, forgetTextFeild);
PropertyStrong(UIButton, sendCodeButton);
@end

NS_ASSUME_NONNULL_END
