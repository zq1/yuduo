//
//  LoginView.h
//  yuduo
//
//  Created by Mac on 2019/8/7.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LoginView : UIView
//登录
PropertyStrong(UIImageView, logoImg);
PropertyStrong(UIView, phoneBgView);
PropertyStrong(UIImageView, phoneImg);
PropertyStrong(UITextField, phoneText);
//密码
PropertyStrong(UIView, pswBgView);
PropertyStrong(UIImageView, pswImg);
PropertyStrong(UITextField, pswText);
//忘记密码
PropertyStrong(UIButton, forgetButton);
//立即注册
PropertyStrong(UIButton, registerButton);
//立即登录
PropertyStrong(UIButton, atOnceLogin);
//微信
PropertyStrong(UIButton, WXButton);
//QQ登录
PropertyStrong(UIButton, QQButton);

@property (nonatomic ,strong) void(^loginButtonBlock)(NSInteger indexButton);
@end

NS_ASSUME_NONNULL_END
