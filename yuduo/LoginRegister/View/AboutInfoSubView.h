//
//  AboutInfoSubView.h
//  yuduo
//
//  Created by 刘耀聪 on 2019/10/24.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AboutInfoSubView : UIView
-(void)reloadImage:(NSString *)img title:(NSString *)title;
@end

NS_ASSUME_NONNULL_END
