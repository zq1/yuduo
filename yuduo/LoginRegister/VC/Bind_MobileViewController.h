//
//  Bind_MobileViewController.h
//  yuduo
//
//  Created by Mac on 2019/9/2.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface Bind_MobileViewController : UIViewController
PropertyStrong(UIView, bgView);
PropertyStrong(UILabel, line);
PropertyStrong(UIImageView, forgetImg);
PropertyStrong(UITextField, forgetTextFeild);
PropertyStrong(UIButton, sendCodeButton);
//userID
PropertyStrong(NSString, user_id);
@end

NS_ASSUME_NONNULL_END
