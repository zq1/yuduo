//
//  RegisterViewController.m
//  yuduo
//
//  Created by Mac on 2019/8/7.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "RegisterViewController.h"
#import "RegisterView.h"//注册UI
#import "SendCodeModel.h"//发送验证码模型
#import "ContinueRegisterViewController.h" // 继续注册
@interface RegisterViewController ()<UITextFieldDelegate>

{
    RegisterView *registerV;
    SendCodeModel *_codeModel;
}

@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    [self navigationBar];
    
    
    // Do any additional setup after loading the view.
}

-(void)navigationBar {
    
    self.view.backgroundColor = GMWhiteColor;
    self.navigationController.navigationBar.hidden = YES;
    registerV = [[RegisterView alloc]initWithFrame:[UIScreen mainScreen].bounds];
    [self.view addSubview:registerV];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    backButton.backgroundColor = GMBlueColor;
    [backButton setBackgroundImage:[UIImage imageNamed:@"back-icon"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
    [registerV addSubview:backButton];
    backButton.sd_layout.topSpaceToView(registerV,MStatusBarHeight+18)
    .leftSpaceToView(registerV, 15)
    .widthIs(11).heightIs(19);
    UIButton *backButtonView = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 80, MStatusBarHeight+44)];
    backButtonView.backgroundColor = RGBA(15, 15, 15, 0);
    [backButtonView addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
    [registerV addSubview:backButtonView];
    __weak typeof(self) weakSelf = self;
    registerV.registerButtonBlock = ^(NSInteger indexButton) {
        
        [weakSelf registerClick:indexButton];
    };
    
}

#pragma mark--注册页点击事件

- (void)registerClick:(NSInteger)send {
    switch (send) {
        case 10000:
        {
            //发送验证码
            NSMutableDictionary *sendCodeParms = [NSMutableDictionary dictionary];
            sendCodeParms[@"mobile"] = registerV.phoneText.text;
            sendCodeParms[@"code"] = @"R";
            
            [GMAfnTools PostHttpDataWithUrlStr:KRegisterCode Dic:sendCodeParms SuccessBlock:^(id  _Nonnull responseObject) {
                NSLog(@"00000%@",responseObject);
//
                self->_codeModel = [SendCodeModel mj_objectWithKeyValues:responseObject];
                
            } FailureBlock:^(id  _Nonnull error) {
//                NSLog(@"9999%@",error);
            }];
            
            [self timeOut];
        }
            break;
            //立即注册
        case 10001:
        {
            
            if (registerV.registerPtlImg.selected == YES) {
                NSMutableDictionary *sendCodeParms = [NSMutableDictionary dictionary];
                sendCodeParms[@"mobile"] = registerV.phoneText.text;
                sendCodeParms[@"password"] = registerV.pswText.text;
                sendCodeParms[@"code"]  = registerV.codeText.text;
                [GMAfnTools PostHttpDataWithUrlStr:KRegister Dic:sendCodeParms SuccessBlock:^(id  _Nonnull responseObject) {
                    if ([[responseObject objectForKey:@"code"] isEqualToString:@"1"]) {
                        NSUserDefaults *userInfo = [NSUserDefaults standardUserDefaults];
                        [userInfo setObject:[responseObject objectForKey:@"user_id"] forKey:@"user_id"];
                        [userInfo setObject:[responseObject objectForKey:@"token"] forKey:@"token"];
                        
                        ContinueRegisterViewController *continueRVC = [[ContinueRegisterViewController alloc]init];
                        [self.navigationController pushViewController:continueRVC animated:YES];
                        
                    }else {
                        [self showError:[responseObject objectForKey:@"msg"]];
                    }
                } FailureBlock:^(id  _Nonnull error) {
                    NSLog(@"------%@",error);
                }];
            }else {
                
                [self showError:@"请阅读注册协议"];
            }
           
        }
            break;
        case 10002:
        {registerV.registerPtlImg.selected = !registerV.registerPtlImg.selected;
            NSLog(@"协议图标");
            
           
        }
            break;
        case 10003:
        {
            NSLog(@"注册协议");
        }
            break;
        case 10004:
        {
            NSLog(@"七天免费Vip");
        }
            break; 
        default:
            break;
    }
}

#pragma mark -- 提示框
- (void)showError:(NSString *)errorMsg {
    // 1.弹框提醒
    // 初始化对话框
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:errorMsg preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:nil]];
    // 弹出对话框
    [self presentViewController:alert animated:true completion:nil];
}
#pragma mark --验证码倒计时
- (void)timeOut {
    NSLog(@"发送验证码");
    __block int timeout= 60; //倒计时时间
    //            __weak typeof(self) weakSelf = self;
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
    
    dispatch_source_set_event_handler(_timer, ^{
        if(timeout<=0){ //倒计时结束，关闭
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置（倒计时结束后调用）
                [self->registerV.codeSend setTitle:@"发送验证码" forState:UIControlStateNormal];
                //设置不可点击
                self->registerV.codeSend.userInteractionEnabled = YES;
//                self->registerV.codeSend.backgroundColor = RGB(23, 153, 167);
                
            });
        }else{
            //            int minutes = timeout / 60;    //这里注释掉了，这个是用来测试多于60秒时计算分钟的。
            int seconds = timeout % 60;
            NSString *strTime = [NSString stringWithFormat:@"%d", seconds];
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
                NSLog(@"____%@",strTime);
                [self->registerV.codeSend setTitle:[NSString stringWithFormat:@"%@秒",strTime] forState:UIControlStateNormal];
                
                //设置可点击
                self->registerV.codeSend.userInteractionEnabled = NO;
//                self->registerV.codeSend .backgroundColor = [UIColor lightGrayColor];
            });
            
            timeout--;
        }
    });
    
    dispatch_resume(_timer);
    
}

- (void)backBtn {
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

@end
