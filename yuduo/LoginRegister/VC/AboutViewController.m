//
//  AboutViewController.m
//  yuduo
//
//  Created by Mac on 2019/9/26.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "AboutViewController.h"
#import "AboutInfoSubView.h"
@interface AboutViewController ()
@property(nonatomic,strong)UIView * contenBackview;
@property(nonatomic,strong)UILabel * contentLabel;
@property(nonatomic,strong)AboutInfoSubView * phoneView;
@property(nonatomic,strong)AboutInfoSubView * addressView;
@property(nonatomic,strong)AboutInfoSubView * companyiew;
@end

@implementation AboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self getInfo];
    [self.view addSubview:self.contenBackview];
    [self.contenBackview addSubview:self.contentLabel];
    [self.view addSubview: self.phoneView];
     [self.view addSubview: self.addressView];
     [self.view addSubview: self.companyiew];
    [self changeNavigation];
    [self setupSubview];
    // Do any additional setup after loading the view.
}
-(void)getInfo{
    NSMutableDictionary *infoDic = [NSMutableDictionary dictionary];
    [GMAfnTools PostHttpDataWithUrlStr:KAboutMsg Dic:infoDic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"获取关于我的信息 == %@",responseObject);
        [self setContentText:responseObject[@"content"]];
        [self.phoneView reloadImage:@"aboutEmail" title:responseObject[@"mailbox"]];
        [self.addressView reloadImage:@"aboutAddress" title:responseObject[@"address"]];
        [self.companyiew reloadImage:@"aboutLogo" title:@"北京承启文化传播有限公司"];
    } FailureBlock:^(id  _Nonnull error) {
        
    }];
}

- (void)setContentText: (NSString *)content {
    NSString *HTMLString = [NSString stringWithFormat:@"<html><body><p>%@</p></body></html>", content ];
    NSDictionary *options = @{NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType,
                              NSCharacterEncodingDocumentAttribute : @(NSUTF8StringEncoding)
                              };
    NSData *data = [HTMLString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableAttributedString * attributedString = [[NSMutableAttributedString alloc] initWithData:data options:options documentAttributes:nil error:nil];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];   // 调整行间距
    paragraphStyle.lineSpacing = 8.0;
    paragraphStyle.alignment = NSTextAlignmentJustified;
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, attributedString.length)];
    
    [attributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:15] range:NSMakeRange(0, attributedString.length)];
    
    _contentLabel.attributedText = attributedString;
}

-(UILabel *)contentLabel{
    if (!_contentLabel) {
        _contentLabel = [[UILabel alloc] init];
        _contentLabel.frame = CGRectMake(15, 15, KScreenW-60, 230);
        _contentLabel.textColor = [UIColor blackColor];
        _contentLabel.numberOfLines = 0;
        _contentLabel.font = [UIFont fontWithName:@"PingFang SC" size: 14];
        
    }
    return _contentLabel;
}
-(AboutInfoSubView *)phoneView{
    if (!_phoneView) {
        _phoneView = [[AboutInfoSubView alloc] init];
        _phoneView.frame = CGRectMake(10, 475, 200, 25);
    }
    return _phoneView;
}
-(AboutInfoSubView *)addressView{
    if (!_addressView) {
        _addressView = [[AboutInfoSubView alloc] init];
        _addressView.frame = CGRectMake(10, 520, 200, 25);
    }
    return _addressView;
}
-(AboutInfoSubView *)companyiew{
    if (!_companyiew) {
        _companyiew = [[AboutInfoSubView alloc] init];
        _companyiew.frame = CGRectMake((KScreenW-200)/2,KScreenH-50,200,13.5);
    }
    return _companyiew;
}
-(UIView *)contenBackview{
    if (!_contenBackview) {
        _contenBackview = [[UIView alloc] init];
        _contenBackview.backgroundColor = [UIColor whiteColor];
        _contenBackview.frame = CGRectMake(15, 183.5, KScreenW- 30, 262.5);
        _contenBackview.layer.cornerRadius = 10;
        
        _contenBackview.layer.shadowColor = [UIColor blackColor].CGColor;
        // 设置阴影偏移量
        _contenBackview.layer.shadowOffset = CGSizeMake(0,2);
        // 设置阴影透明度
        _contenBackview.layer.shadowOpacity = 1;
        // 设置阴影半径
        _contenBackview.layer.shadowRadius = 2;
        _contenBackview.clipsToBounds = NO;
    
    }
    return _contenBackview;
}
- (void)changeNavigation {
    
    self.navigationController.navigationBar.topItem.title = @"关于我们";
    UIView *titleView = [[UIView alloc]init];
    titleView.backgroundColor = GMWhiteColor;
    [self.view addSubview:titleView];
    titleView.sd_layout.topSpaceToView(self.view,0)
    .leftSpaceToView(self.view, 0)
    .rightSpaceToView(self.view, 0)
    .widthIs(KScreenW).heightIs(MStatusBarHeight+44);
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setBackgroundImage:[UIImage imageNamed:@"back-icon"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
    [titleView addSubview:backButton];
    backButton.sd_layout.bottomSpaceToView(titleView,13)
    .leftSpaceToView(titleView, 15)
    .widthIs(11).heightIs(19);
    UIButton *backButtonView = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 80, MStatusBarHeight+44)];
    backButtonView.backgroundColor = RGBA(15, 15, 15, 0);
    [backButtonView addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButtonView];
    UILabel *titleLabel = [[UILabel alloc]init];
    [titleView addSubview:titleLabel];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"关于我们" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 18],NSForegroundColorAttributeName: [UIColor colorWithRed:69/255.0 green:69/255.0 blue:69/255.0 alpha:1.0]}];
    titleLabel.attributedText = string;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.sd_layout.bottomSpaceToView(titleView, 15)
    .centerXEqualToView(titleView)
    .heightIs(18).widthIs(100);
   
}
-(void)setupSubview{
    UIImageView *logoImg = [[UIImageView alloc]init];
       logoImg.image = [UIImage imageNamed:@"logo"];
       logoImg.contentMode = UIViewContentModeScaleAspectFill;
       [self.view addSubview:logoImg];
       logoImg.sd_layout.topSpaceToView(self.view, MStatusBarHeight+68)
       .centerXEqualToView(self.view)
       .widthIs(60).heightIs(36);
       
       UILabel *versionLabel = [[UILabel alloc] init];
       versionLabel.numberOfLines = 0;
       //versionLabel.backgroundColor = [UIColor blackColor];
       versionLabel.text = [NSString stringWithFormat:@"V%@",kCurrentAppVersion];
       versionLabel.textAlignment = NSTextAlignmentCenter;
       versionLabel.font = [UIFont fontWithName:KPFType size:14];
       versionLabel.textColor = RGB(153, 153, 153);
       [self.view addSubview:versionLabel];
       versionLabel.sd_layout.topSpaceToView(logoImg, 15)
       .centerXIs(self.view.center.x)
       .heightIs(14).widthIs(100);
       //=====
       
//       UILabel *label = [[UILabel alloc] init];
//    label.frame = CGRectMake(15, 23, 300, 40);
//       //label.frame = CGRectMake(30.5,207,313.5,39.5);
//       label.numberOfLines = 0;
//       [self.contenBackview addSubview:label];
//
//       NSMutableAttributedString *stringx = [[NSMutableAttributedString alloc] initWithString:@"北京出版集团旗下北京承启文化传播有限公司主办，是专注于场景式育儿教育的内容平台" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFang SC" size: 14],NSForegroundColorAttributeName: [UIColor colorWithRed:69/255.0 green:69/255.0 blue:69/255.0 alpha:1.0]}];
//
//       label.attributedText = stringx;
//
//       UILabel *labelw = [[UILabel alloc] init];
//       labelw.frame = CGRectMake(15,90,300,115.5);
//       labelw.numberOfLines = 0;
//       [self.contenBackview addSubview:labelw];
//
//       NSMutableAttributedString *stringe = [[NSMutableAttributedString alloc] initWithString:@"育朵到依托《父母必读》杂志40年丰富的内容资源、专家资源、采用文字+音频、视频等多种形态手段，通过科学、系统的场景知识体系，专业权威的专家专栏，打造场景式育儿app为0-12岁父母提供一站式养育解决方案。" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFang SC" size: 14],NSForegroundColorAttributeName: [UIColor colorWithRed:69/255.0 green:69/255.0 blue:69/255.0 alpha:1.0]}];
//
//       labelw.attributedText = stringe;
//
//       UILabel *labely = [[UILabel alloc] init];
//       labely.frame = CGRectMake(118,622.5,180,13.5);
//       labely.numberOfLines = 0;
//       [self.view addSubview:labely];
//
//       NSMutableAttributedString *stringg = [[NSMutableAttributedString alloc] initWithString:@"北京承启文化传播有限公司" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFang SC" size: 14],NSForegroundColorAttributeName: [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1.0]}];
//
//       labely.attributedText = stringg;
}
- (void)backBtn {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
