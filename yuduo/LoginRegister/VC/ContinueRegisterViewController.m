//
//  ContinueRegisterViewController.m
//  yuduo
//
//  Created by Mac on 2019/8/21.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "ContinueRegisterViewController.h"
#import "SelectChildViewController.h" //选择孩子
@interface ContinueRegisterViewController ()

@end

@implementation ContinueRegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = GMWhiteColor;
    [self setNavigation];
    
    
    // Do any additional setup after loading the view.
}

- (void)setNavigation {
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:[UIImage imageNamed:@"back-icon"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButton];
    backButton.sd_layout.topSpaceToView(self.view,MStatusBarHeight+12)
    .leftSpaceToView(self.view, 15)
    .widthIs(11).heightIs(19);
    UIButton *backButtonView = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 80, MStatusBarHeight+44)];
    backButtonView.backgroundColor = RGBA(15, 15, 15, 0);
    [backButtonView addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButtonView];
    UILabel *titleLabel = [[UILabel alloc]init];
    [self.view addSubview:titleLabel];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"选择您的身份" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 18],NSForegroundColorAttributeName: [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1.0]}];
    titleLabel.attributedText = string;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.sd_layout.topSpaceToView(self.view, MStatusBarHeight+13)
    .centerXEqualToView(self.view)
    .heightIs(18).widthIs(150);
    
    NSArray *titleArray = @[@"宝妈",@"宝爸"];
    NSArray *imgArray = @[@"baoma",@"宝爸"];
    for (int i = 0 ; i < 2; i ++) {
        UIButton *parentsButton = [UIButton buttonWithType:UIButtonTypeCustom];
        parentsButton.frame = CGRectMake((45+i*((100+85)*kGMWidthScale)*kGMWidthScale), 234*kGMHeightScale, 100*kGMWidthScale, 102*kGMHeightScale);
        
        UILabel *parentsLabel = [[UILabel alloc]initWithFrame:CGRectMake((79+i*((33+155)*kGMWidthScale)*kGMWidthScale), 234*kGMHeightScale+112*kGMHeightScale, 33*kGMWidthScale, 12*kGMHeightScale)];
        parentsLabel.textColor = RGB(69, 69, 69);
        parentsLabel.font = [UIFont fontWithName:@"PingFang SC" size:12];
        parentsLabel.text = [titleArray objectAtIndex:i];
        [parentsButton setBackgroundImage:[UIImage imageNamed:[imgArray objectAtIndex:i]] forState:UIControlStateNormal];
        [parentsButton addTarget:self action:@selector(parentClick:) forControlEvents:UIControlEventTouchUpInside];
        parentsButton.layer.cornerRadius = 102*kGMHeightScale/2;
        parentsButton.tag = 10000+i;
        parentsButton.backgroundColor = GMlightGrayColor;
        
        [self.view addSubview:parentsButton];
        [self.view addSubview:parentsLabel];
        
    }
}

- (void)parentClick:(UIButton *)send {
    
    if (send.tag == 10000) {
        NSLog(@"宝妈");
        SelectChildViewController *select = [[SelectChildViewController alloc]init];
        select.parentSexIndex = @"2";
        [self.navigationController pushViewController:select animated:YES];
    }else {
        SelectChildViewController *select = [[SelectChildViewController alloc]init];
        select.parentSexIndex = @"1";
        [self.navigationController pushViewController:select animated:YES];
        NSLog(@"宝爸");
    }
    
}
- (void)backBtn {
    [self.navigationController popViewControllerAnimated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
