//
//  SelectChildViewController.m
//  yuduo
//
//  Created by Mac on 2019/8/21.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "SelectChildViewController.h"
#import "SexBabyViewController.h"
@interface SelectChildViewController ()

@end

@implementation SelectChildViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = GMWhiteColor;
    NSLog(@"xxxxxxxxxx%@",self.parentSexIndex);
    
    [self setNavigation];
    // Do any additional setup after loading the view.
}

- (void)setNavigation {
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:[UIImage imageNamed:@"back-icon"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButton) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButton];
    backButton.sd_layout.topSpaceToView(self.view,MStatusBarHeight+12)
    .leftSpaceToView(self.view, 15)
    .widthIs(11).heightIs(19);
    UIButton *backButtonView = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 80, MStatusBarHeight+44)];
    backButtonView.backgroundColor = RGBA(15, 15, 15, 0);
    [backButtonView addTarget:self action:@selector(backButton) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButtonView];
    UILabel *titleLabel = [[UILabel alloc]init];
    [self.view addSubview:titleLabel];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"选择孩子" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 18],NSForegroundColorAttributeName: [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1.0]}];
    titleLabel.attributedText = string;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.sd_layout.topSpaceToView(self.view, MStatusBarHeight+13)
    .centerXEqualToView(self.view)
    .heightIs(18).widthIs(150);
    
    UILabel *label = [[UILabel alloc] init];
    label.text = @"您现在有几个孩子";
    label.textColor = RGB(23, 153, 167);
    label.font = [UIFont fontWithName:@"PingFang SC" size:15];
    [self.view addSubview:label];
    label.sd_layout.topSpaceToView(self.view, 154+MStatusBarHeight)
    .centerXEqualToView(self.view)
    .widthIs(130).heightIs(15);
    
    UIButton *oneButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [oneButton addTarget:self action:@selector(oneClick) forControlEvents:UIControlEventTouchUpInside];
    oneButton.layer.borderWidth = 1;
    oneButton.backgroundColor = GMWhiteColor;
    [oneButton setTitle:@"一个" forState:UIControlStateNormal];
    [oneButton setTitleColor:RGB(23, 153, 167) forState:UIControlStateNormal];
    oneButton.layer.borderColor = RGB(23, 153, 167).CGColor;
    oneButton.layer.cornerRadius = 22.5;
    [self.view addSubview:oneButton];
    oneButton.sd_layout.topSpaceToView(label, 40)
    .leftSpaceToView(self.view, 28)
    .rightSpaceToView(self.view, 28)
    .heightIs(45);
    
    UIButton *twoButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [twoButton addTarget:self action:@selector(twoClick) forControlEvents:UIControlEventTouchUpInside];
    twoButton.layer.borderWidth = 1;
    twoButton.backgroundColor = GMWhiteColor;
    [twoButton setTitle:@"两个" forState:UIControlStateNormal];
    [twoButton setTitleColor:RGB(23, 153, 167) forState:UIControlStateNormal];
    twoButton.layer.borderColor = RGB(23, 153, 167).CGColor;
    twoButton.layer.cornerRadius = 22.5;
    [self.view addSubview:twoButton];
    twoButton.sd_layout.topSpaceToView(oneButton, 30)
    .leftSpaceToView(self.view, 28)
    .rightSpaceToView(self.view, 28)
    .heightIs(45);
   
    UIButton *moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [moreButton addTarget:self action:@selector(moreClick) forControlEvents:UIControlEventTouchUpInside];
    moreButton.layer.borderWidth = 1;
    moreButton.backgroundColor = RGB(23, 153, 167);
    [moreButton setTitle:@"更多" forState:UIControlStateNormal];
    [moreButton setTitleColor:GMWhiteColor forState:UIControlStateNormal];
    moreButton.layer.borderColor = RGB(23, 153, 167).CGColor;
    moreButton.layer.cornerRadius = 22.5;
    [self.view addSubview:moreButton];
    moreButton.sd_layout.topSpaceToView(twoButton, 80)
    .leftSpaceToView(self.view, 28)
    .rightSpaceToView(self.view, 28)
    .heightIs(45);
    
}

- (void)oneClick {
    NSLog(@"一个");
    SexBabyViewController *sexBaby = [[SexBabyViewController alloc]init];
    sexBaby.parentSelectIndex = self.parentSexIndex;
    sexBaby.babyNumIndex = @"1";
    [self.navigationController pushViewController:sexBaby animated:YES];
}
- (void)twoClick {
    NSLog(@"两个");
    SexBabyViewController *sexBaby = [[SexBabyViewController alloc]init];
    sexBaby.parentSelectIndex = self.parentSexIndex;
    sexBaby.babyNumIndex = @"2";
    [self.navigationController pushViewController:sexBaby animated:YES];
    
}
- (void)moreClick {
    NSLog(@"更多");
}
- (void)backButton {
    [self.navigationController popViewControllerAnimated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
