//
//  Bind_MobileViewController.m
//  yuduo
//
//  Created by Mac on 2019/9/2.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "Bind_MobileViewController.h"
#import "ForgetTableViewCell.h"
@interface Bind_MobileViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
PropertyStrong(UITableView, detalTablView);
@end

@implementation Bind_MobileViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavigation];
    // Do any additional setup after loading the view.
}
- (void)setNavigation {
    self.view.backgroundColor = RGB(244, 243, 244);
    
    UIView *titleView = [[UIView alloc]init];
    titleView.backgroundColor = GMWhiteColor;
    [self.view addSubview:titleView];
    titleView.sd_layout.topSpaceToView(self.view, 0)
    .leftEqualToView(self.view)
    .widthIs(KScreenW).heightIs(44+MStatusBarHeight);
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //    backButton.backgroundColor = GMBlueColor;
    [backButton setBackgroundImage:[UIImage imageNamed:@"back-icon"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
    [titleView addSubview:backButton];
    backButton.sd_layout.topSpaceToView(titleView,MStatusBarHeight+15)
    .leftSpaceToView(titleView, 15)
    .widthIs(11).heightIs(19);
    UIButton *backButtonView = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 80, MStatusBarHeight+44)];
    backButtonView.backgroundColor = RGBA(15, 15, 15, 0);
    [backButtonView addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButtonView];
    
    UILabel *forgetPsw = [[UILabel alloc]init];
    forgetPsw.text = @"绑定手机号";
    forgetPsw.font = kFont(18);
    [titleView addSubview:forgetPsw];
    forgetPsw.sd_layout.topSpaceToView(titleView, MStatusBarHeight+15)
    .centerXEqualToView(titleView).widthIs(150).heightIs(17);
    self.detalTablView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.detalTablView.backgroundColor = GMWhiteColor;
    self.detalTablView.delegate = self;
    self.detalTablView.dataSource = self;
    self.detalTablView.showsHorizontalScrollIndicator = NO;
    self.detalTablView.showsVerticalScrollIndicator = NO;
    self.detalTablView.bounces = NO;
    [self.view addSubview:self.detalTablView];
    self.detalTablView.sd_layout.topSpaceToView(titleView, 10)
    .leftEqualToView(self.view)
    .widthIs(KScreenW).heightIs(150*kGMHeightScale);
    
    UIButton *confirmButton = [UIButton buttonWithType:UIButtonTypeCustom];
    confirmButton = [UIButton buttonWithType:UIButtonTypeCustom];
    confirmButton.backgroundColor = RGB(23, 153, 167);
    confirmButton.layer.cornerRadius = 22.5;
    
    [confirmButton addTarget:self action:@selector(confirmButton:) forControlEvents:UIControlEventTouchUpInside];
    [confirmButton setTitleColor:GMWhiteColor forState:UIControlStateNormal];
    [confirmButton setTitle:@"确定" forState:UIControlStateNormal];
    [self.view addSubview:confirmButton];
    confirmButton.sd_layout.topSpaceToView(self.detalTablView, 60*kGMHeightScale)
    .leftSpaceToView(self.view, 27)
    .rightSpaceToView(self.view, 27)
    .heightIs(45);
}

- (void)confirmButton:(UIButton *)send {
    NSLog(@"e确定");
    
    UITextField *phoneText = (UITextField *)[self.view viewWithTag:10000];
    UITextField *codeText = (UITextField *)[self.view viewWithTag:10001];
    UITextField *pswText = (UITextField *)[self.view viewWithTag:10002];
    
    NSMutableDictionary *mobileDic = [NSMutableDictionary dictionary];
    mobileDic[@"mobile"] = phoneText.text;
    mobileDic[@"password"] = pswText.text;
    mobileDic[@"code"] = codeText.text;
    mobileDic[@"user_id"] = self.user_id;
    [GMAfnTools PostHttpDataWithUrlStr:KBindWX Dic:mobileDic SuccessBlock:^(id  _Nonnull responseObject) {

        if ([[responseObject objectForKey:KSucessCode] isEqualToString:@"1"]) {
            NSLog(@"xxxxxxx登录成功");
            
        }else {
            NSLog(@"是否需要绑定手机号");
            
            
        }
    } FailureBlock:^(id  _Nonnull error) {
        
    }];

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50*kGMHeightScale;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *imgArray = @[@"shouji",@"yanzhengma",@"psw"];
    NSArray *placeHoder = @[@"请输入手机号",@"请输入验证码",@"请输入新密码"];
    static NSString *cellId = @"cellID";
    ForgetTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[ForgetTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    }
    self.forgetImg = [[UIImageView alloc]init];
    self.forgetImg.image = [UIImage imageNamed:[imgArray objectAtIndex:indexPath.row]];
    [cell addSubview:self.forgetImg];
    self.forgetImg.sd_layout.topSpaceToView(cell, 16)
    .leftSpaceToView(cell, 14)
    .centerYEqualToView(cell);
    
    self.forgetTextFeild = [[UITextField alloc]init];
    //        self.forgetTextFeild.backgroundColor = GMBrownColor;
    self.forgetTextFeild.font = kFont(15);
    self.forgetTextFeild.tag = indexPath.row+10000;
    NSLog(@"======+%ld",self.forgetTextFeild.tag);
    self.forgetTextFeild.delegate = self;
    self.forgetTextFeild.placeholder = [placeHoder objectAtIndex:indexPath.row];
    [cell addSubview:self.forgetTextFeild];
    
    self.forgetTextFeild.sd_layout.topSpaceToView(cell, 15)
    .leftSpaceToView(self.forgetImg, 8)
    .widthIs(200).heightIs(20*kGMHeightScale);
    
    if (indexPath.row == 1) {
        self.sendCodeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.sendCodeButton addTarget:self action:@selector(sendButton:) forControlEvents:UIControlEventTouchUpInside];
        [self.sendCodeButton setTitle:@"发送验证码" forState:UIControlStateNormal];
        [self.sendCodeButton setTitleColor:RGB(23, 153, 67) forState:UIControlStateNormal];
        self.sendCodeButton.titleLabel.font = [UIFont systemFontOfSize:14];
        [cell.contentView addSubview:self.sendCodeButton];
        self.sendCodeButton.sd_layout.topSpaceToView(cell.contentView, 18)
        .rightSpaceToView(cell.contentView, 16)
        .widthIs(75).heightIs(14);
    }
    
    return cell;
}



#pragma mark -- 发送验证码
- (void)sendButton:(UIButton *)send {
    NSLog(@"发送验证码");
    [self timeOut];
    
    NSMutableDictionary *senCodeDic = [NSMutableDictionary dictionary];
    UITextField *phoneText = (UITextField *)[self.view viewWithTag:10000];
    UITextField *sendCodeText = (UITextField *)[self.view viewWithTag:10001];
    
    UITextField *pswText = (UITextField *)[self.view viewWithTag:10002];
    senCodeDic[@"mobile"] = phoneText.text;
    senCodeDic[@"code"] = @"P";
    senCodeDic[@"password"] = pswText.text;
    
    [GMAfnTools PostHttpDataWithUrlStr:KRegisterCode Dic:senCodeDic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"获取验证码%@",responseObject);
    } FailureBlock:^(id  _Nonnull error) {
    }];
    NSLog(@"--手机号 : %@",phoneText.text);
    NSLog(@"--密码  :  %@",pswText.text);
}
#pragma mark --验证码倒计时
- (void)timeOut {
    NSLog(@"发送验证码");
    __block int timeout= 60; //倒计时时间
    //            __weak typeof(self) weakSelf = self;
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
    
    dispatch_source_set_event_handler(_timer, ^{
        if(timeout<=0){ //倒计时结束，关闭
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置（倒计时结束后调用）
                [self.sendCodeButton setTitle:@"发送验证码" forState:UIControlStateNormal];
                //设置不可点击
                self.sendCodeButton.userInteractionEnabled = YES;
                //                self->registerV.codeSend.backgroundColor = RGB(23, 153, 167);
                
            });
        }else{
            //            int minutes = timeout / 60;    //这里注释掉了，这个是用来测试多于60秒时计算分钟的。
            int seconds = timeout % 60;
            NSString *strTime = [NSString stringWithFormat:@"%d", seconds];
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
                NSLog(@"____%@",strTime);
                [self.sendCodeButton setTitle:[NSString stringWithFormat:@"%@秒",strTime] forState:UIControlStateNormal];
                
                //设置可点击
                self.sendCodeButton.userInteractionEnabled = NO;
                //                self->registerV.codeSend .backgroundColor = [UIColor lightGrayColor];
            });
            
            timeout--;
        }
    });
    
    dispatch_resume(_timer);
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSLog(@"-------%ld",(long)indexPath.row);
}

- (void)backBtn {
    [self.navigationController popViewControllerAnimated:YES];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
