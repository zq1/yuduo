//
//  LoginViewController.m
//  yuduo
//
//  Created by Mac on 2019/8/7.
//  Copyright © 2019 yaocongkeji. All rights reserved.

#import "LoginViewController.h"
#import "LoginView.h"
#import "RegisterViewController.h"
#import "ForgetViewController.h"
#import "MysetBgView.h"

#import "AppDelegate.h"

#import "WXAuth.h"
#import "Bind_MobileViewController.h"
@interface LoginViewController ()<SXWWeChatDelegate,WXApiDelegate>

@property (nonatomic,strong) LoginView *loginV;
@property (nonatomic,strong) MysetBgView *mySetView;
PropertyStrong(NSString, codeStringTest);
@end

@implementation LoginViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self navigationBar];
    // Do any additional setup after loading the view.
}

-(void)navigationBar {
    _loginV = [[LoginView alloc]initWithFrame:[UIScreen mainScreen].bounds];
    [self.view addSubview:_loginV];
    __weak typeof(self) weakSelf = self;
    _loginV.loginButtonBlock = ^(NSInteger indexButton) {
        [weakSelf buttonClick:indexButton];
    };
    self.view.backgroundColor = GMWhiteColor;
    self.navigationController.navigationBar.hidden = YES;
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setBackgroundImage:[UIImage imageNamed:@"back-icon"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
    [_loginV addSubview:backButton];
    backButton.sd_layout.topSpaceToView(_loginV,MStatusBarHeight+18)
    .leftSpaceToView(_loginV, 15)
    .widthIs(11).heightIs(19);
    
    UIButton *backButtonView = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 80, MStatusBarHeight+44)];
    backButtonView.backgroundColor = RGBA(15, 15, 15, 0);
    [backButtonView addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButtonView];
}

- (void)backBtn {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - 设置弹出提示语
- (void)setupAlertController {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"温馨提示" message:@"请先安装微信客户端" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *actionConfirm = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:actionConfirm];
    [self presentViewController:alert animated:YES completion:nil];
}
- (void)dissmiss {
    RegisterViewController *registerVC = [[RegisterViewController alloc]init];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dismissNow) name:@"registerSuccess" object:nil];
//    //            [self.navigationController pushViewController:registerVC animated:YES];
    [self presentViewController:registerVC animated:YES completion:nil];
}

- (void)buttonClick:(NSInteger)send {
    switch (send) {
        case 10000:
        {
            NSLog(@"忘记密码");
            ForgetViewController *forgetVC = [[ForgetViewController alloc]init];
            [self.navigationController pushViewController:forgetVC animated:YES];
        }
            break;
        case 10001:
            NSLog(@"立即注册");
        {
            RegisterViewController *registerVC = [[RegisterViewController alloc]init];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dissmiss) name:@"registerSuccess" object:nil];
//            [self.navigationController pushViewController:registerVC animated:YES];
            [self presentViewController:registerVC animated:YES completion:nil];
        }
            break;
        case 10002:
        {
            
            NSLog(@"立即登录");
            NSMutableDictionary *loginDic = [NSMutableDictionary dictionary];
            loginDic[@"mobile"] = _loginV.phoneText.text;
            loginDic[@"password"] = _loginV.pswText.text;

            typeof(self) weakSelf = self;
            [GMAfnTools PostHttpDataWithUrlStr:KLogin Dic:loginDic SuccessBlock:^(id  _Nonnull responseObject) {
                NSLog(@"-------%@",responseObject);
                
                NSString *username = weakSelf.loginV.phoneText.text;
                NSString *password = weakSelf.loginV.pswText.text;
                
//
//                //对用户信息的验证
                if ([[responseObject objectForKey:@"code"] isEqualToString:@"1"]){
                    //获取userDefault单例
                    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                    //登陆成功后把用户名和密码存储到UserDefault
                    [userDefaults setObject:username forKey:@"name"];
                    [userDefaults setObject:password forKey:@"password"];
                    NSString *tokenString = [[responseObject objectForKey:@"data"] objectForKey:@"token"];
               
                    NSMutableDictionary *dicx = [responseObject objectForKey:@"data"];
                    NSString *userxxxx = [dicx objectForKey:@"user_id"];
                    NSLog(@"l登录存user_id==%@",userxxxx);
                    [userDefaults setObject:tokenString forKey:@"token"];
                    [userDefaults setObject:userxxxx forKey:@"user_id"];
                    [userDefaults setObject:@(YES) forKey:kIsLogin];
                    [userDefaults synchronize];
               
                    
                    
//                    //创建通知对象
//                    NSNotification *notification = [NSNotification notificationWithName:KLoginNotification object:nil];
//                    //发送通知
//                    [[NSNotificationCenter defaultCenter] postNotification:notification];
                    [self dismissViewControllerAnimated:YES completion:^{
                        [[NSNotificationCenter defaultCenter] postNotificationName:KLoginNotification object:nil];
                    }];
//                    if (!self.isTouristLogin) {
//                        [[NSNotificationCenter defaultCenter] postNotificationName:@"tongzhi" object:nil];
//                    }
                }else {
                    //提示用户名或密码错误从新输入
                    [self showError:[responseObject objectForKey:@"msg"]];
                    
                }

            } FailureBlock:^(id  _Nonnull error) {
               
                NSLog(@"登录 ======%@",error);
            }];
        }
            break;
        case 10003:
        {
            NSLog(@"微信登录");
            [WXAUTH sendWXAuthReq];
            WXAUTH.codeBlock = ^(NSString *codeString) {
                [self string:codeString];
            };
           
          
//            NSLog(@"access : %@ === openId :  %@",access_token,openId);

        }
            break;
        case 10004:
        {
            NSLog(@"QQ登录");
            
        }
            break;
        default:
            break;
    }
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self.view endEditing:YES];
    
}
#pragma mark -- 提示框
- (void)showError:(NSString *)errorMsg {
    // 1.弹框提醒
    // 初始化对话框
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:errorMsg preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:nil]];
    // 弹出对话框
    [self presentViewController:alert animated:true completion:nil];
    
    [self performSelector:@selector(dimisssAlert:) withObject:alert afterDelay:1];
    
}

- (void) dimisssAlert:(UIAlertController *)alert
{
    if (alert) {
        [alert dismissViewControllerAnimated:YES completion:nil];
    }
    
}

- (void)string:(NSString *)reqCode {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"code"] = reqCode;
    NSLog(@"获取code成功%@",reqCode);
    [GMAfnTools PostHttpDataWithUrlStr:KWXAcessTOKEN Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"=====获取成功---%@",responseObject);
        NSString *bind = [NSString stringWithFormat:@"%@",[responseObject objectForKey:@"is_bind_mobile"]];
        NSString *user_id = [responseObject objectForKey:@"user_id"];
        NSString *toke = [responseObject objectForKey:@"token"];
        NSUserDefaults *defalts = [NSUserDefaults standardUserDefaults];
        [defalts setObject:user_id forKey:@"user_id"];
        [defalts setObject:toke forKey:@"token"];
        
        if ([bind isEqualToString:@"2"]) {
            Bind_MobileViewController *bind = [[Bind_MobileViewController alloc]init];
            bind.user_id = [responseObject objectForKey:@"user_id"];
            [self.navigationController pushViewController:bind animated:YES];
        }else {
            
            [self dismissViewControllerAnimated:YES completion:nil];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:KLoginNotification object:nil];
        }
    } FailureBlock:^(id  _Nonnull error) {
        NSLog(@" 上传code报错====%@",error);
    }];
}

- (void )bindPhoneNumber {
    
  
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
