//
//  SexBabyViewController.m
//  yuduo
//
//  Created by Mac on 2019/8/21.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "SexBabyViewController.h"
#import "BRTextField.h"
#import "BRPickerView.h"
@interface SexBabyViewController ()<UITextFieldDelegate>
/** 出生年月 */
@property (nonatomic, strong) BRTextField *birthdayTF;

@end

@implementation SexBabyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = GMWhiteColor;
    [self setNavigation];
    // Do any additional setup after loading the view.
}

- (void)setNavigation {
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:[UIImage imageNamed:@"back-icon"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButton) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButton];
    UIButton *backButtonView = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 80, MStatusBarHeight+44)];
    backButtonView.backgroundColor = RGBA(15, 15, 15, 0);
    [backButtonView addTarget:self action:@selector(backButton) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButtonView];
    backButton.sd_layout.topSpaceToView(self.view,MStatusBarHeight+12)
    .leftSpaceToView(self.view, 15)
    .widthIs(11).heightIs(19);
    UILabel *titleLabel = [[UILabel alloc]init];
    [self.view addSubview:titleLabel];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"选择宝宝的性别" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 18],NSForegroundColorAttributeName: [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1.0]}];
    titleLabel.attributedText = string;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.sd_layout.topSpaceToView(self.view, MStatusBarHeight+13)
    .centerXEqualToView(self.view)
    .heightIs(18).widthIs(150);
    
    NSArray *titleArray = @[@"男宝",@"女宝"];
    NSArray *imgArray = @[@"男宝",@"女宝"];
    for (int i = 0 ; i < 2; i ++) {
        UIButton *parentsButton = [UIButton buttonWithType:UIButtonTypeCustom];
        parentsButton.frame = CGRectMake((45+i*((100+85)*kGMWidthScale)*kGMWidthScale), 234*kGMHeightScale, 100*kGMWidthScale, 102*kGMHeightScale);
        
        UILabel *parentsLabel = [[UILabel alloc]initWithFrame:CGRectMake((79+i*((33+155)*kGMWidthScale)*kGMWidthScale), 234*kGMHeightScale+112*kGMHeightScale, 33*kGMWidthScale, 12*kGMHeightScale)];
        parentsLabel.textColor = RGB(69, 69, 69);
        parentsLabel.font = [UIFont fontWithName:@"PingFang SC" size:12];
        parentsLabel.text = [titleArray objectAtIndex:i];
        [parentsButton setBackgroundImage:[UIImage imageNamed:[imgArray objectAtIndex:i]] forState:UIControlStateNormal];
        [parentsButton addTarget:self action:@selector(parentClick:) forControlEvents:UIControlEventTouchUpInside];
        parentsButton.layer.cornerRadius = 102*kGMHeightScale/2;
        parentsButton.tag = 10000+i;
        parentsButton.backgroundColor = GMlightGrayColor;
        
        [self.view addSubview:parentsButton];
        [self.view addSubview:parentsLabel];
        
    }
}

- (void)parentClick:(UIButton *)send {
    if (send.tag == 10000) {
        NSLog(@"男宝");
        NSLog(@"xxxxxxxx%@",self.parentSelectIndex);
        NSLog(@"rrrrrrrr%@",self.babyNumIndex);
        
        [self upBabyBoy];
       
    }else {
        [self upBabyGirl];
        NSLog(@"女宝");
        
    }
    
}

- (void) upBabyGirl {
    
    [BRDatePickerView showDatePickerWithTitle:@"选择出生时间" dateType:UIDatePickerModeDate defaultSelValue:nil minDateStr:nil maxDateStr:nil isAutoSelect:YES resultBlock:^(NSString *selectValue) {
        NSLog(@"宝宝出生日期 : %@",selectValue);
        NSMutableDictionary *babyInfo = [NSMutableDictionary dictionary];
        NSDictionary  *dict = @{@"baby_sex":@"2" ,@"birthday":selectValue};
        NSMutableArray *babyArray = [NSMutableArray array];
        [babyArray addObject:dict];
//        babyInfo[@"user_id"] = KUser_id;
//        babyInfo[@"token"] = KToken;
        NSUserDefaults *detafaults = [NSUserDefaults standardUserDefaults];
        NSString  *user_id =  [detafaults objectForKey:@"user_id"];
        NSString *token =  [detafaults objectForKey:@"token"];
        babyInfo[@"user_id"] = user_id;
        babyInfo[@"token"] = token;
        //父母性别
//        babyInfo[@"sex"] = self.parentSelectIndex;
//        //孩子个数
//        babyInfo[@"children_number"] = self.babyNumIndex;
        //父母性别
        babyInfo[@"sex"] = @"1";
        //孩子个数
        babyInfo[@"children_number"] = @"2";
        babyInfo[@"babyinfo"] = babyArray;
        NSLog(@"打印宝宝上传信息==%@",babyInfo);
        
        [GMAfnTools PostHttpDataWithUrlStr:KContinueRegister Dic:babyInfo SuccessBlock:^(id  _Nonnull responseObject) {
            NSLog(@"上传baby信息%@",responseObject);
        } FailureBlock:^(id  _Nonnull error) {
            NSLog(@"baby info erro%@",error);
        }];
        
    }];
    
}

- (void) upBabyBoy {
    
    [BRDatePickerView showDatePickerWithTitle:@"选择出生时间" dateType:UIDatePickerModeDate defaultSelValue:nil minDateStr:nil maxDateStr:nil isAutoSelect:YES resultBlock:^(NSString *selectValue) {
        NSLog(@"宝宝出生日期 : %@",selectValue);
        NSMutableDictionary *babyInfo = [NSMutableDictionary dictionary];
        NSDictionary  *dict = @{@"baby_sex":@"1" ,@"birthday":selectValue};
        NSMutableArray *babyArray = [NSMutableArray array];
        [babyArray addObject:dict];
        NSUserDefaults *detafaults = [NSUserDefaults standardUserDefaults];
        NSString  *user_id =  [detafaults objectForKey:@"user_id"];
        NSString *token =  [detafaults objectForKey:@"token"];
        babyInfo[@"user_id"] = user_id;
        babyInfo[@"token"] = token;
//        babyInfo[@"user_id"] = KUser_id;
//        babyInfo[@"token"] = KToken;
        //父母性别
        babyInfo[@"sex"] = self.parentSelectIndex;
        babyInfo[@"children_number"] = self.babyNumIndex;
        babyInfo[@"babyinfo"] = babyArray;
        [GMAfnTools PostHttpDataWithUrlStr:KContinueRegister Dic:babyInfo SuccessBlock:^(id  _Nonnull responseObject) {
            NSLog(@"上传baby信息%@",responseObject);
            if (responseObject) {
                [self.navigationController dismissViewControllerAnimated:YES completion:nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"registerSuccess" object:nil];
            }
            else {
                [self showError:[responseObject objectForKey:@"msg"]];
            }
        } FailureBlock:^(id  _Nonnull error) {
            NSLog(@"baby info erro%@",error);
            
        }];
        
    }];
    
}
#pragma mark -- 提示框
- (void)showError:(NSString *)errorMsg {
    // 1.弹框提醒
    // 初始化对话框
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:errorMsg preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:nil]];
    // 弹出对话框
    [self presentViewController:alert animated:true completion:nil];
    
    [self performSelector:@selector(dimisssAlert:) withObject:alert afterDelay:1];
    
}

- (void) dimisssAlert:(UIAlertController *)alert
{
    if (alert) {
        [alert dismissViewControllerAnimated:YES completion:nil];
    }
    
}
- (void)backButton {
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
