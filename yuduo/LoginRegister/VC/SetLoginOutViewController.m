//
//  SetLoginOutViewController.m
//  yuduo
//
//  Created by Mac on 2019/8/28.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "SetLoginOutViewController.h"
#import "AboutViewController.h"
@interface SetLoginOutViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    UILabel *nextLabel;
}
PropertyStrong(UIButton, loginOutButton);
PropertyStrong(UITableView, setTableView);
@end

@implementation SetLoginOutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = GMWhiteColor;
    [self setNavigation];
    self.setTableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.setTableView.delegate = self;
    self.setTableView.dataSource = self;
    self.setTableView.scrollEnabled = NO;
    [self.view addSubview:self.setTableView];
    self.setTableView.sd_layout.topSpaceToView(self.view, MStatusBarHeight+54)
    .leftSpaceToView(self.view, 15)
    .rightSpaceToView(self.view, 15)
    .heightIs(90);
    // Do any additional setup after loading the view.
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"cellID";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
        
    }
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    NSArray *arr = @[@"清理缓存",@"关于我们"];
    if (indexPath.row == 0) {
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%.2f%@", [[SDImageCache sharedImageCache] totalDiskSize]/1024.0/1024.0, @"M"];
    }
    cell.textLabel.text = [arr objectAtIndex:indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
//        UPSDImageCache.shared().clearMemory()
//        UPSDImageCache.shared().clearDisk()
        [[SDImageCache sharedImageCache] clearMemory];
        [[SDImageCache sharedImageCache] clearDiskOnCompletion:^{
            [self showError:@"缓存清除成功"];
            [self.setTableView reloadData];
        }];
    }
    if (indexPath.row == 1) {
        AboutViewController *aboutVC = [[AboutViewController alloc]init];
        [self.navigationController pushViewController:aboutVC animated:YES];
    }
}
#pragma mark -- 提示框
- (void)showError:(NSString *)errorMsg {
    // 1.弹框提醒
    // 初始化对话框
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:errorMsg preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:nil]];
    // 弹出对话框
    [self presentViewController:alert animated:true completion:nil];
    
    [self performSelector:@selector(dimisssAlert:) withObject:alert afterDelay:1];
    
}

- (void) dimisssAlert:(UIAlertController *)alert
{
    if (alert) {
        [alert dismissViewControllerAnimated:YES completion:nil];
    }
    
}
- (void)setNavigation {
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:[UIImage imageNamed:@"back-icon"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButton) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButton];
    backButton.sd_layout.topSpaceToView(self.view,MStatusBarHeight+12)
    .leftSpaceToView(self.view, 15)
    .widthIs(11).heightIs(19);
    UIButton *backButtonView = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 80, MStatusBarHeight+44)];
    backButtonView.backgroundColor = RGBA(15, 15, 15, 0);
    [backButtonView addTarget:self action:@selector(backButton) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButtonView];
    UILabel *titleLabel = [[UILabel alloc]init];
    [self.view addSubview:titleLabel];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"设置" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 18],NSForegroundColorAttributeName: [UIColor colorWithRed:69/255.0 green:69/255.0 blue:69/255.0 alpha:1.0]}];
    titleLabel.attributedText = string;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.sd_layout.topSpaceToView(self.view, MStatusBarHeight+13)
    .centerXEqualToView(self.view)
    .heightIs(18).widthIs(150);
    
    //登录按钮
    self.loginOutButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.loginOutButton.backgroundColor = RGB(23, 153, 167);
    self.loginOutButton.layer.cornerRadius = 22.5;
    [self.loginOutButton addTarget:self action:@selector(loginOutButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.loginOutButton setTitleColor:GMWhiteColor forState:UIControlStateNormal];
    [self.loginOutButton setTitle:@"退出登录" forState:UIControlStateNormal];
    [self.view addSubview:self.loginOutButton];
    self.loginOutButton.sd_layout.topSpaceToView(self.view, MStatusBarHeight+274)
    .leftSpaceToView(self.view, 27)
    .rightSpaceToView(self.view, 27)
    .heightIs(45);
}
- (void)loginOutButton:(UIButton *)send {
    NSLog(@"点击了退出登录按钮");
    NSUserDefaults *account = [NSUserDefaults standardUserDefaults];
    [account removeObjectForKey:@"token"];
    [account removeObjectForKey:@"name"];
    [account removeObjectForKey: @"password"];
    [account removeObjectForKey:@"user_id"];
    [account removeObjectForKey:kIsLogin];
//    [account setObject:@(NO) forKey:kIsLogin];
    NSLog(@"xxxxx------%@",[account objectForKey:kIsLogin]);
//    if ([account objectForKey:kIsLogin]) {
//        [self.navigationController popViewControllerAnimated:YES];
//    }else {
//        NSLog(@"出错了");
//    }
    [[NSNotificationCenter defaultCenter] postNotificationName:KLogOutNotifacation object:nil];
    [self.navigationController popViewControllerAnimated:YES];
//
//    NSUserDefaults *logOut = [NSUserDefaults standardUserDefaults];
//    [logOut objectForKey:kIsLogin];
//    NSLog(@"xxxxxxx%@",[logOut objectForKey:kIsLogin]);
//    if ([logOut objectForKey:kIsLogin] == NO) {
////
//        [[NSNotificationCenter defaultCenter]postNotificationName:KLogOutNotifacation object:nil];
//    [self .navigationController popViewControllerAnimated:YES];
//    }
//    NSLog(@"%@",[account objectForKey:kIsLogin]);
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)backButton {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
