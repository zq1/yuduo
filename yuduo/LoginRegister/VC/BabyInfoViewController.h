//
//  BabyInfoViewController.h
//  yuduo
//
//  Created by Mac on 2019/8/30.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BabyInfoViewController : UIViewController
PropertyStrong(NSString, parentSex);
PropertyStrong(NSString, babySex);
PropertyStrong(NSString, babyNumber);
@end

NS_ASSUME_NONNULL_END
