//
//  SearchLiveModel.h
//  yuduo
//
//  Created by mason on 2019/9/4.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface SearchLiveModel : BaseModel
PropertyStrong(NSString, activity_name);
PropertyStrong(NSString, activity_address);
PropertyStrong(NSString, activity_stop_date);
PropertyStrong(NSString, activity_cover);
PropertyStrong(NSString, activity_type);
PropertyStrong(NSString, activity_end_date);
PropertyStrong(NSString, activity_price);
PropertyStrong(NSString, activity_start_date);
PropertyStrong(NSString, activity_id);




@end

NS_ASSUME_NONNULL_END
