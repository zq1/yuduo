//
//  HotSearchModel.h
//  yuduo
//
//  Created by Mac on 2019/9/3.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface HotSearchModel : BaseModel
PropertyStrong(NSString, keys);
@end

NS_ASSUME_NONNULL_END
