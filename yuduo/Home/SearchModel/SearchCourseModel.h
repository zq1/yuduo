//
//  SearchCourseModel.h
//  yuduo
//
//  Created by mason on 2019/9/3.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface SearchCourseModel : BaseModel
PropertyStrong(NSString, course_category_id);
PropertyStrong(NSString, course_list_cover);
PropertyStrong(NSString, course_price);
PropertyStrong(NSString, is_free);
PropertyStrong(NSString, name);
PropertyStrong(NSString, start_date);
PropertyStrong(NSString, member_is_free);
PropertyStrong(NSString, course_under_price);
PropertyStrong(NSString, course_id);
PropertyStrong(NSString, course_name);
@end

NS_ASSUME_NONNULL_END
