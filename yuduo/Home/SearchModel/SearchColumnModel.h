//
//  SearchColumnModel.h
//  yuduo
//
//  Created by mason on 2019/9/4.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface SearchColumnModel : BaseModel
PropertyStrong(NSString, course_vrows);
PropertyStrong(NSString, course_price);
PropertyStrong(NSString, gement_id);
PropertyStrong(NSString, start_date);
PropertyStrong(NSString, course_list_cover);
PropertyStrong(NSString, member_is_free);
PropertyStrong(NSString, course_under_price);
PropertyStrong(NSString, end_date);
PropertyStrong(NSString, course_name);
PropertyStrong(NSString, is_free);
PropertyStrong(NSString, class_hour);

@end

NS_ASSUME_NONNULL_END
