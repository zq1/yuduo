//
//  HistorySearchModel.h
//  yuduo
//
//  Created by Mac on 2019/9/3.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface HistorySearchModel : BaseModel
PropertyStrong(NSString, keys);
PropertyStrong(NSString, search_id);
@end

NS_ASSUME_NONNULL_END
