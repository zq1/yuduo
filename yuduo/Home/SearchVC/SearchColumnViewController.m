//
//  SearchColumnViewController.m
//  yuduo
//
//  Created by mason on 2019/9/3.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "SearchColumnViewController.h"
#import "ZhuanLanTableViewCell.h"
#import "HomeColumnDetailViewController.h"
#import "HomeHotColumnModel.h"
#import "SearchColumnModel.h"
@interface SearchColumnViewController ()
<UITableViewDelegate,UITableViewDataSource>
PropertyStrong(UITableView, coureseTableView);
PropertyStrong(NSMutableArray, columnSelectArray);
@end

@implementation SearchColumnViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.columnArray.count == 0) {
        //        self.view.backgroundColor = GMRedColor;
        UIImageView *img = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"wuneir.png"]];
        [self.view addSubview:img];
        img.sd_layout.topSpaceToView(self.view, 200).centerXEqualToView(self.view);
        UILabel *label = [[UILabel alloc]init];
        label.textColor = RGB(153, 153, 153);
        label.font = [UIFont fontWithName:KPFType size:14];
        label.text = @"无相关搜索内容";
        [self.view addSubview:label];
        label.sd_layout.topSpaceToView(img, 15)
        .leftEqualToView(img)
        .widthIs(100).heightIs(14);
    } else {
      

    self.coureseTableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.coureseTableView.delegate = self;
    self.coureseTableView.dataSource = self;
//    self.coureseTableView.backgroundColor = RGB(244, 243, 244);
    [self.coureseTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.view addSubview:self.coureseTableView];
    self.coureseTableView.sd_layout.topSpaceToView(self.view, 0)
    .leftSpaceToView(self.view, 0)
    .rightSpaceToView(self.view,0)
    .widthIs(KScreenW).heightIs(KScreenH-100);
    }
    // Do any additional setup after loading the view.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.columnArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 120;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"cellID";
    ZhuanLanTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[ZhuanLanTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
  self.columnSelectArray = [NSMutableArray array];
    for (NSMutableDictionary *columnDic in self.columnArray) {
        SearchColumnModel *mol = [[SearchColumnModel alloc]init];
        [mol mj_setKeyValues:columnDic];
        [self.columnSelectArray addObject:mol];
    }
    

    
    SearchColumnModel *molx = [self.columnSelectArray objectAtIndex:indexPath.row];
    NSString *xpricelabel = [@"¥" stringByAppendingString:molx.course_under_price];
    NSString *pricelabel = [@"¥" stringByAppendingString:molx.course_price];
    cell.titleLable.text = molx.course_name;
    [cell.headImg sd_setImageWithURL:[NSURL URLWithString:molx.course_list_cover]];
//    cell.xPriceLabel.text = xpricelabel;
    NSMutableAttributedString *xPriceAttText = [[NSMutableAttributedString alloc] initWithString:xpricelabel attributes:@{NSStrikethroughStyleAttributeName:@(NSUnderlineStyleSingle)}];
    cell.xPriceLabel.attributedText = xPriceAttText;
    cell.priceLabel.text = pricelabel;
    cell.peopleLable.text = molx.course_vrows;
    cell.keshiLable.text = [NSString stringWithFormat:@"%@课时", molx.class_hour];
    cell.backgroundColor = RGB(244, 243, 244);
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    HomeColumnDetailViewController *coureDetailVC = [[HomeColumnDetailViewController alloc]init];
    HomeHotColumnModel *mol = [self.columnSelectArray objectAtIndex:indexPath.row];
    coureDetailVC.courseDetailIDString = mol.gement_id;
    
    [coureDetailVC setHidesBottomBarWhenPushed:YES];
    [self.navigationController pushViewController:coureDetailVC animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
