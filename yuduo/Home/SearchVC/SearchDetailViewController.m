//
//  SearchDetailViewController.m
//  yuduo
//
//  Created by Mac on 2019/9/3.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "SearchDetailViewController.h"
#import "TapSliderScrollView.h"
#import "SimpleTapSliderScrollView.h"

//详细列表
#import "KeChengViewController.h"
#import "ZhuanLanViewController.h"
#import "ZhiBoViewController.h"
#import "WenZhangViewController.h"

//搜索列表
#import "SearchCourseViewController.h"
#import "SearchColumnViewController.h"
#import "SearchLiveViewController.h"
#import "SearchAticileViewController.h"

#import "SearchCourseModel.h"
#import "SearchAticleModel.h"
#import "SearchLiveModel.h"
#import "SearchColumnModel.h"
//免费专区
@interface SearchDetailViewController ()
<SliderLineViewDelegate,SimpleSliderLineViewDelegate,UITableViewDelegate,UITableViewDataSource, UIScrollViewDelegate>
@property (nonatomic ,assign) BOOL selectSort;

@property (nonatomic, strong) NSArray *dataArr;

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) NSMutableArray *myChildViewControllers;
@end

@implementation SearchDetailViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"=======搜索详情页%@",self.allDictionrySearch);
    [self changeNavigation];
    [self scrollZX];
    
    //    [self tab];
    // Do any additional setup after loading the view.
}
- (void)scrollZX {
    
    NSMutableArray *activityArray = [NSMutableArray array];
    NSMutableArray *articleyArray = [NSMutableArray array];
    NSMutableArray *courseArray = [NSMutableArray array];
    NSMutableArray *gementArray = [NSMutableArray array];
    
    activityArray = [self.allDictionrySearch objectForKey:@"activity"];
    articleyArray = [self.allDictionrySearch objectForKey:@"article"];
    courseArray = [self.allDictionrySearch objectForKey:@"course"];
    gementArray = [self.allDictionrySearch objectForKey:@"gement"];
    NSLog(@"=======课程 ==%ld",courseArray.count);
    NSLog(@"========专栏==%ld",gementArray.count);
    NSLog(@"=======活动 ==%ld",activityArray.count);
    NSLog(@"========文章==%ld",articleyArray.count);
    
    SearchCourseViewController *vc1 = [[SearchCourseViewController alloc]init];
    vc1.courseArray = courseArray;
    
    SearchColumnViewController *vc2 = [[SearchColumnViewController alloc]init];
    vc2.columnArray = gementArray;
    
    SearchLiveViewController *vc3 = [[SearchLiveViewController alloc]init];
    vc3.liveArray = activityArray;
    
    SearchAticileViewController *vc4 = [[SearchAticileViewController alloc]init];
    vc4.aticleArray = articleyArray;

    SimpleTapSliderScrollView *simpleview = [[SimpleTapSliderScrollView alloc]initWithFrame:CGRectMake(0, 45+MStatusBarHeight, KScreenW, KScreenH)];
    
    simpleview.delegate = self;
    simpleview.sliderViewColor = RGB(21, 152, 164);
    simpleview.titileColror = RGB(153, 153, 153);
    simpleview.selectedColor = RGB(21, 152, 164);
    [simpleview createView:@[@"课程",@"专栏",@"活动",@"文章"] andViewArr:@[vc1,vc2,vc3,vc4] andRootVc:self];
    [self.view addSubview:simpleview];
    
    NSLog(@"====%ld",courseArray.count);
    
    if (courseArray.count == 0 && gementArray.count == 0 && activityArray.count == 0 && articleyArray.count == 0) {
        [simpleview sliderToViewIndex:0];
    } else {
        if (courseArray.count != 0) {
            [simpleview sliderToViewIndex:0];
        } else if (gementArray.count != 0) {
            [simpleview sliderToViewIndex:1];
        } else if (activityArray.count != 0) {
            [simpleview sliderToViewIndex:2];
        } else if (articleyArray.count != 0) {
            [simpleview sliderToViewIndex:3];
        }
    }
}
#pragma mark sliderDelegate -- OC
-(void)sliderViewAndReloadData:(NSInteger)index
{
    NSLog(@"刷新数据啦%ld",index);
    if (index == 3) {
        NSLog(@"创建tableview");
        
    }
}


- (void)tab {
    UITableView *table = [[UITableView alloc]initWithFrame:CGRectMake(0, 100, 300, 400) style:UITableViewStylePlain];
//    table.backgroundColor = GMBlueColor;
    table.delegate =self;
    table.dataSource= self;
    [self.view addSubview:table];
}


- (void)changeNavigation {
    
    self.navigationController.navigationBar.topItem.title = @"搜索结果";
    UIView *titleView = [[UIView alloc]init];
    titleView.backgroundColor = GMWhiteColor;
    [self.view addSubview:titleView];
    titleView.sd_layout.topSpaceToView(self.view,0)
    .leftSpaceToView(self.view, 0)
    .rightSpaceToView(self.view, 0)
    .widthIs(KScreenW).heightIs(MStatusBarHeight+44);
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setBackgroundImage:[UIImage imageNamed:@"back-icon"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
    [titleView addSubview:backButton];
    backButton.sd_layout.bottomSpaceToView(titleView,13)
    .leftSpaceToView(titleView, 15)
    .widthIs(11).heightIs(19);
    UIButton *backButtonView = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 80, MStatusBarHeight+44)];
    backButtonView.backgroundColor = RGBA(15, 15, 15, 0);
    [backButtonView addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButtonView];
    UILabel *titleLabel = [[UILabel alloc]init];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [titleView addSubview:titleLabel];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"搜索" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 18],NSForegroundColorAttributeName: [UIColor colorWithRed:69/255.0 green:69/255.0 blue:69/255.0 alpha:1.0]}];
    titleLabel.attributedText = string;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.sd_layout.bottomSpaceToView(titleView, 15)
    .centerXEqualToView(titleView)
    .heightIs(18).widthIs(100);
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}
#pragma mark--返回行高
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40;
}
#pragma mark--返回行数
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    static NSString *key0=@"cell0";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:key0];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:key0];
        
    }
    //    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (void)backBtn {
    NSLog(@"xxxxxxxx");
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
