//
//  SearchViewController.h
//  yuduo
//
//  Created by Mac on 2019/9/3.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SearchViewController : UIViewController
@property (nonatomic ,copy) void (^searchCourseBlock) (NSMutableArray *coureArr);
@end

NS_ASSUME_NONNULL_END
