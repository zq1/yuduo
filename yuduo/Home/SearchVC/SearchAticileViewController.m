//
//  SearchAticileViewController.m
//  yuduo
//
//  Created by mason on 2019/9/3.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "SearchAticileViewController.h"
#import "WenZhangTableViewCell.h"
#import "JingxuanTableViewCell.h"
#import "SearchAticleModel.h"
#import "NewYuDuoChoiceDetailViewController.h"
@interface SearchAticileViewController ()
<UITableViewDelegate,UITableViewDataSource>
PropertyStrong(UITableView, coureseTableView);
@end

@implementation SearchAticileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.aticleArray.count == 0) {
        //        self.view.backgroundColor = GMRedColor;
        UIImageView *img = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"wuneir.png"]];
        [self.view addSubview:img];
        img.sd_layout.topSpaceToView(self.view, 200).centerXEqualToView(self.view);
        UILabel *label = [[UILabel alloc]init];
        label.textColor = RGB(153, 153, 153);
        label.font = [UIFont fontWithName:KPFType size:14];
        label.text = @"无相关搜索内容";
        [self.view addSubview:label];
        label.sd_layout.topSpaceToView(img, 15)
        .leftEqualToView(img)
        .widthIs(100).heightIs(14);
    } else {
        
    self.coureseTableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    
    [self.coureseTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.coureseTableView.delegate = self;
    self.coureseTableView.dataSource = self;
    //    self.coureseTableView.backgroundColor = GMBrownColor;
    [self.view addSubview:self.coureseTableView];
    self.coureseTableView.sd_layout.topSpaceToView(self.view, 0)
    .leftSpaceToView(self.view, 15)
    .rightSpaceToView(self.view, 15)
    .widthIs(KScreenW-30).heightIs(KScreenH-100);
    // Do any additional setup after loading the view.
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.aticleArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 120;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"cellID";
    JingxuanTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[JingxuanTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    NSMutableArray *xxx = [NSMutableArray array];
    for (NSMutableDictionary *articDic in self.aticleArray) {
        SearchAticleModel *mol = [[SearchAticleModel alloc]init];
        [mol mj_setKeyValues:articDic];
        [xxx addObject:mol];
    }
    
    SearchAticleModel *mol = [xxx objectAtIndex:indexPath.row];
//    NSLog(@"---------_%@",mol.article_title);
//    cell.titleLabel.text = mol.article_title;
    
    cell.titleLabel.text = mol.article_title;
//    cell.zanLabel.text = [mol.thumbs_up stringByAppendingString:@"人"];
    cell.mianfeiLabel.text = @"免费";
    cell.zanLabel.text = mol.article_vrows;
    [cell.headImgView sd_setImageWithURL:[NSURL URLWithString:mol.article_cover]];
    CGFloat price = mol.article_price.floatValue;
    if (price > 0) {
        cell.priceLabel.text = [NSString stringWithFormat:@"¥%.2f", price];
    } else {
        cell.priceLabel.text = @"免费";
    }
    cell.messageLabel.text = [NSString stringWithFormat:@"%ld", (long)mol.comment_count.integerValue];
    cell.detailLabel.text = mol.brief;
    if ([mol.article_member_is_free isEqualToString:@"1"]) {
        cell.mianfeiLabel.text = @"vip免费";
    }else {
        cell.mianfeiLabel.hidden = YES;
        cell.titleLabel.sd_layout.leftSpaceToView(cell.headImgView, 17);
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSMutableArray *xxx = [NSMutableArray array];
    for (NSMutableDictionary *articDic in self.aticleArray) {
        SearchAticleModel *mol = [[SearchAticleModel alloc]init];
        [mol mj_setKeyValues:articDic];
        [xxx addObject:mol];
    }
    
    SearchAticleModel *mol = [xxx objectAtIndex:indexPath.row];
    
    NewYuDuoChoiceDetailViewController *vc = [[NewYuDuoChoiceDetailViewController alloc] init];
    vc.articleStringID = mol.article_id;
    vc.articleTitle = mol.article_title;
    [self.navigationController pushViewController:vc animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
