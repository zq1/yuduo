//
//  SearchCourseViewController.m
//  yuduo
//
//  Created by mason on 2019/9/3.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "SearchCourseViewController.h"
#import "MianFeiCourseTableViewCell.h"
#import "SearchCourseModel.h"
#import "CoureDetailViewController.h"

#import "NewCourseTwoTableViewCell.h"
#import "HomeNewCourseModel.h"

@interface SearchCourseViewController ()
<UITableViewDelegate,UITableViewDataSource>
PropertyStrong(UITableView, coureseTableView);
PropertyStrong(UITableView, myZXTableView);

PropertyStrong(NSMutableArray, courseSelectArray);
@property (nonatomic, copy) NSArray *courseModelArray;
@end

@implementation SearchCourseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.courseArray.count == 0) {
//        self.view.backgroundColor = GMRedColor;
        UIImageView *img = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"wuneir.png"]];
        [self.view addSubview:img];
        img.sd_layout.topSpaceToView(self.view, 200).centerXEqualToView(self.view);
        UILabel *label = [[UILabel alloc]init];
        label.textColor = RGB(153, 153, 153);
        label.font = [UIFont fontWithName:KPFType size:14];
        label.text = @"无相关搜索内容";
        [self.view addSubview:label];
        label.sd_layout.topSpaceToView(img, 15)
        .leftEqualToView(img)
        .widthIs(100).heightIs(14);
        
    } else {
        [self showTableView];
    }

 
}

- (void)showTableView {
    
    //    self.view.backgroundColor = RGB(244, 243, 244);
    self.coureseTableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.coureseTableView.delegate = self;
    self.coureseTableView.dataSource = self;
    //    self.coureseTableView.backgroundColor = RGB(244, 243, 244);
    [self.view addSubview:self.coureseTableView];
    self.coureseTableView.tableFooterView = [UIView new];
    self.coureseTableView.sd_layout.topSpaceToView(self.view, 0)
    .leftEqualToView(self.view)
    .widthIs(KScreenW).heightIs(KScreenH-MStatusBarHeight);
    // Do any additional setup after loading the view.
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 50;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.courseArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 159;
}

- (void)setCourseArray:(NSMutableArray *)courseArray {
    _courseArray = courseArray;
    
    self.courseModelArray = [HomeNewCourseModel mj_objectArrayWithKeyValuesArray:courseArray];
    
    [self.coureseTableView reloadData];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"cell";
    NewCourseTwoTableViewCell *cell = [[NewCourseTwoTableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    //                cell1.backgroundColor = [UIColor redColor];
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    HomeNewCourseModel *homeModel = [self.courseModelArray objectAtIndex:indexPath.row];
    
    NSString *xpricelabel = [@"¥" stringByAppendingString:homeModel.course_price];
    NSString *pricelabel = [@"¥" stringByAppendingString:homeModel.course_under_price];
    cell.titleLabel.text = homeModel.course_name;
    
    cell.xPriceLabel.text = pricelabel;
    cell.priceLabel.text = xpricelabel;
    
    if ([homeModel.is_free isEqualToString:@"1"]) {
        cell.priceLabel.text = @"免费";
    }
    
    NSString *keshiString = [homeModel.class_hour stringByAppendingString:@"课时"];
    cell.keshiLabel.text = keshiString;
    NSString *peopleString = homeModel.course_vrows;
    cell.peoplesLabel.text = peopleString;
    [cell.headImgView sd_setImageWithURL:[NSURL URLWithString:homeModel.course_list_cover]];
    cell.headImgView.sd_cornerRadius = [NSNumber numberWithInteger:10];
    cell.detailLabel.text = homeModel.brief;
    
    //判断j会员标识
    if ([homeModel.member_is_free isEqualToString:@"1"]) {
        cell.mianfeiLabel.hidden = NO;
        cell.mianfeiLabel.text = @"vip免费";
        cell.mianfeiLabel.textColor = GMWhiteColor;
        cell.mianfeiLabel.textAlignment = NSTextAlignmentCenter;
    }else {
        cell.mianfeiLabel.hidden = YES;
        cell.titleLabel.sd_layout.topEqualToView(cell.mianfeiLabel)
        .leftSpaceToView(cell.headImgView, 20)
        .widthIs(161*KScreenW/375).heightIs(15);
    }
    
    return cell;
//    static NSString *cellIdentifier = @"cellID";
//    MianFeiCourseTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
//    if (!cell) {
//        cell = [[MianFeiCourseTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
//    }
//    self.courseSelectArray = [NSMutableArray array];
//    for (NSMutableDictionary *courseDic in self.courseArray) {
//        SearchCourseModel *mol = [[SearchCourseModel alloc]init];
//        [mol mj_setKeyValues:courseDic];
//        [self.courseSelectArray addObject:mol];
//    }
//
//    SearchCourseModel *molx = [self.courseSelectArray objectAtIndex:indexPath.row];
//    cell.titleLabel.text = molx.course_name;
//
//    [cell.headImgView sd_setImageWithURL:[NSURL URLWithString:molx.course_list_cover]];
//    cell.priceLabel.text = molx.course_under_price;
//    cell.xPriceLabel.text = molx.course_price;
//    NSLog(@"------搜索课程--%@",molx.course_id);
//    if ([molx.is_free isEqualToString:@"1"]) {
//
//        cell.priceLabel.text = @"免费";
//    } else {
//
//        cell.priceLabel.text = molx.course_price;
//
//    }
//    if ([molx.member_is_free isEqualToString:@"1"]) {
//        cell.mianfeiLabel.hidden = NO;
//
//    }else {
//        cell.mianfeiLabel.hidden = YES; cell.titleLabel.sd_layout.leftSpaceToView(cell.headImgView, 17);
//    }
//    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
     SearchCourseModel *molx = [self.courseModelArray objectAtIndex:indexPath.row];
    NSLog(@"打印搜索结果 ID == %@",molx.course_id);
    CoureDetailViewController *detailVC = [[CoureDetailViewController alloc]init];
    detailVC.courseDetailIDString = molx.course_id;
    [self.navigationController pushViewController:detailVC animated:YES];
}
@end
