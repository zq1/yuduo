//
//  SearchLiveViewController.m
//  yuduo
//
//  Created by mason on 2019/9/3.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "SearchLiveViewController.h"
#import "ActiveDetailViewController.h"

#import "HomeLiveTableViewCell.h"
#import "ActiveCenterOnlineTableViewCell.h"
#import "ActiveCenterDefultTableViewCell.h"

#import "SearchLiveModel.h"
#import "HomeActiveModel.h"
@interface SearchLiveViewController ()
<UITableViewDelegate,UITableViewDataSource>
PropertyStrong(UITableView, coureseTableView);
@end

@implementation SearchLiveViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.liveArray.count == 0) {
        //        self.view.backgroundColor = GMRedColor;
        UIImageView *img = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"wuneir.png"]];
        [self.view addSubview:img];
        img.sd_layout.topSpaceToView(self.view, 200).centerXEqualToView(self.view);
        UILabel *label = [[UILabel alloc]init];
        label.textColor = RGB(153, 153, 153);
        label.font = [UIFont fontWithName:KPFType size:14];
        label.text = @"无相关搜索内容";
        [self.view addSubview:label];
        label.sd_layout.topSpaceToView(img, 15)
        .leftEqualToView(img)
        .widthIs(100).heightIs(14);
    } else {
        
        
        self.coureseTableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        self.coureseTableView.delegate = self;
        self.coureseTableView.dataSource = self;
        [self.coureseTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        //    self.coureseTableView.backgroundColor = GMBrownColor;
        [self.view addSubview:self.coureseTableView];
        self.coureseTableView.sd_layout.topSpaceToView(self.view, 0)
        .leftSpaceToView(self.view, 15)
        .rightSpaceToView(self.view, 15)
        .widthIs(KScreenW-30).heightIs(KScreenH-100);
    }
    // Do any additional setup after loading the view.
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.liveArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    HomeActiveModel *activeModel = [HomeActiveModel mj_objectWithKeyValues:[self.liveArray objectAtIndex:indexPath.row]];
    if ([activeModel.activity_type integerValue] == 1) {
       return 270;
    } else {
       return  290;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    static NSString *cellIdentifier = @"cellID";
//    HomeLiveTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
//    if (!cell) {
//        cell = [[HomeLiveTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
//    }
//    NSMutableArray *liveArray = [NSMutableArray array];
//    for (NSMutableDictionary *liveDic in self.liveArray) {
//        HomeActiveModel *mol = [[HomeActiveModel alloc]init];
//        [mol mj_setKeyValues:liveDic];
//        [liveArray addObject:mol];
//    }
//
//    HomeActiveModel *molx = [liveArray objectAtIndex:indexPath.row];
   
    HomeActiveModel *molx = [HomeActiveModel mj_objectWithKeyValues:[self.liveArray objectAtIndex:indexPath.row]];
    if ([molx.activity_type integerValue] == 1) {
        static NSString *cellID = @"cellOnlineID";
            ActiveCenterOnlineTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        //    ActiveAddressTableViewCell *cell1 = [tableView dequeueReusableCellWithIdentifier:cellID1];
            if (!cell) {
                cell = [[ActiveCenterOnlineTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
            }
        cell.model = molx;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }else{
        static NSString *cellID = @"cellDefultID";
            ActiveCenterDefultTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
            if (!cell) {
                cell = [[ActiveCenterDefultTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
            }
        cell.model = molx;
         cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    return [[UITableViewCell alloc] init];
    
//    cell.titleLabel.text = molx.activity_name;
//    cell.backgroundColor = RGB(244, 243, 244);
//    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    HomeActiveModel *molx = [HomeActiveModel mj_objectWithKeyValues:[self.liveArray objectAtIndex:indexPath.row]];
    
    ActiveDetailViewController *vc = [[ActiveDetailViewController alloc] init];
    vc.category_id = molx.activity_id;
    vc.online = [molx.activity_type isEqualToString:@"1"];
    [self.navigationController pushViewController:vc animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
