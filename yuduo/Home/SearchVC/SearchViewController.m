//
//  SearchViewController.m
//  yuduo
//
//  Created by Mac on 2019/9/3.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "SearchViewController.h"
//历史搜索
#import "HistorySearchModel.h"
//热门搜索
#import "HotSearchModel.h"

#import "SearchDetailViewController.h"
#import "LoginViewController.h"
#import "SearchCourseViewController.h"

#import "SearchCourseModel.h"
#import "SearchLiveModel.h"
#import "SearchColumnModel.h"
#import "SearchAticleModel.h"
@interface SearchViewController ()<UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITextFieldDelegate>

PropertyStrong(UIButton, searchButton);  //搜索按钮
PropertyStrong(UITextField, searchText); //搜索框
PropertyStrong(UITableView, searchTableView);
PropertyStrong(UIView, searchView);
PropertyStrong(NSArray, sectionTitle);
PropertyStrong(UIView, sectionView);
@property (strong,nonatomic)UICollectionView *collectionV;
@property (strong,nonatomic)UICollectionViewFlowLayout *flowLayout;
PropertyStrong(NSArray,searchCountArr);
//热门搜索
PropertyStrong(NSMutableArray, hotSearchArray);
//历史搜索
PropertyStrong(NSMutableArray, historySearchArray);
PropertyStrong(NSString, search_idString);
//rowcount
@property (nonatomic,assign) NSInteger rowCount;
@end

@implementation SearchViewController
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = GMWhiteColor;
    self.searchCountArr = @[@"测试1",@"测试2",@"测试3"];
    self.hotSearchArray = [NSMutableArray array];
    //热门搜索
    [self getHotSearch];
    [self setNavigation];
    //创建搜索
    [self search];
    //历史搜索
    [self getHistorySearch];
    [self createTableView];
    //历史搜索
    // Do any additional setup after loading the view.
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    
    [self.view endEditing:YES];
}

- (void)createTableView {
    
       self.sectionTitle = @[@"热门搜索",@"历史搜索"];
    self.searchTableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.searchTableView.delegate = self;
    [self.searchTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone]; 
    self.searchTableView.dataSource = self;
    [self.view addSubview:self.searchTableView];
    self.searchTableView.sd_layout.topSpaceToView(self.searchView, 22)
    .leftSpaceToView(self.view, 15)
    .rightSpaceToView(self.view, 15)
    .widthIs(KScreenW-30).heightIs(KScreenH-80);
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return  101;
    }
    return 50;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    }
    if ( section == 1) {
        return self.historySearchArray.count;
    }
    return 0;

}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    
    return self.sectionTitle.count;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 50;
}
#pragma mark- 禁止section停留
-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    CGFloat sectionHeaderHeight = 50;
    
    if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
        
        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
        
    } else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
        
        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
        
    }
    [self.view endEditing:YES];
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section

{
    self.sectionView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenW, 0)];
    //    self.sectionView.backgroundColor = [UIColor cyanColor];
    //各个模块
    UILabel *sectionTitle = [[UILabel alloc] init];
    sectionTitle.frame = CGRectMake(16,8.5,90,17);
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:[self.sectionTitle objectAtIndex:section] attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Medium" size: 18],NSForegroundColorAttributeName: [UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:1.0]}];
    sectionTitle.attributedText = string;
    [self.sectionView addSubview:sectionTitle];
    
    if (section == 1) {
        
        //更多
        UIButton *gdButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [gdButton setTitle:@"清除" forState:UIControlStateNormal];
        [gdButton addTarget:self action:@selector(clearClick:) forControlEvents:UIControlEventTouchUpInside];
        [gdButton setTitleColor:RGB(23, 151, 164) forState:UIControlStateNormal];
        gdButton.tag = 10000;
        [gdButton.titleLabel setFont:[UIFont systemFontOfSize:14]];
        [self.sectionView addSubview:gdButton];
        gdButton.sd_layout.topSpaceToView(self.sectionView, 8)
        .rightSpaceToView(self.sectionView,  0)
        .widthIs(60).heightIs(16);
    }
    return self.sectionView;
}
- (void)clearClick:(UIButton *)send {
    NSLog(@"清除");
    NSMutableDictionary *celarDic = [NSMutableDictionary dictionary];
    celarDic[@"only_value"] = DIV_UUID;
    [GMAfnTools PostHttpDataWithUrlStr:KhomeClearSearch Dic:celarDic SuccessBlock:^(id  _Nonnull responseObject) {
        if ([[responseObject objectForKey:@"code"]isEqualToString:@"1"]) {
            
            [self.historySearchArray removeAllObjects];
        }
        [self.searchTableView reloadData];
        [self showError:@"清除成功"];
    } FailureBlock:^(id  _Nonnull error) {
        
    }];
}

#pragma mark -- 提示框
- (void)showError:(NSString *)errorMsg {
    // 1.弹框提醒
    // 初始化对话框
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:errorMsg preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:nil]];
    // 弹出对话框
    [self presentViewController:alert animated:true completion:nil];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"searchCell";
    UITableViewCell *cellTable = [tableView dequeueReusableCellWithIdentifier:cellID ];
    
    if (!cellTable) {
        cellTable = [[UITableViewCell alloc]init];
    }
    
    if (indexPath.section == 0 && indexPath.row == 0) {
        self.flowLayout = [[UICollectionViewFlowLayout alloc]init];
        self.flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        self.collectionV = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, self.searchTableView.frame.size.width, 101) collectionViewLayout:self.flowLayout];
        self.collectionV.backgroundColor = GMWhiteColor;
        self.collectionV.delegate = self;
        self.collectionV.dataSource = self;

        [self.collectionV registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"collection"];
        [cellTable addSubview:self.collectionV];
        
    }
    if(indexPath.section == 1) {
        
        HistorySearchModel *searchModel = [self.historySearchArray objectAtIndex:indexPath.row];
        cellTable.textLabel.text = searchModel.keys;
        cellTable.textLabel.textColor = RGB(153, 153, 153);
        cellTable.textLabel.font = [UIFont fontWithName:KPFType size:12];
        UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [closeButton setBackgroundImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
        [closeButton addTarget:self action:@selector(delete:) forControlEvents:UIControlEventTouchUpInside];
        NSInteger searchx = [searchModel.search_id integerValue];
        closeButton.tag = searchx;
        [cellTable addSubview:closeButton];
        closeButton.sd_layout.centerYEqualToView(cellTable)
        .rightSpaceToView(cellTable, 15)
        .widthIs(9).heightIs(9);
        
        UIButton *bgButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [bgButton setBackgroundColor:RGBA(15, 15, 15, 0)];
        [bgButton addTarget:self action:@selector(delete:) forControlEvents:UIControlEventTouchUpInside];
        bgButton.tag = closeButton.tag;
        [cellTable addSubview:bgButton];
        bgButton.sd_layout.topSpaceToView(cellTable, 0)
        .rightSpaceToView(cellTable, 0)
        .widthIs(50).heightIs(cellTable.frame.size.height);
        
    }
    
    
 
    return cellTable;
}
#pragma mark-- 删除历史搜索
- (void)delete:(UIButton *)send  {
    
    NSString *str = [NSString stringWithFormat:@"%ld",send.tag];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"search_id"] = str;
    [GMAfnTools PostHttpDataWithUrlStr:KHomeDeleteSearch Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"xxxxxX%@",responseObject);
        if ([[responseObject  objectForKey:@"code"] isEqualToString:@"1"]) {

            [self getHistorySearch];
        }
    } FailureBlock:^(id  _Nonnull error) {
        
    }];
    [self.searchTableView reloadData];
 
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSLog(@"+++++++%ld",indexPath.row);
    HistorySearchModel *mol = [self.historySearchArray objectAtIndex:indexPath.row];
    [self getGJCSearch:mol.keys onlyV:DIV_UUID];
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    // 先删除数据源
    [self.historySearchArray removeObjectAtIndex:indexPath.row];
    // 再删除cell
    [self.searchTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(collectionView.frame.size.width/6 ,28);
}
#pragma mark -- collectionView delegate , datasource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.hotSearchArray.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"collection" forIndexPath:indexPath];

    cell.layer.cornerRadius = 5.0f;
    cell.layer.borderWidth = 0.08f;
    cell.layer.borderColor = [UIColor lightGrayColor].CGColor;
    cell.layer.masksToBounds = NO;
    cell.backgroundColor = RGB(245, 245, 245);
    HotSearchModel *hotModel = [self.hotSearchArray objectAtIndex:indexPath.row];
    UILabel *hotLabel = [[UILabel alloc]init];
    hotLabel.text = hotModel.keys;
    hotLabel.textColor = RGB(153, 153, 153);
    hotLabel.textAlignment = NSTextAlignmentCenter;
    hotLabel.font = [UIFont fontWithName:KPFType size:12];
    [cell addSubview:hotLabel];
    hotLabel.sd_layout.topEqualToView(0)
    .leftEqualToView(0)
    .widthIs(cell.frame.size.width)
    .heightIs(cell.frame.size.height);
    
 
    
    return cell;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"%ld %ld",indexPath.section,indexPath.row);
    
    HotSearchModel *mol = [self.hotSearchArray objectAtIndex:indexPath.row];
    [self getGJCSearch:mol.keys onlyV:DIV_UUID];
    
    
}
- (void)search {
   self.searchView = [[UIView alloc] init];
    self.searchView.backgroundColor = [UIColor colorWithRed:244/255.0 green:243/255.0 blue:244/255.0 alpha:1.0];
    self.searchView.layer.cornerRadius = 14.5;
//        view.backgroundColor = GMBlueColor;
    [self.view addSubview:self.searchView];
    self.searchView.sd_layout.topSpaceToView(self.view, MStatusBarHeight+7)
    .leftSpaceToView(self.view, 60)
    .rightSpaceToView(self.view, 15)
    .heightIs(30);
    
    //搜索按钮
    self.searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.searchButton setImage:[UIImage imageNamed:@"ico_search"] forState:UIControlStateNormal];
    [self.searchButton addTarget:self action:@selector(searchClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.searchView addSubview:self.searchButton];
    self.searchButton.sd_layout.topSpaceToView(self.searchView, 8)
    .leftSpaceToView(self.searchView, 15)
    .widthIs(16).heightIs(16);
    
    //搜索框
    self.searchText = [[UITextField alloc] init];
    self.searchText.font = [UIFont fontWithName:@"PingFangSC-Regular" size:13];
    self.searchText.textColor = RGB(153, 153, 153);
    self.searchText.placeholder = @"请输入搜索内容";
    self.searchText.returnKeyType = UIReturnKeySearch;
    self.searchText.delegate = self;
    self.searchText.font = [UIFont systemFontOfSize:13];
    //    self.searchText.backgroundColor = GMBrownColor;
    [self.searchView addSubview:self.searchText];
    self.searchText.sd_layout.topSpaceToView(self.view, 0)
    .bottomSpaceToView(self.searchView, 0)
    .leftSpaceToView(self.searchView, 41)
    .widthIs(KScreenW - 120)
    .heightIs(27);
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [self getGJCSearch:self.searchText.text onlyV:DIV_UUID];
    return YES;
}
#pragma mark--关键词搜索
- (void)searchClick:(UIButton *)send {
    NSLog(@"搜索内容");
    //关键词搜索
    [self getGJCSearch:self.searchText.text onlyV:DIV_UUID];
    
//    SearchDetailViewController *search = [[SearchDetailViewController alloc]init];
//    [self.navigationController pushViewController:search animated:YES];
    
}

- (void) setNavigation {
        UIView *titleView = [[UIView alloc]init];
        titleView.backgroundColor = GMWhiteColor;
        [self.view addSubview:titleView];
        titleView.sd_layout.topSpaceToView(self.view, 0)
        .leftEqualToView(self.view)
        .widthIs(KScreenW).heightIs(44+MStatusBarHeight);
        
        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        //    backButton.backgroundColor = GMBlueColor;
        [backButton setBackgroundImage:[UIImage imageNamed:@"back-icon"] forState:UIControlStateNormal];
        [backButton addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
        [titleView addSubview:backButton];
        backButton.sd_layout.topSpaceToView(titleView,MStatusBarHeight+15)
        .leftSpaceToView(titleView, 15)
        .widthIs(11).heightIs(19);
    
        UIButton *backButtonView = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 50, MStatusBarHeight+44)];
        backButtonView.backgroundColor = RGBA(15, 15, 15, 0);
        [backButtonView addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:backButtonView];
    
}

- (void)backBtn {
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark -- 热门搜索
- (void)getHotSearch {
    
    [GMAfnTools PostHttpDataWithUrlStr:KHomeHotSearch Dic:nil SuccessBlock:^(id  _Nonnull responseObject) {
        
        NSLog(@"热门搜索 : %@",responseObject);
        for (NSMutableDictionary *hotSearchDci in responseObject) {
            HotSearchModel *mol = [[HotSearchModel alloc]init];
            [mol mj_setKeyValues:hotSearchDci];
            [self.hotSearchArray addObject:mol];
        }
        [self.searchTableView reloadData];
    } FailureBlock:^(id  _Nonnull error) {
        NSLog(@"热门搜索 : %@",error);
    }];
}
#pragma mark -- 历史搜索
- (void)getHistorySearch{
    self.historySearchArray = [NSMutableArray array];
    
    NSMutableDictionary *searchDic = [NSMutableDictionary dictionary];
    searchDic[@"only_value"] = DIV_UUID;
    NSLog(@"----UUID===%@",DIV_UUID);
    [GMAfnTools PostHttpDataWithUrlStr:KHomeHistorySearch Dic:searchDic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"历史搜索 : %@",responseObject);
        for (NSMutableDictionary *historyDic in responseObject) {
            HistorySearchModel *historyModel = [[HistorySearchModel alloc]init];
            [historyModel mj_setKeyValues:historyDic];
            [self.historySearchArray addObject:historyModel];
        }
        [self.searchTableView reloadData];
    } FailureBlock:^(id  _Nonnull error) {
        NSLog(@"历史搜索搜索 : %@",error);
    }];
}
#pragma mark -- 关键词搜索
- (void)getGJCSearch:(NSString *)keys onlyV:(NSString *)only_value {
    
    NSMutableDictionary *searchDic = [NSMutableDictionary dictionary];
    searchDic[@"keys"] = keys;
    searchDic[@"only_value"] = only_value;
    [GMAfnTools PostHttpDataWithUrlStr:KhomeGJCSearch Dic:searchDic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"关键词搜索结果==%@",responseObject);
        if (responseObject) {
            SearchDetailViewController *detail = [[SearchDetailViewController alloc]init];
            detail.allDictionrySearch = responseObject;
            [detail setHidesBottomBarWhenPushed:YES];
            [self.navigationController pushViewController:detail animated:YES];
        }
    } FailureBlock:^(id  _Nonnull error) {
        NSLog(@"热门搜索 : %@",error);
    }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
