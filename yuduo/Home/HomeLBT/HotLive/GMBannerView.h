//
//  GMBannerView.h
//  GLBTPerfect
//
//  Created by Mac on 2019/7/29.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MGBannerModel.h"

@class GMBannerView;

@protocol GMBannerViewDelegate <NSObject>

@optional
/**
 该方法用来处理item的点击，会返回item在moxing数组中的索引
 
 @param bannerView  控件本身
 @param index       索引
 */
- (void)HZBannerViewTest:(GMBannerView *)bannerView didSelectedAt:(NSInteger)indexGM;

@end

@interface GMBannerView : UIView

#pragma mark - 属性
/**
 *  模型数组
 */
@property (nonatomic, strong) NSArray<MGBannerModel *> *models;
/**
 *  代理，用来处理图片的点击
 */
@property (nonatomic, weak) id <GMBannerViewDelegate> delegate;
/**
 *  每一页停留时间，默认为3s，最少1s
 *  当设置的值小于1s时，则为默认值
 */
@property (nonatomic, assign) NSTimeInterval timeInterval;

#pragma mark - 方法
/**
 *  开启定时器
 *  默认已开启，调用该方法会重新开启
 */
- (void)startTimer;
/**
 *  停止定时器
 *  停止后，如果手动滚动图片，定时器会重新开启
 */
- (void)stopTimer;

@end



