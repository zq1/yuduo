//
//  GMBannerCell.m
//  GLBTPerfect
//
//  Created by Mac on 2019/7/29.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "GMBannerCell.h"
#import "MGBannerModel.h"

@interface GMBannerCell ()
{
    UIImageView *_imageView;
    UILabel *_textLabel;
    
    UIView *_bgView;
    UILabel *_dateLabel;
    UILabel *_zhibo;
    UILabel *_lax;
}
@end

@implementation GMBannerCell

- (instancetype)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        
        self.layer.cornerRadius = 10.0f;
        self.layer.masksToBounds = YES;
//        self.backgroundColor = [UIColor grayColor];
       
        
        [self setupUI];
    }
    return self;
}
// 添加四边阴影效果
- (void)addShadowToView:(UIView *)theView withColor:(UIColor *)theColor {
    // 阴影颜色
    theView.layer.shadowColor = theColor.CGColor;
    // 阴影偏移，默认(0, -3)
    theView.layer.shadowOffset = CGSizeMake(0,0);
    // 阴影透明度，默认0
    theView.layer.shadowOpacity = 0.5;
    // 阴影半径，默认3
    theView.layer.shadowRadius = 5;
}
- (void)setupUI {
    
    UIView *shadowView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenW, 200)];
    shadowView.layer.shadowColor = [UIColor blackColor].CGColor;
    shadowView.layer.shadowOffset = CGSizeMake(0, 3);
    shadowView.layer.shadowOpacity = 0.3;
    shadowView.layer.shadowRadius = 5.0;
    shadowView.layer.cornerRadius = 20.0;
    shadowView.layer.masksToBounds = NO;    //剪裁
    [self addSubview:shadowView];
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenW, 200)];
//    view.backgroundColor = GMRedColor;.
    view.layer.shadowColor = [UIColor blackColor].CGColor;
    view.layer.shadowOffset = CGSizeMake(0, 0);
    view.layer.shadowOpacity = 0.8;
    view.layer.shadowRadius = 9.0;
    view.layer.cornerRadius = 9.0;
    [shadowView addSubview:view];
    CGFloat labelHeight = 41;
    CGFloat imageViewHeight = self.bounds.size.height - labelHeight;
    _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, KScreenW, 150)];
//    _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, imageViewHeight)];
    _imageView.contentMode = UIViewContentModeScaleAspectFill;
    
    _imageView.layer.masksToBounds = true;
    [view addSubview:_imageView];
    
    UILabel *la = [[UILabel alloc]initWithFrame:CGRectMake(0, _imageView.frame.size.height, 300, 41)];
//    la.backgroundColor = GMGreenColor;
    la.backgroundColor = GMWhiteColor;
    la.layer.cornerRadius = 5;
    la.layer.borderWidth = 0.03;
//    la.text = @"正在直播";
    [view addSubview:la];
    _lax = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, 350, 31)];
    //    la.backgroundColor = GMGreenColor;
    _lax.backgroundColor = GMWhiteColor;
    _lax.font = kFont(14);
    _lax.textColor = RGB(23, 151, 164);
//    lax.layer.cornerRadius = 5;
//    lax.layer.borderWidth = 0.03;
    _lax.text = @"正在直播";
    [la addSubview:_lax];
//    [self addSubview:_imageView];
//    _textLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, imageViewHeight, self.bounds.size.width, labelHeight)];
//    _textLabel.textColor = [UIColor colorWithRed:102.0f/255.0f green:102.0f/255.0f blue:102.0f/255.0f alpha:1];
//    _textLabel.font = [UIFont systemFontOfSize:10];
//    //    _textLabel.textAlignment = NSTextAlignmentCenter;
//    _textLabel.adjustsFontSizeToFitWidth = true;
//    [self addSubview:_textLabel];
    _textLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 115, _imageView.frame.size.width, 30)];
    
    _textLabel.backgroundColor = [UIColor blackColor];
    _textLabel.alpha = 0.7;
    _textLabel.font = [UIFont systemFontOfSize:13];
    _textLabel.text = @"课程标题";
    _textLabel.layer.cornerRadius = 15;
    _textLabel.textColor = GMWhiteColor;
    [_imageView addSubview:_textLabel];
    
//
//    _bgView = [[UIView alloc]initWithFrame:CGRectMake(0, imageViewHeight, 300, labelHeight)];
//    _bgView.backgroundColor = GMWhiteColor;
//    [view addSubview:_bgView];
//
//    _bgView.layer.shadowColor = [UIColor blackColor].CGColor;
//    _bgView.layer.shadowOffset = CGSizeMake(0, 0);
//    _bgView.layer.shadowOpacity = 0.8;
//    _bgView.layer.shadowRadius = 9.0;
//    _bgView.layer.cornerRadius = 9.0;
//
//    _zhibo = [[UILabel alloc]initWithFrame:CGRectMake(15, 0, 150, 50)];
//    _zhibo.numberOfLines = 0;
//    [_bgView addSubview:_zhibo];
//
//    _zhibo.layer.shadowColor = [UIColor blackColor].CGColor;
//    _zhibo.layer.shadowOffset = CGSizeMake(0, 0);
//    _zhibo.layer.shadowOpacity = 0.8;
//    _zhibo.layer.shadowRadius = 9.0;
//    _zhibo.layer.cornerRadius = 9.0;
//    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"正在直播" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 14],NSForegroundColorAttributeName: [UIColor colorWithRed:23/255.0 green:151/255.0 blue:164/255.0 alpha:1.0]}];
//    _zhibo.attributedText = string;
//
    _dateLabel = [[UILabel alloc] init];
    _dateLabel.text = @"2019-5-60";
    _dateLabel.font = kFont(13);
    _dateLabel.textAlignment = NSTextAlignmentRight;
    [self addSubview:_dateLabel];
    [_dateLabel sizeToFit];
    _dateLabel.mj_w += 100;
    _dateLabel.mj_y = self.mj_h - _dateLabel.mj_h - 5;
    _dateLabel.mj_x = self.mj_w - _dateLabel.mj_w - 10;
//    [self.contentView bringSubviewToFront:_dateLabel];
}

-(void)setModelx:(MGBannerModel *)modelx
{
    _modelx = modelx;
    
    [_imageView sd_setImageWithURL:[NSURL URLWithString:modelx.picture]];
//    _imageView.image = [UIImage imageNamed:modelx.picture];

    if (modelx.date.doubleValue > 0) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"yyyy-MM-dd";
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:modelx.date.doubleValue];
        _dateLabel.text = [formatter stringFromDate:date];
    } else {
        _dateLabel.text = modelx.date;
    }
    switch (modelx.status) {
            case 0:
            _lax.text = @"无状态";
            break;
        case 1:
            _lax.text = @"未直播";
            break;
        case 2:
            _lax.text = @"直播中";
            break;
        case 3:
            _lax.text = @"直播结束";
            break;
        default:
            break;
    }
    
    _textLabel.text = modelx.title;
    //[_textLabel sizeToFit];
}


@end
