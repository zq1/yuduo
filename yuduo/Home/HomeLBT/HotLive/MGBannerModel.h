//
//  MGBannerModel.h
//  GLBTPerfect
//
//  Created by Mac on 2019/7/29.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface MGBannerModel : NSObject

/**  图片 */
@property (nonatomic, copy) NSString *picture;
/**  标题 */
@property (nonatomic, copy) NSString *title;
/**  时间标题 */
@property (nonatomic, copy) NSString *date;
/// 直播状态
@property (nonatomic, assign) NSInteger status;

@end


