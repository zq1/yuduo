//
//  HZBannerModel.h
//
//  Created by zenghz on 2017/4/18.
//  Copyright © 2017年 Personal. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HZBannerModel : NSObject

/**  图片 */
@property (nonatomic, copy) NSString *picture;
@property (nonatomic , copy)NSString *course_list_cover;
/**  标题 */
@property (nonatomic, copy) NSString *title;
/**  时间标题 */
@property (nonatomic, copy) NSString *date;
//标题
@property (nonatomic, strong) NSString *courseID;

@property (nonatomic, copy) NSString *isFree;

@end
