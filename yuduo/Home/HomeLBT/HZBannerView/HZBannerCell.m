//
//  HZBannerCell.m
//
//  Created by zenghz on 2017/4/18.
//  Copyright © 2017年 Personal. All rights reserved.
//

#import "HZBannerCell.h"
#import "HZBannerModel.h"

@interface HZBannerCell ()
{
    UILabel *_textLabel;
    UILabel *_zhibo;
    UILabel *_label;
    UILabel *_FreeLabel;
}
PropertyStrong(UIImageView, imageView);
@end

@implementation HZBannerCell

- (instancetype)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        
        self.layer.cornerRadius = 10.0f;
        self.layer.masksToBounds = YES;
        self.backgroundColor = [UIColor grayColor];
   
        
        [self setupUI];
    }
    return self;
}

- (void)setupUI {
    
    CGFloat labelHeight = 81-24;
    CGFloat imageViewHeight = self.bounds.size.height - labelHeight;
    
    UIView *shadowView = [[UIView alloc]initWithFrame:CGRectMake(0, 18, 200, 120)];
    shadowView.layer.shadowColor = [UIColor blackColor].CGColor;
    shadowView.layer.shadowOffset = CGSizeMake(0, 3);
    shadowView.layer.shadowOpacity = 0.3;
    shadowView.layer.shadowRadius = 5.0;
    shadowView.layer.cornerRadius = 20.0;
    //剪裁
    [self addSubview:shadowView];
    _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 199, 119)];
    _imageView.layer.cornerRadius = 10;
    _imageView.contentMode = UIViewContentModeScaleAspectFill;
    _imageView.layer.masksToBounds = true;
    [shadowView addSubview:_imageView];
    
    _textLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, _imageView.frame.size.height+19, self.bounds.size.width, labelHeight)];
    _textLabel.textColor = [UIColor colorWithRed:102.0f/255.0f green:102.0f/255.0f blue:102.0f/255.0f alpha:1];
    _textLabel.font = [UIFont systemFontOfSize:10];
    _textLabel.adjustsFontSizeToFitWidth = true;
    [self addSubview:_textLabel];
    
    _label = [[UILabel alloc]initWithFrame:CGRectMake(0, 19, 225, 14)];
    _label.numberOfLines = 0;
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"儿童医生支招儿童辅食营养" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 14],NSForegroundColorAttributeName: [UIColor colorWithRed:69/255.0 green:69/255.0 blue:69/255.0 alpha:1.0]}];
    _label.attributedText = string;
    [_textLabel addSubview:_label];
    
//    UIView *view = [[UIView alloc] init];
//    view.frame = CGRectMake(0,label.frame.origin.y+22,46,16);
//
//    view.layer.cornerRadius = 3;
//    [_textLabel addSubview:view];
    
    _FreeLabel = [[UILabel alloc] init];
    _FreeLabel.backgroundColor = GMWhiteColor;
    _FreeLabel.layer.borderWidth = 1;
    _FreeLabel.layer.borderColor = RGB(21, 152, 164).CGColor;
    _FreeLabel.textColor = RGB(21, 152, 164);
    _FreeLabel.textAlignment = NSTextAlignmentCenter;
    _FreeLabel.frame = CGRectMake(5  ,_label.frame.origin.y+22,46,16);
    
    _FreeLabel.textAlignment = NSTextAlignmentCenter;
    _FreeLabel.numberOfLines = 0;
    [_textLabel addSubview:_FreeLabel];
    
    NSMutableAttributedString *stringTime = [[NSMutableAttributedString alloc] initWithString:@"限时免费" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 9],NSForegroundColorAttributeName: [UIColor colorWithRed:21/255.0 green:152/255.0 blue:164/255.0 alpha:1.0]}];
    
    _FreeLabel.attributedText = stringTime;
//    _zhibo = [[UILabel alloc]initWithFrame:CGRectMake(100, imageViewHeight, 100, labelHeight)];
//    _zhibo.backgroundColor = [UIColor blueColor];
//    [self addSubview:_zhibo];

    //图片横幅文字
//    UILabel  *llll = [[UILabel alloc]initWithFrame:CGRectMake(0, 100, 150, 50)];
//    llll.backgroundColor = [UIColor blackColor];
//    [_imageView addSubview:llll];
    
    
}

- (void)setModel:(HZBannerModel *)model{
    
    _model = model;
    
    _imageView.image = [UIImage imageNamed:model.picture];
    _label.text = model.title;
//    轮播图赋值
    [_imageView sd_setImageWithURL:[NSURL URLWithString:model.picture]];
    _FreeLabel.hidden = ![model.isFree isEqualToString:@"1"];

//    _textLabel.text = model.title;
}


@end
