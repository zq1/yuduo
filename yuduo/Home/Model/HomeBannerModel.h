//
//  HomeBannerModel.h
//  yuduo
//
//  Created by Mac on 2019/8/20.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeBannerModel : NSObject
PropertyStrong(NSString, status);
PropertyStrong(NSString, sort);
PropertyStrong(NSString, xid);
PropertyStrong(NSString, title);
PropertyStrong(NSString, picture);
PropertyStrong(NSString, create_time);
PropertyStrong(NSString, url);


@end

NS_ASSUME_NONNULL_END
