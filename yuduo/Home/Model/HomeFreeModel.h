//
//  HomeFreeModel.h
//  yuduo
//
//  Created by Mac on 2019/8/20.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeFreeModel : NSObject
PropertyStrong(NSString, course_free_cover);
PropertyStrong(NSString, course_list_cover);
PropertyStrong(NSString, course_name);
PropertyStrong(NSString, course_id);
PropertyStrong(NSString, class_hour);
PropertyStrong(NSString, course_vrows);
PropertyStrong(NSString, course_details);
PropertyStrong(NSString, brief);

@end

NS_ASSUME_NONNULL_END
