//
//  SysMsgListModel.h
//  yuduo
//
//  Created by Mac on 2019/9/22.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface SysMsgListModel : BaseModel
PropertyStrong(NSString, mgs_id);
PropertyStrong(NSString, title);
@end

NS_ASSUME_NONNULL_END
