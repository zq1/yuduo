//
//  ZXHFDetailModel.h
//  yuduo
//
//  Created by mason on 2019/9/17.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZXHFDetailModel : BaseModel
PropertyStrong(NSString, manag_reply);
PropertyStrong(NSString, manag_reply_time);
PropertyStrong(NSString, manag_id);
PropertyStrong(NSString, type);
PropertyStrong(NSString, manipulate);

PropertyStrong(NSString, portrait);
@end

NS_ASSUME_NONNULL_END
