//
//  ActiveClassListModel.h
//  yuduo
//
//  Created by 刘耀聪 on 2019/10/24.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ActiveClassListModel : NSObject
PropertyStrong(NSNumber, category_id);
PropertyStrong(NSString, title);
@end

NS_ASSUME_NONNULL_END
