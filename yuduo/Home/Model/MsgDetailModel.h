//
//  MsgDetailModel.h
//  yuduo
//
//  Created by Mac on 2019/9/22.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MsgDetailModel : BaseModel
PropertyStrong(NSString, content);
PropertyStrong(NSString, title);
PropertyStrong(NSString, create_time);
@end

NS_ASSUME_NONNULL_END
