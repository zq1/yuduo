//
//  HomeNewCourseModel.h
//  yuduo
//
//  Created by Mac on 2019/8/20.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface HomeNewCourseModel : BaseModel
PropertyStrong(NSString, class_hour);
PropertyStrong(NSString, course_id);//最新课程详情
PropertyStrong(NSString, course_list_cover);
PropertyStrong(NSString, course_name);
PropertyStrong(NSString, course_price);
PropertyStrong(NSString, course_under_price);
PropertyStrong(NSString, course_vrows);
PropertyStrong(NSString, is_free);
PropertyStrong(NSString, member_is_free);
PropertyStrong(NSString, brief);
@end

NS_ASSUME_NONNULL_END
