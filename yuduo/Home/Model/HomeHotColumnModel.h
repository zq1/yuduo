//
//  HomeHotColumnModel.h
//  yuduo
//
//  Created by Mac on 2019/8/26.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface HomeHotColumnModel : BaseModel
PropertyStrong(NSString, course_vrows);//课时
PropertyStrong(NSString, course_price); //专栏最热课程id
PropertyStrong(NSString, gement_id);
PropertyStrong(NSString, start_date);//标题
PropertyStrong(NSString, course_list_cover);
PropertyStrong(NSString, course_free_cover);
PropertyStrong(NSString, member_is_free);//最新价格
PropertyStrong(NSString, class_hour);
PropertyStrong(NSString, course_under_price);
PropertyStrong(NSString, end_date);
PropertyStrong(NSString, course_name);
PropertyStrong(NSString, is_free);
@end

NS_ASSUME_NONNULL_END
