//
//  HotZXModel.h
//  yuduo
//
//  Created by Mac on 2019/9/4.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface HotZXModel : BaseModel
PropertyStrong(NSString, manag_id);
PropertyStrong(NSString, manag_title);
PropertyStrong(NSString, manag_picture);
PropertyStrong(NSString, thumbsup);
PropertyStrong(NSString, course_list_cover);
PropertyStrong(NSString, browse);
PropertyStrong(NSString, manag_time);






@end

NS_ASSUME_NONNULL_END
