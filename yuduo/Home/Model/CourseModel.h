//
//  CourseModel.h
//  yuduo
//
//  Created by Mac on 2019/9/24.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CourseModel : BaseModel
PropertyStrong(NSString , course_hour_trysee);
PropertyStrong(NSString, course_hour_upload_time);
PropertyStrong(NSString, course_hour_type);
PropertyStrong(NSString, course_hour_free);
PropertyStrong(NSString, course_hour_name);
PropertyStrong(NSString, course_hour_video);
PropertyStrong(NSString, course_hour_trytime);
PropertyStrong(NSString, course_hour_video_audio_name);
PropertyStrong(NSString, course_hour_time);
PropertyStrong(NSString, course_id);
PropertyStrong(NSString, course_hour_id);
@end

NS_ASSUME_NONNULL_END
