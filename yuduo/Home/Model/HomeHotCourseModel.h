//
//  HomeHotCourseModel.h
//  yuduo
//
//  Created by Mac on 2019/8/20.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeHotCourseModel : NSObject
PropertyStrong(NSString, course_name);
PropertyStrong(NSString, course_list_cover);
PropertyStrong(NSString, course_price);
PropertyStrong(NSString, start_date);
PropertyStrong(NSString, member_is_free);
PropertyStrong(NSString, course_under_price);
PropertyStrong(NSString, end_date);
PropertyStrong(NSString, is_free);
PropertyStrong(NSString, course_id);
PropertyStrong(NSString, class_hour);
PropertyStrong(NSString, course_vrows); //浏览量
PropertyStrong(NSString, brief);

@end

NS_ASSUME_NONNULL_END
