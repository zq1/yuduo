//
//  YDSelectModel.h
//  yuduo
//
//  Created by Mac on 2019/8/20.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface YDSelectModel : NSObject
PropertyStrong(NSString, article_cover); //图片
PropertyStrong(NSString, article_title); //标题
PropertyStrong(NSString, article_is_friends_buys);//朋友帮买
PropertyStrong(NSString, article_member_is_free);//会员免费
PropertyStrong(NSString, article_is_free);
PropertyStrong(NSString, thumbs_up);
PropertyStrong(NSString, article_vrows);
PropertyStrong(NSString, article_id);
// 2019.10.23加入
PropertyStrong(NSString, brief);//简介
PropertyStrong(NSString, comment_count);//评论数
PropertyStrong(NSString, article_price);//价格



@end

NS_ASSUME_NONNULL_END
