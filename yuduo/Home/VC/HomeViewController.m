//
//  HomeViewController.m
//  yuduo
//
//  Created by Mac on 2019/7/23.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "HomeViewController.h"
#import "NewCourseTableViewCell.h"
#import "NewCourseTwoTableViewCell.h"
#import "ColumnsTableViewCell.h"
#import "MianFeiCourseTableViewCell.h"
#import "HotLiveTableViewCell.h"
#import "HotConsultTableViewCell.h"
#import "JingxuanTableViewCell.h"
#import "HZBannerView.h"
#import "HZBannerModel.h"
#import "GMBannerView.h"
#import "MGBannerModel.h"
//消息
#import "MessageViewController.h"
#import "DetailViewController.h"
//最新课程轮播详情
#import "CoureDetailViewController.h"
//专栏详情
#import "HomeColumnDetailViewController.h"
#import "NewColumnsCollectionViewCell.h"
#import "ActiveViewController.h"
//课程列表
#import "CourseListViewController.h"
//直播
#import "HomeLiveViewController.h"
//咨询
#import "HomeZXViewController.h"
//
#import "DetailViewController.h"
#import "CourseListViewController.h"
#import "NewZLViewController.h"
//免费课程列表
#import "MianFeiDetailViewController.h"

//课程详情
#import "CoureDetailViewController.h"
//育朵精选
#import "NewYuDuoChoiceDetailViewController.h"
#import "JXdetailViewController.h"
//热门咨询详情
#import "HomeZXDetailViewController.h"
#import "HomeHoliveViewController.h"
/*---------------数据模型-------------------------*/
#import "HomeBannerModel.h"//轮播图
#import "HomeHotCourseModel.h"//最热课程
#import "HomeNewCourseModel.h" //最新课程
#import "HomeHotColumnModel.h" //最热专栏
#import "HomeColumnModel.h" //最热专栏 首页列表
#import "HomeFreeModel.h"  //免费专区
#import "HotZXModel.h"
#import "YDSelectModel.h" //育朵精选
#import "CourseLiveListsModel.h" //直播列表
#import "ZXDetailViewController.h"
#import "SearchViewController.h"
#import "NewZhiboDetailViewController.h"
#import "SDCycleScrollView.h"
#import "yuduo-Swift.h"
#import "UIImageView+WebCache.h"
@interface HomeViewController ()<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate,HZBannerViewDelegate,GMBannerViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,SDCycleScrollViewDelegate>
{
    NSArray *_imagesURLStrings;
    SDCycleScrollView *_customCellScrollViewDemo;
}

@property (nonatomic, strong) SDCycleScrollView *bannerCycleScrollerView;

@property(nonatomic,strong)UIView *sectionView;
@property(nonatomic,strong)NSMutableArray *homeCourseArray;//最新课程
@property(nonatomic,strong)NSMutableArray *homeHotCourseArray;//最热课程
@property(nonatomic,strong)NSMutableArray *homeBannerArray;//轮播图
@property(nonatomic,strong)NSMutableArray *homeHotColumnArray;//最热专栏
@property(nonatomic,strong)NSMutableArray *homeFreeArray;//免费专区
@property(nonatomic,strong)NSMutableArray *homeHotZXArray;//热门咨询
@property(nonatomic,strong)NSMutableArray *homeYDSelectArray;//育朵精选
@property(nonatomic,strong)NSMutableArray *courseLiveArray;//热门直播

PropertyStrong(NSMutableArray,  models);

PropertyStrong(NSString, couseIDString);
@end
//底部播放view
//static CGFloat FootHeight = 80;

// typedef NS_ENUM(NSInteger , HomeCellType) {
//     NewCourseTableViewCell1 = 0,
//     NewCourseTwoTableViewCell2,
//     ColumnsTableViewCell3,
//     MianFeiCourseTableViewCell4,
//     HotLiveTableViewCell5,
//     HotConsultTableViewCell6,
// };

@implementation HomeViewController

/**
 *  初始化按钮
 *
 *  @param frame 尺寸
 *  @param title 标题
 *  @param aSEL  按钮的方法
 */
- (void)initializeButtonWithFrame:(CGRect)frame title:(NSString*)title action:(SEL)aSEL{
    
    self.btn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.btn.frame = frame;
    
    //        [self.btn setBackgroundColor:RGB(21, 152, 164)];
    [self.btn setBackgroundImage:[UIImage imageNamed:@"zhiding"] forState:UIControlStateNormal];
    [self.btn setTitle:title forState:0];
    [self.btn addTarget:self action:aSEL forControlEvents:UIControlEventTouchUpInside];
    self.btn.layer.cornerRadius = 25;
    [self.view addSubview:self.btn];
    
}
- (void)scrollToTop:(UIButton*)sender{
    sender.hidden = YES;
    //    [self.view setContentOffset:CGPointMake(0, 20) animated:Y÷ES];
    //    [self.HomeTableView setContentOffset:CGPointMake(0, -34) ];
    // (statusbar)
    //判断状态栏高度
    CGRect rectOfStatusbar = [[UIApplication sharedApplication] statusBarFrame];
    if (rectOfStatusbar.size.height == 20) {
        [self.HomeTableView setContentOffset:CGPointMake(0, -20)];
    }
    else {
        [self.HomeTableView setContentOffset:CGPointMake(0, -44)];
    }
    NSLog(@"statusbar height: %f", rectOfStatusbar.size.height);   // 高度
    NSLog(@"滚到顶部");
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    self.btn.hidden  = YES;
    CGPoint point=scrollView.contentOffset;
    if (point.y  > 800) {
        //按钮frame设置
        [self initializeButtonWithFrame:CGRectMake(KScreenW-50, KScreenH-200,50, 50) title:@"滚到顶部" action:@selector(scrollToTop:)];
    }
    //打印坐标
    //    NSLog(@"%f,%f",point.x,point.y);
}

#pragma mark ---- 将时间戳转换成时间
//参数:时间戳
//返回值格式:2019-04-19 10:33:35.886
- (NSString *)getTimestamp:(NSString*)mStr{
    
    NSTimeInterval interval = [mStr doubleValue] / 1000.0;
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:interval];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    //    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss.SSS"];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Beijing"]];
    NSString *dateString      = [formatter stringFromDate: date];
    NSLog(@"时间戳对应的时间是:%@",dateString);
    
    return dateString;
    
}

//获取当前时间 （以毫秒为单位）
//返回值格式:2019-04-19 10:33:35.886
- (NSString *)getNowTimeTimestamp{
    
    NSDate *datenow = [NSDate date];//现在时间,你可以输出来看下是什么格式
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss.SSS"];
    
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Beijing"]];
    
    NSString *dateString      = [formatter stringFromDate: datenow];
    
    NSLog(@"当前时间戳对应的时间是:%@",dateString);
    
    return dateString;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"+++++%@",DIV_UUID);
    NSUserDefaults *xx = [NSUserDefaults standardUserDefaults];
    NSString *str = [xx objectForKey:@"user_id"];
    NSLog(@"xxxxxxxxx打印user%@",str);
    //创建tabview
    [self creatHomeTabView];
    self.sectionTitleArray = @[@"最热课程",@"最新课程",@"最热专栏",@"免费专区",@"热门直播",@"热门咨询",@"育朵精选"];
    /*-----------------搜索-------------------**/
    [self searchButton];
    /*-----------------消息按钮----------------*/
    [self messageBtn];
    //轮播图
    [self getDataHomeBanner];
    //最热课程
    [self getDataHomeHotCourse];
    //最新课程
    [self getDataHomeNewCourse];
    //最热专栏
    [self getdataHomeHotColumn];
    //免费专区
    [self getdataHomeFree];
    //热门直播
    [self getCourseLiveData];
    //热门咨询
    [self getdataHomeHotZX];
    //育朵精选
    [self getdataHomeHotYDSelect];
    //隐藏navigation
    self.navigationController.navigationBarHidden = YES;
    [UIApplication sharedApplication].statusBarHidden = NO;
    [[UITabBar appearance] setBackgroundImage:[UIImage new]];
    // Do any additional setup after loading the view.
    
}
#pragma mark 搜索按钮

- (void)searchButton {
    __weak typeof(self) weakSelf = self;
    self.headBGView.searButtonViewBlock = ^(NSInteger viewButtonTag) {
        SearchViewController *searchVC = [[SearchViewController alloc]init];
        [searchVC setHidesBottomBarWhenPushed:YES];
        [weakSelf.navigationController pushViewController:searchVC animated:YES];
    };
}
#pragma mark 消息按钮

- (void)messageBtn {
    __weak typeof(self) weakSelf = self;
    self.headBGView.messageBlock = ^(NSInteger messageIdex) {
        NSLog(@"点击了消息按钮");
        MessageViewController *messVC = [[MessageViewController alloc]init];
        [messVC setHidesBottomBarWhenPushed:YES];
        [weakSelf.navigationController pushViewController:messVC animated:YES];
        
    };
    
}
#pragma mark 活动课程直播点击事件
- (void)activityLiveBtn:(NSInteger)tag {
    switch (tag) {
        case 10000:
        {
            NSLog(@"活动");
            
            ActiveViewController *activeVC = [[ActiveViewController alloc]init];
            [activeVC setHidesBottomBarWhenPushed:YES];
            //activeVC.navigationController.navigationBar.hidden = NO;
            
            
            [self.navigationController pushViewController:activeVC animated:NO];
        }
            break;
        case 10001:
        {
            //点击了课程
            CourseListViewController *courseDetail = [[CourseListViewController alloc]init];
            [courseDetail setHidesBottomBarWhenPushed:YES];
            [self.navigationController pushViewController:courseDetail animated:YES];
            NSLog(@"课程");
        }
            break;
        case 10002:
        {
            NSLog(@"直播");
            HomeLiveViewController *homeLive = [[HomeLiveViewController alloc]init];
            homeLive.view.backgroundColor = RGB(244, 243, 244);
            [homeLive setHidesBottomBarWhenPushed:YES];
            [self.navigationController pushViewController:homeLive animated:YES];
        }
            break;
        case 10003:
        {
            NSLog(@"咨询");
            HomeZXViewController *homeZX = [[HomeZXViewController alloc]init];
            homeZX.view.backgroundColor = RGBA(244, 243, 244, 1);
            [homeZX setHidesBottomBarWhenPushed:YES];
            [self.navigationController pushViewController:homeZX animated:YES];
        }
            break;
        default:
            break;
    }
}
#pragma mark 创建TableView
- (void)creatHomeTabView {
    self.headBGView = [[HomeHeadView alloc]initWithFrame:CGRectMake(0, 0, KScreenW, 320)];
    //活动课程直播咨询
    __weak typeof(self) weakSelfFour = self;
    self.headBGView.fourBlock = ^(NSInteger indexBtn) {
        [weakSelfFour activityLiveBtn:indexBtn];
    };
    
    self.HomeTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, KScreenW, KScreenH)style:UITableViewStyleGrouped];
    [self.HomeTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.HomeTableView.delegate = self;
    self.HomeTableView.dataSource = self;
    self.HomeTableView.scrollsToTop = NO;
    self.HomeTableView.backgroundColor = GMWhiteColor;
    self.HomeTableView.tableHeaderView = self.headBGView;
    self.HomeTableView.backgroundView = nil;
    self.HomeTableView.backgroundColor = [UIColor clearColor];
    [self.HomeTableView addSubview:self.headBGView];
    self.HomeTableView.clipsToBounds = YES;
    self.HomeTableView.layer.masksToBounds = YES;
    [self.view addSubview:self.HomeTableView];
    self.HomeTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        //轮播图
        [self getDataHomeBanner];
        //最热课程
        [self getDataHomeHotCourse];
        //最新课程
        [self getDataHomeNewCourse];
        //最热专栏
        [self getdataHomeHotColumn];
        //免费专区
        [self getdataHomeFree];
        //热门直播
        [self getCourseLiveData];
        //热门咨询
        [self getdataHomeHotZX];
        //育朵精选
        [self getdataHomeHotYDSelect];
    }];
    
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    view.frame = CGRectMake(0, 0, KScreenW, 20);
    [self.view addSubview:view];
    
    NSArray *imagesURLStrings = @[
                                  @"http://yuduo.oss-cn-beijing.aliyuncs.com/image/709550e6e063eddf73a05965f0ed163c.jpg",
                                  @"http://yuduo.oss-cn-beijing.aliyuncs.com/image/e3332348a2d45444c9e4699ec5579e3f.jpg"
                                 
                                  ];
    _imagesURLStrings = imagesURLStrings;
    
    // 网络加载 ---
    UIView *view1 = [[UIView alloc]initWithFrame:CGRectMake(15, 45, KScreenW-30, 175)];
//    view1.layer.backgroundColor = GMBlackColor.CGColor;
//            NSString *str = [NSString stringWithFormat:@" %ld",pageIndex+1];
//            NSString *sumLabel = @"/3";
//            view.text = [str stringByAppendingString:sumLabel];
            view1.layer.cornerRadius = 9;
    [self.HomeTableView addSubview:view1];
//            view1.textColor = GMWhiteColor;
    
    self.bannerCycleScrollerView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, KScreenW-30, 175) delegate:self placeholderImage:[UIImage imageNamed:@"placeholder"]];
    self.bannerCycleScrollerView.layer.cornerRadius = 10;
    self.bannerCycleScrollerView.layer.masksToBounds = YES;
    self.bannerCycleScrollerView.currentPageDotImage = [UIImage imageNamed:@"pageControlCurrentDot"];
    
    self.bannerCycleScrollerView.pageDotImage = [UIImage imageNamed:@"pageControlDot"];
    self.bannerCycleScrollerView.imageURLStringsGroup = imagesURLStrings;
    [view1 addSubview:self.bannerCycleScrollerView];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    switch (indexPath.section) {
        case 1:
        {
            CoureDetailViewController *coureDetailVC = [[CoureDetailViewController alloc]init];
            HomeNewCourseModel *model = [self.homeCourseArray objectAtIndex:indexPath.row];
            NSLog(@"课程详情ID==%@",model.course_id);
            //课程详情ID
            coureDetailVC.courseDetailIDString = model.course_id;
            //            NSLog(@"---------987987%@-",coureDetailVC.courseDetailIDString);
            //课程详情标题
            coureDetailVC.courseTitle = model.course_name;
            [coureDetailVC setHidesBottomBarWhenPushed:YES];
            [self.navigationController pushViewController:coureDetailVC animated:YES];
        }
            break;
            
        case 3:
        {
            HomeFreeModel *modelx = [self.homeFreeArray objectAtIndex:indexPath.row];
            CoureDetailViewController *coureDetailVCcc = [[CoureDetailViewController alloc]init];
            coureDetailVCcc.courseDetailIDString = modelx.course_id;
            
            NSLog(@"打印免费专区----课程ID--%@",modelx);
            [coureDetailVCcc setHidesBottomBarWhenPushed:YES];
            [self.navigationController pushViewController:coureDetailVCcc animated:YES];
        }
            break;
            //        case 4:
            //        {
            //            HomeFreeModel *modelx = [self.homeFreeArray objectAtIndex:indexPath.row];
            //            CoureDetailViewController *coureDetailVCcc = [[CoureDetailViewController alloc]init];
            //            coureDetailVCcc.courseDetailIDString = modelx.course_id;
            //            NSLog(@"打印免费专区----课程ID--%@",modelx);
            //            [coureDetailVCcc setHidesBottomBarWhenPushed:YES];
            //            [self.navigationController pushViewController:coureDetailVCcc animated:YES];
            //        }
            //            break;
            
        case 5:
        {
            HotZXModel *mol = [self.homeHotZXArray objectAtIndex:indexPath.row];
            ZXDetailViewController *homeZXDetail = [[ZXDetailViewController alloc]init];
            homeZXDetail.zxIDString = mol.manag_id;
            homeZXDetail.zxtitleString = mol.manag_title;
            homeZXDetail.zxtimeString = mol.manag_time;
            homeZXDetail.zxpicString = mol.manag_picture;
            homeZXDetail.likeNumber = mol.thumbsup;
            [homeZXDetail setHidesBottomBarWhenPushed:YES];
            [self.navigationController pushViewController:homeZXDetail animated:YES];
        }
            break;
        case 6:
        {
            NewYuDuoChoiceDetailViewController *homeJX = [[NewYuDuoChoiceDetailViewController alloc]init];
            YDSelectModel *mol = [self.homeYDSelectArray objectAtIndex:indexPath.row];
            homeJX.articleStringID = mol.article_id;
            homeJX.articleTitle = mol.article_title;
            [homeJX setHidesBottomBarWhenPushed:YES];
            
            [self.navigationController pushViewController:homeJX animated:NO];
        }
        default:
            break;
    }
}

- (void)clickImage {
    //选择图片
    NSLog(@"xxxxxxx%ld",self.selectIndex);
}

#pragma mark -返回行高
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0:
            return 259;
            //            return 500;
            break;
        case 1:
            return 152;
        case 2:
            return 486;
        case 3:
            return 158;
        case 4:
            return 220;
        case 5:
            return 90;
        case 6:
            return 110;
            break;
            
    }
    return 0;
    
}
#pragma mark -- tabviewMethod
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return 1;
            break;
            
        case 1:
            return self.homeCourseArray.count;
        case 2:
            return 1;
        case 3:
            return self.homeFreeArray.count;
        case 5:
            return self.homeHotZXArray.count;
        case 6:
            return self.homeYDSelectArray.count;
        default:
            break;
    }
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    [tableView.mj_header endRefreshing];
    return self.sectionTitleArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 31;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section

{
    self.sectionView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenW, 0)];
    //各个模块
    UILabel *sectionTitle = [[UILabel alloc] init];
    sectionTitle.frame = CGRectMake(16,8.5,90,17);
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:[self.sectionTitleArray objectAtIndex:section] attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Medium" size: 18],NSForegroundColorAttributeName: [UIColor colorWithRed:23/255.0 green:151/255.0 blue:164/255.0 alpha:1.0]}];
    sectionTitle.attributedText = string;
    [self.sectionView addSubview:sectionTitle];
    
    //更多
    UIButton *gdButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [gdButton setTitle:@"更多" forState:UIControlStateNormal];
    [gdButton addTarget:self action:@selector(moreClick:) forControlEvents:UIControlEventTouchUpInside];
    gdButton.tag = section;
    [gdButton setTitleColor:GMlightGrayColor forState:UIControlStateNormal];
    [gdButton.titleLabel setFont:[UIFont systemFontOfSize:14]];
    [self.sectionView addSubview:gdButton];
    gdButton.sd_layout.topSpaceToView(self.sectionView, 8)
    .rightSpaceToView(self.sectionView, 0 )
    .widthIs(100).heightIs(16);
    //按钮
    UIButton *moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [moreButton setImage:[UIImage imageNamed:@"ico_next"] forState:UIControlStateNormal];
    [moreButton addTarget:self action:@selector(moreClick:) forControlEvents:UIControlEventTouchUpInside];
    moreButton.tag = section;
    [self.sectionView addSubview:moreButton];
    moreButton.sd_layout.topSpaceToView(self.sectionView, 8.5)
    .rightSpaceToView(self.sectionView, 15).widthIs(8).heightIs(14);
    
    if (section == 6) {
        
        UILabel *bgLabel = [[UILabel alloc]init];
        bgLabel.backgroundColor = GMWhiteColor;
        [self.sectionView addSubview:bgLabel];
        bgLabel.sd_layout.topSpaceToView(self.sectionView, 0)
        .rightSpaceToView(self.sectionView, 0)
        .widthIs(80)
        .heightIs(31);
        
    }
    return self.sectionView;
}

- (void)moreClick:(UIButton *)send {
    
    switch (send.tag) {
        case 0:
        {
            CourseListViewController *detailVC = [[CourseListViewController alloc]init];
            [detailVC setHidesBottomBarWhenPushed:YES];
            [self.navigationController pushViewController:detailVC animated:YES];
        }
            break;
        case 1:
        {
            CourseListViewController *newDetail = [[CourseListViewController alloc]init];
            [newDetail setHidesBottomBarWhenPushed:YES];
            [self.navigationController pushViewController:newDetail animated:YES];
        }
            break;
        case 2:
        {
            NewZLViewController *newZLVC = [[NewZLViewController alloc]init];
            newZLVC.title = @"阿斯蒂芬";
            
            [newZLVC setHidesBottomBarWhenPushed:YES];
            newZLVC.navigationController.navigationBar.topItem.title = @"asdfasdxxx";
            [self.navigationController pushViewController:newZLVC animated:YES];
            
        }
            break;
        case 3:
        {
            MianFeiDetailViewController *mfVC = [[MianFeiDetailViewController alloc]init];
            [mfVC setHidesBottomBarWhenPushed:YES];
            mfVC.view.backgroundColor = RGB(244, 243, 244);
            [self.navigationController pushViewController:mfVC animated:YES];
        }
            break;
        case 4:
        {
            //HomeHoliveViewController *homeLiveVC = [[HomeHoliveViewController alloc]init];
            HomeLiveViewController * homeLiveVC = [[HomeLiveViewController alloc] init];
            [homeLiveVC setHidesBottomBarWhenPushed:YES];
            [self.navigationController pushViewController:homeLiveVC animated:YES];
            
            
        }
            break;
        case 5:
        {
            HomeZXViewController *zxVC = [[HomeZXViewController alloc]init];
            [zxVC setHidesBottomBarWhenPushed:YES];
            [self.navigationController pushViewController:zxVC animated:YES];
        }
        default:
            break;
    }
    
    
    
    NSLog(@"更多");
}
+(UIImage *) getImageFromURL:(NSString *)fileURL

{
    UIImage * result;
    
    NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:fileURL]];
    
    result = [UIImage imageWithData:data];
    
    return result;
}
#pragma mark--datasource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //
    static NSString *cellIdentifier0 = @"cell0";
    static NSString *cellIdentifier1 = @"cell1";
    static NSString *cellIdentifier2 = @"cell2";
    static NSString *cellIdentifier3 = @"cell3";
    static NSString *cellIdentifier4 = @"cell4";
    static NSString *cellIdentifier5 = @"cell5";
    static NSString *cellIdentifier6 = @"cell6";
    
    switch (indexPath.section){
            // 最新课程1
        case 0:
        {
            NewCourseTableViewCell *cell0 = [[NewCourseTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier0];
            cell0.selectionStyle = UITableViewCellSelectionStyleNone;
            NSMutableArray *arr = [NSMutableArray array];
            for (HomeHotCourseModel *mol in self.homeHotCourseArray) {
                NSMutableDictionary *dic = [NSMutableDictionary dictionary];
                [dic setObject:mol.course_list_cover forKey:@"picture"];
                [dic setObject:mol.course_name forKey:@"title"];
                [dic setObject:mol.course_id forKey:@"courseID"];
                // 限时免费
                [dic setObject:mol.is_free forKey:@"isFree"];
                //                    //限时免费
                ////                    dic setObject:mol.cou forKey:<#(nonnull id<NSCopying>)#>
                [arr addObject:dic];
            }
            //                NSDictionary *xxxx = @{@"picture":@"http://yuduo.oss-cn-beijing.aliyuncs.com/70bdce5697c082cf6623ad5adf43e777.jpg"};
            //                NSArray *arr = @[xxxx];
            _models = @[].mutableCopy;
            for (NSDictionary *dic in arr) {
                HZBannerModel *model = [HZBannerModel new];
                [model setValuesForKeysWithDictionary:dic];
                NSLog(@"-------hahaha%@",model);
                [_models addObject:model];
            }
            
            HZBannerView *banner = ({
                //                    CGFloat width = self.view.bounds.size.width;
                HZBannerView *banner = [[HZBannerView alloc] initWithFrame:CGRectMake(0, 0,KScreenW, 219)];
                banner.models = _models;
                banner.delegate = self;
                banner;
            });
            [cell0.imageBgView addSubview:banner];
            
            return cell0;
        }
            break;
            //最新课程2
        case 1:
        {
            NewCourseTwoTableViewCell *cell1 = [[NewCourseTwoTableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier1];
            //                cell1.backgroundColor = [UIColor redColor];
            cell1.selectionStyle = UITableViewCellSelectionStyleNone;;
            HomeNewCourseModel *homeModel = [self.homeCourseArray objectAtIndex:indexPath.row];
            
            NSString *xpricelabel = [@"¥" stringByAppendingString:homeModel.course_price];
            NSString *pricelabel = [@"¥" stringByAppendingString:homeModel.course_under_price];
            cell1.titleLabel.text = homeModel.course_name;
            
            cell1.xPriceLabel.text = pricelabel;
            cell1.priceLabel.text = xpricelabel;
            NSString *keshiString = [homeModel.class_hour stringByAppendingString:@"课时"];
            if ([homeModel.is_free isEqualToString:@"1"]) {
                cell1.priceLabel.text = @"免费";
            }
            cell1.keshiLabel.text = keshiString;
            NSString *peopleString = homeModel.course_vrows;
            cell1.peoplesLabel.text = peopleString;
            [cell1.headImgView sd_setImageWithURL:[NSURL URLWithString:homeModel.course_list_cover]];
            cell1.headImgView.sd_cornerRadius = [NSNumber numberWithInteger:10];
            cell1.detailLabel.text = homeModel.brief;
            
            //判断j会员标识
            if ([homeModel.member_is_free isEqualToString:@"1"]) {
                cell1.mianfeiLabel.hidden = NO;
                cell1.mianfeiLabel.text = @"vip免费";
                cell1.mianfeiLabel.textColor = GMWhiteColor;
                cell1.mianfeiLabel.textAlignment = NSTextAlignmentCenter;
            }else {
                cell1.mianfeiLabel.hidden = YES;      cell1.titleLabel.sd_layout.topEqualToView(cell1.mianfeiLabel)
                .leftSpaceToView(cell1.headImgView, 20)
                .widthIs(161*KScreenW/375).heightIs(15);
            }
            
            return cell1;
        }
            break;
            //最新专栏
        case 2:
        {
            UITableViewCell *cell2 = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier2];
            cell2.backgroundColor = GMBlackColor;
            cell2.selectionStyle = UITableViewCellSelectionStyleNone;
            self.flowLayout = [[UICollectionViewFlowLayout alloc]init];
            self.flowLayout.itemSize = CGSizeMake((self.view.frame.size.width-28)/2-5, 225);
            self.flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
            //    self.flowLayout.minimumLineSpacing = 10;
            //    self.flowLayout.minimumInteritemSpacing = 10;
            self.collectionV = [[UICollectionView alloc]initWithFrame:CGRectMake(15, 25, self.view.frame.size.width-28, 460) collectionViewLayout:self.flowLayout];
            self.collectionV.backgroundColor = GMWhiteColor;
            [cell2.contentView addSubview:self.collectionV];
            self.collectionV.delegate = self;
            self.collectionV.dataSource = self;
            [self.collectionV registerClass:[NewColumnsCollectionViewCell class] forCellWithReuseIdentifier:@"myCell"];
            return cell2;
        }
            break;
            //免费课程
        case 3:
        {
            
            MianFeiCourseTableViewCell *cell3=[[MianFeiCourseTableViewCell alloc]initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:cellIdentifier3];
            HomeFreeModel *freeModel = [self.homeFreeArray objectAtIndex:indexPath.row];
            cell3.titleLabel.text = freeModel.course_name;
            cell3.keshiLabel.text = freeModel.class_hour;
            cell3.peoplesLabel.text = freeModel.course_vrows;
            [cell3.headImgView sd_setImageWithURL:[NSURL URLWithString:freeModel.course_free_cover]];
            cell3.detailLabel.text = freeModel.brief;
            [cell3.headImgView setSd_cornerRadius:[NSNumber numberWithInt:10]];
            //                cell3.backgroundColor = [UIColor purpleColor];
            cell3.selectionStyle = UITableViewCellSelectionStyleNone;;
            return cell3;
        }
            break;
            //热门直播
        case 4:
        {
            HotLiveTableViewCell *cell4 = [[HotLiveTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier4];
//            NSString *filePath = [[NSBundle mainBundle] pathForResource:@"DataPropertyList" ofType:@"plist"];
//                            NSMutableDictionary *dicx = [NSMutableDictionary dictionary];
//                            [dicx setObject:@"孩子感冒发烧"forKey:@"title"];
//                            //    NSMutableDictionary *dicx2 = [NSMutableDictionary dictionary];
//                            [dicx setObject:@"1.jpg" forKey:@"picture"];
//
//                            NSMutableDictionary *dicx2 = [NSMutableDictionary dictionary];
//                            [dicx2 setObject:@"怎么办"forKey:@"title"];
//                            //    NSMutableDictionary *dicx2 = [NSMutableDictionary dictionary];
//                            [dicx2 setObject:@"2.jpg" forKey:@"picture"];
//
//                            NSMutableDictionary *dicx3 = [NSMutableDictionary dictionary];
//                            [dicx3 setObject:@"测试一下"forKey:@"title"];
//                            //    NSMutableDictionary *dicx2 = [NSMutableDictionary dictionary];
//                            [dicx3 setObject:@"3.jpg" forKey:@"picture"];
//                            NSMutableArray *arx = [NSMutableArray array];
//                            [arx addObject:dicx];
//                            [arx addObject:dicx2];
//                            [arx addObject:dicx3];
//            NSArray *arr = [NSArray arrayWithContentsOfFile:filePath];
            NSMutableArray *models = @[].mutableCopy;
            for (CourseLiveListsModel *dic in self.courseLiveArray) {
                MGBannerModel *model = [MGBannerModel new];
                model.title = dic.name;
                model.picture = dic.publicity_cover;
                model.date = dic.start_date;
                model.status = dic.live_status.integerValue;
//                [model setValuesForKeysWithDictionary:dic];
                [models addObject:model];
            }
            //                cell4.textLabel.text = @"asdfasdfasfd";
            
            GMBannerView *banner = ({
                
                //                    CGFloat width = self.view.bounds.size.width;
                GMBannerView *banner = [[GMBannerView alloc] initWithFrame:CGRectMake(0, 0,KScreenW, 255)];
                banner.models = models;
                banner.delegate = self;
                
                banner;
            });
            [cell4.imageBgView addSubview:banner];
            cell4.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell4;
        }
            //热门咨询
        case 5:
        {
            HotConsultTableViewCell *cell5 = [[HotConsultTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier5];
            
            HotZXModel *zxModel = [self.homeHotZXArray objectAtIndex:indexPath.row];
            //                [cell5.headImg sd_setImageWithURL:[NSURL URLWithString:zxModel.manag_picture]];
            cell5.timeLabel.text = [zxModel.manag_time substringToIndex:10];
            cell5.titleLabel.text = zxModel.manag_title;
            cell5.dianZanLabel.text = zxModel.thumbsup ;
            cell5.peopleLabel.text = zxModel.browse;
            [cell5.headImg sd_setImageWithURL:[NSURL URLWithString:zxModel.manag_picture]];
            cell5.selectionStyle = UITableViewCellSelectionStyleNone;
            WS(weakSelf);
            cell5.HotConsultTableViewCellLikeTouchBlock = ^{
        NSMutableDictionary* dic = [[NSMutableDictionary alloc] init];
                   [dic setValue:zxModel.manag_id forKey:@"manag_id"];
                [weakSelf consultLikeWithId: dic andUrl:KhomeTalkLike type:0];
            };
            return cell5;
        }
            //育朵精选
        case 6:
        {
            JingxuanTableViewCell *cell6 = [[JingxuanTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier6];
            YDSelectModel *ydSelectM = [self.homeYDSelectArray objectAtIndex:indexPath.row];
            cell6.titleLabel.text = ydSelectM.article_title;
            cell6.zanLabel.text = [ydSelectM.thumbs_up stringByAppendingString:@"人"];
            cell6.mianfeiLabel.text = @"免费";
            cell6.zanLabel.text = ydSelectM.article_vrows;
            [cell6.headImgView sd_setImageWithURL:[NSURL URLWithString:ydSelectM.article_cover]];
            CGFloat price = ydSelectM.article_price.floatValue;
            if (price > 0) {
                cell6.priceLabel.text = [NSString stringWithFormat:@"¥%.2f", price];
            } else {
                cell6.priceLabel.text = @"免费";
            }
            cell6.messageLabel.text = ydSelectM.comment_count;
            cell6.detailLabel.text = ydSelectM.brief;
            if ([ydSelectM.article_member_is_free isEqualToString:@"1"]) {
                cell6.mianfeiLabel.text = @"vip免费";
            }else {
                cell6.mianfeiLabel.hidden = YES;
                cell6.titleLabel.sd_layout.leftSpaceToView(cell6.headImgView, 17).widthIs(226*KScreenW/375);
            }
            WS(weakSelf);
            cell6.JingxuanTableViewCellLikeTouchBlock = ^{
                NSMutableDictionary* dic = [[NSMutableDictionary alloc] init];
                   [dic setValue:ydSelectM.article_id forKey:@"article_id"];
                [weakSelf consultLikeWithId: dic andUrl:KhomeArticelLike type:1];
            };
            cell6.selectionStyle = UITableViewCellSelectionStyleNone;
//            cell6.messageLabel.text =
            return cell6;
        }
    }
    return nil;
}
-(void)consultLikeWithId:(NSMutableDictionary *)dic andUrl:(NSString *)url type:(NSInteger)type{
   
    [GMAfnTools PostHttpDataWithUrlStr:url Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
        //点赞成功
        NSLog(@"点赞状态：%@",responseObject);
        if ([responseObject [@"code"] isEqualToString:@"1"]) {
            if (type == 0) {
                 //[self getdataHomeHotZX];
            }else{
                //[self getdataHomeHotYDSelect];
            }
           
        }
    } FailureBlock:^(id  _Nonnull error) {
        //点赞失败
    }];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    cell.backgroundColor = [UIColor clearColor];
    HotConsultTableViewCell *myCell = (HotConsultTableViewCell *)cell;
    myCell.backgroundView.backgroundColor = [UIColor whiteColor];
}


#pragma mark ----HZBannerViewDelegate----
-(void)HZBannerViewTest:(GMBannerView *)bannerView didSelectedAt:(NSInteger)indexGM{
    NewZhiBoDetailsViewController * zhibo = [[NewZhiBoDetailsViewController alloc] init];
    [self.navigationController pushViewController:zhibo animated:NO];
}
- (void)HZBannerView:(HZBannerView *)bannerView didSelectedAt:(NSInteger)index{
    //    NSLog(@"xx打印---最热课程--%ld",index);
    if (index-2 >= _models.count) {
        return;
    }
    if (index == 6) {
        index = 2;
        HZBannerModel *mol = [_models objectAtIndex:index-2];
        CoureDetailViewController *coureVC = [[CoureDetailViewController alloc]init];
        coureVC.courseDetailIDString = mol.courseID;
        coureVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:coureVC animated:YES];
    }else {
         HZBannerModel *mol = [_models objectAtIndex:index-2];
        CoureDetailViewController *coureVC = [[CoureDetailViewController alloc]init];
        coureVC.courseDetailIDString = mol.courseID;
        coureVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:coureVC animated:YES];
        
    }
}

#pragma mark -- collectionView delegate , datasource
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}
#pragma mark--最热专栏
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    NSLog(@"++++%ld",self.homeHotColumnArray.count);
    if (self.homeHotColumnArray.count > 4) {
        return 4;
    }
    return self.homeHotColumnArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NewColumnsCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"myCell" forIndexPath:indexPath];
    cell.contentView.layer.cornerRadius = 5.0f;
    cell.contentView.layer.borderWidth = 0.08f;
    cell.contentView.layer.borderColor = [UIColor blackColor].CGColor;
    cell.contentView.layer.masksToBounds = YES;
    
    HomeHotColumnModel *columnModel = [self.homeHotColumnArray objectAtIndex:indexPath.row];
    cell.titleLabel.text = columnModel.course_name;
    cell.keshiLabel.text = [columnModel.class_hour stringByAppendingString:@"课时"];
    cell.peoplesLabel.text = [columnModel.course_vrows stringByAppendingString:@"人"];
    cell.priceLabel.text = [@"¥" stringByAppendingString:columnModel.course_price];
    cell.xpriceLabel.text = [@"¥" stringByAppendingString:columnModel.course_under_price];
    [cell.imgView sd_setImageWithURL:[NSURL URLWithString:columnModel.course_free_cover]];
    return cell;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"%ld %ld",indexPath.section,indexPath.row);
    
    HomeColumnDetailViewController *coureDetailVC = [[HomeColumnDetailViewController alloc]init];
    HomeHotColumnModel *mol = [self.homeHotColumnArray objectAtIndex:indexPath.row];
    coureDetailVC.courseDetailIDString = mol.gement_id;
    coureDetailVC.courseTitle = mol.course_name;
    [coureDetailVC setHidesBottomBarWhenPushed:YES];
    [self.navigationController pushViewController:coureDetailVC animated:YES];
    
}



-(void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBarHidden = YES;
}
#pragma mark--轮播图1
- (void)getDataHomeBanner {
    self.homeBannerArray = [NSMutableArray array];
    NSMutableDictionary *bannerDic = [NSMutableDictionary dictionary];
    bannerDic[@"type"] = @"3";
    bannerDic[@"url_type"] = @"1";
    
    [GMAfnTools PostHttpDataWithUrlStr:KHomeBanner Dic:bannerDic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"首页轮播图-----%@",responseObject);
        NSMutableArray *imageList = [NSMutableArray array];
        for (NSMutableDictionary *dic in responseObject) {
            HomeBannerModel *mol = [[HomeBannerModel alloc]init];
            [mol mj_setKeyValues:dic];
            if ([mol.status isEqualToString:@"1"]) {
                [self.homeBannerArray addObject:mol];
                [imageList addObject:mol.picture];
            }
        }
        self.bannerCycleScrollerView.imageURLStringsGroup = imageList;
        [self.HomeTableView reloadData];
    } FailureBlock:^(id  _Nonnull error) {
        
    }];
}

#pragma mark--最热课程2
- (void)getDataHomeHotCourse {
    
    self.homeHotCourseArray = [NSMutableArray array];
    [GMAfnTools PostHttpDataWithUrlStr:KHomeHotCourse Dic:nil SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"首页--最热课程%@",responseObject);
        for (NSMutableDictionary *dic in responseObject) {
            HomeHotCourseModel *mol = [[HomeHotCourseModel alloc]init];
            [mol mj_setKeyValues:dic];
            [self.homeHotCourseArray addObject:mol];
        }
        [self.HomeTableView reloadData];
    } FailureBlock:^(id  _Nonnull error) {
        
    }];
}
#pragma mark--最新课程3
- (void)getDataHomeNewCourse {
     self.homeCourseArray = [NSMutableArray array];
    [GMAfnTools PostHttpDataWithUrlStr:KhomeNewCourse Dic:nil SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"首页--最新课程%@",responseObject);
        for (NSMutableDictionary *dic in responseObject) {
            HomeNewCourseModel *mol = [[HomeNewCourseModel alloc]init];
            [mol mj_setKeyValues:dic];
            [self.homeCourseArray addObject:mol];
        }
        [self.HomeTableView reloadData];
    } FailureBlock:^(id  _Nonnull error) {
    }];
}

- (void)column:(NSString *)columnString {
    self.homeHotColumnArray = [NSMutableArray array];
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"category_id"] = columnString;
    dic[@"screen"] = @"1";
    [GMAfnTools PostHttpDataWithUrlStr:KCoulumnGementList Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"打印最新专栏列表====%@",responseObject);
        for (NSMutableDictionary *dic in responseObject) {
            HomeHotColumnModel *mol = [[HomeHotColumnModel alloc]init];
            [mol mj_setKeyValues:dic];
            [self.homeHotColumnArray addObject:mol];
        }
        //        NSLog(@"最新专栏---%@",responseObject);
        [self.HomeTableView reloadData];
    } FailureBlock:^(id  _Nonnull error) {
        
    }];
}
#pragma mark--最新专栏4
- (void)getdataHomeHotColumn {
    
    
    __weak typeof(self) weakSelf = self;
    NSMutableDictionary *columnDic = [NSMutableDictionary dictionary];
    columnDic[@"pid"] = @"2";
    [GMAfnTools PostHttpDataWithUrlStr:KCategoryID Dic:columnDic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"xxxxx8888888%@",responseObject);
        for (NSMutableDictionary *dic in responseObject) {
            HomeColumnModel *mol = [[HomeColumnModel alloc]init];
            [mol mj_setKeyValues:dic];
            
            [weakSelf column:mol.category_id];
            //            NSLog(@"打印最新专栏4--%@",mol.category_id);
        }
    } FailureBlock:^(id  _Nonnull error) {
        
    }];
}
#pragma mark--免费专区
- (void)getdataHomeFree {
    
    self.homeFreeArray = [NSMutableArray array];
    [GMAfnTools PostHttpDataWithUrlStr:KhomeFreeZone Dic:nil SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"首页免费课程%@",responseObject);
        for (NSMutableDictionary *dic in responseObject) {
            HomeFreeModel *mol = [[HomeFreeModel alloc]init];
            [mol mj_setKeyValues:dic];
            [self.homeFreeArray addObject:mol];
        }
        //        NSLog(@"首页--免费课程%@",self.homeFreeArray);
        [self.HomeTableView reloadData];
    } FailureBlock:^(id  _Nonnull error) {
        
    }];
}
#pragma mark -- 推荐直播
- (void)getCourseLiveData {
    self.courseLiveArray = [NSMutableArray array];
    [GMAfnTools PostHttpDataWithUrlStr:KCourseLiveList Dic:nil SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"----课程---推荐直播--%@",responseObject);
        for (NSMutableDictionary *liveDic in responseObject) {
            CourseLiveListsModel *mol = [[CourseLiveListsModel alloc]init];
            [mol mj_setKeyValues:liveDic];
            
            [self.courseLiveArray addObject:mol];
        }
        [self.HomeTableView reloadData];
    } FailureBlock:^(id  _Nonnull error) {
        NSLog(@"-+++++%@",error);
    }];
}
#pragma mark--热门咨询
- (void)getdataHomeHotZX {
    
    self.homeHotZXArray = [NSMutableArray array];
    NSLog(@"+++++++++%@",KhomeHotZX);
    [GMAfnTools PostHttpDataWithUrlStr:KhomeHotZX Dic:nil SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"热门咨询====%@",responseObject);
        for (NSMutableDictionary *dic in responseObject) {
            HotZXModel *mol = [[HotZXModel alloc]init];
            [mol mj_setKeyValues:dic];
            [self.homeHotZXArray addObject:mol];
        }
        [self.HomeTableView reloadData];
    } FailureBlock:^(id  _Nonnull error) {
        NSLog(@"=======热门咨询 报错%@",error);
    }];
}

#pragma mark--育朵精选
- (void)getdataHomeHotYDSelect {
    self.homeYDSelectArray = [NSMutableArray array];
    [GMAfnTools PostHttpDataWithUrlStr:KhomeSelected Dic:nil SuccessBlock:^(id  _Nonnull responseObject) {
                NSLog(@"首页--育朵精选%@",responseObject);
        
        for (NSMutableDictionary *dic in responseObject) {
            YDSelectModel *mol = [[YDSelectModel alloc]init];
            [mol mj_setKeyValues:dic];
            [self.homeYDSelectArray addObject:mol];
            //                    NSLog(@"打印育朵精选====%@",)
        }
        [self.HomeTableView reloadData];
    } FailureBlock:^(id  _Nonnull error) {
        
    }];
}

#pragma mark - SDCycleScrollViewDelegate

- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index
{
    NSLog(@"---点击了第%ld张图片", (long)index);
    

}

@end
