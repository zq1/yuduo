//
//  HomeViewController.h
//  yuduo
//
//  Created by Mac on 2019/7/23.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "BaseViewController.h"
#import "HomeHeadView.h"
#import "MCScrollView.h"
NS_ASSUME_NONNULL_BEGIN

@interface HomeViewController : BaseViewController
PropertyStrong(UITableView, HomeTableView);//首页tableview

PropertyStrong(UIView, headerView);//tableview头部
PropertyStrong(MCScrollView, scrollView); //第一个lbt
@property(assign,nonatomic) NSInteger selectIndex;
PropertyStrong(UIScrollView, bgScrollView);//最底部scrollview

PropertyStrong(HomeHeadView, headBGView); // 
//PropertyStrong(UIView,  footer);
//区标题
PropertyStrong(NSArray, sectionTitleArray);
//返回顶部
PropertyStrong(UIButton, btn);

@property (strong,nonatomic)UICollectionView *collectionV;
@property (strong,nonatomic)UICollectionViewFlowLayout *flowLayout;
@end

NS_ASSUME_NONNULL_END
