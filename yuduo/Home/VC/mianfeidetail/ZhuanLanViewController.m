//
//  ZhuanLanViewController.m
//  yuduo
//
//  Created by Mac on 2019/8/13.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "ZhuanLanViewController.h"
#import "ZhuanLanTableViewCell.h"
#import "SouCangCourseModel.h"
@interface ZhuanLanViewController ()<UITableViewDelegate,UITableViewDataSource>
PropertyStrong(UITableView, coureseTableView);
@property(nonatomic,strong)NSMutableArray<SouCangCourseModel *>*dataSource;
@end

@implementation ZhuanLanViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataSource = [[NSMutableArray alloc] init];
    self.coureseTableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.coureseTableView.delegate = self;
    self.coureseTableView.dataSource = self;
    self.coureseTableView.tableFooterView = [UIView new];
    self.coureseTableView.backgroundColor = RGB(244, 243, 244);
    [self.view addSubview:self.coureseTableView];
    self.coureseTableView.sd_layout.topSpaceToView(self.view, 0)
    .leftSpaceToView(self.view, 0)
    .rightSpaceToView(self.view,0)
    .widthIs(KScreenW).heightIs(KScreenH-100);
    [self getZLList];
    // Do any additional setup after loading the view.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.dataSource.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 120;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"cellID";
    ZhuanLanTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[ZhuanLanTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.backgroundColor = RGB(244, 243, 244);
    SouCangCourseModel *mol = [self.dataSource objectAtIndex:indexPath.row];
    [cell.headImg sd_setImageWithURL:[NSURL URLWithString:mol.course_list_cover]];
    cell.headImg.sd_cornerRadius = [NSNumber numberWithInteger:10];
    cell.titleLable.text = mol.course_name;
    cell.keshiLable.text = mol.class_hour;
    cell.peopleLable.text = mol.course_vrows;
    if (self.isFree) {
        cell.priceLabel.text = @"免费";
    }else{
        cell.priceLabel.text = mol.course_price;
        cell.xPriceLabel.text = mol.course_under_price;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.ZhuanLanViewControllerCellSelect) {
        self.ZhuanLanViewControllerCellSelect(self.dataSource[indexPath.row].course_id,self.dataSource[indexPath.row].course_name);
    }
}
-(void)setIsFree:(BOOL)isFree{
    _isFree = isFree;
}
- (void)getZLList {
    
    NSMutableDictionary *courseDic = [NSMutableDictionary dictionary];
    
    [GMAfnTools PostHttpDataWithUrlStr:KCourseFreeList Dic:courseDic SuccessBlock:^(id  _Nonnull responseObject) {
        for (NSMutableDictionary *dic in responseObject[@"free_gement"]) {
             SouCangCourseModel *soucangCM = [[SouCangCourseModel alloc]init];
             [soucangCM mj_setKeyValues:dic];
              [self.dataSource addObject:soucangCM];
        }
        [self.coureseTableView reloadData];
    }
                          FailureBlock:^(id  _Nonnull error) {
                          }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
