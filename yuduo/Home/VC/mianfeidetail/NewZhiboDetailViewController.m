//
//  NewZhiboDetailViewController.m
//  yuduo
//
//  Created by 刘耀聪 on 2019/10/26.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "NewZhiboDetailViewController.h"
#import <WebKit/WebKit.h>
#import "yuduo-Swift.h"
@interface NewZhiboDetailViewController ()<WKNavigationDelegate, WKScriptMessageHandler, WKUIDelegate>
@property (nonatomic, strong) WKWebViewConfiguration *configuration;
@property (nonatomic, strong) UIProgressView *progressView;
@property (nonatomic, strong) WKWebView *webView;
@end

@implementation NewZhiboDetailViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:self.webView];
    self.title = @"直播详情";
    [self setLeftBarItemWithImage:@"back-icon"];
    self.progressView = [[UIProgressView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 2)];
    self.progressView.backgroundColor = GMBrownColor;
    self.progressView.progressTintColor = GMBrownColor;
    self.progressView.transform = CGAffineTransformMakeScale(1.0f, 1.5f);
    [self.view addSubview:self.progressView];
    [self.webView addObserver:self forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew context:nil];
}
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    if ([keyPath isEqualToString:@"estimatedProgress"]) {
        self.progressView.progress = self.webView.estimatedProgress;
        if (self.progressView.progress == 1) {
            [UIView animateWithDuration:0.25f delay:0.3f options:UIViewAnimationOptionCurveEaseOut animations:^{
                self.progressView.transform = CGAffineTransformMakeScale(1.0f, 1.4f);
            } completion:^(BOOL finished) {
                self.progressView.hidden = YES;
            }];
        }
    }else{
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}
#pragma mark -- WKScriptMessageHandler
-(void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message
{
    if ([self.title isEqualToString:@"积分规则"]) {
        return;
    }
    NSLog(@"body:%@",message.body);
    //[[UIApplication sharedApplication].keyWindow addSubview:self.detailShareView];
}

-(void)leftBarItemAction:(UITapGestureRecognizer *)gesture{
    [self backAction];
}
#pragma mark - 动作 - 后退和退栈 -
- (void) backAction
{
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        //如果可以后退
        if ([self.webView canGoBack]) {
            //执行后退
        [self.webView goBack];
        }else {
            //如果栈中控制器大于1个
            if (self.navigationController.viewControllers.count > 1) {
                //返回时退栈
                [self.navigationController popViewControllerAnimated:NO];
            }else {
                //如果控制器没有大于1 就是模态出的web,那么就dismiss
                [self.navigationController dismissViewControllerAnimated:YES completion:^{
                }];
            }
        }
    }else {
        //如果系统低于8.0 执行UIWebview的方法
        if ([self.webView canGoBack]) {
            [self.webView goBack];
        }else {
            if (self.navigationController.viewControllers.count > 1) {
                [self.navigationController popViewControllerAnimated:NO];
            }else {
                [self.navigationController dismissViewControllerAnimated:YES completion:^{
                }];
            }
        }
    }
}

- (WKWebViewConfiguration *)configuration{
    if (!_configuration) {
        _configuration = [[WKWebViewConfiguration alloc] init];
        WKUserContentController *userContentController = [[WKUserContentController alloc] init];
        
        [userContentController addScriptMessageHandler:self name:@"shareInfo"];
        _configuration.userContentController = userContentController;
        WKPreferences *preferences = [WKPreferences new];
        preferences.javaScriptCanOpenWindowsAutomatically = YES;
        //preferences.minimumFontSize = 40.0;
        _configuration.preferences = preferences;
    }
    return _configuration;
}

- (WKWebView *)webView{
    if (!_webView) {
        _webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 0, KScreenW, KScreenH-kNavigationHeight ) configuration:self.configuration];
        [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"https://cs1.91hzty.com/web/broadcast"]]];
        [_webView setAllowsBackForwardNavigationGestures:true];
        _webView.navigationDelegate = self;
        _webView.UIDelegate = self;
        //[_webView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
//        [_webView setMultipleTouchEnabled:YES];
//        [_webView setAutoresizesSubviews:YES];
//        [_webView.scrollView setAlwaysBounceVertical:YES];
    }
    return _webView;
}

//开始加载
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
    self.progressView.hidden = NO;
    self.progressView.transform = CGAffineTransformMakeScale(1.0f, 1.5f);
    [self.view bringSubviewToFront:self.progressView];
}
//加载失败
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation withError:(NSError *)error {
    self.progressView.hidden = YES;
}
- (void)dealloc {
    [self.webView removeObserver:self forKeyPath:@"estimatedProgress"];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
