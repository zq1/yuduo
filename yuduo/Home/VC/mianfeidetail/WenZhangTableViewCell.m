//
//  WenZhangTableViewCell.m
//  yuduo
//
//  Created by Mac on 2019/8/13.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "WenZhangTableViewCell.h"

@implementation WenZhangTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        
        [self createNewCourse];
    }
    return self;
}

- (void)createNewCourse {
    //背景view（所有控件都加到背景）
    self.bgView = [[UIView alloc]init];
    self.bgView.frame = CGRectMake(0, 0, KScreenW, 110);
    //    self.bgView.backgroundColor = GMRedColor;
    [self.contentView addSubview:self.bgView];
    //头像
    self.headImgView = [[UIImageView alloc]init];
    self.headImgView.backgroundColor = GMlightGrayColor;
    self.headImgView.sd_cornerRadius = [NSNumber numberWithFloat:10];
    [self.bgView addSubview:self.headImgView];
    self.headImgView.sd_layout.topSpaceToView(self.bgView, 15)
    .leftSpaceToView(self.bgView, 15)
    .bottomSpaceToView(self.bgView, 15)
    .widthIs(80).heightIs(80);
#pragma mark 免费标签
    {
        /*  //免费logo
         self.mianfeiLabel = [[UILabel alloc] init];
         self.mianfeiLabel.numberOfLines = 0;
         NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"vip免费" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 11],NSForegroundColorAttributeName: [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0]}];
         self.mianfeiLabel.backgroundColor = [UIColor colorWithRed:228/255.0 green:102/255.0 blue:67/255.0 alpha:1.0];
         self.mianfeiLabel.attributedText = string;
         [self.bgView addSubview:self.mianfeiLabel];
         self.mianfeiLabel.sd_cornerRadius = [NSNumber numberWithInteger:8.5];
         self.mianfeiLabel.sd_layout.topSpaceToView(self.bgView, 40).leftSpaceToView(self.headImgView, 15).widthIs(45).heightIs(17);
         */
    }
    //标题
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.numberOfLines = 0;
    self.titleLabel.font = [UIFont fontWithName:KPFType size:14];
    self.titleLabel.textColor = RGB(69, 69, 69);

    [self.bgView addSubview:self.titleLabel];
    self.titleLabel.sd_layout.topSpaceToView(self.bgView, 15)
    .leftSpaceToView(self.headImgView, 64)
    .widthIs(166*KScreenW/375).heightIs(14);
    //课程简介
    self.detailLabel = [[UILabel alloc] init];
    self.detailLabel.font = [UIFont fontWithName:KPFType size:13];
    self.detailLabel.textColor = RGB(153, 153, 153);
    self.detailLabel.numberOfLines = 2;
    self.detailLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [self.bgView addSubview:self.detailLabel];
    
    //     self.detailLabel.frame = CGRectMake(132,755.5,228.5,32.5);
    self.detailLabel.sd_layout.topSpaceToView(self.titleLabel, 11)
    .leftSpaceToView(self.headImgView, 16)
    .rightSpaceToView(self.bgView, 19)
    .heightIs(30);
    //消息按钮
    self.messageButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.messageButton setBackgroundImage:[UIImage imageNamed:@"更多"] forState:UIControlStateNormal];
    [self.bgView addSubview:self.messageButton];
    self.messageButton.sd_layout.leftSpaceToView(self.headImgView, 18)
    .bottomSpaceToView(self.bgView, 19).widthIs(14).heightIs(12);
    //多少人看过
    self.messageLabel = [[UILabel alloc] init];
    //    label.frame = CGRectMake(157,816,34.5,10.5);
    self.messageLabel.numberOfLines = 0;
    self.messageLabel.font = [UIFont fontWithName:KPFType size:11];
    self.messageLabel.textColor = RGB(153, 153, 153);

    [self.bgView addSubview:self.messageLabel];
    self.messageLabel.sd_layout.bottomSpaceToView(self.bgView, 19)
    .leftSpaceToView(self.headImgView, 42)
    .widthIs(38).heightIs(11);
    
    //点赞图标
    self.zanImg = [[UIImageView alloc]init];
    //    self.zanImg.backgroundColor = GMGreenColor;
    self.zanImg.image = [UIImage imageNamed:@"ico_zan_1"];
    [self.bgView addSubview:self.zanImg];
    self.zanImg.sd_layout.bottomSpaceToView(self.bgView, 17).leftSpaceToView(self.headImgView, 92).widthIs(14).heightIs(14);
    
    
    //点赞人数
    
    self.zanLabel = [[UILabel alloc] init];
    self.zanLabel.numberOfLines = 0;
    self.zanLabel.font = [UIFont fontWithName:KPFType size:12];
    self.zanLabel.textColor = RGB(153, 153, 153);

    [self.bgView addSubview:self.zanLabel];
    self.zanLabel.sd_layout.bottomSpaceToView(self.bgView, 19)
    .leftSpaceToView(self.headImgView, 116)
    .widthIs(50).heightIs(9);
    
    
    //现在的价格
    
    self.priceLabel = [[UILabel alloc] init];
    //    sel.frame = CGRectMake(311.5,815.5,48.5,11.5);
    self.priceLabel.numberOfLines = 0;
    self.priceLabel.font = [UIFont fontWithName:KPFType size:15];
    self.priceLabel.textColor = RGB(228, 102, 67);
    self.priceLabel.text = @"10086";
    [self.bgView addSubview:self.priceLabel];
    self.priceLabel.sd_layout.bottomSpaceToView(self.bgView, 16)
    .rightSpaceToView(self.bgView, 15).widthIs(49).heightIs(12);
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
