//
//  ZhuanLanTableViewCell.m
//  yuduo
//
//  Created by Mac on 2019/8/13.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "ZhuanLanTableViewCell.h"

@implementation ZhuanLanTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier]) {
        
        [self zhuanlanLayout];
    }
    return self;
}

- (void)zhuanlanLayout {
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = GMWhiteColor;
    [self.contentView addSubview:self.bgView];
    self.bgView.sd_layout.topSpaceToView(self.contentView, 0)
    .leftSpaceToView(self.contentView,0)
    .widthIs(KScreenW).heightIs(120);
//
    self.headImg = [[UIImageView alloc]init];
    self.headImg.backgroundColor = GMlightGrayColor;
    
    [self.bgView addSubview:self.headImg];
    self.headImg.sd_layout.topSpaceToView(self.bgView, 20)
    .leftSpaceToView(self.bgView, 14)
    .widthIs(120*kGMWidthScale).heightIs(80*kGMHeightScale);
//
    self.titleLable = [[UILabel alloc]init];
    self.titleLable.numberOfLines = 2;
    self.titleLable.font = [UIFont fontWithName:KPFType size:14];
    self.titleLable.textColor = RGB(69, 69, 69);
    self.titleLable.text = @"前央视主持人李雷：50堂魅力声音必修课";
    [self.bgView addSubview:self.titleLable];
    self.titleLable.sd_layout.topSpaceToView(self.bgView, 20)
    .leftSpaceToView(self.headImg, 15)
    .widthIs(210).heightIs(40);
    
    self.keshiImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"更多"]];
    [self.bgView addSubview:self.keshiImg];
    self.keshiImg.sd_layout.bottomSpaceToView(self.bgView, 21)
    .leftSpaceToView(self.headImg, 15)
    .widthIs(10).heightIs(15);
//
    self.keshiLable = [[UILabel alloc]init];
    self.keshiLable.font = [UIFont fontWithName:KPFType size:11];
    self.keshiLable.textColor = RGB(153, 153, 153);
    self.keshiLable.text = @"55课时";
    [self.bgView addSubview:self.keshiLable];
    self.keshiLable.sd_layout.bottomSpaceToView(self.bgView, 21)
    .leftSpaceToView(self.keshiImg, 11)
    .widthIs(37).heightIs(15);
//
    self.seeImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ico_look"]];
    [self.bgView addSubview:self.seeImg];
    self.seeImg.sd_layout.bottomSpaceToView(self.bgView, 21)
    .leftSpaceToView(self.keshiLable, 20*kGMWidthScale)
    .widthIs(15).heightIs(15);
//
    self.peopleLable = [[UILabel alloc]init];
    self.peopleLable.text = @"2478人";
    self.peopleLable.font = [UIFont fontWithName:KPFType size:11];
    self.peopleLable.textColor = RGB(153, 153, 153);
    [self.bgView addSubview:self.peopleLable];
    self.peopleLable.sd_layout.bottomSpaceToView(self.bgView, 21)
    .leftSpaceToView(self.seeImg, 11*kGMWidthScale)
    .widthIs(38).heightIs(15);
//
    self.mianfeiLable = [[UILabel alloc]init];
//    self.mianfeiLable.text = @"免费";
    self.mianfeiLable.textColor = RGB(228, 102, 67);
    self.mianfeiLable.font = [UIFont fontWithName:KPFType size:15];
    [self.bgView addSubview:self.mianfeiLable];
    self.mianfeiLable.sd_layout.bottomSpaceToView(self.bgView, 19)
    .rightSpaceToView(self.bgView, 15)
    .widthIs(31).heightIs(15);
    
    //划掉的价格
    self.xPriceLabel = [[UILabel alloc] init];
    self.xPriceLabel.numberOfLines = 0;
    self.xPriceLabel.textColor = RGB(153, 153, 153);
    self.xPriceLabel.font = [UIFont fontWithName:@"PingFangSC-Regular"size:12];
    [self.bgView addSubview:self.xPriceLabel];
    self.xPriceLabel.sd_layout.topSpaceToView(self.bgView, 70)
    .rightSpaceToView(self.bgView, 15).heightIs(10);
    [self.xPriceLabel setSingleLineAutoResizeWithMaxWidth:100];
    //现在的价格
    self.priceLabel = [[UILabel alloc] init];
    self.priceLabel.numberOfLines = 0;
    self.priceLabel.textColor = RGB(228, 102, 67);
    self.priceLabel.font = [UIFont fontWithName:@"PingFangSC-Regular"size:15];
    [self.bgView addSubview:self.priceLabel];
    self.priceLabel.sd_layout.topSpaceToView(self.xPriceLabel, 6)
    .rightSpaceToView(self.bgView, 16).heightIs(12);
    [self.priceLabel setSingleLineAutoResizeWithMaxWidth:100];
    
    self.lineLabel = [[UILabel alloc]init];
    self.lineLabel.font = [UIFont fontWithName:@"PingFangSC-Regular"size:12];
    self.lineLabel.textColor = [UIColor lightGrayColor];
    self.lineLabel.backgroundColor = [UIColor lightGrayColor];
    [self.xPriceLabel addSubview:self.lineLabel];
    self.lineLabel.sd_layout.topSpaceToView(self.xPriceLabel, 5)
    .leftSpaceToView(self.xPriceLabel, 0)
    .heightIs(1);
    [self.lineLabel setSingleLineAutoResizeWithMaxWidth:100];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
