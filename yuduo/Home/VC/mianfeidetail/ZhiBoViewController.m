//
//  ZhiBoViewController.m
//  yuduo
//
//  Created by Mac on 2019/8/13.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "ZhiBoViewController.h"
#import "HomeLiveTableViewCell.h"
#import "CourseLiveListsModel.h"

@interface ZhiBoViewController ()<UITableViewDelegate,UITableViewDataSource>
PropertyStrong(UITableView, coureseTableView);
@property(nonatomic,strong)NSMutableArray<CourseLiveListsModel *> *dataSource;
@end

@implementation ZhiBoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataSource = [[NSMutableArray alloc] init];
    self.coureseTableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.coureseTableView.delegate = self;
    self.coureseTableView.dataSource = self;
    self.coureseTableView.tableFooterView = [UIView new];
    //self.coureseTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//    self.coureseTableView.backgroundColor = GMBrownColor;
    [self.view addSubview:self.coureseTableView];
    self.coureseTableView.sd_layout.topSpaceToView(self.view, 0)
    .leftSpaceToView(self.view, 15)
    .rightSpaceToView(self.view, 15)
    .widthIs(KScreenW-30).heightIs(KScreenH-100);
    [self freeZbGetDate];
    // Do any additional setup after loading the view.
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.dataSource.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 239;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"cellID";
    HomeLiveTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[HomeLiveTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.contentView.backgroundColor = GMWhiteColor;
    CourseLiveListsModel * model = self.dataSource[indexPath.row];
    cell.backgroundColor = RGB(244, 243, 244);
//    PropertyStrong(UIImageView, imgView);
//    PropertyStrong(UILabel, titleLabel);
//    PropertyStrong(UILabel ,tagLiveLabel);
//    PropertyStrong(UILabel , dateLabel);
    [cell.imgView sd_setImageWithURL:[NSURL URLWithString:model.publicity_cover]];
    cell.titleLabel.text = model.name;
    switch ([model.live_status integerValue]) {
        case 0:
            {
                cell.tagLiveLabel.text = @"未知状态";
            }
            break;
        case 1:
            {
                cell.tagLiveLabel.text = @"即将直播";
                cell.tagLiveLabel.textColor = RGB(23, 151, 164);
            }
            break;
        case 2:
            {
                cell.tagLiveLabel.text = @"正在直播";
                cell.tagLiveLabel.textColor = RGB(223, 95, 25);
            }
            break;
        case 3:
            {
                cell.tagLiveLabel.text = @"直播结束";
                cell.tagLiveLabel.textColor = GMBlackColor;
            }
            break;
        default:
            break;
    }
    //cell.tagLiveLabel.text = [model.live_status integerValue] == 0 ? @"直播结束":@"直播中";
    cell.dateLabel.text = model.start_date;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.ZhiBoViewControllerCellSelect) {
        self.ZhiBoViewControllerCellSelect(self.dataSource[indexPath.row].live_id);
    }
}
- (void)freeZbGetDate{
    NSMutableDictionary *newCourseDic = [NSMutableDictionary dictionary];
    [GMAfnTools PostHttpDataWithUrlStr:KCourseFreeList Dic:newCourseDic SuccessBlock:^(id  _Nonnull responseObject) {
        for (NSMutableDictionary *dic in responseObject[@"free_live"]) {
            CourseLiveListsModel *mol = [[CourseLiveListsModel alloc]init];
            [mol mj_setKeyValues:dic];
            [self.dataSource addObject:mol];
        }
        [self.coureseTableView reloadData];
    }
                          FailureBlock:^(id  _Nonnull error) {
                          }];
    //    [self.tableView reloadData];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
