//
//  ZhuanLanViewController.h
//  yuduo
//
//  Created by Mac on 2019/8/13.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZhuanLanViewController : UIViewController
@property (nonatomic ,strong) void(^ZhuanLanViewControllerCellSelect)(NSString * categaryId,NSString * title);
@property(nonatomic,assign)BOOL isFree;

@end

NS_ASSUME_NONNULL_END
