//
//  NewZhiBoDetailsViewController.swift
//  yuduo
//
//  Created by 刘耀聪 on 2019/10/28.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

import UIKit
import WebKit
import AVFoundation
class NewZhiBoDetailsViewController: BaseViewController,WKUIDelegate,WKNavigationDelegate {
    public var webView: WKWebView!;
    private var jsBridge: RecordAppJsBridge!;
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        self.navigationController?.isNavigationBarHidden = false;
        
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "直播详情";
        self.setLeftBarItemWithImage("back-icon");
        let microphoneUsesPermission:RecordAppJsBridge.MicrophoneUsesPermission={ [weak self] call in
            self?.reqMicrophonePermission(call);
        };
        let config=WKWebViewConfiguration();
        
        //******调用核心方法*********************
        //注入JsBridge, 实现api接口
        //*******以下内容无关紧要*****************
        config.requiresUserActionForMediaPlayback=false;
        config.allowsInlineMediaPlayback=true;
        config.allowsAirPlayForMediaPlayback=true;
        
        webView=WKWebView(frame: CGRect(x: 0, y: 0, width:self.view.bounds.width,height:self.view.bounds.height-64), configuration: config);
        self.view.addSubview(webView);
        
        webView.scrollView.bounces=false;
        webView.isOpaque=false;
        webView.backgroundColor=UIColor.white;
        
        webView.uiDelegate = self;
        webView.navigationDelegate = self;
        
        let url="https://cs1.91hzty.com/web/broadcast";
        webView.load(URLRequest(url: URL(string: url)!));
        jsBridge=RecordAppJsBridge(config, microphoneUsesPermission){ [weak self] code in
            self?.webView.evaluateJavaScript(code, completionHandler: nil);
        };
        // Do any additional setup after loading the view.
    }
    deinit {
        jsBridge?.close();
    }
    //录音权限处理
    private func reqMicrophonePermission(_ call:@escaping (Bool)->Void){
        let statues = AVAudioSession.sharedInstance().recordPermission
        if statues == .undetermined {
            AVAudioSession.sharedInstance().requestRecordPermission { [weak self] (granted) in
                if granted {
                    call(true);
                } else {
                    self?.reqMicrophonePermission(call);
                }
            }
        } else if statues == .granted {
            call(true);
        } else {
            call(false);
        }
    }
    public func webView(_ webView: WKWebView, runJavaScriptTextInputPanelWithPrompt prompt: String, defaultText: String?, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping (String?) -> Void) {
        //如果是js bridge的请求，就接管并处理它
        if let data=self.jsBridge.acceptPrompt(prompt) {
            completionHandler(data);
            return;
        }
        //此方法还需实现prompt弹框
        completionHandler(nil);
    }

}
