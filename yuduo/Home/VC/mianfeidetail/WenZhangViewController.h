//
//  WenZhangViewController.h
//  yuduo
//
//  Created by Mac on 2019/8/13.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WenZhangViewController : UIViewController
@property (nonatomic ,strong) void(^WenZhangViewControllerCellSelect)(NSString * categaryId,NSString * title);
@end

NS_ASSUME_NONNULL_END
