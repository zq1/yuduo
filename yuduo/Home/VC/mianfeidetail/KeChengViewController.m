//
//  KeChengViewController.m
//  yuduo
//
//  Created by Mac on 2019/8/13.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "KeChengViewController.h"
#import "MianFeiCourseTableViewCell.h"
#import "HomeFreeModel.h"
@interface KeChengViewController ()<UITableViewDelegate,UITableViewDataSource>
PropertyStrong(UITableView, coureseTableView);
@property(nonatomic,strong)NSMutableArray<HomeFreeModel*>* dataSourcce;
@end

@implementation KeChengViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"xxxxxxxx最终结果%@",self.arr);
//    self.view.backgroundColor = RGB(244, 243, 244);
    self.dataSourcce = [[NSMutableArray alloc] init];
    self.coureseTableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.coureseTableView.delegate = self;
    self.coureseTableView.dataSource = self;
    self.coureseTableView.tableFooterView = [UIView new];
//    self.coureseTableView.backgroundColor = RGB(244, 243, 244);
    [self.view addSubview:self.coureseTableView];
    self.coureseTableView.sd_layout.topSpaceToView(self.view, 0)
    .leftEqualToView(self.view)
    .widthIs(KScreenW).heightIs(KScreenH-MStatusBarHeight);
    [self freeGetDate];
    // Do any additional setup after loading the view.
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 50;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.dataSourcce.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 159;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"cellID";
    MianFeiCourseTableViewCell *cell3=[[MianFeiCourseTableViewCell alloc]initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:cellIdentifier];
    HomeFreeModel *freeModel = self.dataSourcce[indexPath.row];
    cell3.titleLabel.text = freeModel.course_name;
    cell3.keshiLabel.text = freeModel.class_hour;
    cell3.peoplesLabel.text = freeModel.course_vrows;
    [cell3.headImgView sd_setImageWithURL:[NSURL URLWithString:freeModel.course_list_cover]];
    cell3.detailLabel.text = freeModel.brief;
    [cell3.headImgView setSd_cornerRadius:[NSNumber numberWithInt:10]];
    //                cell3.backgroundColor = [UIColor purpleColor];
    cell3.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell3;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.KeChengViewControllerCellSelect) {
        self.KeChengViewControllerCellSelect(self.dataSourcce[indexPath.row].course_id);
    }
}
- (void)freeGetDate{
    NSMutableDictionary *newCourseDic = [NSMutableDictionary dictionary];
    [GMAfnTools PostHttpDataWithUrlStr:KCourseFreeList Dic:newCourseDic SuccessBlock:^(id  _Nonnull responseObject) {
        for (NSMutableDictionary *dic in responseObject[@"free_course"]) {
            HomeFreeModel *mol = [[HomeFreeModel alloc]init];
            [mol mj_setKeyValues:dic];
            [self.dataSourcce addObject:mol];
        }
        [self.coureseTableView reloadData];
    }
                          FailureBlock:^(id  _Nonnull error) {
                          }];
    //    [self.tableView reloadData];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
