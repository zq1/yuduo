//
//  WenZhangViewController.m
//  yuduo
//
//  Created by Mac on 2019/8/13.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "WenZhangViewController.h"
#import "WenZhangTableViewCell.h"
#import "YDSelectModel.h"
@interface WenZhangViewController ()<UITableViewDelegate,UITableViewDataSource>
PropertyStrong(UITableView, coureseTableView);
@property(nonatomic,strong)NSMutableArray<YDSelectModel *> * dataSource;
@end

@implementation WenZhangViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataSource = [[NSMutableArray alloc] init];
    self.coureseTableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.coureseTableView.delegate = self;
    self.coureseTableView.dataSource = self;
    self.coureseTableView.tableFooterView = [UIView new];
//    self.coureseTableView.backgroundColor = GMBrownColor;
    [self.view addSubview:self.coureseTableView];
    self.coureseTableView.sd_layout.topSpaceToView(self.view, 0)
    .leftSpaceToView(self.view, 15)
    .rightSpaceToView(self.view, 15)
    .widthIs(KScreenW-30).heightIs(KScreenH-100);
    [self getAriceList];
    // Do any additional setup after loading the view.
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.dataSource.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 120;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"cellID";
    WenZhangTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[WenZhangTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    YDSelectModel *ydSelectM = [self.dataSource objectAtIndex:indexPath.row];
    cell.titleLabel.text = ydSelectM.article_title;
    cell.zanLabel.text = [ydSelectM.thumbs_up stringByAppendingString:@"人"];
    cell.mianfeiLabel.text = @"免费";
    cell.zanLabel.text = ydSelectM.article_vrows;
    [cell.headImgView sd_setImageWithURL:[NSURL URLWithString:ydSelectM.article_cover]];
    CGFloat price = ydSelectM.article_price.floatValue;
    if (price > 0) {
        cell.priceLabel.text = [NSString stringWithFormat:@"¥%.2f", price];
    } else {
        cell.priceLabel.text = @"免费";
    }
    cell.messageLabel.text = ydSelectM.comment_count;
    cell.detailLabel.text = ydSelectM.brief;
//    if ([ydSelectM.article_member_is_free isEqualToString:@"1"]) {
//        cell.mianfeiLabel.text = @"vip免费";
//    }else {
//
//    }
    cell.mianfeiLabel.hidden = YES;
    cell.titleLabel.sd_layout.leftSpaceToView(cell.headImgView, 17).widthIs(226*KScreenW/375);
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.WenZhangViewControllerCellSelect) {
        self.WenZhangViewControllerCellSelect(self.dataSource[indexPath.row].article_id,self.dataSource[indexPath.row].article_title);
    }
}
-(void)getAriceList{
    [GMAfnTools PostHttpDataWithUrlStr:KCourseFreeList Dic:nil SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"首页--育朵精选%@",responseObject);
        for (NSMutableDictionary *dic in responseObject[@"free_article"]) {
            YDSelectModel *mol = [[YDSelectModel alloc]init];
            [mol mj_setKeyValues:dic];
            [self.dataSource addObject:mol];
        }
        [self.coureseTableView reloadData];
    } FailureBlock:^(id  _Nonnull error) {
        
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
