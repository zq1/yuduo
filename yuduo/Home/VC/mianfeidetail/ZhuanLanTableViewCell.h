//
//  ZhuanLanTableViewCell.h
//  yuduo
//
//  Created by Mac on 2019/8/13.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZhuanLanTableViewCell : UITableViewCell
PropertyStrong(UIView, bgView);
PropertyStrong(UIImageView, headImg);
PropertyStrong(UILabel, titleLable);
PropertyStrong(UIImageView, keshiImg);
PropertyStrong(UILabel, keshiLable);
PropertyStrong(UIImageView, seeImg);
PropertyStrong(UILabel, peopleLable);
PropertyStrong(UILabel, mianfeiLable);
PropertyStrong(UILabel, xPriceLabel);
PropertyStrong(UILabel, priceLabel);
PropertyStrong(UILabel, lineLabel);
@end

NS_ASSUME_NONNULL_END
