//
//  HomeRightItemView.m
//  Beauty
//
//  Created by llk on 2019/3/1.
//  Copyright © 2019 Beauty. All rights reserved.
//

#import "HomeRightItemView.h"

@interface HomeRightItemView()

@property(nonatomic,strong)UIButton * messageButton;

@property(nonatomic,strong)UIButton * bbsBtn;

@property(nonatomic,strong)UILabel * numberLabel;

@end

@implementation HomeRightItemView

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self addSubview:self.messageButton];
       // [self.messageImageView addSubview:self.numberLabel];
        
        [self addSubview:self.bbsBtn];
    }
    return self;
}
-(UIButton *)messageButton{
    if (!_messageButton) {
        _messageButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _messageButton.frame= CGRectMake(5, 0, 25, 25);
         [_messageButton setBackgroundImage:[UIImage imageNamed:@"矢量智能对象(14)"] forState:UIControlStateSelected];
         [_messageButton setBackgroundImage:[UIImage imageNamed:@"矢量智能对象(8)"] forState:UIControlStateNormal];
        [_messageButton addTarget:self action:@selector(messageButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _messageButton;
}
/*
-(UILabel *)numberLabel{
    if (!_numberLabel) {
        _numberLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, -5, 13, 13)];
        _numberLabel.backgroundColor = kHexRGB(0x6e88c1);
        //_numberLabel.text = @"0";
        _numberLabel.textColor = kWhiteColor;
        _numberLabel.font = kPingFangTC_Light(kIphone6Width(10));
        _numberLabel.textAlignment = NSTextAlignmentCenter;
        _numberLabel.layer.cornerRadius = kIphone6Width(6.5);
        _numberLabel.layer.masksToBounds = YES;
        _numberLabel.hidden = YES;
    }
    return _numberLabel;
}*/
-(UIButton *)bbsBtn{
    if (!_bbsBtn) {
        _bbsBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _bbsBtn.frame= CGRectMake(45, 0, 25, 25);
        
        [_bbsBtn addTarget:self action:@selector(bbsBtnTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _bbsBtn;
}
-(void)messageButtonTouchUpInside:(UIButton *)send{
    
    if (self.HomeRightItemViewMessageButtonBlock) {
        self.HomeRightItemViewMessageButtonBlock(send);
    }
}
-(void)bbsBtnTouchUpInside:(UIButton *)sender{
    if (self.HomeRightItemViewBbsButtonBlock) {
        self.HomeRightItemViewBbsButtonBlock(sender);
    }
}
-(void)reloadLeftImage:(NSString *)left rightImage:(NSString *)right{
    [_messageButton setImage:[UIImage imageNamed:left] forState:UIControlStateNormal];
    [_bbsBtn setImage:[UIImage imageNamed:right] forState:UIControlStateNormal];
}
-(void)setIsCollection:(NSString *)isCollection{
    _isCollection = isCollection;
    self.messageButton.selected = isCollection;;
//    if (isCollection) {
//        
//        [_messageButton setImage:[UIImage imageNamed:@"矢量智能对象(14)"] forState:UIControlStateNormal];
//    }else{
//        [_messageButton setImage:[UIImage imageNamed:@"矢量智能对象(8)"] forState:UIControlStateNormal];
//        
//    }
}
/*
-(void)setMessageNum:(NSInteger)messageNum{
    _messageNum = messageNum;
    if (messageNum != 0) {
        self.numberLabel.hidden = NO;
        self.numberLabel.text = [NSString stringWithFormat:@"%ld",messageNum];
    }else{
        self.numberLabel.hidden = YES;
    }
}*/
@end
