//
//  HomeRightItemView.h
//  Beauty
//
//  Created by llk on 2019/3/1.
//  Copyright © 2019 Beauty. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeRightItemView : UIView

@property (nonatomic, copy) void (^HomeRightItemViewMessageButtonBlock)(UIButton * sender);

@property (nonatomic, copy) void (^HomeRightItemViewBbsButtonBlock)(UIButton * sender);
-(void)reloadLeftImage:(NSString *)left rightImage:(NSString *)right;

@property(nonatomic,strong)NSString * isCollection;
@end

NS_ASSUME_NONNULL_END
