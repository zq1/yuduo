//
//  NewYuDuoChoiceDetailViewController.h
//  yuduo
//
//  Created by 刘耀聪 on 2019/10/25.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface NewYuDuoChoiceDetailViewController : BaseViewController
PropertyStrong(NSString, articleStringID);
PropertyStrong(NSString, articleTitle);
@end

NS_ASSUME_NONNULL_END
