//
//  NewYuduoChoiceTableViewHeadView.m
//  yuduo
//
//  Created by 刘耀聪 on 2019/10/25.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "NewYuduoChoiceTableViewHeadView.h"

@interface NewYuduoChoiceTableViewHeadView ()
<UIWebViewDelegate,WKNavigationDelegate,WKUIDelegate,WKScriptMessageHandler,UIScrollViewDelegate>

@property (nonatomic, assign) CGFloat webHeight;

@property (nonatomic, strong) WKWebViewConfiguration *configuration;
@end

@implementation NewYuduoChoiceTableViewHeadView
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = GMWhiteColor;
    }
    return self;
}
- (WKWebView *)webView{
    if (!_webView) {
        _webView = [[WKWebView alloc] init];
        _webView.navigationDelegate = self;
        _webView.UIDelegate = self;
        [_webView.scrollView setShowsVerticalScrollIndicator:NO];
        [_webView.scrollView setShowsHorizontalScrollIndicator:NO];
        //[_webView.scrollView addObserver:self forKeyPath:@"contentSize" options:NSKeyValueObservingOptionNew context:nil];
       //////// _webView.scrollView.scrollEnabled = NO;
        self.webView.frame = CGRectMake(10, 0,KScreenW-20, self.frame.size.height);
               [self addSubview:self.webView];
    }
    return _webView;
}
-(void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
    NSString *injectionJSString = @"var script = document.createElement('meta');"
    "script.name = 'viewport';"
    "script.content=\"width=device-width, user-scalable=no\";"
    "document.getElementsByTagName('head')[0].appendChild(script);";
    [_webView evaluateJavaScript:injectionJSString completionHandler:nil];

}
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context{
    
//    WS(weakSelf);
    if ([keyPath isEqualToString:@"contentSize"]) {
        
        self.webHeight = self.webView.scrollView.contentSize.height;
        if ([self.delegate respondsToSelector:@selector(changeHeaderViewHeight:)]) {
            [self.delegate changeHeaderViewHeight:self.webHeight +36];
        }
        self.webView.frame = CGRectMake(10, 0,KScreenW-20, self.webHeight);
        [self addSubview:self.webView];
    }
}
-(void)setWebCode:(NSString *)webCode{
    [self.webView loadHTMLString:webCode baseURL:nil];
}
@end
