//
//  MessageDetailInputView.m
//  Buauty
//
//  Created by llk on 2018/4/2.
//  Copyright © 2017年 llk. All rights reserved.
//

#import "MessageDetailInputView.h"

@interface MessageDetailInputView () <UITextFieldDelegate>

/// 发送按钮
@property (nonatomic, strong) UIButton *sendButton;
@property (nonatomic, assign) NSInteger textLocation;//这里声明一个全局属性，用来记录输入位置

@end

@implementation MessageDetailInputView

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self addSubview:self.inputTextField];
        [self addSubview:self.sendButton];
       // [self addMasonry];
    }
    return self;
}

#pragma mark - # Event Response
- (void)sendButtonTouchUpInside:(UIButton *)sender {
    
    if ([self.inputTextField.text isEqualToString:@""]) {
      //  [ToastManage showTopToastWith:@"请输入评论内容"];
        return;
    }
    if (self.messageDetailInputViewSendButtonBlock) {
        self.messageDetailInputViewSendButtonBlock(self.inputTextField.text);
    }
}


#pragma mark - # Getter
- (UITextField *)inputTextField {
    if (!_inputTextField) {
        _inputTextField = [[UITextField alloc] initWithFrame:CGRectMake(10, 10, KScreenW - 100, 40)];
        [_inputTextField.layer setMasksToBounds:YES];
        [_inputTextField.layer setCornerRadius:5];
        [_inputTextField setPlaceholder:@"说点什么吧..."];
        [_inputTextField setDelegate:self];
        _inputTextField.layer.borderColor = [GMBrownColor CGColor];
        _inputTextField.layer.borderWidth =1;
        //[_inputTextField setValue:kFont(13) forKeyPath:@"_placeholderLabel.font"];
        _inputTextField.leftView = [[UIView alloc] initWithFrame:CGRectMake(15, 0, 5, 40)];
        _inputTextField.leftViewMode = UITextFieldViewModeAlways;
    }
    return _inputTextField;
}

- (UIButton *)sendButton {
    if (!_sendButton) {
        _sendButton = [[UIButton alloc] initWithFrame:CGRectMake(KScreenW - 60, 15, 50, 30)];
        [_sendButton addTarget:self action:@selector(sendButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
        [_sendButton setTitle:@"提交" forState:UIControlStateNormal];
        [_sendButton setTitleColor:GMWhiteColor forState:UIControlStateNormal];
        _sendButton.backgroundColor = RGB(21, 152, 164);
        _sendButton.layer.cornerRadius = 15;
        _sendButton.layer.masksToBounds = YES;
        //[_sendButton setTitleColor:GMRedColor forState:UIControlStateNormal];
        _sendButton.titleLabel.font = kFont(13);
    }
    return _sendButton;
}

@end

