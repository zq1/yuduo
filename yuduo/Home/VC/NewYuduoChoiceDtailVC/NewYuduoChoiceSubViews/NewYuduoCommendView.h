//
//  NewYuduoCommendView.h
//  yuduo
//
//  Created by 刘耀聪 on 2019/10/25.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NewYuduoCommendView : UITableViewHeaderFooterView
@property (nonatomic, copy) void (^commentHeaderViewBlock)(NSString * commendText);
@property (nonatomic, copy) void (^likeButtonTouchUpInsideBlock)(UIButton * sender);
@property(nonatomic,assign)NSInteger number;
@property(nonatomic,assign)NSInteger likeNumber;

@end

NS_ASSUME_NONNULL_END
