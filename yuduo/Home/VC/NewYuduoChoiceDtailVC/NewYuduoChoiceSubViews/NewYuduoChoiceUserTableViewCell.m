//
//  NewYuduoChoiceUserTableViewCell.m
//  zhuanzhuan
//
//  Created by ZZUIHelper on 2019/10/25.
//  Copyright © 2017年 ZZUIHelper. All rights reserved.
//

#import "NewYuduoChoiceUserTableViewCell.h"
#import "NSString+Extension.h"
@interface NewYuduoChoiceUserTableViewCell ()

@property (nonatomic, strong) UIImageView *headImageView;

@property (nonatomic, strong) UILabel *userNameLabel;

@property (nonatomic, strong) UILabel *timeLabel;

@property (nonatomic, strong) UILabel *commendLabel;

@property (nonatomic, strong) UIView *lineView;

@end

@implementation NewYuduoChoiceUserTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self.contentView addSubview:self.headImageView];
        [self.contentView addSubview:self.userNameLabel];
        [self.contentView addSubview:self.timeLabel];
        [self.contentView addSubview:self.commendLabel];
        [self.contentView addSubview:self.lineView];
        [self addMasonry];
    }
    return self;
}

#pragma mark - # Private Methods
- (void)addMasonry {
    self.headImageView.frame = CGRectMake(15, 15, 40, 40);
    self.userNameLabel.frame = CGRectMake(60, 15, KScreenW-100, 18);
    self.timeLabel.frame = CGRectMake(60, 40, KScreenW-100, 18);
    
}

#pragma mark - # Getter
- (UIImageView *)headImageView {
    if (!_headImageView) {
        _headImageView = [[UIImageView alloc] init];
        _headImageView.contentMode = UIViewContentModeScaleAspectFill;
        _headImageView.backgroundColor = GMBrownColor;
        _headImageView.layer.cornerRadius = 20;
        _headImageView.layer.masksToBounds = YES;
    }
    return _headImageView;
}

- (UILabel *)userNameLabel {
    if (!_userNameLabel) {
        _userNameLabel = [[UILabel alloc] init];
        _userNameLabel.textColor = GMBlackColor;
        _userNameLabel.font = kFont(14);
        _userNameLabel.text = @"用户昵称";
    }
    return _userNameLabel;
}

- (UILabel *)timeLabel {
    if (!_timeLabel) {
        _timeLabel = [[UILabel alloc] init];
        _timeLabel.textColor = RGB(153, 153, 153);
        _timeLabel.font = kFont(14);
        _timeLabel.text = @"2019.05.10  10:00:00";
    }
    return _timeLabel;
}

- (UILabel *)commendLabel {
    if (!_commendLabel) {
        _commendLabel = [[UILabel alloc] init];
        _commendLabel.numberOfLines = 0;
        _commendLabel.textColor = GMBlackColor;
        _commendLabel.font = kFont(14);
        _commendLabel.text = @"课程很好，在对过敏性鼻炎，哮喘和咳嗽的长期控制中，是否正常的打育苗？需要提前停药吗？";
    }
    return _commendLabel;
}

- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = RGB(153, 153, 153);
    }
    return _lineView;
}
-(void)setModel:(KCPJModel *)model{
    _model = model;
    [self.headImageView sd_setImageWithURL:[NSURL URLWithString:model.portrait]];
    self.userNameLabel.text = model.nikname;
    self.timeLabel.text = model.comments_time;
    self.commendLabel.text =model.comments;
    CGSize size = [model.comments calculateTextConstrainedInSize:CGSizeMake(KScreenW - 100, MAXFLOAT) textFont:13 lineBreakMode:NSLineBreakByClipping];
    //self.commendLabel.height = size.height+ 20;
    self.commendLabel.frame = CGRectMake(60, 60, KScreenW - 80, size.height+ 20);
    self.lineView.frame = CGRectMake(60, CGRectGetMaxY(self.commendLabel.frame)+10, KScreenW - 80, 1);
}
@end
