//
//  NewYuduoChoiceDetailToolView.m
//  yuduo
//
//  Created by 刘耀聪 on 2019/10/28.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "NewYuduoChoiceDetailToolView.h"
@interface NewYuduoChoiceDetailToolView()

@property(nonatomic,strong)UIView * lineView;

@property(nonatomic,strong)UIButton * buyButton;

@end
@implementation NewYuduoChoiceDetailToolView

-(id)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
       [self addSubview:self.lineView];
        [self addSubview:self.buyButton];
        self.lineView.frame = CGRectMake(0, 0, KScreenW, 1);
        self.buyButton.frame = CGRectMake(10, 8, KScreenW-20, 30);
    }
    return self;
}
-(UIView *)lineView{
    if (!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = GMlightGrayColor;
    }
    return _lineView;
}
-(UIButton *)buyButton{
    if (!_buyButton) {
        _buyButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_buyButton setTitle:@"为朋友购买" forState:UIControlStateNormal];
        [_buyButton setTintColor:GMWhiteColor];
        _buyButton.backgroundColor = RGB(228, 102, 67);
        _buyButton.layer.cornerRadius = 15;
        _buyButton.layer.masksToBounds = YES;
        [_buyButton addTarget:self action:@selector(buyButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _buyButton;
}
-(void)buyButtonTouchUpInside:(UIButton *)sender{
    
}
@end
