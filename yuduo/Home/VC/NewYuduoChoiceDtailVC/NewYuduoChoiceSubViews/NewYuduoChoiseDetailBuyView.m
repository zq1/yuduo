//
//  NewYuduoChoiseDetailBuyView.m
//  zhuanzhuan
//
//  Created by ZZUIHelper on 2019/10/28.
//  Copyright © 2017年 ZZUIHelper. All rights reserved.
//

#import "NewYuduoChoiseDetailBuyView.h"

@interface NewYuduoChoiseDetailBuyView ()

@property (nonatomic, strong) UIView *lineView;

@property (nonatomic, strong) UILabel *priceLabel;

@property (nonatomic, strong) UIButton *friendButton;

@property (nonatomic, strong) UIButton *buyButton;

@end

@implementation NewYuduoChoiseDetailBuyView

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self addSubview:self.lineView];
        [self addSubview:self.priceLabel];
        [self addSubview:self.friendButton];
        [self addSubview:self.buyButton];
        [self addMasonry];
    }
    return self;
}

#pragma mark - # Event Response
- (void)friendButtonTouchUpInside:(UIButton *)sender {
    
}

- (void)buyButtonTouchUpInside:(UIButton *)sender {
    
}

#pragma mark - # Private Methods
- (void)addMasonry {
    self.lineView.frame = CGRectMake(0, 0, KScreenW, 1);
    self.priceLabel.frame = CGRectMake(15, 10, 50, 30);
    self.friendButton.frame = CGRectMake(KScreenW/2, 10, 80, 30);
    self.buyButton.frame = CGRectMake((KScreenW/2)+90, 10, 80, 30);
}

#pragma mark - # Getter
- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = GMlightGrayColor;
    }
    return _lineView;
}

- (UILabel *)priceLabel {
    if (!_priceLabel) {
        _priceLabel = [[UILabel alloc] init];
        _priceLabel.textAlignment = NSTextAlignmentCenter;
        _priceLabel.font = kFont(16);
        _priceLabel.textColor = RGB(228, 102, 67);
    }
    return _priceLabel;
}

- (UIButton *)friendButton {
    if (!_friendButton) {
        _friendButton = [[UIButton alloc] init];
        [_friendButton setTitle:@"为朋友购买" forState:UIControlStateNormal];
        _friendButton.titleLabel.font = kFont(13);
               [_friendButton setTintColor:GMWhiteColor];
               _friendButton.backgroundColor = RGB(252, 171, 71);
               _friendButton.layer.cornerRadius = 15;
               _friendButton.layer.masksToBounds = YES;
        [_friendButton addTarget:self action:@selector(friendButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _friendButton;
}

- (UIButton *)buyButton {
    if (!_buyButton) {
        _buyButton = [[UIButton alloc] init];
        [_buyButton setTitle:@"立即购买" forState:UIControlStateNormal];
        [_buyButton setTintColor:GMWhiteColor];
        _buyButton.titleLabel.font = kFont(13);
        _buyButton.backgroundColor = RGB(228, 102, 67);
        _buyButton.layer.cornerRadius = 15;
        _buyButton.layer.masksToBounds = YES;
        [_buyButton addTarget:self action:@selector(buyButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _buyButton;
}
-(void)setPriceNumber:(CGFloat)priceNumber{
    self.priceLabel.text = [NSString stringWithFormat:@"%.2f",priceNumber];
}
@end
