//
//  NewYuduoChoiceReplayTableViewCell.h
//  yuduo
//
//  Created by 刘耀聪 on 2019/10/25.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KCPJModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface NewYuduoChoiceReplayTableViewCell : UITableViewCell
@property(nonatomic,strong)KCPJModel * model;

@end

NS_ASSUME_NONNULL_END
