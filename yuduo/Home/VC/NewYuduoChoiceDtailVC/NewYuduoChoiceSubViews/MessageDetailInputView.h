//
//  MessageDetailInputView.h
//  Beauty
//
//  Created by llk on 2018/4/2.
//  Copyright © 2018年 Beauty. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageDetailInputView : UIView

/// 输入框
@property (nonatomic, strong) UITextField *inputTextField;
@property (copy , nonatomic) void(^messageDetailInputViewSendButtonBlock)(NSString *inputTFText);
@end
