//
//  NewYuduoCommendView.m
//  zhuanzhuan
//
//  Created by ZZUIHelper on 2019/10/25.
//  Copyright © 2017年 ZZUIHelper. All rights reserved.
//

#import "NewYuduoCommendView.h"

@interface NewYuduoCommendView ()<UITextFieldDelegate>

@property (nonatomic, strong) UILabel *commendNumberLabel;

@property (nonatomic, strong) UIView *commendView;

@property (nonatomic, strong) UILabel *inputTextField;

@property (nonatomic, strong) UIButton *sendButton;
@property (nonatomic, strong) UILabel *lineLabel;
@property(nonatomic,strong)UIButton * likeButton;
@property (nonatomic, strong) UILabel *likeLabel;
@end

@implementation NewYuduoCommendView

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
          [self.contentView addSubview:self.lineLabel];
        [self.contentView addSubview:self.commendNumberLabel];
        [self.contentView addSubview:self.likeButton];
        [self.contentView addSubview:self.likeLabel];
        [self.contentView addSubview:self.commendView];
        [self.commendView addSubview:self.inputTextField];
        [self.commendView addSubview:self.sendButton];
        [self addMasonry];
        self.contentView.userInteractionEnabled = YES;
    }
    return self;
}

#pragma mark - # Event Response
- (void)sendButtonTouchUpInside:(UIButton *)sender {
//    if (self.commentHeaderViewBlock) {
//        self.commentHeaderViewBlock(@"点击发送");
//    }
    //[self.inputTextField resignFirstResponder];
    
}
-(void)inputTextFieldTouchUpInside:(UIGestureRecognizer *)tap{
    if (self.commentHeaderViewBlock) {
        self.commentHeaderViewBlock(@"点击发送");
    }
}
-(void)likeButtonTouchUpInside:(UIButton *)sender{
    if (self.likeButtonTouchUpInsideBlock) {
        self.likeButtonTouchUpInsideBlock(sender);
    }
}
#pragma mark - # Private Methods
- (void)addMasonry {
    self.lineLabel.frame=  CGRectMake(0, 0, KScreenW, 10);
    self.commendNumberLabel.frame = CGRectMake(15, 25, 150, 25);
    self.likeButton.frame = CGRectMake(KScreenW-70, 25, 25, 25);
    self.likeLabel.frame = CGRectMake(KScreenW - 45, 25, 40, 25);
    self.commendView.frame = CGRectMake(15, 60, KScreenW-30, 50) ;
    self.inputTextField.frame = CGRectMake(5, 10, KScreenW-150, 30);
    self.sendButton.frame = CGRectMake(KScreenW - 110, 10, 60, 30);
}

#pragma mark - # Getter
- (UILabel *)commendNumberLabel {
    if (!_commendNumberLabel) {
        _commendNumberLabel = [[UILabel alloc] init];
        _commendNumberLabel.font = kFont(15);
        _commendNumberLabel.textColor = GMBlackColor;
        
        //@"全部评价（99+）";
    }
    return _commendNumberLabel;
}
-(UILabel *)lineLabel{
    if (!_lineLabel) {
        _lineLabel = [[UILabel alloc] init];
        _lineLabel.backgroundColor = RGB(230, 230, 230);
    }
    return _lineLabel;
}
-(UILabel *)likeLabel{
    if (!_likeLabel) {
        _likeLabel = [[UILabel alloc] init];
        _likeLabel.backgroundColor = GMWhiteColor;
        //_likeLabel.textAlignment = NSTextAlignmentCenter;
        _likeLabel.font = kFont(12);
        _likeLabel.textColor = GMBlackColor;
    }
    return _likeLabel;
}
-(UIButton *)likeButton{
    if (!_likeButton) {
        _likeButton = [[UIButton alloc] init];
        //[_likeButton setTitle:@"发布" forState:UIControlStateNormal];
        [_likeButton setImage:[UIImage imageNamed:@"ico_zan_1"] forState:UIControlStateNormal];
        [_likeButton addTarget:self action:@selector(likeButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _likeButton;
}
- (UIView *)commendView {
    if (!_commendView) {
        _commendView = [[UIView alloc] init];
        _commendView.backgroundColor = GMWhiteColor;
        _commendView.layer.shadowColor = [UIColor blackColor].CGColor;
        // 设置阴影偏移量
        _commendView.layer.shadowOffset = CGSizeMake(0,1);
        // 设置阴影透明度
        _commendView.layer.shadowOpacity = 1;
        // 设置阴影半径
        _commendView.layer.shadowRadius = 1;
        _commendView.clipsToBounds = NO;
    }
    return _commendView;
}

- (UILabel *)inputTextField {
    if (!_inputTextField) {
        _inputTextField = [[UILabel alloc] init];
        [_inputTextField.layer setMasksToBounds:YES];
        [_inputTextField.layer setCornerRadius:7.5];
        _inputTextField.text = @"  在这里输入你的评论";
//        [_inputTextField setPlaceholder:@"在这里输入你的评论"];
//        [_inputTextField setDelegate:self];
        _inputTextField.layer.borderColor = [GMBlackColor CGColor];
        _inputTextField.layer.borderWidth =1;
        _inputTextField.font = kFont(12);
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(inputTextFieldTouchUpInside:)];
        [_inputTextField addGestureRecognizer:tap];
        _inputTextField.userInteractionEnabled = YES;
       // [_inputTextField setValue:kFont13 forKeyPath:@"_placeholderLabel.font"];
//        _inputTextField.leftView = [[UIView alloc] initWithFrame:CGRectMake(15, 0, 5, 40)];
//        _inputTextField.leftViewMode = UITextFieldViewModeAlways;
    }
    return _inputTextField;
}

- (UIButton *)sendButton {
    if (!_sendButton) {
        _sendButton = [[UIButton alloc] init];
        [_sendButton setTitle:@"发布" forState:UIControlStateNormal];
        [_sendButton setTitleColor:GMWhiteColor forState:UIControlStateNormal];
        _sendButton.titleLabel.font = kFont(14);
        _sendButton.backgroundColor = RGB(21, 152, 164);
        _sendButton.layer.cornerRadius = 15;
        _sendButton.layer.masksToBounds = YES;
        [_sendButton addTarget:self action:@selector(sendButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _sendButton;
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
}

-(void)textFieldDidChange:(UITextField *)textField{
    CGFloat maxLength = 200;
    NSString *toBeString = textField.text;
    //获取高亮部分
    UITextRange *selectedRange = [textField markedTextRange];
    UITextPosition *position = [textField positionFromPosition:selectedRange.start offset:0];
    if (!position || !selectedRange)
    {
        if (toBeString.length > maxLength)
        {
            NSRange rangeIndex = [toBeString rangeOfComposedCharacterSequenceAtIndex:maxLength];
            if (rangeIndex.length == 1){
                textField.text = [toBeString substringToIndex:maxLength];
            }
            else{
                NSRange rangeRange = [toBeString rangeOfComposedCharacterSequencesForRange:NSMakeRange(0, maxLength)];
                textField.text = [toBeString substringWithRange:rangeRange];
            }
        }
    }
}
-(void)setNumber:(NSInteger)number{
    _number = number;
    self.commendNumberLabel.text = [NSString stringWithFormat:@"全部评价（%ld)",number];
}
-(void)setLikeNumber:(NSInteger)likeNumber{
    _likeNumber = likeNumber;
    self.likeLabel.text = [NSString stringWithFormat:@"%ld",likeNumber];
}
@end
