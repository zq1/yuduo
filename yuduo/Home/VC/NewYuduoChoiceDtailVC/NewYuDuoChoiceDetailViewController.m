//
//  NewYuDuoChoiceDetailViewController.m
//  yuduo
//
//  Created by 刘耀聪 on 2019/10/25.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "NewYuDuoChoiceDetailViewController.h"
#import "NewYuduoChoiceTableViewHeadView.h"
#import "NewYuduoCommendView.h"
#import "NewYuduoChoiceUserTableViewCell.h"
#import "KCPJModel.h"
#import "MessageDetailInputView.h"
#import "NewYuduoChoiceReplayTableViewCell.h"
#import "NSString+Extension.h"
#import "HomeRightItemView.h"
#import "NewYuduoChoiceDetailToolView.h"
#import "NewYuduoChoiseDetailBuyView.h"
#import "NewYuduoChoiceLabel.h"
@interface NewYuDuoChoiceDetailViewController ()<UITableViewDataSource, UITableViewDelegate, UIWebViewDelegate, NewYuduoChoiceTableViewHeadViewDelegate,UITextFieldDelegate>
@property(nonatomic,strong)NewYuduoChoiceTableViewHeadView * headWebView;
@property(nonatomic, strong) UITableView *tableView;
@property(nonatomic, strong) NewYuduoCommendView *commendView;

@property(nonatomic, strong) NSMutableArray<KCPJModel *> *dateSource;
@property(nonatomic,strong)HomeRightItemView * rightItemvView;
@property(nonatomic,strong)NewYuduoChoiceLabel * topTitleLabel;
@property(nonatomic,assign)NSInteger likeNumber;
@property(nonatomic,strong)MessageDetailInputView * commendInputView;
@property(nonatomic,strong)NewYuduoChoiceDetailToolView * toolView;
@property(nonatomic,strong)NewYuduoChoiseDetailBuyView * buyView;

@end

@implementation NewYuDuoChoiceDetailViewController
-(void)setupRightItem{
    UIBarButtonItem *btn_right = [[UIBarButtonItem alloc] initWithCustomView:self.rightItemvView];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -10;
    
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:negativeSpacer, btn_right, nil];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.dateSource = [[NSMutableArray alloc] init];
    [self setLeftBarItemWithImage:@"back-icon"];
    //[self setRightBarItemWithString:@""];
    self.topTitleLabel.text = self.articleTitle;
    [self.view addSubview:self.tableView];
    [self getArticleString];
    [self setupTitle];
    [self setupRightItem];
    self.view.backgroundColor = RGB(230, 230, 230);
    self.commendInputView.hidden = YES;
    [self.view addSubview:self.toolView];

}
-(void)setupTitle{
    if (self.articleTitle.length > 10) {
        self.articleTitle = [[self.articleTitle substringWithRange:NSMakeRange(0,10)] stringByAppendingString:@"..."];
    }
    self.title = self.articleTitle;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHidden:) name:UIKeyboardWillHideNotification object:nil];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = YES;
     [[NSNotificationCenter defaultCenter] removeObserver:self];
}
//键盘即将出现的时候
- (void)keyboardWillShow:(NSNotification *)sender{

    CGRect keyboardRect = [(sender.userInfo[UIKeyboardFrameEndUserInfoKey]) CGRectValue];
    //改变bttomView的y值，防止被键盘遮住
    CGRect bottomViewRect = self.commendInputView.frame;
    bottomViewRect.origin.y = KScreenH - keyboardRect.size.height - self.commendInputView.frame.size.height-60;
    //bottomViewRect.size.height;
    self.commendInputView.frame = bottomViewRect;
    self.commendInputView.hidden = NO;
}
//键盘即将消失的时候
- (void)keyboardWillHidden:(NSNotification *)sender{
    CGRect bottomViewRect = self.commendInputView.frame;
    bottomViewRect.origin.y = self.view.frame.size.height;
    self.commendInputView.frame = bottomViewRect;
    self.commendInputView.hidden = YES;
}
- (void)getArticleString {
    [self.dateSource removeAllObjects];
    NSUserDefaults *detafaults = [NSUserDefaults standardUserDefaults];
    NSString  *user_id =  [detafaults objectForKey:@"user_id"];
    NSString *token =  [detafaults objectForKey:@"token"];
    NSMutableDictionary *articleDic = [NSMutableDictionary dictionary];
    articleDic[@"user_id"] = user_id;
    articleDic[@"token"] = token;
    articleDic[@"article_id"] = self.articleStringID;
    WS(weakSelf);
    [GMAfnTools PostHttpDataWithUrlStr:KWenZHangDetail Dic:articleDic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"打印文章详情----%@",responseObject);
        for (NSDictionary * dic in responseObject[@"user_comment"]) {
             KCPJModel *mol = [[KCPJModel alloc]init];
             [mol mj_setKeyValues:dic];
            [self.dateSource addObject:mol];
        }
        if ([responseObject[@"article_is_free"] integerValue] == 1) {
            weakSelf.headWebView.webCode = responseObject[@"article_details"];
        }else{
            if ([responseObject[@"article_is_try"] integerValue] == 1) {
                 weakSelf.headWebView.webCode = responseObject[@"article_try_details"];
                self.buyView.priceNumber = [responseObject[@"article_price"] floatValue];
                [self.view addSubview:self.buyView];
            }else{
                if ([responseObject[@"article_is_friends_buys"] integerValue] == 1){
                    weakSelf.headWebView.webCode = responseObject[@"article_details"];
                    [self.view addSubview:self.toolView];
                }else{
                    weakSelf.headWebView.webCode = responseObject[@"article_details"];
                }
            }
            
        }
        weakSelf.rightItemvView.isCollection = responseObject[@"is_collection"];
        [weakSelf.tableView reloadData];
    } FailureBlock:^(id  _Nonnull error) {
        
    }];
}
#pragma mark Getter...
-(MessageDetailInputView *)commendInputView{
    if (!_commendInputView) {
        _commendInputView = [[MessageDetailInputView alloc] init];
        _commendInputView.frame = CGRectMake(0, KScreenH, KScreenW, 60);
        _commendInputView.backgroundColor = GMWhiteColor;
        _commendInputView.inputTextField.delegate = self;
        WS(weakSelf);
        __weak typeof(MessageDetailInputView) *weakInputView = _commendInputView;
        _commendInputView.messageDetailInputViewSendButtonBlock = ^(NSString *inputTFText) {
            [weakSelf postCommend:inputTFText];
            weakInputView.hidden = YES;
            [weakInputView.inputTextField resignFirstResponder];
        };
        [self.view addSubview:_commendInputView];
    }
    return _commendInputView;
}
-(NewYuduoChoiceDetailToolView *)toolView{
    if (!_toolView) {
        _toolView = [[NewYuduoChoiceDetailToolView alloc] initWithFrame:CGRectMake(0, KScreenH-kNavigationHeight - 50, KScreenW, 50)];
        _toolView.backgroundColor= GMWhiteColor;
    }
    return _toolView;
}
-(NewYuduoChoiseDetailBuyView *)buyView{
    if (!_buyView) {
        _buyView = [[NewYuduoChoiseDetailBuyView alloc] initWithFrame:CGRectMake(0, KScreenH-kNavigationHeight - 50, KScreenW, 50)];
        _buyView.backgroundColor= GMWhiteColor;
    }
    return _buyView;
}
-(NewYuduoChoiceLabel *)topTitleLabel{
    if (!_topTitleLabel) {
        _topTitleLabel = [[NewYuduoChoiceLabel alloc] initWithFrame:CGRectMake(0, 10, KScreenW, 50)];
        //_topTitleLabel.textAlignment = NSTextAlignmentCenter;
        _topTitleLabel.font = kFont(16);
        _topTitleLabel.textColor = GMBlackColor;
        _topTitleLabel.backgroundColor = GMWhiteColor;
        _topTitleLabel.numberOfLines = 0;
        UIView * lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 49, KScreenW, 1)];
        lineView.backgroundColor = GMlightGrayColor;
        [_topTitleLabel addSubview:lineView];
        [self.view addSubview:_topTitleLabel];
    }
    return _topTitleLabel;
}
-(HomeRightItemView *)rightItemvView{
    if (!_rightItemvView) {
        _rightItemvView = [[HomeRightItemView alloc] initWithFrame:CGRectMake(0, 0, 60, 30)];
        [_rightItemvView reloadLeftImage:@"矢量智能对象(8)" rightImage:@"fx-dis"];
        WS(weakSelf);
        _rightItemvView.HomeRightItemViewMessageButtonBlock = ^(UIButton * _Nonnull sender) {
            [weakSelf oneClick:sender];
        };
        _rightItemvView.HomeRightItemViewBbsButtonBlock = ^(UIButton * _Nonnull sender) {
            //[weakSelf twoClick];
        };
    }
    return _rightItemvView;
}
- (void)postCommend:(NSString *)commend{
    NSUserDefaults *defauls = [NSUserDefaults standardUserDefaults];
    NSString *user_id = [defauls objectForKey:@"user_id"];
    NSString *token = [defauls objectForKey:@"token"];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"user_id"] = user_id;
    dic[@"token"] = token;
    dic[@"type"] = @"3";
    dic[@"title"] = self.articleTitle;
    dic[@"content_id"] = self.articleStringID;
    dic[@"reply"] = commend;
    //self.plArray = [NSMutableArray array];
    WS(weakSelf);
    [GMAfnTools PostHttpDataWithUrlStr:KHomeCoursePJ Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"文章评价----%@",responseObject);
        [weakSelf getArticleString];
    } FailureBlock:^(id  _Nonnull error) {
        
    }];
}
- (void)oneClick:(UIButton *)send {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:@"token"] ) {
        
        
        send.selected = !send.selected;
        if (send.selected == YES) {
           
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            dic[@"user_id"] = [defaults objectForKey:@"user_id"];
            dic[@"token"] = [defaults objectForKey:@"token"];
            dic[@"type"] = @"3";
            dic[@"collection_id"] = self.articleStringID;
            
            [GMAfnTools PostHttpDataWithUrlStr:KCollectAdd Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
                NSLog(@"收藏成功++%@",responseObject);
                if ([[responseObject objectForKey:@"code"] isEqualToString:@"1"]) {
                } else {
                    NSLog(@"取消成功");
                    //                [self showError:[responseObject objectForKey:@"msg"]];
                }
            } FailureBlock:^(id  _Nonnull error) {
                
            }];
            
            NSLog(@"搜藏");
        }
        if (send.selected == NO) {
            //NSLog(@"xxxxxx%@",self.cancelCollectString);
           // [send setBackgroundImage:[UIImage imageNamed:@"矢量智能对象(8)"] forState:UIControlStateNormal];
            [self test];
            NSLog(@"取消搜藏");
        }
    }else {
        [self showError:@"请先登录"];
    }
}
- (void)test {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"user_id"] = [defaults objectForKey:@"user_id"];
    dic[@"token"] = [defaults objectForKey:@"token"];
    dic[@"type"] = @"1";
    dic[@"collection_id"] = self.articleStringID;
    NSLog(@"--%@--%@",dic,KCollectCancelxx);
    [GMAfnTools PostHttpDataWithUrlStr:KCollectCancelxx Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
        if ([[responseObject objectForKey:@"code"] isEqualToString:@"1"]) {
            [self showError:[responseObject objectForKey:@"msg"]];
        }
        NSLog(@"%@",responseObject);
    } FailureBlock:^(id  _Nonnull error) {
        NSLog(@"取消收藏失败---%@",error);
    }];
}
#pragma mark -- 提示框
- (void)showError:(NSString *)errorMsg {
    // 1.弹框提醒
    // 初始化对话框
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:errorMsg preferredStyle:UIAlertControllerStyleAlert];
    //    [alert addAction:[UIAlertAction actionWithTitle:@"" style:UIAlertActionStyleDefault handler:nil]];
    // 弹出对话框
    [self presentViewController:alert animated:true completion:nil];
    
    [self performSelector:@selector(dimisssAlert:) withObject:alert afterDelay:1];
    
}

- (void) dimisssAlert:(UIAlertController *)alert
{
    if (alert) {
        [alert dismissViewControllerAnimated:YES completion:nil];
    }
    
}
#pragma mark - # Getter
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0,60, KScreenW, KScreenH-kNavigationHeight - 60) style:UITableViewStyleGrouped];
        [_tableView setDelegate:self];
        [_tableView setDataSource:self];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//        _tableView.rowHeight = UITableViewAutomaticDimension;
//        _tableView.estimatedRowHeight = 100;
        _tableView.tableHeaderView = self.headWebView;
        //self.headWebView.delegate = self;
        //_tableView.tableHeaderView.height = kIphone6Width(320);
    }
        
    return _tableView;
}
-(NewYuduoChoiceTableViewHeadView *)headWebView{
        if (_headWebView == nil) {
            _headWebView = [[NewYuduoChoiceTableViewHeadView alloc] initWithFrame:CGRectMake(10, 0, KScreenW-20, 400)];
        }
        return _headWebView;
}

-(void)changeHeaderViewHeight:(CGFloat)height{
    self.headWebView.height = 500;
    [self.tableView reloadData];
}
#pragma mark - # Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.dateSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    KCPJModel * model = self.dateSource[indexPath.row];
    if (!model.service_reply) {
        static NSString *cellID = @"NewYuduoChoiceUserTableViewCell";
            NewYuduoChoiceUserTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        //    ActiveAddressTableViewCell *cell1 = [tableView dequeueReusableCellWithIdentifier:cellID1];
            if (!cell) {
                cell = [[NewYuduoChoiceUserTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
            }
        cell.model = model;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }else{
        static NSString *cellID = @"NewYuduoChoiceReplayTableViewCell";
            NewYuduoChoiceReplayTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        //    ActiveAddressTableViewCell *cell1 = [tableView dequeueReusableCellWithIdentifier:cellID1];
            if (!cell) {
                cell = [[NewYuduoChoiceReplayTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
            }
        cell.model = model;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
   
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{

    static NSString *viewIdentfier = @"headView";
    
    NewYuduoCommendView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:viewIdentfier];
    
    if(!headerView){
        headerView = [[NewYuduoCommendView alloc] initWithReuseIdentifier:viewIdentfier];
    }
    headerView.contentView.backgroundColor = GMWhiteColor;
    headerView.number = self.dateSource.count;
    headerView.likeNumber = self.likeNumber;
//    headerView.model = self.postDetailModel;
    WS(weakSelf);
    headerView.commentHeaderViewBlock = ^(NSString * _Nonnull commendText) {
        weakSelf.inputView.hidden = NO;
        [self.commendInputView.inputTextField becomeFirstResponder];
    };
    __weak typeof(NewYuduoCommendView *) newHeadView = headerView;
    headerView.likeButtonTouchUpInsideBlock = ^(UIButton * _Nonnull sender) {
        NSMutableDictionary* dic = [[NSMutableDictionary alloc] init];
                          [dic setValue:self.articleStringID forKey:@"article_id"];
        [GMAfnTools PostHttpDataWithUrlStr:KhomeArticelLike Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
            //点赞成功
            NSLog(@"点赞状态：%@",responseObject);
            if ([responseObject [@"code"] isEqualToString:@"1"]) {
                newHeadView.likeNumber = self.likeNumber+=1;
            }
        } FailureBlock:^(id  _Nonnull error) {
            //点赞失败
        }];
    };
    
    return headerView;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 120;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    KCPJModel * model = self.dateSource[indexPath.row];
    if (!model.service_reply) {
        CGSize size = [model.comments calculateTextConstrainedInSize:CGSizeMake(KScreenW - 100, MAXFLOAT) textFont:13 lineBreakMode:NSLineBreakByClipping];
        return 100 + size.height;
    }else{
         CGSize size = [model.comments calculateTextConstrainedInSize:CGSizeMake(KScreenW - 100, MAXFLOAT) textFont:13 lineBreakMode:NSLineBreakByClipping];
         CGSize resize = [model.service_reply calculateTextConstrainedInSize:CGSizeMake(KScreenW - 100, MAXFLOAT) textFont:13 lineBreakMode:NSLineBreakByClipping];
        return 130 + size.height + resize.height;
    }
    
}


//- (NewYuduoChoiceTableViewHeadView *)headWebView{

//}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
