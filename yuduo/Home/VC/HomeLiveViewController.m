//
//  HomeLiveViewController.m
//  yuduo
//
//  Created by Mac on 2019/8/12.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "HomeLiveViewController.h"
#import "HomeLiveTableViewCell.h"
#import "NewZhiboDetailViewController.h"
#import "CourseLiveListsModel.h"
@interface HomeLiveViewController ()<UITableViewDelegate,UITableViewDataSource>
PropertyStrong(UITableView, liveTableView);
@property(nonatomic,strong)NSMutableArray<CourseLiveListsModel*>*dateSource;
@end

@implementation HomeLiveViewController

//修改返回问题
#pragma mark--解决返回导航消失问题
- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBar.topItem.title = @"发现";
}
//- (void)viewWillDisappear:(BOOL)animated
//{
//    [super viewWillDisappear:animated];
//    self.navigationController.navigationBar.topItem.title = @"上一级标题";
//
//}
- (void)viewDidLoad {
    [super viewDidLoad];
    //创建tableview
    [self createTableView];
    [self changeNavigation];
    [self getCourseLiveData];
    // Do any additional setup after loading the view.
}
- (void)getCourseLiveData {
    self.dateSource = [NSMutableArray array];
    [GMAfnTools PostHttpDataWithUrlStr:KCourseLiveList Dic:nil SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"----课程---推荐直播--%@",responseObject);
        for (NSMutableDictionary *liveDic in responseObject) {
            CourseLiveListsModel *mol = [[CourseLiveListsModel alloc]init];
            [mol mj_setKeyValues:liveDic];
            
            [self.dateSource addObject:mol];
        }
        [self.liveTableView reloadData];
    } FailureBlock:^(id  _Nonnull error) {
        NSLog(@"-+++++%@",error);
    }];
}
- (void)createTableView {
    self.liveTableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.liveTableView.delegate = self;
    self.liveTableView.showsVerticalScrollIndicator = NO;
    self.liveTableView.dataSource = self;
    self.liveTableView.separatorStyle = UITableViewCellSelectionStyleNone;
    [self.view addSubview:self.liveTableView];
    self.liveTableView.sd_layout.topSpaceToView(self.view, 54+MStatusBarHeight)
    .leftSpaceToView(self.view, 0)
    .rightSpaceToView(self.view, 0)
    .widthIs(KScreenW).heightIs(KScreenH-54-MStatusBarHeight);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dateSource.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 239;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellId = @"cellID";
    HomeLiveTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    
    if (!cell) {
        
        cell = [[HomeLiveTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];;
        
    }
    [cell.imgView sd_setImageWithURL:[NSURL URLWithString:self.dateSource[indexPath.row].publicity_cover]];
    cell.titleLabel.text = self.dateSource[indexPath.row].name;
    //cell.tagLiveLabel.text = self.dateSource[indexPath.row].live_status;
    switch ([self.dateSource[indexPath.row].live_status integerValue]) {
        case 0:
            {
                cell.tagLiveLabel.text = @"未知状态";
            }
            break;
        case 1:
            {
                cell.tagLiveLabel.text = @"即将直播";
                cell.tagLiveLabel.textColor = RGB(23, 151, 164);
            }
            break;
        case 2:
            {
                cell.tagLiveLabel.text = @"正在直播";
                cell.tagLiveLabel.textColor = RGB(223, 95, 25);
            }
            break;
        case 3:
            {
                cell.tagLiveLabel.text = @"直播结束";
                cell.tagLiveLabel.textColor = GMBlackColor;
            }
            break;
        default:
            break;
    }
    cell.dateLabel.text = self.dateSource[indexPath.row].start_date;
    cell.backgroundColor = GMWhiteColor;
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NewZhiboDetailViewController * zhibo = [[NewZhiboDetailViewController alloc] init];
    
    [self.navigationController pushViewController:zhibo animated:NO];
}
- (void)changeNavigation {
    
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = GMWhiteColor;
    [self.view addSubview:view];
    view.sd_layout.topSpaceToView(self.view, 0)
    .leftSpaceToView(self.view, 0)
    .widthIs(KScreenW).heightIs(44+MStatusBarHeight);
    
    self.navigationController.navigationBar.topItem.title = @"直播";
    self.view.backgroundColor = GMWhiteColor;
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setBackgroundImage:[UIImage imageNamed:@"back-icon"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:backButton];
    backButton.sd_layout.bottomSpaceToView(view, 13)
    .leftSpaceToView(view, 15)
    .widthIs(11).heightIs(19);
    UIButton *backButtonView = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 80, MStatusBarHeight+44)];
    backButtonView.backgroundColor = RGBA(15, 15, 15, 0);
    [backButtonView addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButtonView];
    UILabel *titleLabel = [[UILabel alloc]init];
    [view addSubview:titleLabel];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"直播" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 18],NSForegroundColorAttributeName: [UIColor colorWithRed:69/255.0 green:69/255.0 blue:69/255.0 alpha:1.0]}];
    titleLabel.attributedText = string;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.sd_layout.bottomSpaceToView(view, 14)
    .centerXEqualToView(view)
    .heightIs(17).widthIs(100);
}

- (void)backBtn {
    [self.navigationController popViewControllerAnimated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
