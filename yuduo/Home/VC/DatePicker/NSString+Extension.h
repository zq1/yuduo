//
//  NSString+Extension.h
//  FindDrug
//
//  Created by Admin on 2017/11/14.
//  Copyright © 2017年 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Extension)

/**
 修改字符串中个别字符的颜色

 @param color 颜色
 @param range 位置
 @return NSMutableAttributedString
 */
-(NSMutableAttributedString *)changeWordColor:(UIColor *)color forRange:(NSRange)range;

/* 通过字符串计算Label高度 或 宽度
 * size : 宽度固定  CGSizeMake(width,CGFLOAT_MAX)
 高度固定  CGSizeMake(CGFLOAT_MAX,height)
 */
- (CGSize)calculateTextConstrainedInSize:(CGSize)size textFont:(CGFloat)fontSize lineBreakMode:(NSLineBreakMode)breakMode;
/**
 *  计算字符串的尺寸
 *
 *  @param font    字符串字体的大小
 *  @param maxSize 定义字符串显示的范围
 *
 *  @return 返回字符串的size
 */
- (CGSize)sizeOfStringWithFont:(UIFont *)font maxSize:(CGSize)maxSize;

/**
 *  计算字符串的高度
 *
 *  @param font  字符串的字体
 *  @param width 字符串显示的宽度,高度默认为MAXFLOAT
 *
 *  @return 字符串的高度
 */
- (CGFloat)heightOfStringWithFont:(UIFont *)font width:(CGFloat)width;

/**
 输入标准时间 输出倒计时小时和分钟
 */
+ (NSString *)stringWithTimeString:(NSString *)timeString;

/**
 手机号中间4位****

 @param phoneNum 手机号
 @return 新手机号
 */
+(NSString *)stringMakeThePhoneNumber:(NSString *)phoneNum;

- (NSString *)md5;

/// double类型price 变为 字符串类型

/**
 价格的精度处理

 @param conversionValue double类型price
 @return 字符串类型price
 */
+(NSString *)getPriceStrWithValue:(double)conversionValue;

+(NSString *)setPriceTopStrWithValue:(double)value;
/**
 判断字符串是否含有表情
 */
- (BOOL)isIncludingEmoji;

/**
 过滤掉字符串中中的emoji
 */
- (instancetype)removedEmojiString;

//字符串中是否含有中文
+(NSURL *)hasChinese:(NSString *)str;
/**
 字符串编码为 \ud83d\ude02
 */
+ (NSString *)EmojiCodingToStringWithEmojiStr:(NSString *)emojiStr;

/**
 \ud83d\ude02 解码为表情或汉字
 */
+ (NSString *)enCodeToEmojiStrWithStr:(NSString *)str;

@end
