//
//  NSString+Extension.m
//  FindDrug
//
//  Created by Admin on 2017/11/14.
//  Copyright © 2017年 Admin. All rights reserved.
//

#import "NSString+Extension.h"
#import <CommonCrypto/CommonDigest.h>
@implementation NSString (Extension)

-(NSMutableAttributedString *)changeWordColor:(UIColor *)color forRange:(NSRange)range{
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:self];
    [str addAttribute:NSForegroundColorAttributeName value:color range:range];
    return str;
}

- (CGSize)calculateTextConstrainedInSize:(CGSize)size textFont:(CGFloat)fontSize lineBreakMode:(NSLineBreakMode)breakMode {
    CGFloat systemVersion = [[UIDevice currentDevice].systemVersion floatValue];
    UIFont *font = [UIFont fontWithName:@"STHeitiSC-Light" size:fontSize];
    CGSize textSize = CGSizeZero;
    if (systemVersion >= 7.0) {
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineBreakMode = breakMode;
        
        NSDictionary *attributes = @{NSFontAttributeName:font,
                                     };
        textSize = [self boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size;
        
    }
   return textSize;
}
- (CGSize)sizeOfStringWithFont:(UIFont *)font maxSize:(CGSize)maxSize
{
    return [self sizeWithFont:font constrainedToSize:maxSize];
}

- (CGFloat)heightOfStringWithFont:(UIFont *)font width:(CGFloat)width
{
    return [self sizeOfStringWithFont:font maxSize:CGSizeMake(width, CGFLOAT_MAX)].height;
}
+ (NSString *)stringWithTimeString:(NSString *)timeString
{
    NSTimeInterval _interval = [timeString doubleValue] / 1000.0;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
    NSDateFormatter *objDateformat = [[NSDateFormatter alloc] init];
    [objDateformat setDateFormat:@"yyyy-MM-dd"];
    
    return [objDateformat stringFromDate: date];
}
+(NSString *)stringMakeThePhoneNumber:(NSString *)phoneNum
{
    NSString *numStr = phoneNum;
    NSString *str = @"****";
    NSString *rStr = nil;
    if (numStr.length == 11) {
        rStr = [numStr stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:str];
    }
    return rStr;
}

#pragma mark - Md5加密
- (NSString *)md5 {
    
    if(self == nil || [self length] == 0)
        return nil;
    
    const char *value = [self UTF8String];
    
    unsigned char outputBuffer[CC_MD5_DIGEST_LENGTH];
    CC_MD5(value, (CC_LONG)strlen(value), outputBuffer);
    
    NSMutableString *outputString = [[NSMutableString alloc] initWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for(NSInteger count = 0; count < CC_MD5_DIGEST_LENGTH; count++){
        [outputString appendFormat:@"%02x",outputBuffer[count]];
    }
    
    DLog(@"%@", outputString);
    
    return outputString;
}

#pragma mark - 价格的精度处理
+(NSString *)getPriceStrWithValue:(double)conversionValue{
    NSString *doubleString = [NSString stringWithFormat:@"%lf", conversionValue];
    NSDecimalNumber *decNumber = [NSDecimalNumber decimalNumberWithString:doubleString];
    return [decNumber stringValue];
}
+(NSString *)setPriceTopStrWithValue:(double)value{
    NSString *doubleString = [NSString stringWithFormat:@"%lf", value];
    NSString * orderString = @"";
    if ([doubleString containsString:@"."]) {
        NSArray * arr  = [doubleString componentsSeparatedByString:@"."];
        NSString * topNumber = arr[0];
        NSString * number = arr[1];
        if (number.length > 2) {
             NSString *numberThree = [number substringWithRange:NSMakeRange(2,1)];
            if (![numberThree isEqualToString:@"0"]) {
                NSString *str2 = [number substringWithRange:NSMakeRange(0,2)];//str2 = "name"
                doubleString =  [NSString stringWithFormat:@"%@.%@",topNumber,str2];
                orderString = [NSString stringWithFormat:@"%.2f",[doubleString doubleValue] + 0.01];
            }else{
                orderString = [NSString stringWithFormat:@"%.2f",value];
            }
        }else{
            orderString = [NSString stringWithFormat:@"%.2f",value];
        }
    }
    return orderString;
}
#pragma mark - 判断字符串中是否包含表情
- (BOOL)isIncludingEmoji {
    BOOL __block result = NO;
    
    [self enumerateSubstringsInRange:NSMakeRange(0, [self length])
                             options:NSStringEnumerationByComposedCharacterSequences
                          usingBlock: ^(NSString* substring, NSRange substringRange, NSRange enclosingRange, BOOL* stop) {
                              if ([substring isEmoji]) {
                                  *stop = YES;
                                  result = YES;
                              }
                          }];
    
    return result;
}
#pragma amrk - 移除字符串中的表情
- (instancetype)removedEmojiString {
    NSMutableString* __block buffer = [NSMutableString stringWithCapacity:[self length]];
    
    [self enumerateSubstringsInRange:NSMakeRange(0, [self length])
                             options:NSStringEnumerationByComposedCharacterSequences
                          usingBlock: ^(NSString* substring, NSRange substringRange, NSRange enclosingRange, BOOL* stop) {
                              [buffer appendString:([substring isEmoji])? @"": substring];
                          }];
    
    return buffer;
}

- (BOOL)isEmoji {
    const unichar high = [self characterAtIndex: 0];
    if (0xd800 <= high && high <= 0xdbff) {
        const unichar low = [self characterAtIndex: 1];
        const int codepoint = ((high - 0xd800) * 0x400) + (low - 0xdc00) + 0x10000;
        
        return (0x1d000 <= codepoint && codepoint <= 0x1f77f);
    } else {
        return (0x2100 <= high && high <= 0x27bf);
    }
}
#pragma mark - 表情编码
+ (NSString *)EmojiCodingToStringWithEmojiStr:(NSString *)emojiStr{
    
    NSString *uniStr = [NSString stringWithUTF8String:[emojiStr UTF8String]];
//    NSData *uniData = [uniStr dataUsingEncoding:NSNonLossyASCIIStringEncoding];
//    NSString *goodStr = [[NSString alloc] initWithData:uniData encoding:NSUTF8StringEncoding];

    NSUInteger length = [uniStr length];
    NSMutableString *s = [NSMutableString stringWithCapacity:10];
    
    for (int i = 0;i < length; i++){
        unichar _char = [uniStr characterAtIndex:i];
        //判断是否为英文和数字
        if (_char <= '9' && _char >='0'){
            [s appendFormat:@"\\u00%x",[uniStr characterAtIndex:i]];
        }else if(_char >='a' && _char <= 'z'){
            [s appendFormat:@"\\u%x",[uniStr characterAtIndex:i]];
        }
        else if(_char >='A' && _char <= 'Z'){
            [s appendFormat:@"\\u%x",[uniStr characterAtIndex:i]];
        }else{
            [s appendFormat:@"\\u%x",[uniStr characterAtIndex:i]];
        }
    }
    return s;
}
+(NSURL *)hasChinese:(NSString *)str {
    for(int i=0; i< [str length];i++){
        int a = [str characterAtIndex:i];
        if( a > 0x4e00 && a < 0x9fff){
            return [NSURL URLWithString:[str stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
        }
    }
    return [NSURL URLWithString:str];
}
#pragma mark - 表情解码
+ (NSString *)enCodeToEmojiStrWithStr:(NSString *)str{
    const char *jsonString = [str UTF8String];
    NSData *jsonData = [NSData dataWithBytes:jsonString length:strlen(jsonString)];
    NSString *emojiMsg1 = [[NSString alloc] initWithData:jsonData encoding:NSNonLossyASCIIStringEncoding];
    return emojiMsg1;
}

@end
