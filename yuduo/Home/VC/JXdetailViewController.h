//
//  JXdetailViewController.h
//  yuduo
//
//  Created by mason on 2019/8/15.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface JXdetailViewController : UIViewController
PropertyStrong(NSString, articleStringID);
PropertyStrong(NSString, articleTitle);
@end

NS_ASSUME_NONNULL_END
