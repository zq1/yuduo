//
//  myZXViewController.m
//  yuduo
//
//  Created by Mac on 2019/8/13.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "myZXViewController.h"
#import "MyZXDetailTableViewCell.h"
#import "CommitViewController.h"
#import "ZXDetailViewController.h"
#import "MysetMyZXModel.h"
@interface myZXViewController ()<UITableViewDelegate,UITableViewDataSource>
PropertyStrong(UITableView, myZXTableView);
PropertyStrong(NSMutableArray, zxArray);
@end

@implementation myZXViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.myZXTableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.myZXTableView.delegate = self;
    self.myZXTableView.dataSource = self;
    self.myZXTableView.backgroundColor = RGB(244, 243, 244);
    self.myZXTableView.tableFooterView = [UIView new];
    [self.view addSubview:self.myZXTableView];
    self.myZXTableView.sd_layout.topSpaceToView(self.view, 0)
    .leftSpaceToView(self.view, 0)
    .widthIs(KScreenW).heightIs(KScreenH-90-MStatusBarHeight);
    __weak typeof(self)  weakSelf = self;
    self.myZXTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf getZXList];
    }];
    [self.myZXTableView.mj_header beginRefreshing];
    [self zxbutton];
    // Do any additional setup after loading the view.
}


- (void)zxbutton {
    UIButton *zxButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [zxButton setBackgroundImage:[UIImage imageNamed:@"zx-icon"] forState:UIControlStateNormal];
    [zxButton addTarget:self action:@selector(zxClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:zxButton];
    zxButton.sd_layout.bottomSpaceToView(self.view, 179*kGMHeightScale)
    .rightSpaceToView(self.view, 20)
    .widthIs(60).heightIs(60);
}

- (void)zxClick:(UIButton *)send {
    NSLog(@"咨询");
    CommitViewController *commitVC = [[CommitViewController alloc]init];
    commitVC.view.backgroundColor = RGB(244, 243, 244);
    
    [self.navigationController pushViewController:commitVC animated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.zxArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"cellID";
    MyZXDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[MyZXDetailTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    MysetMyZXModel *zxMol = [self.zxArray objectAtIndex:indexPath.row];
    cell.titleLabel.text = zxMol.manag_title;
    cell.timeLabel.text = zxMol.manag_time;
    [cell.headImg sd_setImageWithURL:[NSURL URLWithString:zxMol.portrait]];
    cell.backgroundColor = RGB(244, 243, 244);
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    ZXDetailViewController *zxDetailVC = [[ZXDetailViewController alloc]init];
    MysetMyZXModel *model = [self.zxArray objectAtIndex:indexPath.row];
    zxDetailVC.zxIDString = model.manag_id;
    zxDetailVC.zxtitleString = model.manag_title;
    zxDetailVC.zxtimeString = model.manag_time;
     zxDetailVC.likeNumber = model.thumbsup;
    [self.navigationController pushViewController:zxDetailVC animated:YES];
}

- (void)getZXList {
    
    self.zxArray = [NSMutableArray array];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
     NSString *user_id = [defaults objectForKey:@"user_id"];
    if (!user_id) {
        NSLog(@"用户未登录");
        [self.myZXTableView.mj_header endRefreshing];
       // [SVProgressHUD showErrorWithStatus:@"用户未登录"];
    }else{
        NSMutableDictionary *zxDic = [NSMutableDictionary dictionary];
        zxDic[@"user_id"] = [defaults objectForKey:@"user_id"];
        zxDic[@"token"] = [defaults objectForKey:@"token"];
        __weak typeof(self)  weakSelf = self;
        [GMAfnTools PostHttpDataWithUrlStr:KMySetyMyZX Dic:zxDic SuccessBlock:^(id  _Nonnull responseObject) {
            NSLog(@"我的咨询列表获取成功===%@",responseObject);
            [weakSelf.myZXTableView.mj_header endRefreshing];
            for (NSMutableDictionary *zxDic in responseObject) {
                MysetMyZXModel *mol = [[MysetMyZXModel alloc]init];
                [mol mj_setKeyValues:zxDic];
                [weakSelf.zxArray addObject:mol];
            }
            [self.myZXTableView reloadData];
        } FailureBlock:^(id  _Nonnull error) {
            NSLog(@"我的咨询列表===%@",error);
            [weakSelf.myZXTableView.mj_header endRefreshing];
        }];
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
