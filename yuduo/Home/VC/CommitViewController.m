//
//  CommitViewController.m
//  yuduo
//
//  Created by Mac on 2019/8/13.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "CommitViewController.h"
#import "AliUpLoadImageTool.h"
#import "ZXShuoMingViewController.h"
#import "PersonInfoModel.h"
@interface CommitViewController ()<UITextViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
PropertyStrong(UIImagePickerController, picker);
PropertyStrong(UIImageView , twoImg);
PropertyStrong(UIImageView , threeImg);
PropertyStrong(UILabel , messageLabel);
PropertyStrong(NSString , ltatiotNumber);
@end

@implementation CommitViewController
-(void)viewTapped:(UITapGestureRecognizer*)tapGr
{
    [self.titleText resignFirstResponder];
    [self.detailText resignFirstResponder];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    UITapGestureRecognizer *tapGr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped:)];
    tapGr.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapGr];
    self.view.backgroundColor = GMWhiteColor;
    
    self.picker.delegate = self;
    self.picker.allowsEditing = YES;
    
    [self changeNavigation];
    [self zxLayout];
    [self getInfo];
    // Do any additional setup after loading the view.
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    //    获取图片
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    self.twoImg.image = image;
   
    //    获取图片后返回
    [picker dismissViewControllerAnimated:YES completion:nil];
}

//按取消按钮时候的功能
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    //    返回
    [picker dismissViewControllerAnimated:YES completion:nil];
}
- (UIImagePickerController *)picker
{
    if (!_picker) {
        _picker = [[UIImagePickerController alloc]init];
    }
    return _picker;
}

- (void)getInfo {
    NSMutableDictionary *infoDic = [NSMutableDictionary dictionary];
    NSUserDefaults *detafaults = [NSUserDefaults standardUserDefaults];
    NSString  *user_id =  [detafaults objectForKey:@"user_id"];
    NSString *token =  [detafaults objectForKey:@"token"];
    infoDic[@"user_id"] = user_id;
    infoDic[@"token"] = token;
    __weak typeof(self) weakSelf = self;
    [GMAfnTools PostHttpDataWithUrlStr:KInfoShow Dic:infoDic SuccessBlock:^(id  _Nonnull responseObject) {
        PersonInfoModel *personModel = [[PersonInfoModel alloc]init];
        [personModel mj_setKeyValues:responseObject];
        weakSelf.ltatiotNumber = personModel.ltation_number;
        weakSelf.messageLabel.text = [[@"提示:VIP咨询剩余免费次数" stringByAppendingString:personModel.ltation_number]stringByAppendingString:@"次"];
    } FailureBlock:^(id  _Nonnull error) {
        NSLog(@"获取个人信息失败 == %@",error);
    }];

}


- (void)zxLayout {
    
    self.backgroundView = [[UIScrollView alloc]init];
    self.backgroundView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    self.backgroundView.contentSize = CGSizeMake(KScreenW, KScreenH-MStatusBarHeight-53);
    self.backgroundView.delegate = self;
    self.backgroundView.showsVerticalScrollIndicator = NO;
//    self.backgroundView.contentOffset = CGPointMake(0, KScreenH-MStatusBarHeight-54);
//    self.backgroundView.backgroundColor = GMlightGrayColor;
    [self.view addSubview:self.backgroundView];
    self.backgroundView.sd_layout.topSpaceToView(self.view, MStatusBarHeight+54)
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .widthIs(KScreenW).heightIs(KScreenH-MStatusBarHeight-54);
    
    self.titleLabel = [[UILabel alloc]init];
    [self.backgroundView addSubview:self.titleLabel];
    self.titleLabel.sd_layout.topSpaceToView(self.backgroundView, 15)
    .leftSpaceToView(self.backgroundView, 17)
    .widthIs(66).heightIs(15);
    NSMutableAttributedString *titleLabelString = [[NSMutableAttributedString alloc] initWithString:@"咨询标题:" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 15],NSForegroundColorAttributeName: [UIColor colorWithRed:69/255.0 green:69/255.0 blue:69/255.0 alpha:1.0]}];
    self.titleLabel.attributedText = titleLabelString;
    
    self.titleText = [[UITextView alloc]init];
    self.titleText.text = @"请输入标题内容";
    self.titleText.delegate = self;
    self.titleText.textColor = RGB(153, 153, 153);
    [self.backgroundView addSubview:self.titleText];
    self.titleText.sd_layout.topSpaceToView(self.titleLabel, 16)
    .leftSpaceToView(self.backgroundView, 15)
    .rightSpaceToView(self.backgroundView, 15)
    .widthIs(KScreenW-30).heightIs(100*kGMHeightScale);
    
    self.detailLable = [[UILabel alloc]init];
    [self.backgroundView addSubview:self.detailLable];
    self.detailLable.sd_layout.topSpaceToView(self.titleText, 15)
    .leftSpaceToView(self.backgroundView, 17)
    .widthIs(66).heightIs(15);
    NSMutableAttributedString *detailLabelString = [[NSMutableAttributedString alloc] initWithString:@"咨询内容:" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 15],NSForegroundColorAttributeName: [UIColor colorWithRed:69/255.0 green:69/255.0 blue:69/255.0 alpha:1.0]}];
    self.detailLable.attributedText = detailLabelString;
    
    self.detailText = [[UITextView alloc]init];
    self.detailText.delegate = self;
    self.detailText.text = @"请输入咨询内容";
    self.detailText.textColor = RGB(153, 153, 153);
    [self.backgroundView addSubview:self.detailText];
    self.detailText.sd_layout.topSpaceToView(self.detailLable, 15)
    .leftSpaceToView(self.backgroundView, 15)
    .rightSpaceToView(self.backgroundView, 15)
    .widthIs(KScreenW-30).heightIs(100*kGMHeightScale);
    
    self.imgLabel = [[UILabel alloc]init];
    self.imgLabel.textColor = RGB(69, 69, 69);
    self.imgLabel.font = [UIFont fontWithName:KPFType size:15];
    self.imgLabel.text = @"图片上传";
    [self.backgroundView addSubview:self.imgLabel];
    self.imgLabel.sd_layout.topSpaceToView(self.detailText, 15)
    .leftSpaceToView(self.backgroundView, 17)
    .widthIs(66).heightIs(15);
    
    UIButton *updatePicBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [updatePicBtn setBackgroundImage:[UIImage imageNamed:@"addpic"] forState:UIControlStateNormal];
    [updatePicBtn addTarget:self action:@selector(updatePicClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.backgroundView addSubview:updatePicBtn];
    updatePicBtn.sd_layout.topSpaceToView(self.imgLabel, 17)
    .leftSpaceToView(self.backgroundView, 15)
    .widthIs(80).heightIs(80);
    
    self.twoImg = [[UIImageView alloc]init];
    self.twoImg.backgroundColor = GMlightGrayColor;
    [self.backgroundView addSubview:self.twoImg];
    self.twoImg.sd_layout.topEqualToView(updatePicBtn)
    .leftSpaceToView(updatePicBtn, 20)
    .widthIs(80).heightIs(80);
    self.twoImg.userInteractionEnabled = YES;
    UITapGestureRecognizer *ges = [[UITapGestureRecognizer alloc] initWithTarget:self action: @selector(twoImgClick)];
    [self.twoImg addGestureRecognizer:ges];
//    self.threeImg = [[UIImageView alloc]init];
//    self.threeImg.backgroundColor = GMlightGrayColor;
//    [self.backgroundView addSubview:self.threeImg];
//    self.threeImg.sd_layout.topEqualToView(updatePicBtn)
//    .leftSpaceToView(self.twoImg, 20)
//    .widthIs(80).heightIs(80);
    //普通咨询
    UIButton *customQuestion  = [UIButton buttonWithType:UIButtonTypeCustom];
    customQuestion.backgroundColor = RGB(21, 151, 164);
    customQuestion.layer.cornerRadius = 22.5;
    [customQuestion addTarget:self action:@selector(customClick:) forControlEvents:UIControlEventTouchUpInside];
    [customQuestion setTitleColor:GMWhiteColor forState:UIControlStateNormal];
    [customQuestion setTitle:@"普通咨询" forState:UIControlStateNormal];
    [self.backgroundView addSubview:customQuestion];
    customQuestion.sd_layout.topSpaceToView(updatePicBtn,31 )
    .leftSpaceToView(self.backgroundView, 15)
    .rightSpaceToView(self.backgroundView,15)
    .heightIs(45);
    //Vip咨询
    UIButton *vipQuestion  = [UIButton buttonWithType:UIButtonTypeCustom];
    vipQuestion.backgroundColor = RGB(252, 171, 71);
    vipQuestion.layer.cornerRadius = 22.5;
    [vipQuestion addTarget:self action:@selector(VipClick:) forControlEvents:UIControlEventTouchUpInside];
    [vipQuestion setTitleColor:GMWhiteColor forState:UIControlStateNormal];
    [vipQuestion setTitle:@"VIP咨询" forState:UIControlStateNormal];
    [self.backgroundView addSubview:vipQuestion];
    vipQuestion.sd_layout.topSpaceToView(customQuestion,15)
    .leftSpaceToView(self.backgroundView, 15)
    .rightSpaceToView(self.backgroundView,15)
    .heightIs(45);
    
    self.messageLabel = [[UILabel alloc]init];
    self.messageLabel.textColor = RGB(178, 178, 178);
    self.messageLabel.font = [UIFont fontWithName:KPFType size:14];
    [self.backgroundView addSubview:self.messageLabel];
    self.messageLabel.sd_layout.topSpaceToView(vipQuestion, 12)
    .centerXEqualToView(self.backgroundView)
    .heightIs(14).widthIs(210);
}

- (void)twoImgClick {
    if (self.twoImg.image != nil) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提升" message:@"是否删除当前选择的图片" preferredStyle:(UIAlertControllerStyleAlert)];
        [alert addAction:[UIAlertAction actionWithTitle:@"删除" style:(UIAlertActionStyleDestructive) handler:^(UIAlertAction * _Nonnull action) {
            self.twoImg.image = nil;
        }]];
        [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

#pragma mark - 调用系统相册
- (void)updatePicClick:(UIButton *)sender {
    NSLog(@"调用系统相册");
    UIAlertController *alertVc = [UIAlertController alertControllerWithTitle:@"请选择" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    //默认只有标题 没有操作的按钮:添加操作的按钮 UIAlertAction
    
    UIAlertAction *cancelBtn = [UIAlertAction actionWithTitle:@"相机" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSLog(@"取消");
        [self selectImg:10000];
    }];
    //添加确定
    UIAlertAction *sureBtn = [UIAlertAction actionWithTitle:@"相册" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull   action) {
        NSLog(@"确定");
        [self selectImg:10001];
    }];
    //设置`确定`按钮的颜色
//    [sureBtn setValue:[UIColor redColor] forKey:@"titleTextColor"];
    //将action添加到控制器
    [alertVc addAction:cancelBtn];
    [alertVc addAction :sureBtn];
    
    [alertVc addAction:[UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:nil]];
    //展示
    [self presentViewController:alertVc animated:YES completion:nil];

}

- (void)selectImg:(NSInteger)sender {
    
        BOOL isPicker = NO;
    
        switch (sender) {
            case 10000:
                //            打开相机
                isPicker = true;
                //            判断相机是否可用
                if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                    self.picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                    isPicker = true;
                }
                break;
    
            case 10001:
                //            打开相册
                self.picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                isPicker = true;
                break;
    
            default:
                self.picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                isPicker = true;
                break;
        }
    
        if (isPicker) {
            [self presentViewController:self.picker animated:YES completion:nil];
        }else {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"错误" message:@"相机不可用" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:nil];
            [alert addAction:action];
    
            [self presentViewController:alert animated:YES completion:nil];
        }
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
         [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
}
#pragma mark - 普通咨询
- (void)customClick:(UIButton *)send {
    NSLog(@"普通咨询");
    NSLog(@"xxxxxxx%@",self.twoImg);
    
    if ([self.titleText.text isEqualToString:@"请输入标题内容"] || [self.detailText.text isEqualToString:@"请输入咨询内容"]) {
        [self showError:@"咨询标题或内容不能为空"];
    } else if (self.twoImg.image == nil) {
        [self sendUpZX:@"" type:@"1"];
    } else {
        
        [AliUpLoadImageTool upLoadImage:self.twoImg.image success:^(NSString * _Nonnull url) {
            NSLog(@"xxxxxxxx%@",url);
            NSLog(@"%@",[NSThread currentThread]);
            dispatch_async(dispatch_get_main_queue(), ^{
                [self sendUpZX:url type:@"1"];
            });
            
        } failure:^(NSString * _Nonnull errorString) {
            NSLog(@"222222%@",errorString);
        }];
    }
//    if (resultImageArr.count) {
//        // MARK: - 上传图片至阿里云
//        NSMutableArray *dataItems = [NSMutableArray new];
//        for (UIImage *faceImage in resultImageArr) {
//            [dataItems addObject:UIImageJPEGRepresentation(faceImage, 0.3)];
//        }
//        __block NSMutableArray *_imageUrls = [NSMutableArray new];
//        dispatch_queue_t queue = dispatch_queue_create("com.Chan.uploadImageToAliyunServer.www", DISPATCH_QUEUE_CONCURRENT);
//        dispatch_async(queue, ^{
//            [[AliUpLoadImageTool shareIncetance] upLoadImageWithPamgamar:nil imageDataArray:dataItems success:^(NSArray *objectKeys) {
//                //这里拿到的objectKeys就是图片在阿里云服务器上保存的位置地址URL，名字是用时间戳随机生成的（此处随自己意愿）
//                if (objectKeys && objectKeys.count) {
//                    [_imageUrls addObject:objectKeys];
//                    NSLog(@"xxxxxxx打印图片地址==%@",objectKeys);
//                }
//            } faile:^(NSError *error) {
//
//            }];
//        });
//        dispatch_barrier_async(queue, ^{
//            NSLog(@"this is a barrier!");
//        });
//    }
}

- (void)sendUpZX:(NSString *)url type: (NSString *)type {
    NSLog(@"%@",[NSThread currentThread]);
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *zxDic = [NSMutableDictionary dictionary];
    zxDic[@"user_id"] = [defaults objectForKey:@"user_id"];
    zxDic[@"token"] = [defaults objectForKey:@"token"];
    zxDic[@"manag_type"] = type;
    zxDic[@"manag_title"] = self.titleText.text;
    zxDic[@"manag_content"] = self.detailText.text;
    zxDic[@"manag_picture"] = url;
    
    [GMAfnTools PostHttpDataWithUrlStr:KUpLoadZX Dic:zxDic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"上传成功 success==%@",responseObject);
        if ([defaults objectForKey:@"token"]) {
            if ([[responseObject objectForKey:@"code"] isEqualToString:@"1"]) {
                [self showError:[responseObject objectForKey:@"msg"]];
            }else {
                
                [self showError:[responseObject objectForKey:@"msg"]];
            }
        }else {
            [self showError:@"请先登录"];
        }
    } FailureBlock:^(id  _Nonnull error) {
        NSLog(@"上传失败 error == %@",error);
    }];
}
#pragma mark -- 提示框
- (void)showError:(NSString *)errorMsg {
    // 1.弹框提醒
    // 初始化对话框
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:errorMsg preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:nil]];
    // 弹出对话框
    [self presentViewController:alert animated:true completion:nil];
    
    [self performSelector:@selector(dimisssAlert:) withObject:alert afterDelay:1];
    
}

- (void) dimisssAlert:(UIAlertController *)alert
{
    if (alert) {
        [alert dismissViewControllerAnimated:YES completion:nil];
    }
    
}
#pragma mark - VIP咨询
- (void)VipClick:(UIButton *)send {
    NSLog(@"VIP咨询");
    if ([self.ltatiotNumber isEqualToString:@"0"] || [self.ltatiotNumber isEqualToString:@""]) {
        [self showError:@"您当前没有VIP咨询免费次数"];
    } else if ([self.titleText.text isEqualToString:@"请输入标题内容"] || [self.detailText.text isEqualToString:@"请输入咨询内容"]) {
        [self showError:@"咨询标题或内容不能为空"];
    } else if (self.twoImg.image == nil) {
        [self sendUpZX:@"" type:@"2"];
    } else {
        [AliUpLoadImageTool upLoadImage:self.twoImg.image success:^(NSString * _Nonnull url) {
            NSLog(@"xxxxxxxx%@",url);
            NSLog(@"%@",[NSThread currentThread]);
            dispatch_async(dispatch_get_main_queue(), ^{
                [self sendUpZX:url type:@"2"];
            });
        } failure:^(NSString * _Nonnull errorString) {
            NSLog(@"222222%@",errorString);
        }];
    }
}
#pragma mark - UITextViewDelegate

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if(textView.text.length < 1){
        textView.text = @"请输入标题内容";
        textView.text = @"请输入咨询内容";
        textView.textColor = [UIColor grayColor];
    }
}
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if([textView.text isEqualToString:@"请输入标题内容"] || [textView.text isEqualToString:@"请输入咨询内容"]){
        textView.text=@"";
        textView.textColor=[UIColor blackColor];
    }
}


- (void)changeNavigation {
    
    self.navigationController.navigationBar.topItem.title = @"咨询";
    UIView *titleView = [[UIView alloc]init];
    titleView.backgroundColor = GMWhiteColor;
    [self.view addSubview:titleView];
    titleView.sd_layout.topSpaceToView(self.view,0)
    .leftSpaceToView(self.view, 0)
    .rightSpaceToView(self.view, 0)
    .widthIs(KScreenW).heightIs(MStatusBarHeight+44);
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setBackgroundImage:[UIImage imageNamed:@"back-icon"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
    [titleView addSubview:backButton];
    backButton.sd_layout.bottomSpaceToView(titleView,13)
    .leftSpaceToView(titleView, 15)
    .widthIs(11).heightIs(19);
    UIButton *backButtonView = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 80, MStatusBarHeight+44)];
    backButtonView.backgroundColor = RGBA(15, 15, 15, 0);
    [backButtonView addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButtonView];
    UILabel *titleLabel = [[UILabel alloc]init];
    [titleView addSubview:titleLabel];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"咨询" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 18],NSForegroundColorAttributeName: [UIColor colorWithRed:69/255.0 green:69/255.0 blue:69/255.0 alpha:1.0]}];
    titleLabel.attributedText = string;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.sd_layout.bottomSpaceToView(titleView, 15)
    .centerXEqualToView(titleView)
    .heightIs(18).widthIs(36);
    
    UIButton *zxLable = [UIButton buttonWithType:UIButtonTypeCustom];
    [zxLable setTitleColor:RGB(21, 151, 164) forState:UIControlStateNormal];
    [zxLable.titleLabel setFont:[UIFont fontWithName:@"PingFangSC-Regular" size: 16]];
    [zxLable setTitle:@"咨询说明" forState:UIControlStateNormal];
    [zxLable addTarget:self action:@selector(zxClick:) forControlEvents:UIControlEventTouchUpInside];
    [titleView addSubview:zxLable];
    zxLable.sd_layout.bottomSpaceToView(titleView, 14)
    .rightSpaceToView(titleView, 17)
    .widthIs(70).heightIs(16);
    
    //    [self cateTableViewCreate];
}
- (void)zxClick:(UIButton *)send {
    NSLog(@"xxxx咨询说明");
    ZXShuoMingViewController *zx = [[ZXShuoMingViewController alloc]init];
    [self.navigationController pushViewController:zx animated:YES];
}
- (void)backBtn {
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
