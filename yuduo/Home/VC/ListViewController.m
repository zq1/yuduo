//
//  ListViewController.m
//  yuduo
//
//  Created by Mac on 2019/8/12.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "ListViewController.h"
//活动
#import "ActiveCenterTableViewCell.h"
#import "ActiveAddressTableViewCell.h"
#import "HomeActiveModel.h"
#import "ActiveDetailViewController.h"
#import "ActiveCenterOnlineTableViewCell.h"
#import "ActiveCenterDefultTableViewCell.h"
@interface ListViewController ()<UITableViewDelegate,UITableViewDataSource>
PropertyStrong(NSMutableArray, activeArray);

@end

@implementation ListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
        self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, KScreenW, KScreenH-48-MStatusBarHeight-44) style:UITableViewStylePlain];
    self.tableView.separatorStyle =UITableViewCellSeparatorStyleNone;
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
    __weak typeof(self)  weakSelf = self;
    
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
           
        [weakSelf.tableView.mj_header endRefreshing];
    }];
    
    
//        [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
        [self.view addSubview:self.tableView];
}
#pragma mark--设置刷新显示文字

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    HomeActiveModel *activeModel = [self.activeArray objectAtIndex:indexPath.row];
       if ([activeModel.activity_type integerValue] == 1) {
           return 270;
       }else{
           return  290;
       }
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.activeArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    HomeActiveModel *activeModel = [self.activeArray objectAtIndex:indexPath.row];
    if ([activeModel.activity_type integerValue] == 1) {
        static NSString *cellID = @"cellOnlineID";
            ActiveCenterOnlineTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        //    ActiveAddressTableViewCell *cell1 = [tableView dequeueReusableCellWithIdentifier:cellID1];
            if (!cell) {
                cell = [[ActiveCenterOnlineTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
            }
        cell.model = activeModel;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }else{
        static NSString *cellID = @"cellDefultID";
            ActiveCenterDefultTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
            if (!cell) {
                cell = [[ActiveCenterDefultTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
            }
        cell.model = activeModel;
         cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    return [[UITableViewCell alloc] init];
}
-(void)setIndex:(NSInteger)index{
    _index = index;
    [self getDataActiveForId:index];
}
-(void)setIsAll:(BOOL)isAll{
    if (isAll) {
        [self getAllInfo];
    }
}
-(void)getAllInfo{
     NSMutableDictionary *infoDic = [NSMutableDictionary dictionary];
    //infoDic[@"category_id"] = @"3";
   // WS(weakSelf);
    self.activeArray = [NSMutableArray array];
    [GMAfnTools PostHttpDataWithUrlStr:KDiscoveryAllList Dic:infoDic SuccessBlock:^(id  _Nonnull responseObject) {
        for (NSMutableDictionary *dic in responseObject) {
            HomeActiveModel *mol = [[HomeActiveModel alloc]init];
            [mol mj_setKeyValues:dic];
            [self.activeArray addObject:mol];
        }
        [self.tableView reloadData];
        [self.tableView.mj_header endRefreshing];
    } FailureBlock:^(id  _Nonnull error) {
        
    }];
}
#pragma mark 获取数据
- (void)getDataActiveForId:(NSInteger)Id {
    self.activeArray = [NSMutableArray array];
    NSMutableDictionary *ActiveListDic = [NSMutableDictionary dictionary];
    ActiveListDic[@"category_id"] = [NSString stringWithFormat:@"%ld",Id];//活动分类
    ActiveListDic[@"activity_start_date"] = @"1";//开始时间
    ActiveListDic[@"activity_end_date"] = @"1"; //结束时间
    
    [GMAfnTools PostHttpDataWithUrlStr:KHomeActive Dic:ActiveListDic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"获取课程列表成功 == %@",responseObject);
        for (NSMutableDictionary *dic in responseObject) {
            HomeActiveModel *mol = [[HomeActiveModel alloc]init];
            [mol mj_setKeyValues:dic];
            [self.activeArray addObject:mol];
        }
        [self.tableView reloadData];
        [self.tableView.mj_header endRefreshing];
    } FailureBlock:^(id  _Nonnull error) {
    }];
    
    [self.tableView reloadData];
}
#pragma mark 活动详情
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    if (self.index == 0) {
        NSLog(@"xxxxxxxx跳转活动详情");
        
        ActiveDetailViewController *activcVC = [[ActiveDetailViewController alloc]init];
        HomeActiveModel *mol = [self.activeArray objectAtIndex:indexPath.row];
        activcVC.category_id = mol.activity_id;
        
    [[NSNotificationCenter  defaultCenter] postNotificationName:@"activePush" object:@{@"id":mol.activity_id,@"type":mol.activity_type}];
  

    
//    }
    
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
