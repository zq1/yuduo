//
//  HotZXViewController.m
//  yuduo
//
//  Created by Mac on 2019/8/13.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "HotZXViewController.h"
#import "HotZXDetailTableViewCell.h"
#import "CommitViewController.h"
#import "ZXDetailViewController.h"
#import "HotZXModel.h"
@interface HotZXViewController ()<UITableViewDelegate,UITableViewDataSource>
PropertyStrong(UITableView, myZXTableView);
PropertyStrong(NSMutableArray, zxListArray);
@end

@implementation HotZXViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.myZXTableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.myZXTableView.delegate = self;
    self.myZXTableView.dataSource = self;
    [self.myZXTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.myZXTableView.backgroundColor = RGB(244, 243, 244);
    [self.view addSubview:self.myZXTableView];
    self.myZXTableView.sd_layout.topSpaceToView(self.view, 0)
    .leftSpaceToView(self.view, 0)
    .widthIs(KScreenW).heightIs(KScreenH-90-MStatusBarHeight);
    __weak typeof(self)  weakSelf = self;
    self.myZXTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf getHotZX];
    }];
    [self.myZXTableView.mj_header beginRefreshing];
    [self zxbutton];
    // Do any additional setup after loading the view.
}
- (void)getHotZX {
    self.zxListArray = [NSMutableArray array];
    __weak typeof(self)  weakSelf = self;
    [GMAfnTools PostHttpDataWithUrlStr:KhomeHotZXMore Dic:nil SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"xxxxxxxx热门咨询列表--%@",responseObject);
        [weakSelf.myZXTableView.mj_header endRefreshing];
        for (NSMutableDictionary *dic in responseObject) {
        HotZXModel *hotModel = [[HotZXModel alloc]init];
            
            [hotModel mj_setKeyValues:dic];
            [weakSelf.zxListArray addObject:hotModel];
        }
        [weakSelf.myZXTableView reloadData];
    } FailureBlock:^(id  _Nonnull error) {
        [weakSelf.myZXTableView.mj_header endRefreshing];
    }];
}

- (void)zxbutton {
    UIButton *zxButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [zxButton setBackgroundImage:[UIImage imageNamed:@"zx-icon"] forState:UIControlStateNormal];
    [zxButton addTarget:self action:@selector(zxClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:zxButton];
    zxButton.sd_layout.bottomSpaceToView(self.view, 179*kGMHeightScale)
    .rightSpaceToView(self.view, 20)
    .widthIs(60).heightIs(60);
}

- (void)zxClick:(UIButton *)send {
    NSLog(@"咨询");
    CommitViewController *commitVC = [[CommitViewController alloc]init];
    commitVC.view.backgroundColor = RGB(244, 243, 244);
    
    [self.navigationController pushViewController:commitVC animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.zxListArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"cellID";
    HotZXDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[HotZXDetailTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.backgroundColor = RGB(244, 243, 244);
    HotZXModel *mol = [self.zxListArray objectAtIndex:indexPath.row];
    cell.timeLabel.text = [mol.manag_time substringToIndex:10];
    cell.titleLabel.text = mol.manag_title;
    cell.peopleLabel.text = mol.thumbsup;
    cell.dianZanLabel.text = mol.browse;
    [cell.headImg sd_setImageWithURL:[NSURL URLWithString:mol.manag_picture]];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    HotZXModel *hotModel = [self.zxListArray objectAtIndex:indexPath.row];
    ZXDetailViewController *zxDetailVC = [[ZXDetailViewController alloc]init];
    zxDetailVC.zxIDString = hotModel.manag_id;
    zxDetailVC.zxtimeString = hotModel.manag_time;
    zxDetailVC.zxtitleString = hotModel.manag_title;
    zxDetailVC.likeNumber = hotModel.thumbsup;
    [self.navigationController pushViewController:zxDetailVC animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
