//
//  HomeZXViewController.h
//  yuduo
//
//  Created by Mac on 2019/8/12.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeZXViewController : UIViewController
PropertyStrong(UITableView, sortTableView);

@property(nonatomic,strong)UIScrollView*Scrview1; //滚动条
@property(nonatomic,strong)UIScrollView*Scrview2;
@property(nonatomic,strong)UITableView*HotTableView;
@end

NS_ASSUME_NONNULL_END
