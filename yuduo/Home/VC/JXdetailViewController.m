//
//  JXdetailViewController.m
//  yuduo
//
//  Created by mason on 2019/8/15.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "JXdetailViewController.h"
#import "PJTableViewCell.h"
#import "KCPJModel.h"
@interface JXdetailViewController ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
PropertyStrong(UIButton, collectButton);
PropertyStrong(UIButton, shareButton);
PropertyStrong(NSMutableArray, cancelCollectString);
PropertyStrong(UIImageView, detailImg);
PropertyStrong(UILabel, commentLabel);

PropertyStrong(UIView, commentView);
PropertyStrong(UIButton, sendButton);
PropertyStrong(UITextField, commentText);

PropertyStrong(UITableView, myZXTableView);
//评论
PropertyStrong(NSMutableArray, plArray);
PropertyStrong(NSMutableArray, listArray);
@end

@implementation JXdetailViewController
- (void)getArticleList {
    self.listArray = [NSMutableArray array];
    NSUserDefaults *defaus = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    if ([defaus objectForKey:@"user_id"] == nil) {
        return;
    }
    dic[@"user_id"] = [defaus objectForKey:@"user_id"];
    dic[@"token"] = [defaus objectForKey:@"token"];
    dic[@"type"] = @"3";
    [GMAfnTools PostHttpDataWithUrlStr:KCoursePJList Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"打印文章评价列表----%@",responseObject);
        for (NSMutableDictionary *userDic in responseObject) {
            for (NSMutableDictionary *detailDic in [userDic objectForKey:@"user_comment_lists"]) {
                
                KCPJModel *mol = [[KCPJModel alloc]init];
                [mol mj_setKeyValues:detailDic];
                [self.listArray addObject:mol];
                NSLog(@"打印评论列表 Array---%@",self.listArray);
            }
        }
        [self.myZXTableView reloadData];
    } FailureBlock:^(id  _Nonnull error) {
        
    }];
}
- (void)viewDidLoad {
    [super viewDidLoad];
   
    [self getArticleList];
    self.view.backgroundColor = GMWhiteColor;
   
    [self setNavigation];

}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}
- (void)btnClick {
    [self getData];
}

- (void)getData {
    NSUserDefaults *defauls = [NSUserDefaults standardUserDefaults];
    NSString *user_id = [defauls objectForKey:@"user_id"];
    NSString *token = [defauls objectForKey:@"token"];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"user_id"] = user_id;
    dic[@"token"] = token;
    dic[@"type"] = @"3";
    dic[@"title"] = self.articleTitle;
    dic[@"content_id"] = self.articleStringID;
    dic[@"reply"] = self.commentText.text;
    self.plArray = [NSMutableArray array];
    [GMAfnTools PostHttpDataWithUrlStr:KHomeCoursePJ Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"文章评价----%@",responseObject);
        if (responseObject) {
            [self showError:[responseObject objectForKey:@"msg"]];
            [self getArticleList];
        }else {
            [self showError:[responseObject objectForKey:@"msg"]];
        }
    } FailureBlock:^(id  _Nonnull error) {
        
    }];
}

- (void)getArticleString {
    NSMutableDictionary *articleDic = [NSMutableDictionary dictionary];
    articleDic[@"article_id"] = self.articleStringID;
    [GMAfnTools PostHttpDataWithUrlStr:KWenZHangDetail Dic:articleDic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"打印文章详情----%@",responseObject);
        [self.detailImg sd_setImageWithURL:[NSURL URLWithString:[responseObject objectForKey:@"article_cover"]]];
    } FailureBlock:^(id  _Nonnull error) {
        
    }];
}

- (void)setNavigation {
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:[UIImage imageNamed:@"back-icon"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButton];
    backButton.sd_layout.topSpaceToView(self.view,MStatusBarHeight+12)
    .leftSpaceToView(self.view, 15)
    .widthIs(11).heightIs(19);
    UIButton *backButtonView = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 80, MStatusBarHeight+44)];
    backButtonView.backgroundColor = RGBA(15, 15, 15, 0);
    [backButtonView addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButtonView];
    UILabel *titleLabel = [[UILabel alloc]init];
    [self.view addSubview:titleLabel];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"育朵精选" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 18],NSForegroundColorAttributeName: [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1.0]}];
    titleLabel.attributedText = string;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.sd_layout.topSpaceToView(self.view, MStatusBarHeight+13)
    .centerXEqualToView(self.view)
    .heightIs(18).widthIs(100);
    
    self.collectButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.collectButton setBackgroundImage:[UIImage imageNamed:@"ico_my_1_4"] forState:UIControlStateNormal];
    [self.collectButton addTarget:self action:@selector(collect:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.collectButton];
    self.collectButton.sd_layout.topSpaceToView(self.view, MStatusBarHeight+14)
    .rightSpaceToView(self.view, 53)
    .widthIs(24).heightIs(24);
    
    self.shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.shareButton setBackgroundImage:[UIImage imageNamed:@"fx-icon(1)"] forState:UIControlStateNormal];
    [self.shareButton addTarget:self action:@selector(share) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.shareButton];
    self.shareButton.sd_layout.topEqualToView(self.collectButton)
    .rightSpaceToView(self.view, 15)
    .widthIs(24).heightIs(24);
}
#pragma mark-收藏
- (void)collect:(UIButton *)send {
    //    NSUserDefaults *defaultsx =  [NSUserDefaults standardUserDefaults];
    //    if ([[defaultsx objectForKey:@"user_id"] isEqualToString:@""]) {
    //        LoginViewController *login = [[LoginViewController alloc]init];
    //        [self.navigationController presentViewController:login animated:YES completion:nil];
    //    }
    self.cancelCollectString = [NSMutableArray array];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:@"token"] ) {
        
        
        send.selected = !send.selected;
        if (send.selected == YES) {
            [send setBackgroundImage:[UIImage imageNamed:@"矢量智能对象(14)"] forState:UIControlStateNormal];
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            dic[@"user_id"] = [defaults objectForKey:@"user_id"];
            dic[@"token"] = [defaults objectForKey:@"token"];
            dic[@"type"] = @"3";
            dic[@"collection_id"] = self.articleStringID;
            
            [GMAfnTools PostHttpDataWithUrlStr:KCollectAdd Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
                NSLog(@"收藏成功++%@",responseObject);
                if ([[responseObject objectForKey:@"code"] isEqualToString:@"1"]) {
                    self.cancelCollectString = [[responseObject objectForKey:@"data"] objectForKey:@"collection_id"];
                    
                    NSUserDefaults *collectStringID = [NSUserDefaults standardUserDefaults];
                    [collectStringID setObject:self.cancelCollectString forKey:@"collection_id"];
                    [self showError:[responseObject objectForKey:@"msg"]];
                } else {
                    NSLog(@"取消成功");
                    //                [self showError:[responseObject objectForKey:@"msg"]];
                }
            } FailureBlock:^(id  _Nonnull error) {
                
            }];
            
            NSLog(@"搜藏");
        }
        if (send.selected == NO) {
//            NSLog(@"xxxxxx%@",self.cancelCollectString);
            [send setBackgroundImage:[UIImage imageNamed:@"ico_my_1_4"] forState:UIControlStateNormal];
            [self test];
            NSLog(@"取消搜藏");
        }
    }else {
        [self showError:@"请先登录"];
    }
}
- (void)test {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"user_id"] = [defaults objectForKey:@"user_id"];
    dic[@"token"] = [defaults objectForKey:@"token"];
    dic[@"type"] = @"3";
    dic[@"collection_id"] = self.articleStringID;
    
    NSLog(@"--%@--",self.articleStringID);
    [GMAfnTools PostHttpDataWithUrlStr:KCollectCancelxx Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
        if ([[responseObject objectForKey:@"code"] isEqualToString:@"1"]) {
            [self showError:[responseObject objectForKey:@"msg"]];
        }
        NSLog(@"%@",responseObject);
        
        
        //            if ([[responseObject objectForKey:@"code"] isEqualToString:@"1"]) {
        //                self.cancelCollectString = [[responseObject objectForKey:@"data"] objectForKey:@"collection_id"];
        //
        //                NSUserDefaults *collectStringID = [NSUserDefaults standardUserDefaults];
        //                [collectStringID setObject:self.cancelCollectString forKey:@"collection_id"];
        //                [self showError:[responseObject objectForKey:@"msg"]];
        //            } else {
        //                [self showError:[responseObject objectForKey:@"msg"]];
        //            }
    } FailureBlock:^(id  _Nonnull error) {
        NSLog(@"取消收藏失败---%@",error);
    }];
}
#pragma mark -- 提示框
- (void)showError:(NSString *)errorMsg {
    // 1.弹框提醒
    // 初始化对话框
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:errorMsg preferredStyle:UIAlertControllerStyleAlert];
    //    [alert addAction:[UIAlertAction actionWithTitle:@"" style:UIAlertActionStyleDefault handler:nil]];
    // 弹出对话框
    [self presentViewController:alert animated:true completion:nil];
    
    [self performSelector:@selector(dimisssAlert:) withObject:alert afterDelay:1];
    
}

- (void) dimisssAlert:(UIAlertController *)alert
{
    if (alert) {
        [alert dismissViewControllerAnimated:YES completion:nil];
    }
    
}

- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBar.topItem.title = @"课程";
}

- (void)backBtn {
    [self.navigationController popViewControllerAnimated:YES];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 150;
}

//-(CGFloat)cellHeightForIndexPath:(NSIndexPath *)indexPath cellContentViewWidth:(CGFloat)width tableView:(UITableView *)tableView {
//    return 300;
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.listArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"cellID";
    PJTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[PJTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    KCPJModel *mol = [self.listArray objectAtIndex:indexPath.row];
    NSLog(@"xxxxxxX%@",mol.nikname);
    cell.timeLabel.text = mol.comments_time;
    cell.nameLabel.text = mol.nikname;
    cell.answerLabel.text = mol.comments;

    return cell;
}
@end
