
//
//  ZXShuoMingViewController.m
//  yuduo
//
//  Created by mason on 2019/9/26.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "ZXShuoMingViewController.h"

@interface ZXShuoMingViewController ()

@end

@implementation ZXShuoMingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = GMWhiteColor;
    [self  changeNavigation];
    [self zxShuoMing];
    // Do any additional setup after loading the view.
}
- (void)zxShuoMing {
    UILabel *label = [[UILabel alloc] init];
    label.numberOfLines = 0;
    [self.view addSubview:label];
    label.sd_layout.topSpaceToView(self.view, MStatusBarHeight+74)
    .leftSpaceToView(self.view, 5)
    .widthIs(100).heightIs(15);
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"一、咨询内容" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFang SC" size: 15],NSForegroundColorAttributeName: [UIColor colorWithRed:69/255.0 green:69/255.0 blue:69/255.0 alpha:1.0]}];
    label.attributedText = string;
    
    UILabel *label1 = [[UILabel alloc] init];
    label1.numberOfLines = 2;
    label1.font = kFont(14);
    [self.view addSubview:label1];
    label1.sd_layout.topSpaceToView(label, 19)
    .leftSpaceToView(self.view, 16)
    .widthIs(KScreenW-32).heightIs(45);
    
    
    NSMutableAttributedString *string1 = [[NSMutableAttributedString alloc] initWithString:@"1.1   vip咨询也可以单独购买，定价是一次咨询10元，全年12次就是120元" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFang SC" size: 14],NSForegroundColorAttributeName: [UIColor colorWithRed:102/255.0 green:102/255.0 blue:102/255.0 alpha:1.0]}];
    
    label1.attributedText = string1;
    
    UILabel *label2 = [[UILabel alloc] init];
//    label2.frame = CGRectMake(17,176.5,338,36.5);
    label2.numberOfLines = 0;
    label2.font = kFont(14);
    [self.view addSubview:label2];
    label2.sd_layout.topSpaceToView(label1, 14)
    .leftSpaceToView(self.view, 16)
    .widthIs(KScreenW-32).heightIs(45);
    
    NSMutableAttributedString *string2 = [[NSMutableAttributedString alloc] initWithString:@"1.2  vip咨询也可以单独购买，定价是一次咨询10元，全年12次就是120元" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFang SC" size: 14],NSForegroundColorAttributeName: [UIColor colorWithRed:102/255.0 green:102/255.0 blue:102/255.0 alpha:1.0]}];
    
    label2.attributedText = string2;
    
    UILabel *label3 = [[UILabel alloc] init];
    label3.frame = CGRectMake(16,235.5,89,14.5);
    label3.numberOfLines = 2;
    label3.font = kFont(14);
    [self.view addSubview:label3];
    label3.sd_layout.topSpaceToView(label2, 23)
    .leftSpaceToView(self.view, 16)
    .widthIs(KScreenW-32).heightIs(15);
    
    NSMutableAttributedString *string3 = [[NSMutableAttributedString alloc] initWithString:@"二、咨询内容" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFang SC" size: 15],NSForegroundColorAttributeName: [UIColor colorWithRed:69/255.0 green:69/255.0 blue:69/255.0 alpha:1.0]}];
    
    label3.attributedText = string3;
    
    UILabel *label4 = [[UILabel alloc] init];
    label4.frame = CGRectMake(17,268.5,342.5,36.5);
    label4.numberOfLines = 2;
    label4.font = kFont(14);
    [self.view addSubview:label4];
    label4.sd_layout.topSpaceToView(label3, 23)
    .leftSpaceToView(self.view, 16)
    .widthIs(KScreenW-32).heightIs(45);
    
    NSMutableAttributedString *string4 = [[NSMutableAttributedString alloc] initWithString:@"2.1   vip咨询也可以单独购买，定价是一次咨询10元，全年12次就是120元" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFang SC" size: 14],NSForegroundColorAttributeName: [UIColor colorWithRed:102/255.0 green:102/255.0 blue:102/255.0 alpha:1.0]}];
    
    label4.attributedText = string4;
    
    UILabel *label5 = [[UILabel alloc] init];
    label5.frame = CGRectMake(17,318.5,341,36.5);
    label5.numberOfLines = 2;
    label5.font = kFont(14);
    [self.view addSubview:label5];
    label5.sd_layout.topSpaceToView(label4, 14)
    .leftSpaceToView(self.view, 16)
    .widthIs(KScreenW-32).heightIs(45);
    
    NSMutableAttributedString *string5 = [[NSMutableAttributedString alloc] initWithString:@"2.2  vip咨询也可以单独购买，定价是一次咨询10元，全年12次就是120元" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFang SC" size: 14],NSForegroundColorAttributeName: [UIColor colorWithRed:102/255.0 green:102/255.0 blue:102/255.0 alpha:1.0]}];
    
    label5.attributedText = string5;
}
- (void)changeNavigation {
    
    self.navigationController.navigationBar.topItem.title = @"咨询说明";
    UIView *titleView = [[UIView alloc]init];
    titleView.backgroundColor = GMWhiteColor;
    [self.view addSubview:titleView];
    titleView.sd_layout.topSpaceToView(self.view,0)
    .leftSpaceToView(self.view, 0)
    .rightSpaceToView(self.view, 0)
    .widthIs(KScreenW).heightIs(MStatusBarHeight+44);
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setBackgroundImage:[UIImage imageNamed:@"back-icon"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
    [titleView addSubview:backButton];
    backButton.sd_layout.bottomSpaceToView(titleView,13)
    .leftSpaceToView(titleView, 15)
    .widthIs(11).heightIs(19);
    UIButton *backButtonView = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 80, MStatusBarHeight+44)];
    backButtonView.backgroundColor = RGBA(15, 15, 15, 0);
    [backButtonView addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButtonView];
    UILabel *titleLabel = [[UILabel alloc]init];
    [titleView addSubview:titleLabel];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"咨询说明" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 18],NSForegroundColorAttributeName: [UIColor colorWithRed:69/255.0 green:69/255.0 blue:69/255.0 alpha:1.0]}];
    titleLabel.attributedText = string;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.sd_layout.bottomSpaceToView(titleView, 15)
    .centerXEqualToView(titleView)
    .heightIs(18).widthIs(100);
    
    
    //    [self cateTableViewCreate];
    
}

- (void)backBtn {
    [self.navigationController popViewControllerAnimated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
