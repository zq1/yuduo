//
//  CommitViewController.h
//  yuduo
//
//  Created by Mac on 2019/8/13.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CommitViewController : UIViewController
PropertyStrong(UIScrollView, backgroundView);
PropertyStrong(UILabel, titleLabel);
PropertyStrong(UITextView, titleText);
PropertyStrong(UILabel , detailLable);
PropertyStrong(UITextView, detailText);
PropertyStrong(UILabel, imgLabel);
PropertyStrong(UIButton, imgUpdate);
PropertyStrong(UIButton, customButton);
PropertyStrong(UIButton , vipButton);
PropertyStrong(UILabel, alertLabel);
@end

NS_ASSUME_NONNULL_END
