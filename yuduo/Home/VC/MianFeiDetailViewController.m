//
//  MianFeiDetailViewController.m
//  yuduo
//
//  Created by Mac on 2019/8/13.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "MianFeiDetailViewController.h"
#import "TapSliderScrollView.h"
#import "SimpleTapSliderScrollView.h"

//详细列表
#import "KeChengViewController.h"
#import "ZhuanLanViewController.h"
#import "ZhiBoViewController.h"
#import "WenZhangViewController.h"

//免费专区
#import "CoureDetailViewController.h"
#import "HomeColumnDetailViewController.h"
#import "NewYuDuoChoiceDetailViewController.h"
#import "NewZhiboDetailViewController.h"

@interface MianFeiDetailViewController ()<SliderLineViewDelegate,SimpleSliderLineViewDelegate,UITableViewDelegate,UITableViewDataSource, UIScrollViewDelegate>
@property (nonatomic ,assign) BOOL selectSort;

@property (nonatomic, strong) NSArray *dataArr;

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) NSMutableArray *myChildViewControllers;
@end

@implementation MianFeiDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self changeNavigation];
    [self scrollZX];
    
    //    [self tab];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}
- (void)scrollZX {
    
    KeChengViewController *vc1 = [[KeChengViewController alloc]init];
    vc1.KeChengViewControllerCellSelect = ^(NSString * _Nonnull categaryId) {
        CoureDetailViewController *coureDetailVCcc = [[CoureDetailViewController alloc]init];
                   coureDetailVCcc.courseDetailIDString = categaryId;
                   [coureDetailVCcc setHidesBottomBarWhenPushed:YES];
                   [self.navigationController pushViewController:coureDetailVCcc animated:NO];
    };
    
    ZhuanLanViewController *vc2 = [[ZhuanLanViewController alloc]init];
    vc2.isFree = YES;
    //专栏详情
    vc2.ZhuanLanViewControllerCellSelect = ^(NSString * _Nonnull categaryId, NSString * _Nonnull title) {
        HomeColumnDetailViewController *coureDetailVC = [[HomeColumnDetailViewController alloc]init];
        coureDetailVC.courseDetailIDString = categaryId;
        coureDetailVC.courseTitle = title;
        [coureDetailVC setHidesBottomBarWhenPushed:YES];
        [self.navigationController pushViewController:coureDetailVC animated:NO];
    };
    ZhiBoViewController *vc3 = [[ZhiBoViewController alloc]init];
    vc3.ZhiBoViewControllerCellSelect = ^(NSString * _Nonnull categaryId) {
        //直播详情
        NewZhiboDetailViewController * detail = [[NewZhiboDetailViewController alloc] init];
        [self.navigationController pushViewController:detail animated:NO];
    };
    WenZhangViewController *vc4 = [[WenZhangViewController alloc]init];
    vc4.WenZhangViewControllerCellSelect = ^(NSString * _Nonnull categaryId, NSString * _Nonnull title) {
        NewYuDuoChoiceDetailViewController *homeJX = [[NewYuDuoChoiceDetailViewController alloc]init];
        homeJX.articleStringID = categaryId;
        homeJX.articleTitle = title;
        [homeJX setHidesBottomBarWhenPushed:YES];
        [self.navigationController pushViewController:homeJX animated:NO];
    };
   
    SimpleTapSliderScrollView *simpleview = [[SimpleTapSliderScrollView alloc]initWithFrame:CGRectMake(0, 45+MStatusBarHeight, KScreenW, KScreenH)];
    simpleview.delegate = self;
    simpleview.sliderViewColor = RGB(21, 152, 164);
    simpleview.titileColror = RGB(153, 153, 153);
    simpleview.selectedColor = RGB(21, 152, 164);
    [simpleview createView:@[@"课程",@"专栏",@"直播",@"文章"] andViewArr:@[vc1,vc2,vc3,vc4] andRootVc:self];
    [self.view addSubview:simpleview];
    [simpleview sliderToViewIndex:0];
}
#pragma mark sliderDelegate -- OC
-(void)sliderViewAndReloadData:(NSInteger)index
{
    NSLog(@"刷新数据啦%ld",index);
    if (index == 3) {
        NSLog(@"创建tableview");
    }
}


- (void)tab {
    UITableView *table = [[UITableView alloc]initWithFrame:CGRectMake(0, 100, 300, 400) style:UITableViewStylePlain];
    table.backgroundColor = GMBlueColor;
    table.delegate =self;
    table.dataSource= self;
    [self.view addSubview:table];
}


- (void)changeNavigation {
    
    self.navigationController.navigationBar.topItem.title = @"免费专区";
    UIView *titleView = [[UIView alloc]init];
    titleView.backgroundColor = GMWhiteColor;
    [self.view addSubview:titleView];
    titleView.sd_layout.topSpaceToView(self.view,0)
    .leftSpaceToView(self.view, 0)
    .rightSpaceToView(self.view, 0)
    .widthIs(KScreenW).heightIs(MStatusBarHeight+44);
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setBackgroundImage:[UIImage imageNamed:@"back-icon"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
    [titleView addSubview:backButton];
    backButton.sd_layout.bottomSpaceToView(titleView,13)
    .leftSpaceToView(titleView, 15)
    .widthIs(11).heightIs(19);
    UIButton *backButtonView = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 80, MStatusBarHeight+44)];
    backButtonView.backgroundColor = RGBA(15, 15, 15, 0);
    [backButtonView addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButtonView];
    UILabel *titleLabel = [[UILabel alloc]init];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [titleView addSubview:titleLabel];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"免费专区" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 18],NSForegroundColorAttributeName: [UIColor colorWithRed:69/255.0 green:69/255.0 blue:69/255.0 alpha:1.0]}];
    titleLabel.attributedText = string;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.sd_layout.bottomSpaceToView(titleView, 15)
    .centerXEqualToView(titleView)
    .heightIs(18).widthIs(100);
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}
#pragma mark--返回行高
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40;
}
#pragma mark--返回行数
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
  
    static NSString *key0=@"cell0";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:key0];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:key0];
        
    }
    //    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (void)backBtn {
    NSLog(@"xxxxxxxx");
    [self.navigationController popViewControllerAnimated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
