//
//  ActiveViewController.m
//  yuduo
//
//  Created by Mac on 2019/8/8.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "ActiveViewController.h"
#import "JZLSliderMenuView.h"
#import "ListViewController.h"
#import "ActiveDetailViewController.h"
#import "ActiveClassListModel.h"
#import "WSDatePickerView.h"

@interface ActiveViewController ()<JZLSliderMenuDelegate>
@property (nonatomic, strong) JZLSliderMenuView *sliderMenuView;
@property (nonatomic, strong) NSMutableArray<ActiveClassListModel *> * activeClassModels;

@end

@implementation ActiveViewController

- (void)viewWillAppear:(BOOL)animated {
//        self.navigationController.navigationBarHidden = YES;
//        //self.navigationController.navigationBar.topItem.title = @"发现";
    self.navigationController.navigationBarHidden = NO;

}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    //self.navigationController.navigationBarHidden = YES;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"活动中心";
    self.activeClassModels = [[NSMutableArray alloc] init];
    //通知 push
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(activePush:) name:@"activePush" object:nil];;
    [self setRightBarItemWithImage:@"ActiveDateIcon"];
    //[self changeNavigation];
    [self setLeftBarItemWithImage:@"back-icon"];
   
    [self getInfo];
}
-(void)rightBarItemAction:(UITapGestureRecognizer *)gesture{
    WSDatePickerView *datepicker = [[WSDatePickerView alloc] initWithDateStyle:DateStyleShowYearMonthDay CompleteBlock:^(NSDate *selectDate) {
        NSString *dateString = [selectDate stringWithFormat:@"yyyy-MM-dd"];
        NSLog(@"选择的日期：%@",dateString);
        
    }];
    datepicker.dateLabelColor =GMBrownColor;
    datepicker.datePickerColor = GMBrownColor;
    datepicker.doneButtonColor = GMBlackColor;
    [datepicker show];
}

-(void)getInfo{
     NSMutableDictionary *infoDic = [NSMutableDictionary dictionary];
    infoDic[@"pid"] = @"3";
    WS(weakSelf);
    [GMAfnTools PostHttpDataWithUrlStr:KCategoryID Dic:infoDic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"获取课程分类成功 == %@",responseObject);
        if (responseObject) {
            dispatch_async(dispatch_get_main_queue(), ^{
                for (NSMutableDictionary *idDic in responseObject) {
                    ActiveClassListModel *model = [[ActiveClassListModel alloc]init];
                    [model mj_setKeyValues:idDic];
                    [weakSelf.activeClassModels addObject:model];
                }
                [weakSelf reloadDataForModels:weakSelf.activeClassModels];
               [SVProgressHUD dismiss];
            });
            
            NSLog(@"打印课程-----%@",responseObject);
        }else {
            //加载失败隐藏
        }
    } FailureBlock:^(id  _Nonnull error) {
        
    }];
}
- (void)changeNavigation {
    
    self.navigationController.navigationBar.topItem.title = @"活动详情";
    self.view.backgroundColor = GMWhiteColor;
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setBackgroundImage:[UIImage imageNamed:@"back-icon"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButton];
    backButton.sd_layout.topSpaceToView(self.view,MStatusBarHeight+12)
    .leftSpaceToView(self.view, 15)
    .widthIs(11).heightIs(19);
    
    UIButton *backButtonView = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 80, MStatusBarHeight+44)];
    backButtonView.backgroundColor = RGBA(15, 15, 15, 0);
    [backButtonView addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButtonView];
    
    UILabel *titleLabel = [[UILabel alloc]init];
    [self.view addSubview:titleLabel];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"活动中心" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 18],NSForegroundColorAttributeName: [UIColor colorWithRed:69/255.0 green:69/255.0 blue:69/255.0 alpha:1.0]}];
    titleLabel.attributedText = string;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.sd_layout.topSpaceToView(self.view, MStatusBarHeight+13)
    .centerXEqualToView(self.view)
    .heightIs(18).widthIs(100);
    
    
}
- (void)backBtn {
    NSLog(@"xxxxxxxx");
    
    [self.navigationController popViewControllerAnimated:YES];
}
//- (void)pushVC:(NSNotification *)courseID {
//    CoureDetailViewController *home = [[CoureDetailViewController alloc]init];
//    home.courseDetailIDString = [courseID object];
//    [self.navigationController pushViewController:home animated:YES];
//}
- (void)activePush:(NSNotification *)activeID {
    ActiveDetailViewController *detailVC = [[ActiveDetailViewController alloc]init];
    detailVC.category_id = [activeID object][@"id"];
    detailVC.online = [[activeID object][@"type"] integerValue] == 1 ? YES : NO;
    [self.navigationController pushViewController:detailVC animated:YES];
}

-(void)reloadDataForModels:(NSMutableArray<ActiveClassListModel *> *)models {
    NSMutableArray * titles = [[NSMutableArray alloc] init];
    [titles addObject:@"全部"];
    for (ActiveClassListModel * model in models) {
        [titles addObject:model.title];
    }
   
    NSMutableArray *vcArr = [NSMutableArray array];
    for (int i = 0; i < titles.count ; i ++) {
        ListViewController *vc = [[ListViewController alloc] init];
        if (i == 0) {
            vc.isAll = YES;
            
        }else{
            vc.isAll = NO;
            vc.index = [models[i-1].category_id integerValue];
        }
        [vcArr addObject:vc];
    }
    self.sliderMenuView = [JZLSliderMenuView initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, KScreenH-MStatusBarHeight-44) childViewControllers:vcArr titleArray:titles selectedIndex:0];
    self.sliderMenuView.delegate = self;
    [self.view addSubview:self.sliderMenuView];
//
//    //模拟控制器数据处理
//    ListViewController *vc = vcArr[1];
//    vc.index = 1;
//    [vc.tableView reloadData];
}

- (void)sliderMenu:(JZLSliderMenuView *)sliderMenuView selectItemAtIndex:(NSInteger)index {
    ListViewController *vc = self.sliderMenuView.childViewControllers[index];
    if (index != 0) {
        vc.isAll = NO;
        vc.index = [self.activeClassModels[index - 1].category_id integerValue];
    }else{
        vc.isAll = YES;
    }
      
}
- (void)dealloc {
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}
@end
