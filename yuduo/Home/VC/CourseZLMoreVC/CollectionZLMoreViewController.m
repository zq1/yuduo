//
//  CollectionZLMoreViewController.m
//  yuduo
//
//  Created by mason on 2019/8/17.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "CollectionZLMoreViewController.h"
#import "NewColumnsCollectionViewCell.h"
//专栏分类
#import "HomeColumnModel.h"
//最热专栏
#import "HotZLModel.h"
//最新专栏
#import "NewZLModel.h"
//免费课程
#import "MianFeiZLModel.h"
//vip专栏
#import "ViPZLModel.h"
@interface CollectionZLMoreViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic,strong) NSMutableArray *respoceArray;
PropertyStrong(NSMutableArray, courseArray);
PropertyStrong(NSMutableArray, hotCourseArray);
PropertyStrong(NSMutableArray, mianfeiArray);
PropertyStrong(NSMutableArray, vipMianfeiArray);
@end

@implementation CollectionZLMoreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createCollectionVC];
    self.view.backgroundColor = RGB(244, 243, 244);
    
    
    // Do any additional setup after loading the view.
}
- (void)createCollectionVC {
    //最新专栏
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(HomeNewcourseZL) name:@"HomeNewcourseZL" object:nil];
//    //最热专栏
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(HomeHotCourseZL) name:@"HomeHotCourseZL" object:nil];
//    //免费专栏
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(MianfeiCourseZL) name:@"mianfeiCourseZL" object:nil];
//    //vip限免
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(vipMianfeiZL) name:@"vipMianfeiZL" object:nil];
    self.flowLayout = [[UICollectionViewFlowLayout alloc]init];
    self.flowLayout.itemSize = CGSizeMake((self.view.frame.size.width-30)/2-5, 225);
    self.flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    //    self.flowLayout.minimumLineSpacing = 10;
    //    self.flowLayout.minimumInteritemSpacing = 10;
    self.tableView = [[UICollectionView alloc]initWithFrame:CGRectMake(15, 0, self.view.frame.size.width-30, KScreenH-MStatusBarHeight-25-104) collectionViewLayout:self.flowLayout];
    self.tableView.backgroundColor = GMWhiteColor;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:self.tableView];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView registerClass:[NewColumnsCollectionViewCell class] forCellWithReuseIdentifier:@"myCell"];
   // [self vipMianfeiZL];
}
//
///*-----------++++++++++++++VIP课程----------------------*/
//#pragma mark--VIP免费
- (void)vipMianfeiZL {
    [self vipCourse];
}
-(void)setCategaeyId:(NSString *)categaeyId{
    _categaeyId = categaeyId;
    //[self setupDateSource];
}
#pragma mark--VIP免费
- (void)vipCourse{
    self.respoceArray = [NSMutableArray array];
    NSMutableDictionary *newCourseDic = [NSMutableDictionary dictionary];
    newCourseDic[@"category_id"] = self.categaeyId;
    newCourseDic[@"screen"] = @"4";
    [GMAfnTools PostHttpDataWithUrlStr:KCoulumnGementList Dic:newCourseDic SuccessBlock:^(id  _Nonnull responseObject) {
        for (NSMutableDictionary *dic in responseObject) {
            HotZLModel *mol = [[HotZLModel alloc]init];
            [mol mj_setKeyValues:dic];
            [self.respoceArray addObject:mol];
        }
        [self.tableView reloadData];

    }
                          FailureBlock:^(id  _Nonnull error) {
                          }];
    //    [self.tableView reloadData];
}
///*-----------++++++++++++++免费课程----------------------*/
//#pragma mark--免费专栏
- (void)MianfeiCourseZL {
    [self mianfeiCourse];
}
#pragma mark--免费专栏
- (void)mianfeiCourse{
    self.respoceArray = [NSMutableArray array];
    NSMutableDictionary *newCourseDic = [NSMutableDictionary dictionary];
    newCourseDic[@"category_id"] = self.categaeyId;
    newCourseDic[@"screen"] = @"3";
    [GMAfnTools PostHttpDataWithUrlStr:KCoulumnGementList Dic:newCourseDic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"打印专栏免费--%@",responseObject);
        for (NSMutableDictionary *dic in responseObject) {
            HotZLModel *mol = [[HotZLModel alloc]init];
            [mol mj_setKeyValues:dic];
            [self.respoceArray addObject:mol];
        }
        [self.tableView reloadData];

    }
                          FailureBlock:^(id  _Nonnull error) {
                          }];
    //    [self.tableView reloadData];
}
///*----------------最热专栏------------------------*/
//#pragma mark--最热专栏
- (void)HomeHotCourseZL {
    [self hotCourse];
}
#pragma mark--最热专栏
- (void)hotCourse{
    self.respoceArray = [NSMutableArray array];
    NSMutableDictionary *newCourseDic = [NSMutableDictionary dictionary];
    newCourseDic[@"category_id"] = self.categaeyId;
    newCourseDic[@"screen"] = @"2";
    [GMAfnTools PostHttpDataWithUrlStr:KCoulumnGementList Dic:newCourseDic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"54545445454545454==%@",responseObject);
        for (NSMutableDictionary *dic in responseObject) {
            HotZLModel *mol = [[HotZLModel alloc]init];
            [mol mj_setKeyValues:dic];
            [self.respoceArray addObject:mol];
        }
        [self.tableView reloadData];

    }
                          FailureBlock:^(id  _Nonnull error) {
                          }];
    //    [self.tableView reloadData];
}

/*++++++++++++++++最新专栏++++++++++++++++++++++++++++*/
- (void)column{
    self.respoceArray = [NSMutableArray array];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"category_id"] = self.categaeyId;
    dic[@"screen"] = @"1";
    [GMAfnTools PostHttpDataWithUrlStr:KCoulumnGementList Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"打印最新专栏列表====%@",responseObject);
        for (NSMutableDictionary *dic in responseObject) {
            HotZLModel *mol = [[HotZLModel alloc]init];
            [mol mj_setKeyValues:dic];
            [self.respoceArray addObject:mol];
        }
        [self.tableView reloadData];
    } FailureBlock:^(id  _Nonnull error) {
        
    }];
}
#pragma mark--最新专栏4
- (void)HomeNewcourseZL {
    [self column];
}

#pragma mark -- collectionView delegate , datasource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.respoceArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NewColumnsCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"myCell" forIndexPath:indexPath];
    
    cell.contentView.layer.cornerRadius = 5.0f;
    cell.contentView.layer.borderWidth = 0.08f;
    cell.contentView.layer.borderColor = [UIColor blackColor].CGColor; cell.contentView.layer.masksToBounds =YES;
    
//    self.index = indexPath.row;
     HotZLModel *mol = [self.respoceArray objectAtIndex:indexPath.row];
           cell.titleLabel.text = mol.course_name;
           [cell.imgView sd_setImageWithURL:[NSURL URLWithString:mol.course_free_cover]];
           cell.keshiLabel.text = mol.class_hour;
           cell.peoplesLabel.text = mol.course_vrows;
           cell.priceLabel.text = [@"¥" stringByAppendingString:mol.course_price];
           cell.xpriceLabel.text = [@"¥" stringByAppendingString:mol.course_under_price];
//    if (self.index == 1) {
//        cell.titleLabel.text = @"测试22222";
//    }
//    if (self.index == 2) {
//        cell.titleLabel.text = @"测试33333";
//    }
    return cell;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"%ld %ld",indexPath.section,indexPath.row);
    HotZLModel * model = self.respoceArray[indexPath.row];
    if (self.CollectionZLMoreViewControllerSelect) {
        self.CollectionZLMoreViewControllerSelect(model.gement_id);
    }

    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
