//
//  CollectionZLMoreViewController.h
//  yuduo
//
//  Created by mason on 2019/8/17.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CollectionZLMoreViewController : UIViewController
@property (nonatomic, assign) NSInteger index;
@property (nonatomic, strong) NSString * categaeyId;
@property (nonatomic, strong) UICollectionView *tableView;
//@property (strong,nonatomic)UICollectionView *collectionV;
@property (strong,nonatomic)UICollectionViewFlowLayout *flowLayout;
@property (nonatomic ,strong) void(^CollectionZLMoreViewControllerSelect)(NSString * categaryId);
@end

NS_ASSUME_NONNULL_END
