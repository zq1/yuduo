//
//  ListViewController.h
//  yuduo
//
//  Created by Mac on 2019/8/12.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ListViewController : UIViewController
@property (nonatomic, assign) NSInteger index;
@property (nonatomic, assign) BOOL isAll;
@property (nonatomic, strong) UITableView *tableView;


@end

NS_ASSUME_NONNULL_END
