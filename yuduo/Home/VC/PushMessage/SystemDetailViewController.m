//
//  SystemDetailViewController.m
//  yuduo
//
//  Created by Mac on 2019/9/22.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "SystemDetailViewController.h"

@interface SystemDetailViewController ()

@end

@implementation SystemDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavigation];
    UIView *bgView = [[UIView alloc]init];
    bgView.backgroundColor = GMWhiteColor;
    [self.view addSubview:bgView];
    bgView.sd_layout.topSpaceToView(self.view, MStatusBarHeight+54)
    .leftSpaceToView(self.view, 0)
    .rightSpaceToView(self.view, 0)
    .heightIs(KScreenH-MStatusBarHeight-54);
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"msg_id"] = self.sysMsgIDString;
    [GMAfnTools PostHttpDataWithUrlStr:KSysMsgDetailURL Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"消息详情----%@",responseObject);
        UILabel *msgTitle = [[UILabel alloc] init];
        msgTitle.textColor = RGB(69, 69, 69);
        msgTitle.text = [responseObject objectForKey:@"title"];
        msgTitle.font = [UIFont fontWithName:KPFType size:15];
        [bgView addSubview:msgTitle];
        msgTitle.sd_layout.topSpaceToView(bgView, 16)
        .leftSpaceToView(bgView, 16)
        .rightSpaceToView(bgView, 16)
        .heightIs(16);
        
        //时间
        UILabel *labelTime = [[UILabel alloc] init];
        labelTime.numberOfLines = 0;
        labelTime.textColor = RGB(128, 128, 128);
//        NSString *timeStr = [self getTimeFromTimesTamp:[responseObject objectForKey:@"create_time"]];
        labelTime.text = [responseObject objectForKey:@"create_time"];
        
        labelTime.font = [UIFont fontWithName:KPFType size:13];
        [bgView addSubview:labelTime];
        labelTime.sd_layout.topSpaceToView(msgTitle, 10)
        .leftSpaceToView(bgView, 17)
        .rightSpaceToView(bgView, 17)
        .heightIs(10);
        
        UIView *line = [[UIView alloc] init];
        line.backgroundColor = RGB(230, 230, 230);
        [bgView addSubview:line];
        line.sd_layout.topSpaceToView(labelTime, 15)
        .leftSpaceToView(bgView, 15)
        .rightSpaceToView(bgView, 15)
        .heightIs(1);
        
        //消息q详情
        UILabel *labelContent = [[UILabel alloc] init];
        labelContent.numberOfLines = 0;
        labelContent.font = [UIFont fontWithName:KPFType size:14];
        labelContent.textColor = RGB(102, 102, 102);
        NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithData:[[responseObject objectForKey:@"content"] dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes: nil error:nil];
        labelContent.attributedText = text;
//        labelContent.text = [responseObject objectForKey:@"content"];
        [bgView addSubview:labelContent];
        labelContent.sd_layout.topSpaceToView(labelTime, 40)
        .leftSpaceToView(bgView, 16)
        .rightSpaceToView(bgView, 16);
//        .heightIs(319);
        [labelContent sizeToFit];
    } FailureBlock:^(id  _Nonnull error) {
        
    }];
    
    
    // Do any additional setup after loading the view.
}
- (void)setNavigation {
    self.view.backgroundColor = RGB(244, 243, 244);
    
    UIView *titleView = [[UIView alloc]init];
    titleView.backgroundColor = GMWhiteColor;
    [self.view addSubview:titleView];
    titleView.sd_layout.topSpaceToView(self.view, 0)
    .leftEqualToView(self.view)
    .widthIs(KScreenW).heightIs(44+MStatusBarHeight);
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //    backButton.backgroundColor = GMBlueColor;
    [backButton setBackgroundImage:[UIImage imageNamed:@"back-icon"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
    [titleView addSubview:backButton];
    backButton.sd_layout.topSpaceToView(titleView,MStatusBarHeight+15)
    .leftSpaceToView(titleView, 15)
    .widthIs(11).heightIs(19);
    UIButton *backButtonView = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 80, MStatusBarHeight+44)];
    backButtonView.backgroundColor = RGBA(15, 15, 15, 0);
    [backButtonView addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButtonView];
    
    UILabel *forgetPsw = [[UILabel alloc]init];
    forgetPsw.text = @"系统消息详情";
    forgetPsw.textAlignment = NSTextAlignmentCenter;
    forgetPsw.font = kFont(18);
    [titleView addSubview:forgetPsw];
    forgetPsw.sd_layout.topSpaceToView(titleView, MStatusBarHeight+15)
    .centerXEqualToView(titleView).widthIs(150).heightIs(17);

}

- (void)backBtn {
    [self.navigationController popViewControllerAnimated:YES];
}
//时间戳：1520915618

- (NSString *)getTimeFromTimesTamp:(NSString *)timeStr

{
    
        double time = [timeStr doubleValue];
    
        NSDate *myDate = [NSDate dateWithTimeIntervalSince1970:time];
    
        NSDateFormatter *formatter = [NSDateFormatter new];
    
        [formatter setDateFormat:@"YYYY-MM-dd"];
    
        //将时间转换为字符串
    
        NSString *timeS = [formatter stringFromDate:myDate];
    
        return timeS;
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
