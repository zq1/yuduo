//
//  MyMsgViewController.m
//  yuduo
//
//  Created by mason on 2019/9/19.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "MyMsgViewController.h"
#import "MyMsgDetailViewController.h"
#import "SysMsgListModel.h"
@interface MyMsgViewController ()<UITableViewDelegate,UITableViewDataSource>
PropertyStrong(UITableView, detalTablView);
PropertyStrong(NSMutableArray, myMsgListArray);
@end

@implementation MyMsgViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getMyList];
    self.detalTablView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.detalTablView.backgroundColor = GMWhiteColor;
    self.detalTablView.tableFooterView = [UIView new];
    self.detalTablView.delegate = self;
    self.detalTablView.dataSource = self;
    self.detalTablView.showsHorizontalScrollIndicator = NO;
    self.detalTablView.showsVerticalScrollIndicator = NO;
    self.detalTablView.bounces = NO;
    [self.view addSubview:self.detalTablView];
    self.detalTablView.sd_layout.topSpaceToView(self.view, 10)
    .leftEqualToView(self.view)
    .widthIs(KScreenW).bottomSpaceToView(self.view, 0);
    // Do any additional setup after loading the view.
}
- (void)getMyList {
    NSUserDefaults *defaulst = [NSUserDefaults standardUserDefaults];
    NSString *user_id = [defaulst objectForKey:@"user_id"];
    NSString *token = [defaulst objectForKey:@"token"];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"user_id"] = user_id;
    dic[@"token"] = token;
    self.myMsgListArray = [NSMutableArray array];
    if (user_id == nil) {
        [self.detalTablView reloadData];
        return;
    }
    [GMAfnTools PostHttpDataWithUrlStr:KMyMsgListURL Dic:dic SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"获取我的消息列表====%@",responseObject);
        for (NSMutableDictionary *dic in responseObject) {
            SysMsgListModel *mol = [[SysMsgListModel alloc]init];
            [mol mj_setKeyValues:dic];
            [self.myMsgListArray addObject:mol];
        }
        [self.detalTablView reloadData];
    } FailureBlock:^(id  _Nonnull error) {
        NSLog(@"获取我的消息列表---%@",error);
    }];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.myMsgListArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"cellID";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
//    cell.textLabel.text = @"你已经成功购买丛草日记最新课程";
    SysMsgListModel *mol = [self. myMsgListArray objectAtIndex:indexPath.row];
    cell.textLabel.text = mol.title;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    MyMsgDetailViewController *myMsgVC = [[MyMsgDetailViewController alloc]init];
    SysMsgListModel *mol = [self.myMsgListArray objectAtIndex:indexPath.row];
    
    myMsgVC.myMsgDetailString = mol.mgs_id;
    
    [self.navigationController pushViewController:myMsgVC animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
