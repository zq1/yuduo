//
//  SystemMsgViewController.m
//  yuduo
//
//  Created by mason on 2019/9/19.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "SystemMsgViewController.h"
#import "SysMsgListModel.h"
#import "SystemDetailViewController.h"
@interface SystemMsgViewController ()<UITableViewDelegate,UITableViewDataSource>
PropertyStrong(UITableView, detalTablView);
PropertyStrong(NSMutableArray, msgListArray);
@end

@implementation SystemMsgViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getDataList];
    self.detalTablView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.detalTablView.backgroundColor = GMWhiteColor;
    self.detalTablView.tableFooterView = [UIView new];
    self.detalTablView.delegate = self;
    self.detalTablView.dataSource = self;
    self.detalTablView.showsHorizontalScrollIndicator = NO;
    self.detalTablView.showsVerticalScrollIndicator = NO;
    self.detalTablView.bounces = NO;
    [self.view addSubview:self.detalTablView];
    self.detalTablView.sd_layout.topSpaceToView(self.view, 10)
    .leftEqualToView(self.view).bottomSpaceToView(self.view, 0)
    .widthIs(KScreenW);
    // Do any additional setup after loading the view.
}

- (void)getDataList {
    self.msgListArray = [NSMutableArray array];
    [GMAfnTools PostHttpDataWithUrlStr:KSysMsgListURL Dic:nil SuccessBlock:^(id  _Nonnull responseObject) {
        NSLog(@"xxxx消息列表===%@",responseObject);
        
        for (NSMutableDictionary *dic in responseObject) {
            SysMsgListModel *mol = [[SysMsgListModel alloc]init];
            [mol mj_setKeyValues:dic];
            [self.msgListArray addObject:mol];
        }
        [self.detalTablView reloadData];
    } FailureBlock:^(id  _Nonnull error) {
        
    }];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.msgListArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"cellID";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        
    }
    cell.textLabel.text = @"关于3-8的,开启最新活动通知";
    SysMsgListModel *mol = [self.msgListArray objectAtIndex:indexPath.row];
    cell.textLabel.text = mol.title;
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    SysMsgListModel *mol = [self.msgListArray objectAtIndex:indexPath.row];
    SystemDetailViewController *systemVC = [[SystemDetailViewController alloc]init];
    systemVC.sysMsgIDString = mol.mgs_id;
    
    [self.navigationController pushViewController:systemVC animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
