//
//  ActiveAddressTableViewCell.h
//  yuduo
//
//  Created by Mac on 2019/8/15.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ActiveAddressTableViewCell : UITableViewCell
PropertyStrong(UIView, bgView);
PropertyStrong(UIImageView, headImg);
PropertyStrong(UILabel, titleLabel);
PropertyStrong(UILabel, activeLabel);
PropertyStrong(UILabel, dateLabel);
PropertyStrong(UILabel, priceLabel);
//
PropertyStrong(UILabel, vipLabel);
PropertyStrong(UILabel, addressLabel);
@end

NS_ASSUME_NONNULL_END
