//
//  NewColumnsCollectionViewCell.h
//  yuduo
//
//  Created by Mac on 2019/8/7.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NewColumnsCollectionViewCell : UICollectionViewCell
PropertyStrong(UIView, bgView);

PropertyStrong(UIImageView, imgView); //背景图片
PropertyStrong(UILabel , titleLabel); //标题
PropertyStrong(UIImageView, keshiImg); //课时图标
PropertyStrong(UILabel , keshiLabel); //课时
PropertyStrong(UIImageView, seeImg);  //观看图标
PropertyStrong(UILabel, peoplesLabel); //观看人数
PropertyStrong(UILabel, priceLabel); // 现在价格
PropertyStrong(UILabel, xpriceLabel);//原价
@end

NS_ASSUME_NONNULL_END
