//
//  ActiveCenterDetailWebView.m
//  yuduo
//
//  Created by 刘耀聪 on 2019/10/24.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "ActiveCenterDetailWebView.h"
@interface ActiveCenterDetailWebView ()
<UIWebViewDelegate,WKNavigationDelegate,WKUIDelegate,WKScriptMessageHandler,UIScrollViewDelegate>

@property (nonatomic, assign) CGFloat webHeight;

@property (nonatomic, strong) WKWebViewConfiguration *configuration;

@end
@implementation ActiveCenterDetailWebView
#pragma mark
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        

    }
    return self;
}
- (WKWebView *)webView{
    if (!_webView) {
        _webView = [[WKWebView alloc] init];
        _webView.navigationDelegate = self;
        _webView.UIDelegate = self;
        _webView.frame = CGRectMake(0, 0,KScreenW-30, self.frame.size.height);
        [_webView.scrollView setShowsVerticalScrollIndicator:NO];
        [_webView.scrollView setShowsHorizontalScrollIndicator:NO];
        [self addSubview:self.webView];
//        _webView.scrollView.scrollEnabled = NO;
//        [_webView.scrollView addObserver:self forKeyPath:@"contentSize" options:NSKeyValueObservingOptionNew context:nil];
    }
    return _webView;
}
-(void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
}
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context{
    
//    WS(weakSelf);
    if ([keyPath isEqualToString:@"contentSize"]) {
        
        self.webHeight = self.webView.scrollView.contentSize.height;
        if ([self.delegate respondsToSelector:@selector(changeHeaderViewHeight:)]) {
            [self.delegate changeHeaderViewHeight:self.webHeight +36];
        }
        self.webView.frame = CGRectMake(0, 0,KScreenW, self.webHeight);
        [self addSubview:self.webView];
    }
}
-(void)setWebCode:(NSString *)webCode{
    [self.webView loadHTMLString:webCode baseURL:nil];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
