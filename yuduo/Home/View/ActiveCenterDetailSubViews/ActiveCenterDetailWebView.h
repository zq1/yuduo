//
//  ActiveCenterDetailWebView.h
//  yuduo
//
//  Created by 刘耀聪 on 2019/10/24.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>
NS_ASSUME_NONNULL_BEGIN
@protocol ActiveCenterDetailWebViewDelegate <NSObject>

// 获取webView高度后改变tableViewHeaderView高度
- (void)changeHeaderViewHeight:(CGFloat)height;

@end
@interface ActiveCenterDetailWebView : UIView

@property (nonatomic, weak) id<ActiveCenterDetailWebViewDelegate> delegate;
@property (nonatomic, strong) WKWebView *webView;
@property (nonatomic, strong) NSString *webCode;
@end

NS_ASSUME_NONNULL_END
