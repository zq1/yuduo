
//
//  ActiveCenterTableViewCell.m
//  yuduo
//
//  Created by Mac on 2019/8/13.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "ActiveCenterTableViewCell.h"

@implementation ActiveCenterTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createLayout];
    }
    return self;
}

- (void)createLayout {
    
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = GMWhiteColor;
    [self.contentView addSubview:self.bgView];
    self.bgView.sd_layout.topSpaceToView(self.contentView, 0)
    .leftSpaceToView(self.contentView, 0)
    .rightSpaceToView(self.contentView, 0)
    .widthIs(KScreenW).heightIs(276);
    
    self.headImg = [[UIImageView alloc]init];
    self.headImg.backgroundColor = GMlightGrayColor;
    [self.bgView addSubview:self.headImg];
    self.headImg.sd_layout.topSpaceToView(self.bgView, 0)
    .leftSpaceToView(self.bgView, 0)
    .widthIs(KScreenW).heightIs(175);
    
    self.vipLabel = [[UILabel alloc]init];
    self.vipLabel.backgroundColor = [UIColor orangeColor];
    self.vipLabel.textColor = GMWhiteColor;
    self.vipLabel.layer.cornerRadius = 9;
    self.vipLabel.layer.masksToBounds = YES;
    self.vipLabel.font = [UIFont fontWithName:@"PingFang SC" size:11];
    self.vipLabel.textAlignment = NSTextAlignmentCenter;
    [self.bgView addSubview:self.vipLabel];
    self.vipLabel.sd_layout.topSpaceToView(self.headImg, 19)
    .leftSpaceToView(self.bgView, 14)
    .widthIs(52*kGMWidthScale).heightIs(17);
    
    self.titleLabel = [[UILabel alloc]init];
    self.titleLabel.text = @"活动标题";
    self.titleLabel.textColor = RGB(69, 69, 69);
    self.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular"size:15];
    [self.bgView addSubview:self.titleLabel];
    self.titleLabel.sd_layout.topSpaceToView(self.headImg, 20)
    .leftSpaceToView(self.vipLabel, 6)
    .widthIs(255).heightIs(14);

    self.activeLabel = [[UILabel alloc]init];
    self.activeLabel.text = @"线上活动";
    self.activeLabel.font = kFont(10);
    [self.bgView addSubview:self.activeLabel];
    self.activeLabel.sd_layout.topSpaceToView(self.vipLabel, 17)
    .leftSpaceToView(self.bgView, 15)
    .widthIs(55).heightIs(17);
    
    self.dateLabel = [[UILabel alloc]init];
    self.dateLabel.text = @"2019-05-05 18:00";
    self.dateLabel.textColor = RGB(153, 153, 153);
    self.dateLabel.font = [UIFont fontWithName:@"PingFangSC-Regular"size:12];
    [self.bgView addSubview:self.dateLabel];
    self.dateLabel.sd_layout.topSpaceToView(self.titleLabel, 19)
    .leftSpaceToView(self.activeLabel, 11)
    .widthIs(110).heightIs(10);
    
    self.priceLabel = [[UILabel alloc]init];
    self.priceLabel.text = @"¥99.00";
    self.priceLabel.textColor = RGB(228, 102, 67);
    self.priceLabel.font = [UIFont fontWithName:@"PingFangSC-Regular"size:18];
    [self.bgView addSubview:self.priceLabel];
    self.priceLabel.sd_layout.topSpaceToView(self.headImg, 47)
    .rightSpaceToView(self.bgView, 22)
    .widthIs(65).heightIs(14);

    
    
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
