//
//  HotConsultTableViewCell.m
//  yuduo
//
//  Created by Mac on 2019/7/24.
//  Copyright © 2019 yaocongkeji. All rights reserved.

#import "HotConsultTableViewCell.h"
@implementation HotConsultTableViewCell
{
    UIView *_backView;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        [self creatView];
    }
    return self;
}
- (void)creatView {
    //背景view（所有控件都加到背景）
    self.bgView = [[UIView alloc]init];
//        self.bgView.backgroundColor = GMBlueColor;
    self.bgView.frame = CGRectMake(0, 0, KScreenW, 85+25);
    [self.contentView addSubview:self.bgView];
    
    self.headImg = [[UIImageView alloc] init];
//    imageView.frame = CGRectMake(30,2742.5,60,60);
//    [UIImage imageNamed:@"图层 2.png"];
    self.headImg.layer.cornerRadius = 30;
    self.headImg.layer.masksToBounds = YES;
    self.headImg.backgroundColor = GMlightGrayColor;
    [self.bgView addSubview:self.headImg];
    self.headImg.sd_layout.topSpaceToView(self.bgView, 10)
    .leftSpaceToView(self.bgView, 15).widthIs(60).heightIs(60);
    
    self.titleLabel = [[UILabel alloc] init];
//    label.frame = CGRectMake(105.5,2754.5,132.5,13.5);
    self.titleLabel.numberOfLines = 0;
    self.titleLabel.text = @"如何让自己的孩子睡觉";
    self.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:14];
    self.titleLabel.textColor = RGB(69, 69, 69);
    [self.bgView addSubview:self.titleLabel];

    self.titleLabel.sd_layout.topSpaceToView(self.bgView, 12)
    .leftSpaceToView(self.headImg, 16)
    .widthIs(200).heightIs(14);
    
    self.timeImg = [[UIImageView alloc]init];
//    self.timeImg.backgroundColor = GMRedColor;
    self.timeImg.image = [UIImage imageNamed:@"ico_time"];
    [self.bgView addSubview:self.timeImg];
    self.timeImg.sd_layout.topSpaceToView(self.bgView, 49)
    .leftSpaceToView(self.headImg, 15).widthIs(15).heightIs(15);
    
    self.timeLabel = [[UILabel alloc] init];
//    label.frame = CGRectMake(129.5,2785.5,68.5,9);
    self.timeLabel.numberOfLines = 0;
    self.timeLabel.text = [self currentdateInterval];
    self.timeLabel.text = @"2019-5-30";
    self.timeLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:12];
    self.timeLabel.textColor = RGB(153, 153, 153);
    [self.bgView addSubview:self.timeLabel];
    self.timeLabel.sd_layout.topSpaceToView(self.bgView, 53)
    .leftSpaceToView(self.timeImg, 10).widthIs(82).heightIs(9);
    
    //多少人看过图标
    self.seeImg = [[UIImageView alloc] init];
    self.seeImg.image = [UIImage imageNamed:@"ico_look"];
    [self.bgView addSubview:self.seeImg];
    self.seeImg.sd_layout.topEqualToView(self.timeImg).leftSpaceToView(self.timeLabel, 22*kGMWidthScale).widthIs(15).heightIs(15);
    
    //多少人看过具体人数标签
    self.peopleLabel = [[UILabel alloc] init];
//    self.peopleLabel.frame = CGRectMake(236.5,970,36,10.5);
    self.peopleLabel.numberOfLines = 0;
    self.peopleLabel.textColor = RGB(153, 153, 153);
    self.peopleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:11];
    self.peopleLabel.text = @"2478人";
    [self.bgView addSubview:self.peopleLabel];
   
    self.peopleLabel.sd_layout.topEqualToView(self.timeLabel)
    .leftSpaceToView(self.seeImg, 6).widthIs(33).heightIs(9);
  
    self.zanImg = [[UIImageView alloc]init];
//    self.zanImg.backgroundColor = GMGreenColor;
    self.zanImg.image = [UIImage imageNamed:@"ico_zan_1"];
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(zanImgTouchUpInside:)];
    self.zanImg.userInteractionEnabled = YES;
    [self.zanImg addGestureRecognizer:tap];
    [self.bgView addSubview:self.zanImg];
    
    self.zanImg.sd_layout.topSpaceToView(self.bgView, 50)
    .leftSpaceToView(self.peopleLabel, 15*kGMWidthScale)
    .widthIs(15)
    .heightIs(15);
    self.dianZanLabel = [[UILabel alloc] init];
//    label.frame = CGRectMake(324.5,2785.5,20.5,9);
    self.dianZanLabel.numberOfLines = 0;
    self.dianZanLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:12];
    self.dianZanLabel.text = @"200";
    self.dianZanLabel.textColor = RGB(153, 153, 153);
    [self.bgView addSubview:self.dianZanLabel];

    self.dianZanLabel.sd_layout.topEqualToView(self.peopleLabel)
    .leftSpaceToView(self.zanImg, 10*kGMWidthScale).widthIs(25).heightIs(9);
    
}
-(void)zanImgTouchUpInside:(UIGestureRecognizer *)tap{
    if (self.HotConsultTableViewCellLikeTouchBlock) {
        self.HotConsultTableViewCellLikeTouchBlock();
    }
}
-(NSString *)currentdateInterval {
NSDate *datenow = [NSDate date];
    NSString *timeSp = [NSString stringWithFormat:@"%ld", (long)([datenow timeIntervalSince1970]*1000)];
    return timeSp;
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
