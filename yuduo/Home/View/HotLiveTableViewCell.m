//
//  HotLiveTableViewCell.m
//  yuduo
//
//  Created by Mac on 2019/7/24.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "HotLiveTableViewCell.h"

@implementation HotLiveTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createHotOneCourse];
    };
    return self;
}

- (void)createHotOneCourse {
    
    self.cellView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenW, 259)];
    self.cellView.layer.cornerRadius = 10;
//    self.cellView.backgroundColor = GMlightGrayColor;
    [self.contentView addSubview:self.cellView];
    
    self.imageBgView = [[UIView alloc]init];
    self.imageBgView.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0];
    [self.cellView addSubview:self.imageBgView];
    self.imageBgView.backgroundColor = GMlightGrayColor;
    self.imageBgView.layer.cornerRadius = 10;
    [self.cellView addSubview:self.imageBgView];
    self.imageBgView.sd_layout.topSpaceToView(self.cellView, 5).leftSpaceToView(self.cellView, 0)
    .rightSpaceToView(self.cellView, 0).heightIs(190);
}
@end
