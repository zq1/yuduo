//
//  NewCourseTwoTableViewCell.m
//  yuduo
//
//  Created by Mac on 2019/7/24.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "NewCourseTwoTableViewCell.h"

@implementation NewCourseTwoTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {

        [self createNewCourse];
    }
    return self;
}

- (void)createNewCourse {
    //背景view（所有控件都加到背景）
    self.bgView = [[UIView alloc]init];
//    self.bgView.backgroundColor = GMBlueColor;
    self.bgView.frame = CGRectMake(0, 0, KScreenW, 134+18);
    [self.contentView addSubview:self.bgView];
    //头像
    self.headImgView = [[UIImageView alloc]init];
    self.headImgView.backgroundColor = GMlightGrayColor;
    
    [self.bgView addSubview:self.headImgView];
    self.headImgView.sd_layout.topSpaceToView(self.bgView, 18)
    .leftSpaceToView(self.bgView, 15).widthIs(100*kGMWidthScale).heightIs(134);
    //免费logo
    self.mianfeiLabel = [[UILabel alloc] init];
    self.mianfeiLabel.numberOfLines = 0;
    self.mianfeiLabel.textColor = GMWhiteColor;
    self.mianfeiLabel.textAlignment = NSTextAlignmentCenter;
    self.mianfeiLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:11];
    self.mianfeiLabel.backgroundColor = [UIColor colorWithRed:228/255.0 green:102/255.0 blue:67/255.0 alpha:1.0];
    [self.bgView addSubview:self.mianfeiLabel];
    self.mianfeiLabel.sd_cornerRadius = [NSNumber numberWithInteger:8.5];
    self.mianfeiLabel.sd_layout.topSpaceToView(self.bgView, 40).leftSpaceToView(self.headImgView, 15).widthIs(45).heightIs(17);
    //标题
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.numberOfLines = 0;
    self.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:15];
    self.titleLabel.textColor = RGB(69, 69, 69);
    [self.bgView addSubview:self.titleLabel];
    
    self.titleLabel.sd_layout.topEqualToView(_mianfeiLabel)
    .leftSpaceToView(self.mianfeiLabel, 5)
    .widthIs(161*KScreenW/375).heightIs(15);
    //课程简介
    self.detailLabel = [[UILabel alloc] init];
    self.detailLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:13];
    self.detailLabel.text = @"课程简介";
    self.detailLabel.textColor = RGB(153, 153, 153);
    self.detailLabel.numberOfLines = 2;
    self.detailLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [self.bgView addSubview:self.detailLabel];
//     self.detailLabel.frame = CGRectMake(132,755.5,228.5,32.5);
    self.detailLabel.sd_layout.topSpaceToView(self.mianfeiLabel, 12)
    .leftSpaceToView(self.headImgView, 17)
    .rightSpaceToView(self.bgView, 15)
   .heightIs(38);
    //课时按钮
    self.keshiButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.keshiButton setBackgroundImage:[UIImage imageNamed:@"更多"] forState:UIControlStateNormal];
    [self.bgView addSubview:self.keshiButton];
    self.keshiButton.sd_layout.leftSpaceToView(self.headImgView, 17*kGMWidthScale)
    .topSpaceToView(self.detailLabel, 26).widthIs(15).heightIs(15);
    
    //课时标签
    self.keshiLabel = [[UILabel alloc] init];
//    label.frame = CGRectMake(157,816,34.5,10.5);
    self.keshiLabel.numberOfLines = 0;
    self.keshiLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:11];
    
    self.keshiLabel.textColor = RGB(153 , 153,153);
    [self.bgView addSubview:self.keshiLabel];
    
    
    self.keshiLabel.sd_layout.topSpaceToView(self.detailLabel, 28)
    .leftSpaceToView(self.keshiButton, 11*kGMWidthScale).widthIs(38).heightIs(11);
    //多少人看过图标
    self.seeImg = [[UIImageView alloc] init];
    self.seeImg.image = [UIImage imageNamed:@"ico_look"];
    [self.bgView addSubview:self.seeImg];
    self.seeImg.sd_layout.topEqualToView(self.keshiButton).leftSpaceToView(self.keshiLabel, 20*kGMWidthScale).widthIs(15).heightIs(15);
    
    
    //多少人看过具体人数标签
    self.peoplesLabel = [[UILabel alloc] init];
    self.peoplesLabel.numberOfLines = 0;
    self.peoplesLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:11];
    self.peoplesLabel.textColor = RGB(153 , 153,153);
    [self.bgView addSubview:self.peoplesLabel];
    self.peoplesLabel.sd_layout.topEqualToView(self.keshiLabel).leftSpaceToView(self.seeImg, 11*kGMWidthScale).widthIs(40).heightIs(11);
    //划掉的价格
    self.xPriceLabel = [[UILabel alloc] init];
    self.xPriceLabel.numberOfLines = 0;
    self.xPriceLabel.textColor = RGB(153, 153, 153);
    self.xPriceLabel.font = [UIFont fontWithName:@"PingFangSC-Regular"size:12];
    [self.bgView addSubview:self.xPriceLabel];
    self.xPriceLabel.sd_layout.topSpaceToView(self.detailLabel, 10)
    .rightSpaceToView(self.bgView, 15).widthIs(50).heightIs(10);
    //现在的价格
    self.priceLabel = [[UILabel alloc] init];
    self.priceLabel.numberOfLines = 0;
    self.priceLabel.textColor = RGB(228, 102, 67);
    self.priceLabel.font = [UIFont fontWithName:@"PingFangSC-Regular"size:15];
    [self.bgView addSubview:self.priceLabel];
    self.priceLabel.sd_layout.topSpaceToView(self.xPriceLabel, 6)
    .rightSpaceToView(self.bgView, 16).widthIs(55).heightIs(12);
    
    UILabel *lineLabel = [[UILabel alloc]init];
    lineLabel.backgroundColor = [UIColor lightGrayColor];
    [self.xPriceLabel addSubview:lineLabel];
    lineLabel.sd_layout.topSpaceToView(self.xPriceLabel, 5)
    .leftSpaceToView(self.xPriceLabel, 0)
    .widthIs(40).heightIs(1);
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
