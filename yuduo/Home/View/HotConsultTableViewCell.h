//  HotConsultTableViewCell.h
//  yuduo
//
//  Created by Mac on 2019/7/24.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HotConsultTableViewCell : UITableViewCell

PropertyStrong(UIView, bgView);
PropertyStrong(UIImageView,headImg);
PropertyStrong(UILabel, titleLabel);
PropertyStrong(UILabel, timeLabel);
PropertyStrong(UIImageView, timeImg);
PropertyStrong(UIImageView, seeImg);
PropertyStrong(UILabel, peopleLabel);
PropertyStrong(UIImageView, zanImg);
PropertyStrong(UILabel, dianZanLabel);
@property (nonatomic, copy) void (^HotConsultTableViewCellLikeTouchBlock)(void);

@end

NS_ASSUME_NONNULL_END
