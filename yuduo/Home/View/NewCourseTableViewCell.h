//
//  NewCourseTableViewCell.h
//  yuduo
//
//  Created by Mac on 2019/7/24.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NewCourseTableViewCell : UITableViewCell
PropertyStrong(UIView , imageBgView);
PropertyStrong(UIView, cellView);
//PropertyStrong(UIImageView, headImageView);
@end

NS_ASSUME_NONNULL_END
