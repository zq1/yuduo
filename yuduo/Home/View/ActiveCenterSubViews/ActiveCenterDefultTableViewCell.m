//
//  ActiveCenterDefultTableViewCell.m
//  yuduo
//
//  Created by 刘耀聪 on 2019/10/24.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "ActiveCenterDefultTableViewCell.h"
@interface ActiveCenterDefultTableViewCell ()

@property (nonatomic, strong) UIImageView *activeImageView;

@property (nonatomic, strong) UIImageView *stateImageView;

@property (nonatomic, strong) UILabel *addressLabel;
@property (nonatomic, strong) UILabel *ingLabel;
@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, strong) UILabel *activeClassLabel;

@property (nonatomic, strong) UILabel *endTimeLabel;

@property (nonatomic, strong) UILabel *priceLabel;

@end
@implementation ActiveCenterDefultTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self.contentView addSubview:self.activeImageView];
        [self.contentView addSubview:self.stateImageView];
        [self.stateImageView addSubview:self.ingLabel];
        [self.contentView addSubview:self.addressLabel];
        [self.contentView addSubview:self.titleLabel];
        [self.contentView addSubview:self.activeClassLabel];
        [self.contentView addSubview:self.endTimeLabel];
        [self.contentView addSubview:self.priceLabel];
        [self addMasonry];
    }
    return self;
}

#pragma mark - # Private Methods
- (void)addMasonry {
    self.activeImageView.frame = CGRectMake(0, 0, KScreenW, 175);
    self.stateImageView.frame = CGRectMake(0, 0, 66, 25);
    self.ingLabel.frame = CGRectMake(5, 5, 40, 15);
    self.addressLabel.frame = CGRectMake(15, 224, KScreenW-80, 17);
    self.titleLabel.frame = CGRectMake(15, 197, KScreenW-80, 20);
    self.activeClassLabel.frame = CGRectMake(15, 255, 45, 17) ;
    self.endTimeLabel.frame = CGRectMake(75, 252, KScreenW-180, 20);
    self.priceLabel.frame = CGRectMake(KScreenW - 100, 240, 80, 30);
}

#pragma mark - # Getter
- (UIImageView *)activeImageView {
    if (!_activeImageView) {
        _activeImageView = [[UIImageView alloc] init];
        _activeImageView.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _activeImageView;
}

- (UIImageView *)stateImageView {
    if (!_stateImageView) {
        _stateImageView = [[UIImageView alloc] init];
        _stateImageView.contentMode = UIViewContentModeScaleAspectFill;
        _stateImageView.image = [UIImage imageNamed:@"activeStateImage"];
    }
    return _stateImageView;
}
-(UILabel *)ingLabel{
    if (!_ingLabel) {
        _ingLabel = [[UILabel alloc] init];
        _ingLabel.text = @"进行中";
        _ingLabel.textColor = [UIColor whiteColor];
        _ingLabel.font = kFont(10);
    }
    return _ingLabel;
}
- (UILabel *)addressLabel {
    if (!_addressLabel) {
        _addressLabel = [[UILabel alloc] init];
        _addressLabel.backgroundColor =[UIColor whiteColor];
       _addressLabel.textColor = RGB(21, 151, 164) ;
        _addressLabel.font = kFont(11);
        _addressLabel.textAlignment = NSTextAlignmentLeft;
    }
    return _addressLabel;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textColor = [UIColor blackColor];
        _titleLabel.font = kFont(15);
    }
    return _titleLabel;
}

- (UILabel *)activeClassLabel {
    if (!_activeClassLabel) {
        _activeClassLabel = [[UILabel alloc] init];
        _activeClassLabel.textColor = RGB(228, 102, 67);
        _activeClassLabel.font = kFont(10);
        _activeClassLabel.textAlignment = NSTextAlignmentCenter;
        _activeClassLabel.layer.borderColor = RGB(228, 102, 67).CGColor;
        _activeClassLabel.layer.borderWidth = 1;
        _activeClassLabel.text = @"线下活动";
    }
    return _activeClassLabel;
}

- (UILabel *)endTimeLabel {
    if (!_endTimeLabel) {
        _endTimeLabel = [[UILabel alloc] init];
        _endTimeLabel.textColor = RGB(153,153,153);
        _endTimeLabel.font = kFont(10);
    }
    return _endTimeLabel;
}

- (UILabel *)priceLabel {
    if (!_priceLabel) {
        _priceLabel = [[UILabel alloc] init];
        _priceLabel.textColor = RGB(228, 102, 67);
        _priceLabel.font = kFont(18);
        _priceLabel.textAlignment = NSTextAlignmentRight;
    }
    return _priceLabel;
}
-(void)setModel:(HomeActiveModel *)model{
    _model = model;
    [self.activeImageView sd_setImageWithURL:[NSURL URLWithString:model.activity_cover]];
    self.titleLabel.text = model.activity_name;
    NSString *timeStr = [self  timewithIntervasting:model.activity_end_date];
    self.addressLabel.text = [NSString stringWithFormat:@"线下活动：%@",model.activity_address];
    self.endTimeLabel.text =timeStr;
    self.priceLabel.text =[@"¥"stringByAppendingString:  model.activity_price];
}

- (void)setMyAcModel:(MyActiviteModel *)myAcModel {
    _myAcModel = myAcModel;
        NSString *cover = (myAcModel.cover == nil || [myAcModel.cover isEqualToString:@""])? myAcModel.activity_cover : myAcModel.cover;
    [self.activeImageView sd_setImageWithURL:[NSURL URLWithString:cover]];
    self.activeImageView.clipsToBounds = true;
    self.titleLabel.text = myAcModel.name;
    NSString *timeStr = myAcModel.activity_end_date;
    self.addressLabel.text = [NSString stringWithFormat:@"线下活动：%@",myAcModel.activity_address];
    self.endTimeLabel.text = timeStr;
    NSString *price = (myAcModel.pay_price == nil || [myAcModel.pay_price isEqualToString:@""]) ? myAcModel.activity_price : myAcModel.pay_price;
    self.priceLabel.text = price == nil ? @"" : [@"¥"stringByAppendingString:  price];
}


//时间戳转指定格式日期
-(NSString *)timewithIntervasting:(NSString*)string{
    
    // iOS 生成的时间戳是10位
    NSTimeInterval interval    =[string doubleValue] / 1000.0;
    NSDate *date               = [NSDate dateWithTimeIntervalSince1970:interval];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];//更改自己想要的时间格式
    NSString *dateString       = [formatter stringFromDate: date];
    return dateString;
}
@end
