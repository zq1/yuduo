//
//  ActiveCenterOnlineTableViewCell.m
//  zhuanzhuan
//
//  Created by ZZUIHelper on 2019/10/24.
//  Copyright © 2017年 ZZUIHelper. All rights reserved.
//

#import "ActiveCenterOnlineTableViewCell.h"

@interface ActiveCenterOnlineTableViewCell ()

@property (nonatomic, strong) UIImageView *activeImageView;

@property (nonatomic, strong) UIImageView *stateImageView;
@property (nonatomic, strong) UILabel *ingLabel;
@property (nonatomic, strong) UILabel *vipLabel;

@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, strong) UILabel *activeClassLabel;

@property (nonatomic, strong) UILabel *endTimeLabel;

@property (nonatomic, strong) UILabel *priceLabel;

@end

@implementation ActiveCenterOnlineTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self.contentView addSubview:self.activeImageView];
        [self.contentView addSubview:self.stateImageView];
        [self.stateImageView addSubview:self.ingLabel];
        [self.contentView addSubview:self.vipLabel];
        [self.contentView addSubview:self.titleLabel];
        [self.contentView addSubview:self.activeClassLabel];
        [self.contentView addSubview:self.endTimeLabel];
        [self.contentView addSubview:self.priceLabel];
        [self addMasonry];
    }
    return self;
}

#pragma mark - # Private Methods
- (void)addMasonry {
    self.activeImageView.frame = CGRectMake(0, 0, KScreenW, 175);
    self.stateImageView.frame = CGRectMake(0, 0, 66, 25);
    self.ingLabel.frame = CGRectMake(5, 5, 40, 15);
    self.vipLabel.frame = CGRectMake(15, 197, 45, 17);
    self.titleLabel.frame = CGRectMake(75, 194, KScreenW-80, 20);
    self.activeClassLabel.frame = CGRectMake(15, 225, 45, 17) ;
    self.endTimeLabel.frame = CGRectMake(75, 222, KScreenW-180, 20);
    self.priceLabel.frame = CGRectMake(KScreenW - 100, 220, 80, 30);
}

#pragma mark - # Getter
- (UIImageView *)activeImageView {
    if (!_activeImageView) {
        _activeImageView = [[UIImageView alloc] init];
        _activeImageView.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _activeImageView;
}

- (UIImageView *)stateImageView {
    if (!_stateImageView) {
        _stateImageView = [[UIImageView alloc] init];
        _stateImageView.contentMode = UIViewContentModeScaleAspectFill;
        _stateImageView.image = [UIImage imageNamed:@"activeStateImage"];
    }
    return _stateImageView;
}
-(UILabel *)ingLabel{
    if (!_ingLabel) {
        _ingLabel = [[UILabel alloc] init];
        _ingLabel.text = @"进行中";
        _ingLabel.textColor = [UIColor whiteColor];
        _ingLabel.font = kFont(10);
    }
    return _ingLabel;
}
- (UILabel *)vipLabel {
    if (!_vipLabel) {
        _vipLabel = [[UILabel alloc] init];
        _vipLabel.backgroundColor = RGB(228, 102, 67);
       _vipLabel.textColor = [UIColor whiteColor];
        _vipLabel.font = kFont(11);
        _vipLabel.layer.cornerRadius = 5;
        _vipLabel.layer.masksToBounds = YES;
        _vipLabel.text =@"vip免费";
        _vipLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _vipLabel;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textColor = [UIColor blackColor];
        _titleLabel.font = kFont(15);
    }
    return _titleLabel;
}

- (UILabel *)activeClassLabel {
    if (!_activeClassLabel) {
        _activeClassLabel = [[UILabel alloc] init];
        _activeClassLabel.textColor = RGB(228, 102, 67);
        _activeClassLabel.font = kFont(10);
        _activeClassLabel.textAlignment = NSTextAlignmentCenter;
        _activeClassLabel.layer.borderColor = RGB(228, 102, 67).CGColor;
        _activeClassLabel.layer.borderWidth = 1;
        _activeClassLabel.text = @"线上活动";
    }
    return _activeClassLabel;
}

- (UILabel *)endTimeLabel {
    if (!_endTimeLabel) {
        _endTimeLabel = [[UILabel alloc] init];
        _endTimeLabel.textColor = RGB(153,153,153);
        _endTimeLabel.font = kFont(10);
    }
    return _endTimeLabel;
}

- (UILabel *)priceLabel {
    if (!_priceLabel) {
        _priceLabel = [[UILabel alloc] init];
        _priceLabel.textColor = RGB(228, 102, 67);
        _priceLabel.font = kFont(18);
        _priceLabel.textAlignment = NSTextAlignmentRight;
    }
    return _priceLabel;
}
-(void)setModel:(HomeActiveModel *)model{
    _model = model;
    [self.activeImageView sd_setImageWithURL:[NSURL URLWithString:model.activity_cover]];
    self.titleLabel.text = model.activity_name;
    NSString *timeStr = model.activity_end_date;
    self.endTimeLabel.text =timeStr;
    self.priceLabel.text =[@"¥"stringByAppendingString:  model.activity_price];
}

- (void)setMyAcModel:(MyActiviteModel *)myAcModel {
    _myAcModel = myAcModel;
    NSString *cover = (myAcModel.cover == nil || [myAcModel.cover isEqualToString:@""])? myAcModel.activity_cover : myAcModel.cover;
    self.activeImageView.clipsToBounds = true;
    [self.activeImageView sd_setImageWithURL:[NSURL URLWithString:cover]];
    self.titleLabel.text = myAcModel.name;
    NSString *timeStr = myAcModel.activity_end_date;
    self.endTimeLabel.text = timeStr;
    NSString *price = myAcModel.pay_price == nil? myAcModel.activity_price : myAcModel.pay_price;
    self.priceLabel.text = price == nil ? @"" : [@"¥"stringByAppendingString:  price];
}

@end
