//
//  ActiveCenterOnlineTableViewCell.h
//  yuduo
//
//  Created by 刘耀聪 on 2019/10/24.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyActiviteModel.h"
#import "HomeActiveModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface ActiveCenterOnlineTableViewCell : UITableViewCell
@property(nonatomic,strong)HomeActiveModel * model;
@property(nonatomic,strong)MyActiviteModel * myAcModel;
@end

NS_ASSUME_NONNULL_END
