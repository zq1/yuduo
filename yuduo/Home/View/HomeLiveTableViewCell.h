//
//  HomeLiveTableViewCell.h
//  yuduo
//
//  Created by Mac on 2019/8/12.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN

@interface HomeLiveTableViewCell : UITableViewCell
PropertyStrong(UIView, bgView);//cell背景view
PropertyStrong(UIImageView, imgView);
PropertyStrong(UILabel, titleLabel);
PropertyStrong(UILabel ,tagLiveLabel);
PropertyStrong(UILabel , dateLabel);

@end
NS_ASSUME_NONNULL_END
