//
//  ColumnsTableViewCell.h
//  yuduo
//
//  Created by Mac on 2019/7/24.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ColumnsTableViewCell : UITableViewCell
PropertyStrong(UIView, bgView);

PropertyStrong(UIImageView, imgView); //背景图片
PropertyStrong(UILabel , titleLabel); //标题
PropertyStrong(UIImageView, keshiImg); //课时图标
PropertyStrong(UILabel , keshiLabel); //课时
PropertyStrong(UIImageView, seeImg);  //观看图标
PropertyStrong(UILabel, peoplesLabel); //多少人看过
PropertyStrong(UILabel, priceLabel); // 现在价格
@end

NS_ASSUME_NONNULL_END
