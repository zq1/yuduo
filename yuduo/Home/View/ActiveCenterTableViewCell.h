//
//  ActiveCenterTableViewCell.h
//  yuduo
//
//  Created by Mac on 2019/8/13.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ActiveCenterTableViewCell : UITableViewCell
PropertyStrong(UIView, bgView);
PropertyStrong(UIImageView, headImg);
PropertyStrong(UILabel, titleLabel);
PropertyStrong(UILabel, activeLabel);
PropertyStrong(UILabel, dateLabel);
PropertyStrong(UILabel, priceLabel);
//
PropertyStrong(UILabel, vipLabel);
PropertyStrong(UILabel, addressLabel);

@end

NS_ASSUME_NONNULL_END
