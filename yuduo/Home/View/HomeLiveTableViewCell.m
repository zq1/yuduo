//
//  HomeLiveTableViewCell.m
//  yuduo
//
//  Created by Mac on 2019/8/12.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "HomeLiveTableViewCell.h"

@implementation HomeLiveTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createHotOneCourse];
    };
    return self;
}

- (void)createHotOneCourse {
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = GMWhiteColor;
    self.bgView.layer.cornerRadius = 10;
    self.bgView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.bgView.layer.shadowOffset = CGSizeMake(0, 1);
    self.bgView.layer.shadowOpacity = 0.1;
    self.bgView.layer.shadowRadius = 1.0;
    [self.contentView addSubview:self.bgView];
    self.bgView.sd_layout.topSpaceToView(self.contentView, 10)
    .bottomSpaceToView(self.contentView, 10)
    .leftSpaceToView(self.contentView, 10)
    .widthIs(KScreenW-20)
    .heightIs(229);
    
    self.imgView = [[UIImageView alloc]init];
//    self.imgView.backgroundColor = GMBlueColor;
    self.imgView.layer.cornerRadius = 10;
    self.imgView.image = [UIImage imageNamed:@"1.jpg"];
    [self.bgView addSubview:self.imgView];
    self.imgView.sd_layout.topSpaceToView(self.bgView, 10)
    .leftSpaceToView(self.bgView, 0)
    .rightSpaceToView(self.bgView, 0)
    .heightIs(173);
    
    UIView *titleBgView = [[UIView alloc]init];
    titleBgView.backgroundColor = RGBA(255, 255, 255, 0.7);
    [self.imgView addSubview:titleBgView];
    
    self.titleLabel = [[UILabel alloc]init];
    self.titleLabel.backgroundColor = RGBA(255, 255, 255, 0.5);
    self.titleLabel.text = @"课程标题标题标题";
    self.titleLabel.textColor = GMWhiteColor;
    self.titleLabel.textColor = RGB(255, 255, 255);
    self.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular"size:13];
    [self.imgView addSubview:self.titleLabel];
    self.titleLabel.sd_layout.bottomSpaceToView(self.imgView, 10)
    .leftSpaceToView(self.imgView, 0)
    .rightSpaceToView(self.imgView, 0)
    .heightIs(30);
    
    self.tagLiveLabel = [[UILabel alloc]init];
    self.tagLiveLabel.text = @"正在直播";
    self.tagLiveLabel.textColor = RGB(23, 151, 164);
    self.tagLiveLabel.font = [UIFont fontWithName:@"PingFangSC-Regular"size:14];
    [self.bgView addSubview:self.tagLiveLabel];
    self.tagLiveLabel.sd_layout.bottomSpaceToView(self.bgView, 16)
    .leftSpaceToView(self.bgView, 16)
    .widthIs(65).heightIs(15);
    
    self.dateLabel = [[UILabel alloc]init];
    self.dateLabel.textColor = RGB(102, 102, 102);
    self.dateLabel.font = [UIFont fontWithName:@"PingFangSC-Regular"size:14];
    [self.bgView addSubview:self.dateLabel];
    self.dateLabel.sd_layout.bottomSpaceToView(self.bgView, 18)
    .rightSpaceToView(self.bgView, 22)
    .widthIs(130).heightIs(10);

}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
