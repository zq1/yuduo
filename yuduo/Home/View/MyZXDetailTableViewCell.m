//
//  MyZXDetailTableViewCell.m
//  yuduo
//
//  Created by Mac on 2019/8/13.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "MyZXDetailTableViewCell.h"

@implementation MyZXDetailTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier]) {
     
        [self createLayout];
    }
    return self;
}

- (void)createLayout {
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = GMWhiteColor;
    self.bgView.layer.cornerRadius = 10;
    [self.contentView addSubview:self.bgView];
    self.bgView.sd_layout.topSpaceToView(self.contentView, 10)
    .leftSpaceToView(self.contentView, 15)
    .rightSpaceToView(self.contentView, 15)
    .widthIs(KScreenW-30).heightIs(90);
    
    self.headImg = [[UIImageView alloc]init];
    self.headImg.backgroundColor = GMlightGrayColor;
    self.headImg.layer.cornerRadius = 30;
    self.headImg.clipsToBounds = YES;
    [self.bgView addSubview:self.headImg];
    self.headImg.sd_layout.topSpaceToView(self.bgView, 15)
    .leftSpaceToView(self.bgView, 15)
    .widthIs(60).heightEqualToWidth();
    
    self.titleLabel = [[UILabel alloc]init];
    self.titleLabel.text = @"老是流鼻血可能要死了";
    self.titleLabel.textColor = RGB(69, 69, 69);
    self.titleLabel.font = [UIFont fontWithName:KPFType size:14];
    [self.bgView addSubview:self.titleLabel];
    self.titleLabel.sd_layout.topSpaceToView(self.bgView, 23)
    .leftSpaceToView(self.headImg, 17).rightSpaceToView(self.bgView, 15)
    .heightIs(14);
    
    self.timeImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ico_time"]];
    [self.bgView addSubview:self.timeImg];
    self.timeImg.sd_layout.bottomSpaceToView(self.bgView, 25)
    .leftSpaceToView(self.headImg, 17)
    .widthIs(13).heightIs(13);
    
    self.timeLabel = [[UILabel alloc]init];
    self.timeLabel.textColor = RGB(153, 153, 153);
    self.timeLabel.text = @"2019-5-30";
    self.timeLabel.font = [UIFont fontWithName:@"PingFangSC-Regular"size:12];
    [self.bgView addSubview:self.timeLabel];
    self.timeLabel.sd_layout.bottomSpaceToView(self.bgView, 27)
    .leftSpaceToView(self.timeImg, 11)
    .widthIs(74).heightIs(9);
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
