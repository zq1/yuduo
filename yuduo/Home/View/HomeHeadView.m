//
//  HomeHeadView.m
//  yuduo
//
//  Created by Mac on 2019/7/24.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "HomeHeadView.h"

@interface HomeHeadView ()

@property (nonatomic, strong) UILabel *messageBadgeLabel;

@end

@implementation HomeHeadView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
    
        [self creatHeadView];
    }
    return self;
}

- (void)homeSearch:(UIButton *)send {
    self.searButtonViewBlock(send.tag);
}

- (void)creatHeadView {
    UIButton *view = [UIButton buttonWithType:UIButtonTypeCustom];
    view.backgroundColor = [UIColor colorWithRed:245/255.0 green:245/255.0 blue:245/255.0 alpha:1.0];
    [view setTitle:@"请输入搜索内容" forState:UIControlStateNormal];
    view.titleLabel.font = [UIFont fontWithName:KPFType size:13];

    view.layer.cornerRadius = 14.5;
    [view addTarget:self action:@selector(homeSearch:) forControlEvents:UIControlEventTouchUpInside];
    view.tag = 1008611;
    [view setTitleColor:RGB(153, 153, 153) forState:UIControlStateNormal];
    [self addSubview:view];
    view.sd_layout.topSpaceToView(self, 3)
    .leftSpaceToView(self, 15)
    .rightSpaceToView(self, 60)
    
    
    .heightIs(30);
    
    //搜索按钮
    self.searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.searchButton setImage:[UIImage imageNamed:@"ico_search"] forState:UIControlStateNormal];
    [self.searchButton addTarget:self action:@selector(searchClick:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:self.searchButton];
    self.searchButton.sd_layout.topSpaceToView(view, 8)
                       .leftSpaceToView(view, 15)
                       .widthIs(16).heightIs(16);
   
    
    //消息按钮
    self.messageButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.messageButton addTarget:self action:@selector(messageClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.messageButton setImage:[UIImage imageNamed:@"xiaoxi-2"] forState:UIControlStateNormal];
//    self.messageButton.backgroundColor = GMRedColor;
    [self addSubview:self.messageButton];
    self.messageButton.sd_layout.topSpaceToView(self, 8)
    .leftSpaceToView(view, 50)
    .rightSpaceToView(self, 16)
    .widthIs(20).heightIs(16);
    
    
    
    [self creatHomeImage];
    //第一个轮播图
    self.homeOneImge = [[UIImageView alloc]init];
//    self.homeOneImge.backgroundColor = GMBlueColor;
    [self addSubview:self.homeOneImge];
    self.homeOneImge.sd_layout.topSpaceToView(view, 17)
    .leftSpaceToView(self, 15)
    .rightSpaceToView(self, 15).heightIs(175);
    
     
    /*----------首页四个按钮-------------------*/
    NSArray *fourName = @[@"活动",@"课程",@"直播",@"咨询"];
    NSArray *fourImage = @[@"activity",@"course",@"zhibo",@"Consultation"];
    for (int i = 0; i < fourImage.count; i ++) {
        self.fourButton = [UIButton buttonWithType:UIButtonTypeCustom];
//        [self.fourButton setImage:[fourImage objectAtIndex:i] forState:UIControlStateNormal];
//        self.fourButton.backgroundColor = GMBrownColor;
        
        [self.fourButton setImage:[UIImage imageNamed:[fourImage objectAtIndex:i]] forState:UIControlStateNormal];
        
        self.fourButton.frame = CGRectMake(KScreenW/4*i+32, self.homeOneImge.frame.size.height+60, 32, 32);
        
        [self.fourButton addTarget:self action:@selector(fourClick:) forControlEvents:UIControlEventTouchUpInside];
        
        self.fourButton.tag = 10000+i;
        
        [self addSubview:self.fourButton];
        
        self.fourLable = [[UILabel alloc] init];
        self.fourLable.frame = CGRectMake(KScreenW/4*i+34,self.fourButton.frame.origin.y+11+32,28,11.5);
        
        NSString *str = [fourName objectAtIndex:i];
        NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:str attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 12],NSForegroundColorAttributeName: [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1.0]}];
        self.fourLable.text = [fourName objectAtIndex:i];
        self.fourLable.attributedText = string;
        self.fourLable.numberOfLines = 0;
        [self addSubview:self.fourLable];
    }
}

//消息按钮
- (void)messageClick:(UIButton *)btnMessage {
    if (self.messageButton) {
        self.messageBlock(btnMessage.tag);
    }
}
//搜索按钮
- (void)searchClick:(UIButton *)btn {
    if (self.searchBlock) {
        self.searchBlock(btn.tag);
    }
}
#pragma mark 活动课程直播
- (void)fourClick: (UIButton *)send {
    if (self.fourBlock) {
        self.fourBlock(send.tag);
    }
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [_searchText resignFirstResponder];
}

- (void)creatHomeImage {
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
