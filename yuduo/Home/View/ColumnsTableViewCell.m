//
//  ColumnsTableViewCell.m
//  yuduo
//
//  Created by Mac on 2019/7/24.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import "ColumnsTableViewCell.h"

@implementation ColumnsTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createNewCourse];
    }
    return self;
}

   
- (void)createNewCourse {

    [self One];
    [self two];
    [self three];
    [self four];
}



-(void)mapBtnClick:(UIButton *)sender{
    
    NSLog(@"%ld",sender.tag);
}



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
#pragma mark 最新专栏 - 1
- (void)One {
#define Start_X          15.0f      // 第一个按钮的X坐标
#define Start_Y          0.0f     // 第一个按钮的Y坐标
#define Width_Space      10.0f      // 2个按钮之间的横间距
#define Height_Space     10.0f     // 竖间距
#define Button_Height   225.0f    // 高
#define Button_Width    168.0f    // 宽

UIView *view1 = [[UIView alloc]init];
view1.layer.shadowColor = [UIColor blackColor].CGColor;
view1.layer.shadowOffset = CGSizeMake(0, 0);
view1.layer.shadowOpacity = 0.1;
view1.layer.shadowRadius = 3.0;
view1.layer.cornerRadius = 9.0;
view1.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0];
view1.layer.masksToBounds = NO;
[self.contentView addSubview:view1];
view1.sd_layout.topSpaceToView(self.contentView, 25)
.leftSpaceToView(self.contentView, 15)
.widthIs(168*KScreenW/375).heightIs(225);

    UIButton *oneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//        oneBtn.backgroundColor = GMBrownColor;
    oneBtn.frame = view1.frame;
    [oneBtn addTarget:self action:@selector(oneClick) forControlEvents:UIControlEventTouchUpInside];
    [view1 addSubview:oneBtn];
    
//背景
self.imgView = [[UIImageView alloc]initWithFrame:CGRectMake(0,0, Button_Width*KScreenW/375, 112)];
self.imgView.backgroundColor = [UIColor lightGrayColor];
self.imgView.layer.cornerRadius = 10;
[oneBtn addSubview:self.imgView];
//标题
self.titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, self.imgView.frame.size.height+15, 142, 43)];
self.titleLabel.numberOfLines = 2;
//            self.titleLabel.backgroundColor = [UIColor greenColor];
self.titleLabel.text = @"前央视主持人魅力必修课";
[oneBtn addSubview:self.titleLabel];
NSMutableAttributedString *stringTitle = [[NSMutableAttributedString alloc] initWithString:@"前央视主持人李雷：50堂魅力声音必修课" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 14],NSForegroundColorAttributeName: [UIColor colorWithRed:69/255.0 green:69/255.0 blue:69/255.0 alpha:1.0]}];
self.titleLabel.attributedText = stringTitle;

// 课时图标
self.keshiImg = [[UIImageView alloc]initWithFrame:CGRectMake(5, self.titleLabel.frame.origin.y+58, 15, 10)];
//            self.keshiImg.backgroundColor = GMBlackColor;
self.keshiImg.image = [UIImage imageNamed:@"更多"];
[oneBtn addSubview:self.keshiImg];
// 课时label
self.keshiLabel = [[UILabel alloc]initWithFrame:CGRectMake(33, self.keshiImg.frame.origin.y-1, 70, 15)];
[oneBtn addSubview:self.keshiLabel];
NSMutableAttributedString *stringKs = [[NSMutableAttributedString alloc] initWithString:@"55课时" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 11],NSForegroundColorAttributeName: [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1.0]}];

self.keshiLabel.attributedText = stringKs;

// 观看图标
self.seeImg = [[UIImageView alloc]initWithFrame:CGRectMake(self.keshiLabel.frame.origin.x+56, self.keshiImg.frame.origin.y, 15, 10)];
//            self.seeImg.backgroundColor = GMBrownColor;
self.seeImg.image = [UIImage imageNamed:@"ico_look"];
[oneBtn addSubview:self.seeImg];
// 观看人数
self.peoplesLabel = [[UILabel alloc]initWithFrame:CGRectMake(self.seeImg.frame.origin.x+20, self.seeImg.frame.origin.y-1, 40, 15)];
self.peoplesLabel.text = @"2478";
[oneBtn addSubview:self.peoplesLabel];

NSMutableAttributedString *stringP = [[NSMutableAttributedString alloc] initWithString:@"10028" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 11],NSForegroundColorAttributeName: [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1.0]}];

self.peoplesLabel.attributedText = stringP;
self.priceLabel = [[UILabel alloc] init];
self.priceLabel.frame = CGRectMake(11,oneBtn.frame.size.height-20,64,12);
self.priceLabel.numberOfLines = 0;

[oneBtn addSubview:self.priceLabel];

NSMutableAttributedString *stringPrice = [[NSMutableAttributedString alloc] initWithString:@"¥199.00" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 15],NSForegroundColorAttributeName: [UIColor colorWithRed:228/255.0 green:102/255.0 blue:67/255.0 alpha:1.0]}];
self.priceLabel.attributedText = stringPrice;

}

#pragma mark 最新专栏 - 2
- (void)two {
#define Start_X          15.0f      // 第一个按钮的X坐标
#define Start_Y          0.0f     // 第一个按钮的Y坐标
#define Width_Space      10.0f      // 2个按钮之间的横间距
#define Height_Space     10.0f     // 竖间距
#define Button_Height   225.0f    // 高
#define Button_Width    168.0f    // 宽
    
    UIView *view1 = [[UIView alloc]init];
    view1.layer.shadowColor = [UIColor blackColor].CGColor;
    view1.layer.shadowOffset = CGSizeMake(0, 0);
    view1.layer.shadowOpacity = 0.1;
    view1.layer.shadowRadius = 3.0;
    view1.layer.cornerRadius = 9.0;
    view1.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0];
    view1.layer.masksToBounds = NO;
    [self.contentView addSubview:view1];
    view1.sd_layout.topSpaceToView(self.contentView, 25)
    .rightSpaceToView(self.contentView, 15)
    .widthIs(168*KScreenW/375).heightIs(225);
    
    UIButton *oneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    //        oneBtn.backgroundColor = GMBrownColor;
    oneBtn.frame = view1.frame;
    [oneBtn addTarget:self action:@selector(twoClick) forControlEvents:UIControlEventTouchUpInside];
    [view1 addSubview:oneBtn];
    //背景
    self.imgView = [[UIImageView alloc]initWithFrame:CGRectMake(0,0, Button_Width*KScreenW/375, 112)];
    self.imgView.backgroundColor = [UIColor lightGrayColor];
    self.imgView.layer.cornerRadius = 10;
    [oneBtn addSubview:self.imgView];
    //标题
    self.titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, self.imgView.frame.size.height+15, 142, 43)];
    self.titleLabel.numberOfLines = 2;
    //            self.titleLabel.backgroundColor = [UIColor greenColor];
    self.titleLabel.text = @"前央视主持人魅力必修课";
    [oneBtn addSubview:self.titleLabel];
    NSMutableAttributedString *stringTitle = [[NSMutableAttributedString alloc] initWithString:@"前央视主持人李雷：50堂魅力声音必修课" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 14],NSForegroundColorAttributeName: [UIColor colorWithRed:69/255.0 green:69/255.0 blue:69/255.0 alpha:1.0]}];
    self.titleLabel.attributedText = stringTitle;
    
    // 课时图标
    self.keshiImg = [[UIImageView alloc]initWithFrame:CGRectMake(5, self.titleLabel.frame.origin.y+58, 15, 10)];
    //            self.keshiImg.backgroundColor = GMBlackColor;
    self.keshiImg.image = [UIImage imageNamed:@"更多"];
    [oneBtn addSubview:self.keshiImg];
    // 课时label
    self.keshiLabel = [[UILabel alloc]initWithFrame:CGRectMake(33, self.keshiImg.frame.origin.y-1, 70, 15)];
    [oneBtn addSubview:self.keshiLabel];
    NSMutableAttributedString *stringKs = [[NSMutableAttributedString alloc] initWithString:@"55课时" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 11],NSForegroundColorAttributeName: [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1.0]}];
    
    self.keshiLabel.attributedText = stringKs;
    
    // 观看图标
    self.seeImg = [[UIImageView alloc]initWithFrame:CGRectMake(self.keshiLabel.frame.origin.x+56, self.keshiImg.frame.origin.y, 15, 10)];
    //            self.seeImg.backgroundColor = GMBrownColor;
    self.seeImg.image = [UIImage imageNamed:@"ico_look"];
    [oneBtn addSubview:self.seeImg];
    // 观看人数
    self.peoplesLabel = [[UILabel alloc]initWithFrame:CGRectMake(self.seeImg.frame.origin.x+20, self.seeImg.frame.origin.y-1, 40, 15)];
    self.peoplesLabel.text = @"2478";
    [oneBtn addSubview:self.peoplesLabel];
    
    NSMutableAttributedString *stringP = [[NSMutableAttributedString alloc] initWithString:@"10028" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 11],NSForegroundColorAttributeName: [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1.0]}];
    
    self.peoplesLabel.attributedText = stringP;
    self.priceLabel = [[UILabel alloc] init];
    self.priceLabel.frame = CGRectMake(11,oneBtn.frame.size.height-20,64,12);
    self.priceLabel.numberOfLines = 0;
    
    [oneBtn addSubview:self.priceLabel];
    
    NSMutableAttributedString *stringPrice = [[NSMutableAttributedString alloc] initWithString:@"¥199.00" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 15],NSForegroundColorAttributeName: [UIColor colorWithRed:228/255.0 green:102/255.0 blue:67/255.0 alpha:1.0]}];
    self.priceLabel.attributedText = stringPrice;
    
}
#pragma mark 最新专栏 - 3
- (void)three {
#define Start_X          15.0f      // 第一个按钮的X坐标
#define Start_Y          0.0f     // 第一个按钮的Y坐标
#define Width_Space      10.0f      // 2个按钮之间的横间距
#define Height_Space     10.0f     // 竖间距
#define Button_Height   225.0f    // 高
#define Button_Width    168.0f    // 宽
    
    UIView *view1 = [[UIView alloc]init];
    view1.layer.shadowColor = [UIColor blackColor].CGColor;
    view1.layer.shadowOffset = CGSizeMake(0, 0);
    view1.layer.shadowOpacity = 0.1;
    view1.layer.shadowRadius = 3.0;
    view1.layer.cornerRadius = 9.0;
    view1.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0];
    view1.layer.masksToBounds = NO;
    [self.contentView addSubview:view1];
    view1.sd_layout.bottomSpaceToView(self.contentView, 0)
    .leftSpaceToView(self.contentView, 15)
    .widthIs(168*KScreenW/375).heightIs(225);
    
    UIButton *oneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    //        oneBtn.backgroundColor = GMBrownColor;
    [oneBtn addTarget:self action:@selector(threeClick) forControlEvents:UIControlEventTouchUpInside];
    oneBtn.frame = view1.frame;
    [view1 addSubview:oneBtn];
    //背景
    self.imgView = [[UIImageView alloc]initWithFrame:CGRectMake(0,0, Button_Width*KScreenW/375, 112)];
    self.imgView.backgroundColor = [UIColor lightGrayColor];
    self.imgView.layer.cornerRadius = 10;
    [oneBtn addSubview:self.imgView];
    //标题
    self.titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, self.imgView.frame.size.height+15, 142, 43)];
    self.titleLabel.numberOfLines = 2;
    //            self.titleLabel.backgroundColor = [UIColor greenColor];
    self.titleLabel.text = @"前央视主持人魅力必修课";
    [oneBtn addSubview:self.titleLabel];
    NSMutableAttributedString *stringTitle = [[NSMutableAttributedString alloc] initWithString:@"前央视主持人李雷：50堂魅力声音必修课" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 14],NSForegroundColorAttributeName: [UIColor colorWithRed:69/255.0 green:69/255.0 blue:69/255.0 alpha:1.0]}];
    self.titleLabel.attributedText = stringTitle;
    
    // 课时图标
    self.keshiImg = [[UIImageView alloc]initWithFrame:CGRectMake(5, self.titleLabel.frame.origin.y+58, 15, 10)];
    //            self.keshiImg.backgroundColor = GMBlackColor;
    self.keshiImg.image = [UIImage imageNamed:@"更多"];
    [oneBtn addSubview:self.keshiImg];
    // 课时label
    self.keshiLabel = [[UILabel alloc]initWithFrame:CGRectMake(33, self.keshiImg.frame.origin.y-1, 70, 15)];
    [oneBtn addSubview:self.keshiLabel];
    NSMutableAttributedString *stringKs = [[NSMutableAttributedString alloc] initWithString:@"55课时" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 11],NSForegroundColorAttributeName: [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1.0]}];
    
    self.keshiLabel.attributedText = stringKs;
    
    // 观看图标
    self.seeImg = [[UIImageView alloc]initWithFrame:CGRectMake(self.keshiLabel.frame.origin.x+56, self.keshiImg.frame.origin.y, 15, 10)];
    //            self.seeImg.backgroundColor = GMBrownColor;
    self.seeImg.image = [UIImage imageNamed:@"ico_look"];
    [oneBtn addSubview:self.seeImg];
    // 观看人数
    self.peoplesLabel = [[UILabel alloc]initWithFrame:CGRectMake(self.seeImg.frame.origin.x+20, self.seeImg.frame.origin.y-1, 40, 15)];
    self.peoplesLabel.text = @"2478";
    [oneBtn addSubview:self.peoplesLabel];
    
    NSMutableAttributedString *stringP = [[NSMutableAttributedString alloc] initWithString:@"10028" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 11],NSForegroundColorAttributeName: [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1.0]}];
    
    self.peoplesLabel.attributedText = stringP;
    self.priceLabel = [[UILabel alloc] init];
    self.priceLabel.frame = CGRectMake(11,oneBtn.frame.size.height-20,64,12);
    self.priceLabel.numberOfLines = 0;
    
    [oneBtn addSubview:self.priceLabel];
    
    NSMutableAttributedString *stringPrice = [[NSMutableAttributedString alloc] initWithString:@"¥199.00" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 15],NSForegroundColorAttributeName: [UIColor colorWithRed:228/255.0 green:102/255.0 blue:67/255.0 alpha:1.0]}];
    self.priceLabel.attributedText = stringPrice;
    
}

#pragma mark 最新专栏 - 4
- (void)four {
#define Start_X          15.0f      // 第一个按钮的X坐标
#define Start_Y          0.0f     // 第一个按钮的Y坐标
#define Width_Space      10.0f      // 2个按钮之间的横间距
#define Height_Space     10.0f     // 竖间距
#define Button_Height   225.0f    // 高
#define Button_Width    168.0f    // 宽
    
    UIView *view1 = [[UIView alloc]init];
    view1.layer.shadowColor = [UIColor blackColor].CGColor;
    view1.layer.shadowOffset = CGSizeMake(0, 0);
    view1.layer.shadowOpacity = 0.1;
    view1.layer.shadowRadius = 3.0;
    view1.layer.cornerRadius = 9.0;
    view1.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0];
    view1.layer.masksToBounds = NO;
    [self.contentView addSubview:view1];
    view1.sd_layout.bottomSpaceToView(self.contentView, 0)
    .rightSpaceToView(self.contentView, 15)
    .widthIs(168*KScreenW/375).heightIs(225);
    
    UIButton *oneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    //        oneBtn.backgroundColor = GMBrownColor;
    oneBtn.frame = view1.frame;
    [oneBtn addTarget:self action:@selector(fourClick) forControlEvents:UIControlEventTouchUpInside];
    [view1 addSubview:oneBtn];
    //背景
    self.imgView = [[UIImageView alloc]initWithFrame:CGRectMake(0,0, Button_Width*KScreenW/375, 112)];
    self.imgView.backgroundColor = [UIColor lightGrayColor];
    self.imgView.layer.cornerRadius = 10;
    [oneBtn addSubview:self.imgView];
    //标题
    self.titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, self.imgView.frame.size.height+15, 142, 43)];
    self.titleLabel.numberOfLines = 2;
    //            self.titleLabel.backgroundColor = [UIColor greenColor];
    self.titleLabel.text = @"前央视主持人魅力必修课";
    [oneBtn addSubview:self.titleLabel];
    NSMutableAttributedString *stringTitle = [[NSMutableAttributedString alloc] initWithString:@"前央视主持人李雷：50堂魅力声音必修课" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 14],NSForegroundColorAttributeName: [UIColor colorWithRed:69/255.0 green:69/255.0 blue:69/255.0 alpha:1.0]}];
    self.titleLabel.attributedText = stringTitle;
    
    // 课时图标
    self.keshiImg = [[UIImageView alloc]initWithFrame:CGRectMake(5, self.titleLabel.frame.origin.y+58, 15, 10)];
    //            self.keshiImg.backgroundColor = GMBlackColor;
    self.keshiImg.image = [UIImage imageNamed:@"更多"];
    [oneBtn addSubview:self.keshiImg];
    // 课时label
    self.keshiLabel = [[UILabel alloc]initWithFrame:CGRectMake(33, self.keshiImg.frame.origin.y-1, 70, 15)];
    [oneBtn addSubview:self.keshiLabel];
    NSMutableAttributedString *stringKs = [[NSMutableAttributedString alloc] initWithString:@"55课时" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 11],NSForegroundColorAttributeName: [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1.0]}];
    
    self.keshiLabel.attributedText = stringKs;
    
    // 观看图标
    self.seeImg = [[UIImageView alloc]initWithFrame:CGRectMake(self.keshiLabel.frame.origin.x+56, self.keshiImg.frame.origin.y, 15, 10)];
    //            self.seeImg.backgroundColor = GMBrownColor;
    self.seeImg.image = [UIImage imageNamed:@"ico_look"];
    [oneBtn addSubview:self.seeImg];
    // 观看人数
    self.peoplesLabel = [[UILabel alloc]initWithFrame:CGRectMake(self.seeImg.frame.origin.x+20, self.seeImg.frame.origin.y-1, 40, 15)];
    self.peoplesLabel.text = @"2478";
    [oneBtn addSubview:self.peoplesLabel];
    
    NSMutableAttributedString *stringP = [[NSMutableAttributedString alloc] initWithString:@"10028" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 11],NSForegroundColorAttributeName: [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1.0]}];
    
    self.peoplesLabel.attributedText = stringP;
    self.priceLabel = [[UILabel alloc] init];
    self.priceLabel.frame = CGRectMake(11,oneBtn.frame.size.height-20,64,12);
    self.priceLabel.numberOfLines = 0;
    [oneBtn addSubview:self.priceLabel];
    NSMutableAttributedString *stringPrice = [[NSMutableAttributedString alloc] initWithString:@"¥199.00" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 15],NSForegroundColorAttributeName: [UIColor colorWithRed:228/255.0 green:102/255.0 blue:67/255.0 alpha:1.0]}];
    self.priceLabel.attributedText = stringPrice;
}
- (void)oneClick {
    NSLog(@"1111");
}
- (void)twoClick {
    NSLog(@"2222");
}
- (void)threeClick {
    NSLog(@"3333");
}
- (void)fourClick {
    NSLog(@"44444");
}
@end
