//
//  HomeHeadView.h
//  yuduo
//
//  Created by Mac on 2019/7/24.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCScrollView.h"
NS_ASSUME_NONNULL_BEGIN

@interface HomeHeadView : UIView<UITextFieldDelegate>

PropertyStrong(UIButton, searchButton);  //搜索按钮
PropertyStrong(UITextField, searchText); //搜索框

@property (nonatomic,copy)void (^searButtonViewBlock)(NSInteger viewButtonTag);

@property(nonatomic , copy) void (^searchButtonBgViewBlock) (NSInteger searchB);

@property(nonatomic,copy)void(^searchBlock)(NSInteger indext); //

PropertyStrong(MCScrollView, scrollView); //第一个lbt
@property(assign,nonatomic) NSInteger selectIndex;
@property(nonatomic,copy)void(^messageBlock)(NSInteger messageIdex); //消息按钮
PropertyStrong(UIButton, messageButton); //消息点击事件
PropertyStrong(UIImageView, homeOneImge); //首页第一个轮播图

PropertyStrong(UIButton , fourButton);//首页四个按钮
@property (nonatomic , copy) void (^fourBlock)(NSInteger indexBtn);
PropertyStrong(UILabel, fourLable); //四个标题
@end

NS_ASSUME_NONNULL_END
