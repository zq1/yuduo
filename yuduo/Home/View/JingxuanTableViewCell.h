//
//  JingxuanTableViewCell.h
//  yuduo
//
//  Created by Mac on 2019/7/25.
//  Copyright © 2019 yaocongkeji. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface JingxuanTableViewCell : UITableViewCell
PropertyStrong(UIView, bgView); //cell底部view 所有控件加到上面
PropertyStrong(UIImageView, headImgView); //头像
PropertyStrong(UILabel , mianfeiLabel); //免费logo
PropertyStrong(UILabel , titleLabel); // 课程名称标题
PropertyStrong(UILabel, detailLabel); //课程简介
PropertyStrong(UIButton, messageButton); //可是列表按钮
PropertyStrong(UILabel, messageLabel); //可是标签
PropertyStrong(UIImageView, zanImg); //点赞图标
PropertyStrong(UILabel, zanLabel);//点赞人数
PropertyStrong(UILabel, priceLabel); // 现在价格
@property (nonatomic, copy) void (^JingxuanTableViewCellLikeTouchBlock)(void);
@end

NS_ASSUME_NONNULL_END
